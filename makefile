include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl
	$(INSTALL) -D -p -m 0644 include/swl/*.h $(DEST)$(INCLUDEDIR)/swl/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/fileOps
	$(INSTALL) -D -p -m 0644 include/swl/fileOps/*.h $(DEST)$(INCLUDEDIR)/swl/fileOps/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/oo
	$(INSTALL) -D -p -m 0644 include/swl/oo/*.h $(DEST)$(INCLUDEDIR)/swl/oo/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/map
	$(INSTALL) -D -p -m 0644 include/swl/map/*.h $(DEST)$(INCLUDEDIR)/swl/map/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/types
	$(INSTALL) -D -p -m 0644 include/swl/types/*.h $(DEST)$(INCLUDEDIR)/swl/types/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/subtypes
	$(INSTALL) -D -p -m 0644 include/swl/subtypes/*.h $(DEST)$(INCLUDEDIR)/swl/subtypes/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/wifiDm
	$(INSTALL) -D -p -m 0644 include/swl/wifiDm/*.h $(DEST)$(INCLUDEDIR)/swl/wifiDm/
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/swl/ttb
	$(INSTALL) -D -p -m 0644 include/swl/ttb/*.h $(DEST)$(INCLUDEDIR)/swl/ttb/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION)
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config.pc $(PKG_CONFIG_LIBDIR)/swlc.pc
	ln -sfr $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl
	$(INSTALL) -D -p -m 0644 include/swl/*.h $(PKGDIR)$(INCLUDEDIR)/swl/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/fileOps
	$(INSTALL) -D -p -m 0644 include/swl/fileOps/*.h $(PKGDIR)$(INCLUDEDIR)/swl/fileOps/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/oo
	$(INSTALL) -D -p -m 0644 include/swl/oo/*.h $(PKGDIR)$(INCLUDEDIR)/swl/oo/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/map
	$(INSTALL) -D -p -m 0644 include/swl/map/*.h $(PKGDIR)$(INCLUDEDIR)/swl/map/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/types
	$(INSTALL) -D -p -m 0644 include/swl/types/*.h $(PKGDIR)$(INCLUDEDIR)/swl/types/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/subtypes
	$(INSTALL) -D -p -m 0644 include/swl/subtypes/*.h $(PKGDIR)$(INCLUDEDIR)/swl/subtypes/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/wifiDm
	$(INSTALL) -D -p -m 0644 include/swl/wifiDm/*.h $(PKGDIR)$(INCLUDEDIR)/swl/wifiDm/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/swl/ttb
	$(INSTALL) -D -p -m 0644 include/swl/ttb/*.h $(PKGDIR)$(INCLUDEDIR)/swl/ttb/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so.$(VERSION) $(PKGDIR)$(LIBDIR)/$(COMPONENT).so.$(VERSION)
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config.pc $(PKGDIR)$(PKG_CONFIG_LIBDIR)/swlc.pc
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc


test:
	$(MAKE) -C src all
	$(MAKE) -C test report
	$(MAKE) -C test coverage_report

.PHONY: all clean changelog install package doc test