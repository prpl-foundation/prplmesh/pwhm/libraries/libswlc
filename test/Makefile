RUNTEST_SUB_MAKEFILES = $(wildcard */*/Makefile) $(wildcard */Makefile)
RUNTEST_SUBDIRS = $(subst /Makefile,,$(RUNTEST_SUB_MAKEFILES))
REPORT_XMLS = $(subst %,%.xml,$(RUNTEST_SUBDIRS))

# Workaround.
# For some reason on local testruns sometimes there are build errors
# because the PKG_CONFIG_LIBDIR set does not work, so unset it.
unexport PKG_CONFIG_LIBDIR

test: $(RUNTEST_SUBDIRS)
runtest: test

report: $(patsubst %,%.xml,$(REPORT_XMLS))

$(RUNTEST_SUBDIRS):
	$(MAKE) -C $(subst .xml,,$@) runtest

%.xml:
	$(MAKE) -C $(subst .xml,,$@) unit.xml
	cp $(subst .xml,,$@)/unit.xml $@

export GCOV_SETTINGS=--print-summary --root $(realpath ..)
coverage_report:
	mkdir -p coverage
	gcovr $(GCOV_SETTINGS) | tee coverage/gcovr_summary.txt # job output and dashboard
	gcovr $(GCOV_SETTINGS) --html --html-details -o coverage/index.html
	mkdir -p $$CI_PROJECT_DIR/output/result/lib_swlc
	cp *.xml $$CI_PROJECT_DIR/output/result/lib_swlc
	cp */index.html $$CI_PROJECT_DIR/output/result/lib_swlc
	cp */gcovr_summary.txt $$CI_PROJECT_DIR/output/result/lib_swlc

clean:
	rm -rf coverage
	find . -name "*.o" -delete
	find . -name "*.xml" -delete
	find . -name "*.gcno" -delete
	find . -name "*.gcda" -delete
	find . -name "*.d" -delete
	find . -name "test.*_test" -delete
	
	find . -name "gentest_*" -delete

.PHONY: $(RUNTEST_SUBDIRS) $(REPORT_XMLS) coverage_report report runtest test clean
