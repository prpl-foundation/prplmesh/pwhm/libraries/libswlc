/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_hex.h"
#include "swl/swl_common_mac.h"

#include "test_swlc_type_testCommon.h"

typedef struct {
    uint8_t bMac[SWL_MAC_BIN_LEN];
    char cMac[SWL_MAC_CHAR_LEN];
} macTestData_t;

typedef struct {
    swl_macBin_t bMac;
    swl_macChar_t cMac;
} macTestStruct_t;

macTestData_t testDataMacro[] = {
    {.bMac = {0, 0, 0, 0, 0, 0},
        .cMac = "00:00:00:00:00:00"},
    {.bMac = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
        .cMac = "FF:FF:FF:FF:FF:FF"},
    {.bMac = {0x0a, 0x1b, 0x2c, 0x3d, 0x4e, 0x5f},
        .cMac = "0A:1B:2C:3D:4E:5F"},
    {.bMac = {0x0a, 0x1b, 0x2c, 0x3d, 0x4e, 0x5f},
        .cMac = "0A-1b-2C-3d-4E-5f"},
    {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf},
        .cMac = "Aa:b:c:d:ee:f"},
    {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf},
        .cMac = "Aa_b_c_d_ee_f"},
    {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf},
        .cMac = "Aa0b0c0dee0f"},
    {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf},
        .cMac = "Aa*b*c*d*ee*f"},
};

macTestStruct_t testData[] = {
    {.bMac = {.bMac = {0, 0, 0, 0, 0, 0}},
        .cMac = {.cMac = "00:00:00:00:00:00"}},
    {.bMac = {.bMac = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
        .cMac = {.cMac = "FF:FF:FF:FF:FF:FF"}},
    {.bMac = {.bMac = {0x0a, 0x1b, 0x2c, 0x3d, 0x4e, 0x5f}},
        .cMac = {.cMac = "0A:1B:2C:3D:4E:5F"}},
    {.bMac = {.bMac = {0x0a, 0x1b, 0x2c, 0x3d, 0x4e, 0x5f}},
        .cMac = {.cMac = "0A-1b-2C-3d-4E-5f"}},
    {.bMac = {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf}},
        .cMac = {.cMac = "Aa:b:c:d:ee:f"}},
    {.bMac = {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf}},
        .cMac = {.cMac = "Aa_b_c_d_ee_f"}},
    {.bMac = {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf}},
        .cMac = {.cMac = "Aa0b0c0dee0f"}},
    {.bMac = {.bMac = {0xaa, 0x0b, 0x0c, 0x0d, 0xee, 0xf}},
        .cMac = {.cMac = "Aa*b*c*d*ee*f"}},
};

static void test_swl_mac_data(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        swl_macBin_t testBin;
        memset(&testBin, 0, sizeof(swl_macBin_t));
        swl_macChar_t testHex;
        memset(&testHex, 0, sizeof(testHex));

        swl_mac_charToBin(&testBin, &testData[i].cMac);
        assert_true(swl_mac_binMatches(&testData[i].bMac, &testBin));

        swl_mac_binToChar(&testHex, &testData[i].bMac);
        assert_true(swl_mac_charMatches(&testData[i].cMac, &testHex));
    }
}
static void test_swl_mac_macro(void** state _UNUSED) {

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testDataMacro); i++) {
        swl_macBin_t testBin;
        memset(&testBin, 0, sizeof(swl_macBin_t));
        swl_macChar_t testHex;
        memset(&testHex, 0, sizeof(testHex));
        SWL_MAC_CHAR_TO_BIN(&testBin, testDataMacro[i].cMac);
        SWL_MAC_BIN_TO_CHAR(&testHex, testDataMacro[i].bMac);

        assert_true(SWL_MAC_CHAR_MATCHES(testDataMacro[i].cMac, &testHex));
        assert_true(SWL_MAC_BIN_MATCHES(testDataMacro[i].bMac, &testBin));
    }
}

static void test_swl_mac_str_parse_errors(void** state _UNUSED) {
    swl_macBin_t testBin;
    /* short mac str*/
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01-ff-20-ff-00"));
    /* odd length */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01ff20ff000"));
    /* missing separator after 1st byte */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01ff-20-ff-00-00"));
    /* missing separator in the middle*/
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01_ff20_ff_00_00"));
    /* invalid byte value */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01-fff-20-ff-00-00"));
    /* mixed separators. */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01-ff:20-ff-00-00"));
    /* invalid hex char */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, "01-ff-20-fg-00-00"));
    /* empty mac str */
    assert_false(SWL_MAC_CHAR_TO_BIN(&testBin, ""));
}

static void s_helper_swl_mac_charIsStandard(swl_macChar_t* cInMac, bool expectedOk) {
    bool ok;
    ok = swl_mac_charIsStandard(cInMac);
    assert_int_equal(expectedOk, ok);
}

static void test_swl_mac_charIsStandard(void** state _UNUSED) {
    //Normal case: all hex digit are numbers
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "00:00:00:00:00:00", true);
    //Normal case: all hex digit are upper case
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FF:FF:FF:FF:FF:FF", true);
    //Error case: lower case hex digit
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "fF:FF:FF:FF:FF:FF", false);
    //Error case: no separator
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FFFFFFFFFFFF", false);
    //Error case: invalid separator
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FF-FF-FF-FF-FF-FF", false);
    //Error case: mixed separator
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FF:FF:FF-FF:FF:FF", false);
    //Error case: short mac str
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FF:FF:FF:FF:FF", false);
    //Error case: invalid hex digit
    s_helper_swl_mac_charIsStandard((swl_macChar_t*) "FF:FF:gF:FF:FF:FF", false);
}

static void s_helper_swl_mac_charToStandard(const char* inStr, const char* outStr, bool expectedOk) {
    bool ok;
    swl_macChar_t cOutMac;
    ok = swl_mac_charToStandard(&cOutMac, inStr);
    assert_int_equal(expectedOk, ok);
    if(ok) {
        assert_string_equal(cOutMac.cMac, outStr);
    }
}

static void test_swl_mac_charToStandard(void** state _UNUSED) {
    //Normal case: standard format
    s_helper_swl_mac_charToStandard("00:FF:00:FF:FF:00", "00:FF:00:FF:FF:00", true);
    //Normal case: lower to upper
    s_helper_swl_mac_charToStandard("00:ff:00:Ff:aA:00", "00:FF:00:FF:AA:00", true);
    //Normal case: one to two hex digits
    s_helper_swl_mac_charToStandard("0:f:0:Ff:aA:0", "00:0F:00:FF:AA:00", true);
    //Normal case: change separator
    s_helper_swl_mac_charToStandard("2-ff-15-Ff-a-b0", "02:FF:15:FF:0A:B0", true);
    //Normal case: insert separators
    s_helper_swl_mac_charToStandard("00ff00FfaA00", "00:FF:00:FF:AA:00", true);
    //Error case: odd length without separators
    s_helper_swl_mac_charToStandard("00ff00FfaA0", "", false);
    //Error case: mac str too long
    s_helper_swl_mac_charToStandard("00:FF:00:FF:FF:00:FF", "", false);
    //Error case: mac str too short
    s_helper_swl_mac_charToStandard("00:FF:00:FF:FF", "", false);
    //Error case: invalid hex digit
    s_helper_swl_mac_charToStandard("00:ff:00:Ff:gA:00", "", false);
    //Error case: missing separator after first byte
    s_helper_swl_mac_charToStandard("00ff:00:Ff:bA:00", "", false);
    //Error case: missing separator in the middle
    s_helper_swl_mac_charToStandard("00:ff:00:FfbA:00", "", false);
    //Error case: invalid separator
    s_helper_swl_mac_charToStandard("00:ff:00:Ff-bA:00", "", false);
    //Error case: Empty mac
    s_helper_swl_mac_charToStandard("", "", false);
}

static void s_helper_swl_mac_binToCharSep(swl_macBin_t* bInMac, bool uppercase, char separator, const char* outStr, bool expectedOk) {
    bool ok;
    swl_macChar_t cOutMac;
    ok = swl_mac_binToCharSep(&cOutMac, bInMac, uppercase, separator);
    assert_int_equal(expectedOk, ok);
    if(ok) {
        assert_string_equal(cOutMac.cMac, outStr);
    }
}

static void test_swl_mac_binToCharSep(void** state _UNUSED) {
    swl_macBin_t bInMac = {.bMac = {0, 0xff, 0, 0xab, 3, 0}};
    s_helper_swl_mac_binToCharSep(&bInMac, true, ':', "00:FF:00:AB:03:00", true);
    s_helper_swl_mac_binToCharSep(&bInMac, false, '-', "00-ff-00-ab-03-00", true);
    s_helper_swl_mac_binToCharSep(&bInMac, true, 0, "00FF00AB0300", true);
    s_helper_swl_mac_binToCharSep(&bInMac, false, 0, "00ff00ab0300", true);
    s_helper_swl_mac_binToCharSep(NULL, true, 0, "", false);
    assert_false(swl_mac_binToCharSep(NULL, &bInMac, false, 0));
}

typedef struct {
    swl_macChar_t input;
    swl_macChar_t result;
    int64_t value;
    int32_t mask;
} swl_macTest_addTest;

swl_macTest_addTest testAddData[] = {
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "00:00:00:00:00:00"}, .value = 0, .mask = 0},
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "00:00:00:00:00:06"}, .value = 6, .mask = 4},
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "00:00:00:00:00:01"}, .value = 7, .mask = 1},
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "FF:FF:FF:FF:FF:FF"}, .value = -1, .mask = 48},
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "FF:FF:FF:FF:FF:FF"}, .value = -1, .mask = -1},
    {.input = {.cMac = "FF:FF:FF:FF:FF:FF"}, .result = {.cMac = "00:00:00:00:00:00"}, .value = 1, .mask = 48},
    {.input = {.cMac = "FF:FF:FF:FF:FF:FF"}, .result = {.cMac = "00:00:00:00:00:00"}, .value = 1, .mask = -1},
    {.input = {.cMac = "00:00:00:00:00:00"}, .result = {.cMac = "00:00:00:00:01:02"}, .value = 258, .mask = -1},
};

static void test_swl_mac_binAddVal(void** state _UNUSED) {

    assert_false(swl_mac_binAddVal(NULL, 0, 0));

    swl_macBin_t tmpInput;
    swl_macBin_t tmpResult;

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testAddData); i++) {
        swl_mac_charToBin(&tmpInput, &testAddData[i].input);
        swl_mac_charToBin(&tmpResult, &testAddData[i].result);
        bool result = swl_mac_binAddVal(&tmpInput, testAddData[i].value, testAddData[i].mask);
        assert_true(result);
        assert_memory_equal(tmpInput.bMac, tmpResult.bMac, SWL_MAC_BIN_LEN);
    }

    swl_mac_charToBin(&tmpResult, (swl_macChar_t*) "55:55:55:55:55:55");


    for(int i = 1; i <= 48; i++) {
        swl_mac_charToBin(&tmpInput, (swl_macChar_t*) "55:55:55:55:55:55");
        swl_mac_binAddVal(&tmpInput, 0x555555555555, i);
        int byte = (48 - i) / 8;
        int bit = (i - 1) % 8;
        if(bit % 2 == 0) {
            tmpResult.bMac[byte] &= ~(1 << bit);
        } else {
            tmpResult.bMac[byte] |= (1 << bit);
        }
        assert_memory_equal(tmpInput.bMac, tmpResult.bMac, SWL_MAC_BIN_LEN);
    }

    swl_mac_charToBin(&tmpResult, (swl_macChar_t*) "AA:AA:AA:AA:AA:AA");
    for(int i = 1; i <= 48; i++) {
        swl_mac_charToBin(&tmpInput, (swl_macChar_t*) "AA:AA:AA:AA:AA:AA");
        swl_mac_binAddVal(&tmpInput, -1 * 0x555555555555, i);

        int byte = (48 - i) / 8;
        int bit = (i - 1) % 8;
        if(bit % 2 == 0) {
            tmpResult.bMac[byte] |= (1 << bit);
        } else {
            tmpResult.bMac[byte] &= ~(1 << bit);
        }
        assert_memory_equal(tmpInput.bMac, tmpResult.bMac, SWL_MAC_BIN_LEN);
    }
}


static void test_swl_mac_charAddVal(void** state _UNUSED) {
    swl_macChar_t tmpInput;
    assert_false(swl_mac_charAddVal(NULL, 0, 0));
    snprintf(tmpInput.cMac, SWL_MAC_CHAR_LEN, "%s", "");
    assert_false(swl_mac_charAddVal(&tmpInput, 0, 0));
    snprintf(tmpInput.cMac, SWL_MAC_CHAR_LEN, "%s", "GG:GG:GG:GG:GG:GG");
    assert_false(swl_mac_charAddVal(&tmpInput, 0, 0));
    snprintf(tmpInput.cMac, SWL_MAC_CHAR_LEN, "%s", "FF:FF:FF");
    assert_false(swl_mac_charAddVal(&tmpInput, 0, 0));

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testAddData); i++) {
        memcpy(&tmpInput.cMac, &testAddData[i].input, SWL_MAC_CHAR_LEN);

        bool result = swl_mac_charAddVal(&tmpInput, testAddData[i].value, testAddData[i].mask);
        assert_true(result);
        assert_string_equal(tmpInput.cMac, testAddData[i].result.cMac);
    }

    swl_macBin_t tmpResult;
    swl_mac_charToBin(&tmpResult, (swl_macChar_t*) "55:55:55:55:55:55");
    for(int i = 1; i <= 48; i++) {
        snprintf(tmpInput.cMac, SWL_MAC_CHAR_LEN, "%s", "55:55:55:55:55:55");
        swl_mac_charAddVal(&tmpInput, 0x555555555555, i);
        int byte = (48 - i) / 8;
        int bit = (i - 1) % 8;
        if(bit % 2 == 0) {
            tmpResult.bMac[byte] &= ~(1 << bit);
        } else {
            tmpResult.bMac[byte] |= (1 << bit);
        }
        swl_macChar_t tmpResStr;
        swl_mac_binToChar(&tmpResStr, &tmpResult);
        assert_string_equal(tmpInput.cMac, tmpResStr.cMac);
    }

    swl_mac_charToBin(&tmpResult, (swl_macChar_t*) "AA:AA:AA:AA:AA:AA");
    for(int i = 1; i <= 48; i++) {
        snprintf(tmpInput.cMac, SWL_MAC_CHAR_LEN, "%s", "AA:AA:AA:AA:AA:AA");
        swl_mac_charAddVal(&tmpInput, -1 * 0x555555555555, i);

        int byte = (48 - i) / 8;
        int bit = (i - 1) % 8;
        if(bit % 2 == 0) {
            tmpResult.bMac[byte] |= (1 << bit);
        } else {
            tmpResult.bMac[byte] &= ~(1 << bit);
        }
        swl_macChar_t tmpResStr;
        swl_mac_binToChar(&tmpResStr, &tmpResult);
        assert_string_equal(tmpInput.cMac, tmpResStr.cMac);
    }
}

static void s_testBasic(bool (* tgtBinFun)(const swl_macBin_t* mac), bool (* tgtCharFun)(const swl_macChar_t* mac), bool tgtNull, bool tgtbCast) {
    assert_false(tgtBinFun(NULL));
    assert_false(tgtCharFun(NULL));

    assert_int_equal(tgtNull, tgtBinFun(&g_swl_macBin_null));
    assert_int_equal(tgtNull, tgtCharFun(&g_swl_macChar_null));

    assert_int_equal(tgtbCast, tgtBinFun(&g_swl_macBin_bCast));
    assert_int_equal(tgtbCast, tgtCharFun(&g_swl_macChar_bCast));
}

static void s_testHelper(bool (* tgtBinFun)(const swl_macBin_t* mac), bool (* tgtCharFun)(const swl_macChar_t* mac), swl_macBin_t* binArray, size_t nrEl, bool tgtVal) {
    for(size_t i = 0; i < nrEl; i++) {
        if(tgtBinFun(&binArray[i]) != tgtVal) {
            printf("test fail array " SWL_MAC_FMT " exp: %u", SWL_MAC_ARG(binArray[i].bMac), tgtVal);
            assert_true(false);
        }

        swl_macChar_t testMac;
        assert_true(swl_mac_binToChar(&testMac, &binArray[i]));

        if(tgtCharFun(&testMac) != tgtVal) {
            printf("test fail array " SWL_MAC_FMT " exp: %u", SWL_MAC_ARG(binArray[i].bMac), tgtVal);
            assert_true(false);
        }
    }
}


static void test_swl_mac_isLocal(void** state _UNUSED) {
    s_testBasic(swl_mac_binIsLocal, swl_mac_charIsLocal, false, true);

    swl_macBin_t falseArray[] = { {{0, 0, 0, 0, 0, 0}},
        {{0xfd, 0xff, 0xff, 0xff, 0xff, 0xff}} };
    swl_macBin_t trueArray[] = { {{0x02, 0, 0, 0, 0, 0}},
        {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}} };
    s_testHelper(swl_mac_binIsLocal, swl_mac_charIsLocal, falseArray, SWL_ARRAY_SIZE(falseArray), false);
    s_testHelper(swl_mac_binIsLocal, swl_mac_charIsLocal, trueArray, SWL_ARRAY_SIZE(trueArray), true);
}

static void test_swl_mac_isBroadcast(void** state _UNUSED) {
    s_testBasic(swl_mac_binIsBroadcast, swl_mac_charIsBroadcast, false, true);

    swl_macBin_t falseArray[] = { {{0, 0, 0, 0, 0, 0}},
        {{0x02, 0, 0, 0, 0, 0}}, };
    swl_macBin_t trueArray[] = { {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}} };
    s_testHelper(swl_mac_binIsBroadcast, swl_mac_charIsBroadcast, falseArray, SWL_ARRAY_SIZE(falseArray), false);
    s_testHelper(swl_mac_binIsBroadcast, swl_mac_charIsBroadcast, trueArray, SWL_ARRAY_SIZE(trueArray), true);
}

static void test_swl_mac_isNull(void** state _UNUSED) {
    s_testBasic(swl_mac_binIsNull, swl_mac_charIsNull, true, false);

    swl_macBin_t falseArray[] = { {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
        {{0x02, 0, 0, 0, 0, 0}},
        {{0xfd, 0xff, 0xff, 0xff, 0xff, 0xff}} };
    swl_macBin_t trueArray[] = { {{0, 0, 0, 0, 0, 0}} };
    s_testHelper(swl_mac_binIsNull, swl_mac_charIsNull, falseArray, SWL_ARRAY_SIZE(falseArray), false);
    s_testHelper(swl_mac_binIsNull, swl_mac_charIsNull, trueArray, SWL_ARRAY_SIZE(trueArray), true);
}

static void test_swl_mac_charClear(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        swl_macChar_t tmp = testData[i].cMac;
        assert_int_equal(i == 0, swl_mac_charIsNull(&tmp));
        swl_mac_charClear(&tmp);
        assert_true(swl_mac_charIsNull(&tmp));
    }
}

static void test_swl_mac_binClear(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        swl_macBin_t tmp = testData[i].bMac;
        assert_int_equal(i == 0, swl_mac_binIsNull(&tmp));
        swl_mac_binClear(&tmp);
        assert_true(swl_mac_binIsNull(&tmp));
    }
}


static void test_swl_mac_isValid(void** state _UNUSED) {
    swl_macChar_t testChar;
    memset(&testChar, 0, sizeof(testChar));
    /* empty MAC */
    assert_false(swl_mac_charIsValidStaMac(&testChar));
    /* NULL MAC */
    assert_false(swl_mac_charIsValidStaMac(NULL));
    /* Broadcast MAC */
    snprintf(testChar.cMac, SWL_MAC_CHAR_LEN, "%s", "FF:FF:FF:FF:FF:FF");
    assert_false(swl_mac_charIsValidStaMac(&testChar));
    /* Valid MAC */
    snprintf(testChar.cMac, SWL_MAC_CHAR_LEN, "%s", "AA:BB:CC:DD:EE:FF");
    assert_true(swl_mac_charIsValidStaMac(&testChar));
}

static void test_swl_mac_isMulticast(void** state _UNUSED) {
    s_testBasic(swl_mac_binIsMulticast, swl_mac_charIsMulticast, false, false);

    swl_macBin_t falseArray[] = { {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
        {{0x02, 0, 0, 0, 0, 0}},
        {{0x01, 0x00, 0x5f, 0x01, 0x02, 0x03}},
        {{0x33, 0x34, 0xaa, 0x06, 0x05, 0x04}},
        {{0x01, 0x80, 0xcf, 0x07, 0x08, 0x09}},
        {{0x01, 0x1b, 0x1a, 0x0c, 0x0b, 0x0a}},
        {{0x01, 0x0c, 0xce, 0x0d, 0x0e, 0x0f}},
        {{0x01, 0x00, 0x0d, 0x03, 0x02, 0x01}},
        {{0xfd, 0xff, 0xff, 0xff, 0xff, 0xff}},
        {{0xfd, 0xff, 0xff, 0xff, 0xff, 0xff}} };
    swl_macBin_t trueArray[] = {{{0x01, 0x00, 0x5e, 0x01, 0x02, 0x03}},
        {{0x33, 0x33, 0xa0, 0x06, 0x05, 0x04}},
        {{0x01, 0x80, 0xc2, 0x07, 0x08, 0x09}},
        {{0x01, 0x1b, 0x19, 0x0c, 0x0b, 0x0a}},
        {{0x01, 0x0c, 0xcd, 0x0d, 0x0e, 0x0f}},
        {{0x01, 0x00, 0x0c, 0x03, 0x02, 0x01}}, };
    s_testHelper(swl_mac_binIsMulticast, swl_mac_charIsMulticast, falseArray, SWL_ARRAY_SIZE(falseArray), false);
    s_testHelper(swl_mac_binIsMulticast, swl_mac_charIsMulticast, trueArray, SWL_ARRAY_SIZE(trueArray), true);
}

typedef struct {
    uint8_t test1;
    uint8_t test2;
    uint8_t testMask;
} mask_test_t;

void s_testMask(swl_macBin_t* mac1, swl_macBin_t* mac2, swl_macBin_t* mask, bool target) {
    bool testBase = swl_mac_binMatchesMask(mac1, mac2, mask);

    swl_macMask_t macMask;
    memcpy(&macMask.mac, mac2, sizeof(swl_macBin_t));
    memcpy(&macMask.mask, mask, sizeof(swl_macBin_t));

    bool testMask = swl_macMask_binMatches(&macMask, mac1);

    if((testBase != target) || (testMask != target)) {
        printf(" fail test mask :" SWL_MAC_FMT " " SWL_MAC_FMT " " SWL_MAC_FMT " %u : %u / %u \n", SWL_MAC_ARG(mac1->bMac), SWL_MAC_ARG(mac2->bMac), SWL_MAC_ARG(mask->bMac),
               target, testBase, testMask);
        assert_false(true);
    }

    swl_macChar_t charMac1;
    swl_macChar_t charMac2;
    swl_macChar_t charMask;
    swl_mac_binToChar(&charMac1, mac1);
    swl_mac_binToChar(&charMac2, mac2);
    swl_mac_binToChar(&charMask, mask);
    bool testChar = swl_macMask_charMatches(&macMask, &charMac1);
    bool testCharMask = swl_mac_charMatchesMask(&charMac1, &charMac2, &charMask);
    if((testChar != target) || (testCharMask != target)) {
        printf(" fail test mask : %s " SWL_MAC_FMT " " SWL_MAC_FMT " %u : %u / %u \n",
               charMac1.cMac, SWL_MAC_ARG(mac2->bMac), SWL_MAC_ARG(mask->bMac),
               target, testChar, testCharMask);
        assert_false(true);
    }
}

static void test_swl_mac_testMask(void** state _UNUSED) {
    mask_test_t testArrayTrue[] = {
        {0x00, 0x00, 0x00},
        {0xff, 0xff, 0xff},
        {0xff, 0xff, 0xaa},
        {0xff, 0xaf, 0xaa},
        {0xfa, 0xaf, 0xaa},
        {0xa, 0x6, 0x00},
        {0xff, 0xee, 0xcc},
    };

    mask_test_t testArrayFalse[] = {
        {0x00, 0x01, 0xff},
        {0xf1, 0x01, 0xff},
        {0xff, 0x7f, 0x80},
        {0x7f, 0xff, 0x80},
    };

    swl_macBin_t mac1;
    swl_macBin_t mac2;
    swl_macBin_t macMask;

    for(uint8_t i = 0; i < SWL_ARRAY_SIZE(testArrayTrue); i++) {
        memset(&mac1, 0, SWL_MAC_BIN_LEN);
        memset(&mac2, 0, SWL_MAC_BIN_LEN);
        memset(&macMask, 0, SWL_MAC_BIN_LEN);
        for(uint8_t j = 0; j < SWL_MAC_BIN_LEN; j++) {
            mac1.bMac[j] = testArrayTrue[i].test1;
            mac2.bMac[j] = testArrayTrue[i].test2;
            macMask.bMac[j] = testArrayTrue[i].testMask;
            s_testMask(&mac1, &mac2, &macMask, true);
        }
    }

    for(uint8_t i = 0; i < SWL_ARRAY_SIZE(testArrayFalse); i++) {
        for(uint8_t j = 0; j < SWL_MAC_BIN_LEN; j++) {
            memset(&mac1, 0, SWL_MAC_BIN_LEN);
            memset(&mac2, 0, SWL_MAC_BIN_LEN);
            memset(&macMask, 0, SWL_MAC_BIN_LEN);
            mac1.bMac[j] = testArrayFalse[i].test1;
            mac2.bMac[j] = testArrayFalse[i].test2;
            macMask.bMac[j] = testArrayFalse[i].testMask;
            s_testMask(&mac1, &mac2, &macMask, false);
        }
    }

}
#define NR_MAC_TEST_VAL 4

const char* macStr_strVal[NR_MAC_TEST_VAL] = {"00:00:00:00:00:00",
    "FF:FF:FF:FF:FF:FF",
    "AA:BB:CC:DD:EE:FF",
    "00:00:00:00:00:00", };

size_t macStr_count[NR_MAC_TEST_VAL] = {2, 1, 1, 2};

swl_macChar_t macStr_base[NR_MAC_TEST_VAL] = {
    {.cMac = "00:00:00:00:00:00"},
    {.cMac = "FF:FF:FF:FF:FF:FF"},
    {.cMac = "AA:BB:CC:DD:EE:FF"},
    {.cMac = "00:00:00:00:00:00"},
};

swl_macBin_t macBin_base[NR_MAC_TEST_VAL] = {
    {.bMac = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}},
    {.bMac = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}},
    {.bMac = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF}},
    {.bMac = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}},
};

swl_macChar_t macStr_notContains[NR_MAC_TEST_VAL] = {
    {.cMac = "00:00:00:00:00:01"},
    {.cMac = "FF:FF:FF:FF:FF:FA"},
    {.cMac = "FF:EE:DD:CC:BB:AA"},
    {.cMac = "01:00:00:00:00:00"},
};

swl_macBin_t macBin_notContains[NR_MAC_TEST_VAL] = {
    {.bMac = {0x00, 0x00, 0x00, 0x00, 0x00, 0x01}},
    {.bMac = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE}},
    {.bMac = {0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA}},
    {.bMac = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00}},
};

#define NR_MAC_TEST_BAD_VALS 3

const char* macStr_bad[NR_MAC_TEST_BAD_VALS] = {
    "00:11",                // too short
    "GG:HH:II:00:00:00",    // invalid char
    "FF:EE:DD:CC:BB:AA:BB", // to long
};

swlc_type_testCommonData_t testMacCharData = {
    .name = "macChar",
    .testType = swl_type_macChar,
    .refType = &gtSwl_type_macCharPtr.type,
    .strValues = macStr_strVal,
    .baseValues = &macStr_base,
    .valueCount = macStr_count,
    .notContains = &macStr_notContains,
    .nrTestValues = NR_MAC_TEST_VAL,
    .badVals = macStr_bad,
    .nrBadVals = NR_MAC_TEST_BAD_VALS
};

swlc_type_testCommonData_t testMacBinData = {
    .name = "macBin",
    .testType = swl_type_macBin,
    .refType = &gtSwl_type_macBinPtr.type,
    .strValues = macStr_strVal,
    .baseValues = &macBin_base,
    .valueCount = macStr_count,
    .notContains = &macBin_notContains,
    .nrTestValues = NR_MAC_TEST_VAL,
    .badVals = macStr_bad,
    .nrBadVals = NR_MAC_TEST_BAD_VALS
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mac_data),
        cmocka_unit_test(test_swl_mac_macro),
        cmocka_unit_test(test_swl_mac_str_parse_errors),
        cmocka_unit_test(test_swl_mac_charIsStandard),
        cmocka_unit_test(test_swl_mac_charToStandard),
        cmocka_unit_test(test_swl_mac_binToCharSep),
        cmocka_unit_test(test_swl_mac_binAddVal),
        cmocka_unit_test(test_swl_mac_charAddVal),
        cmocka_unit_test(test_swl_mac_isLocal),
        cmocka_unit_test(test_swl_mac_isBroadcast),
        cmocka_unit_test(test_swl_mac_isNull),
        cmocka_unit_test(test_swl_mac_charClear),
        cmocka_unit_test(test_swl_mac_binClear),
        cmocka_unit_test(test_swl_mac_isMulticast),
        cmocka_unit_test(test_swl_mac_testMask),
        cmocka_unit_test(test_swl_mac_isValid),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);

    rc |= runCommonTypeTest(&testMacCharData);
    rc |= runCommonTypeTest(&testMacBinData);

    sahTraceClose();
    return rc;
}
