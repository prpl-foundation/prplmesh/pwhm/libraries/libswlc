/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <arpa/inet.h>

#include "test-toolbox/ttb_assert.h"

#include <debug/sahtrace.h>
#include "swl/swl_string.h"
#include "swl/swl_common.h"

static void test_swl_str_toLower(void** state) {
    (void) state;
    char buffer[32] = {0};
    snprintf(buffer, sizeof(buffer), "AbCdEfGh1234");
    swl_str_toLower(buffer, sizeof(buffer));
    assert_string_equal(buffer, "abcdefgh1234");

    snprintf(buffer, sizeof(buffer), "AbCdEfGh");
    swl_str_toLower(buffer, 4);
    assert_string_equal(buffer, "abcdEfGh");
    return;
}

static void test_swl_str_toUpper(void** state) {
    (void) state;
    char buffer[32] = {0};
    snprintf(buffer, sizeof(buffer), "AbCdEfGh1234");
    swl_str_toUpper(buffer, sizeof(buffer));
    assert_string_equal(buffer, "ABCDEFGH1234");


    snprintf(buffer, sizeof(buffer), "AbCdEfGh");
    swl_str_toUpper(buffer, 4);
    assert_string_equal(buffer, "ABCDEfGh");
    return;
}

static void test_swl_str_copy(void** state) {
    (void) state;
    char buffer[32] = {0};
    bool success = swl_str_copy(buffer, sizeof(buffer), "AbCdEfGh1234");
    assert_string_equal(buffer, "AbCdEfGh1234");
    assert_true(success);

    success = swl_str_copy(buffer, sizeof(buffer), NULL);
    assert_string_equal(buffer, "");
    assert_true(success);

    char smallBuf[4] = {0};
    success = swl_str_copy(smallBuf, sizeof(smallBuf), "AbCdEfGh1234");
    assert_int_equal(strlen(smallBuf), 3);
    assert_string_equal(smallBuf, "AbC");
    assert_false(success);

    success = swl_str_copy(smallBuf, sizeof(smallBuf), NULL);
    assert_string_equal(smallBuf, "");
    assert_true(success);
}


static void test_swl_str_cat(void** state) {
    (void) state;
    char buffer[10] = {0};
    bool success = swl_str_cat(buffer, sizeof(buffer), "abc");
    assert_string_equal(buffer, "abc");
    assert_true(success);

    success = swl_str_cat(buffer, sizeof(buffer), "def");
    assert_string_equal(buffer, "abcdef");
    assert_true(success);

    success = swl_str_cat(buffer, sizeof(buffer), NULL);
    assert_string_equal(buffer, "abcdef");
    assert_true(success);

    success = swl_str_cat(buffer, sizeof(buffer), "abcdef");
    assert_string_equal(buffer, "abcdefabc");
    assert_false(success);

    success = swl_str_cat(buffer, sizeof(buffer), NULL);
    assert_string_equal(buffer, "abcdefabc");
    assert_true(success);
}

static void test_swl_str_ncopy(void** state) {
    (void) state;
    char buffer[32] = {0};
    bool success = swl_str_ncopy(buffer, sizeof(buffer), "AbCdEfGh1234", 6);
    assert_string_equal(buffer, "AbCdEf");
    assert_true(success);

    success = swl_str_ncopy(buffer, sizeof(buffer), NULL, 5);
    assert_string_equal(buffer, "");
    assert_true(success);

    char smallBuf[4] = {0};
    char srcBuf[] = {'f', 'f', 'f'};
    success = swl_str_ncopy(smallBuf, sizeof(smallBuf), srcBuf, 3);
    assert_string_equal(smallBuf, "fff");
    assert_true(success);

    success = swl_str_ncopy(smallBuf, sizeof(smallBuf), "ghijk", 4);
    assert_int_equal(strlen(smallBuf), 3);
    assert_string_equal(smallBuf, "ghi");
    assert_false(success);

    success = swl_str_ncopy(smallBuf, sizeof(smallBuf), "xxxxx", 0);
    assert_string_equal(smallBuf, "ghi");
    assert_true(success);
}

static void test_swl_str_ncat(void** state) {
    (void) state;
    char buffer[10] = {0};
    bool success = swl_str_ncat(buffer, sizeof(buffer), "abcb", 3);
    assert_string_equal(buffer, "abc");
    assert_true(success);

    success = swl_str_ncat(buffer, sizeof(buffer), "defg", 2);
    assert_string_equal(buffer, "abcde");
    assert_true(success);

    success = swl_str_ncat(buffer, sizeof(buffer), NULL, 5);
    assert_string_equal(buffer, "abcde");
    assert_true(success);

    success = swl_str_ncat(buffer, sizeof(buffer), "ffffff", 0);
    assert_string_equal(buffer, "abcde");
    assert_true(success);

    char buf[] = {'f', 'f', 'f'};
    success = swl_str_ncat(buffer, sizeof(buffer), &buf[1], 2);
    assert_string_equal(buffer, "abcdeff");
    assert_true(success);

    success = swl_str_ncat(buffer, sizeof(buffer), "ghijkl", 6);
    assert_string_equal(buffer, "abcdeffgh");
    assert_false(success);
}

static void test_swl_str_catFormat(void** state) {
    (void) state;
    char buffer[10] = {0};
    bool success = swl_str_catFormat(buffer, sizeof(buffer), "%s%u", "abc", 1);
    assert_string_equal(buffer, "abc1");
    assert_true(success);

    success = swl_str_catFormat(buffer, sizeof(buffer), "%s%u", "def", 2);
    assert_string_equal(buffer, "abc1def2");
    assert_true(success);

    success = swl_str_catFormat(buffer, sizeof(buffer), "%s", "abc");
    assert_string_equal(buffer, "abc1def2a");
    assert_false(success);

    success = swl_str_catFormat(buffer, sizeof(buffer), NULL);
    assert_false(success);

    success = swl_str_catFormat(buffer, sizeof(buffer), "%s", "");
    assert_true(success);

    strncpy(buffer, "a", sizeof(buffer));
    success = swl_str_catFormat(buffer, sizeof(buffer), "%s", NULL);
    assert_string_equal(buffer, "a(null)");
    assert_true(success);
}

static void test_swl_strlst_cat(void** state) {
    (void) state;

    char buffer[10] = {0};
    bool success = swl_strlst_cat(buffer, sizeof(buffer), ",", "abc");
    assert_string_equal(buffer, "abc");
    assert_true(success);

    success = swl_strlst_cat(buffer, sizeof(buffer), ",", "def");
    assert_string_equal(buffer, "abc,def");
    assert_true(success);

    success = swl_strlst_cat(buffer, sizeof(buffer), ",", "toolong");
    assert_false(success);

    // not enough space for trailing '\0'.
    strncpy(buffer, "12345678", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), ",", "X");
    assert_false(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), "very very very very long", "abc");
    assert_false(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), "", "def");
    assert_string_equal(buffer, "abcdef");
    assert_true(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), " and ", "");
    assert_string_equal(buffer, "abc and ");
    assert_true(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), NULL, "def");
    assert_true(success);
    assert_string_equal(buffer, "abcdef");

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_cat(buffer, sizeof(buffer), ";", NULL);
    assert_string_equal(buffer, "abc;");
    assert_true(success);
}


static void test_swl_strlst_catFormat(void** state) {
    (void) state;

    char buffer[10] = {0};
    bool success;

    strncpy(buffer, "", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s%u", "abc", 1);
    assert_string_equal(buffer, "abc1");
    assert_true(success);

    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s%u", "def", 2);
    assert_string_equal(buffer, "abc1,def2");
    assert_true(success);

    strncpy(buffer, "", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s", "abc");
    assert_string_equal(buffer, "abc");
    assert_true(success);

    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s", "def");
    assert_string_equal(buffer, "abc,def");
    assert_true(success);

    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s", "toolong");
    assert_false(success);

    // not enough space for trailing '\0'.
    strncpy(buffer, "12345678", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s", "X");
    assert_false(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), "%s", "very very very very long", "abc");
    assert_false(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), "", "%s", "def");
    assert_string_equal(buffer, "abcdef");
    assert_true(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ",", "%s %u", "a", 123);
    assert_string_equal(buffer, "abc,a 123");
    assert_true(success);

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), NULL, "%s", "def");
    assert_true(success);
    assert_string_equal(buffer, "abcdef");

    strncpy(buffer, "abc", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ";", "");
    assert_string_equal(buffer, "abc;");
    assert_true(success);

    strncpy(buffer, "a", sizeof(buffer));
    success = swl_strlst_catFormat(buffer, sizeof(buffer), ";", "%s", NULL);
    assert_string_equal(buffer, "a;(null)");
    assert_true(success);
}

static void test_swl_strlst_contains(void** state _UNUSED) {
    // Normal cases
    assert_true(swl_strlst_contains("abc,def,ghi", ",", "abc"));
    assert_true(swl_strlst_contains("abc,def,ghi", ",", "def"));
    assert_true(swl_strlst_contains("abc,def,ghi", ",", "ghi"));

    // needle substring of element
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "a"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "b"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "c"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "d"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "e"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "f"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "g"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "h"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", "i"));

    // element substring of needle
    assert_false(swl_strlst_contains("a,d,g", ",", "abc"));
    assert_false(swl_strlst_contains("a,d,g", ",", "def"));
    assert_false(swl_strlst_contains("a,d,g", ",", "ghi"));
    assert_false(swl_strlst_contains("a,d,g", ",", "xa"));
    assert_false(swl_strlst_contains("a,d,g", ",", "xd"));
    assert_false(swl_strlst_contains("a,d,g", ",", "xg"));

    // Null argument
    assert_false(swl_strlst_contains(NULL, ",", "abc"));
    assert_false(swl_strlst_contains("abc,def,ghi", NULL, "def"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", NULL));
    assert_false(swl_strlst_contains(NULL, NULL, NULL));

    // Empty arguments
    assert_false(swl_strlst_contains("", ",", "abc"));
    assert_false(swl_strlst_contains("abc,def,ghi", "", "def"));
    assert_false(swl_strlst_contains("abc,def,ghi", ",", ""));

    // Long separators
    assert_true(swl_strlst_contains("hilongseparatorhellolongseparatorbye", "longseparator", "hi"));
    assert_true(swl_strlst_contains("hilongseparatorhellolongseparatorbye", "longseparator", "hello"));
    assert_true(swl_strlst_contains("hilongseparatorhellolongseparatorbye", "longseparator", "bye"));

    // Empty element
    assert_false(swl_strlst_contains("a,b,c", ",", ""));
    assert_false(swl_strlst_contains("abc,,ghi", ",", ""));
    assert_false(swl_strlst_contains(",abc,ghi", ",", ""));
    assert_false(swl_strlst_contains("abc,def,", ",", ""));
    assert_false(swl_strlst_contains(",", ",", ""));
    assert_false(swl_strlst_contains("longseparator", "longseparator", ""));
    assert_false(swl_strlst_contains("longseparatorlongseparator", "longseparator", ""));

    // Needle contains separator
    assert_false(swl_strlst_contains("a,b,c", ",", "a,b"));
    assert_false(swl_strlst_contains("a,b,c", ",", ",c"));
    assert_false(swl_strlst_contains("a,b,c", ",", ","));
    assert_false(swl_strlst_contains("a,b,c", ",", "a,b,c"));
}

static void testCatChar(char* baseChar, uint32_t bufSize, char addChar, char* expectedChar, bool outcome) {
    char buffer[bufSize];
    swl_str_copy(buffer, bufSize, baseChar);

    bool success = swl_str_catChar(buffer, bufSize, addChar);
    assert_string_equal(buffer, expectedChar);
    assert_int_equal(success, outcome);
}

static void test_swl_str_catChar(void** state) {
    (void) state;
    testCatChar("abc", 5, 'd', "abcd", true);
    testCatChar("", 5, 'd', "d", true);
    testCatChar("abc", 4, 'd', "abc", false);
    testCatChar("abc", 4, '\0', "abc", false);
    testCatChar("abc", 5, '\0', "abc", true);
}

static void test_swl_str_countChar(void** state) {
    (void) state;
    assert_int_equal(0, swl_str_countChar("", ' '));
    assert_int_equal(5, swl_str_countChar("     ", ' '));
    assert_int_equal(2, swl_str_countChar("abcdefabcdef", 'a'));
    assert_int_equal(5, swl_str_countChar("a a a a a", 'a'));
}

static void test_swl_str_startsWith(void** state) {
    (void) state;
    assert_true(swl_str_startsWith("", ""));
    assert_true(swl_str_startsWith("abcdef", ""));
    assert_true(swl_str_startsWith("abcdef", "abc"));
    assert_true(swl_str_startsWith(NULL, NULL));
    assert_false(swl_str_startsWith("defabc", "abc"));
    assert_false(swl_str_startsWith("defabc", NULL));
    assert_false(swl_str_startsWith(NULL, "abc"));
}

static void test_swl_str_matches(void** state) {
    (void) state;
    assert_true(swl_str_matches(NULL, NULL));
    assert_true(swl_str_matches("", ""));
    assert_true(swl_str_matches("abc", "abc"));
    assert_true(swl_str_matches("\ntest\r\t", "\ntest\r\t"));

    assert_false(swl_str_matches("abc", NULL));
    assert_false(swl_str_matches(NULL, "abc"));
    assert_false(swl_str_matches("\ntest\r", "\ntest\r\t"));
    assert_false(swl_str_matches("cba", "abc"));
    assert_false(swl_str_matches("abc", "abcd"));
    assert_false(swl_str_matches("abcd", "abc"));
    assert_false(swl_str_matches("abc", "abcd"));
    assert_false(swl_str_matches("abc", ""));
    assert_false(swl_str_matches("", "abc"));
}

static void test_swl_str_nmatches(void** state) {
    (void) state;
    assert_true(swl_str_nmatches(NULL, NULL, 10));
    assert_true(swl_str_nmatches("", "", 10));
    assert_true(swl_str_nmatches("abc", "abc", 10));
    assert_true(swl_str_nmatches("\ntest\r\t", "\ntest\r\t", 10));

    assert_true(swl_str_nmatches("abc", "def", 0));
    assert_true(swl_str_nmatches("abc123", "abcdef", 3));

    assert_false(swl_str_nmatches("abc", NULL, 10));
    assert_false(swl_str_nmatches(NULL, "abc", 10));
    assert_false(swl_str_nmatches("\ntest\r", "\ntest\r\t", 10));
    assert_false(swl_str_nmatches("cba", "abc", 10));
    assert_false(swl_str_nmatches("abc", "abcd", 10));
    assert_false(swl_str_nmatches("abcd", "abc", 10));
    assert_false(swl_str_nmatches("abc", "abcd", 10));
    assert_false(swl_str_nmatches("abc", "", 10));
    assert_false(swl_str_nmatches("", "abc", 10));
}

static void test_swl_str_getMismatchIndex(void** state _UNUSED) {
    ttb_assert_int_eq(swl_str_getMismatchIndex("abc", "abc"), SWL_RC_ERROR);
    ttb_assert_int_eq(swl_str_getMismatchIndex("abc", ""), 0);
    ttb_assert_int_eq(swl_str_getMismatchIndex("", "abc"), 0);
    ttb_assert_int_eq(swl_str_getMismatchIndex("abc", "bc"), 0);
    ttb_assert_int_eq(swl_str_getMismatchIndex("bc", "abc"), 0);
    ttb_assert_int_eq(swl_str_getMismatchIndex("abc", "abcd"), 3);
    ttb_assert_int_eq(swl_str_getMismatchIndex("abcd", "abc"), 3);

    ttb_assert_int_eq(swl_str_getMismatchIndex("ab cd", "abcd"), 2);
    ttb_assert_int_eq(swl_str_getMismatchIndex("abcd", "ab cd"), 2);


    ttb_assert_int_eq(swl_str_getMismatchIndex("abc", NULL), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_getMismatchIndex(NULL, NULL), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_getMismatchIndex(NULL, "abc"), SWL_RC_INVALID_PARAM);
}

static void test_swl_str_ngetMismatchIndex(void** state _UNUSED) {
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "abc", 3), SWL_RC_ERROR);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "", 3), 0);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("", "abc", 3), 0);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "bc", 3), 0);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("bc", "abc", 3), 0);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "abcd", 4), 3);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abcd", "abc", 4), 3);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("ab cd", "abcd", 5), 2);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abcd", "ab cd", 5), 2);

    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abcF", "abcG", 3), SWL_RC_ERROR);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abcFdef", "abcGdef", 3), SWL_RC_ERROR);



    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", NULL, 3), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex(NULL, NULL, 3), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex(NULL, "abc", 3), SWL_RC_INVALID_PARAM);

    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "abc", 0), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", "abc", -1), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex("abc", NULL, 0), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex(NULL, NULL, 0), SWL_RC_INVALID_PARAM);
    ttb_assert_int_eq(swl_str_ngetMismatchIndex(NULL, "abc", 0), SWL_RC_INVALID_PARAM);
}


static void test_swl_str_matchesIgnoreCase(void** state) {
    (void) state;
    assert_true(swl_str_matchesIgnoreCase(NULL, NULL));
    assert_true(swl_str_matchesIgnoreCase("", ""));
    assert_true(swl_str_matchesIgnoreCase("abc", "Abc"));
    assert_true(swl_str_matchesIgnoreCase("\nTest\r\t", "\ntest\r\t"));

    assert_false(swl_str_matchesIgnoreCase("Abc", NULL));
    assert_false(swl_str_matchesIgnoreCase(NULL, "Abc"));
    assert_false(swl_str_matchesIgnoreCase("\ntest\r", "\nTest\r\t"));
    assert_false(swl_str_matchesIgnoreCase("cba", "Abc"));
    assert_false(swl_str_matchesIgnoreCase("Abc", "abcd"));
    assert_false(swl_str_matchesIgnoreCase("abcd", "Abc"));
    assert_false(swl_str_matchesIgnoreCase("Abc", "abcd"));
    assert_false(swl_str_matchesIgnoreCase("Abc", ""));
    assert_false(swl_str_matchesIgnoreCase("", "Abc"));
}

static void test_swl_str_nmatchesIgnoreCase(void** state) {
    (void) state;
    assert_true(swl_str_nmatchesIgnoreCase(NULL, NULL, 10));
    assert_true(swl_str_nmatchesIgnoreCase("", "", 10));
    assert_true(swl_str_nmatchesIgnoreCase("abc", "ABC0123456", 3));
    assert_true(swl_str_nmatchesIgnoreCase("\nTest\r\t", "\ntest\r\t", 10));

    assert_true(swl_str_nmatchesIgnoreCase("abc", "def", 0));
    assert_true(swl_str_nmatchesIgnoreCase("aBc123", "abcdef", 3));

    assert_false(swl_str_nmatchesIgnoreCase("Abc", NULL, 10));
    assert_false(swl_str_nmatchesIgnoreCase(NULL, "Abc", 10));
    assert_false(swl_str_nmatchesIgnoreCase("\nTest\r", "\ntest\r\t", 10));
    assert_false(swl_str_nmatchesIgnoreCase("cba", "Abc", 10));
    assert_false(swl_str_nmatchesIgnoreCase("Abc", "abcd", 10));
    assert_false(swl_str_nmatchesIgnoreCase("abcd", "Abc", 10));
    assert_false(swl_str_nmatchesIgnoreCase("abc", "Abcd", 10));
    assert_false(swl_str_nmatchesIgnoreCase("Abc", "", 10));
    assert_false(swl_str_nmatchesIgnoreCase("", "Abc", 10));
}


static void testAddEscapeChar(char* base, uint32_t bufSize, char* toEscape, char escape, bool outcome, char* result) {
    char buffer[bufSize];
    swl_str_copy(buffer, bufSize, base);
    bool success = swl_str_addEscapeChar(buffer, bufSize, toEscape, escape);
    assert_int_equal(success, outcome);
    assert_string_equal(buffer, result);
}

static void test_swl_str_addEscapeChar(void** state _UNUSED) {
    testAddEscapeChar("abc,abc", 10, ",\\", '\\', true, "abc\\,abc");
    testAddEscapeChar("aaa", 10, "ab", 'b', true, "bababa");
    testAddEscapeChar("ab=c,cd=e,100\\20=33\\40", 40, "=,\\", '\\', true, "ab\\=c\\,cd\\=e\\,100\\\\20\\=33\\\\40");
    testAddEscapeChar("ab=c,cd=e,100\\20=33\\40", 25, "=,\\", '\\', false, "ab=c,cd=e,100\\20=33\\40");
    testAddEscapeChar("abc,abc", 10, ":\\", '\\', true, "abc,abc");
    testAddEscapeChar("abc,abc", 10, ",", '\\', false, "abc,abc");

    assert_int_equal(false, swl_str_addEscapeChar("abc,abc", 0, ",\\", '\\'));
}

static void testAddEscapeCharPrint(char* base, size_t bufSize, size_t curSize, char* toEscape, char escape, ssize_t outcome, char* result) {
    char buffer[bufSize];
    swl_str_copy(buffer, bufSize, base);
    ssize_t outSize = swl_str_addEscapeCharPrint(buffer, bufSize, curSize, toEscape, escape);
    assert_int_equal(outSize, outcome);
    assert_string_equal(buffer, result);
}

static void test_swl_str_addEscapeCharPrint(void** state _UNUSED) {
    testAddEscapeCharPrint("abc,abc", 10, 7, ",\\", '\\', 8, "abc\\,abc");
    testAddEscapeCharPrint("aaa", 10, 3, "ab", 'b', 6, "bababa");
    testAddEscapeCharPrint("ab=c,cd=e,100\\20=33\\40", 40, 0, "=,\\", '\\', 29, "ab\\=c\\,cd\\=e\\,100\\\\20\\=33\\\\40");
    testAddEscapeCharPrint("ab=c,cd=e,100\\20=33\\40", 25, 0, "=,\\", '\\', 29, "ab\\=c\\,cd\\=e\\,100\\\\20...");
    testAddEscapeCharPrint("abc,abc", 10, 7, ":\\", '\\', 7, "abc,abc");
    testAddEscapeCharPrint("abc,abc", 10, 7, ",", '\\', SWL_RC_INVALID_PARAM, "abc,abc");
    testAddEscapeCharPrint("abc,abc", 10, 7, NULL, '\\', SWL_RC_INVALID_PARAM, "abc,abc");

    assert_int_equal(0, swl_str_addEscapeCharPrint("abc,abc", 0, 0, ",\\", '\\'));

}

static void testRemoveEscapeChar(char* base, uint32_t bufSize, char* toEscape, char escape, bool outcome, char* result) {
    char buffer[bufSize];
    swl_str_copy(buffer, bufSize, base);
    bool success = swl_str_removeEscapeChar(buffer, bufSize, toEscape, escape);
    assert_int_equal(success, outcome);
    assert_string_equal(buffer, result);
}

static void test_swl_str_removeEscapeChar(void** state _UNUSED) {
    testRemoveEscapeChar("abbbc", 10, "bc", 'b', true, "abc");
    testRemoveEscapeChar("abbbc", 10, "c", 'b', false, "abbbc");
    testRemoveEscapeChar("acbbbc", 10, "bc", 'b', false, "acbbbc");
    testRemoveEscapeChar("ab\\=c\\,cd\\=e\\,100\\\\20\\=33\\\\40", 40, "=,\\", '\\', true, "ab=c,cd=e,100\\20=33\\40");
    testRemoveEscapeChar("abc\\,abc", 10, ",\\", '\\', true, "abc,abc");
    testRemoveEscapeChar("abc\\,abc", 10, NULL, '\\', false, "abc\\,abc");
}

static void testRemoveWhitespace(char* base, char* result) {
    size_t len = strlen(base);
    char buffer[len + 1];
    snprintf(buffer, len + 1, "%s", base);
    swl_str_removeWhitespaceEdges(buffer);
    assert_string_equal(buffer, result);
}

static void test_swl_str_removeWhitespace(void** state _UNUSED) {
    testRemoveWhitespace("abbbc", "abbbc");
    testRemoveWhitespace("foo \nbar", "foo \nbar");
    testRemoveWhitespace("foobar", "foobar");
    testRemoveWhitespace("\nfoobar ", "foobar");
    testRemoveWhitespace(" foobar\n", "foobar");
    testRemoveWhitespace(" foobar", "foobar");
    testRemoveWhitespace("\n   \t   foobar   \t  \n ", "foobar");
    testRemoveWhitespace("\n   \t   foo \n\t bar   \t  \n ", "foo \n\t bar");
    testRemoveWhitespace(" \n \t \t \n ", "");
    testRemoveWhitespace(" ", "");
}

static void testStripChar(char* base, char toStrip, bool success, char* result) {
    size_t len = strlen(base);
    char buffer[len + 1];
    snprintf(buffer, len + 1, "%s", base);
    assert_int_equal(success, swl_str_stripChar(buffer, toStrip));

    assert_string_equal(buffer, result);
}

static void test_swl_str_stripChar(void** state _UNUSED) {
    testStripChar("\"abbbc\"", '\"', true, "abbbc");
    testStripChar("abbbc\"", '\"', false, "abbbc\"");
    testStripChar("\"abbbc", '\"', false, "\"abbbc");
    testStripChar("abbba", 'a', true, "bbb");
    testStripChar("\"\"abbbc\"\"", '\"', true, "\"abbbc\"");
    testStripChar(" abc ", ' ', true, "abc");
    testStripChar("\nabc\n", '\n', true, "abc");
}


typedef struct {
    char* key;
    int32_t val;
} myTestData;

#define DATASET_SIZE 5

static myTestData dataSet[DATASET_SIZE] = {
    {.key = "foo=bar", .val = 3},
    {.key = "foo\\=t=bar", .val = 6},
    {.key = "abra", .val = 4},
    {.key = "", .val = 0},
    {.key = "foo\\=\\\\\\==\\=\\bar", .val = 9},
};

static void test_swl_str_getNonEscCharLoc(void** state _UNUSED) {
    for(int i = 0; i < DATASET_SIZE; i++) {
        assert_int_equal(swl_str_getNonEscCharLoc(dataSet[i].key, '=', '\\'), dataSet[i].val);
    }
}

static void testHelp_replace(bool expectedSuccess, char* expectedStr, uint32_t bufSize, char* srcStr, char* old, char* new) {
    char buffer[bufSize];
    memset(buffer, 0, bufSize);
    bool success = swl_str_replace(buffer, bufSize, srcStr, old, new);
    assert_int_equal(success, expectedSuccess);
    if(success) {
        assert_string_equal(expectedStr, buffer);
    }

    size_t testBufSize = SWL_MAX(strlen(srcStr) + 1, bufSize);
    char testBuf[testBufSize];
    swl_str_copy(testBuf, testBufSize, srcStr);
    success = swl_str_replace(testBuf, testBufSize, NULL, old, new);
    assert_int_equal(success, expectedSuccess);
    if(success) {
        assert_string_equal(expectedStr, testBuf);
    }
}

static void test_swl_str_replace(void** state _UNUSED) {
    testHelp_replace(true, "footest", 128, "foobar", "bar", "test");
    testHelp_replace(true, "footesttest_abc_test_abc", 128, "foobarbar_abc_bar_abc", "bar", "test");
    testHelp_replace(true, "testatest", 128, "rararar", "rar", "test");
    testHelp_replace(true, "aa", 128, "arararar", "rar", "");
    testHelp_replace(true, "abcdef", 128, "abcdef", "xyz", "gogo");
    testHelp_replace(true, "zbczbc", 128, "abcabc", "a", "z");
    //fail testing
    testHelp_replace(false, "", 9, "rararar", "rar", "test");
    testHelp_replace(false, "", 128, "rararar", "", "test");

    // case: replacing with itself
    testHelp_replace(true, "abcxyzdef", 128, "abcxyzdef", "xyz", "xyz");
    // case: replacing empty string with empty string
    testHelp_replace(false, "abcxyzdef", 128, "abcxyzdef", "", "");
    // case: replacing empty string with nonempty string
    testHelp_replace(false, "abcxyzdef", 128, "abcxyzdef", "", "a");
    // case: replacee is bigger than source
    testHelp_replace(true, "something very very long", 128, "xyz", "xyz", "something very very long");
    // case: first replacement causes new possible replacement to the right
    testHelp_replace(true, "xyz", 128, "xyzz", "xyz", "xy");
    // case: first replacement causes new possible replacement to the left
    testHelp_replace(true, "xyz", 128, "xxyz", "xyz", "yz");
    // case: replacement causes new possible replacement left&right
    testHelp_replace(true, "xyz", 128, "xxyzz", "xyz", "y");
    // case: replacement when strlen(targetStr) = strlen(srcStr) and targetSize = strlen("targetStr") + 1
    testHelp_replace(true, "2_4GHz", strlen("2_4GHz") + 1, "2.4GHz", ".", "_");
    // case: replacement when strlen(targetStr) < strlen(srcStr) and targetSize = strlen("targetStr") + 1
    testHelp_replace(true, "xyz", strlen("xyz") + 1, "xxyzz", "xyz", "y");
    // case: replacement when strlen(targetStr) > strlen(srcStr) and targetSize = strlen("targetStr") + 1
    testHelp_replace(true, "testatest", strlen("testatest") + 1, "rararar", "rar", "test");
    // case: replacement when strlen(targetStr) > strlen(srcStr) and targetSize = strlen("srcStr") + 1
    testHelp_replace(false, "testatest", strlen("rararar") + 1, "rararar", "rar", "test");
}

static void test_swl_str_printableByteSequenceLen(void** state) {
    (void) state;
    struct {
        bool withSpaces;
        uint32_t seqLen;
        int32_t startId;
        size_t dataLen;
        char data[16];
    } testSeqs[] = {
        {.withSpaces = 0, .seqLen = 4, .startId = 0, .dataLen = 5, .data = {'a', 'b', 'c', 'd', '\0'}, },
        {.withSpaces = 0, .seqLen = 5, .startId = 0, .dataLen = 5, .data = {'a', 'b', 'c', 'd', 'e'}, },
        {.withSpaces = 0, .seqLen = 1, .startId = 1, .dataLen = 7, .data = {'a', 'b', ' ', '\t', '\f', '\v', '\n', '\r'}, },
        {.withSpaces = 1, .seqLen = 7, .startId = 1, .dataLen = 7, .data = {'a', 'b', ' ', '\t', '\f', '\v', '\n', '\r'}, },
        {.withSpaces = 1, .seqLen = 5, .startId = 3, .dataLen = 6, .data = {'a', 'b', ' ', '\t', '\f', '\v', '\n', '\r', '\0'}, },
        {.withSpaces = 0, .seqLen = 1, .startId = 1, .dataLen = 8, .data = {'a', 'b', ' ', '\t', '\f', '\v', '\n', '\r', 0xff}, },
        {.withSpaces = 1, .seqLen = 0, .startId = 1, .dataLen = 8, .data = {'a', 'b', ' ', '\t', '\f', '\v', '\n', '\r', 0xff}, },
        {.withSpaces = 0, .seqLen = 0, .startId = 1, .dataLen = 6, .data = {0xff, 0xfe, 'a', 'b', ':', 'd', '\0'}, },
        {.withSpaces = 0, .seqLen = 4, .startId = 2, .dataLen = 5, .data = {0xff, 0xfe, 'a', 'b', ':', 'd', '\0'}, },
        {.withSpaces = 0, .seqLen = 4, .startId = 2, .dataLen = 4, .data = {0xff, 0xfe, 'a', 'b', ':', 'd'}, },
        {.withSpaces = 0, .seqLen = 0, .startId = 2, .dataLen = 5, .data = {0xff, 0xfe, 'a', 'b', ':', 'd', 0xcd}, },
        {.withSpaces = 0, .seqLen = 4, .startId = 2, .dataLen = 6, .data = {0xff, 0xfe, 'a', 'b', ':', 'd', 0x00, 0xcd}, },
    };
    uint32_t ret;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        ret = swl_str_printableByteSequenceLen(&testSeqs[i].data[testSeqs[i].startId], testSeqs[i].dataLen, testSeqs[i].withSpaces);
        assert_int_equal(ret, testSeqs[i].seqLen);
    }
}

static void test_swl_str_nrCharOccurances(void** state) {
    (void) state;
    struct {
        char byteCheck;
        int32_t seqLen;
        int32_t startId;
        bool continuous;
        size_t dataLen;
        char data[16];
    } testSeqs[] = {
        {.byteCheck = 0x00, .seqLen = 0, .startId = 0, .continuous = true, .dataLen = 7, .data = {0xff, 0xfe, 0xfe, 'b', ':', 'd', 0xfe}, },
        {.byteCheck = 0x00, .seqLen = 0, .startId = 0, .continuous = false, .dataLen = 7, .data = {0xff, 0xfe, 0xfe, 'b', ':', 'd', 0xfe}, },
        {.byteCheck = 0xfe, .seqLen = 2, .startId = 0, .continuous = true, .dataLen = 7, .data = {0xff, 0xfe, 0xfe, 'b', ':', 'd', 0xfe}, },
        {.byteCheck = 0xfe, .seqLen = 3, .startId = 0, .continuous = false, .dataLen = 7, .data = {0xff, 0xfe, 0xfe, 'b', ':', 'd', 0xfe}, },
    };
    int32_t ret;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        ret = swl_str_nrCharOccurances(testSeqs[i].byteCheck, &testSeqs[i].data[testSeqs[i].startId], testSeqs[i].dataLen, testSeqs[i].continuous);
        assert_int_equal(ret, testSeqs[i].seqLen);
    }
}

static void test_swl_str_dumpByteBuffer(void** state) {
    (void) state;
    struct {
        int32_t minPrintable;
        bool showSpaces;
        size_t dataLen;
        union {
            uint32_t num;
            char data[16];
        };
        char result[DUMP_BUF_SIZE(16)];
    } testSeqs[] = {
        {.minPrintable = 1, .showSpaces = 0, .dataLen = sizeof(int), .num = htonl(0x200a), .result = {"/00/00/20/0a"}, },
        {.minPrintable = 3, .showSpaces = 0, .dataLen = 9, .data = {'1', '2', '3', '4', '\0', 1, 2, 3, 4}, .result = {"1234/00/01/02/03/04"}, },
        {.minPrintable = 5, .showSpaces = 0, .dataLen = 9, .data = {'1', '2', '3', '4', '\0', 1, 2, 3, 4}, .result = {"/31/32/33/34/00/01/02/03/04"}, },
        {.minPrintable = 3, .showSpaces = 0, .dataLen = 9, .data = {'1', '2', '3', '4', 1, 1, 2, 3, 4}, .result = {"/31/32/33/34/01/01/02/03/04"}, },
        {.minPrintable = 3, .showSpaces = 0, .dataLen = 7, .data = {1, 2, '3', '4', 0, 3, 4}, .result = {"/01/02/33/34/00/03/04"}, },
        {.minPrintable = 4, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '3', '4', '5', '6', 0, 3, 4}, .result = {"/01/02//3456/00/03/04"}, },
        {.minPrintable = -1, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '3', '4', '5', '6', 0, 3, 4}, .result = {"/01/02/33/34/35/36/00/03/04"}, },
        {.minPrintable = 0, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '3', '4', '5', '6', 1, 3, 4}, .result = {"/01/02//3456/01/03/04"}, },
        {.minPrintable = 1, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '3', '4', '5', '6', 1, 3, 4}, .result = {"/01/02/33/34/35/36/01/03/04"}, },
        {.minPrintable = 4, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '3', ' ', '5', '6', 0, 3, 4}, .result = {"/01/02/33/20/35/36/00/03/04"}, },
        {.minPrintable = 4, .showSpaces = 1, .dataLen = 9, .data = {1, 2, '/', '#', ' ', '\\', 0, 3, 4}, .result = {"/01/02//\\/\\# \\\\/00/03/04"}, },
        {.minPrintable = 2, .showSpaces = 0, .dataLen = 9, .data = {1, 2, '/', '#', ' ', '\\', 0, 3, 4}, .result = {"/01/02//\\/\\#/20/5c/00/03/04"}, },
        {.minPrintable = 3, .showSpaces = 0, .dataLen = 11, .data = {'1', '2', '3', '4', 0, 0, 1, 2, 0, 0, 0xff}, .result = {"1234/00/00/01/02/00/00/ff"}, },
        {.minPrintable = 3, .showSpaces = 0, .dataLen = 12, .data = {'1', '2', '3', '4', 0, 0, 0, 1, 2, 0, 0, 0xff}, .result = {"1234/00#3/01/02/00/00/ff"}, },
        {.minPrintable = 0, .showSpaces = 0, .dataLen = 12, .data = {'1', '2', '3', '4', 0, 0, 0, 0, 0, 0, '5', 0xff}, .result = {"1234/00#6//5/ff"}, },
        {.minPrintable = -1, .showSpaces = 0, .dataLen = 12, .data = {'1', '2', '3', '4', 0, 0, 0, 0, 0, 0, '5', 0xff}, .result = {"/31/32/33/34/00#6/35/ff"}, },
        {.minPrintable = 1, .showSpaces = 0, .dataLen = 12, .data = {'1', '2', '3', '4', 0, 0, 0, 0, 0, 0, '5', 0xff}, .result = {"1234/00#6/35/ff"}, },
        {.minPrintable = 1, .showSpaces = 0, .dataLen = 12, .data = {'1', '2', '3', '4', 0xff, 0xff, 0, 0, 0, 0, 0, '5'}, .result = {"/31/32/33/34/ff/ff/00#5//5"}, },
    };
    int32_t ret;
    const char* dump_overflowStr = "...";
    size_t overflowStr_size = strlen(dump_overflowStr) + 1;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        char dumpBuf[32];
        ret = swl_str_dumpByteBuffer(dumpBuf, sizeof(dumpBuf), testSeqs[i].data, testSeqs[i].dataLen, testSeqs[i].minPrintable, testSeqs[i].showSpaces, dump_overflowStr);
        if(ret) {
            assert_string_equal(dumpBuf, testSeqs[i].result);
        } else {
            assert_memory_equal(dumpBuf, testSeqs[i].result, sizeof(dumpBuf) - overflowStr_size);
            assert_string_equal(&dumpBuf[sizeof(dumpBuf) - overflowStr_size], dump_overflowStr);
        }
    }
}

static void test_swl_str_nlen(void** state _UNUSED) {
    struct {
        char data[5];
        size_t size;
        size_t result;
    } testSeqs[] = {
        {{"abc"}, 4, 3},
        {{""}, 5, 0},
        {{"abc"}, 0, 0},
        {{"abc"}, 1, 1},
        {{"abc"}, 2, 2},
        {{"abc"}, 3, 3},
        {.data = {'a', 'b', 'c', 'd', 'e'}, 0, 0},
        {.data = {'a', 'b', 'c', 'd', 'e'}, 3, 3},
        {.data = {'a', 'b', 'c', 'd', 'e'}, 5, 5},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        assert_int_equal(testSeqs[i].result, swl_str_nlen(testSeqs[i].data, testSeqs[i].size));
    }
    assert_int_equal(0, swl_str_nlen(NULL, 10));
}

static void test_swl_str_len(void** state _UNUSED) {
    struct {
        char data[5];
        size_t result;
    } testSeqs[] = {
        {{"abc"}, 3},
        {{""}, 0},
        {{"a"}, 1},
        {{"ab"}, 2},
        {{"a c"}, 3},
        {{"abc"}, 3},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        assert_int_equal(testSeqs[i].result, swl_str_len(testSeqs[i].data));
    }
    assert_int_equal(0, swl_str_len(NULL));
}


static void swl_str_copyMalloc_copyNullNoFreeNeeded(void** state _UNUSED) {
    // GIVEN nullpointer
    char* tgtStr = NULL;

    // WHEN copying nullpointer over nullpointer
    bool ok = swl_str_copyMalloc(&tgtStr, NULL);

    // THEN target nullpointer stays
    assert_true(ok);
    assert_null(tgtStr);
}

static void swl_str_copyMalloc_copyNullFreeNeeded(void** state _UNUSED) {
    // GIVEN string pointer pointing to allocated string
    char* tgtStr = strdup("hi");

    // WHEN copying nullpointer over allocated string
    bool ok = swl_str_copyMalloc(&tgtStr, NULL);

    // THEN string pointer is null and old string is freed (valgrind reports no memory leaks)
    assert_true(ok);
    assert_null(tgtStr);
}

static void swl_str_copyMalloc_freeNeeded(void** state _UNUSED) {
    // GIVEN pointer to allocated string
    char* tgtStr = strdup("hi");
    const char* src = "hello";

    // WHEN copying another string over it
    bool ok = swl_str_copyMalloc(&tgtStr, src);

    // THEN pointer points to same contents
    assert_true(ok);
    assert_non_null(tgtStr);
    assert_string_equal("hello", tgtStr);
    // AND THEN pointer points to new memory
    assert_ptr_not_equal(tgtStr, src);

    free(tgtStr);
}

static void swl_str_copyMalloc_noOp(void** state _UNUSED) {
    // GIVEN pointer to allocated string
    char* tgtStr = strdup("test123");
    char* tgtStrPtrCopy = tgtStr;

    // WHEN copying itself over itself
    bool ok = swl_str_copyMalloc(&tgtStr, tgtStr);

    // THEN pointer keeps pointing to the same
    assert_true(ok);
    assert_ptr_equal(tgtStrPtrCopy, tgtStr);
    // AND THEN nothing was overwritten either
    assert_string_equal("test123", tgtStr);

    free(tgtStr);
}

static void swl_str_copyMalloc_tgtIsNull(void** state _UNUSED) {
    // GIVEN nothing

    // WHEN copying something to the nullpointer
    bool ok = swl_str_copyMalloc(NULL, "hi");

    // THEN that was refused
    assert_false(ok);
}

static void swl_str_copyMalloc_tgtPointsToNull(void** state _UNUSED) {
    // GIVEN pointer to null-string
    char* tgtStr = NULL;
    const char* src = "hi";
    const char* srcCopy = src;

    // WHEN copying something over that null-string
    bool ok = swl_str_copyMalloc(&tgtStr, src);

    // THEN the string is copied (not just the pointer)
    assert_true(ok);
    assert_non_null(tgtStr);
    assert_string_equal(tgtStr, "hi");
    assert_ptr_not_equal(tgtStr, srcCopy);

    free(tgtStr);
}

static void swl_str_safeAssign_nullTarget(void** state _UNUSED) {
    // GIVEN a nullpointer.
    const char** nullPtr = NULL;

    // WHEN safe-assigning to it
    swl_str_safeAssign(nullPtr, "hi");

    // THEN it did not crash.
}

static void swl_str_safeAssign_allNull(void** state _UNUSED) {
    // GIVEN a nullpointer.
    const char** nullPtr = NULL;

    // WHEN safe-assigning NULL to it
    swl_str_safeAssign(nullPtr, NULL);

    // THEN it did not crash.
}

static void swl_str_safeAssign_normalCase(void** state _UNUSED) {
    // GIVEN a pointer.
    const char* targetText = "hi";
    const char** ptr = &targetText;

    // WHEN safe-assigning text to it
    const char* sourceText = "hello";
    swl_str_safeAssign(ptr, sourceText);

    // THEN the assignment happened.
    assert_ptr_equal(targetText, sourceText);
}

static void swl_str_safeAssign_assignNull(void** state _UNUSED) {
    // GIVEN a pointer.
    const char* targetText = "hi";
    const char** ptr = &targetText;

    // WHEN safe-assigning NULL to it
    swl_str_safeAssign(ptr, NULL);

    // THEN the assignment happened.
    assert_ptr_equal(targetText, NULL);
}

static void swl_str_safeAssign_assignItself(void** state _UNUSED) {
    // GIVEN a pointer.
    const char* targetText = "hi";
    const char** ptr = &targetText;

    // WHEN safe-assigning itself
    const char* targetTextCopy = targetText;
    swl_str_safeAssign(ptr, targetText);

    // THEN nothing happened
    assert_ptr_equal(targetText, targetTextCopy);
}

static void s_paramValTest(char* buffer, size_t resSize, char* param, char* expected, bool found, bool res) {
    char result[resSize + 1];
    char* paramPos = NULL;
    memset(result, 0, resSize + 1);
    bool inRes = swl_str_getParameterValue(result, resSize, buffer, param, &paramPos);
    assert_int_equal(inRes, res);
    if(paramPos) {
        bool paramIsFound = swl_str_nmatches(paramPos, param, strlen(param));
        assert_int_equal(paramIsFound, found);
    }
    assert_string_equal(result, expected);


    memset(result, 0, resSize + 1);
    inRes = swl_str_getParameterValue(result, resSize, buffer, param, NULL);
    assert_int_equal(inRes, res);
    assert_string_equal(result, expected);
}

static void test_swl_str_getParameterValue(void** state) {
    (void) state;
    char buffer[64] = {0};

    snprintf(buffer, sizeof(buffer), "state=ENABLED\nphy\nfreq=2462\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "", false, false);

    // Test for a param who doesn't have a value
    s_paramValTest(buffer, 16, "phy", "", true, false);
    s_paramValTest(buffer, 16, "state", "ENABLED", true, true);
    s_paramValTest(buffer, strlen("ENABLED") + 1, "state", "ENABLED", true, true);
    s_paramValTest(buffer, strlen("ENABLED"), "state", "ENABLE", true, false);
    s_paramValTest(buffer, 16, "freq", "2462", true, true);
    s_paramValTest(buffer, 16, "num_sta_non_erp", "0", true, true);

    // Test for a truncated param in the src buffer
    snprintf(buffer, sizeof(buffer), "abcdef=2462\nstate=ENABLED\nphy\nnum_sta_non_erp=0");

    s_paramValTest(buffer, 16, "abc", "", false, false);

    snprintf(buffer, sizeof(buffer), "state=ENABLED\nphy\nabcdef=2462\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "", false, false);

    snprintf(buffer, sizeof(buffer), "state=ENABLED\nphy\nnum_sta_non_erp=0\nabcdef=2462");
    s_paramValTest(buffer, 16, "abc", "", false, false);

    snprintf(buffer, sizeof(buffer), "state=ENABLED\nphy\ndefabc=2462\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "", false, false);

    snprintf(buffer, sizeof(buffer), "state=ENABLED\nphy\nnum_sta_non_erp=0\ndefabc=2462");
    s_paramValTest(buffer, 16, "abc", "", false, false);

    snprintf(buffer, sizeof(buffer), "defabc=2462\nstate=ENABLED\nphy\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "", false, false);


    snprintf(buffer, sizeof(buffer), "abc=2462\nstate=ENABLED\ndefabc=2500\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "2462", true, true);


    snprintf(buffer, sizeof(buffer), "abc=2462\nstate=ENABLED\nabcdef=2500\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "2462", true, true);

    snprintf(buffer, sizeof(buffer), "defabc=2462\nstate=ENABLED\nabc=2500\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "2500", true, true);

    snprintf(buffer, sizeof(buffer), "abcdef=2462\nstate=ENABLED\nabc=2700\nnum_sta_non_erp=0");
    s_paramValTest(buffer, 16, "abc", "2700", true, true);

    snprintf(buffer, sizeof(buffer), "abcdef\nabc\nbcde");
    s_paramValTest(buffer, 16, "abc", "", true, false);
    s_paramValTest(buffer, 16, "abcdef", "", true, false);
    s_paramValTest(buffer, 16, "bcde", "", true, false);
    s_paramValTest(buffer, 16, "def", "", false, false);
    s_paramValTest(buffer, 16, "cd", "", false, false);


    s_paramValTest(buffer, 0, "abc", "", true, false);
    s_paramValTest(buffer, 0, "abcdef", "", true, false);
    s_paramValTest(buffer, 0, "bcde", "", true, false);
    s_paramValTest(buffer, 0, "def", "", false, false);
    s_paramValTest(buffer, 0, "cd", "", false, false);

    s_paramValTest(NULL, 16, "cd", "", false, false);
    s_paramValTest(buffer, 16, NULL, "", false, false);
    s_paramValTest(NULL, 0, NULL, "", false, false);
    return;
}

static void s_findTest(char* haystack, char* needle, ssize_t result) {
    ssize_t val = swl_str_find(haystack, needle);
    assert_int_equal(val, result);
}

static void test_swl_str_find(void** state _UNUSED) {
    s_findTest("bla", "bla", 0);
    s_findTest("bla", "la", 1);
    s_findTest("bla", "a", 2);
    s_findTest("bla", "b", 0);
    s_findTest("bla", "l", 1);
    s_findTest("bla", "bbla", -1);
    s_findTest("bla", "c", -1);
    s_findTest(NULL, "c", -2);
    s_findTest("bla", NULL, -2);
    s_findTest(NULL, NULL, -2);
}

static void s_findNoEscapeTest(char* haystack, char* needle, char escape, ssize_t result) {
    ssize_t val = swl_str_findNoEscape(haystack, needle, escape);
    assert_int_equal(val, result);
}

static void test_swl_str_findNoEscape(void** state _UNUSED) {
    s_findNoEscapeTest("bla", "bla", '\\', 0);
    s_findNoEscapeTest("bla", "la", '\\', 1);
    s_findNoEscapeTest("bla", "a", '\\', 2);
    s_findNoEscapeTest("bla", "b", '\\', 0);
    s_findNoEscapeTest("bla", "l", '\\', 1);
    s_findNoEscapeTest("bla", "bbla", '\\', -1);
    s_findNoEscapeTest("bla", "c", '\\', -1);
    s_findNoEscapeTest(NULL, "c", '\\', -2);
    s_findNoEscapeTest("bla", NULL, '\\', -2);
    s_findNoEscapeTest(NULL, NULL, '\\', -2);
    s_findNoEscapeTest("bl\\abbba", "a", '\\', 7);
    s_findNoEscapeTest("bl\\\\abbba", "a", '\\', 4);
}

static void s_isOnlyWhitespace(const char* str, size_t len, bool expectedValue) {
    bool ret = swl_str_isOnlyWhitespace(str, len);
    assert_int_equal(ret, expectedValue);
}

static void test_swl_str_isOnlyWhitespace(void** state _UNUSED) {
    s_isOnlyWhitespace("", 10, true);
    s_isOnlyWhitespace("       ", 10, true);
    s_isOnlyWhitespace("    def", 4, true);
    s_isOnlyWhitespace(" \t\n", 4, true);
    s_isOnlyWhitespace("abc", 0, true);
    s_isOnlyWhitespace(" \t\n\r\v\f\t\t\n\r\v\f\t", 50, true);

    s_isOnlyWhitespace("abc", 3, false);
    s_isOnlyWhitespace(" abc", 3, false);
    s_isOnlyWhitespace(" a ", 3, false);
    s_isOnlyWhitespace("a  ", 3, false);
    s_isOnlyWhitespace("  a", 3, false);
    s_isOnlyWhitespace(" \t\n\r\v\f\t\t\n\rr\v\f\t", 50, false); // extra r after /r
}

void s_testStr(char* str, char* sep, ssize_t index, bool isNull, bool ignoreEmptySpace, char* result) {
    char* test = swl_strlst_copyStringAtIndex(str, sep, index, ignoreEmptySpace);
    if(isNull) {
        assert_null(test);
    } else {
        assert_string_equal(test, result);
        free(test);
    }
}

static void test_swl_strlst_copyStringAtIndex(void** state _UNUSED) {
    s_testStr("WiFi.Radio", ".", 0, false, true, "WiFi");
    s_testStr("WiFi.Radio", ".", 1, false, true, "Radio");
    s_testStr("WiFi.Radio", ",", 1, true, true, NULL);
    s_testStr("WiFi.Radio.", ",", 1, true, true, NULL);
    s_testStr("WiFi.Radio", ".", -1, false, true, "Radio");
    s_testStr("WiFi.Radio.wifi0", ".", -1, false, true, "wifi0");
    s_testStr("WiFi.Radio.wifi0", ".", 1, false, true, "Radio");
    s_testStr("WiFi.Radio.wifi0", ".", 2, false, true, "wifi0");
    s_testStr("WiFi.Radio..Channel.", ".", -1, false, true, "Channel");
    s_testStr("WiFi.Radio..Channel.", ".", -1, false, false, "");
    s_testStr("WiFi.Radio.wifi0", ".", 10, true, true, NULL);
    s_testStr("WiFi.Radio..Channel.", ".", 2, false, true, "Channel");
    s_testStr("WiFi.Radio..Channel.", ".", 2, false, false, "");
    s_testStr("...", ".", -1, false, false, "");
    s_testStr("...", ".", 1, false, false, "");
    s_testStr("...", ".", 2, false, false, "");
    s_testStr("...", ".", 3, false, false, "");
}

static void s_testStrBuffer(char* data, size_t* dataLen, char* str, char* sep, ssize_t index, bool ignoreEmptySpace, char* result) {
    swl_strlst_copyStringAtIndexToBuffer(data, dataLen, str, sep, index, ignoreEmptySpace);
    assert_string_equal(data, result);
}

static void test_swl_strlst_copyStringAtIndexToBuffer(void** state _UNUSED) {
    char data[32] = {'\0'};
    size_t dataLen = sizeof(data);
    s_testStrBuffer(data, &dataLen, "WiFi.Radio", ".", 0, true, "WiFi");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio", ".", 1, true, "Radio");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio", ",", 1, true, "");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio.", ",", 1, true, "");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio", ".", -1, true, "Radio");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio.wifi0", ".", -1, true, "wifi0");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio.wifi0", ".", 1, true, "Radio");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio.wifi0", ".", 2, true, "wifi0");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio..Channel.", ".", -1, true, "Channel");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio..Channel.", ".", -1, false, "");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio.wifi0", ".", 10, true, "");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio..Channel.", ".", 2, true, "Channel");
    s_testStrBuffer(data, &dataLen, "WiFi.Radio..Channel.", ".", 2, false, "");
    s_testStrBuffer(data, &dataLen, "...", ".", -1, false, "");
    s_testStrBuffer(data, &dataLen, "...", ".", 1, false, "");
    s_testStrBuffer(data, &dataLen, "...", ".", 2, false, "");
    s_testStrBuffer(data, &dataLen, "...", ".", 3, false, "");
}

static void s_nrStrDiff(const char* str1, const char* str2, size_t len, size_t expectedNrDiff) {
    size_t nrDiff = swl_str_nrStrDiff(str1, str2, len);
    assert_int_equal(nrDiff, expectedNrDiff);
}

static void test_swl_str_nrStrDiff(void** state _UNUSED) {
    s_nrStrDiff("", "", 0, 0);
    s_nrStrDiff("", "", -1, 0);
    s_nrStrDiff("abc", "", -1, 3);
    s_nrStrDiff("", "ab", -1, 2);
    s_nrStrDiff("abc", "", 0, 0);
    s_nrStrDiff("abc", "", 2, 2);
    s_nrStrDiff("abcd", "dcb", -1, 4);
    s_nrStrDiff("abcd", "dcb", 3, 3);
    s_nrStrDiff("abcde", "adcbe", 5, 2);
    s_nrStrDiff("abcde", "0bcde", 5, 1);
    s_nrStrDiff("abcde", "edcba", 5, 4);
    s_nrStrDiff("abcde", "abcdef", 5, 0);
    s_nrStrDiff("abcde", "abcdef", -1, 1);
    s_nrStrDiff("02:10:18:04:30:03", "02:10:18:05:30:04", -1, 2);
}

static void s_remWhiteAndNew(const char* in, const char* out, size_t result) {
    char* test = NULL;
    if(in != NULL) {
        char str[32] = {'\0'};
        snprintf(str, sizeof(str), "%s", in);
        test = str;
    }
    size_t size = swl_str_removeWhitespace(test);
    assert_int_equal(size, result);
    if((test != NULL) && (out != NULL)) {
        assert_string_equal(test, out);
    }
}

static void test_swl_str_whiteSpaceNewLine(void** state _UNUSED) {
    s_remWhiteAndNew("AA BB CC \n", "AABBCC", 6);
    s_remWhiteAndNew(" AA\nBB\nCC\n a b  c  ", "AABBCCabc", 9);
    s_remWhiteAndNew(" AA\nBB\nCC\n a b  c \r ", "AABBCCabc", 9);
    s_remWhiteAndNew("\n\n\n\n\n\n\n a b  c \r ", "abc", 3);
    s_remWhiteAndNew("   a    b   ", "ab", 2);
    s_remWhiteAndNew(NULL, "", 0);
    s_remWhiteAndNew("", NULL, 0);
}

static int setup_suite(void** state) {
    (void) state;
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlStr");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_str_toLower),
        cmocka_unit_test(test_swl_str_toUpper),
        cmocka_unit_test(test_swl_str_copy),
        cmocka_unit_test(test_swl_str_cat),
        cmocka_unit_test(test_swl_str_catFormat),
        cmocka_unit_test(test_swl_strlst_cat),
        cmocka_unit_test(test_swl_strlst_catFormat),
        cmocka_unit_test(test_swl_strlst_contains),
        cmocka_unit_test(test_swl_str_countChar),
        cmocka_unit_test(test_swl_str_startsWith),
        cmocka_unit_test(test_swl_str_matches),
        cmocka_unit_test(test_swl_str_nmatches),
        cmocka_unit_test(test_swl_str_toLower),
        cmocka_unit_test(test_swl_str_toUpper),
        cmocka_unit_test(test_swl_str_copy),
        cmocka_unit_test(test_swl_str_cat),
        cmocka_unit_test(test_swl_str_catChar),
        cmocka_unit_test(test_swl_str_catFormat),
        cmocka_unit_test(test_swl_strlst_cat),
        cmocka_unit_test(test_swl_strlst_catFormat),
        cmocka_unit_test(test_swl_str_countChar),
        cmocka_unit_test(test_swl_str_startsWith),
        cmocka_unit_test(test_swl_str_matches),
        cmocka_unit_test(test_swl_str_nmatches),
        cmocka_unit_test(test_swl_str_matchesIgnoreCase),
        cmocka_unit_test(test_swl_str_nmatchesIgnoreCase),
        cmocka_unit_test(test_swl_str_getMismatchIndex),
        cmocka_unit_test(test_swl_str_ngetMismatchIndex),
        cmocka_unit_test(test_swl_str_addEscapeChar),
        cmocka_unit_test(test_swl_str_addEscapeCharPrint),
        cmocka_unit_test(test_swl_str_removeEscapeChar),
        cmocka_unit_test(test_swl_str_removeWhitespace),
        cmocka_unit_test(test_swl_str_stripChar),
        cmocka_unit_test(test_swl_str_getNonEscCharLoc),
        cmocka_unit_test(test_swl_str_replace),
        cmocka_unit_test(test_swl_str_ncopy),
        cmocka_unit_test(test_swl_str_ncat),
        cmocka_unit_test(test_swl_str_printableByteSequenceLen),
        cmocka_unit_test(test_swl_str_nrCharOccurances),
        cmocka_unit_test(test_swl_str_dumpByteBuffer),
        cmocka_unit_test(test_swl_str_nlen),
        cmocka_unit_test(test_swl_str_len),
        cmocka_unit_test(swl_str_copyMalloc_copyNullNoFreeNeeded),
        cmocka_unit_test(swl_str_copyMalloc_copyNullFreeNeeded),
        cmocka_unit_test(swl_str_copyMalloc_freeNeeded),
        cmocka_unit_test(swl_str_copyMalloc_noOp),
        cmocka_unit_test(swl_str_copyMalloc_tgtIsNull),
        cmocka_unit_test(swl_str_copyMalloc_tgtPointsToNull),
        cmocka_unit_test(swl_str_safeAssign_nullTarget),
        cmocka_unit_test(swl_str_safeAssign_allNull),
        cmocka_unit_test(swl_str_safeAssign_normalCase),
        cmocka_unit_test(swl_str_safeAssign_assignNull),
        cmocka_unit_test(swl_str_safeAssign_assignItself),
        cmocka_unit_test(test_swl_str_getParameterValue),
        cmocka_unit_test(test_swl_str_find),
        cmocka_unit_test(test_swl_str_findNoEscape),
        cmocka_unit_test(test_swl_str_isOnlyWhitespace),
        cmocka_unit_test(test_swl_str_nrStrDiff),
        cmocka_unit_test(test_swl_strlst_copyStringAtIndex),
        cmocka_unit_test(test_swl_strlst_copyStringAtIndexToBuffer),
        cmocka_unit_test(test_swl_str_whiteSpaceNewLine),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
