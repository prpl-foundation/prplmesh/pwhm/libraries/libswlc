/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include <time.h>
#include <cmocka.h>
#include <debug/sahtrace.h>
#include <swl/swl_common.h>
#include <swl/swl_common_mac.h>
#include <swl/swl_ieee802_1x_defs.h>
#include "swl/swl_genericFrameParser.h"
#include <swl/swl_hex.h>
#include "test-toolbox/ttb_util.h"

// Create buffer
#define BUFF_SIZE 5120

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

static size_t s_loadFrame(uint8_t* buf, size_t bufSize, const char* binFileName) {
    memset(buf, 0, bufSize);
    FILE* fp = fopen(binFileName, "rb");
    assert_non_null(fp);

    size_t flen = fread(buf, 1, bufSize, fp);
    fclose(fp);
    assert_true(flen <= bufSize);
    return flen;
}

static void s_checkingResults(swl_wirelessDevice_infoElements_t* ieData, swl_wirelessDevice_infoElements_t* ieExpectedData) {

    cmocka_set_message_output(CM_OUTPUT_STDOUT);
    print_message("vendorOUI:                    %d/%d\n",
                  ieData->vendorOUI.count, ieExpectedData->vendorOUI.count);
    for(uint8_t i = 0; i < ieData->vendorOUI.count; i++) {
        print_message("OUI[%d]:                       %x/%x\n", i,
                      SWL_OUI_GET(ieData->vendorOUI.oui[i].ouiBytes),
                      SWL_OUI_GET(ieExpectedData->vendorOUI.oui[i].ouiBytes));
    }
    print_message("capabilities:                 %x/%x\n",
                  ieData->capabilities, ieExpectedData->capabilities);
    print_message("vendorCapabilities:           %x/%x \n",
                  ieData->vendorCapabilities, ieExpectedData->vendorCapabilities);
    print_message("htCapabilities:               %x/%x\n",
                  ieData->htCapabilities, ieExpectedData->htCapabilities);
    print_message("vhtCapabilities:              %x/%x\n",
                  ieData->vhtCapabilities, ieExpectedData->vhtCapabilities);
    print_message("heCapabilities:               %d/%d\n",
                  ieData->heCapabilities, ieExpectedData->heCapabilities);
    print_message("ehtCapabilities:              0x%x/0x%x\n",
                  ieData->ehtCapabilities, ieExpectedData->ehtCapabilities);
    print_message("freqCapabilities:             %d/%d\n",
                  ieData->freqCapabilities, ieExpectedData->freqCapabilities);
    print_message("uniiBandsCapabilities:        0x%x/0x%x\n",
                  ieData->uniiBandsCapabilities, ieExpectedData->uniiBandsCapabilities);
    print_message("operChanSpec:                 b:%d/c:%d/w:%d ==> b:%d/c:%d/w:%d\n",
                  ieData->operChanInfo.band,
                  ieData->operChanInfo.channel,
                  ieData->operChanInfo.bandwidth,
                  ieExpectedData->operChanInfo.band,
                  ieExpectedData->operChanInfo.channel,
                  ieExpectedData->operChanInfo.bandwidth);
    if(!swl_str_isEmpty(ieExpectedData->country)) {
        print_message("country:                      (%s)/(%s)\n",
                      ieData->country, ieExpectedData->country);
        print_message("operClass region:             %d/%d\n",
                      ieData->operClassRegion, ieExpectedData->operClassRegion);
    }
    print_message("secModeEnabled:               %d/%d\n",
                  ieData->secModeEnabled, ieExpectedData->secModeEnabled);
    print_message("operatingStandards:           %d/%d\n",
                  ieData->operatingStandards, ieExpectedData->operatingStandards);
    print_message("WPS_ConfigMethodsEnabled:     %d/%d\n",
                  ieData->WPS_ConfigMethodsEnabled, ieExpectedData->WPS_ConfigMethodsEnabled);
    print_message("maxRxSpatialStreamsSupported: %d/%d\n",
                  ieData->maxRxSpatialStreamsSupported, ieExpectedData->maxRxSpatialStreamsSupported);
    print_message("maxTxSpatialStreamsSupported: %d/%d\n",
                  ieData->maxTxSpatialStreamsSupported, ieExpectedData->maxTxSpatialStreamsSupported);
    print_message("maxDownlinkRateSupported:     %d/%d\n",
                  ieData->maxDownlinkRateSupported, ieExpectedData->maxDownlinkRateSupported);
    print_message("maxUplinkRateSupported:       %d/%d\n",
                  ieData->maxUplinkRateSupported, ieExpectedData->maxUplinkRateSupported);
    print_message("ssidLen:                      %d/%d, (SIZE = %ld)\n",
                  ieData->ssidLen, ieExpectedData->ssidLen, strlen(ieExpectedData->ssid));
    print_message("ssid:                         %s/%s\n",
                  ieData->ssid, ieExpectedData->ssid);
    print_message("RRM Caps:                     %x/%x\n",
                  ieData->rrmCapabilities, ieExpectedData->rrmCapabilities);
    print_message("MLD Address:                  %s/%s\n",
                  swl_typeMacBin_toBuf32Ref(&ieData->ehtMldMacAddress).buf,
                  swl_typeMacBin_toBuf32Ref(&ieExpectedData->ehtMldMacAddress).buf);
    print_message("EML support:                  %x/%x\n",
                  ieData->ehtEmlCapabilities, ieExpectedData->ehtEmlCapabilities);
    for(uint8_t i = 0; (i < SWL_ARRAY_SIZE(ieData->ehtLinksMacAddress)); i++) {
        if(((1 << i) & ieData->ehtLinksMask) == 0) {
            continue;
        }
        print_message("Link[%d]:                      %s/%s\n",
                      i, swl_typeMacBin_toBuf32Ref(&ieData->ehtLinksMacAddress[i]).buf,
                      swl_typeMacBin_toBuf32Ref(&ieExpectedData->ehtLinksMacAddress[i]).buf);
    }
    print_message("==============================\n");

    // testing
    assert_int_equal((int) ieData->vendorOUI.count, (int) ieExpectedData->vendorOUI.count);
    for(uint8_t i = 0; i < ieData->vendorOUI.count; i++) {
        assert_int_equal(SWL_OUI_GET(ieData->vendorOUI.oui[i].ouiBytes), SWL_OUI_GET(ieExpectedData->vendorOUI.oui[i].ouiBytes));
    }
    assert_int_equal((int) ieData->capabilities, (int) ieExpectedData->capabilities);
    assert_int_equal((int) ieData->vendorCapabilities, (int) ieExpectedData->vendorCapabilities);
    assert_int_equal((int) ieData->htCapabilities, (int) ieExpectedData->htCapabilities);
    assert_int_equal((int) ieData->vhtCapabilities, (int) ieExpectedData->vhtCapabilities);
    assert_int_equal((int) ieData->heCapabilities, (int) ieExpectedData->heCapabilities);
    assert_int_equal((int) ieData->ehtCapabilities, (int) ieExpectedData->ehtCapabilities);
    assert_int_equal((int) ieData->rrmCapabilities, (int) ieExpectedData->rrmCapabilities);
    assert_int_equal((int) ieData->freqCapabilities, (int) ieExpectedData->freqCapabilities);
    assert_int_equal((int) ieData->uniiBandsCapabilities, (int) ieExpectedData->uniiBandsCapabilities);
    assert_int_equal((int) ieData->operChanInfo.band, (int) ieExpectedData->operChanInfo.band);
    assert_int_equal((int) ieData->operChanInfo.bandwidth, (int) ieExpectedData->operChanInfo.bandwidth);
    assert_int_equal((int) ieData->operChanInfo.channel, (int) ieExpectedData->operChanInfo.channel);
    if(!swl_str_isEmpty(ieExpectedData->country)) {
        assert_string_equal(ieData->country, ieExpectedData->country);
        assert_int_equal((int) ieData->operClassRegion, (int) ieExpectedData->operClassRegion);
    }
    assert_int_equal((int) ieData->ssidLen, (int) ieExpectedData->ssidLen);
    assert_string_equal(ieData->ssid, ieExpectedData->ssid);
    assert_int_equal((int) ieData->secModeEnabled, (int) ieExpectedData->secModeEnabled);
    assert_int_equal((int) ieData->operatingStandards, (int) ieExpectedData->operatingStandards);
    assert_int_equal((int) ieData->WPS_ConfigMethodsEnabled, (int) ieExpectedData->WPS_ConfigMethodsEnabled);
    assert_int_equal((int) ieData->maxRxSpatialStreamsSupported, (int) ieExpectedData->maxRxSpatialStreamsSupported);
    assert_int_equal((int) ieData->maxTxSpatialStreamsSupported, (int) ieExpectedData->maxTxSpatialStreamsSupported);
    assert_int_equal((int) ieData->maxDownlinkRateSupported, (int) ieExpectedData->maxDownlinkRateSupported);
    assert_int_equal((int) ieData->maxUplinkRateSupported, (int) ieExpectedData->maxUplinkRateSupported);
    assert_true(SWL_MAC_BIN_MATCHES(&ieData->ehtMldMacAddress, &ieExpectedData->ehtMldMacAddress));
    assert_int_equal((int) ieData->ehtEmlCapabilities, (int) ieExpectedData->ehtEmlCapabilities);
    for(uint8_t i = 0; (i < SWL_ARRAY_SIZE(ieData->ehtLinksMacAddress)); i++) {
        assert_true(SWL_MAC_BIN_MATCHES(&ieData->ehtLinksMacAddress[i], &ieExpectedData->ehtLinksMacAddress[i]));
    }
}

struct {
    const char* iesFileName;
    const char* frameFileName;
    swl_80211_mgmtFrame_t expectedHeader;
    swl_wirelessDevice_infoElements_t expectedIEs;
} rawIEsInfo[] = {
    {
        .iesFileName = "probRespIE2GHz",
        .frameFileName = "probRespFrame2GHz.bin",
        .expectedHeader = {
            .fc = {.subType = (SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE >> 4), },
            .transmitter = {{0xd8, 0xfb, 0x5e, 0x5d, 0x1e, 0xe4}},
            .destination = {{0x48, 0x8d, 0x36, 0xd5, 0xdf, 0x94}},
            .bssid = {{0xd8, 0xfb, 0x5e, 0x5d, 0x1e, 0xe4}},
        },
        .expectedIEs = {
            .vendorOUI.count = 3,
            .vendorOUI.oui = { { {0x00, 0x50, 0xf2}},     // OUI Microsoft Corp. (there is two of this one in the frame, but the 2nd will not be taken into acount because of: swl_typeOui_arrayContains())
                { {0x00, 0x90, 0x4c}},                    // OUI Epigram, Inc.
                { {0x00, 0x10, 0x18}} },                  // OUI Broadcom.

            .capabilities = M_SWL_STACAP_RRM | M_SWL_STACAP_BTM,
            .vendorCapabilities = M_SWL_STACAP_VENDOR_MS_WPS,
            .htCapabilities = (M_SWL_STACAP_HT_40MHZ | M_SWL_STACAP_HT_SGI20 | M_SWL_STACAP_HT_SGI40),       // extracted from 0x19e3
            .vhtCapabilities = (M_SWL_STACAP_VHT_SGI80 | M_SWL_STACAP_VHT_SU_BFR | M_SWL_STACAP_VHT_SU_BFE), // extracted from 0x0f8379b0
            .heCapabilities = 0,                                                                             // not supported in this frame
            .rrmCapabilities = (M_SWL_STACAP_RRM_LINK_ME | M_SWL_STACAP_RRM_NEIGHBOR_RE | M_SWL_STACAP_RRM_BEACON_ACTIVE_ME |
                                M_SWL_STACAP_RRM_BEACON_PASSIVE_ME | M_SWL_STACAP_RRM_FRAME_ME | M_SWL_STACAP_RRM_CHAN_LOAD_ME |
                                M_SWL_STACAP_RRM_NOISE_HISTOGRAM_ME | M_SWL_STACAP_RRM_STATS_ME | M_SWL_STACAP_RRM_TRANS_SC_ME),
            .rrmOnChannelMaxDuration = 0,
            .rrmOffChannelMaxDuration = 0,
            .freqCapabilities = M_SWL_FREQ_BAND_EXT_5GHZ,                                                    // deduced from VHT Operation info in this frame
            .operChanInfo = {
                .channel = 104,
                .bandwidth = SWL_BW_80MHZ,
                .band = SWL_FREQ_BAND_EXT_5GHZ,
            },
            .country = "DE",
            .operClassRegion = SWL_OP_CLASS_COUNTRY_GLOBAL,
            .maxRxSpatialStreamsSupported = 4,
            .maxTxSpatialStreamsSupported = 4,
            .maxDownlinkRateSupported = 1733333,
            .maxUplinkRateSupported = 1733333,

            .ssidLen = 23,
            .ssid = "Internet-Box-51595_5GHz",
            .secModeEnabled = SWL_SECURITY_APMODE_WPA2_P,
            .operatingStandards = (M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC),
            .WPS_ConfigMethodsEnabled = (M_SWL_WPS_CFG_MTHD_ETH | M_SWL_WPS_CFG_MTHD_LABEL | M_SWL_WPS_CFG_MTHD_PBC | M_SWL_WPS_CFG_MTHD_PBC_V | M_SWL_WPS_CFG_MTHD_PBC_P), // extracted from 0x0686
        }
    },
    {
        .iesFileName = "probRespIE5GHz",
        .frameFileName = "probRespFrame5GHz.bin",
        .expectedHeader = {
            .fc = {.subType = (SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE >> 4), },
            .transmitter = {{0x98, 0x42, 0x65, 0x2d, 0x23, 0x40}},
            .destination = {{0x02, 0xaf, 0xc6, 0x91, 0x51, 0xc4}},
            .bssid = {{0x98, 0x42, 0x65, 0x2d, 0x23, 0x40}},
        },
        .expectedIEs = {
            .vendorOUI.count = 4,
            .vendorOUI.oui = { { {0x00, 0x26, 0x86}},  // OUI Quantina.
                { {0x00, 0x50, 0xf2}},                 // OUI Microsoft Corp. (there is two of this one in the frame, but the 2nd will not be taken into acount because of: swl_typeOui_arrayContains())
                { {0x00, 0x90, 0x4c}},                 // OUI Epigram, Inc.
                { {0x00, 0x10, 0x18}} },               // OUI Broadcom.

            .capabilities = M_SWL_STACAP_RRM | M_SWL_STACAP_BTM,
            .vendorCapabilities = M_SWL_STACAP_VENDOR_MS_WPS,
            .htCapabilities = (M_SWL_STACAP_HT_40MHZ | M_SWL_STACAP_HT_SGI20 | M_SWL_STACAP_HT_SGI40),                                 // extracted from 0x01ef
            .vhtCapabilities = (M_SWL_STACAP_VHT_SGI80 | M_SWL_STACAP_VHT_SU_BFR | M_SWL_STACAP_VHT_SU_BFE | M_SWL_STACAP_VHT_MU_BFR), // extracted from 0x0f8b79b6
            .heCapabilities = (M_SWL_STACAP_HE_SU_BFR | M_SWL_STACAP_HE_SU_AND_MU_BFE | M_SWL_STACAP_HE_MU_BFR),                       // extracted from 0x6fc0
            .rrmCapabilities = (M_SWL_STACAP_RRM_LINK_ME | M_SWL_STACAP_RRM_NEIGHBOR_RE | M_SWL_STACAP_RRM_BEACON_ACTIVE_ME |
                                M_SWL_STACAP_RRM_BEACON_PASSIVE_ME | M_SWL_STACAP_RRM_FRAME_ME | M_SWL_STACAP_RRM_CHAN_LOAD_ME |
                                M_SWL_STACAP_RRM_NOISE_HISTOGRAM_ME | M_SWL_STACAP_RRM_STATS_ME | M_SWL_STACAP_RRM_TRANS_SC_ME),
            .rrmOnChannelMaxDuration = 0,
            .rrmOffChannelMaxDuration = 0,
            .freqCapabilities = M_SWL_FREQ_BAND_EXT_5GHZ,
            .operChanInfo = {
                .channel = 100,
                .bandwidth = SWL_BW_80MHZ,
                .band = SWL_FREQ_BAND_EXT_5GHZ,
            },
            .country = "FR",
            .operClassRegion = SWL_OP_CLASS_COUNTRY_GLOBAL,
            .uniiBandsCapabilities = M_SWL_BAND_UNII_ALL_5G,
            .maxRxSpatialStreamsSupported = 4,
            .maxTxSpatialStreamsSupported = 4,
            .maxDownlinkRateSupported = 4803897,
            .maxUplinkRateSupported = 4803897,

            .ssidLen = 12,
            .ssid = "Livebox-2340",
            .secModeEnabled = SWL_SECURITY_APMODE_WPA2_P,
            .operatingStandards = (M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX),
            .WPS_ConfigMethodsEnabled = 0,  // Registrar Config Methods not supported in this frame
        }
    },
    {
        .frameFileName = "beaconFrameEHT.bin",
        .iesFileName = "beaconEHT.bin",
        .expectedHeader = {
            .fc = {.subType = (SWL_80211_MGT_FRAME_TYPE_BEACON >> 4), },
            .transmitter = {{0x02, 0x00, 0x00, 0x7e, 0x1c, 0x68}},
            .destination = {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
            .bssid = {{0x02, 0x00, 0x00, 0x7e, 0x1c, 0x68}},
        },
        .expectedIEs = {
            .vendorOUI.count = 1,
            .vendorOUI.oui = {
                { {0x00, 0x50, 0xf2} }
            },
            .capabilities = M_SWL_STACAP_RRM,
            .vendorCapabilities = M_SWL_STACAP_VENDOR_MS_WPS,
            .htCapabilities = (M_SWL_STACAP_HT_40MHZ | M_SWL_STACAP_HT_SGI20 | M_SWL_STACAP_HT_SGI40),
            .vhtCapabilities = (M_SWL_STACAP_VHT_SGI80),
            .heCapabilities = 0,
            .ehtCapabilities = (M_SWL_STACAP_EHT_SU_BEAMFORMEE | M_SWL_STACAP_EHT_SU_BEAMFORMER |
                                M_SWL_STACAP_EHT_MU_BEAMFORMER_80MHZ | M_SWL_STACAP_EHT_MU_BEAMFORMER_160MHZ),
            .rrmCapabilities = (M_SWL_STACAP_RRM_NEIGHBOR_RE | M_SWL_STACAP_RRM_BEACON_ACTIVE_ME |
                                M_SWL_STACAP_RRM_BEACON_PASSIVE_ME | M_SWL_STACAP_RRM_BEACON_TABLE_ME),
            .rrmOnChannelMaxDuration = 0,
            .rrmOffChannelMaxDuration = 0,
            .freqCapabilities = M_SWL_FREQ_BAND_EXT_5GHZ,
            .operChanInfo = {
                .channel = 36,
                .bandwidth = SWL_BW_80MHZ,
                .band = SWL_FREQ_BAND_EXT_5GHZ,
            },
            .country = "DE",
            .operClassRegion = SWL_OP_CLASS_COUNTRY_EU,
            .uniiBandsCapabilities = M_SWL_BAND_UNII_ALL_5G,
            .maxRxSpatialStreamsSupported = 2,
            .maxTxSpatialStreamsSupported = 2,
            .maxDownlinkRateSupported = 1200955,
            .maxUplinkRateSupported = 1200955,

            .ssidLen = 6,
            .ssid = "prplOS",
            .secModeEnabled = SWL_SECURITY_APMODE_WPA2_P,
            .operatingStandards = (M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE),
            .WPS_ConfigMethodsEnabled = 0,
            .ehtMldMacAddress = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x02}},
        }
    },
    {
        .frameFileName = "ehtAssocReq.bin",
        .iesFileName = "assocMlo.bin",
        .expectedHeader = {
            .fc = {.subType = (SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST >> 4), },
            .transmitter = {{0xea, 0x5b, 0xb5, 0x76, 0xfa, 0x1d}},
            .destination = {{0x5a, 0x96, 0x71, 0xee, 0x2e, 0x98}},
            .bssid = {{0x5a, 0x96, 0x71, 0xee, 0x2e, 0x98}},
        },
        .expectedIEs = {
            .vendorOUI.count = 4,
            .vendorOUI.oui = {
                { {0x50, 0x6f, 0x9a} },
                { {0x00, 0x90, 0x4c} },
                { {0x00, 0x10, 0x18} },
                { {0x00, 0x50, 0xf2} },
            },
            .capabilities = M_SWL_STACAP_RRM | M_SWL_STACAP_PMF,
            .vendorCapabilities = 0,
            .htCapabilities = (M_SWL_STACAP_HT_40MHZ | M_SWL_STACAP_HT_SGI20 | M_SWL_STACAP_HT_SGI40),
            .vhtCapabilities = (M_SWL_STACAP_VHT_SGI80 | M_SWL_STACAP_VHT_SGI160 | M_SWL_STACAP_VHT_SU_BFE | M_SWL_STACAP_VHT_MU_BFE),
            .heCapabilities = M_SWL_STACAP_HE_SU_AND_MU_BFE,
            .ehtCapabilities = (M_SWL_STACAP_EHT_SU_BEAMFORMEE),
            .rrmCapabilities = (M_SWL_STACAP_RRM_BEACON_ACTIVE_ME | M_SWL_STACAP_RRM_LINK_ME | M_SWL_STACAP_RRM_NEIGHBOR_RE |
                                M_SWL_STACAP_RRM_BEACON_PASSIVE_ME | M_SWL_STACAP_RRM_BEACON_TABLE_ME |
                                M_SWL_STACAP_RRM_STATS_ME | M_SWL_STACAP_RRM_AP_CHAN_RE),
            .rrmOnChannelMaxDuration = 0,
            .rrmOffChannelMaxDuration = 0,
            .freqCapabilities = M_SWL_FREQ_BAND_EXT_2_4GHZ | M_SWL_FREQ_BAND_EXT_5GHZ | M_SWL_FREQ_BAND_EXT_6GHZ,
            .operChanInfo = {
                .channel = 0,
                .bandwidth = SWL_BW_80MHZ,
                .band = SWL_FREQ_BAND_EXT_5GHZ,
            },
            .country = "",
            .operClassRegion = SWL_OP_CLASS_COUNTRY_EU,
            .uniiBandsCapabilities = M_SWL_BAND_UNII_ALL_6G | M_SWL_BAND_UNII_ALL_5G,
            .maxRxSpatialStreamsSupported = 2,
            .maxTxSpatialStreamsSupported = 2,
            .maxDownlinkRateSupported = 2401911,
            .maxUplinkRateSupported = 2401911,

            .ssidLen = 3,
            .ssid = "bg7",
            .secModeEnabled = SWL_SECURITY_APMODE_WPA3_P,
            .operatingStandards = (M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE),
            .WPS_ConfigMethodsEnabled = 0,
            .ehtMldMacAddress = {{0xea, 0x5b, 0xb5, 0x76, 0xfa, 0x1c}},
            .ehtLinksMask = 0x2,
            .ehtLinksMacAddress = {
                [1] = {{0xea, 0x5b, 0xb5, 0x76, 0xfa, 0x1e}},
            },
        }
    },
    {
        .frameFileName = "mlFrmAssocReq.bin",
        .iesFileName = "mlIEAssocReq.bin",
        .expectedHeader = {
            .fc = {.subType = (SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST >> 4), },

            .transmitter = {{0x4a, 0xe4, 0x95, 0x8e, 0x53, 0x45}},
            .destination = {{0x58, 0x96, 0x71, 0xee, 0x2c, 0x99}},
            .bssid = {{0x58, 0x96, 0x71, 0xee, 0x2c, 0x99}},
        },
        .expectedIEs = {
            .vendorOUI.count = 1,
            .vendorOUI.oui = {
                { {0x00, 0x50, 0xf2} },
            },
            .capabilities = M_SWL_STACAP_RRM | M_SWL_STACAP_BTM | M_SWL_STACAP_QOS_MAP | M_SWL_STACAP_PMF,
            .vendorCapabilities = 0,
            .htCapabilities = 0,
            .vhtCapabilities = 0,
            .heCapabilities = M_SWL_STACAP_HE_SU_AND_MU_BFE,
            .ehtCapabilities = (M_SWL_STACAP_EHT_320MHZ_6GHZ | M_SWL_STACAP_EHT_SU_BEAMFORMEE),
            .rrmCapabilities = (M_SWL_STACAP_RRM_LINK_ME | M_SWL_STACAP_RRM_BEACON_ACTIVE_ME |
                                M_SWL_STACAP_RRM_BEACON_PASSIVE_ME | M_SWL_STACAP_RRM_BEACON_TABLE_ME),
            .rrmOnChannelMaxDuration = 0,
            .rrmOffChannelMaxDuration = 0,
            .freqCapabilities = M_SWL_FREQ_BAND_EXT_2_4GHZ | M_SWL_FREQ_BAND_EXT_5GHZ | M_SWL_FREQ_BAND_EXT_6GHZ,
            .operChanInfo = {
                .channel = 0,
                .bandwidth = SWL_BW_20MHZ,
                .band = SWL_FREQ_BAND_EXT_6GHZ,
            },
            .country = "",
            .operClassRegion = SWL_OP_CLASS_COUNTRY_EU,
            .uniiBandsCapabilities = M_SWL_BAND_UNII_ALL_6G | M_SWL_BAND_UNII_ALL_5G,
            .maxRxSpatialStreamsSupported = 2,
            .maxTxSpatialStreamsSupported = 2,
            .maxDownlinkRateSupported = 2401911,
            .maxUplinkRateSupported = 2401911,

            .ssidLen = 3,
            .ssid = "bg7",
            .secModeEnabled = SWL_SECURITY_APMODE_WPA3_P,
            .operatingStandards = (M_SWL_RADSTD_AX | M_SWL_RADSTD_BE),
            .WPS_ConfigMethodsEnabled = 0,
            .ehtMldMacAddress = {{0x90, 0x09, 0xdf, 0xb5, 0xa7, 0x43}},
            .ehtEmlCapabilities = M_SWL_STACAP_EHT_EML_EMLSR,
            .ehtLinksMask = 0x6,
            .ehtLinksMacAddress = {
                [1] = {{0x9e, 0xb5, 0x52, 0xd7, 0x44, 0x13}},
                [2] = {{0x42, 0xf9, 0xed, 0x7c, 0x35, 0x21}},
            },
        }
    },
};

static void test_parseMultiIEsInRawBuffer(void** state _UNUSED) {
    swl_wirelessDevice_infoElements_t results;
    uint8_t binData[BUFF_SIZE] = {0};
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(rawIEsInfo); i++) {
        memset(&results, 0, sizeof(swl_wirelessDevice_infoElements_t));
        size_t binLen = s_loadFrame(binData, sizeof(binData), rawIEsInfo[i].iesFileName);
        ssize_t parsedLen = swl_80211_parseInfoElementsBuffer(&results, NULL, binLen, binData);
        assert_int_equal(parsedLen, binLen);
        s_checkingResults(&results, &rawIEsInfo[i].expectedIEs);
    }
}

static void test_parseMultiIEsInMgmtFrame(void** state _UNUSED) {
    swl_wirelessDevice_infoElements_t results;
    uint8_t binData[BUFF_SIZE] = {0};
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(rawIEsInfo); i++) {
        memset(&results, 0, sizeof(swl_wirelessDevice_infoElements_t));
        size_t binLen = s_loadFrame(binData, sizeof(binData), rawIEsInfo[i].frameFileName);
        swl_80211_mgmtFrame_t* frame = swl_80211_getMgmtFrame(binData, binLen);
        assert_non_null(frame);
        size_t iesLen = 0;
        swl_bit8_t* iesData = swl_80211_getInfoElementsOfMgmtFrame(&iesLen, binData, binLen);
        assert_non_null(iesData);
        assert_int_equal(swl_80211_parseInfoElementsBuffer(&results, NULL, iesLen, iesData), iesLen);
        assert_int_equal(frame->fc.subType, rawIEsInfo[i].expectedHeader.fc.subType);
        assert_memory_equal(&frame->transmitter, &rawIEsInfo[i].expectedHeader.transmitter, SWL_MAC_BIN_LEN);
        assert_memory_equal(&frame->destination, &rawIEsInfo[i].expectedHeader.destination, SWL_MAC_BIN_LEN);
        assert_memory_equal(&frame->bssid, &rawIEsInfo[i].expectedHeader.bssid, SWL_MAC_BIN_LEN);
        s_checkingResults(&results, &rawIEsInfo[i].expectedIEs);
    }
}

#define IE_MAX_SIZE 255
static void test_parseOneIE(void** state _UNUSED) {
    struct {
        uint8_t ieData[IE_MAX_SIZE];
        swl_wirelessDevice_infoElements_t expectedIEs;
    } oneIEInfo[] = {
        //parse IE SSID
        {
            .ieData = {SWL_80211_EL_ID_SSID, strlen("testSSID"), 't', 'e', 's', 't', 'S', 'S', 'I', 'D'},
            .expectedIEs = {
                .ssidLen = strlen("testSSID"),
                .ssid = {"testSSID"},
            },
        },
        //parse IE VHT Operation Info
        {
            /* center sg0 106, center sg1 114 */
            .ieData = {SWL_80211_EL_ID_VHT_OP, 5, SWL_80211_VHT_OPER_CHAN_WIDTH_80_160_8080, 106, 114, 0x00, 0x00},
            .expectedIEs = {
                .operChanInfo.bandwidth = SWL_BW_160MHZ,
                .operatingStandards = M_SWL_RADSTD_AC,
            },
        },
        //parse IE HT Operation Info
        {
            /* primary chan 11, 0x8: no secd channel, bw only 20MHz.*/
            .ieData = {SWL_80211_EL_ID_HT_OP, 22, 11, 0x8, 0x00, 0x11, /* remaining zeros */ },
            .expectedIEs = {
                .operChanInfo = {.channel = 11, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ, },
                .freqCapabilities = M_SWL_FREQ_BAND_EXT_2_4GHZ,
                .operatingStandards = M_SWL_RADSTD_B | M_SWL_RADSTD_N,
            },
        },
        //parse IE Supported operating classes
        {
            /* Current operating class 84, alternates: 81, 83, 84 */
            .ieData = {SWL_80211_EL_ID_SUP_OP_CLASS, 4, 84, 81, 83, 84, },
            .expectedIEs = {
                .operChanInfo = {.band = SWL_FREQ_BAND_EXT_2_4GHZ, .bandwidth = SWL_BW_40MHZ, },
                .freqCapabilities = M_SWL_FREQ_BAND_EXT_2_4GHZ,
                .operatingStandards = M_SWL_RADSTD_B, /* Legacy */
                .uniiBandsCapabilities = 0,
            },
        },
        //parse IE Supported operating classes
        {
            /* Current operating class 128, no alternates: 0 */
            .ieData = {SWL_80211_EL_ID_SUP_OP_CLASS, 2, 128, 0, },
            .expectedIEs = {
                .operChanInfo = {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_80MHZ, },
                .freqCapabilities = M_SWL_FREQ_BAND_EXT_5GHZ,
                .operatingStandards = M_SWL_RADSTD_A, /* Legacy */
                .uniiBandsCapabilities = M_SWL_BAND_UNII_ALL_5G,
            },
        },
        //parse MBO Vendor Specific IE
        {
            .ieData = {SWL_80211_EL_ID_VENDOR_SPECIFIC, 9, 0x50, 0x6f, 0x9a, SWL_OUI_TYPE_MBO, 0x02, 0x00, 0x03, 0x01, 0x02, },
            .expectedIEs = {
                .vendorOUI.count = 1,
                .vendorOUI.oui = {{{0x50, 0x6f, 0x9a}}},
                .vendorCapabilities = M_SWL_STACAP_VENDOR_WFA_MBO,
            },
        },
        //parse IE Supported Rates 1(B), 2(B), 5.5(B), 11(B), [Mbit/sec]
        {
            .ieData = {SWL_80211_EL_ID_SUP_RATES, 0x04, 0x82, 0x84, 0x8b, 0x96, },
            .expectedIEs = {
                .operatingStandards = M_SWL_RADSTD_B,
            },
        },
        //parse IE Extended Supported Rates 24, 36, 48, 54, [Mbit/sec]
        {
            .ieData = {SWL_80211_EL_ID_EXT_CAP_RATE_BSS_MEMB, 0x32, 0x04, 0x30, 0x48, 0x60, 0x6c, },
            .expectedIEs = {
                .operatingStandards = M_SWL_RADSTD_G,
            },
        },
    };
    swl_wirelessDevice_infoElements_t results;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(oneIEInfo); i++) {
        memset(&results, 0, sizeof(swl_wirelessDevice_infoElements_t));
        swl_rc_ne rc = swl_80211_processInfoElementFrame(&results, NULL, oneIEInfo[i].ieData[0], oneIEInfo[i].ieData[1], &oneIEInfo[i].ieData[2]);
        assert_int_equal(rc, SWL_RC_OK);
        s_checkingResults(&results, &oneIEInfo[i].expectedIEs);
    }
}

typedef struct {
    swl_macBin_t src;
    swl_macBin_t dst;
    int16_t mgmtFrameType;
    int16_t wnmPurpose;
    int16_t btmQueryReason;
    int16_t btmRespRetCode;
    int16_t deauthReason;
    int16_t disassocReason;
    size_t iesLen;
    swl_bit8_t* iesData;
    swl_wirelessDevice_infoElements_t wirelessDevIEs;
    swl_oui_t actionVsOUI;
} frameHandlingResult_t;

static void s_mgmtFrameCb(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    pResult->mgmtFrameType = swl_80211_mgtFrameType(&frame->fc);
    pResult->src = frame->transmitter;
    pResult->dst = frame->destination;
    memset(&pResult->wirelessDevIEs, 0, sizeof(pResult->wirelessDevIEs));
    pResult->iesData = swl_80211_getInfoElementsOfMgmtFrame(&pResult->iesLen, (swl_bit8_t*) frame, frameLen);
    if(pResult->iesLen > 0) {
        ssize_t parsedLen = swl_80211_parseInfoElementsBuffer(&pResult->wirelessDevIEs, NULL, pResult->iesLen, (uint8_t*) pResult->iesData);
        assert_int_equal(parsedLen, pResult->iesLen);
    }
}

static void s_assocReqCb(void* userData _UNUSED, swl_80211_mgmtFrame_t* frame, size_t frameLen _UNUSED, swl_80211_assocReqFrameBody_t* assocReq _UNUSED, size_t assocReqDataLen _UNUSED) {
    assert_memory_equal(frame->destination.bMac, frame->bssid.bMac, SWL_MAC_BIN_LEN);
}

static void s_reassocReqCb(void* userData _UNUSED, swl_80211_mgmtFrame_t* frame, size_t frameLen _UNUSED, swl_80211_reassocReqFrameBody_t* reassocReq, size_t reassocReqDataLen _UNUSED) {
    assert_memory_equal(frame->destination.bMac, frame->bssid.bMac, SWL_MAC_BIN_LEN);
    assert_memory_equal(reassocReq->currentAp.bMac, frame->bssid.bMac, SWL_MAC_BIN_LEN);
}

static void s_btmQueryCb(void* userData, swl_80211_mgmtFrame_t* frame _UNUSED, size_t frameLen _UNUSED, swl_80211_wnmActionBTMQueryFrameBody_t* btmQuery, size_t btmQueryDataLen _UNUSED) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    pResult->wnmPurpose = SWL_80211_WNM_ACTION_PURPOSE_BTM_QUERY;
    pResult->btmQueryReason = btmQuery->reason;
}

static void s_btmRespCb(void* userData, swl_80211_mgmtFrame_t* frame _UNUSED, size_t frameLen _UNUSED, swl_80211_wnmActionBTMResponseFrameBody_t* btmResp, size_t btmRespDataLen _UNUSED) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    pResult->wnmPurpose = SWL_80211_WNM_ACTION_PURPOSE_BTM_RESPONSE;
    pResult->btmRespRetCode = btmResp->statusCode;
}

static void s_actionVsCb(void* userData, swl_80211_mgmtFrame_t* frame _UNUSED, size_t frameLen _UNUSED, swl_80211_vendorIdEl_t* vendor, size_t vendorLen _UNUSED) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    memcpy(&pResult->actionVsOUI.ouiBytes, vendor->oui, SWL_OUI_BYTE_LEN);
}

static void s_disassocCb(void* userData, swl_80211_mgmtFrame_t* frame _UNUSED, swl_80211_disassocFrameBody_t* disassocFrame) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    pResult->disassocReason = disassocFrame->reason;
}

static void s_deauthCb(void* userData, swl_80211_mgmtFrame_t* frame _UNUSED, swl_80211_deauthFrameBody_t* deauthFrame) {
    frameHandlingResult_t* pResult = (frameHandlingResult_t*) userData;
    assert_non_null(pResult);
    pResult->deauthReason = deauthFrame->reason;
}

static void test_handleMgmtFrame(void** state _UNUSED) {
    struct {
        const char* fileName;
        swl_rc_ne expectedhandleRc;
        frameHandlingResult_t expectedResult;
    } tests[] = {
        {
            .fileName = "beacon.bin",
            .expectedhandleRc = SWL_RC_OK,
            .expectedResult = {
                .src = {{0x00, 0x90, 0x4c, 0x34, 0x22, 0x02}},
                .dst = {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_BEACON,
                .iesLen = 331,
                .wirelessDevIEs = {
                    .ssid = "OpenWrt-5.0",
                    .maxRxSpatialStreamsSupported = 3,
                    .maxTxSpatialStreamsSupported = 3,
                    .maxDownlinkRateSupported = 1801470,
                    .maxUplinkRateSupported = 1801470,
                    .supportedHtMCS = {
                        .mcsNbr = 16,
                        .mcs = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                    },
                    .supportedVhtMCS = {
                        {
                            .nssNbr = 3,
                            .nssMcsNbr = {9, 9, 9},
                        },
                        {
                            .nssNbr = 3,
                            .nssMcsNbr = {9, 9, 9},
                        },
                    },
                    .supportedHeMCS = {
                        {
                            .nssNbr = 3,
                            .nssMcsNbr = {11, 11, 11},
                        },
                        {
                            .nssNbr = 3,
                            .nssMcsNbr = {11, 11, 11},
                        },
                    },
                    .country = "US",
                    .operClassRegion = SWL_OP_CLASS_COUNTRY_GLOBAL,
                },
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "probRespFrame5GHz.bin",
            .expectedResult = {
                .src = {{0x98, 0x42, 0x65, 0x2d, 0x23, 0x40}},
                .dst = {{0x02, 0xaf, 0xc6, 0x91, 0x51, 0xc4}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE,
                .iesLen = 519,
                .wirelessDevIEs = {
                    .ssid = "Livebox-2340",
                    .maxRxSpatialStreamsSupported = 4,
                    .maxTxSpatialStreamsSupported = 4,
                    .maxDownlinkRateSupported = 4803897,
                    .maxUplinkRateSupported = 4803897,
                    .supportedHtMCS = {
                        .mcsNbr = 16,
                        .mcs = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                    },
                    .supportedVhtMCS = {
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {9, 9, 9, 9},
                        },
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {9, 9, 9, 9},
                        },
                    },
                    .supportedHeMCS = {
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {11, 11, 11, 11},
                        },
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {11, 11, 11, 11},
                        },
                    },
                    .supportedHe160MCS = {
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {11, 11, 11, 11},
                        },
                        {
                            .nssNbr = 4,
                            .nssMcsNbr = {11, 11, 11, 11},
                        },
                    },
                    .modelName = "SagemcomFast5670E_OFR",
                    .manufacturer = "Sagemcom",
                    .modelNumber = "SG_LB6_1.0.2",
                    .country = "FR",
                    .operClassRegion = SWL_OP_CLASS_COUNTRY_GLOBAL,
                },
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "probeReqBroadcast.bin",
            .expectedResult = {
                .src = {{0x52, 0xaf, 0x5c, 0x8a, 0xda, 0x68}},
                .dst = {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST,
                .iesLen = 150,
                .wirelessDevIEs = {
                    .ssid = "\0", /* wildcard */
                    .maxRxSpatialStreamsSupported = 2,
                    .maxTxSpatialStreamsSupported = 2,
                    .maxDownlinkRateSupported = 1200955,
                    .maxUplinkRateSupported = 1200955,
                    .supportedHtMCS = {
                        .mcsNbr = 16,
                        .mcs = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                    },
                    .supportedHeMCS = {
                        {
                            .nssNbr = 2,
                            .nssMcsNbr = {9, 9},
                        },
                        {
                            .nssNbr = 2,
                            .nssMcsNbr = {9, 9},
                        },
                    },
                },
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "reassocReq.bin",
            .expectedResult = {
                .src = {{0x52, 0xaf, 0x5c, 0x8a, 0xda, 0x68}},
                .dst = {{0x00, 0x90, 0x4c, 0x34, 0x23, 0x02}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_REASSOC_REQUEST,
                .iesLen = 199,
                .wirelessDevIEs = {
                    .ssid = "OpenWrt",
                    .maxRxSpatialStreamsSupported = 2,
                    .maxTxSpatialStreamsSupported = 2,
                    .maxDownlinkRateSupported = 1200955,
                    .maxUplinkRateSupported = 1200955,
                    .supportedHtMCS = {
                        .mcsNbr = 16,
                        .mcs = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                    },
                    .supportedHeMCS = {
                        {
                            .nssNbr = 2,
                            .nssMcsNbr = {9, 9},
                        },
                        {
                            .nssNbr = 2,
                            .nssMcsNbr = {9, 9},
                        },
                    },
                },
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "btmQuery.bin",
            .expectedResult = {
                .src = {{0x82, 0xa1, 0xff, 0x8f, 0x0e, 0x5a}},
                .dst = {{0x02, 0x10, 0x18, 0x04, 0x30, 0x03}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_ACTION,
                .iesLen = 0,
                .wnmPurpose = SWL_80211_WNM_ACTION_PURPOSE_BTM_QUERY,
                .btmQueryReason = 6, //Better AP Found
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "btmResp.bin",
            .expectedResult = {
                .src = {{0x82, 0xa1, 0xff, 0x8f, 0x0e, 0x5a}},
                .dst = {{0x02, 0x10, 0x18, 0x04, 0x30, 0x03}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_ACTION,
                .iesLen = 0,
                .wnmPurpose = SWL_80211_WNM_ACTION_PURPOSE_BTM_RESPONSE,
                .btmQueryReason = -1,
                .btmRespRetCode = 1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "requestToSend.bin",
            .expectedhandleRc = SWL_RC_INVALID_PARAM, /* it's a control frame, not a management frame */
        },
        {
            .fileName = "publicActionVs.bin",
            .expectedResult = {
                .src = {{0xac, 0x91, 0x9b, 0x3a, 0x5f, 0x1a}},
                .dst = {{0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_ACTION,
                .iesLen = 0,
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
                .actionVsOUI.ouiBytes = {0xaa, 0xbb, 0xcc},
            }
        },
        {
            .fileName = "actionVs.bin",
            .expectedResult = {
                .src = {{0xac, 0x91, 0x9b, 0x3a, 0x5f, 0x1a}},
                .dst = {{0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_ACTION,
                .iesLen = 0,
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
                .actionVsOUI.ouiBytes = {0xaa, 0xbb, 0xcc},
            }
        },
        {
            .fileName = "probeWPSEnrollee.bin",
            .expectedResult = {
                .src = {{0x2c, 0x8a, 0x72, 0xbb, 0xc4, 0xa5}},
                .dst = {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST,
                .iesLen = 257,
                .wirelessDevIEs = {
                    .ssid = "\0", /* wildcard */
                    .maxRxSpatialStreamsSupported = 1,
                    .maxTxSpatialStreamsSupported = 1,
                    .maxDownlinkRateSupported = 433333,
                    .maxUplinkRateSupported = 433333,
                    .supportedHtMCS = {
                        .mcsNbr = 16,
                        .mcs = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                    },
                    .supportedVhtMCS = {
                        {
                            .nssNbr = 1,
                            .nssMcsNbr = {9},
                        },
                        {
                            .nssNbr = 1,
                            .nssMcsNbr = {9},
                        },
                    },
                    .modelName = "HTC One_M8",
                    .modelNumber = "HTC One_M8",
                    .manufacturer = "HTC",
                    .wpsRequestToEnroll = true,
                    .wpsPasswordID = SWL_WPS_PASSWORD_ID_DEFAULT,
                },
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "disassoc.bin",
            .expectedResult = {
                .src = {{0x46, 0xcf, 0xe7, 0xcc, 0x88, 0xb2}},
                .dst = {{0xac, 0x91, 0x9b, 0x3a, 0x5d, 0xef}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_DISASSOCIATION,
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = SWL_IEEE80211_DEAUTH_REASON_STA_LEFT_BSS,
                .deauthReason = -1,
            }
        },
        {
            .fileName = "deauth.bin",
            .expectedResult = {
                .src = {{0xb0, 0x46, 0x92, 0xbf, 0xec, 0xc5}},
                .dst = {{0xac, 0x91, 0x9b, 0x3a, 0x5d, 0xef}},
                .mgmtFrameType = SWL_80211_MGT_FRAME_TYPE_DEAUTHENTICATION,
                .wnmPurpose = -1,
                .btmQueryReason = -1,
                .btmRespRetCode = -1,
                .disassocReason = -1,
                .deauthReason = SWL_IEEE80211_DEAUTH_REASON_STA_LEFT_IBSS,
            }
        },
    };

    swl_80211_mgmtFrameHandlers_cb handlers = {
        .fProcMgmtFrame = s_mgmtFrameCb,
        .fProcAssocReq = s_assocReqCb,
        .fProcReAssocReq = s_reassocReqCb,
        .fProcBtmQuery = s_btmQueryCb,
        .fProcBtmResp = s_btmRespCb,
        .fProcActionVs = s_actionVsCb,
        .fProcDisassoc = s_disassocCb,
        .fProcDeauth = s_deauthCb,
    };

    frameHandlingResult_t result;
    uint8_t binData[BUFF_SIZE] = {0};
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        memset(&result, 0, sizeof(result));
        result.mgmtFrameType = result.wnmPurpose = result.btmQueryReason = result.btmRespRetCode = result.deauthReason = result.disassocReason = -1;
        printf(">>> parsing:  %s\n", tests[i].fileName);
        size_t binLen = s_loadFrame(binData, sizeof(binData), tests[i].fileName);
        swl_rc_ne rc = swl_80211_handleMgmtFrame(&result, binData, binLen, &handlers);
        assert_int_equal((int) rc, (int) tests[i].expectedhandleRc);
        if(rc == SWL_RC_OK) {
            assert_memory_equal(tests[i].expectedResult.src.bMac, result.src.bMac, SWL_MAC_BIN_LEN);
            assert_memory_equal(tests[i].expectedResult.dst.bMac, result.dst.bMac, SWL_MAC_BIN_LEN);
            assert_int_equal(tests[i].expectedResult.mgmtFrameType, result.mgmtFrameType);
            assert_int_equal(tests[i].expectedResult.iesLen, result.iesLen);
            assert_string_equal(tests[i].expectedResult.wirelessDevIEs.ssid, result.wirelessDevIEs.ssid);
            assert_int_equal(tests[i].expectedResult.wnmPurpose, result.wnmPurpose);
            assert_int_equal(tests[i].expectedResult.btmQueryReason, result.btmQueryReason);
            assert_int_equal(tests[i].expectedResult.btmRespRetCode, result.btmRespRetCode);
            assert_int_equal(tests[i].expectedResult.disassocReason, result.disassocReason);
            assert_int_equal(tests[i].expectedResult.deauthReason, result.deauthReason);
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.maxRxSpatialStreamsSupported, result.wirelessDevIEs.maxRxSpatialStreamsSupported);
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.maxTxSpatialStreamsSupported, result.wirelessDevIEs.maxTxSpatialStreamsSupported);
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.maxDownlinkRateSupported, result.wirelessDevIEs.maxDownlinkRateSupported);
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.maxUplinkRateSupported, result.wirelessDevIEs.maxUplinkRateSupported);
            if(!swl_str_isEmpty(tests[i].expectedResult.wirelessDevIEs.country)) {
                assert_string_equal(tests[i].expectedResult.wirelessDevIEs.country, result.wirelessDevIEs.country);
                assert_int_equal(tests[i].expectedResult.wirelessDevIEs.operClassRegion, result.wirelessDevIEs.operClassRegion);
            }
            assert_memory_equal(&tests[i].expectedResult.wirelessDevIEs.supportedMCS, &result.wirelessDevIEs.supportedMCS, sizeof(result.wirelessDevIEs.supportedMCS));
            assert_memory_equal(&tests[i].expectedResult.wirelessDevIEs.supportedVhtMCS, &result.wirelessDevIEs.supportedVhtMCS, sizeof(result.wirelessDevIEs.supportedVhtMCS));
            assert_memory_equal(&tests[i].expectedResult.wirelessDevIEs.supportedHeMCS, &result.wirelessDevIEs.supportedHeMCS, sizeof(result.wirelessDevIEs.supportedHeMCS));
            assert_memory_equal(&tests[i].expectedResult.wirelessDevIEs.supportedHe160MCS, &result.wirelessDevIEs.supportedHe160MCS, sizeof(result.wirelessDevIEs.supportedHe160MCS));
            assert_memory_equal(&tests[i].expectedResult.wirelessDevIEs.supportedHe80x80MCS, &result.wirelessDevIEs.supportedHe80x80MCS, sizeof(result.wirelessDevIEs.supportedHe80x80MCS));
            assert_memory_equal(&tests[i].expectedResult.actionVsOUI.ouiBytes, &result.actionVsOUI.ouiBytes, sizeof(result.actionVsOUI.ouiBytes));
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.wpsRequestToEnroll, result.wirelessDevIEs.wpsRequestToEnroll);
            assert_string_equal(tests[i].expectedResult.wirelessDevIEs.modelName, result.wirelessDevIEs.modelName);
            assert_string_equal(tests[i].expectedResult.wirelessDevIEs.manufacturer, result.wirelessDevIEs.manufacturer);
            assert_string_equal(tests[i].expectedResult.wirelessDevIEs.modelNumber, result.wirelessDevIEs.modelNumber);
            assert_int_equal(tests[i].expectedResult.wirelessDevIEs.wpsPasswordID, result.wirelessDevIEs.wpsPasswordID);
        }
    }
}



static void s_parseRawBeaconIE(void* pUserData _UNUSED, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId, uint8_t len, uint8_t* frm) {

    switch(elId) {
    case SWL_80211_EL_ID_SSID: {
        // Tested that the SSID is expected
        char ssid[len + 1];
        memset(ssid, 0, len + 1);
        memcpy(ssid, frm, len);
        assert_string_equal(ssid, "OpenWrt-5.0");
        break;
    }
    case SWL_80211_EL_ID_COUNTRY: {
        // Test that the country is expected
        char country[3] = {0};
        memcpy(country, &frm[0], 2);
        assert_string_equal(country, "US");
        break;
    }
    default:
        break;
    }
}


SWL_TABLE(rawBeaconIETable,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR(
              {SWL_80211_EL_ID_SSID, (void*) s_parseRawBeaconIE},
              {SWL_80211_EL_ID_COUNTRY, (void*) s_parseRawBeaconIE}
              )
          );


// Test Sample of Beacon IEs using custom IE table
static void test_handleCustomBeaconIEsParse(void** state _UNUSED) {

    // Load the beacon frame
    char* fileName = "beacon.bin";
    uint8_t binData[BUFF_SIZE] = {0};
    size_t binLen = s_loadFrame(binData, sizeof(binData), fileName);

    swl_80211_mgmtFrame_t* mgmt_frame = swl_80211_getMgmtFrame(binData, binLen);

    swl_80211_beaconFrameBody_t* beaconBody = (swl_80211_beaconFrameBody_t*) mgmt_frame->data;
    size_t beaconIEsLen = binLen - offsetof(swl_80211_beaconFrameBody_t, data) - SWL_80211_MGMT_FRAME_HEADER_LEN;

    char empty; // Not using a user pointer but it can't be NULL
    ssize_t parsedLen = swl_80211_parseCustomInfoElementsBuffer(&empty, &rawBeaconIETable, NULL, beaconIEsLen, beaconBody->data);
    assert_int_equal(parsedLen, beaconIEsLen);
}


typedef struct {
    swl_80211_htCapIE_t htCap;

    uint8_t supported_rates[12];
    uint8_t supported_rates_len;
} rawRessocIEs_t;

static void s_parseRawRessocIE(void* pUserData, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId, uint8_t len, uint8_t* frm) {
    rawRessocIEs_t* pRawDevIE = (rawRessocIEs_t*) pUserData;

    switch(elId) {
    case SWL_80211_EL_ID_HT_CAP:
        memcpy(&pRawDevIE->htCap, (swl_80211_htCapIE_t*) frm, len);
        break;
    case SWL_80211_EL_ID_SUP_RATES:

        pRawDevIE->supported_rates_len = len;
        memcpy(&pRawDevIE->supported_rates, frm, len);     // Copy over raw supported rates data
    default:
        break;
    }
}


SWL_TABLE(rawRessocIEParseTable,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR(
              {SWL_80211_EL_ID_HT_CAP, (void*) s_parseRawRessocIE},
              {SWL_80211_EL_ID_SUP_RATES, (void*) s_parseRawRessocIE},
              )
          );

static void s_rawMgmtFrameCb(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen) {
    assert_non_null(userData);

    size_t iesLen = 0;
    uint8_t* iesData = swl_80211_getInfoElementsOfMgmtFrame(&iesLen, (swl_bit8_t*) frame, frameLen);

    // Parse the IEs using the custom rawRessocIEParseTable parse table
    ssize_t parsedLen = swl_80211_parseCustomInfoElementsBuffer(userData, &rawRessocIEParseTable, NULL, iesLen, iesData);
    assert_int_equal(parsedLen, iesLen);
}


// Test Sample of parsing raw data structures from reassociation request frame using custom IE table
static void test_handleCustomRawMgmtFrameIEsParse(void** state _UNUSED) {

    struct {
        const char* fileName;
        swl_rc_ne expectedhandleRc;
        rawRessocIEs_t expectedResult;
    } tests[] = {
        {
            .fileName = "probRespFrame5GHz.bin",
            .expectedResult = {
                .supported_rates_len = 8,
                .supported_rates = {
                    0x8c, 0x12, 0x18, 0x24, 0xb0, 0x48, 0x60, 0x6c,
                    0x00, 0x00, 0x00, 0x00 // Extras

                },
                .htCap = {
                    .htCapInfo = 0x01ef,
                    .ampduParam = 0x17,
                    .supMCSSet = {
                        0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                    },
                    .htExtCap = 0,
                    .txBfCap = 0,
                    .ASELCap = 0
                }
            }
        },
        {
            .fileName = "probeReqBroadcast.bin",
            .expectedResult = {
                .supported_rates_len = 4,
                .supported_rates = {
                    0x02, 0x04, 0x0b, 0x16,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // Extras

                },
                .htCap = {
                    .htCapInfo = 0x002d,
                    .ampduParam = 0x1b,
                    .supMCSSet = {
                        0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                    },
                    .htExtCap = 0,
                    .txBfCap = 0,
                    .ASELCap = 0
                }
            }
        },
        {
            .fileName = "reassocReq.bin",
            .expectedResult = {
                .supported_rates_len = 8,
                .supported_rates = {
                    0x82, 0x84, 0x8b, 0x96, 0x24, 0x30, 0x48, 0x6c,
                    0x00, 0x00, 0x00, 0x00 // Extras
                },
                .htCap = {
                    .htCapInfo = 0x002d,
                    .ampduParam = 0x1b,
                    .supMCSSet = {
                        0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                    },
                    .htExtCap = 0,
                    .txBfCap = 0,
                    .ASELCap = 0
                }
            }
        },
        {
            .fileName = "requestToSend.bin",
            .expectedhandleRc = SWL_RC_INVALID_PARAM, /* it's a control frame, not a management frame */
        },
        {
            .fileName = "probeWPSEnrollee.bin",
            .expectedResult = {
                .supported_rates_len = 8,
                .supported_rates = {
                    0x0c, 0x12, 0x18, 0x24, 0x30, 0x48, 0x60, 0x6c,
                    0x00, 0x00, 0x00, 0x00 // Extras
                },
                .htCap = {
                    .htCapInfo = 0x016e,
                    .ampduParam = 0x03,
                    .supMCSSet = {
                        0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
                    },
                    .htExtCap = 0,
                    .txBfCap = 0,
                    .ASELCap = 0
                }
            }
        }
    };

    swl_80211_mgmtFrameHandlers_cb handlers = {
        .fProcMgmtFrame = s_rawMgmtFrameCb
    };

    rawRessocIEs_t result;
    memset(&result, 0, sizeof(rawRessocIEs_t));

    uint8_t binData[BUFF_SIZE] = {0};
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        memset(&result, 0, sizeof(result));
        printf(">>> parsing:  %s\n", tests[i].fileName);
        size_t binLen = s_loadFrame(binData, sizeof(binData), tests[i].fileName);
        swl_rc_ne rc = swl_80211_handleMgmtFrame(&result, binData, binLen, &handlers);
        assert_int_equal((int) rc, (int) tests[i].expectedhandleRc);
        if(rc != SWL_RC_OK) {
            continue;
        }

        // Test HT Cap
        swl_80211_htCapIE_t expectedHTCap = tests[i].expectedResult.htCap;

        assert_memory_equal(&result.htCap.htCapInfo, &expectedHTCap.htCapInfo, sizeof(swl_80211_htCapInfo_m));
        assert_int_equal(result.htCap.ampduParam, expectedHTCap.ampduParam);
        assert_memory_equal(result.htCap.supMCSSet, expectedHTCap.supMCSSet, SWL_80211_MCS_SET_LEN);
        assert_int_equal(result.htCap.htExtCap, expectedHTCap.htExtCap);
        assert_int_equal(result.htCap.txBfCap, expectedHTCap.txBfCap);
        assert_int_equal(result.htCap.ASELCap, expectedHTCap.ASELCap);

        // Test supported rates
        assert_int_equal(result.supported_rates_len, tests[i].expectedResult.supported_rates_len);
        assert_memory_equal(result.supported_rates, tests[i].expectedResult.supported_rates, tests[i].expectedResult.supported_rates_len);
    }
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_parseMultiIEsInRawBuffer),
        cmocka_unit_test(test_parseMultiIEsInMgmtFrame),
        cmocka_unit_test(test_parseOneIE),
        cmocka_unit_test(test_handleMgmtFrame),
        cmocka_unit_test(test_handleCustomBeaconIEsParse),
        cmocka_unit_test(test_handleCustomRawMgmtFrameIEsParse),
    };
    ttb_util_setFilter();
    return cmocka_run_group_tests(tests, setup_suite, teardown_suite);
}
