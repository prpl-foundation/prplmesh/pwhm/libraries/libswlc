/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_tlv.h"

static void test_swl_tlv(void** state _UNUSED) {
    char buffer[128];
    memset(buffer, 0, sizeof(buffer));

    char* data1 = "test1";
    char* data2 = "test2";
    char* data3 = "test3";
    uint16_t data4 = 65534;
    uint8_t data5 = 5;
    uint32_t data6 = 6;

    swl_tlvList_t tlvList;
    swl_tlvList_init(&tlvList);
    swl_tlvList_append(&tlvList, 1, strlen(data1), data1);
    swl_tlvList_append(&tlvList, 2, strlen(data2), data2);
    swl_tlvList_append(&tlvList, 3, strlen(data3), data3);
    swl_tlvList_appendType(&tlvList, 4, sizeof(uint16_t), &data4, &gtSwl_type_uint16);
    swl_tlvList_appendType(&tlvList, 5, sizeof(uint8_t), &data5, &gtSwl_type_uint8);
    swl_tlvList_append(&tlvList, 6, sizeof(uint32_t), &data6);

    swl_tlvList_prepend(&tlvList, 0, swl_tlvList_getTotalSize(&tlvList), NULL);

    swl_tlvList_prepend(&tlvList, 99, swl_tlvList_getTotalSize(&tlvList), NULL);

    //Serialize
    size_t secRemainingLen = 5;
    swl_rc_ne rc = swl_tlvList_toBuffer(&tlvList, buffer, sizeof(buffer), &secRemainingLen);
    assert_int_equal(rc, SWL_RC_ERROR);

    size_t remainingLen = sizeof(buffer);
    rc = swl_tlvList_toBuffer(&tlvList, NULL, sizeof(buffer), &remainingLen);
    assert_int_equal(rc, SWL_RC_INVALID_PARAM);

    rc = swl_tlvList_toBuffer(&tlvList, NULL, sizeof(buffer), NULL);
    assert_int_equal(rc, SWL_RC_INVALID_PARAM);

    rc = swl_tlvList_toBuffer(&tlvList, NULL, 129, &remainingLen);
    assert_int_equal(rc, SWL_RC_INVALID_PARAM);

    rc = swl_tlvList_toBuffer(&tlvList, buffer, sizeof(buffer), &remainingLen);
    assert_int_equal(rc, SWL_RC_OK);

    //Cleanup of old list
    swl_tlvList_cleanup(&tlvList);

    //Parsing
    swl_tlvList_t parsedTlvList;
    swl_tlvList_init(&parsedTlvList);
    size_t bufferLen = sizeof(buffer) - remainingLen;
    rc = swl_tlvList_parse(&parsedTlvList, buffer, bufferLen);
    assert_int_equal(rc, SWL_RC_OK);

    char strBuf[128] = {'\0'};
    rc = swl_tlvList_getData(&parsedTlvList, strBuf, sizeof(strBuf), 1);
    assert_int_equal(rc, SWL_RC_OK);
    assert_string_equal(strBuf, data1);

    rc = swl_tlvList_getData(&parsedTlvList, strBuf, sizeof(strBuf), 2);
    assert_int_equal(rc, SWL_RC_OK);
    assert_string_equal(strBuf, data2);

    uint16_t data4Parse = 0;
    rc = swl_tlvList_getDataTyped(&parsedTlvList, &data4Parse, &gtSwl_type_uint16, 4);
    assert_int_equal(rc, SWL_RC_OK);
    assert_int_equal(data4Parse, 65534);

    uint16_t data4Parse2 = 0;
    rc = swl_tlvList_getData(&parsedTlvList, &data4Parse2, sizeof(data4Parse2), 4);
    assert_int_equal(rc, SWL_RC_OK);
    assert_int_equal(data4Parse2, 65279);

    uint32_t data6Parse = 0;
    rc = swl_tlvList_getData(&parsedTlvList, &data6Parse, sizeof(data6Parse), 6);
    assert_int_equal(rc, SWL_RC_OK);
    assert_int_equal(data6Parse, 6);

    rc = swl_tlvList_getData(&parsedTlvList, &data6Parse, sizeof(data6Parse), 25);
    assert_int_equal(rc, SWL_RC_ERROR);

    rc = swl_tlvList_getData(&parsedTlvList, &data6Parse, sizeof(data6Parse), 0);
    assert_int_equal(rc, SWL_RC_ERROR);

    //Cleanup of parsed list
    swl_tlvList_cleanup(&parsedTlvList);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlTlv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tlv),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
