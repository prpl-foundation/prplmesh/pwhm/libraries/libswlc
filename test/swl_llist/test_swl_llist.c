/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>    // needed for cmocka
#include <sys/types.h> // needed for cmocka
#include <setjmp.h>    // needed for cmocka
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_llist.h"

static void test_llist_eachit(void** state _UNUSED) {
    swl_llist_t testList;
    swl_llist_initialize(&testList);

    typedef struct {
        swl_llist_iterator_t it;
    } test_t;

    test_t test1;
    memset(&test1, 0, sizeof(test_t));
    test_t test2;
    memset(&test2, 0, sizeof(test_t));
    swl_llist_append(&testList, &test1.it);
    swl_llist_append(&testList, &test2.it);

    swl_llist_forEachit(it, &testList) {
        test_t* tmpTest = swl_llist_iterator_data(it, test_t, it);
        swl_llist_iterator_take(&tmpTest->it);
        //Next iterator is not lost
    }

    // All entries are flushed
    assert_int_equal(swl_llist_size(&testList), 0);
}

static void test_llist_it(void** state _UNUSED) {
    swl_llist_t testList;
    swl_llist_initialize(&testList);

    typedef struct {
        swl_llist_iterator_t it;
    } test_t;

    test_t test1;
    memset(&test1, 0, sizeof(test_t));
    test_t test2;
    memset(&test2, 0, sizeof(test_t));
    swl_llist_append(&testList, &test1.it);
    swl_llist_append(&testList, &test2.it);

    swl_llist_iterator_t* it = NULL;
    swl_llist_for_each(it, &testList) {
        test_t* tmpTest = swl_llist_iterator_data(it, test_t, it);
        swl_llist_iterator_take(&tmpTest->it);
        //Next iterator is lost
    }

    assert_int_equal(swl_llist_size(&testList), 1);

}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlList");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_llist_eachit),
        cmocka_unit_test(test_llist_it),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
