/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_typeEnum.h"
#include "swl/swl_common.h"
#include "swl/swl_string.h"
#include "test_swlc_type_testCommon.h"


typedef enum {
    SWL_TYPE_TEST_0,
    SWL_TYPE_TEST_1,
    SWL_TYPE_TEST_2,
    SWL_TYPE_TEST_3,
    SWL_TYPE_TEST_4,
    SWL_TYPE_TEST_MAX,
} swl_typeTest_e;

#define M_SWL_TYPE_TEST_0 (1 << SWL_TYPE_TEST_0)
#define M_SWL_TYPE_TEST_1 (1 << SWL_TYPE_TEST_1)
#define M_SWL_TYPE_TEST_2 (1 << SWL_TYPE_TEST_2)
#define M_SWL_TYPE_TEST_3 (1 << SWL_TYPE_TEST_3)
#define M_SWL_TYPE_TEST_4 (1 << SWL_TYPE_TEST_4)

const char* swl_typeTest_str[SWL_TYPE_TEST_MAX] = {"Test0", "Test1", "Test2", "Test3", "Test4"};

SWL_TYPE_ENUM(swl_type_enumTest, swl_typeTest_e, SWL_TYPE_TEST_0, SWL_TYPE_TEST_MAX, swl_typeTest_str);

#define ENUM_TEST_VALUES 5
swl_typeTest_e enum_base[ENUM_TEST_VALUES] = {SWL_TYPE_TEST_1, SWL_TYPE_TEST_0, SWL_TYPE_TEST_2, SWL_TYPE_TEST_1, SWL_TYPE_TEST_3};
swl_typeTest_e enum_notContains[ENUM_TEST_VALUES] = {SWL_TYPE_TEST_4, SWL_TYPE_TEST_4, SWL_TYPE_TEST_4, SWL_TYPE_TEST_4, SWL_TYPE_TEST_4};

size_t enum_count[ENUM_TEST_VALUES] = {2, 1, 1, 2, 1};
#define testStrEnum "Test1,Test0,Test2,Test1,Test3"
#define testStr2Enum "Test1;;Test0;;Test2;;Test1;;Test3"
const char* testStrValuesAuto[ENUM_TEST_VALUES] = {
    "Test1", "Test0", "Test2", "Test1", "Test3"
};

const char* testStrValuesEnumInvalid[ENUM_TEST_VALUES] = {
    "Test1a", "Test01", "ATest2", "ABC", ""
};



static void test_swl_typeEnum_toChar(void** state _UNUSED) {
    for(int i = 0; i < SWL_TYPE_TEST_MAX; i++) {
        assert_string_equal(swl_typeTest_str[i], swl_typeEnum_toChar(&swl_type_enumTest, i));
    }
    int test = SWL_TYPE_TEST_MAX;
    for(int i = 0; i < 10; i++) {
        assert_string_equal(swl_typeTest_str[0], swl_typeEnum_toChar(&swl_type_enumTest, test));
        test = test * 3;
    }
    swl_type_enumTest.defaultEnumVal = SWL_TYPE_TEST_MAX;
    test = SWL_TYPE_TEST_MAX;
    for(int i = 0; i < 10; i++) {
        assert_null(swl_typeEnum_toChar(&swl_type_enumTest, test));
        test = test * 3;
    }
    swl_type_enumTest.defaultEnumVal = 0;
}

static void test_swl_typeEnum_fromChar(void** state _UNUSED) {
    for(int i = 0; i < SWL_TYPE_TEST_MAX; i++) {
        assert_int_equal(i, swl_typeEnum_fromChar(&swl_type_enumTest, swl_typeTest_str[i]));
    }
    for(int i = 0; i < ENUM_TEST_VALUES; i++) {
        assert_int_equal(enum_base[i], swl_typeEnum_fromChar(&swl_type_enumTest, testStrValuesAuto[i]));
    }
    for(int i = 0; i < ENUM_TEST_VALUES; i++) {
        assert_int_equal(SWL_TYPE_TEST_0, swl_typeEnum_fromChar(&swl_type_enumTest, testStrValuesEnumInvalid[i]));
    }
}

typedef struct maskTest {
    uint32_t mask;
    char* string;
} maskTest_t;

#define NR_MASK_TEST 4

maskTest_t maskVals[NR_MASK_TEST] = {
    {0, ""},
    {M_SWL_TYPE_TEST_0, "Test0"},
    {M_SWL_TYPE_TEST_1 | M_SWL_TYPE_TEST_3, "Test1,Test3"},
    {(1 << SWL_TYPE_TEST_MAX) - 1, "Test0,Test1,Test2,Test3,Test4"},
};

maskTest_t maskValsWeird[NR_MASK_TEST] = {
    {0, "Abracadabra"},
    {M_SWL_TYPE_TEST_0, "Test0, Abcdef"},
    {M_SWL_TYPE_TEST_1 | M_SWL_TYPE_TEST_3, "Test3,Test1"},
    {(1 << SWL_TYPE_TEST_MAX) - 1, "a,Test0,b,Test1,c,Test2,d,Test3,e,Test4,f"},
};


static void test_swl_typeEnum_charToMask(void** state _UNUSED) {
    for(int i = 0; i < NR_MASK_TEST; i++) {
        assert_int_equal(maskVals[i].mask, swl_typeEnum_charToMask(&swl_type_enumTest, maskVals[i].string));
    }
    for(int i = 0; i < NR_MASK_TEST; i++) {
        assert_int_equal(maskValsWeird[i].mask, swl_typeEnum_charToMask(&swl_type_enumTest, maskValsWeird[i].string));
    }
}

static void test_swl_typeEnum_maskToChar(void** state _UNUSED) {
    char buffer[128];
    for(int i = 0; i < NR_MASK_TEST; i++) {
        memset(buffer, 0, sizeof(buffer));
        swl_typeEnum_maskToChar(&swl_type_enumTest, buffer, sizeof(buffer), maskVals[i].mask);
        assert_string_equal(maskVals[i].string, buffer);
    }
}

static void test_swl_typeEnum_maskToCharSep(void** state _UNUSED) {
    char buffer[128];
    char tgtBuffer[128];
    for(int i = 0; i < NR_MASK_TEST; i++) {
        memset(buffer, 0, sizeof(buffer));
        swl_str_replace(tgtBuffer, sizeof(tgtBuffer), maskVals[i].string, ",", ";");
        swl_typeEnum_maskToCharSep(&swl_type_enumTest, buffer, sizeof(buffer), maskVals[i].mask, ';');
        assert_string_equal(tgtBuffer, buffer);
    }
}


static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

swlc_type_testCommonData_t testTypeEnumData = {
    .name = "enum",
    .testType = (swl_type_t*) &swl_type_enumTest,
    .serialStr = testStrEnum,
    .serialStr2 = testStr2Enum,
    .strValues = testStrValuesAuto,
    .baseValues = &enum_base,
    .valueCount = enum_count,
    .notContains = &enum_notContains,
    .nrTestValues = ENUM_TEST_VALUES,
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");

    runCommonTypeTest(&testTypeEnumData);

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_typeEnum_toChar),
        cmocka_unit_test(test_swl_typeEnum_fromChar),
        cmocka_unit_test(test_swl_typeEnum_charToMask),
        cmocka_unit_test(test_swl_typeEnum_maskToChar),
        cmocka_unit_test(test_swl_typeEnum_maskToCharSep),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);


    sahTraceClose();
    return rc;
}

