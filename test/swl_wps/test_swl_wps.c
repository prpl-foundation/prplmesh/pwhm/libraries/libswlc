/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>    // needed for cmocka
#include <sys/types.h> // needed for cmocka
#include <setjmp.h>    // needed for cmocka
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_wps.h"

static void test_wps_notifNameToType(void** state _UNUSED) {
    // Null case:
    assert_int_equal(0, swl_wps_notifNameToType(NULL));
    // Empty string case:
    assert_int_equal(0, swl_wps_notifNameToType(""));
    // Nonsense case:
    assert_int_equal(0, swl_wps_notifNameToType("Hello"));
    assert_int_equal(0, swl_wps_notifNameToType("ready"));
    assert_int_equal(0, swl_wps_notifNameToType("Ready"));
    assert_int_equal(0, swl_wps_notifNameToType("pairing"));
    // Wrong uppercase/lowercase case:
    assert_int_equal(0, swl_wps_notifNameToType("pairingready"));
    assert_int_equal(0, swl_wps_notifNameToType("PairingReady"));
    // Padding case:
    assert_int_equal(0, swl_wps_notifNameToType("pairingReady "));
    assert_int_equal(0, swl_wps_notifNameToType(" pairingReady"));

    // Normal case:
    assert_int_equal(110, swl_wps_notifNameToType("pairingReady"));
    assert_int_equal(111, swl_wps_notifNameToType("pairingDone"));
    assert_int_equal(112, swl_wps_notifNameToType("pairingError"));
}

static void test_wps_notifTypeToName(void** state _UNUSED) {
    // Invalid input:
    assert_null(swl_wps_notifTypeToName(0));
    assert_null(swl_wps_notifTypeToName(113)); // you'll have to update tests on new notif names
    assert_null(swl_wps_notifTypeToName(9001));
    assert_null(swl_wps_notifTypeToName(UINT32_MAX));

    // Normal case:
    assert_string_equal("pairingReady", swl_wps_notifTypeToName(110));
    assert_string_equal("pairingDone", swl_wps_notifTypeToName(111));
    assert_string_equal("pairingError", swl_wps_notifTypeToName(112));
}

static void test_wps_configMethodsTypeToName(void** state _UNUSED) {
    struct {
        swl_wps_cfgMethod_ne type;
        const char* name;
    } tests[] = {
        // Invalid input:
        {-1, ""},
        {SWL_WPS_CFG_MTHD_MAX, ""},
        {SWL_WPS_CFG_MTHD_DISPLAY_V - 1, ""}, //offset of wps2.x display sub types
        // Normal case:
        {SWL_WPS_CFG_MTHD_DISPLAY, "Display"},
        {SWL_WPS_CFG_MTHD_PBC, "PushButton"},
        {SWL_WPS_CFG_MTHD_PBC_V, "VirtualPushButton"},
        {SWL_WPS_CFG_MTHD_DISPLAY_V, "VirtualDisplay"},
        {SWL_WPS_CFG_MTHD_DISPLAY_P, "PhysicalDisplay"},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_string_equal(swl_wps_configMethodTypeToName(tests[i].type), tests[i].name);
    }
}

static void test_wps_configMethodsNameToType(void** state _UNUSED) {
    struct {
        const char* name;
        swl_wps_cfgMethod_ne type;
    } tests[] = {
        // Invalid input:
        {"", SWL_WPS_CFG_MTHD_MAX},
        {NULL, SWL_WPS_CFG_MTHD_MAX},
        {"toto", SWL_WPS_CFG_MTHD_MAX},
        {"PBC", SWL_WPS_CFG_MTHD_MAX},
        {"Display,PushButton", SWL_WPS_CFG_MTHD_MAX},
        // Normal case:
        {"Display", SWL_WPS_CFG_MTHD_DISPLAY},
        {"PushButton", SWL_WPS_CFG_MTHD_PBC},
        {"VirtualPushButton", SWL_WPS_CFG_MTHD_PBC_V},
        {"PhysicalDisplay", SWL_WPS_CFG_MTHD_DISPLAY_P},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_wps_configMethodNameToType(tests[i].name), tests[i].type);
    }
}

static void test_wps_configMethodsNamesToMask(void** state _UNUSED) {
    struct {
        const char* names;
        swl_wps_cfgMethod_m mask;
    } tests[] = {
        // Invalid input:
        {"", M_SWL_WPS_CFG_MTHD_NONE},
        {NULL, M_SWL_WPS_CFG_MTHD_NONE},
        {"toto", M_SWL_WPS_CFG_MTHD_NONE},
        {"toto:Display", M_SWL_WPS_CFG_MTHD_NONE},
        // Normal case:
        {"PushButton", M_SWL_WPS_CFG_MTHD_PBC},
        {"toto,Display", M_SWL_WPS_CFG_MTHD_DISPLAY},
        {"toto,,Display", M_SWL_WPS_CFG_MTHD_DISPLAY},
        {"PushButton,Display", M_SWL_WPS_CFG_MTHD_DISPLAY | M_SWL_WPS_CFG_MTHD_PBC},
        {"PushButton,,Display", M_SWL_WPS_CFG_MTHD_DISPLAY | M_SWL_WPS_CFG_MTHD_PBC},
        {"PushButton,VirtualPushButton,PhysicalPushButton", M_SWL_WPS_CFG_MTHD_PBC_ALL},
        {"PhysicalPushButton,PushButton,VirtualPushButton", M_SWL_WPS_CFG_MTHD_PBC_ALL},
        {"PhysicalDisplay,VirtualDisplay,Display", M_SWL_WPS_CFG_MTHD_DISPLAY_ALL},
        {"USBFlashDrive,Ethernet,Label,Display,ExternalNFCToken,"
            "IntegratedNFCToken,NFCInterface,PushButton,PIN", M_SWL_WPS_CFG_MTHD_WPS10_ALL},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_wps_configMethodNamesToMask(tests[i].names), tests[i].mask);
    }
}

static void test_wps_configMethodsMaskToNames(void** state _UNUSED) {
    char buf[256];

    // Invalid input:
    assert_false(swl_wps_configMethodMaskToNames(buf, 0, M_SWL_WPS_CFG_MTHD_PBC_ALL));
    assert_false(swl_wps_configMethodMaskToNames(NULL, SWL_ARRAY_SIZE(buf), M_SWL_WPS_CFG_MTHD_PBC_ALL));

    // Normal case:
    struct {
        swl_wps_cfgMethod_m mask;
        const char* names;
    } tests[] = {
        {0, ""},
        {SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_MAX) - 1,
            "USBFlashDrive,Ethernet,Label,Display,ExternalNFCToken,"
            "IntegratedNFCToken,NFCInterface,PushButton,PIN,"
            "VirtualPushButton,PhysicalPushButton,VirtualDisplay,PhysicalDisplay"},
        {M_SWL_WPS_CFG_MTHD_ALL,
            "USBFlashDrive,Ethernet,Label,Display,ExternalNFCToken,"
            "IntegratedNFCToken,NFCInterface,PushButton,PIN,"
            "VirtualPushButton,PhysicalPushButton,VirtualDisplay,PhysicalDisplay"},
        {M_SWL_WPS_CFG_MTHD_WPS10_ALL,
            "USBFlashDrive,Ethernet,Label,Display,ExternalNFCToken,"
            "IntegratedNFCToken,NFCInterface,PushButton,PIN"},
        {M_SWL_WPS_CFG_MTHD_PBC_ALL,
            "PushButton,VirtualPushButton,PhysicalPushButton"},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_true(swl_wps_configMethodMaskToNames(buf, SWL_ARRAY_SIZE(buf), tests[i].mask));
        assert_string_equal(buf, tests[i].names);
    }
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlWps");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_wps_notifNameToType),
        cmocka_unit_test(test_wps_notifTypeToName),
        cmocka_unit_test(test_wps_configMethodsTypeToName),
        cmocka_unit_test(test_wps_configMethodsNameToType),
        cmocka_unit_test(test_wps_configMethodsNamesToMask),
        cmocka_unit_test(test_wps_configMethodsMaskToNames),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
