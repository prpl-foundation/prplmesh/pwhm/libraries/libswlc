/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_bit.h"

#define TEST_SIZE 4
#define TEST_MAX_INDEX (TEST_SIZE * 32)
static uint32_t testArray[TEST_SIZE];

static uint32_t testArray1[TEST_SIZE];
static uint32_t testArray2[TEST_SIZE];
static uint32_t cmpArray[TEST_SIZE];

#define NR_TEST_LISTS 5
#define NR_VAL_PER_TEST 8
static size_t testLists[NR_TEST_LISTS][NR_VAL_PER_TEST] = {
    {0, 1, 2, 4, 8, 16, 32, 64},
    {0, 1, 2, 3, 4, 5, 6, 7},
    {127, 126, 125, 124, 123, 122, 121, 120},
    {0, 31, 32, 63, 64, 95, 96, 127},
    {15, 16, 47, 48, 79, 80, 111, 112},
};

#define NR_TEST_VALUES 9
static uint32_t testValues[NR_TEST_VALUES] = {
    0,
    UINT32_MAX,
    0x01010101,
    0x10101010,
    0xabcdabcd,
    0x01234567,
    0x89abcdef,
    0xfedcba98,
    0x76543210
};

static bool s_valInList(size_t val, size_t* list, size_t listSize) {
    for(size_t i = 0; i < listSize; i++) {
        if(list[i] == val) {
            return true;
        }
    }
    return false;
}

static void test_bitArr32_setAndClear(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
        swl_bitArr32_set(testArray, TEST_SIZE, i);
        assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
    }
    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        assert_int_equal(testArray[i], UINT32_MAX);
    }
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
        swl_bitArr32_clear(testArray, TEST_SIZE, i);
        assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
    }

    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        assert_int_equal(testArray[i], 0);
    }
}


static void test_bitArr32_write(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
        swl_bitArr32_write(testArray, TEST_SIZE, i, true);
        assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
    }
    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        assert_int_equal(testArray[i], UINT32_MAX);
    }
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
        swl_bitArr32_write(testArray, TEST_SIZE, i, false);
        assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, i));
    }

    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        assert_int_equal(testArray[i], 0);
    }
}

static void test_bitArr32_setAll(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_VALUES; i++) {
        for(int j = 0; j < TEST_SIZE; j++) {
            testArray[j] = testValues[i];
        }
        swl_bitArr32_setAll(testArray, TEST_SIZE);
        for(int j = 0; j < TEST_SIZE; j++) {
            assert_int_equal(testArray[j], UINT32_MAX);
        }
    }
}

static void test_bitArr32_setUntil(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        swl_bitArr32_clearAll(testArray, TEST_SIZE);
        swl_bitArr32_setUntil(testArray, TEST_SIZE, i);
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            assert_int_equal(swl_bitArr32_isSet(testArray, TEST_SIZE, j), j < i);
        }
    }
}

static void test_bitArr32_clearAll(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_VALUES; i++) {
        for(int j = 0; j < TEST_SIZE; j++) {
            testArray[j] = testValues[i];
        }
        swl_bitArr32_clearAll(testArray, TEST_SIZE);
        for(int j = 0; j < TEST_SIZE; j++) {
            assert_int_equal(testArray[j], 0);
        }
    }
}

static void test_bitArr32_clearUntil(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        swl_bitArr32_setAll(testArray, TEST_SIZE);
        swl_bitArr32_clearUntil(testArray, TEST_SIZE, i);
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            assert_int_equal(swl_bitArr32_isSet(testArray, TEST_SIZE, j), j >= i);
        }
    }
}

static void test_bitArr32_isSet(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        for(uint32_t j = 0; j < 32; j++) {
            uint32_t index = 32 * i + j;

            assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, index));
            W_SWL_BIT_SET(testArray[i], j);
            assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, index));
            W_SWL_BIT_CLEAR(testArray[i], j);
            assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, index));
        }
    }
}

static void test_bitArr32_isAnySet(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        for(uint32_t j = 0; j < 32; j++) {
            assert_false(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
            W_SWL_BIT_SET(testArray[i], j);
            assert_true(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
            W_SWL_BIT_CLEAR(testArray[i], j);
            assert_false(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
        }
    }

    for(uint32_t i = 1; i < TEST_MAX_INDEX + 1; i++) {
        swl_bitArr32_clearAll(testArray, TEST_SIZE);
        assert_false(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
        swl_bitArr32_setUntil(testArray, TEST_SIZE, i);
        assert_true(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
    }

    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        swl_bitArr32_setAll(testArray, TEST_SIZE);
        swl_bitArr32_clearUntil(testArray, TEST_SIZE, i);
        assert_true(swl_bitArr32_isAnySet(testArray, TEST_SIZE));
    }

}

static void test_bitArr32_isOnlySet(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_SIZE; i++) {
        for(uint32_t j = 0; j < 32; j++) {
            uint32_t index = 32 * i + j;

            assert_false(swl_bitArr32_isOnlySet(testArray, TEST_SIZE, index));
            W_SWL_BIT_SET(testArray[i], j);
            assert_true(swl_bitArr32_isOnlySet(testArray, TEST_SIZE, index));
            W_SWL_BIT_CLEAR(testArray[i], j);
            assert_false(swl_bitArr32_isOnlySet(testArray, TEST_SIZE, index));
        }
    }

    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        for(uint32_t j = 0; j < TEST_MAX_INDEX; j++) {
            uint32_t index = 32 * i + j;
            swl_bitArr32_clearAll(testArray, TEST_SIZE);
            if(i != j) {
                swl_bitArr32_set(testArray, TEST_SIZE, i);
                swl_bitArr32_set(testArray, TEST_SIZE, j);
                assert_false(swl_bitArr32_isOnlySet(testArray, TEST_SIZE, index));
            }
        }
    }
}

static void test_bitArr32_equals(void** state _UNUSED) {
    swl_bitArr32_setAll(testArray1, TEST_SIZE);
    swl_bitArr32_setAll(testArray2, TEST_SIZE);
    assert_true(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
    swl_bitArr32_clearAll(testArray1, TEST_SIZE);
    swl_bitArr32_clearAll(testArray2, TEST_SIZE);
    assert_true(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
    for(uint32_t i = 0; i < TEST_MAX_INDEX; i++) {
        for(uint32_t j = 0; j < TEST_MAX_INDEX; j++) {
            swl_bitArr32_clearAll(testArray1, TEST_SIZE);
            swl_bitArr32_clearAll(testArray2, TEST_SIZE);
            swl_bitArr32_set(testArray1, TEST_SIZE, i);
            swl_bitArr32_set(testArray2, TEST_SIZE, j);
            if(i != j) {
                assert_false(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
            } else {
                assert_true(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
            }
        }
    }
    for(uint32_t i = 0; i < NR_TEST_LISTS; i++) {
        for(uint32_t j = 0; j < NR_TEST_LISTS; j++) {
            swl_bitArr32_clearAll(testArray1, TEST_SIZE);
            swl_bitArr32_clearAll(testArray2, TEST_SIZE);
            swl_bitArr32_setList(testArray1, TEST_SIZE, &testLists[i][0], NR_VAL_PER_TEST);
            swl_bitArr32_setList(testArray2, TEST_SIZE, &testLists[j][0], NR_VAL_PER_TEST);
            if(i != j) {
                assert_false(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
            } else {
                assert_true(swl_bitArr32_equals(testArray1, testArray2, TEST_SIZE));
            }
        }
    }
}


static void test_bitArr32_getNrSet(void** state _UNUSED) {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        assert_int_equal(swl_bitArr32_getNrSet(testArray, TEST_SIZE), i);
        swl_bitArr32_set(testArray, TEST_SIZE, i);
        assert_int_equal(swl_bitArr32_getNrSet(testArray, TEST_SIZE), i + 1);
    }
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        assert_int_equal(swl_bitArr32_getNrSet(testArray, TEST_SIZE), TEST_MAX_INDEX - i);
        swl_bitArr32_clear(testArray, TEST_SIZE, i);
        assert_int_equal(swl_bitArr32_getNrSet(testArray, TEST_SIZE), TEST_MAX_INDEX - i - 1);
    }
}

static void test_bitArr32_getLowest(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            swl_bitArr32_clearAll(testArray, TEST_SIZE);
            swl_bitArr32_set(testArray, TEST_SIZE, i);
            swl_bitArr32_set(testArray, TEST_SIZE, j);
            assert_int_equal(swl_bitArr32_getLowest(testArray, TEST_SIZE), SWL_MIN(i, j));
        }
    }
}

static void test_bitArr32_getHighest(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            swl_bitArr32_clearAll(testArray, TEST_SIZE);
            swl_bitArr32_set(testArray, TEST_SIZE, i);
            swl_bitArr32_set(testArray, TEST_SIZE, j);
            assert_int_equal(swl_bitArr32_getHighest(testArray, TEST_SIZE), SWL_MAX(i, j));
        }
    }
}

static void test_bitArr32_setList(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];
        swl_bitArr32_clearAll(testArray, TEST_SIZE);
        swl_bitArr32_setList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST);

        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(s_valInList(j, testList, NR_VAL_PER_TEST)) {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }
}
static void test_bitArr32_clearList(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];
        swl_bitArr32_setAll(testArray, TEST_SIZE);
        swl_bitArr32_clearList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST);

        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(s_valInList(j, testList, NR_VAL_PER_TEST)) {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }
}
static void test_bitArr32_writeList(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];
        swl_bitArr32_clearAll(testArray, TEST_SIZE);
        swl_bitArr32_writeList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST, true);

        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(s_valInList(j, testList, NR_VAL_PER_TEST)) {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }

    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];
        swl_bitArr32_setAll(testArray, TEST_SIZE);
        swl_bitArr32_writeList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST, false);

        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(s_valInList(j, testList, NR_VAL_PER_TEST)) {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }
}

static void test_bitArr32_isSetList(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];

        for(int j = 0; j < NR_TEST_LISTS; j++) {
            size_t* mergeList = &testLists[j][0];

            swl_bitArr32_clearAll(testArray, TEST_SIZE);
            swl_bitArr32_setList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST);
            swl_bitArr32_setList(testArray, TEST_SIZE, mergeList, NR_VAL_PER_TEST);

            for(int k = 0; k < NR_TEST_LISTS; k++) {
                if((i == k) || (j == k)) {
                    assert_true(swl_bitArr32_isSetList(testArray, TEST_SIZE, &testLists[k][0], NR_VAL_PER_TEST));
                } else {
                    assert_false(swl_bitArr32_isSetList(testArray, TEST_SIZE, &testLists[k][0], NR_VAL_PER_TEST));
                }
            }
        }
    }
}

static void test_bitArr32_isOnlySetList(void** state _UNUSED) {
    for(int i = 0; i < NR_TEST_LISTS; i++) {
        size_t* testList = &testLists[i][0];

        for(int j = 0; j < NR_TEST_LISTS; j++) {
            size_t* mergeList = &testLists[j][0];

            swl_bitArr32_clearAll(testArray, TEST_SIZE);
            swl_bitArr32_setList(testArray, TEST_SIZE, testList, NR_VAL_PER_TEST);
            swl_bitArr32_setList(testArray, TEST_SIZE, mergeList, NR_VAL_PER_TEST);

            for(int k = 0; k < NR_TEST_LISTS; k++) {
                if((i == k) && (j == k)) {
                    assert_true(swl_bitArr32_isOnlySetList(testArray, TEST_SIZE, &testLists[k][0], NR_VAL_PER_TEST));
                } else {
                    assert_false(swl_bitArr32_isOnlySetList(testArray, TEST_SIZE, &testLists[k][0], NR_VAL_PER_TEST));
                }
            }
        }
    }
}

static void s_clearLists() {
    swl_bitArr32_clearAll(testArray, TEST_SIZE);
    swl_bitArr32_clearAll(testArray1, TEST_SIZE);
    swl_bitArr32_clearAll(testArray2, TEST_SIZE);
}

static void s_setLists() {
    swl_bitArr32_setAll(testArray, TEST_SIZE);
    swl_bitArr32_setAll(testArray1, TEST_SIZE);
    swl_bitArr32_setAll(testArray2, TEST_SIZE);
}

static void s_setListVals(size_t val1, size_t val2) {
    s_clearLists();
    swl_bitArr32_set(testArray1, TEST_SIZE, val1);
    swl_bitArr32_set(testArray2, TEST_SIZE, val2);
}

static void s_clearListVals(size_t val1, size_t val2) {
    s_setLists();
    swl_bitArr32_clear(testArray1, TEST_SIZE, val1);
    swl_bitArr32_clear(testArray2, TEST_SIZE, val2);
}

static void test_bitArr32_and(void** state _UNUSED) {
    // test 1 bit set
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_setListVals(i, j);
            swl_bitArr32_and(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_clearAll(cmpArray, TEST_SIZE);
            if(i == j) {
                swl_bitArr32_set(cmpArray, TEST_SIZE, i);
            }
            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }
    // test 1 bit clear
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_clearListVals(i, j);
            swl_bitArr32_and(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_setAll(cmpArray, TEST_SIZE);
            swl_bitArr32_clear(cmpArray, TEST_SIZE, i);
            swl_bitArr32_clear(cmpArray, TEST_SIZE, j);

            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }
}
static void test_bitArr32_or(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_setListVals(i, j);
            swl_bitArr32_or(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_clearAll(cmpArray, TEST_SIZE);
            swl_bitArr32_set(cmpArray, TEST_SIZE, i);
            swl_bitArr32_set(cmpArray, TEST_SIZE, j);
            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }

    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_clearListVals(i, j);
            swl_bitArr32_or(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_setAll(cmpArray, TEST_SIZE);
            if(i == j) {
                swl_bitArr32_clear(cmpArray, TEST_SIZE, i);
            }
            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }
}
static void test_bitArr32_xor(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_setListVals(i, j);
            swl_bitArr32_xor(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_clearAll(cmpArray, TEST_SIZE);
            if(i != j) {
                swl_bitArr32_set(cmpArray, TEST_SIZE, i);
                swl_bitArr32_set(cmpArray, TEST_SIZE, j);
            }
            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }

    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            s_clearListVals(i, j);
            swl_bitArr32_xor(testArray, testArray1, testArray2, TEST_SIZE);
            swl_bitArr32_clearAll(cmpArray, TEST_SIZE);
            if(i != j) {
                swl_bitArr32_set(cmpArray, TEST_SIZE, i);
                swl_bitArr32_set(cmpArray, TEST_SIZE, j);
            }
            assert_true(swl_bitArr32_equals(testArray, cmpArray, TEST_SIZE));
        }
    }
}
static void test_bitArr32_not(void** state _UNUSED) {
    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        s_clearLists();
        swl_bitArr32_set(testArray1, TEST_SIZE, i);
        swl_bitArr32_not(testArray, testArray1, TEST_SIZE);
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(i != j) {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }

    for(int i = 0; i < TEST_MAX_INDEX; i++) {
        s_setLists();
        swl_bitArr32_clear(testArray1, TEST_SIZE, i);
        swl_bitArr32_not(testArray, testArray1, TEST_SIZE);
        for(int j = 0; j < TEST_MAX_INDEX; j++) {
            if(i == j) {
                assert_true(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            } else {
                assert_false(swl_bitArr32_isSet(testArray, TEST_SIZE, j));
            }
        }
    }
}


//List getters

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_bitArr32_setAndClear),
        cmocka_unit_test(test_bitArr32_write),
        cmocka_unit_test(test_bitArr32_setAll),
        cmocka_unit_test(test_bitArr32_setUntil),
        cmocka_unit_test(test_bitArr32_clearAll),
        cmocka_unit_test(test_bitArr32_clearUntil),
        cmocka_unit_test(test_bitArr32_isSet),
        cmocka_unit_test(test_bitArr32_isAnySet),
        cmocka_unit_test(test_bitArr32_isOnlySet),
        cmocka_unit_test(test_bitArr32_equals),
        cmocka_unit_test(test_bitArr32_getNrSet),
        cmocka_unit_test(test_bitArr32_getLowest),
        cmocka_unit_test(test_bitArr32_getHighest),
        cmocka_unit_test(test_bitArr32_setList),
        cmocka_unit_test(test_bitArr32_clearList),
        cmocka_unit_test(test_bitArr32_writeList),
        cmocka_unit_test(test_bitArr32_isSetList),
        cmocka_unit_test(test_bitArr32_isOnlySetList),
        cmocka_unit_test(test_bitArr32_and),
        cmocka_unit_test(test_bitArr32_or),
        cmocka_unit_test(test_bitArr32_xor),
        cmocka_unit_test(test_bitArr32_not),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
