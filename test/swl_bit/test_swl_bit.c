/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_bit.h"

static void test_bit_macros(void** state _UNUSED) {
    int acc = 0;
    int val = 0;
    for(int i = 0; i < 32; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_SET(val, i);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_CLEAR(val, i);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_WRITE(val, i, true);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_WRITE(val, i, false);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }
}

static void test_bit_writeMacros(void** state _UNUSED) {
    int acc = 0;
    int val = 0;
    for(int i = 0; i < 32; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_SET(val, i);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_CLEAR(val, i);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_WRITE(val, i, true);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 32; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_WRITE(val, i, false);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }
}

static void test_bit_setUntil(void** state _UNUSED) {
    for(int i = 0; i < 32; i++) {
        uint32_t testVal = (SWL_BIT_SHIFT(i) - 1);
        assert_int_equal(SWL_BIT_SET_UNTIL(0, i), testVal);
    }

    for(int i = 0; i < 32; i++) {
        for(int j = 0; j < 32; j++) {
            uint32_t sourceVal = SWL_BIT_SHIFT(j);
            uint32_t testVal = (SWL_BIT_SHIFT(i) - 1) | sourceVal;
            assert_int_equal(SWL_BIT_SET_UNTIL(sourceVal, i), testVal);
        }
    }
}

static void test_bit_clearUntil(void** state _UNUSED) {
    for(int i = 0; i < 32; i++) {
        uint32_t testVal = ~(SWL_BIT_SHIFT(i) - 1);
        assert_int_equal(SWL_BIT_CLEAR_UNTIL(testVal, i), testVal);

    }

    for(int i = 0; i < 32; i++) {
        for(int j = 0; j < 32; j++) {
            uint32_t sourceVal = ~SWL_BIT_SHIFT(j);
            uint32_t testVal = ~(SWL_BIT_SHIFT(i) - 1) & sourceVal;
            assert_int_equal(SWL_BIT_CLEAR_UNTIL(sourceVal, i), testVal);
        }
    }
}

static void test_bit_macros64(void** state _UNUSED) {
    uint64_t acc = 0;
    uint64_t val = 0;
    for(int i = 0; i < 64; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_SET(val, i);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_CLEAR(val, i);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_WRITE(val, i, true);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        val = SWL_BIT_WRITE(val, i, false);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }
}

static void test_bit_writeMacros64(void** state _UNUSED) {
    uint64_t acc = 0;
    uint64_t val = 0;
    for(int i = 0; i < 64; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_SET(val, i);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_CLEAR(val, i);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_false(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_WRITE(val, i, true);
        assert_true(SWL_BIT_IS_SET(val, i));
        acc = acc + SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }

    for(int i = 0; i < 64; i++) {
        assert_true(SWL_BIT_IS_SET(val, i));
        W_SWL_BIT_WRITE(val, i, false);
        assert_false(SWL_BIT_IS_SET(val, i));
        acc = acc - SWL_BIT_SHIFT(i);
        assert_int_equal(val, acc);
    }
}

static void test_bit_setUntil64(void** state _UNUSED) {
    for(int i = 0; i < 64; i++) {
        uint64_t testVal = (SWL_BIT_SHIFT(i) - 1);
        assert_int_equal(SWL_BIT_SET_UNTIL(0, i), testVal);
    }

    for(int i = 0; i < 64; i++) {
        for(int j = 0; j < 64; j++) {
            uint64_t sourceVal = SWL_BIT_SHIFT(j);
            uint64_t testVal = (SWL_BIT_SHIFT(i) - 1) | sourceVal;
            assert_int_equal(SWL_BIT_SET_UNTIL(sourceVal, i), testVal);
        }
    }
}

static void test_bit_clearUntil64(void** state _UNUSED) {
    for(int i = 0; i < 64; i++) {
        uint64_t testVal = ~(SWL_BIT_SHIFT(i) - 1);
        assert_int_equal(SWL_BIT_CLEAR_UNTIL(testVal, i), testVal);

    }

    for(int i = 0; i < 64; i++) {
        for(int j = 0; j < 64; j++) {
            uint64_t sourceVal = ~SWL_BIT_SHIFT(j);
            uint64_t testVal = ~(SWL_BIT_SHIFT(i) - 1) & sourceVal;
            assert_int_equal(SWL_BIT_CLEAR_UNTIL(sourceVal, i), testVal);
        }
    }
}

static void test_bit32_count(void** state _UNUSED) {
    assert_int_equal(swl_bit32_getNrSet(0), 0);
    for(int i = 0; i < 32; i++) {
        assert_int_equal(swl_bit32_getNrSet(SWL_BIT_SHIFT(i)), 1);
    }
    assert_int_equal(swl_bit32_getNrSet(0xf0), 4);
    assert_int_equal(swl_bit32_getNrSet(0b1010110101), 6);
    assert_int_equal(swl_bit32_getNrSet(0b0101101011001110001), 10);
    assert_int_equal(swl_bit32_getNrSet(0xffffffff), 32);
}

static void test_bit32_getLowest(void** state _UNUSED) {
    for(int i = 0; i < 32; i++) {
        int val = SWL_BIT_SHIFT(i);
        assert_int_equal(i, swl_bit32_getLowest(val));
    }
    for(int i = 0; i < 32; i++) {
        for(int j = 0; j < 32; j++) {
            int val = SWL_BIT_SHIFT(i) | SWL_BIT_SHIFT(j);
            assert_int_equal(SWL_MIN(i, j), swl_bit32_getLowest(val));
        }
    }
}

static void test_bit32_getHighest(void** state _UNUSED) {
    for(int i = 0; i < 32; i++) {
        int val = SWL_BIT_SHIFT(i);
        assert_int_equal(i, swl_bit32_getHighest(val));
    }
    for(int i = 0; i < 32; i++) {
        for(int j = 0; j < 32; j++) {
            int val = SWL_BIT_SHIFT(i) | SWL_BIT_SHIFT(j);
            assert_int_equal(SWL_MAX(i, j), swl_bit32_getHighest(val));
        }
    }
}

static void test_bit64_count(void** state _UNUSED) {
    assert_int_equal(swl_bit64_getNrSet(0), 0);
    for(uint64_t i = 0; i < 64; i++) {
        assert_int_equal(swl_bit64_getNrSet((uint64_t) 1 << i), 1);
    }
    assert_int_equal(swl_bit64_getNrSet(0xf0), 4);
    assert_int_equal(swl_bit64_getNrSet(0b1010110101), 6);
    assert_int_equal(swl_bit64_getNrSet(0b0101101011001110001), 10);
    assert_int_equal(swl_bit64_getNrSet(0xffffffff), 32);
    assert_int_equal(swl_bit64_getNrSet(0xffffffffffffffff), 64);
    assert_int_equal(swl_bit64_getNrSet(0x1248124812481248), 16);
    assert_int_equal(swl_bit64_getNrSet(0xedb7edb7edb7edb7), 48);
}

static void test_bit64_getLowest(void** state _UNUSED) {
    for(uint64_t i = 0; i < 64; i++) {
        uint64_t val = SWL_BIT_SHIFT(i);
        assert_int_equal(i, swl_bit64_getLowest(val));
    }
    for(uint64_t i = 0; i < 64; i++) {
        for(uint64_t j = 0; j < 64; j++) {
            uint64_t val = SWL_BIT_SHIFT(i) | SWL_BIT_SHIFT(j);
            assert_int_equal(SWL_MIN(i, j), swl_bit64_getLowest(val));
        }
    }
}

static void test_bit64_getHighest(void** state _UNUSED) {
    for(uint64_t i = 0; i < 64; i++) {
        uint64_t val = SWL_BIT_SHIFT(i);
        assert_int_equal(i, swl_bit64_getHighest(val));
    }
    for(uint64_t i = 0; i < 64; i++) {
        for(uint64_t j = 0; j < 64; j++) {
            uint64_t val = SWL_BIT_SHIFT(i) | SWL_BIT_SHIFT(j);
            assert_int_equal(SWL_MAX(i, j), swl_bit64_getHighest(val));
        }
    }
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_bit_macros),
        cmocka_unit_test(test_bit_writeMacros),
        cmocka_unit_test(test_bit_setUntil),
        cmocka_unit_test(test_bit_clearUntil),
        cmocka_unit_test(test_bit_macros64),
        cmocka_unit_test(test_bit_writeMacros64),
        cmocka_unit_test(test_bit_setUntil64),
        cmocka_unit_test(test_bit_clearUntil64),
        cmocka_unit_test(test_bit32_count),
        cmocka_unit_test(test_bit32_getLowest),
        cmocka_unit_test(test_bit32_getHighest),
        cmocka_unit_test(test_bit64_count),
        cmocka_unit_test(test_bit64_getLowest),
        cmocka_unit_test(test_bit64_getHighest),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
