# Type issue tests

This folder and test file shall be used to test some specific smaller issues observed with the type infrastructure.

These test should declare what the issue was that was observed, and how it was fixed.