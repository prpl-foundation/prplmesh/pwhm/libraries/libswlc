/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_crypto.h"

static void test_crypto_functions(void** state _UNUSED) {
    const char* privKeyPath = "./certs/server.key";
    const char* certPath = "./certs/server.crt";
    const char* rootCAPath = "./certs/rootCA.pem";
    const char* trustedCAPath = "./certs/trustedCert.pem";

    // Test data to encode and decode
    const unsigned char srcData[] = "AABBCCDD";
    unsigned char dataCiphered[4096];
    size_t dataSize = sizeof(dataCiphered);
    unsigned char dataUnciphered[4096];

    // Encode data with certificate
    swl_rc_ne ret = swl_crypto_encodeDataWithCertFile(certPath, srcData, &dataSize, dataCiphered);
    assert_int_equal(ret, SWL_RC_OK);

    // Decode data with private key
    ret = swl_crypto_decodeDataWithPrivateKeyFile(privKeyPath, dataCiphered, dataSize, dataUnciphered);
    assert_int_equal(ret, SWL_RC_OK);

    assert_string_equal(srcData, dataUnciphered);

    // Get certificate and validate
    bool isValid = swl_crypto_verifyCertWithRootCAFile(certPath, rootCAPath);

    assert_true(isValid);

    // Get RSA from the certificate
    X509* cert = swl_crypto_getCertFromFile(certPath);
    assert_non_null(cert);
    EVP_PKEY* evp_rsa = swl_crypto_getRsaPubKeyFromCert(cert);

    assert_non_null(evp_rsa);
    EVP_PKEY_free(evp_rsa);
    X509_free(cert);

    // Check trusted certificate
    cert = swl_crypto_getCertFromFile(trustedCAPath);
    assert_non_null(cert);
    evp_rsa = swl_crypto_getRsaPubKeyFromCert(cert);
    assert_non_null(evp_rsa);
    EVP_PKEY_free(evp_rsa);

    X509_free(cert);
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlhash");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_crypto_functions),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
