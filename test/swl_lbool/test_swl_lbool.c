/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>    // needed for cmocka
#include <sys/types.h> // needed for cmocka
#include <setjmp.h>    // needed for cmocka
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_lbool.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "test-toolbox/ttb_assert.h"

static void s_assertLbool(swl_lbool_t lbool, const char* expectedString, bool expectedBool);

static void test_swl_lbool_true(void** state _UNUSED) {
    // GIVEN nothing

    // WHEN creating an lbool with value true
    swl_lbool_t lbool = SWL_LBOOL_TRUE;

    // THEN its filename, line number, and value (i.e. true) are correct
    s_assertLbool(lbool, "true(test_swl_lbool.c:80)", true);
}

static void test_swl_lbool_false(void** state _UNUSED) {
    // GIVEN nothing

    // WHEN creating an lbool with value false
    swl_lbool_t lbool = SWL_LBOOL_FALSE;

    // THEN its filename, line number, and value (i.e. false) are correct
    s_assertLbool(lbool, "false(test_swl_lbool.c:90)", false);
}

static void test_swl_lbool_fromBool(void** state _UNUSED) {
    // GIVEN a boolean
    bool mybool = true;

    // WHEN converting that boolean to an lbool
    swl_lbool_t lbool = SWL_LBOOL(mybool);

    // THEN the boolean's value is kept and the lbool's filename, line number are of the conversion
    s_assertLbool(lbool, "true(test_swl_lbool.c:101)", true);
}

static void test_swl_lbool_fmt(void** state _UNUSED) {
    // GIVEN an lbool
    swl_lbool_t lbool = SWL_LBOOL_FALSE;

    // WHEN formatting it to a string
    char string[64] = "";
    swl_str_catFormat(string, sizeof(string), "value is " SWL_LBOOL_FMT "!", SWL_LBOOL_ARG(lbool));

    // THEN its formatting contain the value, line and filename of creation.
    assert_string_equal("value is false(test_swl_lbool.c:109)!", string);
}

static swl_lbool_t s_returnAnLbool() {
    return SWL_LBOOL_FALSE;
}

static void test_canBeUsedInReturnStatement(void** state _UNUSED) {
    // GIVEN a function that returns an lbool
    // WHEN calling it
    swl_lbool_t lbool = s_returnAnLbool();
    // THEN the compilers does not complain.
    // AND THEN the lbool's location is of the callee
    s_assertLbool(lbool, "false(test_swl_lbool.c:120)", false);
}

/** test helper function */
static void s_assertLbool(swl_lbool_t lbool, const char* expectedString, bool expectedBool) {
    char string[64] = "";
    bool ok = swl_str_catFormat(string, sizeof(string), SWL_LBOOL_FMT, SWL_LBOOL_ARG(lbool));

    ttb_assert(ok);
    ttb_assert_str_eq(expectedString, string);
    ttb_assert(expectedBool == SWL_LBOOL_BOOL(lbool));
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_lbool_true),
        cmocka_unit_test(test_swl_lbool_false),
        cmocka_unit_test(test_swl_lbool_fromBool),
        cmocka_unit_test(test_swl_lbool_fmt),
        cmocka_unit_test(test_canBeUsedInReturnStatement),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
