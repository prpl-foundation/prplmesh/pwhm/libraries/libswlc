/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common_trilean.h"
#include "swl/swl_common.h"
#include "test-toolbox/ttb_pcb.h"


static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

static void s_testConvertBackForthNormalCase(const char* expectedChar, swl_trl_e expectedTrl,
                                             swl_trl_format_e format) {

    //printf("debugprint: char %s trl %d format %d\n", expectedChar, expectedTrl, format);

    swl_trl_e actualTrl = 123;
    assert_true(swl_trl_fromChar(&actualTrl, expectedChar, format));
    assert_int_equal(expectedTrl, actualTrl);

    const char* actualChar = NULL;
    assert_true(swl_trl_toChar(&actualChar, expectedTrl, format));
    assert_non_null(actualChar);
    assert_string_equal(expectedChar, actualChar);
}

static void test_swl_trl_fromChar_toChar_normalCases(void** state _UNUSED) {
    s_testConvertBackForthNormalCase("Off", SWL_TRL_FALSE, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("On", SWL_TRL_TRUE, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("Auto", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_AUTO);
    s_testConvertBackForthNormalCase("Auto", SWL_TRL_AUTO, SWL_TRL_FORMAT_AUTO);

    s_testConvertBackForthNormalCase("False", SWL_TRL_FALSE, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("True", SWL_TRL_TRUE, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("Unknown", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_UNKNOWN);
    s_testConvertBackForthNormalCase("Unknown", SWL_TRL_AUTO, SWL_TRL_FORMAT_UNKNOWN);

    s_testConvertBackForthNormalCase("0", SWL_TRL_FALSE, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("1", SWL_TRL_TRUE, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("-1", SWL_TRL_UNKNOWN, SWL_TRL_FORMAT_NUMERIC);
    s_testConvertBackForthNormalCase("-1", SWL_TRL_AUTO, SWL_TRL_FORMAT_NUMERIC);
}

static void test_swl_trl_fromChar_errorCases(void** state _UNUSED) {
    swl_trl_e trl = 123;

    // case: Different notation
    assert_false(swl_trl_fromChar(&trl, "Unknown", SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_fromChar(&trl, "Auto", SWL_TRL_FORMAT_UNKNOWN));
    assert_false(swl_trl_fromChar(&trl, "True", SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_fromChar(&trl, "Yes", SWL_TRL_FORMAT_AUTO));

    // case: Empty string
    assert_false(swl_trl_fromChar(&trl, "", SWL_TRL_FORMAT_AUTO));

    // case: NULL string
    assert_false(swl_trl_fromChar(&trl, NULL, SWL_TRL_FORMAT_AUTO));

    // case: string contains extras
    assert_false(swl_trl_fromChar(&trl, "On ", SWL_TRL_FORMAT_AUTO));

    // case: null target
    assert_false(swl_trl_fromChar(NULL, "Off", SWL_TRL_FORMAT_AUTO));

    // case: invalid format
    assert_false(swl_trl_fromChar(NULL, "Off", SWL_TRL_FORMAT_MAX));

    // On error we don't want it to write.
    assert_int_equal(123, trl);
}

static void test_swl_trl_toChar_errorCases(void** state _UNUSED) {
    const char* text = NULL;

    // case: not a trilean value
    assert_false(swl_trl_toChar(&text, SWL_TRL_MAX, SWL_TRL_FORMAT_AUTO));
    assert_false(swl_trl_toChar(&text, -1, SWL_TRL_FORMAT_AUTO));

    // case: null target
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, SWL_TRL_FORMAT_AUTO));

    // case: not a valid format
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, SWL_TRL_FORMAT_MAX));
    assert_false(swl_trl_toChar(NULL, SWL_TRL_TRUE, -1));
}

static void test_swl_trl_isValid(void** state _UNUSED) {
    assert_true(swl_trl_isValid(SWL_TRL_FALSE));
    assert_true(swl_trl_isValid(SWL_TRL_TRUE));
    assert_true(swl_trl_isValid(SWL_TRL_UNKNOWN));

    assert_false(swl_trl_isValid(SWL_TRL_MAX));
    assert_false(swl_trl_isValid(64));
    assert_false(swl_trl_isValid((swl_trl_e) - 1));
}

static void test_swl_trl_fromBool(void** state _UNUSED) {
    assert_true(SWL_TRL_FALSE == swl_trl_fromBool(false));
    assert_true(SWL_TRL_TRUE == swl_trl_fromBool(true));
}

static void test_swl_trl_toBool(void** state _UNUSED) {
    // "true==x" instead of "assert_true(x)" because we don't want it to return 5 or -1.
    assert_true(true == swl_trl_toBool(SWL_TRL_TRUE, false));
    assert_true(true == swl_trl_toBool(SWL_TRL_TRUE, true));
    assert_true(false == swl_trl_toBool(SWL_TRL_FALSE, false));
    assert_true(false == swl_trl_toBool(SWL_TRL_FALSE, true));
    assert_true(false == swl_trl_toBool(SWL_TRL_UNKNOWN, false));
    assert_true(true == swl_trl_toBool(SWL_TRL_UNKNOWN, true));

    // case: not a trilean value
    assert_true(false == swl_trl_toBool(-1, true));
    assert_true(false == swl_trl_toBool(-1, false));
}

static void test_swl_trl_fromInt(void** state _UNUSED) {

    assert_true(SWL_TRL_FALSE == swl_trl_fromInt(0));

    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(1));
    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(88));
    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(INT8_MAX));


    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(-1));
    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(-88));
    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(INT8_MIN));


    // int32 testing
    int32_t testVal = 0;

    testVal = 128;
    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(testVal));
    testVal = INT32_MAX / 2;
    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(testVal));
    testVal = INT32_MAX;
    assert_true(SWL_TRL_TRUE == swl_trl_fromInt(testVal));


    testVal = -129;
    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(testVal));
    testVal = INT32_MIN;
    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(testVal));
    testVal = INT32_MIN / 2;
    assert_true(SWL_TRL_UNKNOWN == swl_trl_fromInt(testVal));
}

static void test_swl_trl_toInt(void** state _UNUSED) {

    assert_true(1 == swl_trl_toInt(SWL_TRL_TRUE));
    assert_true(0 == swl_trl_toInt(SWL_TRL_FALSE));
    assert_true(-1 == swl_trl_toInt(SWL_TRL_UNKNOWN));
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlTrl");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_trl_fromChar_toChar_normalCases),
        cmocka_unit_test(test_swl_trl_fromChar_errorCases),
        cmocka_unit_test(test_swl_trl_toChar_errorCases),
        cmocka_unit_test(test_swl_trl_isValid),
        cmocka_unit_test(test_swl_trl_fromBool),
        cmocka_unit_test(test_swl_trl_toBool),
        cmocka_unit_test(test_swl_trl_fromInt),
        cmocka_unit_test(test_swl_trl_toInt),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
