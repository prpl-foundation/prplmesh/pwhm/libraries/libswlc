/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common_histogram.h"
#include "swl/swl_common.h"
#include "swl/swl_common_conversion.h"
#include "swl/ttb/swl_ttb.h"
#include "test-toolbox/ttb.h"

#define NR_BUCKETS 4
#define NR_WINDOWS 5
#define NR_MEAS_PER_WINDOW 30
#define HISTORY_SIZE 8

static uint8_t bucketSizes[NR_BUCKETS] = {5, 10, 20, 4};

#define MONOTIME_BASE 100000
#define REALTIME_BASE 1588169613
#define REALTIME_STR "2020-04-29T14:13:33Z"

#define MY_TEST_HISTOGRAM_VAR(X, Y) \
    X(Y, test0, "Test0", 5) \
    X(Y, test1, "Test1", 10) \
    X(Y, test2, "Test2", 20) \
    X(Y, test3, "Test3", 4)



SWL_HISTOGRAM(s_myHistInit, swl_myTestHistData_t, MY_TEST_HISTOGRAM_VAR);


struct timespec myMonoTime = {.tv_sec = MONOTIME_BASE, .tv_nsec = 0};
struct timespec myRealTime = {.tv_sec = REALTIME_BASE, .tv_nsec = 0};

static void s_resetTime() {
    ttb_mockClock_init(&myMonoTime, &myRealTime);
}

static void s_checkHistogramToFile(swl_histogram_t* hist, const char* fileName, swl_histogram_windowId_e index) {
    assert_non_null(fileName);
    swl_print_args_t myArgs = g_swl_print_jsonCompact;
    char tmpNameBuffer[L_tmpnam] = {0}; \
    tmpnam(tmpNameBuffer); \
    swl_histogram_printWindowToFileName(hist, gTtb_assert_printMode ? fileName : tmpNameBuffer, index, &myArgs, false);

    if(!gTtb_assert_printMode) { \
        swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
        remove(tmpNameBuffer); \
    } \
}

static void s_checkHistoryToFile(swl_histogram_t* hist, const char* fileName) {
    swl_print_args_t myArgs = g_swl_print_jsonCompact;
    char tmpNameBuffer[L_tmpnam] = {0}; \
    tmpnam(tmpNameBuffer); \

    swl_histogram_printHistoryToFileName(hist, gTtb_assert_printMode ? fileName : tmpNameBuffer, &myArgs, false);

    if(!gTtb_assert_printMode) { \
        swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
        remove(tmpNameBuffer); \
    } \
}

static void s_checkAllHistToFile(swl_histogram_t* hist, const char* fileName) {
    for(uint32_t i = 0; i < SWL_HISTOGRAM_WINDOW_ID_MAX; i++) {
        char buffer[64] = {0};
        snprintf(buffer, sizeof(buffer), "%s_%s.txt", fileName, swl_histogram_tableNames_str[i]);
        s_checkHistogramToFile(hist, buffer, i);
    }
}

static void s_checkHistogramToFileNames(swl_histogram_t* hist, const char* currentFile, const char* lastFile, const char* totalFile) {
    if(currentFile != NULL) {
        s_checkHistogramToFile(hist, currentFile, SWL_HISTOGRAM_WINDOW_ID_CURRENT);
    }
    if(lastFile != NULL) {
        s_checkHistogramToFile(hist, lastFile, SWL_HISTOGRAM_WINDOW_ID_LAST);
    }
    if(totalFile != NULL) {
        s_checkHistogramToFile(hist, totalFile, SWL_HISTOGRAM_WINDOW_ID_TOTAL);
    }
}

static void test_swl_histogram_enum(void** state _UNUSED) {
    ttb_assert_int_eq(s_myHistInit_test0, 0);
    ttb_assert_int_eq(s_myHistInit_test1, 1);
    ttb_assert_int_eq(s_myHistInit_test2, 2);
    ttb_assert_int_eq(s_myHistInit_test3, 3);
    ttb_assert_int_eq(s_myHistInit__max, 4);
}

static void test_swl_histogram_init(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    assert_int_equal(hist.totalNrBuckets, 39);
    assert_non_null(hist.nrBucketsPerValue);
    assert_non_null(hist.offsetList);
    uint16_t offsetList[NR_BUCKETS] = {0, 5, 15, 35};
    assert_memory_equal(offsetList, hist.offsetList, NR_BUCKETS * sizeof(uint16_t));

    assert_non_null(hist.histogramData.currentWindow.buckets);
    assert_non_null(hist.histogramData.totalWindow.buckets);
    assert_non_null(hist.histogramData.lastWindows);
    for(uint32_t i = 0; i < NR_WINDOWS; i++) {
        assert_non_null(hist.histogramData.lastWindows[i].buckets);
    }
    for(uint32_t i = 0; i < SWL_HISTOGRAM_WINDOW_ID_MAX; i++) {
        s_checkHistogramToFile(&hist, "histData/defaultData.txt", i);
    }

    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_initExt(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    swl_histogram_initExt(&hist, &s_myHistInit, NR_MEAS_PER_WINDOW, NR_WINDOWS, HISTORY_SIZE);

    assert_int_equal(hist.totalNrBuckets, 39);
    assert_non_null(hist.nrBucketsPerValue);
    assert_non_null(hist.offsetList);
    uint16_t offsetList[NR_BUCKETS] = {0, 5, 15, 35};
    assert_memory_equal(offsetList, hist.offsetList, NR_BUCKETS * sizeof(uint16_t));

    assert_non_null(hist.histogramData.currentWindow.buckets);
    assert_non_null(hist.histogramData.totalWindow.buckets);
    assert_non_null(hist.histogramData.lastWindows);
    for(uint32_t i = 0; i < NR_WINDOWS; i++) {
        assert_non_null(hist.histogramData.lastWindows[i].buckets);
    }

    for(uint32_t i = 0; i < SWL_HISTOGRAM_WINDOW_ID_MAX; i++) {
        s_checkHistogramToFile(&hist, "histData/defaultDataNamed.txt", i);
    }

    swl_histogram_destroy(&hist);
}

static uint32_t curIndex = 0;
static uint8_t nrFourIndexTest[NR_WINDOWS + 1] = {0, 0, 0, 1, 1, 2};

static void s_addMeasurement(swl_histogram_t* hist) {
    ttb_mockClock_addTime(1, 0);

    swl_myTestHistData_t data;
    data.test0 = curIndex % bucketSizes[0];
    data.test1 = ((curIndex / 2) * 2) % bucketSizes[1];
    data.test2 = (curIndex / 3 + (curIndex + 1) / 3) % bucketSizes[2];
    data.test3 = nrFourIndexTest[ (curIndex / NR_MEAS_PER_WINDOW) % 6];


    swl_histogram_addDataValues(hist, &data);

    curIndex += 1;
}

static void s_addMeasurements(swl_histogram_t* hist, uint32_t nrMeas) {
    for(uint32_t i = 0; i < nrMeas; i++) {
        s_addMeasurement(hist);
    }
}


static char* nameStrings[4];

static void s_setDefaultNameStrings() {
    nameStrings[0] = "0";
    nameStrings[1] = "1";
    nameStrings[2] = "2";
    nameStrings[3] = "3";
}

static void test_swl_histogram_getMap(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_initExt(&hist, &s_myHistInit, NR_MEAS_PER_WINDOW, NR_WINDOWS, HISTORY_SIZE);


    s_checkAllHistToFile(&hist, "addDataTest/testZero");

    s_addMeasurements(&hist, 1);

    s_checkAllHistToFile(&hist, "addDataTest/testOne");

    s_addMeasurements(&hist, NR_MEAS_PER_WINDOW - 2);
    s_checkAllHistToFile(&hist, "addDataTest/testMinusOne");

    s_addMeasurements(&hist, 1);

    s_checkAllHistToFile(&hist, "addDataTest/testFullWindow");


    s_addMeasurements(&hist, (NR_WINDOWS - 1) * NR_MEAS_PER_WINDOW - 1);

    s_checkAllHistToFile(&hist, "addDataTest/testFullMinOne");

    s_addMeasurements(&hist, 1);

    s_checkAllHistToFile(&hist, "addDataTest/testFull");

    s_addMeasurements(&hist, 1);

    s_checkAllHistToFile(&hist, "addDataTest/testFullPlusOne");


    //need to cycle nrWindows + 1 as we have a 6 window cycle, and only 5 windows in histogram
    s_addMeasurements(&hist, (NR_WINDOWS + 1) * NR_MEAS_PER_WINDOW);

    s_checkAllHistToFile(&hist, "addDataTest/testFullCycle");

    swl_histogram_reset(&hist);

    s_checkAllHistToFile(&hist, "addDataTest/testReset");

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW + 1);

    s_checkAllHistToFile(&hist, "addDataTest/testAfterReset");

    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_setNames(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);


    s_checkAllHistToFile(&hist, "setNamesTest/testNoName");

    char* newNames[NR_BUCKETS] = {"name1", "name2", "name3", "name4"};


    swl_histogram_setNames(&hist, newNames);

    s_checkAllHistToFile(&hist, "setNamesTest/testName");


    swl_histogram_destroy(&hist);

}


static uint32_t calls[SWL_HISTOGRAM_EVENT_MAX] = {0};

void myHistogramEventHandler (swl_histogram_t* map, swl_histogram_event_e event, void* data) {
    assert_ptr_equal(calls, data);
    assert_non_null(map);
    assert_true(event < SWL_HISTOGRAM_EVENT_MAX);
    calls[event] += 1;
}

void checkCallValues(uint32_t nrWindowDone, uint32_t nrCycleDone) {
    assert_int_equal(nrWindowDone, calls[SWL_HISTOGRAM_EVENT_WINDOW_DONE]);
    assert_int_equal(nrCycleDone, calls[SWL_HISTOGRAM_EVENT_HISTORY_DONE]);
}



static void s_checkHistoryFromValue(swl_histogram_t* hist, uint8_t* testData, uint8_t testDataSizePerVal, size_t expectNrEl, bool done) {
    for(uint32_t i = 0; i < NR_BUCKETS; i++) {
        uint8_t data1[testDataSizePerVal];
        memset(data1, 0, testDataSizePerVal);
        size_t nrEl = 0;

        swl_rc_ne result = swl_histogram_getHistoryFromValue(hist, data1, testDataSizePerVal, i, &nrEl);
        if(done) {
            assert_int_equal(result, SWL_RC_OK);
        } else {
            assert_int_equal(result, SWL_RC_CONTINUE);
        }
        assert_int_equal(nrEl, expectNrEl);

        //Ensure NULL nrEl call produces same result
        uint8_t data2[testDataSizePerVal];
        memset(data2, 0, testDataSizePerVal);
        swl_histogram_getHistoryFromValue(hist, data2, testDataSizePerVal, i, NULL);
        if(done) {
            assert_int_equal(result, SWL_RC_OK);
        } else {
            assert_int_equal(result, SWL_RC_CONTINUE);
        }

        assert_memory_equal(data1, data2, testDataSizePerVal);
        assert_memory_equal(&testData[i * testDataSizePerVal], data1, testDataSizePerVal);
    }

}




static void test_swl_histogram_setHandler(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);
    swl_histogram_setHandler(&hist, myHistogramEventHandler, calls);

    memset(calls, 0, SWL_HISTOGRAM_EVENT_MAX * SWL_ARRAY_SIZE(calls));
    checkCallValues(0, 0);

    for(uint32_t i = 0; i < 5 * NR_WINDOWS; i++) {
        s_addMeasurements(&hist, NR_MEAS_PER_WINDOW - 1);
        checkCallValues(i, i / NR_WINDOWS);
        s_addMeasurements(&hist, 1);
        checkCallValues(i + 1, (i + 1) / NR_WINDOWS);
    }

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_setNrObsPerWindow(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_checkAllHistToFile(&hist, "setNrObsTest/empty");


    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW - 1);

    s_checkAllHistToFile(&hist, "setNrObsTest/oneMeas");

    s_addMeasurements(&hist, 1);

    swl_histogram_setNrObsPerWindow(&hist, NR_MEAS_PER_WINDOW / 2);

    s_checkAllHistToFile(&hist, "setNrObsTest/changeWindow");


    s_addMeasurements(&hist, 2 * NR_MEAS_PER_WINDOW + 10);


    s_checkAllHistToFile(&hist, "setNrObsTest/changeWindowAdd");

    swl_histogram_destroy(&hist);
}

/**
 * Test setNrWindows and getNrFilledWindows
 */
static void test_swl_histogram_setNrWindows(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS * 2);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS);
    assert_int_equal(0, swl_histogram_getNrFilledWindows(&hist));

    s_checkAllHistToFile(&hist, "setNrWindowTest/empty");

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW + 1);

    s_checkAllHistToFile(&hist, "setNrWindowTest/filled");

    assert_int_equal(NR_WINDOWS, swl_histogram_getNrFilledWindows(&hist));

    // Increase nr windows, no impact allowed

    swl_histogram_setNrWindows(&hist, NR_WINDOWS * 2);

    s_checkAllHistToFile(&hist, "setNrWindowTest/filled");

    assert_int_equal(NR_WINDOWS, swl_histogram_getNrFilledWindows(&hist));

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW * 2);

    s_checkAllHistToFile(&hist, "setNrWindowTest/bigger");

    swl_histogram_setNrWindows(&hist, NR_WINDOWS / 2);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    s_checkAllHistToFile(&hist, "setNrWindowTest/smaller");

    s_addMeasurements(&hist, (NR_WINDOWS) *NR_MEAS_PER_WINDOW);
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));


    s_checkAllHistToFile(&hist, "setNrWindowTest/smallerAdd");

    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS);
    s_checkAllHistToFile(&hist, "setNrWindowTest/smallerAdd");
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));

    swl_histogram_setNrWindows(&hist, NR_WINDOWS / 2);
    s_checkAllHistToFile(&hist, "setNrWindowTest/smallerAdd");
    assert_int_equal(NR_WINDOWS / 2, swl_histogram_getNrFilledWindows(&hist));


    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_getHistoryFromValue(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData[NR_BUCKETS * HISTORY_SIZE] = {
        0, 1, 2, 3, 0, 0, 0, 0,
        0, 0, 2, 2, 0, 0, 0, 0,
        0, 0, 1, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };

    s_checkHistoryFromValue(&hist, historyData, HISTORY_SIZE, HISTORY_SIZE / 2, true);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData2[NR_BUCKETS * HISTORY_SIZE] = {
        0, 1, 2, 3, 4, 0, 1, 2,
        0, 0, 2, 2, 4, 4, 6, 6,
        0, 0, 1, 2, 2, 3, 4, 4,
        0, 0, 0, 0, 0, 0, 0, 0
    };

    s_checkHistoryFromValue(&hist, historyData2, HISTORY_SIZE, HISTORY_SIZE, true);

    s_addMeasurements(&hist, HISTORY_SIZE / 2);

    uint8_t historyData3[NR_BUCKETS * HISTORY_SIZE] = {
        4, 0, 1, 2, 3, 4, 0, 1,
        4, 4, 6, 6, 8, 8, 0, 0,
        2, 3, 4, 4, 5, 6, 6, 7,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    s_checkHistoryFromValue(&hist, historyData3, HISTORY_SIZE, HISTORY_SIZE, true);

    //test smaller len
    uint8_t historyData4[NR_BUCKETS * HISTORY_SIZE / 2] = {
        3, 4, 0, 1,
        8, 8, 0, 0,
        5, 6, 6, 7,
        0, 0, 0, 0,
    };
    s_checkHistoryFromValue(&hist, historyData4, HISTORY_SIZE / 2, HISTORY_SIZE / 2, false);

    uint8_t historyData5[NR_BUCKETS * (HISTORY_SIZE + 2)] = {
        4, 0, 1, 2, 3, 4, 0, 1, 0, 0,
        4, 4, 6, 6, 8, 8, 0, 0, 0, 0,
        2, 3, 4, 4, 5, 6, 6, 7, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    s_checkHistoryFromValue(&hist, historyData5, HISTORY_SIZE + 2, HISTORY_SIZE, true);

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_checkAddError(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_checkAllHistToFile(&hist, "addErrorTest/0");

    uint8_t indexes[NR_BUCKETS] = {100, 100, 100, 100};
    swl_histogram_addValues(&hist, indexes);

    s_checkAllHistToFile(&hist, "addErrorTest/1");



    uint8_t indexes2[NR_BUCKETS] = {0, 0, 100, 100};
    swl_histogram_addValues(&hist, indexes2);


    s_checkAllHistToFile(&hist, "addErrorTest/2");

    swl_histogram_destroy(&hist);
}

static void test_swl_histogram_checkUninitHist(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    uint8_t indexes[NR_BUCKETS] = {100, 100, 100, 100};
    swl_histogram_addValues(&hist, indexes);

}

static uint8_t nrFourIndexTest2[NR_WINDOWS] = {1, 2, 2, 2, 3};

static void s_addMeasurement2(swl_histogram_t* hist) {
    ttb_mockClock_addTime(1, 0);

    uint8_t indexes[NR_BUCKETS] = {0};
    indexes[0] = nrFourIndexTest2[ curIndex % 5];
    indexes[1] = ( 2 * (curIndex % 5)) % bucketSizes[1];
    indexes[2] = 5 + curIndex % 10;
    indexes[3] = curIndex % bucketSizes[3];


    swl_histogram_addValues(hist, indexes);

    curIndex += 1;
}

static void s_addMeasurements2(swl_histogram_t* hist, uint32_t nrMeas) {
    for(uint32_t i = 0; i < nrMeas; i++) {
        s_addMeasurement2(hist);
    }
}


#define NR_BCKT_VAL 7

static void s_checkBuckets(swl_histogram_t* hist, swl_histogram_windowId_e id, uint16_t* offsets, char** strings) {
    char buffer[100];
    uint16_t indices[NR_BCKT_VAL] = {0};
    for(int i = 0; i < 4; i++) {
        swl_histogram_getBucketOffsets(hist, id, i, indices, offsets, NR_BCKT_VAL);

        swl_typeUInt16_arrayToChar(buffer, sizeof(buffer), indices, NR_BCKT_VAL);
        assert_string_equal(buffer, strings[i]);
    }
}

static void test_swl_histogram_getBucketOffsets(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, 40, 4, bucketSizes, HISTORY_SIZE);



    s_addMeasurements2(&hist, (4 + 1) * 40 - 1);

    uint16_t offsets[NR_BCKT_VAL] = {0, 10, 20, 50, 80, 90, 100};

    char* expPctStr[4] = {"100,150,200,250,300,350,400",
        "0,50,200,450,800,850,900",
        "500,600,700,1000,1300,1400,1500",
        "0,40,80,200,320,360,400"};

    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_TOTAL, offsets, expPctStr);
    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_LAST, offsets, expPctStr);

    //current strings are a bit off due to last measurement not being available
    char* curExpPctStr[4] = {"100,150,200,248,296,344,400",
        "0,50,200,445,690,844,900",
        "500,600,700,990,1281,1380,1500",
        "0,40,80,196,316,358,400"};
    s_checkBuckets(&hist, SWL_HISTOGRAM_WINDOW_ID_CURRENT, offsets, curExpPctStr);


    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_setHistoryLen(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_init(&hist, NR_BUCKETS, NR_MEAS_PER_WINDOW, NR_WINDOWS, bucketSizes, HISTORY_SIZE);

    s_checkHistoryToFile(&hist, "historyData/empty.txt");

    s_addMeasurements(&hist, HISTORY_SIZE);

    s_checkHistoryToFile(&hist, "historyData/fill.txt");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE / 2);
    s_checkHistoryToFile(&hist, "historyData/setHalf.txt");

    s_addMeasurements(&hist, HISTORY_SIZE / 2);
    s_checkHistoryToFile(&hist, "historyData/fillHalf.txt");


    s_addMeasurements(&hist, HISTORY_SIZE / 2 + 2);
    s_checkHistoryToFile(&hist, "historyData/setPlusTwo.txt");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE + 2);

    s_checkHistoryToFile(&hist, "historyData/fillPlusTwo.txt");

    s_addMeasurements(&hist, HISTORY_SIZE - 2);

    s_checkHistoryToFile(&hist, "historyData/setMinusTwo.txt");

    s_addMeasurements(&hist, HISTORY_SIZE + 2);
    s_checkHistoryToFile(&hist, "historyData/addMinusTwo.txt");

    s_addMeasurements(&hist, HISTORY_SIZE);
    s_checkHistoryToFile(&hist, "historyData/addMinusTwoFull.txt");

    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE);
    s_checkHistoryToFile(&hist, "historyData/setFullAgain.txt");

    swl_histogram_setHistoryLen(&hist, 0);
    s_checkHistoryToFile(&hist, "historyData/empty.txt");

    s_addMeasurements(&hist, HISTORY_SIZE * 2);
    s_checkHistoryToFile(&hist, "historyData/empty.txt");


    swl_histogram_setHistoryLen(&hist, HISTORY_SIZE);

    s_addMeasurements(&hist, HISTORY_SIZE * 2);
    s_checkHistoryToFile(&hist, "historyData/finalFill.txt");

    swl_histogram_destroy(&hist);
}


static void test_swl_histogram_cycleWindow(void** state _UNUSED) {
    swl_histogram_t hist = {0};
    s_resetTime();
    curIndex = 0;

    swl_histogram_initExt(&hist, &s_myHistInit, 0, 4, 0);

    s_checkHistogramToFileNames(&hist, "emptyNamed.txt", "emptyNamed.txt", "emptyNamed.txt");

    s_addMeasurements(&hist, HISTORY_SIZE * 2);

    s_checkHistogramToFileNames(&hist, "manualCycleTest/fill1.txt", "emptyNamed.txt", "emptyNamed.txt");

    swl_histogram_cycleWindow(&hist);

    s_checkHistogramToFileNames(&hist, "emptyNamed.txt", "manualCycleTest/fill1.txt", "manualCycleTest/fill1.txt");


    s_addMeasurements(&hist, HISTORY_SIZE * 2);

    s_checkHistogramToFileNames(&hist, "manualCycleTest/fill2.txt", "manualCycleTest/fill1.txt", "manualCycleTest/fill1.txt");

    swl_histogram_cycleWindow(&hist);
    s_checkHistogramToFileNames(&hist, "emptyNamed.txt", "manualCycleTest/fill2.txt", "manualCycleTest/fill2Total.txt");

    swl_histogram_destroy(&hist);
}

static int setup_suite(void** state _UNUSED) {
    s_setDefaultNameStrings();
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_histogram_enum),
        cmocka_unit_test(test_swl_histogram_init),
        cmocka_unit_test(test_swl_histogram_initExt),
        cmocka_unit_test(test_swl_histogram_getMap),
        cmocka_unit_test(test_swl_histogram_setNames),
        cmocka_unit_test(test_swl_histogram_setHandler),
        cmocka_unit_test(test_swl_histogram_setNrObsPerWindow),
        cmocka_unit_test(test_swl_histogram_setNrWindows),
        cmocka_unit_test(test_swl_histogram_getHistoryFromValue),
        cmocka_unit_test(test_swl_histogram_setHistoryLen),
        cmocka_unit_test(test_swl_histogram_cycleWindow),
        cmocka_unit_test(test_swl_histogram_checkAddError),
        cmocka_unit_test(test_swl_histogram_checkUninitHist),
        cmocka_unit_test(test_swl_histogram_getBucketOffsets),

    };
    int rc = 0;

    ttb_util_setFilter();
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
