/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_string.h"
#include "swl/fileOps/swl_print.h"
#include "swl/fileOps/swl_fileUtils.h"

#define NR_DATA_FIELDS 3
char* data[NR_DATA_FIELDS] = {
    "testtest",
    "abcdefg",
    "\"bbcda\""
};

#define TMP_FILE_ORIG_LOC "tmpFileOrig.txt"
#define TMP_FILE_WRITE_LOC "tmpFileWrite.txt"


static void test_swl_print_toStreamBuf(void** state _UNUSED) {
    // No escape test
    swl_print_args_t arg = g_swl_print_dm;
    swl_print_args_t argJson = g_swl_print_json;

    for(size_t i = 0; i < NR_DATA_FIELDS; i++) {
        FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
        fprintf(fOrigPtr, "%s", data[i]);
        fclose(fOrigPtr);

        FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
        assert_true(swl_print_toStreamBuf(fWritePtr, &arg, data[i], swl_str_len(data[i])));
        fclose(fWritePtr);

        assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

        remove(TMP_FILE_WRITE_LOC);
        remove(TMP_FILE_ORIG_LOC);
    }

    // escape test

    for(size_t i = 0; i < NR_DATA_FIELDS; i++) {
        FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
        size_t len = swl_str_len(data[i]);
        char buffer[len * 2 + 1];
        memset(buffer, 0, sizeof(buffer));
        snprintf(buffer, sizeof(buffer), "%s", data[i]);
        swl_str_addEscapeChar(buffer, sizeof(buffer), g_swl_print_json.charsToEscape, g_swl_print_json.escapeChar);
        fprintf(fOrigPtr, "%s", buffer);
        fclose(fOrigPtr);

        FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
        assert_true(swl_print_toStreamBuf(fWritePtr, &argJson, data[i], swl_str_len(data[i])));
        fclose(fWritePtr);

        assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

        remove(TMP_FILE_WRITE_LOC);
        remove(TMP_FILE_ORIG_LOC);
    }


    FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
    fclose(fOrigPtr);

    FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
    assert_false(swl_print_toStreamBuf(NULL, &argJson, data[0], swl_str_len(data[0])));
    assert_false(swl_print_toStreamBuf(fWritePtr, NULL, data[0], swl_str_len(data[0])));
    assert_false(swl_print_toStreamBuf(fWritePtr, &argJson, NULL, swl_str_len(data[0])));
    fclose(fWritePtr);

    assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

    remove(TMP_FILE_WRITE_LOC);
    remove(TMP_FILE_ORIG_LOC);

}

#define TEST_FORMAT_1 "%s %u"
#define TEST_ARG_1 "bla", 4
#define TEST_LEN_1 10

#define TEST_FORMAT_2 "%f %s 0x%04x"
#define TEST_ARG_2 8.22, "bla\"", 0x14fe
#define TEST_LEN_2 50

#define TEST_FORMAT_3 "%c %s %c"
#define TEST_ARG_3 '"', "\"\"", '"'
#define TEST_LEN_3 20

#define TEST_TO_STREAM(format, arg, len, printArg) \
    { \
        FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w"); \
        char buffer[len]; \
        memset(buffer, 0, sizeof(buffer)); \
        snprintf(buffer, sizeof(buffer), format, arg); \
        swl_str_addEscapeChar(buffer, sizeof(buffer), (printArg)->charsToEscape, (printArg)->escapeChar); \
        fprintf(fOrigPtr, "%s", buffer); \
        fclose(fOrigPtr); \
        FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w"); \
        assert_true(swl_print_toStreamArgs(fWritePtr, printArg, format, arg)); \
        fclose(fWritePtr); \
        assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC)); \
        remove(TMP_FILE_WRITE_LOC); \
        remove(TMP_FILE_ORIG_LOC); \
    }

static void test_swl_print_toStreamArgs(void** state _UNUSED) {

    swl_print_args_t arg = g_swl_print_dm;

    swl_print_args_t argJson = g_swl_print_json;
    TEST_TO_STREAM(TEST_FORMAT_1, TEST_ARG_1, TEST_LEN_1, &arg);
    TEST_TO_STREAM(TEST_FORMAT_2, TEST_ARG_2, TEST_LEN_2, &arg);
    TEST_TO_STREAM(TEST_FORMAT_3, TEST_ARG_3, TEST_LEN_3, &arg);

    TEST_TO_STREAM(TEST_FORMAT_1, TEST_ARG_1, TEST_LEN_1, &argJson);
    TEST_TO_STREAM(TEST_FORMAT_2, TEST_ARG_2, TEST_LEN_2, &argJson);
    TEST_TO_STREAM(TEST_FORMAT_3, TEST_ARG_3, TEST_LEN_3, &argJson);

    FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
    fclose(fOrigPtr);

    FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
    assert_false(swl_print_toStreamArgs(NULL, &argJson, TEST_FORMAT_1, TEST_ARG_1));
    assert_false(swl_print_toStreamArgs(fWritePtr, NULL, TEST_FORMAT_1, TEST_ARG_1));
    assert_false(swl_print_toStreamArgs(fWritePtr, &argJson, NULL, TEST_ARG_1));
    fclose(fWritePtr);

    assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

    remove(TMP_FILE_WRITE_LOC);
    remove(TMP_FILE_ORIG_LOC);
}

#define TEST_VALUE_TO_STREAM(format, arg, len, printArg, file) \
    { \
        FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w"); \
        assert_true(swl_print_valueToStreamArgs(fOrigPtr, printArg, format, arg)); \
        fclose(fOrigPtr); \
        assert_true(swl_fileUtils_contentMatches(file, TMP_FILE_ORIG_LOC)); \
        remove(TMP_FILE_ORIG_LOC); \
    }


static void test_swl_print_valueToStreamArgs(void** state _UNUSED) {

    swl_print_args_t arg = g_swl_print_dm;
    swl_print_args_t argJson = g_swl_print_json;

    TEST_VALUE_TO_STREAM(TEST_FORMAT_1, TEST_ARG_1, TEST_LEN_1, &arg, "valuePrint/dm1.txt");
    TEST_VALUE_TO_STREAM(TEST_FORMAT_2, TEST_ARG_2, TEST_LEN_2, &arg, "valuePrint/dm2.txt");
    TEST_VALUE_TO_STREAM(TEST_FORMAT_3, TEST_ARG_3, TEST_LEN_3, &arg, "valuePrint/dm3.txt");

    TEST_VALUE_TO_STREAM(TEST_FORMAT_1, TEST_ARG_1, TEST_LEN_1, &argJson, "valuePrint/json1.txt");
    TEST_VALUE_TO_STREAM(TEST_FORMAT_2, TEST_ARG_2, TEST_LEN_2, &argJson, "valuePrint/json2.txt");
    TEST_VALUE_TO_STREAM(TEST_FORMAT_3, TEST_ARG_3, TEST_LEN_3, &argJson, "valuePrint/json3.txt");

    FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
    fclose(fOrigPtr);

    FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
    assert_false(swl_print_toStreamArgs(NULL, &argJson, TEST_FORMAT_1, TEST_ARG_1));
    assert_false(swl_print_toStreamArgs(fWritePtr, NULL, TEST_FORMAT_1, TEST_ARG_1));
    assert_false(swl_print_toStreamArgs(fWritePtr, &argJson, NULL, TEST_ARG_1));
    fclose(fWritePtr);

    assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

    remove(TMP_FILE_WRITE_LOC);
    remove(TMP_FILE_ORIG_LOC);
}

static void s_testElemSize(char* elementStart, bool list, ssize_t expectedVal) {
    ssize_t val = swl_print_getElementSize(swl_print_getDefaultArgs(), elementStart, list);
    assert_int_equal(expectedVal, val);
}


static void test_swl_print_getElementSize(void** state _UNUSED) {
    s_testElemSize("abc]", true, 3);
    s_testElemSize("{abc}]", true, 5);
    s_testElemSize("{abc,[],{a,{b}},c}]", true, 18);
    s_testElemSize("[a,b,c,[a,b,c],d]]", true, 17);

    s_testElemSize("abc,def]", true, 3);
    s_testElemSize("{abc},abba]", true, 5);
    s_testElemSize("{abc,[],{a,{b}},c},{some,other}]", true, 18);
    s_testElemSize("[a,b,c,[a,b,c],d],[no,news]]", true, 17);


    s_testElemSize("abc}", false, 3);
    s_testElemSize("{abc}}", false, 5);
    s_testElemSize("{abc,[],{a,{b}},c}}", false, 18);
    s_testElemSize("[a,b,c,[a,b,c],d]}", false, 17);

    s_testElemSize("abc,def}", false, 3);
    s_testElemSize("{abc},abba}", false, 5);
    s_testElemSize("{abc,[],{a,{b}},c},{some,other}}", false, 18);
    s_testElemSize("[a,b,c,[a,b,c],d],[no,news]}", false, 17);

    s_testElemSize("\\]abc]", true, 5);
    s_testElemSize("\\[\\[abc]", true, 7);
}

static void test_elemScenario(void** state _UNUSED) {
    swl_print_args_t argJson = g_swl_print_json;
    char* parseStr = " \"unk_0_0_0_0\",\n\"DownLinkRateSpec\" : \"unk_0_0_0_0\"}";
    ssize_t val = swl_print_getNextSize(&argJson, parseStr, 1);
    assert_int_equal(val, 35);
}
static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_print_toStreamBuf),
        cmocka_unit_test(test_swl_print_toStreamArgs),
        cmocka_unit_test(test_swl_print_valueToStreamArgs),
        cmocka_unit_test(test_swl_print_getElementSize),
        cmocka_unit_test(test_elemScenario),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
