/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <ftw.h>
#include "test-toolbox/ttb_assert.h"

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/swl_bit.h"

const char* createTestDir = "/tmp/lvl1";
const char* createDirName = "/tmp/lvl1/lvl2/lvl3";


#define NR_TEST_FILES 5

static void test_swl_fileUtils_filesMatch(void** state _UNUSED) {
    char f1[128];
    char f2[128];
    for(int i = 0; i < NR_TEST_FILES; i++) {
        snprintf(f1, sizeof(f1), "testfile_%i_0.txt", i);
        for(int j = 0; j < NR_TEST_FILES; j++) {
            snprintf(f2, sizeof(f2), "testfile_%i_1.txt", j);
            assert_int_equal(i == j && i < NR_TEST_FILES, swl_fileUtils_contentMatches(f1, f2));
        }
    }
}



typedef struct {
    size_t line;
    size_t offset;
    char* buf1;
    char* buf2;
} test_swl_fileUtils_contentTest_t;


test_swl_fileUtils_contentTest_t contentTestDiff[NR_TEST_FILES][NR_TEST_FILES] =
{
    {
        {.line = 0, .offset = 0}, // NA
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
        {.line = 20, .offset = 83},
        {.line = 8, .offset = 28},
    },
    {
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0}, // NA
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
    },
    {
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0}, // NA
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
    },
    {
        {.line = 20, .offset = 83},
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0}, // NA
        {.line = 8, .offset = 28},
    },
    {
        {.line = 8, .offset = 28},
        {.line = 0, .offset = 0},
        {.line = 0, .offset = 0},
        {.line = 8, .offset = 28},
        {.line = 0, .offset = 0}, // NA
    },
};

static void test_swl_fileUtils_getContentDiff(void** state _UNUSED) {
    char f1[128];
    char f2[128];

    char buf1[128];
    char buf2[128];
    size_t line = 0;
    size_t offset = 0;

    for(int i = 0; i < NR_TEST_FILES; i++) {
        snprintf(f1, sizeof(f1), "testfile_%i_0.txt", i);
        for(int j = 0; j < NR_TEST_FILES; j++) {
            ttb_assert_addPrint("Test %i %i", i, j);
            snprintf(f2, sizeof(f2), "testfile_%i_1.txt", j);
            if(i == j) {
                ttb_assert_int_eq(swl_fileUtils_getContentDiff(f1, f2, buf1, buf2, 128, &line, &offset), SWL_RC_ERROR);
            } else {
                ttb_assert_int_eq(swl_fileUtils_getContentDiff(f1, f2, buf1, buf2, 128, &line, &offset), SWL_RC_OK);
                printf("DIFF %zu %zu: \n*'%s'\n*'%s'\n", line, offset, buf1, buf2);
                ttb_assert_int_eq(line, contentTestDiff[i][j].line);
                ttb_assert_int_eq(offset, contentTestDiff[i][j].offset);
                assert_true(strlen(buf1) > 0);
                assert_true(strlen(buf2) > 0);
            }
            ttb_assert_removeLastPrint();
        }
    }
}

static void test_swl_fileUtils_copy(void** state _UNUSED) {
    char* tmpName = "myTest.txt";
    char f1[128];
    for(int i = 0; i < NR_TEST_FILES; i++) {
        for(int j = 0; j < 2; j++) {
            remove(tmpName);
            snprintf(f1, sizeof(f1), "testfile_%i_%i.txt", i, j);
            assert_false(swl_fileUtils_contentMatches(f1, tmpName));
            swl_rc_ne ret = swl_fileUtils_copy(tmpName, f1);
            assert_true(ret == SWL_RC_OK || ret == SWL_RC_DONE);
            assert_true(swl_fileUtils_contentMatches(f1, tmpName));
        }
    }
    remove(tmpName);
}

static void test_swl_fileUtils_copy_sourceDoesNotExist(void** state _UNUSED) {
    // GIVEN a file that does not exist
    const char* doesNotExist = "/tmp/I assume this file does not exist.txt";

    // WHEN copying it
    swl_rc_ne ret = swl_fileUtils_copy("/tmp/something.txt", doesNotExist);

    // THEN an error was returned
    assert_int_equal(SWL_RC_ERROR, ret);
}

static void test_swl_fileUtils_copy_targetInNonExistingDirectory(void** state _UNUSED) {
    // GIVEN a filename in a directory that does not exist
    //       and a file that does exist
    const char* exists = "testfile_0_0.txt";
    assert_true(swl_fileUtils_existsFile(exists));
    assert_false(swl_fileUtils_existsDir("/tmp/nonExistingDir"));

    // WHEN copying the existing file into the non-existing directory
    swl_rc_ne ret = swl_fileUtils_copy("/tmp/nonExistingDir/something.txt", exists);

    // THEN an error was returned
    assert_int_equal(SWL_RC_ERROR, ret);
}

static void test_swl_fileUtils_existsFile(void** state _UNUSED) {
    char* tmpName = tmpnam(NULL);
    char f1[128];
    for(int i = 0; i < NR_TEST_FILES; i++) {
        for(int j = 0; j < 4; j++) {
            snprintf(f1, sizeof(f1), "testfile_%i_%i.txt", i, j);
            assert_int_equal(swl_fileUtils_existsFile(f1), j < 2);
        }
    }
    assert_false(swl_fileUtils_existsFile(tmpName));
    assert_false(swl_fileUtils_existsFile("testDir"));
    assert_true(swl_fileUtils_existsFile("testDir/testFile.txt"));
}

static void test_swl_fileUtils_existsDirectory(void** state _UNUSED) {
    char* tmpName = tmpnam(NULL);

    assert_false(swl_fileUtils_existsDir(tmpName));
    assert_false(swl_fileUtils_existsDir("testfile_0_0.txt"));
    assert_true(swl_fileUtils_existsDir("testDir"));
    assert_false(swl_fileUtils_existsDir("testDir/testFile.txt"));
}


static void s_cleanTestDir() {
    if(swl_fileUtils_existsDir(createDirName)) {
        char command[256];
        snprintf(command, sizeof(command), "rm -rf %s", createTestDir);
        system(command);
    }
}

static void test_swl_fileUtils_mkdirRecursive(void** state _UNUSED) {
    assert_false(swl_fileUtils_existsDir(createDirName));
    assert_true(swl_fileUtils_mkdirRecursive(createDirName));
    assert_true(swl_fileUtils_existsDir(createDirName));

    s_cleanTestDir();

    assert_false(swl_fileUtils_existsDir(createDirName));
    assert_true(swl_fileUtils_mkdirRecursive("//tmp/lvl1//lvl2////lvl3"));
    assert_true(swl_fileUtils_existsDir(createDirName));

    // null directory
    assert_false(swl_fileUtils_mkdirRecursive(""));
    assert_false(swl_fileUtils_mkdirRecursive(NULL));

    //create existing directory
    assert_true(swl_fileUtils_existsDir("testDir/"));
    assert_true(swl_fileUtils_mkdirRecursive("testDir/"));
    assert_true(swl_fileUtils_existsDir("testDir/"));

    //create directory over existing file
    assert_true(swl_fileUtils_existsFile("testDir/testSomeFile"));
    assert_false(swl_fileUtils_existsDir("testDir/testSomeFile"));
    assert_false(swl_fileUtils_mkdirRecursive("testDir/testSomeFile"));
    assert_true(swl_fileUtils_existsFile("testDir/testSomeFile"));
    assert_false(swl_fileUtils_existsDir("testDir/testSomeFile"));
}

static void test_swl_fileUtils_getFileSize(void** state _UNUSED) {
    char* tmpName = tmpnam(NULL);
    // Bad parameters
    assert_int_equal(swl_fileUtils_getFileSize(tmpName), -1);
    assert_int_equal(swl_fileUtils_getFileSize("non_exist_testfile.txt"), -1);
    // Empty file
    assert_int_equal(swl_fileUtils_getFileSize("testDir/testSomeFile"), 0);
    // Nominal case
    assert_int_equal(swl_fileUtils_getFileSize("testfile_0_0.txt"), 1429);

}

static void test_swl_fileUtils_readFileAlloc(void** state _UNUSED) {
    char* buffer;
    // Bad parameters
    assert_int_equal(swl_fileUtils_readFileAlloc(&buffer, NULL, -1), SWL_RC_INVALID_PARAM);
    assert_int_equal(swl_fileUtils_readFileAlloc(&buffer, "non_exist_testfile.txt", -1), SWL_RC_ERROR);
    assert_int_equal(swl_fileUtils_readFileAlloc(NULL, "testfile.txt", -1), SWL_RC_INVALID_PARAM);

    // Empty file
    assert_int_equal(swl_fileUtils_readFileAlloc(&buffer, "testDir/testSomeFile", -1), SWL_RC_OK);
    assert_string_equal(buffer, "");
    assert_int_equal(strlen(buffer), strlen(""));
    free(buffer);

    // Nominal case
    assert_int_equal(swl_fileUtils_readFileAlloc(&buffer, "testfile.txt", -1), SWL_RC_OK);
    assert_string_equal(buffer, "This is a test file.");
    assert_int_equal(strlen(buffer), strlen("This is a test file."));
    free(buffer);

    // Truncated buffer
    assert_int_equal(swl_fileUtils_readFileAlloc(&buffer, "testfile.txt", 7), SWL_RC_OK);
    assert_string_equal(buffer, "This is");
    assert_int_equal(strlen(buffer), strlen("This is"));
    free(buffer);
}

static void test_swl_fileUtils_readFile(void** state _UNUSED) {
    char buffer[4096];
    // Bad parameters
    assert_int_equal(swl_fileUtils_readFile(buffer, sizeof(buffer), NULL), SWL_RC_INVALID_PARAM);
    assert_int_equal(swl_fileUtils_readFile(buffer, sizeof(buffer), "non_exist_testfile.txt"), SWL_RC_ERROR);
    assert_int_equal(swl_fileUtils_readFile(NULL, sizeof(buffer), "testfile.txt"), SWL_RC_INVALID_PARAM);
    assert_int_equal(swl_fileUtils_readFile(buffer, 16, "testfile.txt"), SWL_RC_ERROR);

    // Empty file
    assert_int_equal(swl_fileUtils_readFile(buffer, sizeof(buffer), "testDir/testSomeFile"), SWL_RC_OK);
    assert_string_equal(buffer, "");
    assert_int_equal(strlen(buffer), strlen(""));

    // Nominal case
    assert_int_equal(swl_fileUtils_readFile(buffer, sizeof(buffer), "testfile.txt"), SWL_RC_OK);
    assert_string_equal(buffer, "This is a test file.");
    assert_int_equal(strlen(buffer), strlen("This is a test file."));
}

static void test_swl_fileUtils_writeFile(void** state _UNUSED) {
    char* buffer = "This is a test file.";
    // Bad parameters
    assert_false(swl_fileUtils_writeFile(buffer, NULL));
    assert_false(swl_fileUtils_writeFile(NULL, "testfile_1.txt"));

    // writing in  non existing file
    assert_true(swl_fileUtils_writeFile(buffer, "testfile_1.txt"));
    assert_true(swl_fileUtils_contentMatches("testfile.txt", "testfile_1.txt"));

    // writing in an exisant file
    assert_true(swl_fileUtils_writeFile(buffer, "testfile_1.txt"));
    assert_true(swl_fileUtils_contentMatches("testfile.txt", "testfile_1.txt"));

    unlink("testfile_1.txt");
}

static void test_swl_fileUtils_readLeftoverFileSize(void** state _UNUSED) {
    FILE* test = fopen("testfile_0_0.txt", "r");
    uint32_t size = swl_fileUtils_getFileSize("testfile_0_0.txt");

    printf("file size :%u\n", size);

    assert_true(size > 0);
    assert_int_equal(swl_fileUtils_readLeftoverFileSize(test), size);
    while(size > 0) {
        uint32_t largestBit = swl_bit32_getHighest(size);
        uint32_t readSize = 1 << largestBit;
        fseek(test, readSize, SEEK_CUR);
        size -= readSize;
        printf("file size :%u\n", size);
        assert_int_equal(swl_fileUtils_readLeftoverFileSize(test), size);
    }

    fclose(test);
    assert_int_equal(swl_fileUtils_readLeftoverFileSize(NULL), 0);
}

static void test_swl_fileUtils_writeLine(void** state _UNUSED) {
    char* buffer = "This is a test file.";

    assert_true(swl_fileUtils_writeFile(buffer, "testfile_1.txt"));

    char fileBuf[4096] = {'\0'};
    assert_int_equal(swl_fileUtils_readFile(fileBuf, sizeof(fileBuf), "testfile_1.txt"), SWL_RC_OK);
    assert_string_equal(fileBuf, buffer);

    char* line = "This is a new line.";
    swl_fileUtils_writeLine("testfile_1.txt", NULL, line, 1024);

    assert_int_equal(swl_fileUtils_readFile(fileBuf, sizeof(fileBuf), "testfile_1.txt"), SWL_RC_OK);
    assert_string_equal(fileBuf, "This is a test file.\nThis is a new line.");

    unlink("testfile_1.txt");
}

static void test_swl_fileUtils_writeNewFile(void** state _UNUSED) {
    char* buffer = "This is a test file.";

    assert_true(swl_fileUtils_writeFile(buffer, "testfile_1.txt"));

    char fileBuf[4096] = {'\0'};
    assert_int_equal(swl_fileUtils_readFile(fileBuf, sizeof(fileBuf), "testfile_1.txt"), SWL_RC_OK);
    assert_string_equal(fileBuf, buffer);

    // Append a line on a file that can only contains strlen(buffer).
    char* line = "This is a new line.";
    swl_fileUtils_writeLine("testfile_1.txt", "_3", line, strlen(buffer));

    assert_int_equal(swl_fileUtils_readFile(fileBuf, sizeof(fileBuf), "testfile_1.txt_3"), SWL_RC_OK);
    assert_string_equal(fileBuf, "This is a test file.\nThis is a new line.");

    char* anotherLine = "This is another new line.";
    swl_fileUtils_writeLine("testfile_1.txt_3", NULL, anotherLine, strlen(buffer));

    assert_int_equal(swl_fileUtils_readFile(fileBuf, sizeof(fileBuf), "testfile_1.txt_32"), SWL_RC_OK);
    assert_string_equal(fileBuf, "This is a test file.\nThis is a new line.\nThis is another new line.");

    unlink("testfile_1.txt");
    unlink("testfile_1.txt_3");
    unlink("testfile_1.txt_32");
}

static void test_swl_fileUtils_findStrInFile(void** state _UNUSED) {
    char buffer[128] = {0};

    // Bad parameters
    assert_int_equal(swl_fileUtils_findStrInFile(NULL, 10, "testfile.bin", "toto", true), 0);
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, 0, "testfile.bin", "toto", true), 0);
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), NULL, "toto", true), 0);
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "file.bin", "toto", true), 0);
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", NULL, true), 0);
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "", true), 0);

    // The string beginning with the provided prefix is not found.
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "Bla", true), 0);

    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "Livebox", false), 1);
    assert_string_equal(buffer, "Livebox-2340");

    memset(buffer, 0, sizeof(buffer));
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "Livebox ", false), 1);
    assert_string_equal(buffer, "Livebox 6");

    memset(buffer, 0, sizeof(buffer));
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "Livebox", true), 2);
    assert_string_equal(buffer, "Livebox-2340,Livebox 6");

    // <125 caracters>prefix<10 caracters>
    memset(buffer, 0, sizeof(buffer));
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "Fast53", true), 1);
    assert_string_equal(buffer, "Fast5359_KPN-V12");

    memset(buffer, 0, sizeof(buffer));
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "blabla", false), 1);
    assert_string_equal(buffer, "blabla+++bla+++");

    memset(buffer, 0, sizeof(buffer));
    assert_int_equal(swl_fileUtils_findStrInFile(buffer, sizeof(buffer), "testfile.bin", "blabla", true), 3);
    assert_string_equal(buffer, "blabla+++bla+++,blabla#####,blabla***bla**");
}

static int setup_suite(void** state _UNUSED) {
    s_cleanTestDir();
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    s_cleanTestDir();
    return 0;
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlHex");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_fileUtils_filesMatch),
        cmocka_unit_test(test_swl_fileUtils_getContentDiff),
        cmocka_unit_test(test_swl_fileUtils_copy),
        cmocka_unit_test(test_swl_fileUtils_copy_sourceDoesNotExist),
        cmocka_unit_test(test_swl_fileUtils_copy_targetInNonExistingDirectory),
        cmocka_unit_test(test_swl_fileUtils_existsFile),
        cmocka_unit_test(test_swl_fileUtils_existsDirectory),
        cmocka_unit_test(test_swl_fileUtils_mkdirRecursive),
        cmocka_unit_test(test_swl_fileUtils_getFileSize),
        cmocka_unit_test(test_swl_fileUtils_readFileAlloc),
        cmocka_unit_test(test_swl_fileUtils_readFile),
        cmocka_unit_test(test_swl_fileUtils_writeFile),
        cmocka_unit_test(test_swl_fileUtils_readLeftoverFileSize),
        cmocka_unit_test(test_swl_fileUtils_writeLine),
        cmocka_unit_test(test_swl_fileUtils_writeNewFile),
        cmocka_unit_test(test_swl_fileUtils_findStrInFile),

    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
