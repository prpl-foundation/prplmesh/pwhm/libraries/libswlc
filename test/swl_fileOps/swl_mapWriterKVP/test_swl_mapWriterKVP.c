/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_maps.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/fileOps/swl_mapWriterKVP.h"

#define NR_TEST_FILES 4

#define TMP_FILE_LOC "tmpFile.txt"

typedef struct {
    char* key;
    char* val;
} myTestDataChar;

#define DATASET_CHAR_SIZE 6

static myTestDataChar dataSetChar[DATASET_CHAR_SIZE] = {
    {.key = "foo", .val = "bar"},
    {.key = "test", .val = "1234"},
    {.key = "x", .val = "y"},
    {.key = "1", .val = "2"},
    {.key = "abc,=\\", .val = "cde\\=,"},
    {.key = "abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\abc,=\\",
        .val = "cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,cde\\=,"},
};

#define DATASET_APPEND_CHAR_SIZE 3

static myTestDataChar dataSetAppendChar[DATASET_APPEND_CHAR_SIZE] = {
    {.key = "a", .val = "b"},
    {.key = "2", .val = "3"},
    {.key = "ab,=\\", .val = "cd\\=,"},
};

static const char* dataSetStr = "a=b\n2=3\nab,\\=\\\\=cd\\\\\\=,";

static void test_swl_mapWriterKVP_writeToFile_char(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_CHAR_SIZE; i++) {
        swl_mapChar_add(&map, dataSetChar[i].key, dataSetChar[i].val);
    }

    swl_mapWriterKVP_writeToFile(&map, TMP_FILE_LOC);
    swl_mapChar_cleanup(&map);

    assert_true(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_char.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_char_false.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_int.txt"));
    remove(TMP_FILE_LOC);
}

static void test_swl_mapWriterKVP_updateFile_char(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_CHAR_SIZE; i++) {
        swl_mapChar_add(&map, dataSetChar[i].key, dataSetChar[i].val);
    }

    const char* tmpName = "test1234.txt";
    swl_fileUtils_copy(tmpName, "testUpdatefile_char_pre.txt");
    assert_false(swl_fileUtils_contentMatches(tmpName, "testUpdatefile_char_post.txt"));
    assert_true(swl_mapWriterKVP_updateFile(&map, tmpName));
    assert_true(swl_fileUtils_contentMatches(tmpName, "testUpdatefile_char_post.txt"));

    swl_mapChar_cleanup(&map);
    remove(tmpName);
}

static void test_swl_mapWriterKVP_updateFileExt_char(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_CHAR_SIZE; i++) {
        swl_mapChar_add(&map, dataSetChar[i].key, dataSetChar[i].val);
    }

    const char* tmpName = "test1234.txt";
    swl_fileUtils_copy(tmpName, "testUpdateExtfile_char_pre.txt");
    assert_false(swl_fileUtils_contentMatches(tmpName, "testUpdateExtfile_char_post.txt"));
    assert_true(swl_mapWriterKVP_updateFileExt(&map, tmpName, false));
    assert_true(swl_fileUtils_contentMatches(tmpName, "testUpdateExtfile_char_post.txt"));

    swl_mapChar_t mapToAppend;
    swl_mapChar_init(&mapToAppend);
    for(i = 0; i < DATASET_APPEND_CHAR_SIZE; i++) {
        swl_mapChar_add(&mapToAppend, dataSetAppendChar[i].key, dataSetAppendChar[i].val);
    }
    assert_true(swl_mapWriterKVP_updateFileExt(&mapToAppend, tmpName, true));
    assert_true(swl_fileUtils_contentMatches(tmpName, "testUpdateExtfile_char_append_post.txt"));

    swl_mapChar_cleanup(&map);
    swl_mapChar_cleanup(&mapToAppend);
    remove(tmpName);
}

typedef struct {
    char* key;
    int32_t val;
} myTestDataInt;

#define DATASET_INT_SIZE 5

static myTestDataInt dataSetInt[DATASET_INT_SIZE] = {
    {.key = "foo", .val = 10},
    {.key = "test", .val = -90},
    {.key = "x", .val = 71},
    {.key = "1", .val = 3},
    {.key = "ab", .val = -1586},
};

static void test_swl_mapWriterKVP_writeToFile_int(void** state _UNUSED) {
    swl_mapCharInt32_t map;
    swl_mapCharInt32_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_CHAR_SIZE; i++) {
        swl_mapCharInt32_add(&map, dataSetInt[i].key, dataSetInt[i].val);
    }

    swl_mapWriterKVP_writeToFile(&map, TMP_FILE_LOC);
    swl_mapCharInt32_cleanup(&map);
    assert_true(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_int.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_int_false.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_char.txt"));
    remove(TMP_FILE_LOC);
}

static void test_swl_mapWriterKVP_writeToFptrEsc(void** state _UNUSED) {
    swl_mapCharInt32_t map;
    swl_mapCharInt32_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_CHAR_SIZE; i++) {
        swl_mapCharInt32_add(&map, dataSetInt[i].key, dataSetInt[i].val);
    }
    FILE* fptr = fopen(TMP_FILE_LOC, "w");
    swl_mapWriterKVP_writeToFptrEsc(&map, fptr, "\\", '\\');
    fclose(fptr);
    swl_mapCharInt32_cleanup(&map);
    assert_true(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_int.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_int_false.txt"));
    assert_false(swl_fileUtils_contentMatches(TMP_FILE_LOC, "testFile_char.txt"));
    remove(TMP_FILE_LOC);
}

static void test_swl_mapWriterKVP_writeToStr(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);
    char buffer[128] = {0};

    int i = 0;
    for(i = 0; i < DATASET_APPEND_CHAR_SIZE; i++) {
        swl_mapChar_add(&map, dataSetAppendChar[i].key, dataSetAppendChar[i].val);
    }

    assert_false(swl_mapWriterKVP_writeToStr(NULL, buffer, SWL_ARRAY_SIZE(buffer)));
    assert_false(swl_mapWriterKVP_writeToStr(&map, NULL, SWL_ARRAY_SIZE(buffer)));
    assert_false(swl_mapWriterKVP_writeToStr(&map, buffer, 0));
    assert_false(swl_mapWriterKVP_writeToStr(NULL, NULL, 0));
    assert_true(swl_mapWriterKVP_writeToStr(&map, buffer, SWL_ARRAY_SIZE(buffer)));
    assert_true(swl_str_matches(buffer, dataSetStr));

    swl_mapChar_cleanup(&map);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlHex");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mapWriterKVP_writeToFile_char),
        cmocka_unit_test(test_swl_mapWriterKVP_updateFileExt_char),
        cmocka_unit_test(test_swl_mapWriterKVP_updateFile_char),
        cmocka_unit_test(test_swl_mapWriterKVP_writeToFile_int),
        cmocka_unit_test(test_swl_mapWriterKVP_writeToFptrEsc),
        cmocka_unit_test(test_swl_mapWriterKVP_writeToStr),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
