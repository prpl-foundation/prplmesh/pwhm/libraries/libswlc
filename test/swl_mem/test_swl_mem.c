/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_common_mem.h"

#include "test-toolbox/ttb.h"

void test_swl_mem_testIsMlo(void** state _UNUSED) {
    void* ptr = swl_mem_zalloc(NULL, 100, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    swl_mem_unref(&ptr);
    assert_null(ptr);
}

void test_swl_mem_alloc(void** state _UNUSED) {
    void* ptr = swl_mem_alloc(NULL, 100, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    swl_mem_unref(&ptr);
    assert_null(ptr);
}

void test_swl_mem_ref(void** state _UNUSED) {
    void* ptr = swl_mem_zalloc(NULL, 100, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    void* ptr1 = swl_mem_ref(ptr);
    assert_non_null(ptr1);
    assert_int_equal(swl_mem_nref(ptr), 2);
    assert_int_equal(swl_mem_nref(ptr1), 2);

    swl_mem_unref(&ptr1);
    assert_null(ptr1);

    assert_int_equal(swl_mem_nref(ptr), 1);

    swl_mem_unref(&ptr);
    assert_null(ptr);
}

void test_swl_mem_size(void** state _UNUSED) {
    size_t size = 100;

    void* ptr = swl_mem_alloc(NULL, size, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    assert_int_equal(swl_mem_size(ptr), size);

    swl_mem_unref(&ptr);
    assert_null(ptr);
}

void test_swl_mem_total(void** state _UNUSED) {
    size_t size = 100;

    assert_int_equal(swl_mem_total(), 0);

    void* ptr = swl_mem_alloc(NULL, size, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    assert_int_equal(swl_mem_total(), size);

    void* ptr1 = swl_mem_ref(ptr);
    assert_non_null(ptr1);
    assert_int_equal(swl_mem_nref(ptr), 2);
    assert_int_equal(swl_mem_nref(ptr1), 2);

    assert_int_equal(swl_mem_total(), size);

    void* ptr2 = swl_mem_alloc(NULL, size, NULL);
    assert_non_null(ptr2);
    assert_int_equal(swl_mem_nref(ptr2), 1);

    assert_int_equal(swl_mem_total(), size * 2);

    swl_mem_unref(&ptr1);
    assert_null(ptr1);
    assert_int_equal(swl_mem_nref(ptr), 1);

    assert_int_equal(swl_mem_total(), size * 2);

    swl_mem_unref(&ptr2);
    assert_null(ptr2);

    assert_int_equal(swl_mem_total(), size);

    swl_mem_unref(&ptr);
    assert_null(ptr);

    assert_int_equal(swl_mem_total(), 0);
}

static void s_swl_mem_destroy_h(void* ptr) {
    static bool isSecondCall = false;
    /* get ref in first call (not freed), and let it go in second call */
    if(!isSecondCall) {
        swl_mem_ref(ptr);
        isSecondCall = true;
    }
}

void test_swl_mem_destroy(void** state _UNUSED) {
    size_t size = 100;

    void* ptr = swl_mem_alloc(NULL, size, s_swl_mem_destroy_h);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    assert_int_equal(swl_mem_size(ptr), size);

    void* ptr1 = ptr;
    swl_mem_unref(&ptr1);
    assert_null(ptr1);

    assert_int_equal(swl_mem_nref(ptr), 1);

    swl_mem_unref(&ptr);
    assert_null(ptr);
}

void test_swl_mem_zone(void** state _UNUSED) {
    size_t size = 100;

    swl_memZone_t zone = {
        .name = "test",
        .nrAllocs = 0,
        .size = 0,
    };
    void* ptr = swl_mem_alloc(&zone, size, NULL);
    assert_non_null(ptr);
    assert_int_equal(swl_mem_nref(ptr), 1);

    assert_int_equal(swl_mem_size(ptr), size);
    assert_int_equal(zone.size, size);
    assert_int_equal(zone.nrAllocs, 1);

    swl_mem_unref(&ptr);
    assert_null(ptr);

    assert_int_equal(zone.size, 0);
    assert_int_equal(zone.nrAllocs, 0);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mem_alloc),
        cmocka_unit_test(test_swl_mem_testIsMlo),
        cmocka_unit_test(test_swl_mem_ref),
        cmocka_unit_test(test_swl_mem_size),
        cmocka_unit_test(test_swl_mem_total),
        cmocka_unit_test(test_swl_mem_destroy),
        cmocka_unit_test(test_swl_mem_zone),
    };
    ttb_util_setFilter();

    int rc = cmocka_run_group_tests(tests, NULL, NULL);

    sahTraceClose();
    return rc;
}
