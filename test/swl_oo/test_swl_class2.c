/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/oo/swl_compare_trait.h"

#include "test_swl_class2.h"

#define ME "class2"

bool swl_oo_class2_doSomething1(swl_oo_class1_t* selfi, uint32_t testValue) {
    swl_oo_class2_t* self = (swl_oo_class2_t*) selfi;
    return testValue <= self->data.super.var1 + self->data.var2;
}

bool swl_oo_class2_doSomething2(swl_oo_class2_t* self, uint32_t testValue) {
    return testValue <= self->data.super.var1 + self->data.var2;
}

static swl_comparison_e myCompare(swl_oo_pclass_t* selfi, swl_oo_pclass_t* targeti) {
    swl_oo_class2_t* self = (swl_oo_class2_t*) selfi;
    swl_oo_class2_t* target = (swl_oo_class2_t*) targeti;
    if(self->data.super.var1 != target->data.super.var1) {
        return SWL_COMPARISON_INT(self->data.super.var1, target->data.super.var1);
    }

    return SWL_COMPARISON_INT(self->data.var2, target->data.var2);
}

static swl_compare_trt myCompareTrait = {
    .compare = myCompare,
};


SWL_OO_SUBCLASS_INST(swl_oo_class2_t, swl_oo_class1_t, KEEP_COMMA(
                         .super = {
    .doSomething1 = swl_oo_class2_doSomething1,
    .doSomething1a = NULL,
},
                         .doSomething2 = swl_oo_class2_doSomething2, )
                     , KEEP_COMMA({&swl_compare_trt_uid, (swl_trait_trt*) &myCompareTrait}));
/*
   swl_oo_class2_t_vft swl_oo_class2_t_vfti = {
    .name = "swl_oo_class2_t_vft",
    .size = sizeof(swl_oo_class2_t),
    .vftSize = sizeof(swl_oo_class2_t_fun),
    .superPtr = &swl_oo_class1_t_vfti,
    .vft = {

    },
   };
 */

void swl_oo_class2_t_new(swl_oo_class2_t* self, uint32_t val1, uint32_t val2) {
    swl_oo_class1_t_new((swl_oo_class1_t*) self, val1);
    self->data.var2 = val2;
}

void swl_oo_class2_t_destroy(swl_oo_class2_t* self _UNUSED) {
}
