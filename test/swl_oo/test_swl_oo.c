/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/oo/swl_compare_trait.h"
#include "swl/oo/swl_toString_trait.h"

#include "swl/swl_common.h"
#include "swl/oo/swl_oo.h"
#include "test_swl_class.h"

#define ME "tst_OO"

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

static void test_swl_oo_inherit(void** state _UNUSED) {

    SWL_OO_NEW(swl_oo_class1_t, obj1, 10);

    bool result;

    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class1_t, obj1));
    assert_false(SWL_OO_IS_INSTANCE_OF(swl_oo_class2_t, obj1));
    assert_false(SWL_OO_IS_INSTANCE_OF(swl_oo_class3_t, obj1));

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj1, 10);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj1, 11);
    assert_false(result);

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj1, 10);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj1, 11);
    assert_true(result);

    SWL_OO_NEW(swl_oo_class2_t, obj2, 10, 5);

    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class1_t, obj2));
    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class2_t, obj2));
    assert_false(SWL_OO_IS_INSTANCE_OF(swl_oo_class3_t, obj2));

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj2, 10);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj2, 11);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj2, 16);
    assert_false(result);

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj2, 10);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj2, 11);
    assert_true(result);

    SWL_OO_NEW(swl_oo_class3_t, obj3, 10, 5, 20);

    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class1_t, obj3));
    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class2_t, obj3));
    assert_true(SWL_OO_IS_INSTANCE_OF(swl_oo_class3_t, obj3));

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj3, 10);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj3, 11);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj3, 16);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1, obj3, 26);
    assert_false(result);

    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj3, 10);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj3, 11);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_oo_class1_t, doSomething1a, obj3, 31);
    assert_true(result);

    SWL_OO_DESTROY(obj1);
    SWL_OO_DESTROY(obj2);
    SWL_OO_DESTROY(obj3);
}

static void test_swl_oo_noSupport(void** state _UNUSED) {

    SWL_OO_NEW(swl_oo_class1_t, obj1, 10);
    SWL_OO_CALL(swl_oo_class3_t, doSomething3, obj1, 31);
    SWL_OO_DESTROY(obj1);

    SWL_OO_CALL(swl_oo_class3_t, doSomething3, obj1, 31);

}

static void test_swl_oo_trait(void** state _UNUSED) {
    SWL_OO_NEW(swl_oo_class1_t, obj1a, 10);
    SWL_OO_NEW(swl_oo_class1_t, obj1b, 10);
    SWL_OO_NEW(swl_oo_class1_t, obj1c, 15);

    assert_true(swl_compare_areEqual(obj1a, obj1b));
    assert_true(swl_compare_areEqual(obj1a, obj1a));
    assert_true(swl_compare_areEqual(obj1b, obj1a));
    assert_false(swl_compare_areEqual(obj1a, obj1c));
    assert_false(swl_compare_areEqual(obj1b, obj1c));

    SWL_OO_NEW(swl_oo_class2_t, obj2a, 10, 15);
    SWL_OO_NEW(swl_oo_class2_t, obj2b, 10, 20);
    SWL_OO_NEW(swl_oo_class2_t, obj2c, 15, 20);

    assert_true(swl_compare_areEqual(obj1a, obj2a));
    assert_true(swl_compare_areEqual(obj1c, obj2c));
    assert_false(swl_compare_areEqual(obj1a, obj2c));
    assert_false(swl_compare_areEqual(obj2a, obj1c));

    assert_false(swl_compare_areEqual(obj2a, obj2b));
    assert_false(swl_compare_areEqual(obj2a, obj2c));

    char obj1Str[64] = {0};
    bool retVal = false;
    W_SWL_OO_TRT_CALL(retVal, swl_toString_trt, toString, obj1a, obj1Str);
    assert_true(retVal);
    assert_string_equal(obj1Str, "10");

    memset(obj1Str, 0, sizeof(obj1Str));
    W_SWL_OO_TRT_CALL(retVal, swl_toString_trt, toString, obj2a, obj1Str);
    assert_true(retVal);
    assert_string_equal(obj1Str, "10");

    SWL_OO_DESTROY(obj1a);
    SWL_OO_DESTROY(obj1b);
    SWL_OO_DESTROY(obj1c);
    SWL_OO_DESTROY(obj2a);
    SWL_OO_DESTROY(obj2b);
    SWL_OO_DESTROY(obj2c);

}

static void test_swl_oo_trait_noSupport(void** state _UNUSED) {
    swl_oo_class1_t* obj1 = NULL;

    char obj1Str[64] = {0};
    bool retVal = false;
    W_SWL_OO_TRT_CALL(retVal, swl_toString_trt, toString, obj1, obj1Str);
    assert_false(retVal);
    assert_string_equal(obj1Str, "");
}

static void test_swl_oo_intf(void** state _UNUSED) {
    SWL_OO_NEW(swl_testImpl1a_t, obj1a, 10, 20);
    SWL_OO_NEW(swl_testImpl1b_t, obj1b, 10, 20);


    SWL_OO_NEW(swl_testImpl2a_t, obj2a, 10, 20, 10);
    SWL_OO_NEW(swl_testImpl2b_t, obj2b, 10, 20, 10);

    bool result;

    // Call intf1 obj1



    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1a, 9);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1a, 11);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1a, 21);
    assert_false(result);

    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj1a, 29);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj1a, 31);
    assert_false(result);


    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1b, 9);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1b, 11);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj1b, 21);
    assert_true(result);

    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj1b, 29);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj1b, 31);
    assert_true(result);


    // Call intf1 obj2

    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2a, 9);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2a, 11);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2a, 21);
    assert_false(result);

    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj2a, 19);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj2a, 21);
    assert_false(result);



    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2b, 9);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2b, 11);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1, obj2b, 21);
    assert_true(result);


    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj2b, 9);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf1_i, doIntf1Ext, obj2b, 11);
    assert_true(result);


    // Call intf2 obj2

    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2a, 9);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2a, 11);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2a, 21);
    assert_false(result);


    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2b, 9);
    assert_false(result);
    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2b, 11);
    assert_true(result);
    W_SWL_OO_CALL(result, swl_testIntf2_i, doIntf2, obj2b, 21);
    assert_true(result);


    SWL_OO_DESTROY(obj1a);
    SWL_OO_DESTROY(obj1b);
    SWL_OO_DESTROY(obj2a);
    SWL_OO_DESTROY(obj2b);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_oo_inherit),
        cmocka_unit_test(test_swl_oo_noSupport),
        cmocka_unit_test(test_swl_oo_trait),
        cmocka_unit_test(test_swl_oo_trait_noSupport),
        cmocka_unit_test(test_swl_oo_intf),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
