/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/ttb/swl_ttb.h"
#include "test-toolbox/ttb_util.h"
#include "swl/swl_common_tupleType.h"
#include "swl/swl_common_table.h"
#include "swl/swl_common.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common_type.h"

#define NR_TYPES 3
#define NR_VALUES 4

SWL_TABLE(myTestTable,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"},
              {8851, -289869, "barfoo"},
              )
          );


SWL_TABLE(myTestTableDup,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {1, -200, "test2"},
              {2, -200, "test3"},
              {3, -300, "test3"},
              )
          );


SWL_TABLE_UINT64STR(myTestBitmaskTable, ARR(
                        {0x01, "STRING_1"},
                        {0x02, "STRING_2"},
                        {0x08, "STRING_3"},
                        )
                    );

myTestTableStruct_t zeroValues = {0, 0, NULL};


char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

const char* columnArrays[NR_TYPES] = {
    "1,2,-8852,8851",
    "100,-200,289870,-289869",
    "test1,test2,foobar,barfoo"
};

/**
 * Strings with one offset, for offset testing
 */
const char* columnArraysOffset[NR_TYPES] = {
    "2,-8852,8851,1",
    "-200,289870,-289869,100",
    "test2,foobar,barfoo,test1"
};

#define NR_OTHER_DATA 2

myTestTableStruct_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};

static void test_swl_tuple_getValFromTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 0);
        assert_ptr_equal(&myTestTableValues[i].index, testData);

        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 1);
        assert_ptr_equal(&myTestTableValues[i].key, testData);

        //ptr logic => returned data is pointer itself
        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 2);
        assert_ptr_equal(myTestTableValues[i].val, testData);


        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], myTestTable.nrTuples));
        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], -1));
    }
}

static void test_swl_table_getValue(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            void* data = swl_table_getValue(&myTestTable, i, j);

            assert_true(swl_type_equals(myTestTableTypes[j], srcData, data));

            assert_null(swl_table_getValue(NULL, i, j));
        }

        assert_null(swl_table_getValue(&myTestTable, i, -1));
        assert_null(swl_table_getValue(&myTestTable, i, myTestTableTupleType.nrTypes));
    }
    for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
        assert_null(swl_table_getValue(&myTestTable, -1 - myTestTable.nrTuples, j));
        assert_null(swl_table_getValue(&myTestTable, myTestTable.nrTuples, j));
    }
}

static void test_swl_table_getReference(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], j);
            void* data = swl_table_getReference(&myTestTable, i, j);

            assert_true(swl_type_equals(myTestTableTypes[j], srcData, data));

            assert_null(swl_table_getValue(NULL, i, j));
        }

        assert_null(swl_table_getReference(&myTestTable, i, -1));
        assert_null(swl_table_getReference(&myTestTable, i, myTestTableTupleType.nrTypes));
    }
    for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
        assert_null(swl_table_getReference(&myTestTable, -1 - myTestTable.nrTuples, j));
        assert_null(swl_table_getReference(&myTestTable, myTestTable.nrTuples, j));
    }
}

static void test_swl_table_getTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* tuple = swl_table_getTuple(&myTestTable, i);
        assert_ptr_equal(&myTestTableValues[i], tuple);
    }

    for(ssize_t i = -1 * (ssize_t) myTestTable.nrTuples; i < 0; i++) {
        void* tuple = swl_table_getTuple(&myTestTable, i);
        assert_ptr_equal(&myTestTableValues[i + myTestTable.nrTuples], tuple);
    }


    assert_null(swl_table_getTuple(&myTestTable, -1 - myTestTable.nrTuples));
    assert_null(swl_table_getTuple(&myTestTable, myTestTable.nrTuples));
    assert_null(swl_table_getTuple(NULL, 0));
    assert_null(swl_table_getTuple(NULL, myTestTable.nrTuples - 1));
}

static void test_swl_table_getMatchingValue(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            for(size_t k = 0; k < myTestTableTupleType.nrTypes; k++) {
                void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
                void* tgtData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], k);
                void* data = swl_table_getMatchingValue(&myTestTable, k, j, srcData);

                assert_true(swl_type_equals(myTestTableTypes[k], tgtData, data));

            }
        }
    }

    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            assert_null(swl_table_getMatchingValue(&myTestTable, myTestTable.nrTuples, j, srcData));
            assert_null(swl_table_getMatchingValue(&myTestTable, -1, j, srcData));

            for(size_t k = 0; k < myTestTableTupleType.nrTypes; k++) {
                assert_null(swl_table_getMatchingValue(NULL, k, j, srcData));
            }
        }
    }

    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            for(size_t k = 0; k < myTestTableTupleType.nrTypes; k++) {
                void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &otherData[i], j);
                assert_null(swl_table_getMatchingValue(&myTestTable, k, j, srcData));
            }
        }
    }
}

static void test_swl_table_getMatchingTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            void* tuple = swl_table_getMatchingTuple(&myTestTable, j, srcData);
            assert_ptr_equal(&myTestTableValues[i], tuple);
        }
    }


    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            assert_null(swl_table_getMatchingTuple(&myTestTable, myTestTable.nrTuples, srcData));
            assert_null(swl_table_getMatchingTuple(&myTestTable, -1, srcData));
            assert_null(swl_table_getMatchingTuple(NULL, j, srcData));
        }
    }
}

static void test_swl_table_getMatchingTupleInRange(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            void* tupleIncr = swl_table_getMatchingTupleInRange(&myTestTable, j, srcData,
                                                                (ssize_t) 0, (ssize_t) (-1), (ssize_t) 1);
            void* tupleDecr = swl_table_getMatchingTupleInRange(&myTestTable, j, srcData,
                                                                (ssize_t) (-1), (ssize_t) 0, (ssize_t) (-1));
            // [0, -1, 1] iterates from first to last incrementally
            // [-1, 0, -1] iterates from last to first decrementally
            assert_ptr_equal(&myTestTableValues[i], tupleIncr);
            assert_ptr_equal(tupleIncr, tupleDecr);
            // myTestTable has unique values and as such, iterations in both directions should find
            // the same elements
        }
    }


    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], j);
            assert_null(swl_table_getMatchingTupleInRange(&myTestTable, myTestTable.nrTuples, srcData, 0, -1, 1));
            assert_null(swl_table_getMatchingTupleInRange(&myTestTable, -1, srcData, 0, -1, 1));
            assert_null(swl_table_getMatchingTupleInRange(NULL, j, srcData, 0, -1, 1));
        }
    }
}

#define NR_MASK_VALUES (2 << (NR_TYPES - 1))
#define ALL_MASK (NR_MASK_VALUES - 1)

/**
 * Defining a tuple type using a table
 */
SWL_TABLE(myTestTableMasked,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {2, 200, "test2"},
              {1, 200, "test2"},
              {2, 100, "test2"},
              {1, 100, "test2"},
              {2, 200, "test1"},
              {1, 200, "test1"},
              {2, 100, "test1"},
              {1, 100, "test1"},
              )
          );
myTestTableMaskedStruct_t myBaseVal = {.index = 1, .key = 100, .val = "test1"};

static void s_checkHasNTuples(size_t nrLeft, size_t currOfset, size_t mask) {
    if(nrLeft == 0) {
        return;
    }
    swl_tuple_t* tuple = swl_table_getMatchingTupleByTuple(&myTestTableMasked, &myBaseVal, mask, currOfset);
    assert_true(swl_tupleType_equalsByMask(&myTestTableTupleType, &myBaseVal, tuple, mask));
    size_t index = ((void*) tuple - (void*) myTestTableMaskedValues) / sizeof(myTestTableMaskedStruct_t);
    assert_true((index & mask) == mask);

    for(size_t i = 0; i < NR_TYPES; i++) {
        swl_typeData_t* data = swl_table_getMatchingValueByTuple(&myTestTableMasked, i, &myBaseVal, mask, currOfset);
        assert_ptr_equal(data, swl_tupleType_getValue(&myTestTableTupleType, &myTestTableMaskedValues[index], i));
    }

    s_checkHasNTuples(nrLeft - 1, index + 1, mask);
}

static void test_swl_table_getMatchingXByTuple(void** state _UNUSED) {
    for(size_t i = 0; i < NR_MASK_VALUES; i++) {
        size_t nrBitUnset = NR_TYPES - swl_bit64_getNrSet(i);
        size_t nrMatches = 1 << (nrBitUnset);
        s_checkHasNTuples(nrMatches, 0, i);
    }
}

static void test_swl_table_str2str(void** state _UNUSED) {
    // GIVEN a const char (to force that API can deal with constness)
    const char* lundi = "lundi";

    // WHEN defining a table using the SWL_TABLE_STR2STR macro
    SWL_TABLE_STR2STR(weekdaysFrenchDutch, ARR(
                          {"lundi", "maandag"},
                          {"mardi", "dinsdag"},
                          {"mercredi", "woensdag"},
                          {"jeudi", "donderdag"},
                          {"vendredi", "vrijdag"},
                          {"samedi", "zaterdag"},
                          {"dimanche", "zondag"})
                      );

    // THEN the C compiler does not choke on the preprocessor's output.
    // AND THEN we can use the table, even with const chars.
    assert_string_equal("maandag", swl_table_getMatchingValue(&weekdaysFrenchDutch, 1, 0, lundi));
    assert_string_equal("mardi", swl_table_getMatchingValue(&weekdaysFrenchDutch, 0, 1, "dinsdag"));
}


static void test_swl_table_equals(void** state _UNUSED) {
    swl_table_t testTable = {0};
    swl_table_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);

    for(size_t i = 0; i < NR_VALUES; i++) {
        myTestTableStruct_t* val1 = swl_table_getTuple(&testTable, i);
        myTestTableStruct_t* val2 = swl_table_getTuple(&myTestTable, i);
        memcpy(val1, val2, sizeof(myTestTableStruct_t));
        val1->val = strdup(val2->val);
    }

    assert_true(swl_table_equals(&testTable, &myTestTable));

    for(int i = 0; i < NR_VALUES; i++) {
        for(int j = 0; j < NR_TYPES; j++) {
            swl_type_t* type = myTestTableTypes[j];
            char buffer[type->size];
            void* data = &buffer;
            void* val = swl_table_getReference(&testTable, i, j);
            void* testVal = swl_tupleType_getReference(&myTestTableTupleType, &otherData[0], j);
            memcpy(data, val, type->size);
            memcpy(val, testVal, type->size);
            assert_false(swl_table_equals(&testTable, &myTestTable));
            memcpy(val, data, type->size);
            assert_true(swl_table_equals(&testTable, &myTestTable));
        }
    }

    swl_table_destroy(&testTable);
}


static void test_swl_table_setValue(void** state _UNUSED) {
    swl_table_t testTable = {0};
    swl_table_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);

    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {

            assert_true(swl_type_equals(myTestTableTypes[j],
                                        swl_table_getValue(&testTable, i, j),
                                        swl_tupleType_getValue(&myTestTableTupleType, &zeroValues, j)));
            assert_false(swl_type_equals(myTestTableTypes[j],
                                         swl_table_getValue(&testTable, i, j),
                                         swl_table_getValue(&myTestTable, i, j)));

            swl_table_setValue(&testTable, i, j, swl_table_getValue(&myTestTable, i, j));

            assert_false(swl_type_equals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                         swl_tupleType_getValue(&myTestTableTupleType, &zeroValues, j)));
            assert_true(swl_type_equals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                        swl_table_getValue(&myTestTable, i, j)));

        }
    }

    assert_true(swl_table_equals(&myTestTable, &testTable));

    // Inverse
    swl_table_cleanElements(&testTable);

    for(ssize_t i = -1; i >= -1 * (ssize_t) myTestTable.nrTuples; i--) {
        ttb_assert_addPrint("tuple %zi", i);
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            ttb_assert_addPrint("type %zu", j);
            swl_ttb_assertTypeEquals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                     swl_tupleType_getValue(&myTestTableTupleType, &zeroValues, j));
            swl_ttb_assertTypeNotEquals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                        swl_table_getValue(&myTestTable, i, j));

            swl_table_setValue(&testTable, i, j, swl_table_getValue(&myTestTable, i, j));

            swl_ttb_assertTypeNotEquals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                        swl_tupleType_getValue(&myTestTableTupleType, &zeroValues, j));
            swl_ttb_assertTypeEquals(myTestTableTypes[j], swl_table_getValue(&testTable, i, j),
                                     swl_table_getValue(&myTestTable, i, j));
            ttb_assert_removeLastPrint();
        }
        ttb_assert_removeLastPrint();
    }

    assert_true(swl_table_equals(&myTestTable, &testTable));

    swl_table_destroy(&testTable);
}



static void test_swl_table_setTuple(void** state _UNUSED) {
    swl_table_t testTable = {0};
    swl_table_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);

    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        assert_true(swl_tupleType_equals(&myTestTableTupleType, &zeroValues, swl_table_getTuple(&testTable, i)));
        assert_false(swl_tupleType_equals(&myTestTableTupleType, swl_table_getTuple(&myTestTable, i), swl_table_getTuple(&testTable, i)));

        swl_table_setTuple(&testTable, i, swl_table_getTuple(&myTestTable, i));
        assert_false(swl_tupleType_equals(&myTestTableTupleType, &zeroValues, swl_table_getTuple(&testTable, i)));
        assert_true(swl_tupleType_equals(&myTestTableTupleType, swl_table_getTuple(&myTestTable, i), swl_table_getTuple(&testTable, i)));
    }
    assert_true(swl_table_equals(&myTestTable, &testTable));
    swl_table_cleanElements(&testTable);

    for(ssize_t i = -1; i >= -1 * (ssize_t) myTestTable.nrTuples; i--) {
        assert_true(swl_tupleType_equals(&myTestTableTupleType, &zeroValues, swl_table_getTuple(&testTable, i)));
        assert_false(swl_tupleType_equals(&myTestTableTupleType, swl_table_getTuple(&myTestTable, i), swl_table_getTuple(&testTable, i)));

        swl_table_setTuple(&testTable, i, swl_table_getTuple(&myTestTable, i));
        assert_false(swl_tupleType_equals(&myTestTableTupleType, &zeroValues, swl_table_getTuple(&testTable, i)));
        assert_true(swl_tupleType_equals(&myTestTableTupleType, swl_table_getTuple(&myTestTable, i), swl_table_getTuple(&testTable, i)));
    }

    assert_true(swl_table_equals(&myTestTable, &testTable));

    swl_table_destroy(&testTable);
}


static void test_swl_tableUInt64Char_maskToCharSep(void** state _UNUSED) {

    char result[32] = {0};
    swl_mask64_m mymask = 0;
    int output = 0;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result, sizeof(result), mymask, ',');
    assert_int_equal(output, 0);
    assert_string_equal(result, "");
    mymask |= 0x01;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result, sizeof(result), mymask, ',');
    assert_string_equal(result, "STRING_1");
    assert_int_equal(output, 8);
    mymask |= 0x02;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result, sizeof(result), mymask, ',');
    assert_string_equal(result, "STRING_1,STRING_2");
    assert_int_equal(output, 17);
    mymask |= 0x08;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result, sizeof(result), mymask, ',');
    assert_string_equal(result, "STRING_1,STRING_2,STRING_3");
    assert_int_equal(output, 26);
    mymask = 0x02;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result, sizeof(result), mymask, ',');
    assert_string_equal(result, "STRING_2");
    assert_int_equal(output, 8);
    char result_small_buffer[4] = {0};
    mymask |= 0x01;
    output = swl_tableUInt64Char_maskToCharSep(&myTestBitmaskTable, result_small_buffer, sizeof(result_small_buffer), mymask, ',');
    assert_string_equal(result_small_buffer, "STR");
    assert_int_equal(output, 17);

}


static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tuple_getValFromTuple),
        cmocka_unit_test(test_swl_table_getValue),
        cmocka_unit_test(test_swl_table_getReference),
        cmocka_unit_test(test_swl_table_getTuple),
        cmocka_unit_test(test_swl_table_getMatchingValue),
        cmocka_unit_test(test_swl_table_getMatchingTuple),
        cmocka_unit_test(test_swl_table_getMatchingTupleInRange),
        cmocka_unit_test(test_swl_table_getMatchingXByTuple),
        cmocka_unit_test(test_swl_table_str2str),
        cmocka_unit_test(test_swl_table_equals),
        cmocka_unit_test(test_swl_table_setValue),
        cmocka_unit_test(test_swl_table_setTuple),
        cmocka_unit_test(test_swl_tableUInt64Char_maskToCharSep),
    };
    int rc = 0;
    ttb_util_setFilter();
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}

