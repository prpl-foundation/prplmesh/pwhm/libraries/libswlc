/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"

#include "test_swlc_type_testCommon.h"



#define NR_BOOL_TEST_VALUES 5
bool boolBase[NR_BOOL_TEST_VALUES] = {true, false, false, true, false};
size_t boolCount[NR_BOOL_TEST_VALUES] = {2, 3, 3, 2, 3};
#define testStrBool "True,False,False,True,False"
#define testStr2Bool "True;;False;;False;;True;;False"
const char* testStrValuesBool[NR_BOOL_TEST_VALUES] = {
    "True", "False", "False", "True", "False"
};


#define NR_INT_TEST_VALUES 7


int8_t enum_base[NR_INT_TEST_VALUES] = {0, 111, 2, -3, 21, 2, 4};
size_t int8_count[NR_INT_TEST_VALUES] = {1, 1, 2, 1, 1, 2, 1};
int8_t int8_notContains[NR_INT_TEST_VALUES] = {-1, 5, 12, -21, 114, -2, 8};
#define testStrInt8 "0,111,2,-3,21,2,4"
#define testStr2Int8 "0;;111;;2;;-3;;21;;2;;4"
const char* testStrValuesAuto[NR_INT_TEST_VALUES] = {
    "0", "111", "2", "-3", "21", "2", "4"
};

int32_t int32_base[NR_INT_TEST_VALUES] = {0, 1111, 2, -3, 2201, 2, 4};
size_t int32_count[NR_INT_TEST_VALUES] = {1, 1, 2, 1, 1, 2, 1};
int32_t int32_notContains[NR_INT_TEST_VALUES] = {-1, 5, 12, -2201, 1014, -2, 8};
#define testStrInt32 "0,1111,2,-3,2201,2,4"
#define testStr2Int32 "0;;1111;;2;;-3;;2201;;2;;4"
const char* testStrValuesInt32[NR_INT_TEST_VALUES] = {
    "0", "1111", "2", "-3", "2201", "2", "4"
};

#define NR_DOUBLE_TEST_VALUES 8

double double_base[NR_DOUBLE_TEST_VALUES] = {0.0, 1.1231, 2.124124, -3.123, 2201.2, 2.124124, 4, 7899986};
size_t double_count[NR_DOUBLE_TEST_VALUES] = {1, 1, 2, 1, 1, 2, 1, 1};
double double_notContains[NR_DOUBLE_TEST_VALUES] = {-1.125, 556.0, 12.1212, -2201.111545, 1014.125, -2.2, 8.888888, 1231};
#define testStrDouble "0,1.1231,2.124124,-3.123,2201.2,2.124124,4,7899986"
#define testStr2Double "0;;1.1231;;2.124124;;-3.123;;2201.2;;2.124124;;4;;7899986"
const char* testStrValuesDouble[NR_DOUBLE_TEST_VALUES] = {
    "0", "1.1231", "2.124124", "-3.123", "2201.2", "2.124124", "4", "7899986"
};
const char* testVarStrValuesDouble[NR_DOUBLE_TEST_VALUES] = {
    "0.000000e+00", "1.123100e+00", "2.124124e+00", "-3.123000e+00", "2.201200e+03", "2.124124e+00", "4.000000e+00", "7.899986e+06"
};


#define NR_CHAR_TEST_VALUES 5
char* char_base[NR_CHAR_TEST_VALUES] = {"abc", "def", "xyz", "abc", "4ava"};
size_t monoCount[NR_CHAR_TEST_VALUES] = {2, 1, 1, 2, 1};
char* notContains[NR_CHAR_TEST_VALUES] = {"abd", "3ava", "zyx", "456", "1234"};
#define monoStrChar "abc,def,xyz,abc,4ava"
#define monoStr2Char "abc;;def;;xyz;;abc;;4ava"
const char* monotestStrValuesChar[NR_CHAR_TEST_VALUES] = {
    "abc", "def", "xyz", "abc", "4ava"
};


#define NR_CHARBUF_TEST_VALUES 5
#define CHAR_BUF18_SIZE 18
char charBuf18_base[NR_CHAR_TEST_VALUES][CHAR_BUF18_SIZE] = {"abc", "def", "xyz", "abc", "4ava"};
char charBuf18_notContains[NR_CHAR_TEST_VALUES][CHAR_BUF18_SIZE] = {"abd", "3ava", "zyx", "456", "1234"};

#define testStrCharBuf18 "abc,def,xyz,abc,4ava"
#define testStr2CharBuf18 "abc;;def;;xyz;;abc;;4ava"
const char* testStrValuesCharBuf18[NR_CHAR_TEST_VALUES] = {
    "abc", "def", "xyz", "abc", "4ava"
};


#define CHAR_BUF4_SIZE 4
char charBuf4_base[NR_CHAR_TEST_VALUES][CHAR_BUF4_SIZE] = {"abc", "def", "xyz", "abc", "4av"};
char charBuf4_notContains[NR_CHAR_TEST_VALUES][CHAR_BUF4_SIZE] = {"abd", "3av", "zyx", "456", "124"};

size_t charBuf_count[NR_CHAR_TEST_VALUES] = {2, 1, 1, 2, 1};
#define testStrCharBuf4 "abc,def,xyz,abc,4av"
#define testStr2CharBuf4 "abc;;def;;xyz;;abc;;4av"
const char* testStrValuesCharBuf4[NR_CHAR_TEST_VALUES] = {
    "abc", "def", "xyz", "abc", "4av"
};

#define NR_VOIDPTR_TEST_VALUES 6

void* voidPtr_base[NR_VOIDPTR_TEST_VALUES] = {
    NULL,
    SWL_TYPE_CAST2VOIDPTR(0x15a604),
    SWL_TYPE_CAST2VOIDPTR(0x15a600),
    SWL_TYPE_CAST2VOIDPTR(0x4b04ff8),
    0,
    SWL_TYPE_CAST2VOIDPTR(65535)
};
size_t voidPtr_count[NR_VOIDPTR_TEST_VALUES] = {2, 1, 1, 1, 2, 1};
void* voidPtr_notContains[NR_VOIDPTR_TEST_VALUES] = {
    SWL_TYPE_CAST2VOIDPTR(255),
    SWL_TYPE_CAST2VOIDPTR(0xff),
    SWL_TYPE_CAST2VOIDPTR(0xffffff),
    SWL_TYPE_CAST2VOIDPTR(-1L),
    SWL_TYPE_CAST2VOIDPTR(-3),
    SWL_TYPE_CAST2VOIDPTR(0x12345678)
};
#define testStrVoidPtr "0,0x15a604,0x15a600,0x4b04ff8,0,0xffff"
#define testStr2VoidPtr "0;;0x15a604;;0x15a600;;0x4b04ff8;;0;;0xffff"
const char* testStrValuesVoidPtr[NR_VOIDPTR_TEST_VALUES] = {
    "0", "0x15a604", "0x15a600", "0x4b04ff8", "0", "0xffff"
};


#define NR_DATA_ENTRIES 8
swlc_type_testCommonData_t ouiTestData[NR_DATA_ENTRIES] = {
    {
        .name = "bool",
        .testType = &gtSwl_type_bool,
        .refType = &gtSwl_type_boolPtr.type,
        .serialStr = testStrBool,
        .serialStr2 = testStr2Bool,
        .strValues = testStrValuesBool,
        .baseValues = &boolBase,
        .valueCount = boolCount,
        .notContains = NULL,
        .nrTestValues = NR_BOOL_TEST_VALUES,
    },
    {
        .name = "int8",
        .testType = &gtSwl_type_int8,
        .refType = &gtSwl_type_int8Ptr.type,
        .serialStr = testStrInt8,
        .serialStr2 = testStr2Int8,
        .strValues = testStrValuesAuto,
        .baseValues = &enum_base,
        .valueCount = int8_count,
        .notContains = &int8_notContains,
        .nrTestValues = NR_INT_TEST_VALUES,
    },
    {
        .name = "int32",
        .testType = swl_type_int32,
        .refType = &gtSwl_type_int32Ptr.type,
        .serialStr = testStrInt32,
        .serialStr2 = testStr2Int32,
        .strValues = testStrValuesInt32,
        .baseValues = &int32_base,
        .valueCount = int32_count,
        .notContains = &int32_notContains,
        .nrTestValues = NR_INT_TEST_VALUES,
    },
    {
        .name = "double",
        .testType = swl_type_double,
        .refType = &gtSwl_type_doublePtr.type,
        .serialStr = testStrDouble,
        .serialStr2 = testStr2Double,
        .strValues = testStrValuesDouble,
        .baseValues = &double_base,
        .valueCount = double_count,
        .notContains = &double_notContains,
        .nrTestValues = NR_DOUBLE_TEST_VALUES,
    },
    {
        .name = "charPtr",
        .testType = swl_type_charPtr,
        .serialStr = monoStrChar,
        .serialStr2 = monoStr2Char,
        .strValues = monotestStrValuesChar,
        .baseValues = &char_base,
        .valueCount = monoCount,
        .notContains = &notContains,
        .nrTestValues = NR_CHAR_TEST_VALUES,
    },
    {
        .name = "charBuf4",
        .testType = swl_type_charBuf4,
        .serialStr = testStrCharBuf4,
        .serialStr2 = testStr2CharBuf4,
        .strValues = testStrValuesCharBuf4,
        .baseValues = &charBuf4_base,
        .valueCount = charBuf_count,
        .notContains = &charBuf4_notContains,
        .nrTestValues = NR_CHARBUF_TEST_VALUES,
    },
    {
        .name = "charBuf18",
        .testType = swl_type_charBuf18,
        .serialStr = testStrCharBuf18,
        .serialStr2 = testStr2CharBuf18,
        .strValues = testStrValuesCharBuf18,
        .baseValues = &charBuf18_base,
        .valueCount = charBuf_count,
        .notContains = &charBuf18_notContains,
        .nrTestValues = NR_CHARBUF_TEST_VALUES,
    },
    {
        .name = "voidPtr",
        .testType = swl_type_voidPtr,
        .serialStr = testStrVoidPtr,
        .serialStr2 = testStr2VoidPtr,
        .strValues = testStrValuesVoidPtr,
        .baseValues = &voidPtr_base,
        .valueCount = voidPtr_count,
        .notContains = &voidPtr_notContains,
        .nrTestValues = NR_VOIDPTR_TEST_VALUES,
        .noDeSerial = true,
        .skipFromFile = true,
    },
};


int main(int argc _UNUSED, char* argv[] _UNUSED) {

    int rc = 0;


    for(int i = 0; i < NR_DATA_ENTRIES; i++) {
        rc = runCommonTypeTest(&ouiTestData[i]);
        if(rc != 0) {
            printf("Err %s - %i (%u)\n", ouiTestData[i].name, rc, i);
        }
    }
    //rc = runCommonTypeTest(&ouiTestData[1]);

    sahTraceClose();
    return rc;
}
