/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_swlc_type_testCommon.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/ttb/swl_ttb.h"

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_math.h"
#include "swl/swl_string.h"
#include "swl/fileOps/swl_fileUtils.h"

swlc_type_testCommonData_t* curData = NULL;
swl_type_t* curType = NULL;
swl_type_t* baseType = NULL;

#define TMP_FILE_ORIG_LOC "tmpFileOrig.txt"
#define TMP_FILE_WRITE_LOC "tmpFileWrite.txt"

static swl_typeData_t* s_curElToPtr(swl_typeEl_t* ptr) {
    if(curType->typeFun->isValue) {
        return ptr;
    } else {
        return *(void**) ptr;
    }
}

static swl_typeData_t* s_baseElToPtr(swl_typeEl_t* ptr) {
    if(baseType->typeFun->isValue) {
        return ptr;
    } else {
        return *(swl_typeData_t**) ptr;
    }
}

static swl_typeData_t* s_arrBaseVal(void* array, uint32_t index) {
    return s_baseElToPtr(array + baseType->size * index);
}

static swl_typeData_t* s_arrCurVal(void* array, uint32_t index) {
    return s_curElToPtr(array + curType->size * index);
}

static int setup_suite(void** state) {
    (void) state;
    // Need to duplicate the values for base in equals, otherwise tests may not be valid.
    // Note that we cannot depend on just writing multiple lines of the same string, as compiler may optimize.
    curData->equalsValues = (void**) calloc(curType->size, curData->nrTestValues);
    if(curType->typeFun->isValue) {
        memcpy(curData->equalsValues, curData->baseValues, curType->size * curData->nrTestValues);
    } else {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            *((void**) curData->equalsValues + i) = swl_type_copy(curType, s_arrBaseVal(curData->baseValues, i));
        }
    }

    if(curData->notContains != NULL) {
        curData->ncValues = (void**) calloc(curType->size, curData->nrTestValues);

        if(curType->typeFun->isValue) {
            memcpy(curData->ncValues, curData->notContains, curType->size * curData->nrTestValues);
        } else {
            for(uint32_t i = 0; i < curData->nrTestValues; i++) {
                *((void**) curData->ncValues + i) = swl_type_copy(curType, s_arrBaseVal(curData->notContains, i));
            }
        }
    }

    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    if(!curType->typeFun->isValue) {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            free(*((void**) curData->equalsValues + i));
        }
    }
    free(curData->equalsValues);

    if(curData->ncValues != NULL) {
        if(!curType->typeFun->isValue) {
            for(uint32_t i = 0; i < curData->nrTestValues; i++) {
                free(*((void**) curData->ncValues + i));
            }
        }
        free(curData->ncValues);
    }
    return 0;
}


static void test_valueSetup(void** state _UNUSED) {
    // Test whether the values in base are actually different from the values in equals.
    if(curType->typeFun->isValue) {
        assert_ptr_not_equal(curData->baseValues, curData->equalsValues);
    } else {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            assert_ptr_not_equal(s_arrBaseVal(curData->baseValues, i), s_arrBaseVal(curData->equalsValues, i));
        }
    }

}

static void test_swl_type_equals(void** state _UNUSED) {
    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_equals(curType, s_arrBaseVal(curData->baseValues, i), s_arrCurVal(curData->equalsValues, i)));
        if(curData->notContains != NULL) {
            assert_false(swl_type_equals(curType, s_arrBaseVal(curData->baseValues, i), s_arrBaseVal(curData->notContains, i)));
            assert_false(swl_type_equals(curType, s_arrCurVal(curData->equalsValues, i), s_arrBaseVal(curData->notContains, i)));
        }
    }
}

static void test_swl_type_copy(void** state _UNUSED) {
    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        swl_typeData_t* curBase = s_arrBaseVal(curData->baseValues, i);
        swl_typeData_t* duplicate = swl_type_copy(curType, curBase);
        assert_true(swl_type_equals(curType, curBase, duplicate));
        assert_true(duplicate != curBase);
        swl_type_cleanupPtr(curType, &duplicate);
        assert_null(duplicate);
    }
}

static void test_swl_type_copyCleanup(void** state _UNUSED) {
    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        char data[curType->size];
        void* dataPtr = &data;
        memset(data, 0, curType->size);

        swl_typeData_t* curBase = s_arrBaseVal(curData->baseValues, i);

        swl_type_copyTo(curType, dataPtr, curBase);
        assert_true(swl_type_equals(curType, curBase, swl_type_toPtr(curType, dataPtr)));

        swl_type_cleanup(curType, data);

        char zeroData[curType->size];
        memset(zeroData, 0, curType->size);
        assert_memory_equal(data, zeroData, curType->size);
    }
}


static void test_swl_type_tofromChar(void** state _UNUSED) {

    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        swl_typeData_t* curBase = s_arrBaseVal(curData->baseValues, i);
        ttb_assert_addPrint("%u %s", i, curData->strValues[i]);

        char buffer[128] = {0};
        memset(buffer, 0, sizeof(buffer));
        ssize_t result = swl_type_toChar(curType, buffer, sizeof(buffer), curBase);
        ttb_assert_addPrint("%s", buffer);
        ttb_assert_int_eq(result, swl_str_len(curData->strValues[i]));
        ttb_assert_removeLastPrint();

        assert_string_equal(buffer, curData->strValues[i]);

        char bufDataBuffer[curType->size];
        memset(bufDataBuffer, 0, curType->size);
        void* bufData = &bufDataBuffer;

        swl_type_fromChar(curType, bufData, curData->strValues[i]);
        if(!curData->noDeSerial) {
            swl_ttb_assertTypeEquals(curType, s_curElToPtr(bufData), curBase);
        }

        swl_type_cleanup(curType, bufData);

        assert_int_equal(swl_type_toChar(NULL, buffer, sizeof(buffer), curBase), -EINVAL);
        assert_int_equal(swl_type_toChar(curType, NULL, sizeof(buffer), curBase), -EINVAL);
        assert_int_equal(swl_type_toChar(curType, buffer, 0, NULL), -EINVAL);

        ttb_assert_removeLastPrint();
    }


    swl_typeEl_t* curPtr = curData->baseValues;
    char bufDataBuffer[curType->size];
    memset(bufDataBuffer, 0, curType->size);
    void* bufData = &bufDataBuffer;
    for(i = 0; i < curData->nrTestValues; i++) {
        swl_type_fromChar(curType, bufData, curData->strValues[i]);

        if(!curData->noDeSerial) {
            swl_ttb_assertTypeEquals(curType, SWL_TYPE_TO_PTR(curType, bufData), SWL_TYPE_TO_PTR(baseType, curPtr));
        }
        curPtr += baseType->size;
    }

    swl_type_cleanup(curType, bufData);

    ttb_assert_clearPrint();
}


static void test_swl_type_toCString(void** state _UNUSED) {
    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        swl_typeData_t* curPtr = s_arrBaseVal(curData->baseValues, i);
        char* result = swl_type_toCString(curType, curPtr);
        assert_non_null(result);
        assert_string_equal(result, curData->strValues[i]);
        assert_null(swl_type_toCString(NULL, curPtr));
        free(result);
    }

    assert_null(swl_type_toCString(curType, NULL));
}

static void test_swl_type_toFile(void** state _UNUSED) {

    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {
        FILE* fOrigPtr = fopen(TMP_FILE_ORIG_LOC, "w");
        fprintf(fOrigPtr, "%s", curData->strValues[i]);
        fclose(fOrigPtr);

        FILE* fWritePtr = fopen(TMP_FILE_WRITE_LOC, "w");
        assert_true(swl_type_toFile(curType, fWritePtr, s_arrCurVal(curData->equalsValues, i), NULL));
        fclose(fWritePtr);

        assert_true(swl_fileUtils_contentMatches(TMP_FILE_WRITE_LOC, TMP_FILE_ORIG_LOC));

        if(curData->skipFromFile) {
            continue;
        }

        char bufDataBuffer[curType->size];
        memset(bufDataBuffer, 0, curType->size);
        swl_typeEl_t* bufData = &bufDataBuffer;
        FILE* fReadPtr = fopen(TMP_FILE_WRITE_LOC, "r");
        assert_true(swl_type_fromFile(curType, bufData, fReadPtr, 0, NULL));
        fclose(fReadPtr);
        swl_ttb_assertTypeEquals(curType, s_arrBaseVal(curData->baseValues, i), s_curElToPtr(bufData));

        swl_type_cleanup(curType, bufData);

        remove(TMP_FILE_WRITE_LOC);
        remove(TMP_FILE_ORIG_LOC);
    }
}

/**
 * Test edge conditions (error / buffer of zero size, buffer of half size)
 */
static void test_swl_type_toCharOther(void** state _UNUSED) {

    size_t i = 0;
    for(i = 0; i < curData->nrTestValues; i++) {

        swl_typeData_t* curPtr = s_arrBaseVal(curData->baseValues, i);

        size_t strLen = strlen(curData->strValues[i]);
        size_t halfSize = (strLen / 2) + 1; //add one for zero end
        char buffer[halfSize];
        memset(buffer, 0, sizeof(buffer));
        ssize_t result = swl_type_toChar(curType, buffer, sizeof(buffer), curPtr);
        assert_int_equal(result, strLen);

        char testBuffer[halfSize];
        memset(testBuffer, 0, sizeof(testBuffer));
        snprintf(testBuffer, sizeof(testBuffer), "%s", curData->strValues[i]);


        assert_string_equal(buffer, testBuffer);

        // zero buffer size
        result = swl_type_toChar(curType, buffer, 0, curPtr);
        assert_int_equal(result, strLen);


    }
}

static void test_swl_type_toBuf32(void** state _UNUSED) {
    if(curData->noToBuf32) {
        return;
    }

    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_typeData_t* curPtr = s_arrBaseVal(curData->baseValues, i);
        assert_string_equal(swl_type_toBuf32(curType, curPtr).buf, curData->strValues[i]);
    }
}

static void test_swl_type_getFlag(void** state _UNUSED) {
    swl_typeFlag_e flag = swl_type_getFlag(curType);
    swl_ttb_assertEnumNotEquals(flag, SWL_TYPE_FLAG_REF, swl_typeFlag_str);
    swl_ttb_assertEnumNotEquals(flag, SWL_TYPE_FLAG_MAX, swl_typeFlag_str);
    if(curType != baseType) {
        swl_typeFlag_e baseFlag = swl_type_getFlag(baseType);
        swl_ttb_assertEnumEquals(flag, baseFlag, swl_typeFlag_str);
    }
}

static void test_swl_type_arrayToChar(void** state _UNUSED) {
    char buffer[256];
    swl_type_arrayToChar(curType, buffer, sizeof(buffer), curData->equalsValues, curData->nrTestValues);
    assert_string_equal(buffer, curData->serialStr);

    swl_type_arrayToCharSep(curType, buffer, sizeof(buffer), curData->equalsValues, curData->nrTestValues, ";;");
    assert_string_equal(buffer, curData->serialStr2);
}

static void test_swl_type_arrayFromChar(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];

    size_t nrValues = swl_type_arrayFromChar(curType, buffer, curData->nrTestValues, curData->serialStr);
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }


    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_arrCurVal(buffer, i), s_arrCurVal(curData->equalsValues, i)));
        swl_type_cleanup(curType, buffer + curType->size * i);
    }
}

static void test_swl_type_arrayFromCharSep(void** state _UNUSED) {
    char buffer[curType->size * curData->nrTestValues];

    size_t nrValues = swl_type_arrayFromCharSep(curType, buffer, curData->nrTestValues, curData->serialStr2, ";;");
    if(!curData->noDeSerial) {
        assert_int_equal(nrValues, curData->nrTestValues);
    }

    void* tgtData = buffer;
    void* srcData = curData->equalsValues;

    for(uint32_t i = 0; i < nrValues; i++) {
        assert_true(swl_type_equals(curType, s_curElToPtr(tgtData), s_curElToPtr(srcData)));
        swl_type_cleanup(curType, tgtData);
        tgtData += curType->size;
        srcData += curType->size;
    }
}

static void test_swl_type_arrayEquals(void** state _UNUSED) {


    assert_true(swl_type_arrayEquals(curType, curData->equalsValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues));
    assert_false(swl_type_arrayEquals(curType, curData->equalsValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues - 1));
    if(curData->ncValues != NULL) {
        assert_false(swl_type_arrayEquals(curType, curData->equalsValues, curData->nrTestValues, curData->ncValues, curData->nrTestValues));
    }

    size_t arraySize = curType->size * curData->nrTestValues;
    swl_bit8_t data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayEquals(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));


        if(curData->ncValues != NULL) {
            size_t offset = i * curType->size;
            memcpy(data + offset, curData->ncValues, curType->size);
            assert_false(swl_type_arrayEquals(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));
        }
    }
}

static void test_swl_type_arrayCount(void** state _UNUSED) {

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_int_equal(swl_type_arrayCount(curType, curData->equalsValues, curData->nrTestValues, s_arrCurVal(curData->equalsValues, i)),
                         curData->valueCount[i]);
    }



    if(curData->ncValues != NULL) {
        for(uint32_t i = 0; i < curData->nrTestValues; i++) {
            assert_int_equal(swl_type_arrayCount(curType, curData->equalsValues, curData->nrTestValues, s_arrCurVal(curData->ncValues, i)),
                             0);
        }
    }
}

static bool s_permutate(void* data, size_t index1, size_t index2) {
    size_t offset1 = curType->size * index1;
    size_t offset2 = curType->size * index2;
    if(swl_type_equals(curType, s_curElToPtr(data + offset1), s_curElToPtr(data + offset2))) {
        return true;
    }
    swl_bit8_t buffer[curType->size];
    memcpy(buffer, data + offset1, curType->size);
    memcpy(data + offset1, data + offset2, curType->size);
    memcpy(data + offset2, buffer, curType->size);

    return false;
}

static void test_swl_type_arrayMatches(void** state _UNUSED) {
    assert_true(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues));
    assert_false(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, curData->equalsValues, curData->nrTestValues - 1));


    if(curData->ncValues != NULL) {
        assert_false(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, curData->ncValues, curData->nrTestValues));
    }

    size_t arraySize = curType->size * curData->nrTestValues;
    swl_bit8_t data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));


        if(curData->notContains != NULL) {
            size_t offset = i * curType->size;
            memcpy(data + offset, curData->ncValues, curType->size);
            assert_false(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));
        }
    }

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memcpy(data, curData->equalsValues, arraySize);
            s_permutate(data, i, j);
            assert_true(swl_type_arrayMatches(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));
        }
    }
}

static void test_swl_type_arrayFindOffset(void** state _UNUSED) {
    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        for(ssize_t j = -1 * (ssize_t) curData->nrTestValues; j < (ssize_t) curData->nrTestValues; j++) {
            ssize_t index = swl_type_arrayFindOffset(curType, curData->equalsValues, curData->nrTestValues,
                                                     s_arrBaseVal(curData->baseValues, i), j);

            if((j < -1 * (ssize_t) curData->nrTestValues) || (j >= curData->nrTestValues)) {
                assert_true(index == (ssize_t) -1);
            } else if(j < 0) {
                ssize_t offset = j + curData->nrTestValues;

                if(offset >= i) {
                    assert_true(index >= 0);
                }
                if(index < 0) {
                    assert_true(offset < i);
                } else if(index == i) {
                    assert_true(offset >= i);
                } else {
                    assert_true(curData->valueCount[i] > 1);
                    assert_true(curData->valueCount[index] > 1);
                    assert_true(swl_type_equals(curType,
                                                swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, index),
                                                swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, i)));
                }
            } else { // j >= 0
                if(j <= i) {
                    assert_true(index >= 0);
                }
                if(index < 0) {
                    assert_true(j > i);
                } else if(index == i) {
                    assert_true(j <= i);
                } else {
                    assert_true(curData->valueCount[i] > 1);
                    assert_true(curData->valueCount[index] > 1);
                    assert_true(swl_type_equals(curType,
                                                swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, index),
                                                swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, i)));
                }
            }
        }
    }



    if(curData->ncValues != NULL) {
        for(ssize_t i = 0; i < curData->nrTestValues; i++) {
            for(ssize_t j = -2 * curData->nrTestValues; j < 2 * curData->nrTestValues; j++) {
                ssize_t index = swl_type_arrayFindOffset(curType, curData->equalsValues, curData->nrTestValues,
                                                         s_arrCurVal(curData->ncValues, i), j);
                assert_true(index == -1);
            }


        }
    }
}

static void test_swl_type_arrayFind(void** state _UNUSED) {
    for(ssize_t i = 0; i < curData->nrTestValues; i++) {
        ssize_t index = swl_type_arrayFind(curType, curData->equalsValues, curData->nrTestValues,
                                           s_arrBaseVal(curData->baseValues, i));
        assert_true(index >= 0);
        if(index != i) {
            assert_true(curData->valueCount[i] > 1);
            assert_true(curData->valueCount[index] > 1);
            assert_true(swl_type_equals(curType,
                                        swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, index),
                                        swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, i)));
        }

    }



    if(curData->notContains != NULL) {
        for(ssize_t i = 0; i < curData->nrTestValues; i++) {
            ssize_t index = swl_type_arrayFind(curType, curData->equalsValues, curData->nrTestValues,
                                               s_arrBaseVal(curData->notContains, i));
            assert_true(index == -1);
        }
    }
}

static void test_swl_type_arrayContains(void** state _UNUSED) {
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_arrayContains(curType, curData->equalsValues, curData->nrTestValues,
                                           s_arrBaseVal(curData->baseValues, i)));


        if(curData->notContains != NULL) {
            assert_false(swl_type_arrayContains(curType, curData->equalsValues, curData->nrTestValues,
                                                s_arrBaseVal(curData->notContains, i)));
        }
    }
}

static void test_swl_type_arrayIsSuperset(void** state _UNUSED) {
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        assert_true(swl_type_arrayIsSuperset(curType, curData->equalsValues, curData->nrTestValues,
                                             curData->equalsValues, curData->nrTestValues - i));
    }


    if(curData->ncValues != NULL) {
        for(uint32_t i = 0; i < curData->nrTestValues - 1; i++) {
            assert_false(swl_type_arrayIsSuperset(curType, curData->equalsValues, curData->nrTestValues,
                                                  curData->ncValues, curData->nrTestValues - i));
        }
    }

    size_t arraySize = curType->size * curData->nrTestValues;
    swl_bit8_t data[arraySize];
    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        memcpy(data, curData->equalsValues, arraySize);
        assert_true(swl_type_arrayIsSuperset(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));


        if(curData->notContains != NULL) {
            size_t offset = i * curType->size;
            memcpy(data + offset, curData->ncValues, curType->size);
            assert_false(swl_type_arrayIsSuperset(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues));
        }
    }

    for(uint32_t i = 0; i < curData->nrTestValues; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memcpy(data, curData->equalsValues, arraySize);
            s_permutate(data, i, j);
            for(uint32_t k = 0; k < curData->nrTestValues - 1; k++) {
                assert_true(swl_type_arrayIsSuperset(curType, curData->equalsValues, curData->nrTestValues, data, curData->nrTestValues - k));
            }
        }
    }
}

static void test_swl_type_arrayAddSet(void** state _UNUSED) {
    size_t bufSize = curType->size * curData->nrTestValues;
    size_t arraySize = curData->nrTestValues;

    char testBuffer[bufSize]; // buffer on which functions are run
    char tgtBuffer[bufSize];  // own constructed buffer
    swl_typeEl_t* testArray = &testBuffer;
    swl_typeEl_t* tgtArray = &tgtBuffer;

    memset(testBuffer, 0, bufSize);
    memset(tgtBuffer, 0, bufSize);

    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_type_arraySet(curType, testArray, arraySize, swl_type_toPtr(curType, curData->equalsValues + i * curType->size), i);
        memcpy(tgtArray + i * curType->size, curData->equalsValues + i * curType->size, curType->size);

        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
    }
    assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, curData->equalsValues, arraySize));



    if(curData->ncValues != NULL) {
        for(size_t i = 0; i < curData->nrTestValues; i++) {
            swl_type_arrayPrepend(curType, testBuffer, arraySize, swl_type_toPtr(curType, curData->ncValues), 0);
            memmove(tgtBuffer + curType->size, tgtBuffer, (curData->nrTestValues - 1) * curType->size);
            memcpy(tgtBuffer, curData->ncValues, curType->size);
            assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
        }
        for(size_t i = 0; i < curData->nrTestValues; i++) {
            assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
            swl_type_arrayAppend(curType, testBuffer, arraySize, swl_type_toPtr(curType, curData->equalsValues), -1);
            memcpy(tgtBuffer + (curData->nrTestValues - 1 - i) * curType->size, curData->equalsValues, curType->size);
            assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));
        }

    }

    swl_type_arrayCleanup(curType, testArray, arraySize);
}

static void test_swl_type_arrayCircShift(void** state _UNUSED) {

    size_t bufSize = curType->size * curData->nrTestValues;
    size_t arraySize = curData->nrTestValues;

    char testBuffer[bufSize]; // buffer on which functions are run
    char tgtBuffer[bufSize];  // own constructed buffer
    swl_typeEl_t* testArray = &testBuffer;
    swl_typeEl_t* tgtArray = &tgtBuffer;

    ssize_t testTarget = curData->nrTestValues;
    ssize_t testStartTarget = testTarget * -1;

    char strTest[256];
    char strTgt[256];

    memset(testBuffer, 0, bufSize);
    memset(tgtBuffer, 0, bufSize);

    // shift testing
    for(ssize_t i = testStartTarget; i <= testTarget; i++) {

        for(size_t i = 0; i < curData->nrTestValues; i++) {
            swl_type_arraySet(curType, testArray, arraySize, swl_type_toPtr(curType, curData->equalsValues + i * curType->size), i);
        }


        swl_type_arrayCircShift(curType, testBuffer, arraySize, i);

        size_t firstNrEl = swl_math_fmod(i, arraySize);
        size_t firstSize = firstNrEl * curType->size;
        size_t secondNrEl = arraySize - firstNrEl;
        size_t secondSize = secondNrEl * curType->size;

        memcpy(tgtBuffer, curData->equalsValues + firstSize, secondSize);
        memcpy(tgtBuffer + secondSize, curData->equalsValues, firstSize);

        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));

        if(firstNrEl == 0) {
            assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, curData->equalsValues, arraySize));
        }
    }

    swl_type_arrayToChar(curType, strTest, sizeof(strTest), testArray, arraySize);
    swl_type_arrayToChar(curType, strTgt, sizeof(strTgt), tgtArray, arraySize);

    swl_type_arrayCleanup(curType, testArray, arraySize);
    memset(tgtBuffer, 0, bufSize);
    assert_memory_equal(testBuffer, tgtBuffer, bufSize);

    assert_string_equal(strTest, strTgt);
}

static void test_swl_type_arrayShift(void** state _UNUSED) {

    size_t bufSize = curType->size * curData->nrTestValues;
    size_t arraySize = curData->nrTestValues;

    char testBuffer[bufSize]; // buffer on which functions are run
    char tgtBuffer[bufSize];  // own constructed buffer
    swl_typeEl_t* testArray = &testBuffer;
    swl_typeEl_t* tgtArray = &tgtBuffer;

    ssize_t testTarget = curData->nrTestValues;
    ssize_t testStartTarget = testTarget * -1;


    memset(testBuffer, 0, sizeof(testBuffer));
    memset(tgtBuffer, 0, sizeof(tgtBuffer));

    // shift testing
    for(ssize_t i = testStartTarget; i <= testTarget; i++) {

        for(size_t j = 0; j < curData->nrTestValues; j++) {
            swl_type_arraySet(curType, testArray, arraySize, swl_type_toPtr(curType, curData->equalsValues + j * curType->size), j);
        }

        swl_type_arrayShift(curType, testArray, arraySize, i);

        if(i < 0) {
            size_t nrEl = curData->nrTestValues + i;
            size_t startIndex = curData->nrTestValues - nrEl;

            swl_typeEl_t* data = curData->equalsValues + startIndex * curType->size;

            memcpy(tgtArray, data, nrEl * curType->size);
        } else {
            size_t nrEl = curData->nrTestValues - i;
            swl_typeEl_t* tgtInBuf = tgtArray + i * curType->size;

            memcpy(tgtInBuf, curData->equalsValues, nrEl * curType->size);
        }


        assert_true(swl_type_arrayEquals(curType, testBuffer, arraySize, tgtBuffer, arraySize));

        //Only do cleanup at "expected" places
        for(ssize_t j = SWL_MAX(0, testStartTarget); j < SWL_MIN(curData->nrTestValues, curData->nrTestValues + i); j++) {
            swl_type_arrayCleanAt(curType, testArray, arraySize, j);
        }

        memset(tgtBuffer, 0, sizeof(tgtBuffer));

        assert_memory_equal(tgtBuffer, testBuffer, bufSize);
    }

}


static void test_swl_type_arrayGet(void** state _UNUSED) {
    for(size_t i = 0; i < curData->nrTestValues; i++) {
        swl_typeEl_t* elData = curData->equalsValues + (i * curType->size);
        swl_typeData_t* baseData = s_arrCurVal(curData->equalsValues, i);
        for(ssize_t j = -1; j < 1; j++) {
            swl_typeData_t* data = swl_type_arrayGetValue(curType, curData->equalsValues, curData->nrTestValues, i + j * curData->nrTestValues);

            swl_ttb_assertTypeEquals(curType, data, baseData);

            swl_typeEl_t* refData = swl_type_arrayGetReference(curType, curData->equalsValues, curData->nrTestValues, i + j * curData->nrTestValues);
            assert_ptr_equal(refData, elData);

        }
    }
}

static void test_swl_type_arrayGetCopy(void** state _UNUSED) {
    char buffer[curType->size];
    memset(buffer, 0, curType->size);
    for(size_t i = 0; i < curData->nrTestValues; i++) {

        swl_typeData_t* typeData = s_arrBaseVal(curData->baseValues, i);

        for(ssize_t j = -1; j < 1; j++) {
            memset(buffer, 0, curType->size);
            swl_type_arrayGet(curType, buffer, curData->equalsValues, curData->nrTestValues, i + j * curData->nrTestValues);

            if(curType->typeFun->isValue) {
                assert_memory_equal(typeData, buffer, curType->size);
            } else {
                assert_memory_not_equal(typeData, buffer, curType->size);
                assert_true(swl_type_equals(curType, swl_type_toPtr(curType, buffer), typeData));
            }

            swl_type_cleanup(curType, buffer);
        }
    }

}

static void test_swl_type_extractValueArrayFromStructArray(void** state _UNUSED) {
    uint32_t testNrEntries = 4;
    uint32_t entrySize = curType->size * testNrEntries;
    uint32_t bufSize = entrySize * curData->nrTestValues;

    char buffer[bufSize];

    for(uint32_t i = 0; i < testNrEntries; i++) {
        for(uint32_t j = 0; j < curData->nrTestValues; j++) {
            memset(buffer, 0, bufSize);
            for(uint32_t k = 0; k < curData->nrTestValues; k++) {
                uint32_t index = (j + k) % curData->nrTestValues;
                void* tgtLocation = buffer + index * entrySize + i * curType->size;
                void* srcLocation = curData->equalsValues + k * curType->size;
                memcpy(tgtLocation, srcLocation, curType->size);
            }
            char targetBuffer[curType->size * curData->nrTestValues];

            swl_type_extractValueArrayFromStructArray(curType, targetBuffer, buffer, i * curType->size,
                                                      testNrEntries * curType->size, j, curData->nrTestValues);
            assert_true(swl_type_arrayEquals(curType, targetBuffer, curData->nrTestValues, curData->equalsValues, curData->nrTestValues));
        }
    }
}

static void test_swl_type_badVals(void** state _UNUSED) {
    char buffer[curType->size ];

    for(uint32_t i = 0; i < curData->nrBadVals; i++) {
        memset(buffer, 0, sizeof(buffer));
        ttb_assert_addPrint("%i %s", i, curData->badVals[i]);
        ttb_assert_false(swl_type_fromChar(curType, buffer, curData->badVals[i]));
        ttb_assert_removeLastPrint();
    }

    memset(buffer, 0, sizeof(buffer));
    assert_false(swl_type_fromChar(curType, NULL, NULL));
    assert_false(swl_type_fromChar(curType, NULL, curData->strValues[0]));
    assert_false(swl_type_fromChar(NULL, buffer, curData->strValues[0]));
}

static void s_checkStr(const char** data, char** testString, swlc_type_testCommonData_t* testData, const char* delimiter) {
    if(*data != NULL) {
        return;
    }
    char* myTestData = calloc(1, 256);
    *testString = myTestData;

    for(uint32_t i = 0; i < testData->nrTestValues; i++) {
        swl_str_cat(myTestData, 256, testData->strValues[i]);
        if(i < testData->nrTestValues - 1) {
            swl_str_cat(myTestData, 256, delimiter);
        }
    }

    *data = (const char*) myTestData;
}

static void s_verifyTestData(swlc_type_testCommonData_t* testData) {
    s_checkStr(&testData->serialStr, &testData->serial1, testData, ",");
    s_checkStr(&testData->serialStr2, &testData->serial2, testData, ";;");
}

static void s_cleanTestData(swlc_type_testCommonData_t* testData) {
    if(testData->serial1 != NULL) {
        testData->serialStr = NULL;
        free(testData->serial1);
        testData->serial1 = NULL;
    }
    if(testData->serial2 != NULL) {
        testData->serialStr2 = NULL;
        free(testData->serial2);
        testData->serial2 = NULL;
    }
}

int runCommonTypeTest(swlc_type_testCommonData_t* testData) {
    s_verifyTestData(testData);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_valueSetup),
        cmocka_unit_test(test_swl_type_equals),
        cmocka_unit_test(test_swl_type_copy),
        cmocka_unit_test(test_swl_type_copyCleanup),
        cmocka_unit_test(test_swl_type_tofromChar),
        cmocka_unit_test(test_swl_type_toFile),
        cmocka_unit_test(test_swl_type_toCString),
        cmocka_unit_test(test_swl_type_toCharOther),
        cmocka_unit_test(test_swl_type_toBuf32),
        cmocka_unit_test(test_swl_type_getFlag),
        cmocka_unit_test(test_swl_type_arrayToChar),
        cmocka_unit_test(test_swl_type_arrayFromChar),
        cmocka_unit_test(test_swl_type_arrayFromCharSep),
        cmocka_unit_test(test_swl_type_arrayEquals),
        cmocka_unit_test(test_swl_type_arrayCount),
        cmocka_unit_test(test_swl_type_arrayFindOffset),
        cmocka_unit_test(test_swl_type_arrayFind),
        cmocka_unit_test(test_swl_type_arrayContains),
        cmocka_unit_test(test_swl_type_arrayMatches),
        cmocka_unit_test(test_swl_type_arrayIsSuperset),
        cmocka_unit_test(test_swl_type_extractValueArrayFromStructArray),
        cmocka_unit_test(test_swl_type_arrayAddSet),
        cmocka_unit_test(test_swl_type_arrayCircShift),
        cmocka_unit_test(test_swl_type_arrayShift),
        cmocka_unit_test(test_swl_type_arrayGet),
        cmocka_unit_test(test_swl_type_arrayGetCopy),
        cmocka_unit_test(test_swl_type_badVals),

    };
    curType = testData->testType;
    baseType = testData->testType;
    curData = testData;
    printf("\nTesting %s\n", curData->name);
    ttb_util_setFilter();

    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    if(testData->refType != NULL) {
        curType = testData->refType;
        printf("\nTesting REF TYPE %s\n", curData->name);
        rc += cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    }
    ttb_assert_clearPrint();

    s_cleanTestData(testData);
    sahTraceClose();
    return rc;
}
