/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "test-toolbox/ttb.h"
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_tupleType.h"

#include "test_swlc_type_testCommon.h"



static uint8_t val0 = 1;
static uint8_t val1 = 2;

static void s_callOnBaseTypes_number_cb (swl_type_t* numberType, swl_typeEl_t** array, size_t arraySize,
                                         void* userData, swl_typeCallInfo_t* info) {
    assert_null(userData);
    assert_non_null(info);
    assert_int_equal(info->depth, 0);
    ttb_assert_int_eq(arraySize, 2);
    assert_ptr_equal(numberType, &gtSwl_type_uint8);
    assert_ptr_equal(array[0], &val0);
    assert_ptr_equal(array[1], &val1);
}

static void test_swl_type_callOnBaseTypes_number(void** state _UNUSED) {
    uint8_t* baseArray[2] = {&val0, &val1};

    swl_type_callOnBaseTypes(&gtSwl_type_uint8, (void**) baseArray, 2, s_callOnBaseTypes_number_cb, NULL);

    uint8_t* tmp0 = &val0;
    uint8_t* tmp1 = &val1;
    uint8_t** refArray[2] = {&tmp0, &tmp1};
    swl_type_callOnBaseTypes(&gtSwl_type_uint8Ptr.type, (void**) refArray, 2, s_callOnBaseTypes_number_cb, NULL);
}

#define MY_TEST_TABLE_SUBVAR(X, Y) \
    X(Y, gtSwl_type_int8, test1) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(tMyTestSubTupleType, swl_myTestSubTT_t, MY_TEST_TABLE_SUBVAR, );

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_charPtr, val) \
    X(Y, gtSwl_type_int16, test1) \
    X(Y, tMyTestSubTupleType, sub)
SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, );

#define NR_TT_VALUES 2

swl_myTestTT_t myTestTableValues[NR_TT_VALUES] = {
    {"Bla", 1, {1, "test1"}},
    {"Foo", 3, {31, "no do"}},
};

static uint32_t s_callCount = 0;

static void s_callOnBaseTypes_tupleType_cb (swl_type_t* numberType, swl_typeEl_t** array, size_t arraySize,
                                            void* userData, swl_typeCallInfo_t* info) {
    assert_non_null(userData);
    assert_non_null(info);

    assert_ptr_equal(userData, &s_callCount);

    assert_int_equal(arraySize, NR_TT_VALUES);


    s_callCount++;
    printf("Call %u\n", s_callCount);
    if(s_callCount == 1) {
        assert_int_equal(info->depth, 1);
        assert_int_equal(info->index[0], 0);
        assert_ptr_equal(numberType, &gtSwl_type_charPtr);
        assert_ptr_equal(array[0], &myTestTableValues[0].val);
        assert_ptr_equal(array[1], &myTestTableValues[1].val);
    }
    if(s_callCount == 2) {
        assert_int_equal(info->depth, 1);
        assert_int_equal(info->index[0], 1);
        assert_ptr_equal(numberType, &gtSwl_type_int16);
        assert_ptr_equal(array[0], &myTestTableValues[0].test1);
        assert_ptr_equal(array[1], &myTestTableValues[1].test1);
    }

    if(s_callCount == 3) {
        assert_int_equal(info->depth, 2);
        assert_int_equal(info->index[0], 2);
        assert_int_equal(info->index[1], 0);
        assert_ptr_equal(numberType, &gtSwl_type_int8);
        assert_ptr_equal(array[0], &myTestTableValues[0].sub.test1);
        assert_ptr_equal(array[1], &myTestTableValues[1].sub.test1);
    }
    if(s_callCount == 4) {
        assert_int_equal(info->depth, 2);
        assert_int_equal(info->index[0], 2);
        assert_int_equal(info->index[1], 1);
        assert_ptr_equal(numberType, &gtSwl_type_charPtr);
        assert_ptr_equal(array[0], &myTestTableValues[0].sub.val);
        assert_ptr_equal(array[1], &myTestTableValues[1].sub.val);
    }
}

static void test_swl_type_callOnBaseTypes_tupleType(void** state _UNUSED) {
    s_callCount = 0;
    swl_myTestTT_t* testArray[NR_TT_VALUES] = {&myTestTableValues[0], &myTestTableValues[1]};
    printf("%p %p\n", &myTestTableValues[0], &myTestTableValues[1]);

    swl_type_callOnBaseTypes(&tMyTestTupleType.type, (void**) testArray, NR_TT_VALUES,
                             s_callOnBaseTypes_tupleType_cb, &s_callCount);
    assert_int_equal(s_callCount, 4);
}

static int setup_suite(void** state) {
    (void) state;
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_type_callOnBaseTypes_number),
        cmocka_unit_test(test_swl_type_callOnBaseTypes_tupleType),
    };

    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    sahTraceClose();
    return rc;
}
