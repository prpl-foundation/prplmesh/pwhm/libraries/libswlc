SUT_OBJECTS += $(SUT_DIR)/src/swl_type.o \
				$(SUT_DIR)/src/swl_tupleType.o \
				$(SUT_DIR)/src/swl_tta.o \
				$(SUT_DIR)/src/swl_typeDefs.o \
				$(SUT_DIR)/src/swl_llist.o \
				$(SUT_DIR)/src/swl_string.o \
				$(SUT_DIR)/src/swl_math.o \
				$(SUT_DIR)/src/types/swl_refType.o \
				$(SUT_DIR)/src/swl_bit.o \
				$(SUT_DIR)/src/swl_returnCode.o \
				$(SUT_DIR)/src/swl_conversion.o \
				$(SUT_DIR)/src/fileOps/swl_print.o \
				$(SUT_DIR)/src/fileOps/swl_fileUtils.o \
				$(SUT_DIR)/src/types/swl_refType.o \
				$(SUT_DIR)/src/types/swl_collType.o \
				$(SUT_DIR)/test/swl_type/test_swlc_type_testCommon.o

CFLAGS += -I$(SUT_DIR)/test/swl_type
LDFLAGS += -lm