/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common_tupleType.h"
#include "swl/swl_common_table.h"
#include "swl/swl_common.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common_type.h"
#include "swl/ttb/swl_ttb.h"
#include "swl/types/swl_tupleTypeArray.h"

#define NR_TYPES 3
#define NR_VALUES 4



#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index) \
    X(Y, gtSwl_type_int64, key) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(myTestTTType, swl_myTestTT_t, MY_TEST_TABLE_VAR, structMod);


swl_myTestTT_t myTestTable[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-8852, 289870, "foobar"},
    {8851, -289869, "barfoo"},
};

swl_myTestTT_t myTestTableDup[NR_VALUES] = {
    {1, 100, "test1"},
    {1, -200, "test2"},
    {2, -200, "test3"},
    {3, -300, "test3"},
};

// vars for directed search
#define NR_VALUES_RANGE_TEST 10
#define INDEX_DECR        5
#define INDEX_DECR_VAL_0  50
#define INDEX_INCR        6
#define INDEX_INCR_VAL_0  55

swl_myTestTT_t myTestTableBig[NR_VALUES_RANGE_TEST] = {
    {0, 100, "test1"},
    {1, -100, "test2"},
    {2, 200, "testtwo"},
    {3, -200, "testthree"},
    {4, 400, "testfour"},
    {INDEX_DECR_VAL_0, (-500), "identical"}, // INDEX_DECR trigger element;
    {INDEX_INCR_VAL_0, (-500), "identical"}, // INDEX_INCR trigger element;
    {7, 700, "testseven"},
    {8, -800, "testeight"},
    {9, 900, "testlast"},
};

swl_myTestTT_t zeroValues = {0, 0, NULL};


char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

const char* columnArrays[NR_TYPES] = {
    "1,2,-8852,8851",
    "100,-200,289870,-289869",
    "test1,test2,foobar,barfoo"
};

/**
 * Strings with one offset, for offset testing
 */
const char* columnArraysOffset[NR_TYPES] = {
    "2,-8852,8851,1",
    "-200,289870,-289869,100",
    "test2,foobar,barfoo,test1"
};

#define NR_OTHER_DATA 2

swl_myTestTT_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};

static void test_swl_tta_getValue(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            swl_typeData_t* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], j);
            swl_typeData_t* data = swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j);

            assert_ptr_equal(srcData, data);

            assert_null(swl_tta_getElementValue(NULL, &myTestTable, NR_VALUES, i, j));
        }

        assert_null(swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, -1));
        assert_null(swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, NR_TYPES));
    }
    for(size_t j = 0; j < NR_TYPES; j++) {
        assert_null(swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, -1 - NR_VALUES, j));
        assert_null(swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, NR_VALUES, j));
    }
}

static void test_swl_tta_getReference(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            swl_typeEl_t* srcEl = swl_tupleType_getReference(&myTestTTType, &myTestTable[i], j);
            swl_typeEl_t* tgtEl = swl_tta_getElementReference(&myTestTTType, &myTestTable, NR_VALUES, i, j);
            assert_ptr_equal(srcEl, tgtEl);

            assert_null(swl_tta_getElementValue(NULL, &myTestTable, NR_VALUES, i, j));
        }

        assert_null(swl_tta_getElementReference(&myTestTTType, &myTestTable, NR_VALUES, i, -1));
        assert_null(swl_tta_getElementReference(&myTestTTType, &myTestTable, NR_VALUES, i, NR_TYPES));
    }
    for(size_t j = 0; j < NR_TYPES; j++) {
        assert_null(swl_tta_getElementReference(&myTestTTType, &myTestTable, NR_VALUES, -1 - NR_VALUES, j));
        assert_null(swl_tta_getElementReference(&myTestTTType, &myTestTable, NR_VALUES, NR_VALUES, j));
    }
}

static void test_swl_tta_getTuple(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        swl_tuple_t* tuple = swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i);
        assert_ptr_equal(&myTestTable[i], tuple);
    }

    for(ssize_t i = -1 * (ssize_t) NR_VALUES; i < 0; i++) {
        swl_tuple_t* tuple = swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i);
        assert_ptr_equal(&myTestTable[i + NR_VALUES], tuple);
    }


    assert_null(swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, -1 - NR_VALUES));
    assert_null(swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, NR_VALUES));
    assert_null(swl_tta_getTuple(&myTestTTType, NULL, NR_VALUES, 0));
    assert_null(swl_tta_getTuple(&myTestTTType, NULL, NR_VALUES, NR_VALUES - 1));
}

static void test_swl_tta_getMatchingValue(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            for(size_t k = 0; k < NR_TYPES; k++) {
                swl_typeData_t* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], j);
                swl_typeData_t* tgtData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], k);
                printf("%p : %p %p %p\n", &myTestTTType, srcData, tgtData, &myTestTable[i]);
                swl_typeData_t* data = swl_tta_getMatchingElement(&myTestTTType, &myTestTable, NR_VALUES, 0, k, j, srcData);

                assert_ptr_equal(data, tgtData);

            }
        }
    }

    for(size_t i = 0; i < NR_VALUES; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            swl_typeData_t* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], j);
            assert_null(swl_tta_getMatchingElement(&myTestTTType, &myTestTable, NR_VALUES, 0, NR_VALUES, j, srcData));
            assert_null(swl_tta_getMatchingElement(&myTestTTType, &myTestTable, NR_VALUES, 0, -1, j, srcData));

            for(size_t k = 0; k < NR_TYPES; k++) {
                assert_null(swl_tta_getMatchingElement(&myTestTTType, NULL, NR_VALUES, 0, k, j, srcData));
            }
        }
    }

    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            for(size_t k = 0; k < NR_TYPES; k++) {
                swl_typeData_t* srcData = swl_tupleType_getValue(&myTestTTType, &otherData[i], j);
                assert_null(swl_tta_getMatchingElement(&myTestTTType, &myTestTable, NR_VALUES, 0, k, j, srcData));
            }
        }
    }
}

static void test_swl_tta_getMatchingTuple(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], j);
            void* tuple = swl_tta_getMatchingTuple(&myTestTTType, &myTestTable, NR_VALUES, 0, j, srcData);
            assert_ptr_equal(&myTestTable[i], tuple);
        }
    }


    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTable[i], j);
            assert_null(swl_tta_getMatchingTuple(&myTestTTType, &myTestTable, NR_VALUES, 0, NR_VALUES, srcData));
            assert_null(swl_tta_getMatchingTuple(&myTestTTType, &myTestTable, NR_VALUES, 0, -1, srcData));
            assert_null(swl_tta_getMatchingTuple(&myTestTTType, NULL, NR_VALUES, 0, j, srcData));
        }
    }
}

static void test_swl_tta_getMatchingTupleInRangeIncr(void** state _UNUSED) {

    ssize_t firstEl, lastEl, stepSize;

    // test incremental search, from 0 to -1 with step:1
    firstEl = 0;
    lastEl = -1;
    stepSize = 1;

    /* below is a zoom in on the contents of myTestTableBig around the trigger elements;
       index   value
       4       {4, 400, "testfour"},
       5       {INDEX_DECR_VAL_0, (-500), "identical"}, // INDEX_DECR trigger element;
       6       {INDEX_INCR_VAL_0, (-500), "identical"}, // INDEX_INCR trigger element;
       7       {7, 700, "testseven"},

       we are iterating incrementally on this array; for each index, meaning for each tuple, we are
       testing the getMatchingTupleInRange on each element of the tuple;
       when testing the tuple with the index 6 (which is cloned as the first element of the tuple itself),
       we expect the function to match the previous tuple,
       i.e., trigger for index 5 when using tuple elements 2 and 3
       when the first element of the tuple is used, the function should return the tuple at index 6;
       when the first element of the tuple is used, the funct
     */

    for(size_t i = 0; i < NR_VALUES_RANGE_TEST; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[i], j);
            void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                          j, srcData, firstEl, lastEl, stepSize);

            if(i != INDEX_INCR) {
                // skip INDEX_INCR since it's a special value that needs special treatment
                assert_ptr_equal(&myTestTableBig[i], tuple);
            } else {
                if(j == 0) {
                    // element 0 is different between the two special tuples,
                    // and so the search should find the good tuple
                    assert_ptr_equal(&myTestTableBig[i], tuple);
                } else {
                    // elements 1 and 2 in the INDEX_INCR tuple are identical to previous tuple,
                    // and so the search should find the previous tuple
                    assert_ptr_equal(&myTestTableBig[INDEX_DECR], tuple);
                }
            }
        }
    }
    // test stepSize > 1
    stepSize = INDEX_INCR;
    for(size_t j = 0; j < NR_TYPES; j++) {
        void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[INDEX_INCR], j);
        void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                      j, srcData, firstEl, lastEl, stepSize);
        // no aliasing here, since we should jump over element INDEX_DECR because of the stepSize
        // checking elements 0, STEP_SIZE, STEP_SIZE*2
        assert_ptr_equal(&myTestTableBig[INDEX_INCR], tuple);
    }


    stepSize++; // step over the value
    for(size_t j = 0; j < NR_TYPES; j++) {
        void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[INDEX_INCR], j);
        void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                      j, srcData, firstEl, lastEl, stepSize);
        // no result at all here, since we should jump over INDEX_INCR and INDEX_DECR
        assert_null(tuple);
    }
}

static void test_swl_tta_getMatchingTupleInRangeDecr(void** state _UNUSED) {

    ssize_t firstEl, lastEl, stepSize;

    // test decremental search, from 0 to 1 with step: -1;
    // similar reasoning as the test_swl_tta_getMatchingTupleInRangeIncr() function
    // indexes in the table : 0 1 2 3 4 5 6 7 8 9 : 10 elements
    // elements checked : 0 9 8 7 6 5 4 3 2 1 (firstEl = 0; lastEl = 1, stepSize -1)
    firstEl = 0;
    lastEl = 1;
    stepSize = -1;

    for(size_t i = 0; i < NR_VALUES_RANGE_TEST; i++) {
        for(size_t j = 0; j < NR_TYPES; j++) {
            void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[i], j);
            void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                          j, srcData, firstEl, lastEl, stepSize);

            if(i != INDEX_DECR) {
                // skip INDEX_DECR since it's a special value that needs special treatment
                assert_ptr_equal(&myTestTableBig[i], tuple);
            } else {
                if(j == 0) {
                    // element 0 is different between the two special tuples,
                    // and so the search should find the good tuple
                    assert_ptr_equal(&myTestTableBig[i], tuple);
                } else {
                    // elements 1 and 2 in the INDEX_INCR tuple are identical to previous tuple,
                    // and so the search should find the previous tuple
                    // 'previous' with respect to the order the list is iterated upon
                    assert_ptr_equal(&myTestTableBig[INDEX_INCR], tuple);
                }
            }
        }
    }

    stepSize = (0 - INDEX_DECR);
    // test stepSize < -1;
    // in this case, when firstEl 0, and lastEl 1, this stepSize will check strictly element INDEX_DECR second

    for(size_t j = 0; j < NR_TYPES; j++) {
        void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[INDEX_DECR], j);
        void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                      j, srcData, firstEl, lastEl, stepSize);
        // no aliasing here, since we should jump over element INDEX_INCR because of the stepSize
        // checking elements 0, (-STEP_SIZE)
        assert_ptr_equal(&myTestTableBig[INDEX_DECR], tuple);
    }


    stepSize--;
    // step over the value when iterating in reverse;
    for(size_t j = 0; j < NR_TYPES; j++) {
        void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[INDEX_DECR], j);
        void* tuple = swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTableBig, NR_VALUES_RANGE_TEST,
                                                      j, srcData, firstEl, lastEl, stepSize);
        // no result at all here, since we should jump over both INDEX_DECR and INDEX_INCR
        assert_null(tuple);
    }
}

static void test_swl_tta_getMatchingTupleInRangeAssert(void** state _UNUSED) {

    ssize_t firstEl, lastEl, stepSize;

    firstEl = 0;
    lastEl = -1;
    stepSize = 1;

    void* srcData = swl_tupleType_getValue(&myTestTTType, &myTestTableBig[INDEX_DECR], 0);

    // trigger srcIndex assert (srcIndex == NR_VALUES_RANGE_TEST)
    assert_null(swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTable, NR_VALUES_RANGE_TEST,
                                                NR_VALUES_RANGE_TEST, srcData, firstEl, lastEl, stepSize));

    stepSize = 0; // trigger stepSize assert
    assert_null(swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTable, NR_VALUES_RANGE_TEST,
                                                0, srcData, firstEl, lastEl, stepSize));

    stepSize = 2;                       // reset stepSize to a valid value
    firstEl = NR_VALUES_RANGE_TEST + 2; // trigger firstEl assert
    assert_null(swl_tta_getMatchingTupleInRange(&myTestTTType, &myTestTable, NR_VALUES_RANGE_TEST,
                                                0, srcData, firstEl, lastEl, stepSize));

    // trigger input table NULL assert
    assert_null(swl_tta_getMatchingTupleInRange(&myTestTTType, NULL, NR_VALUES_RANGE_TEST,
                                                0, srcData, firstEl, lastEl, stepSize));
}


#define NR_MASK_VALUES (2 << (NR_TYPES - 1))
#define ALL_MASK (NR_MASK_VALUES - 1)

/**
 * Defining a tuple type using a table
 */
swl_myTestTT_t myTestTableMasked[NR_MASK_VALUES] = {
    {2, 200, "test2"},
    {1, 200, "test2"},
    {2, 100, "test2"},
    {1, 100, "test2"},
    {2, 200, "test1"},
    {1, 200, "test1"},
    {2, 100, "test1"},
    {1, 100, "test1"},
}
;
swl_myTestTT_t myBaseVal = {.index = 1, .key = 100, .val = "test1"};

static void s_checkHasNTuples(size_t nrLeft, size_t currOfset, size_t mask) {
    if(nrLeft == 0) {
        return;
    }
    swl_myTestTT_t* tuple = (swl_myTestTT_t*) swl_tta_getMatchingTupleByMask(&myTestTTType, &myTestTableMasked, NR_MASK_VALUES, currOfset,
                                                                             &myBaseVal, mask);
    assert_true(swl_tupleType_equalsByMask(&myTestTTType, &myBaseVal, tuple, mask));


    size_t index = ((void*) tuple - (void*) myTestTableMasked) / sizeof(swl_myTestTT_t);
    assert_true((index & mask) == mask);

    for(size_t i = 0; i < NR_TYPES; i++) {
        swl_typeData_t* data = swl_tta_getMatchingElementByMask(&myTestTTType, &myTestTableMasked, NR_MASK_VALUES, currOfset,
                                                                i, &myBaseVal, mask);
        assert_ptr_equal(data, swl_tupleType_getValue(&myTestTTType, &myTestTableMasked[index], i));
    }

    s_checkHasNTuples(nrLeft - 1, index + 1, mask);
}

static void test_swl_tta_getMatchingXByTuple(void** state _UNUSED) {
    for(size_t i = 0; i < NR_MASK_VALUES; i++) {
        size_t nrBitUnset = NR_TYPES - swl_bit64_getNrSet(i);
        size_t nrMatches = 1 << (nrBitUnset);
        s_checkHasNTuples(nrMatches, 0, i);
    }
}

static void test_swl_tta_equals(void** state _UNUSED) {
    swl_myTestTT_t testTable[NR_VALUES] = {0};

    for(size_t i = 0; i < NR_VALUES; i++) {
        swl_tuple_t* val1 = swl_tta_getTuple(&myTestTTType, testTable, NR_VALUES, i);
        swl_tuple_t* val2 = swl_tta_getTuple(&myTestTTType, myTestTable, NR_VALUES, i);
        swl_type_copyTo(&myTestTTType.type, val1, val2);
    }


    assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));

    for(int i = 0; i < NR_VALUES; i++) {
        for(int j = 0; j < NR_TYPES; j++) {
            swl_type_t* type = myTestTTType.types[j];
            char buffer[type->size];
            void* data = &buffer;
            void* val = swl_tta_getElementReference(&myTestTTType, &testTable, NR_VALUES, i, j);
            void* testVal = swl_tupleType_getReference(&myTestTTType, &otherData[0], j);
            memcpy(data, val, type->size);
            memcpy(val, testVal, type->size);
            assert_false(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));
            memcpy(val, data, type->size);
            assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));
        }
    }

    for(size_t i = 0; i < NR_VALUES; i++) {
        swl_tuple_t* val1 = swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i);
        swl_type_cleanup(&myTestTTType.type, val1);
    }
}


static void test_swl_tta_setValue(void** state _UNUSED) {
    swl_myTestTT_t testTable[NR_VALUES] = {0};


    for(size_t i = 0; i < NR_VALUES; i++) {
        ttb_assert_addPrint("Value %zu", i);
        for(size_t j = 0; j < NR_TYPES; j++) {
            ttb_assert_addPrint("Type %zu", j);
            swl_type_t* curType = myTestTTType.types[j];

            swl_ttb_assertTypeEquals(curType,
                                     swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                     swl_tupleType_getValue(&myTestTTType, &zeroValues, j));
            swl_ttb_assertTypeNotEquals(curType,
                                        swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                        swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j));

            ttb_assert_int_eq(SWL_RC_OK,
                              swl_tta_setElementValue(&myTestTTType, &testTable, NR_VALUES,
                                                      i, j,
                                                      swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j)));

            swl_ttb_assertTypeNotEquals(curType,
                                        swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                        swl_tupleType_getValue(&myTestTTType, &zeroValues, j));
            swl_ttb_assertTypeEquals(curType,
                                     swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                     swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j));

            ttb_assert_removeLastPrint();
        }
        ttb_assert_removeLastPrint();
    }


    assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));

    // Inverse
    swl_tta_cleanup(&myTestTTType, testTable, NR_VALUES);

    for(ssize_t i = -1; i >= -1 * (ssize_t) NR_VALUES; i--) {
        ttb_assert_addPrint("Value %zi", i);
        for(size_t j = 0; j < NR_TYPES; j++) {
            ttb_assert_addPrint("Type %zu", j);

            swl_type_t* curType = myTestTTType.types[j];
            swl_ttb_assertTypeEquals(curType,
                                     swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                     swl_tupleType_getValue(&myTestTTType, &zeroValues, j));
            swl_ttb_assertTypeNotEquals(curType,
                                        swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                        swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j));

            ttb_assert_int_eq(SWL_RC_OK,
                              swl_tta_setElementValue(&myTestTTType, &testTable, NR_VALUES, i, j,
                                                      swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j)));


            swl_ttb_assertTypeNotEquals(curType,
                                        swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                        swl_tupleType_getValue(&myTestTTType, &zeroValues, j));
            swl_ttb_assertTypeEquals(curType,
                                     swl_tta_getElementValue(&myTestTTType, &testTable, NR_VALUES, i, j),
                                     swl_tta_getElementValue(&myTestTTType, &myTestTable, NR_VALUES, i, j));

            ttb_assert_removeLastPrint();
        }
        ttb_assert_removeLastPrint();
    }


    assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));

    swl_tta_cleanup(&myTestTTType, testTable, NR_VALUES);
}



static void test_swl_tta_setTuple(void** state _UNUSED) {
    swl_myTestTT_t testTable[NR_VALUES] = {0};

    for(size_t i = 0; i < NR_VALUES; i++) {
        ttb_assert_addPrint("Value %zu", i);

        swl_ttb_assertTypeEquals(&myTestTTType.type,
                                 &zeroValues,
                                 swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));
        swl_ttb_assertTypeNotEquals(&myTestTTType.type,
                                    swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i),
                                    swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));

        ttb_assert_int_eq(SWL_RC_OK,
                          swl_tta_setTuple(&myTestTTType, &testTable, NR_VALUES,
                                           i, swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i)));

        swl_ttb_assertTypeNotEquals(&myTestTTType.type,
                                    &zeroValues,
                                    swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));
        swl_ttb_assertTypeEquals(&myTestTTType.type,
                                 swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i),
                                 swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));

        ttb_assert_removeLastPrint();
    }

    assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));
    // Inverse
    swl_tta_cleanup(&myTestTTType, testTable, NR_VALUES);

    for(ssize_t i = -1; i >= -1 * (ssize_t) NR_VALUES; i--) {
        ttb_assert_addPrint("Value %zi", i);

        swl_ttb_assertTypeEquals(&myTestTTType.type,
                                 &zeroValues,
                                 swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));
        swl_ttb_assertTypeNotEquals(&myTestTTType.type,
                                    swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i),
                                    swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));

        ttb_assert_int_eq(SWL_RC_OK,
                          swl_tta_setTuple(&myTestTTType, &testTable, NR_VALUES,
                                           i, swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i)));

        swl_ttb_assertTypeNotEquals(&myTestTTType.type,
                                    &zeroValues,
                                    swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));
        swl_ttb_assertTypeEquals(&myTestTTType.type,
                                 swl_tta_getTuple(&myTestTTType, &myTestTable, NR_VALUES, i),
                                 swl_tta_getTuple(&myTestTTType, &testTable, NR_VALUES, i));

        ttb_assert_removeLastPrint();
    }

    assert_true(swl_tta_equals(&myTestTTType, testTable, NR_VALUES, myTestTable, NR_VALUES));

    swl_tta_cleanup(&myTestTTType, testTable, NR_VALUES);
}

static void test_swl_tta_printLists(void** state _UNUSED) {
    char buffer[256];
    memset(buffer, 0, sizeof(buffer));

    ssize_t result = swl_tta_printLists(&myTestTTType, buffer, sizeof(buffer), &myTestTable, NR_VALUES, 0);
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printListTest.txt");

    result = swl_tta_printLists(&myTestTTType, buffer, sizeof(buffer), &myTestTableMasked, ALL_MASK, 0);
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printListMask.txt");

    result = swl_tta_printLists(&myTestTTType, buffer, sizeof(buffer), &zeroValues, 1, 0);
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printListZeros.txt");

    result = swl_tta_printLists(&myTestTTType, buffer, sizeof(buffer), &zeroValues, 0, 0);
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printListEmpty.txt");
}

static void test_swl_tta_printMaps(void** state _UNUSED) {
    char buffer[256];
    memset(buffer, 0, sizeof(buffer));

    ssize_t result = swl_tta_printMaps(&myTestTTType, buffer, sizeof(buffer), &myTestTable, NR_VALUES, 0,
                                       names, "History");
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printMapTest.txt");

    result = swl_tta_printMaps(&myTestTTType, buffer, sizeof(buffer), &myTestTableMasked, ALL_MASK, 0,
                               names, "History");
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printMapMask.txt");

    result = swl_tta_printMaps(&myTestTTType, buffer, sizeof(buffer), &zeroValues, 1, 0,
                               names, "History");
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printMapZeros.txt");

    result = swl_tta_printMaps(&myTestTTType, buffer, sizeof(buffer), &zeroValues, 0, 0,
                               names, "History");
    assert_true(result > 0);
    swl_fileUtils_writeFile(buffer, "print/printMapEmpty.txt");
}

static void s_fPrintLists(const swl_tuple_t* array, size_t arraySize, const char* fileName) {
    FILE* file = fopen(fileName, "w");
    swl_print_args_t tmpArgs = g_swl_print_jsonCompact;
    assert_true(swl_tta_fprintLists(&myTestTTType, file, array, arraySize, 0, false, &tmpArgs));
    fclose(file);
}

static void test_swl_tta_fPrintLists(void** state _UNUSED) {
    char buffer[256];
    memset(buffer, 0, sizeof(buffer));

    s_fPrintLists(&myTestTable, NR_VALUES, "fprint/printListTest.txt");
    s_fPrintLists(&myTestTableMasked, ALL_MASK, "fprint/printListMask.txt");
    s_fPrintLists(&zeroValues, 1, "fprint/printListZeros.txt");
    s_fPrintLists(&zeroValues, 0, "fprint/printListEmpty.txt");
}

static void s_fPrintMaps(const swl_tuple_t* array, size_t arraySize, const char* fileName) {
    FILE* file = fopen(fileName, "w");
    swl_print_args_t tmpArgs = g_swl_print_jsonCompact;
    assert_true(swl_tta_fprintMaps(&myTestTTType, file, array, arraySize, 0, true, &tmpArgs, names, "History"));
    fclose(file);
}


static void test_swl_tta_fPrintMaps(void** state _UNUSED) {
    char buffer[256];
    memset(buffer, 0, sizeof(buffer));

    s_fPrintMaps(&myTestTable, NR_VALUES, "fprint/printMapTest.txt");
    s_fPrintMaps(&myTestTableMasked, ALL_MASK, "fprint/printMapMask.txt");
    s_fPrintMaps(&zeroValues, 1, "fprint/printMapZeros.txt");
    s_fPrintMaps(&zeroValues, 0, "fprint/printMapEmpty.txt");
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tta_getValue),
        cmocka_unit_test(test_swl_tta_getReference),
        cmocka_unit_test(test_swl_tta_getTuple),
        cmocka_unit_test(test_swl_tta_getMatchingValue),
        cmocka_unit_test(test_swl_tta_getMatchingTuple),
        cmocka_unit_test(test_swl_tta_getMatchingTupleInRangeIncr),
        cmocka_unit_test(test_swl_tta_getMatchingTupleInRangeDecr),
        cmocka_unit_test(test_swl_tta_getMatchingTupleInRangeAssert),
        cmocka_unit_test(test_swl_tta_getMatchingXByTuple),
        cmocka_unit_test(test_swl_tta_equals),
        cmocka_unit_test(test_swl_tta_setValue),
        cmocka_unit_test(test_swl_tta_setTuple),
        cmocka_unit_test(test_swl_tta_printLists),
        cmocka_unit_test(test_swl_tta_printMaps),
        cmocka_unit_test(test_swl_tta_fPrintLists),
        cmocka_unit_test(test_swl_tta_fPrintMaps),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}

