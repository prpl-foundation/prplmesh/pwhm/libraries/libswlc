/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "test-toolbox/ttb.h"
#include "swl/swl_common.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common_chanspec.h"
#include "swl/ttb/swl_ttb.h"

#include "test_swlc_type_testCommon.h"


#define DATASET_SIZE 5

static swl_chanspec_t dataSet[DATASET_SIZE] = {
    {.channel = 1, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ },
    {.channel = 5, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_5GHZ },
    {.channel = 6, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ },
    {.channel = 11, .bandwidth = SWL_BW_160MHZ, .band = SWL_FREQ_BAND_EXT_NONE },
    {.channel = 13, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_AUTO },
};


static swl_chanspec_t* nullSwlChanSpec = NULL;

const char* dataSetStr = "1/20,5/40,6/80,11/160,13/20";


#define DATASET_PART_SIZE 2
const char* dataSetPartStr = "1/20,5/40";

#define NR_INVALID_STR 4
const char* invalidStr[NR_INVALID_STR] = {
    "1/,5/40,6/80,11/160,13/20",
    "1//20,5/40,6/80,11/160,13/20",
    "1456489764351321897897653132156498798767464131684897644641313484313//20,5/40,6/80,11/160,13/20",
    NULL,
};


#define CHECK_ENTRIES(spec, size) \
    entriesMatch(spec, dataSet, size, __FILE__, __LINE__)


static void entriesMatch(swl_chanspec_t* spec, swl_chanspec_t* testdata, uint32_t dataSize, char* file _UNUSED, uint32_t line _UNUSED) {
    uint32_t index = 0;
    for(index = 0; index < dataSize; index++) {
        ttb_assert_int_eq(spec[index].channel, testdata[index].channel);
        ttb_assert_int_eq(spec[index].bandwidth, testdata[index].bandwidth);
    }
    return;
}

/* Test Functions starts from here */

static void test_swl_chanspec_arrayFromChar(void** state _UNUSED) {
    swl_chanspec_t spec1[64];
    int resultSize = swl_chanspec_arrayFromChar(spec1, sizeof(spec1) / sizeof(swl_chanspec_t), dataSetStr);
    ttb_assert_int_eq(DATASET_SIZE, resultSize);
    CHECK_ENTRIES(spec1, DATASET_SIZE);

    resultSize = swl_chanspec_arrayFromChar(spec1, sizeof(spec1) / sizeof(swl_chanspec_t), dataSetPartStr);
    ttb_assert_int_eq(DATASET_PART_SIZE, resultSize);
    CHECK_ENTRIES(spec1, DATASET_PART_SIZE);

    /*Invalid Check*/
    for(int i = 0; i < NR_INVALID_STR; i++) {
        ttb_assert_addPrint("%i %s", i, invalidStr[i]);
        resultSize = swl_chanspec_arrayFromChar(spec1, 10, invalidStr[i]);
        ttb_assert_true(resultSize < 0);
        ttb_assert_removeLastPrint();
    }
}


static void test_swl_chanspec_arrayToChar(void** state _UNUSED) {

    char buffer[320] = {0};
    assert_true(swl_chanspec_arrayToChar(buffer, sizeof(buffer), dataSet, DATASET_SIZE));
    assert_string_equal(buffer, dataSetStr);

    assert_true(swl_chanspec_arrayToChar(buffer, sizeof(buffer), dataSet, DATASET_PART_SIZE));
    assert_string_equal(buffer, dataSetPartStr);

    assert_false(swl_chanspec_arrayToChar(NULL, sizeof(buffer), dataSet, DATASET_PART_SIZE));
    assert_false(swl_chanspec_arrayToChar(buffer, 5, dataSet, DATASET_SIZE));
}

static void test_swl_chanspec_isValidBand(void** state _UNUSED) {
    assert_true(swl_chanspec_isValidBand(SWL_FREQ_BAND_2_4GHZ));
    assert_true(swl_chanspec_isValidBand(SWL_FREQ_BAND_5GHZ));
    assert_true(swl_chanspec_isValidBand(SWL_FREQ_BAND_6GHZ));
    for(int i = SWL_FREQ_BAND_MAX; i < 100; i++) {
        assert_false(swl_chanspec_isValidBand(i));
    }
}

static void test_swl_chanspec_isValidBandExt(void** state _UNUSED) {
    assert_true(swl_chanspec_isValidBandExt(SWL_FREQ_BAND_EXT_2_4GHZ));
    assert_true(swl_chanspec_isValidBandExt(SWL_FREQ_BAND_EXT_5GHZ));
    assert_true(swl_chanspec_isValidBandExt(SWL_FREQ_BAND_EXT_6GHZ));
    for(int i = SWL_FREQ_BAND_EXT_NONE; i < 100; i++) {
        assert_false(swl_chanspec_isValidBandExt(i));
    }
}

static void test_swl_chanspec_getFreq(void** state _UNUSED) {

    bool testflag;
    swl_freqBand_e retBand = SWL_FREQ_BAND_2_4GHZ;

    retBand = swl_chanspec_getFreq(nullSwlChanSpec, SWL_FREQ_BAND_6GHZ, NULL);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[0], SWL_FREQ_BAND_6GHZ, NULL);
    ttb_assert_int_eq(SWL_FREQ_BAND_2_4GHZ, retBand);

    retBand = swl_chanspec_getFreq(nullSwlChanSpec, SWL_FREQ_BAND_6GHZ, &testflag);
    assert_false(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[0], SWL_FREQ_BAND_6GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_2_4GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[1], SWL_FREQ_BAND_2_4GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_EXT_5GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[2], SWL_FREQ_BAND_2_4GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[3], SWL_FREQ_BAND_6GHZ, &testflag);
    assert_false(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_getFreq(&dataSet[4], SWL_FREQ_BAND_5GHZ, &testflag);
    assert_false(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_5GHZ, retBand);

}

static void test_swl_chanspec_freqBandExtToFreqBand(void** state _UNUSED) {

    bool testflag;
    swl_freqBand_e retBand;

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_2_4GHZ, SWL_FREQ_BAND_6GHZ, NULL);
    ttb_assert_int_eq(SWL_FREQ_BAND_2_4GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_NONE, SWL_FREQ_BAND_6GHZ, NULL);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_2_4GHZ, SWL_FREQ_BAND_6GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_2_4GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_5GHZ, SWL_FREQ_BAND_2_4GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_5GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_6GHZ, SWL_FREQ_BAND_2_4GHZ, &testflag);
    assert_true(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_6GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_NONE, SWL_FREQ_BAND_2_4GHZ, &testflag);
    assert_false(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_2_4GHZ, retBand);

    retBand = swl_chanspec_freqBandExtToFreqBand(SWL_FREQ_BAND_EXT_AUTO, SWL_FREQ_BAND_5GHZ, &testflag);
    assert_false(testflag);
    ttb_assert_int_eq(SWL_FREQ_BAND_5GHZ, retBand);

}

typedef struct {
    swl_chanspec_t chanspec;
    uint32_t nrChannelInBand;
    swl_channel_t centreChannel;
    swl_channel_t baseChannel;
    swl_channel_t channels[SWL_BW_CHANNELS_MAX];
    swl_channel_t testChannel;
    bool isInChanspec;
    swl_channel_t complChannel;
    uint32_t ctrlChanFreq;
    uint32_t centreChanFreq;
    bool isDfs;
    bool isWeather;
    bool isHighPower;
    swl_radBw_e radBw;
} swl_chanspecChanTestData_t;

const swl_chanspecChanTestData_t testChanData[] = {
    {SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ), 1, 1, 1, {1, 0, 0, 0, 0, 0, 0, 0}, 2, false, -1, 2412, 2412, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(1, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ), 5, 3, 1, {1, 2, 3, 4, 5, 0, 0, 0}, 2, true, -1, 2412, 2422, false, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW_EXT(5, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_CHANSPEC_EXT_LOW, 0), 5, 3, 1, {1, 2, 3, 4, 5, 0, 0, 0}, 2, true, -1, 2432, 2422, false, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW_EXT(5, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_CHANSPEC_EXT_HIGH, 0), 5, 7, 5, {5, 6, 7, 8, 9, 0, 0, 0}, 2, false, -1, 2432, 2442, false, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW(13, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ), 1, 13, 13, {13, 0, 0, 0, 0, 0, 0, 0}, 13, true, -1, 2472, 2472, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(14, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ), 1, 14, 14, {14, 0, 0, 0, 0, 0, 0, 0}, 13, false, -1, 2484, 2484, false, false, false, SWL_RAD_BW_20MHZ},

    {SWL_CHANSPEC_NEW(36, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ), 1, 36, 36, {36, 0, 0, 0, 0, 0, 0, 0}, 36, true, -1, 5180, 5180, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(56, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ), 2, 54, 52, {52, 56, 0, 0, 0, 0, 0, 0}, 60, false, 52, 5280, 5270, true, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW(112, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ), 4, 106, 100, {100, 104, 108, 112, 0, 0, 0, 0}, 110, false, 100, 5560, 5530, true, false, true, SWL_RAD_BW_80MHZ},
    {SWL_CHANSPEC_NEW(128, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ), 8, 114, 100, {100, 104, 108, 112, 116, 120, 124, 128}, 100, true, 100, 5640, 5570, true, true, true, SWL_RAD_BW_160MHZ},
    {SWL_CHANSPEC_NEW(144, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ), 4, 138, 132, {132, 136, 140, 144, 0, 0, 0, 0}, 128, false, 132, 5720, 5690, true, false, true, SWL_RAD_BW_80MHZ},

    {SWL_CHANSPEC_NEW(161, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ), 1, 161, 161, {161, 0, 0, 0, 0, 0, 0, 0}, 161, true, -1, 5805, 5805, false, false, true, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(161, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ), 2, 159, 157, {157, 161, 0, 0, 0, 0, 0, 0}, 153, false, 157, 5805, 5795, false, false, true, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW(161, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ), 4, 155, 149, {149, 153, 157, 161, 0, 0, 0, 0}, 165, false, 149, 5805, 5775, false, false, true, SWL_RAD_BW_80MHZ},
    {SWL_CHANSPEC_NEW(161, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ), 8, 163, 149, {149, 153, 157, 161, 165, 169, 173, 177}, 149, true, 165, 5805, 5815, false, false, true, SWL_RAD_BW_160MHZ},

    {SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ), 1, 1, 1, {1, 0, 0, 0, 0, 0, 0, 0}, 1, true, -1, 5955, 5955, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(1, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_6GHZ), 2, 3, 1, {1, 5, 0, 0, 0, 0, 0, 0}, 17, false, 5, 5955, 5965, false, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW(1, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_6GHZ), 4, 7, 1, {1, 5, 9, 13, 0, 0, 0, 0}, 5, true, 9, 5955, 5985, false, false, false, SWL_RAD_BW_80MHZ},
    {SWL_CHANSPEC_NEW(1, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_6GHZ), 8, 15, 1, {1, 5, 9, 13, 17, 21, 25, 29}, 37, false, 17, 5955, 6025, false, false, false, SWL_RAD_BW_160MHZ},
    {SWL_CHANSPEC_NEW(2, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ), 1, 2, 2, {2, 0, 0, 0, 0, 0, 0, 0}, 2, true, -1, 5935, 5935, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(29, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_6GHZ), 8, 15, 1, {1, 5, 9, 13, 17, 21, 25, 29}, 3, false, 1, 6095, 6025, false, false, false, SWL_RAD_BW_160MHZ},
    {SWL_CHANSPEC_NEW(189, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ), 1, 189, 189, {189, 0, 0, 0, 0, 0, 0, 0}, 189, true, -1, 6895, 6895, false, false, false, SWL_RAD_BW_20MHZ},
    {SWL_CHANSPEC_NEW(189, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_6GHZ), 2, 187, 185, {185, 189, 0, 0, 0, 0, 0, 0}, 187, false, 185, 6895, 6885, false, false, false, SWL_RAD_BW_40MHZ},
    {SWL_CHANSPEC_NEW(189, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_6GHZ), 4, 183, 177, {177, 181, 185, 189, 0, 0, 0, 0}, 173, false, 177, 6895, 6865, false, false, false, SWL_RAD_BW_80MHZ},
    {SWL_CHANSPEC_NEW(189, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_6GHZ), 8, 175, 161, {161, 165, 169, 173, 177, 181, 185, 189}, 173, true, 161, 6895, 6825, false, false, false, SWL_RAD_BW_160MHZ},
    {SWL_CHANSPEC_NEW_EXT(69, SWL_BW_320MHZ, SWL_FREQ_BAND_EXT_6GHZ, SWL_CHANSPEC_EXT_LOW, 0), 16, 95, 65,
        {65, 69, 73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125}, 101, true, 97, 6295, 6425, false, false, false, SWL_RAD_BW_320MHZ1},
    {SWL_CHANSPEC_NEW_EXT(69, SWL_BW_320MHZ, SWL_FREQ_BAND_EXT_6GHZ, SWL_CHANSPEC_EXT_HIGH, 0), 16, 63, 33,
        {33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93}, 61, true, 33, 6295, 6265, false, false, false, SWL_RAD_BW_320MHZ2},

};

static void test_swl_chanspec_getNrChannelsInBand(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_chanspec_getNrChannelsInBand(&testChanData[i].chanspec), testChanData[i].nrChannelInBand);
    }
}

static void test_swl_chanspec_getNrChannelsPerFreqAndBw(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_chanspec_getNrChannelsPerFreqAndBw(testChanData[i].chanspec.band, testChanData[i].chanspec.bandwidth),
                          testChanData[i].nrChannelInBand);
    }
}

static void test_swl_chanspec_getBwFromFreqCtrlCentre(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%u) %s %i", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, testChanData[i].centreChannel);
        swl_bandwidth_e bw = swl_chanspec_getBwFromFreqCtrlCentre(testChanData[i].chanspec.band, testChanData[i].chanspec.channel,
                                                                  testChanData[i].centreChannel);
        ttb_assert_int_eq(bw, testChanData[i].chanspec.bandwidth);
        ttb_assert_removeLastPrint();
    }
}

static void test_swl_chanspec_fromFreqAndCentreChannel(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        swl_chanspec_t targetChanspec = SWL_CHANSPEC_EMPTY;
        swl_rc_ne ret = swl_chanspec_fromFreqAndCentreChannel(&targetChanspec, testChanData[i].chanspec.band, testChanData[i].centreChannel);
        ttb_assert_int_eq(ret, SWL_RC_OK);
        swl_chanspec_t testChanspec = testChanData[i].chanspec;
        if((testChanspec.band == SWL_FREQ_BAND_EXT_2_4GHZ) && (testChanspec.bandwidth == SWL_BW_40MHZ)) {
            testChanspec.channel = testChanData[i].centreChannel;
            testChanspec.bandwidth = SWL_BW_20MHZ;
        } else {
            testChanspec.channel = testChanData[i].baseChannel;
        }
        swl_ttb_assertTypeEquals(&gtSwl_type_chanspecExt, &testChanspec, &targetChanspec);
    }
}

static void test_swl_chanspec_fromFreqCtrlCentre(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%u) %s %i", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, testChanData[i].centreChannel);
        swl_chanspec_t targetChanspec = SWL_CHANSPEC_EMPTY;
        swl_rc_ne ret = swl_chanspec_fromFreqCtrlCentre(&targetChanspec, testChanData[i].chanspec.band, testChanData[i].chanspec.channel, testChanData[i].centreChannel);
        ttb_assert_int_eq(ret, SWL_RC_OK);

        swl_chanspec_t testChanspec = testChanData[i].chanspec;
        if((testChanspec.band == SWL_FREQ_BAND_EXT_2_4GHZ) && (testChanspec.bandwidth == SWL_BW_40MHZ) && (testChanspec.extensionHigh == SWL_CHANSPEC_EXT_AUTO)) {
            testChanspec.extensionHigh = (testChanspec.channel > 6 ? SWL_CHANSPEC_EXT_LOW : SWL_CHANSPEC_EXT_HIGH);
        }

        swl_ttb_assertTypeEquals(&gtSwl_type_chanspecExt, &testChanspec, &targetChanspec);
        ttb_assert_removeLastPrint();
    }
}

static void test_swl_chanspec_getCentreChannel(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_chanspec_getCentreChannel(&testChanData[i].chanspec), testChanData[i].centreChannel);
    }
}

static void test_swl_chanspec_getBaseChannel(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_chanspec_getBaseChannel(&testChanData[i].chanspec), testChanData[i].baseChannel);
    }
}

static void test_swl_chanspec_getChannelsInChanspec(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        swl_channel_t chanList[SWL_BW_CHANNELS_MAX] = {0};
        swl_chanspec_getChannelsInChanspec(&testChanData[i].chanspec, chanList, SWL_BW_CHANNELS_MAX);
        swl_ttb_assertTypeArrayEquals(&gtSwl_type_uint8, chanList, SWL_BW_CHANNELS_MAX, testChanData[i].channels, SWL_BW_CHANNELS_MAX);
    }
}

static void test_swl_channel_isInChanspec(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%i %s %u", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, testChanData[i].testChannel);
        ttb_assert_int_eq(swl_channel_isInChanspec(&testChanData[i].chanspec, testChanData[i].testChannel), testChanData[i].isInChanspec);
        ttb_assert_removeLastPrint();
    }
}

static void test_swl_channel_getComplementaryBaseChannel(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_channel_getComplementaryBaseChannel(&testChanData[i].chanspec), testChanData[i].complChannel);
    }
}

static void test_swl_chanspec_toRadBw(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%i) %s %s", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, swl_radBw_str[testChanData[i].radBw]);
        ttb_assert_int_eq(swl_chanspec_toRadBw(&testChanData[i].chanspec), testChanData[i].radBw);
        ttb_assert_removeLastPrint();
    }
}

static void test_swl_chanspec_fromDmExt(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%i) %s %s", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, swl_radBw_str[testChanData[i].radBw]);
        swl_chanspec_t tmpSpec;
        memset(&tmpSpec, 0, sizeof(swl_chanspec_t));
        ttb_assert_int_eq(SWL_RC_OK, swl_chanspec_fromDmExt(&tmpSpec, testChanData[i].chanspec.channel, testChanData[i].radBw, testChanData[i].chanspec.band, 0, testChanData[i].chanspec.extensionHigh));
        swl_ttb_assertTypeEquals(&gtSwl_type_chanspecExt, &testChanData[i].chanspec, &tmpSpec);
        ttb_assert_removeLastPrint();
    }

    // failure testing
    swl_chanspec_t tmpSpec;
    memset(&tmpSpec, 0, sizeof(swl_chanspec_t));
    ttb_assert_int_eq(SWL_RC_INVALID_PARAM, swl_chanspec_fromDmExt(NULL, testChanData[0].chanspec.channel, testChanData[0].radBw, testChanData[0].chanspec.band, 0, testChanData[0].chanspec.extensionHigh));
    ttb_assert_int_eq(SWL_RC_INVALID_PARAM, swl_chanspec_fromDmExt(&tmpSpec, testChanData[0].chanspec.channel, SWL_RAD_BW_MAX, testChanData[0].chanspec.band, 0, testChanData[0].chanspec.extensionHigh));
}

static void test_swl_chanspec_fromDm(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%i) %s %s", i, swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf, swl_radBw_str[testChanData[i].radBw]);

        swl_chanspec_t tmpSpec = swl_chanspec_fromDm(testChanData[i].chanspec.channel, testChanData[i].radBw, testChanData[i].chanspec.band);
        swl_chanspec_t targetSpec = testChanData[i].chanspec;

        //Exception cases: 2.4GHz / 40MHz
        if((targetSpec.band == SWL_FREQ_BAND_EXT_2_4GHZ) && (targetSpec.bandwidth == SWL_BW_40MHZ) && (targetSpec.extensionHigh != SWL_CHANSPEC_EXT_AUTO)) {
            targetSpec.extensionHigh = SWL_CHANSPEC_EXT_AUTO;
        }

        swl_ttb_assertTypeEquals(&gtSwl_type_chanspecExt, &targetSpec, &tmpSpec);
        ttb_assert_removeLastPrint();
    }

}


static void test_swl_chanspec_channelFromMHz(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_addPrint("%s", swl_typeChanspecExt_toBuf32(testChanData[i].chanspec).buf);
        swl_chanspec_t testChanspec = {0};
        //test base
        ttb_assert_int_eq(swl_chanspec_channelFromMHz(&testChanspec, testChanData[i].ctrlChanFreq), SWL_RC_OK);
        ttb_assert_int_eq(testChanspec.bandwidth, SWL_BW_AUTO);
        ttb_assert_int_eq(testChanspec.channel, testChanData[i].chanspec.channel);
        ttb_assert_int_eq(testChanspec.band, testChanData[i].chanspec.band);
        memset(&testChanspec, 0, sizeof(swl_chanspec_t));

        ttb_assert_int_eq(swl_chanspec_channelFromMHz(&testChanspec, testChanData[i].centreChanFreq), SWL_RC_OK);
        ttb_assert_int_eq(testChanspec.bandwidth, SWL_BW_AUTO);
        ttb_assert_int_eq(testChanspec.channel, testChanData[i].centreChannel);
        ttb_assert_int_eq(testChanspec.band, testChanData[i].chanspec.band);
        ttb_assert_removeLastPrint();
    }
}

swl_chanspec_t invalidChanspecChanToMhz[] = {
    SWL_CHANSPEC_NEW(0, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
    SWL_CHANSPEC_NEW(15, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
    SWL_CHANSPEC_NEW(51, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),

    SWL_CHANSPEC_NEW(0, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
    SWL_CHANSPEC_NEW(201, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
    SWL_CHANSPEC_NEW(255, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),


    SWL_CHANSPEC_NEW(0, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ),
    SWL_CHANSPEC_NEW(234, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ),
};

static void test_swl_chanspec_channelToMHz(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        uint32_t freq = 0;
        //test base
        ttb_assert_int_eq(swl_chanspec_channelToMHz(&testChanData[i].chanspec, &freq), SWL_RC_OK);
        ttb_assert_int_eq(freq, testChanData[i].ctrlChanFreq);

        ttb_assert_int_eq(swl_chanspec_channelToMHzDef(&testChanData[i].chanspec, 0), testChanData[i].ctrlChanFreq);
    }
    // Test invalid cases
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(invalidChanspecChanToMhz); i++) {
        uint32_t freq = 0;
        printf("%u %s\n", i, swl_typeChanspec_toBuf32(invalidChanspecChanToMhz[i]).buf);
        ttb_assert_int_eq(swl_chanspec_channelToMHz(&invalidChanspecChanToMhz[i], &freq), SWL_RC_INVALID_PARAM);

        uint32_t defFreq = 1111;

        ttb_assert_int_eq(swl_chanspec_channelToMHzDef(&invalidChanspecChanToMhz[i], defFreq), defFreq);
    }
}

static void test_swl_chanspec_fromMHz(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        swl_chanspec_t testChanspec = {0};
        //test base
        ttb_assert_addPrint("%s %u \n", swl_typeChanspec_toBuf32(testChanData[i].chanspec).buf, testChanData[i].centreChanFreq);

        ttb_assert_int_eq(swl_chanspec_fromMHz(&testChanspec, testChanData[i].centreChanFreq), SWL_RC_OK);

        ttb_assert_int_eq(testChanspec.band, testChanData[i].chanspec.band);

        if(testChanspec.band == SWL_FREQ_BAND_EXT_2_4GHZ) {
            ttb_assert_int_eq(testChanspec.bandwidth, SWL_BW_20MHZ);
            ttb_assert_int_eq(testChanspec.channel, testChanData[i].centreChannel);
        } else {
            ttb_assert_int_eq(testChanspec.bandwidth, testChanData[i].chanspec.bandwidth);
            ttb_assert_int_eq(testChanspec.channel, testChanData[i].baseChannel);
        }

        memset(&testChanspec, 0, sizeof(swl_chanspec_t));
        ttb_assert_clearPrint();
    }
}


static void test_swl_chanspec_toMHz(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        uint32_t freq = 0;
        //test base
        ttb_assert_int_eq(swl_chanspec_toMHz(&testChanData[i].chanspec, &freq), SWL_RC_OK);
        ttb_assert_int_eq(freq, testChanData[i].centreChanFreq);

        ttb_assert_int_eq(swl_chanspec_toMHzDef(&testChanData[i].chanspec, 0), testChanData[i].centreChanFreq);
    }
    // Test invalid cases
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(invalidChanspecChanToMhz); i++) {
        uint32_t freq = 0;
        printf("%u %s\n", i, swl_typeChanspec_toBuf32(invalidChanspecChanToMhz[i]).buf);
        ttb_assert_int_eq(swl_chanspec_channelToMHz(&invalidChanspecChanToMhz[i], &freq), SWL_RC_INVALID_PARAM);

        uint32_t defFreq = 1111;

        ttb_assert_int_eq(swl_chanspec_channelToMHzDef(&invalidChanspecChanToMhz[i], defFreq), defFreq);
    }
}

static void test_swl_chanspec_validChannels(void** state _UNUSED) {
    assert_true(swl_channel_is2g(1));
    assert_true(swl_channel_is2g(11));
    assert_false(swl_channel_is2g(0));
    assert_false(swl_channel_is2g(36));
    assert_false(swl_channel_is2g(37));

    assert_true(swl_channel_is5g(36));
    assert_true(swl_channel_is5g(52));
    assert_true(swl_channel_is5g(104));
    assert_true(swl_channel_is5g(132));
    assert_true(swl_channel_is5g(153));
    assert_true(swl_channel_is5g(165));
    assert_true(swl_channel_is5g(169));
    assert_false(swl_channel_is5g(5));
    assert_false(swl_channel_is5g(37));
    assert_false(swl_channel_is5g(62));
    assert_false(swl_channel_is5g(84));
    assert_false(swl_channel_is5g(102));
    assert_false(swl_channel_is5g(105));
    assert_false(swl_channel_is5g(148));
    assert_false(swl_channel_is5g(164));

    assert_true(swl_channel_is6g(5));
    assert_true(swl_channel_is6g(37));
    assert_true(swl_channel_is6g(229));
    assert_false(swl_channel_is6g(0));
    assert_false(swl_channel_is6g(36));
    assert_false(swl_channel_is6g(67));
    assert_false(swl_channel_is6g(237));

    assert_true(swl_channel_is6gPsc(5));
    assert_true(swl_channel_is6gPsc(37));
    assert_true(swl_channel_is6gPsc(229));
    assert_false(swl_channel_is6gPsc(1));
    assert_false(swl_channel_is6gPsc(0));
    assert_false(swl_channel_is6gPsc(189));

    assert_true(swl_channel_isDfs(52));
    assert_true(swl_channel_isDfs(100));
    assert_true(swl_channel_isDfs(144));
    assert_false(swl_channel_isDfs(0));
    assert_false(swl_channel_isDfs(36));
    assert_false(swl_channel_isDfs(149));
    assert_false(swl_channel_isDfs(11));

    assert_true(swl_channel_isWeather(120));
    assert_true(swl_channel_isWeather(124));
    assert_true(swl_channel_isWeather(128));
    assert_false(swl_channel_isWeather(116));
    assert_false(swl_channel_isWeather(132));
    assert_false(swl_channel_isWeather(1));
    assert_false(swl_channel_isWeather(121));

    assert_true(swl_channel_isHighPower(100));
    assert_true(swl_channel_isHighPower(128));
    assert_true(swl_channel_isHighPower(165));
    assert_false(swl_channel_isHighPower(52));
    assert_false(swl_channel_isHighPower(1));

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testChanData); i++) {
        ttb_assert_int_eq(swl_chanspec_isDfs(testChanData[i].chanspec), testChanData[i].isDfs);
        ttb_assert_int_eq(swl_chanspec_isWeather(testChanData[i].chanspec), testChanData[i].isWeather);
        ttb_assert_int_eq(swl_chanspec_isHighPower(testChanData[i].chanspec), testChanData[i].isHighPower);
    }
}

typedef struct {
    swl_channel_t channels[128];
    swl_freqBandExt_e expectedFreqBandExt;
} testChanBandMap_t;
static void test_swl_chanspec_freqBandExtFromBaseChannel(void** state _UNUSED) {
    testChanBandMap_t testChanBandMaps[] = {
        {
            {
                1, 5, 9, 13,             /* first channels of UNII-5 (6GHz) have same index as in 2.4GHz */
                149, 153, 157, 161, 165, /* channels of UNII-3 (5GHz) have same index as in UNII-7 (6GHz) */
                169, 173, 177, 181,      /* channels of UNII-4 (5GHz) have same index as in UNII-7 (6GHz) */
            },
            SWL_FREQ_BAND_EXT_AUTO,      /* => can not deduce frequency band from channel index, as having multiple mappings. */
        },
        {
            {
                2, 3, 4, 6, 7, 8, 10, 11, 12, 14, /* remaining channels of 2.4GHz */
            },
            SWL_FREQ_BAND_EXT_2_4GHZ,             /* => explicit mapping to freq band 2.4GHz. */
        },
        {
            {
                36, 40, 44, 48,                                             /* channels of UNII-1 (5GHz) */
                52, 56, 60, 64,                                             /* channels of UNII-2A (5GHz) */
                100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, /* channels of UNII-2C (5GHz) */
            },
            SWL_FREQ_BAND_EXT_5GHZ,                                         /* => explicit mapping to freq band 5GHz. */
        },
        {
            {
                17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77,
                81, 85, 89, 93,                                                  /* remaining channels of UNII-5 (6GHz) */
                97, 101, 105, 109, 113,                                          /* channels of UNII-6 (6GHz) */
                117, 121, 125, 129, 133, 137, 141, 145,                          /* remaining channels of UNII-7 (6GHz) */
                185, 189, 193, 197, 201, 205, 209, 213, 217, 221, 225, 229, 233, /* channels of UNII-8 (6GHz) */
            },
            SWL_FREQ_BAND_EXT_6GHZ,                                              /* => explicit mapping to freq band 6GHz. */
        },
    };
    for(uint32_t chanId = 1; chanId < SWL_CHANNEL_6G_END + 10; chanId++) {
        bool match = false;
        swl_freqBandExt_e freqBandExt = swl_chanspec_freqBandExtFromBaseChannel(chanId);
        for(uint32_t group = 0; group < SWL_ARRAY_SIZE(testChanBandMaps); group++) {
            for(uint32_t pos = 0; pos < SWL_ARRAY_SIZE(testChanBandMaps[group].channels) && testChanBandMaps[group].channels[pos] > 0; pos++) {
                if(chanId == testChanBandMaps[group].channels[pos]) {
                    ttb_assert_int_eq(freqBandExt, testChanBandMaps[group].expectedFreqBandExt);
                    match = true;
                    break;
                }
            }
            if(match) {
                break;
            }
        }
        if(match) {
            continue;
        }
        ttb_assert_int_eq(freqBandExt, SWL_FREQ_BAND_EXT_NONE); /* no match at all: wrong channel index*/
    }
}

typedef struct {
    swl_operatingClass_t operClass;
    swl_operatingClass_t localOperClass[SWL_OP_CLASS_COUNTRY_MAX];
    swl_freqBandExt_e expectedFreqBandExt;
    swl_bandwidth_e expectedBw;
    swl_uniiBand_m expectedUniiMask;
    swl_phymode_ne phyMode;
} testOperClassUniiMaskMap_t;
static void test_swl_chanspec_operClassToChanInfo(void** state _UNUSED) {
    testOperClassUniiMaskMap_t testOperClassUniiMaskMaps[] = {
        {12, {0, 0, 0, 0}, SWL_FREQ_BAND_EXT_NONE, SWL_BW_AUTO, 0, SWL_PHYMODE_UNKNOWN},
        {81, {12, 4, 30, 7}, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_BW_20MHZ, 0, SWL_PHYMODE_HT},
        {84, {33, 12, 57, 9}, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_BW_40MHZ, 0, SWL_PHYMODE_HT},
        {115, {1, 1, 1, 1}, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_20MHZ, M_SWL_BAND_UNII_1, SWL_PHYMODE_VHT},
        {120, {28, 9, 42, 0 }, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_40MHZ, M_SWL_BAND_UNII_2A, SWL_PHYMODE_VHT},
        {122, {24, 7, 39, 0}, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_40MHZ, M_SWL_BAND_UNII_2C, SWL_PHYMODE_VHT},
        {128, {128, 128, 128, 128}, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_80MHZ, M_SWL_BAND_UNII_ALL_5G, SWL_PHYMODE_VHT},
        {129, {129, 129, 129, 129}, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_160MHZ, M_SWL_BAND_UNII_ALL_5G, SWL_PHYMODE_VHT},
        {131, {131, 131, 131, 131}, SWL_FREQ_BAND_EXT_6GHZ, SWL_BW_20MHZ, M_SWL_BAND_UNII_ALL_6G, SWL_PHYMODE_HE},
        {132, {132, 132, 132, 132}, SWL_FREQ_BAND_EXT_6GHZ, SWL_BW_40MHZ, M_SWL_BAND_UNII_ALL_6G, SWL_PHYMODE_HE},
        {133, {133, 133, 133, 133}, SWL_FREQ_BAND_EXT_6GHZ, SWL_BW_80MHZ, M_SWL_BAND_UNII_ALL_6G, SWL_PHYMODE_HE},
        {134, {134, 134, 134, 134}, SWL_FREQ_BAND_EXT_6GHZ, SWL_BW_160MHZ, M_SWL_BAND_UNII_ALL_6G, SWL_PHYMODE_HE},
        {140, {0, 0, 0, 0}, SWL_FREQ_BAND_EXT_NONE, SWL_BW_AUTO, 0, SWL_PHYMODE_UNKNOWN},
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testOperClassUniiMaskMaps); i++) {
        ttb_assert_int_eq(swl_chanspec_operClassToFreq(testOperClassUniiMaskMaps[i].operClass), testOperClassUniiMaskMaps[i].expectedFreqBandExt);
        ttb_assert_int_eq(swl_chanspec_operClassToBandwidth(testOperClassUniiMaskMaps[i].operClass), testOperClassUniiMaskMaps[i].expectedBw);
        ttb_assert_int_eq(swl_chanspec_operClassToUniiMask(testOperClassUniiMaskMaps[i].operClass), testOperClassUniiMaskMaps[i].expectedUniiMask);
        ttb_assert_int_eq(swl_chanspec_operClassToPhyMode(testOperClassUniiMaskMaps[i].operClass), testOperClassUniiMaskMaps[i].phyMode);
        //check for all supported countries
        for(uint8_t j = 0; j < SWL_OP_CLASS_COUNTRY_MAX; j++) {
            ttb_assert_addPrint("check num %u from %d  and i is %u \n", j, (int) SWL_OP_CLASS_COUNTRY_MAX, i);
            if(testOperClassUniiMaskMaps[i].localOperClass[j] == 0) { //in this case  global value =0
                ttb_assert_int_eq(swl_chanspec_localOperClassToFreq(testOperClassUniiMaskMaps[i].localOperClass[j], j), SWL_FREQ_BAND_EXT_NONE);
                ttb_assert_int_eq(swl_chanspec_localOperClassToBandwidth(testOperClassUniiMaskMaps[i].localOperClass[j], j), SWL_BW_AUTO);
                ttb_assert_int_eq(swl_chanspec_localOperClassToUniiMask(testOperClassUniiMaskMaps[i].localOperClass[j], j), 0);
                ttb_assert_int_eq(swl_chanspec_localOperClassToPhyMode(testOperClassUniiMaskMaps[i].localOperClass[j], j), SWL_PHYMODE_UNKNOWN);
            } else {
                ttb_assert_int_eq(swl_chanspec_localOperClassToFreq(testOperClassUniiMaskMaps[i].localOperClass[j], j), testOperClassUniiMaskMaps[i].expectedFreqBandExt);
                ttb_assert_int_eq(swl_chanspec_localOperClassToBandwidth(testOperClassUniiMaskMaps[i].localOperClass[j], j), testOperClassUniiMaskMaps[i].expectedBw);
                ttb_assert_int_eq(swl_chanspec_localOperClassToUniiMask(testOperClassUniiMaskMaps[i].localOperClass[j], j), testOperClassUniiMaskMaps[i].expectedUniiMask);
                ttb_assert_int_eq(swl_chanspec_localOperClassToPhyMode(testOperClassUniiMaskMaps[i].localOperClass[j], j), testOperClassUniiMaskMaps[i].phyMode);
            }
            ttb_assert_removeLastPrint();
        }
    }
}

typedef struct {
    swl_chanspec_t chanSpec;
    swl_operatingClass_t globalOperClass;
    swl_operatingClass_t localOperClass[SWL_OP_CLASS_COUNTRY_MAX];
} testOperatingClass_t;
static void test_swl_chanspec_getOperClass(void** state _UNUSED) {
    testOperatingClass_t testoperatingClass [] = {
        {{.band = SWL_FREQ_BAND_EXT_2_4GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 7}, 81, {12, 4, 30, 7}},
        {{.band = SWL_FREQ_BAND_EXT_2_4GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 14}, 82, {0, 0, 31, 0}},
        { {.band = SWL_FREQ_BAND_EXT_2_4GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 6}, 83, {32, 11, 56, 8}},
        {{.band = SWL_FREQ_BAND_EXT_2_4GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 12}, 84, {33, 12, 57, 9}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 36}, 115, {1, 1, 1, 1}},
        {{.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 56}, 118, {2, 2, 32, 2}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 104}, 121, {4, 3, 34, 0}},
        {{.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 157}, 124, {3, 0, 0, 0}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 161}, 124, {3, 0, 0, 0}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 173}, 125, {5, 17, 0, 3}},
        {{.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 36}, 116, {22, 5, 36, 4}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 40}, 117, {27, 8, 41, 0}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 60}, 119, {23, 6, 37, 5}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 56}, 120, {28, 9, 42, 0 }},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 108}, 122, {24, 7, 39, 0}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 128}, 123, {29, 10, 44, 0}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 165}, 126, {25, 0, 0, 6}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 169}, 127, {30, 0, 0, 0}},
        {{.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_80MHZ, .channel = 36}, 128, {128, 128, 128, 128}},
        {{.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_80MHZ, .channel = 56}, 128, {128, 128, 128, 128}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_80MHZ, .channel = 108}, 128, {128, 128, 128, 128}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_80MHZ, .channel = 128}, 128, {128, 128, 128, 128}},

        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_160MHZ, .channel = 56}, 129, {129, 129, 129, 129}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_160MHZ, .channel = 108}, 129, {129, 129, 129, 129}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_160MHZ, .channel = 128}, 129, {129, 129, 129, 129}},
        { {.band = SWL_FREQ_BAND_EXT_5GHZ, .bandwidth = SWL_BW_160MHZ, .channel = 108}, 129, {129, 129, 129, 129}},

        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 29}, 131, {131, 131, 131, 131}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 75}, 132, {132, 132, 132, 132}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_40MHZ, .channel = 37}, 132, {132, 132, 132, 132}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_80MHZ, .channel = 103}, 133, {133, 133, 133, 133}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_160MHZ, .channel = 111}, 134, {134, 134, 134, 134}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_20MHZ, .channel = 5}, 131, {131, 131, 131, 131}},
        { {.band = SWL_FREQ_BAND_EXT_6GHZ, .bandwidth = SWL_BW_320MHZ, .channel = 31}, 137, {137, 137, 137, 137}},
    };

    ttb_assert_int_eq(swl_chanspec_getOperClassDirect(100, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_20MHZ), 121);

    ttb_assert_int_eq(swl_chanspec_getOperClassDirect(1, SWL_FREQ_BAND_EXT_5GHZ, SWL_BW_20MHZ), 0);
    ttb_assert_int_eq(swl_chanspec_getOperClassDirect(1, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_BW_20MHZ), 81);
    ttb_assert_int_eq(swl_chanspec_getOperClassDirect(1, SWL_FREQ_BAND_EXT_6GHZ, SWL_BW_20MHZ), 131);

    ttb_assert_int_eq(swl_chanspec_getOperClassDirect(14, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_BW_20MHZ), 82);

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testoperatingClass); i++) {
        ttb_assert_int_eq(swl_chanspec_getOperClass(&(testoperatingClass[i].chanSpec)), testoperatingClass[i].globalOperClass);
        for(uint8_t j = 0; j < SWL_OP_CLASS_COUNTRY_MAX; j++) {
            ttb_assert_addPrint("check num %u from %d  and i is %u \n", j, (int) SWL_OP_CLASS_COUNTRY_MAX, i);
            ttb_assert_int_eq(swl_chanspec_getLocalOperClass(&(testoperatingClass[i].chanSpec), j), testoperatingClass[i].localOperClass[j]);
            ttb_assert_removeLastPrint();
        }
    }
}

//For chanspec testing, band does not matter

#define NR_CHANSPEC_TEST_VAL 14
swl_chanspec_t chanspecBase[NR_CHANSPEC_TEST_VAL] = {
    {.channel = 100, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 6, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 100, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_5GHZ},
    {.channel = 36, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_AUTO, .extensionHigh = SWL_CHANSPEC_EXT_AUTO},
    {.channel = 11, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ, .extensionHigh = SWL_CHANSPEC_EXT_AUTO},
    {.channel = 11, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ, .extensionHigh = SWL_CHANSPEC_EXT_HIGH},
    {.channel = 11, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ, .extensionHigh = SWL_CHANSPEC_EXT_LOW},
    {.channel = 52, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 52, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ},
    {.channel = 52, .bandwidth = SWL_BW_160MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 36, .bandwidth = SWL_BW_160MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 100, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 69, .bandwidth = SWL_BW_320MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ, .extensionHigh = SWL_CHANSPEC_EXT_LOW},
    {.channel = 69, .bandwidth = SWL_BW_320MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ, .extensionHigh = SWL_CHANSPEC_EXT_HIGH},
};
size_t chanspecCount[NR_CHANSPEC_TEST_VAL] = {3, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1};
swl_chanspec_t chanspecNotContains[NR_CHANSPEC_TEST_VAL] = {
    {.channel = 100, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_5GHZ},
    {.channel = 6, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 100, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ},
    {.channel = 40, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 44, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 46, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 10, .bandwidth = SWL_BW_40MHZ, .band = SWL_FREQ_BAND_EXT_2_4GHZ},
    {.channel = 56, .bandwidth = SWL_BW_20MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 60, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 60, .bandwidth = SWL_BW_160MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 0, .bandwidth = SWL_BW_160MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 0, .bandwidth = SWL_BW_80MHZ, .band = SWL_FREQ_BAND_EXT_AUTO},
    {.channel = 1, .bandwidth = SWL_BW_320MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ, .extensionHigh = SWL_CHANSPEC_EXT_LOW},
    {.channel = 5, .bandwidth = SWL_BW_320MHZ, .band = SWL_FREQ_BAND_EXT_6GHZ, .extensionHigh = SWL_CHANSPEC_EXT_HIGH}
};

const char* chanspecStrVal[NR_CHANSPEC_TEST_VAL] = {"100/80", "6/20", "100/80", "36/40",
    "11/40", "11/40-A", "11/40-B", "52/20", "52/80", "52/160", "36/160", "100/80", "69/320-1", "69/320-2"};

#define CHANSPEC_NR_BAD_VALS 5
const char* chanspecBadVals[CHANSPEC_NR_BAD_VALS] = {"abcd/ccdef",
    "168749898764564651321655486746846123186789761651984186161/123123123",
    "100123456789123456789123456789123456789/123123123",
    "100/80                              36",
    "1/"};

swlc_type_testCommonData_t testRadChanspecData = {
    .name = "chanspec",
    .testType = swl_type_chanspec,
    .serialStr = "100/80,6/20,100/80,36/40,11/40,11/40-A,11/40-B,52/20,52/80,52/160,36/160,100/80,69/320-1,69/320-2",
    .serialStr2 = "100/80;;6/20;;100/80;;36/40;;11/40;;11/40-A;;11/40-B;;52/20;;52/80;;52/160;;36/160;;100/80;;69/320-1;;69/320-2",
    .strValues = chanspecStrVal,
    .baseValues = &chanspecBase,
    .valueCount = chanspecCount,
    .notContains = &chanspecNotContains,
    .nrTestValues = NR_CHANSPEC_TEST_VAL,
    .badVals = chanspecBadVals,
    .nrBadVals = CHANSPEC_NR_BAD_VALS
};

const char* chanspecExtStrVal[NR_CHANSPEC_TEST_VAL] = {"-1_100/80", "-1_6/20", "5_100/80",
    "-1_36/40", "2_11/40", "2_11/40-A", "2_11/40-B", "-1_52/20", "6_52/80", "-1_52/160", "-1_36/160", "-1_100/80", "6_69/320-1", "6_69/320-2"};
size_t chanspecExtCount[NR_CHANSPEC_TEST_VAL] = {2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1};

#define NR_BAD_VALS 4
const char* chanspecExtBadVals[NR_BAD_VALS] = {"abcd/ccdef",
    "168749898764564651321655486746846123186789761651984186161_/123123123",
    "100_123456789123456789123456789123456789/123123123",
    "5_100/80                              36"};
swlc_type_testCommonData_t testChanspecExtData = {
    .name = "chanspecExt",
    .testType = swl_type_chanspecExt,
    .serialStr = "-1_100/80,-1_6/20,5_100/80,-1_36/40,2_11/40,2_11/40-A,2_11/40-B,-1_52/20,6_52/80,-1_52/160,-1_36/160,-1_100/80,6_69/320-1,6_69/320-2",
    .serialStr2 = "-1_100/80;;-1_6/20;;5_100/80;;-1_36/40;;2_11/40;;2_11/40-A;;2_11/40-B;;-1_52/20;;6_52/80;;-1_52/160;;-1_36/160;;-1_100/80;;6_69/320-1;;6_69/320-2",
    .strValues = chanspecExtStrVal,
    .baseValues = &chanspecBase,
    .valueCount = chanspecExtCount,
    .notContains = &chanspecNotContains,
    .nrTestValues = NR_CHANSPEC_TEST_VAL,
    .badVals = chanspecExtBadVals,
    .nrBadVals = NR_BAD_VALS
};

static void test_swl_chanspec_testEqualsOnStackAssignment(void** state _UNUSED) {
    swl_chanspec_t spec1 = SWL_CHANSPEC_NEW(100, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ);
    swl_chanspec_t spec2 = SWL_CHANSPEC_NEW(100, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ);
    assert_true(swl_type_equals(&gtSwl_type_chanspecExt, &spec1, &spec2));
    // note, a memcmp will fail due to memory not entirely being initialized.
}

static void test_swl_chanspec_highPowerCountry(void** state _UNUSED) {
    assert_false(swl_channel_isHighPowerWithCountry(100, SWL_OP_CLASS_COUNTRY_USA));
    assert_false(swl_channel_isHighPowerWithCountry(128, SWL_OP_CLASS_COUNTRY_USA));
    assert_true(swl_channel_isHighPowerWithCountry(149, SWL_OP_CLASS_COUNTRY_USA));
    assert_true(swl_channel_isHighPowerWithCountry(165, SWL_OP_CLASS_COUNTRY_USA));
    assert_false(swl_channel_isHighPowerWithCountry(52, SWL_OP_CLASS_COUNTRY_USA));
    assert_true(swl_channel_isHighPowerWithCountry(36, SWL_OP_CLASS_COUNTRY_USA));
    assert_false(swl_channel_isHighPowerWithCountry(1, SWL_OP_CLASS_COUNTRY_USA));

    assert_true(swl_channel_isHighPowerWithCountry(100, SWL_OP_CLASS_COUNTRY_EU));
    assert_true(swl_channel_isHighPowerWithCountry(128, SWL_OP_CLASS_COUNTRY_EU));
    assert_true(swl_channel_isHighPowerWithCountry(149, SWL_OP_CLASS_COUNTRY_EU));
    assert_true(swl_channel_isHighPowerWithCountry(165, SWL_OP_CLASS_COUNTRY_EU));
    assert_false(swl_channel_isHighPowerWithCountry(52, SWL_OP_CLASS_COUNTRY_EU));
    assert_false(swl_channel_isHighPowerWithCountry(36, SWL_OP_CLASS_COUNTRY_EU));
    assert_false(swl_channel_isHighPowerWithCountry(1, SWL_OP_CLASS_COUNTRY_EU));

    assert_int_equal(swl_chanspec_regulatoryDomainToCountry("EU"), SWL_OP_CLASS_COUNTRY_EU);
    assert_int_equal(swl_chanspec_regulatoryDomainToCountry("US"), SWL_OP_CLASS_COUNTRY_USA);
    assert_int_equal(swl_chanspec_regulatoryDomainToCountry("FR"), SWL_OP_CLASS_COUNTRY_EU);
    assert_int_equal(swl_chanspec_regulatoryDomainToCountry("IT"), SWL_OP_CLASS_COUNTRY_EU);
    assert_int_equal(swl_chanspec_regulatoryDomainToCountry(NULL), SWL_OP_CLASS_COUNTRY_UNKNOWN);
}

static void test_swl_chanspec_fromOperClass(void** state _UNUSED) {
    struct {
        swl_operatingClass_t inOpClass;
        swl_opClassCountry_e inRegion;
        swl_channel_t inChan;
        bool allowMultiMatch;
        swl_chanspec_t expecChSpec;
    } tests[] = {
        { // success: direct match of local USA operclass (12) (with valid channel 1) (eqv glob 81)
            12, SWL_OP_CLASS_COUNTRY_USA, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: direct match of local EU operclass (4) (with valid channel 1) (eqv glob 81)
            4, SWL_OP_CLASS_COUNTRY_EU, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        {
            4, SWL_OP_CLASS_COUNTRY_USA, 100, false,
            SWL_CHANSPEC_NEW(100, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: direct match of local JP operclass (30) (with valid channel 1) (eqv glob 81)
            30, SWL_OP_CLASS_COUNTRY_JP, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: direct match of local JP operclass (56) (with valid channel 1) (eqv glob 83)
            56, SWL_OP_CLASS_COUNTRY_JP, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: direct match of global operclass (81) (with valid channel 1)
            81, SWL_OP_CLASS_COUNTRY_UNKNOWN, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: direct match of global operclass (83) (with valid channel 3)
            83, SWL_OP_CLASS_COUNTRY_UNKNOWN, 3, false,
            SWL_CHANSPEC_NEW(3, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: direct match of global operclass (128) (with valid channel 36)
            128, SWL_OP_CLASS_COUNTRY_UNKNOWN, 36, false,
            SWL_CHANSPEC_NEW(36, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        {// success: direct match of global operclass (128) (with valid channel 60)
            128, SWL_OP_CLASS_COUNTRY_UNKNOWN, 60, false,
            SWL_CHANSPEC_NEW(60, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: direct match of global operclass (129) (with valid channel 60)
            129, SWL_OP_CLASS_COUNTRY_UNKNOWN, 60, false,
            SWL_CHANSPEC_NEW(60, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: direct match of local USA operclass (129) (with valid channel 100) (eqv glob 129)
            129, SWL_OP_CLASS_COUNTRY_USA, 100, false,
            SWL_CHANSPEC_NEW(100, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // error: unmatch input(opClass:119,region:UNKNOWN,chan:56) calc(region:UNKNOWN)
            119, SWL_OP_CLASS_COUNTRY_UNKNOWN, 56, false,
            SWL_CHANSPEC_EMPTY,
        },
        { // success: direct match of global operclass (120) (with valid channel 56)
            120, SWL_OP_CLASS_COUNTRY_UNKNOWN, 56, false,
            SWL_CHANSPEC_NEW(56, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: direct match of local EU operclass (9) (with valid channel 64) (eqv glob 120)
            9, SWL_OP_CLASS_COUNTRY_EU, 64, false,
            SWL_CHANSPEC_NEW(64, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: unique match of local EU operclass (9) (with valid channel 64) (eqv glob 120)
            9, SWL_OP_CLASS_COUNTRY_UNKNOWN, 64, false,
            SWL_CHANSPEC_NEW(64, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // error case: opclass(12)/region(UNKNOWN)/channel(11) matches 2 chanspec (max allowed:1) (2_11/20:USA,2_11/40:EU)
            12, SWL_OP_CLASS_COUNTRY_UNKNOWN, 11, false,
            SWL_CHANSPEC_EMPTY,
        },
        { // success: direct match match of local EU operclass (12) (with valid channel 11) (eqv glob 84)
            12, SWL_OP_CLASS_COUNTRY_EU, 11, false,
            SWL_CHANSPEC_NEW(11, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // error: opclass(4)/region(UNKNOWN)/channel(0) matches 3 chanspec (max allowed:1) (5_0/20:USA,2_0/20:EU,5_0/40:CN)
            4, SWL_OP_CLASS_COUNTRY_UNKNOWN, 0, false,
            SWL_CHANSPEC_EMPTY,
        },
        { // recover: opclass(4)/region(UNKNOWN)/channel(0) matches 3 chanspec match (max allowed:5) (5_0/20:USA,2_0/20:EU,5_0/40:CN), and takes first result
            4, SWL_OP_CLASS_COUNTRY_UNKNOWN, 0, true,
            SWL_CHANSPEC_NEW(0, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // success: unique match of local EU operclass (4) (with valid channel 1) (eqv glob 81)
            4, SWL_OP_CLASS_COUNTRY_UNKNOWN, 1, false,
            SWL_CHANSPEC_NEW(1, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ),
        },
        { // success: unique match match of local USA operclass (4) (with valid channel 100) (eqv glob 121)
            4, SWL_OP_CLASS_COUNTRY_UNKNOWN, 100, false,
            SWL_CHANSPEC_NEW(100, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // error: unmatch input(opClass:38,region:UNKNOWN,chan:0) calc(region:UNKNOWN)
            38, SWL_OP_CLASS_COUNTRY_UNKNOWN, 0, false,
            SWL_CHANSPEC_EMPTY,
        },
        { // success: unique match of local JP operclass (39) (eqv glob 122)
            39, SWL_OP_CLASS_COUNTRY_UNKNOWN, 0, false,
            SWL_CHANSPEC_NEW(0, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
        { // error: unmatch input(opClass:40,region:USA,chan:0) calc(region:UNKNOWN)
            40, SWL_OP_CLASS_COUNTRY_USA, 0, false,
            SWL_CHANSPEC_EMPTY,
        },
        { // success: direct match of global operation class 131
            131, SWL_OP_CLASS_COUNTRY_UNKNOWN, 0, false,
            SWL_CHANSPEC_NEW(0, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ),
        },
        { // success: direct match of global operclass 131 (with valid channel 9)
            131, SWL_OP_CLASS_COUNTRY_UNKNOWN, 9, false,
            SWL_CHANSPEC_NEW(9, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ),
        },
        { // success: unique match of local USA operclass 3 (with valid channel 153) (eqv glob 124)
            3, SWL_OP_CLASS_COUNTRY_UNKNOWN, 153, false,
            SWL_CHANSPEC_NEW(153, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ),
        },
    };

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        swl_chanspec_t retChSpec = swl_chanspec_fromOperClassExt(tests[i].inOpClass, tests[i].inRegion, tests[i].inChan, tests[i].allowMultiMatch);
        printf("[in] opClass(%d) region(%d) chan(%d) multi(%d) => [out] chspec(%s) <=> [exp] chspec(%s)\n",
               tests[i].inOpClass, tests[i].inRegion, tests[i].inChan, tests[i].allowMultiMatch,
               swl_typeChanspecExt_toBuf32Ref(&retChSpec).buf,
               swl_typeChanspecExt_toBuf32Ref(&tests[i].expecChSpec).buf);
        assert_true(swl_typeChanspecExt_equalsRef(&retChSpec, &tests[i].expecChSpec));
    }
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlChan");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_chanspec_freqBandExtToFreqBand),
        cmocka_unit_test(test_swl_chanspec_getFreq),
        cmocka_unit_test(test_swl_chanspec_arrayFromChar),
        cmocka_unit_test(test_swl_chanspec_arrayToChar),
        cmocka_unit_test(test_swl_chanspec_isValidBand),
        cmocka_unit_test(test_swl_chanspec_isValidBandExt),
        cmocka_unit_test(test_swl_chanspec_getNrChannelsInBand),
        cmocka_unit_test(test_swl_chanspec_getNrChannelsPerFreqAndBw),
        cmocka_unit_test(test_swl_chanspec_getCentreChannel),
        cmocka_unit_test(test_swl_chanspec_getBwFromFreqCtrlCentre),
        cmocka_unit_test(test_swl_chanspec_fromFreqAndCentreChannel),
        cmocka_unit_test(test_swl_chanspec_fromFreqCtrlCentre),
        cmocka_unit_test(test_swl_chanspec_getBaseChannel),
        cmocka_unit_test(test_swl_chanspec_getChannelsInChanspec),
        cmocka_unit_test(test_swl_channel_isInChanspec),
        cmocka_unit_test(test_swl_channel_getComplementaryBaseChannel),
        cmocka_unit_test(test_swl_chanspec_toRadBw),
        cmocka_unit_test(test_swl_chanspec_fromDmExt),
        cmocka_unit_test(test_swl_chanspec_fromDm),
        cmocka_unit_test(test_swl_chanspec_channelFromMHz),
        cmocka_unit_test(test_swl_chanspec_channelToMHz),
        cmocka_unit_test(test_swl_chanspec_fromMHz),
        cmocka_unit_test(test_swl_chanspec_toMHz),
        cmocka_unit_test(test_swl_chanspec_validChannels),
        cmocka_unit_test(test_swl_chanspec_freqBandExtFromBaseChannel),
        cmocka_unit_test(test_swl_chanspec_operClassToChanInfo),
        cmocka_unit_test(test_swl_chanspec_getOperClass),
        cmocka_unit_test(test_swl_chanspec_testEqualsOnStackAssignment),
        cmocka_unit_test(test_swl_chanspec_highPowerCountry),
        cmocka_unit_test(test_swl_chanspec_fromOperClass),
    };

    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    ttb_assert_clearPrint();

    runCommonTypeTest(&testRadChanspecData);
    runCommonTypeTest(&testChanspecExtData);

    sahTraceClose();
    return rc;
}
