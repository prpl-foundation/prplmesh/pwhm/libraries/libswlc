/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <time.h>

#include <debug/sahtrace.h>
#include "test-toolbox/ttb_mockClock.h"
#include "swl/swl_datetime.h"
#include "swl/swl_common_time_spec.h"
#include "swl/swl_common.h"


#include "test_swlc_type_testCommon.h"

#define MONOTIME_TV_SEC 100000
#define MONOTIME_TV_NSEC 343343000
#define REALTIME_TV_SEC 1588169613
#define REALTIME_TV_NSEC 787787000

// assume system up for 100k seconds
#define REALTIME_STR "2020-04-29T14:13:33.787787Z"


#define TEST1_NULLTIME_STR "0001-01-01T00:00:00.000000Z"

//test 1 old time still in monotime
#define TEST1_MONOTIME 64653
#define TEST1_MONOTIME_NSEC 678679000
#define TEST1_REALTIME 1588134267
#define TEST1_REALTIME_NSEC 123123000
#define TEST1_REALTIME_STR "2020-04-29T04:24:27.123123Z"

//time in future
#define TEST2_MONOTIME 33804636
#define TEST2_MONOTIME_NSEC 545545000
#define TEST2_REALTIME 1621874249
#define TEST2_REALTIME_NSEC 989989000
#define TEST2_REALTIME_STR "2021-05-24T16:37:29.989989Z"

//very old time, before start of mono
#define TEST3_REALTIME 1564365271
#define TEST3_REALTIME_STR "2019-07-29T01:54:31Z"

typedef struct testVal {
    swl_timeSpecMono_t monoTime;
    swl_timeSpecReal_t realTime;
    const char* str;
    char* localStr;
    swl_datetime_t timeStamp;
    bool monoBefore;
} testVal_t;


testVal_t vals[] = {
    {.monoTime = {.tv_sec = 0, .tv_nsec = 0},
        .realTime = {.tv_sec = 0, .tv_nsec = 0},
        .str = TEST1_NULLTIME_STR},
    {.monoTime = {.tv_sec = MONOTIME_TV_SEC, .tv_nsec = MONOTIME_TV_NSEC},
        .realTime = {.tv_sec = REALTIME_TV_SEC, .tv_nsec = REALTIME_TV_NSEC},
        .str = REALTIME_STR},
    {.monoTime = {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
        .realTime = {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
        .str = TEST1_REALTIME_STR},
    {.monoTime = {.tv_sec = TEST2_MONOTIME, .tv_nsec = TEST2_MONOTIME_NSEC},
        .realTime = {.tv_sec = TEST2_REALTIME, .tv_nsec = TEST2_REALTIME_NSEC},
        .str = TEST2_REALTIME_STR },
    {.monoTime = {.tv_sec = 0, .tv_nsec = 1},
        .realTime = {.tv_sec = 1564365271, .tv_nsec = 101101000},
        .str = "2019-07-29T01:54:31.101101Z",
        .monoBefore = true},
};

struct timespec s_baseMonoTime = {.tv_sec = MONOTIME_TV_SEC, .tv_nsec = MONOTIME_TV_NSEC};
struct timespec s_baseRealtime = {.tv_sec = REALTIME_TV_SEC, .tv_nsec = REALTIME_TV_NSEC};

static void s_resetTime() {
    s_baseMonoTime.tv_sec = MONOTIME_TV_SEC;
    s_baseMonoTime.tv_nsec = MONOTIME_TV_NSEC;
    s_baseRealtime.tv_sec = REALTIME_TV_SEC;
    s_baseRealtime.tv_nsec = REALTIME_TV_NSEC;

    ttb_mockClock_init(&s_baseMonoTime, &s_baseRealtime);
}

static int setup_suite(void** state) {
    (void) state;
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        swl_datetime_strptime(vals[i].str, SWL_DATETIME_ISO8601_FMT_US, &vals[i].timeStamp);
        vals[i].localStr = strdup(vals[i].str);
    }
    s_resetTime();
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;

    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        free(vals[i].localStr);
    }

    return 0;
}

static void test_swl_timespec_getMono(void** state _UNUSED) {
    swl_timeSpecMono_t ts;
    swl_timespec_getMono(&ts);
    assert_int_equal(ts.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(ts.tv_nsec, MONOTIME_TV_NSEC);
}

static void test_swl_timespec_getMonoVal(void** state _UNUSED) {
    swl_timeSpecMono_t ts = swl_timespec_getMonoVal();
    assert_int_equal(ts.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(ts.tv_nsec, MONOTIME_TV_NSEC);
}

static void test_swl_timespec_getReal(void** state _UNUSED) {
    swl_timeSpecReal_t ts;
    swl_timespec_getReal(&ts);
    assert_int_equal(ts.tv_sec, REALTIME_TV_SEC);
    assert_int_equal(ts.tv_nsec, REALTIME_TV_NSEC);
}

static void test_swl_timespec_getRealVal(void** state _UNUSED) {
    swl_timeSpecReal_t ts = swl_timespec_getRealVal();
    assert_int_equal(ts.tv_sec, REALTIME_TV_SEC);
    assert_int_equal(ts.tv_nsec, REALTIME_TV_NSEC);
}

static void test_swl_timespec_reset(void** state _UNUSED) {
    swl_timeSpecMono_t ts;
    swl_timespec_getMono(&ts);
    assert_int_equal(ts.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(ts.tv_nsec, MONOTIME_TV_NSEC);
    swl_timespec_reset(&ts);
    assert_int_equal(ts.tv_sec, 0);
    assert_int_equal(ts.tv_nsec, 0);
}

typedef struct {
    time_t sec;
    long int nsec;
    int64_t toSec;
    int64_t toMs;
    int64_t toUs;
    int64_t toNs;
} test_getTime_t;



test_getTime_t getTimeTestVals[] = {
    {MONOTIME_TV_SEC, MONOTIME_TV_NSEC, MONOTIME_TV_SEC,
        ((int64_t) MONOTIME_TV_SEC * SWL_TIMESPEC_MILLI_PER_SEC + MONOTIME_TV_NSEC / SWL_TIMESPEC_NANO_PER_MILLI),
        ((int64_t) MONOTIME_TV_SEC * SWL_TIMESPEC_MICRO_PER_SEC + MONOTIME_TV_NSEC / SWL_TIMESPEC_NANO_PER_MICRO),
        ((int64_t) MONOTIME_TV_SEC * SWL_TIMESPEC_NANO_PER_SEC + MONOTIME_TV_NSEC)},
    { 0, 0, 0, 0, 0, 0},
    { 0, 100 * SWL_TIMESPEC_NANO_PER_MICRO, 0, 0, 100, 100 * SWL_TIMESPEC_NANO_PER_MICRO},
    { 0, 100 * SWL_TIMESPEC_NANO_PER_MILLI, 0, 100, 100 * SWL_TIMESPEC_MICRO_PER_MILLI, 100 * SWL_TIMESPEC_NANO_PER_MILLI},
    {1, 0, 1, SWL_TIMESPEC_MILLI_PER_SEC, SWL_TIMESPEC_MICRO_PER_SEC, SWL_TIMESPEC_NANO_PER_SEC},
    {1, SWL_TIMESPEC_NANO_PER_SEC / 2, 1, SWL_TIMESPEC_MILLI_PER_SEC* 3 / 2, SWL_TIMESPEC_MICRO_PER_SEC* 3 / 2, (int64_t) SWL_TIMESPEC_NANO_PER_SEC * 3 / 2},
    {-1, 0, -1, -SWL_TIMESPEC_MILLI_PER_SEC, -SWL_TIMESPEC_MICRO_PER_SEC, -SWL_TIMESPEC_NANO_PER_SEC},
    {-1, SWL_TIMESPEC_NANO_PER_SEC / 2, -1, -SWL_TIMESPEC_MILLI_PER_SEC / 2, -SWL_TIMESPEC_MICRO_PER_SEC / 2, -SWL_TIMESPEC_NANO_PER_SEC / 2},
    {INT64_MAX, 0, INT64_MAX, INT64_MAX, INT64_MAX, INT64_MAX},
    {INT64_MIN, 0, INT64_MIN, INT64_MIN, INT64_MIN, INT64_MIN},
    {INT64_MAX / SWL_TIMESPEC_NANO_PER_SEC, 0, (int64_t) INT64_MAX / SWL_TIMESPEC_NANO_PER_SEC, (int64_t) INT64_MAX / SWL_TIMESPEC_NANO_PER_SEC * SWL_TIMESPEC_MILLI_PER_SEC, (int64_t) INT64_MAX / SWL_TIMESPEC_NANO_PER_SEC * SWL_TIMESPEC_MICRO_PER_SEC, (int64_t) INT64_MAX},
    {INT64_MAX / SWL_TIMESPEC_MICRO_PER_SEC, 0, (int64_t) INT64_MAX / SWL_TIMESPEC_MICRO_PER_SEC, (int64_t) INT64_MAX / SWL_TIMESPEC_MICRO_PER_SEC * SWL_TIMESPEC_MILLI_PER_SEC, INT64_MAX, INT64_MAX},
    {INT64_MAX / SWL_TIMESPEC_MILLI_PER_SEC, 0, (int64_t) INT64_MAX / SWL_TIMESPEC_MILLI_PER_SEC, (int64_t) INT64_MAX, INT64_MAX, INT64_MAX},

};
static void test_swl_timespec_getTime(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(getTimeTestVals); i++) {
        swl_timeSpec_t time = {.tv_sec = getTimeTestVals[i].sec, .tv_nsec = getTimeTestVals[i].nsec};

        assert_int_equal(swl_timespec_toSec(&time), getTimeTestVals[i].toSec);
        assert_int_equal(swl_timespec_toMs(&time), getTimeTestVals[i].toMs);
        assert_int_equal(swl_timespec_toUs(&time), getTimeTestVals[i].toUs);
        assert_int_equal(swl_timespec_toNs(&time), getTimeTestVals[i].toNs);
        assert_int_equal(swl_timespec_getTimeInMillisec(&time), getTimeTestVals[i].toMs);
        assert_int_equal(swl_timespec_getTimeInMicrosec(&time), getTimeTestVals[i].toUs);
        assert_int_equal(swl_timespec_getTimeInNanosec(&time), getTimeTestVals[i].toNs);
    }
}


static void s_checkTime(struct timespec* start, struct timespec* stop, int expectedRet, time_t sec, uint64_t nsec) {
    struct timespec diff;
    diff.tv_sec = sec;
    diff.tv_nsec = nsec;

    struct timespec result;
    printf("%li %lu \n", sec, nsec);

    int ret = swl_timespec_diff(&result, start, stop);
    assert_int_equal(ret, expectedRet);
    assert_int_equal(result.tv_sec, diff.tv_sec);
    assert_int_equal(result.tv_nsec, diff.tv_nsec);

    assert_int_equal(swl_timespec_diffToSec(start, stop), diff.tv_sec);
    assert_int_equal(swl_timespec_diffToMillisec(start, stop), swl_timespec_getTimeInMillisec(&diff));
    assert_int_equal(swl_timespec_diffToMicrosec(start, stop), swl_timespec_getTimeInMicrosec(&diff));
    assert_int_equal(swl_timespec_diffToNanosec(start, stop), swl_timespec_getTimeInNanosec(&diff));
}

static void test_swl_timespec_diff(void** state _UNUSED) {
    swl_timeSpecMono_t start;
    swl_timeSpecMono_t stop;

    swl_timespec_getMono(&start);
    assert_int_equal(start.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(start.tv_nsec, MONOTIME_TV_NSEC);
    swl_timespec_getMono(&stop);
    assert_int_equal(stop.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(stop.tv_nsec, MONOTIME_TV_NSEC);
    /* Case 1: start and stop time are eqauls
     *         ret = 0
     *         diff time to sec = 0
     *         diff time to nanosec = 0
     */
    s_checkTime(&start, &stop, 0, 0, 0);


    /* Case 2: start time < stop time
     *         ret = 1
     *         diff time to sec > 0
     *         diff time to nanosec > 0
     */
    stop.tv_sec = MONOTIME_TV_SEC + 100;
    stop.tv_nsec = MONOTIME_TV_NSEC + 100;


    s_checkTime(&start, &stop, 1, 100, 100);
    s_checkTime(&stop, &start, -1, -100, 100);


    /* Case 3: start time > stop time
     *         ret = -1
     *         diff time to sec = absolute value of the diff
     *         diff time to nanosec = absolute value of the diff
     */
    stop.tv_sec = MONOTIME_TV_SEC - 200;
    stop.tv_nsec = MONOTIME_TV_NSEC - 200;

    s_checkTime(&start, &stop, -1, -200, 200);
    s_checkTime(&stop, &start, 1, 200, 200);


    /* Case 4: start and stop time are NULL
     *         ret = 0
     *         diff time to sec = 0
     *         diff time to nanosec = 0
     */
    s_checkTime(NULL, NULL, 0, 0, 0);

    /* Case 5: start time is NULL && stop time is !NULL
     *         ret = 1
     *         diff time to sec = INT64_MAX
     *         diff time to nanosec = INT64_MAX
     */

    s_checkTime(NULL, &stop, 1, INT64_MAX, UINT64_MAX);


    /* Case 6: start time is !NULL && stop time is NULL
     *         ret = -1
     *         diff time to sec = INT64_MIN
     *         diff time to nanosec = INT64_MIN
     */

    s_checkTime(&start, NULL, -1, INT64_MIN, 0);
}

static void test_swl_timespec_equals(void** state _UNUSED) {
    swl_timeSpecMono_t firstTs;
    swl_timeSpecMono_t secondTs;
    swl_timespec_getMono(&firstTs);
    assert_int_equal(firstTs.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(firstTs.tv_nsec, MONOTIME_TV_NSEC);
    swl_timespec_getMono(&secondTs);
    assert_int_equal(secondTs.tv_sec, MONOTIME_TV_SEC);
    assert_int_equal(secondTs.tv_nsec, MONOTIME_TV_NSEC);
    assert_true(swl_timespec_equals(&firstTs, &secondTs));
    secondTs.tv_sec = MONOTIME_TV_SEC + 100;
    secondTs.tv_nsec = MONOTIME_TV_NSEC + 100;
    assert_false(swl_timespec_equals(&firstTs, &secondTs));


    assert_true(swl_timespec_equals(NULL, NULL));
    assert_false(swl_timespec_equals(&firstTs, NULL));
    assert_false(swl_timespec_equals(NULL, &secondTs));
}

static void test_swl_timespec_isZero(void** state _UNUSED) {
    swl_timeSpecMono_t tsZero;
    swl_timespec_getMono(&tsZero);
    assert_int_equal(tsZero.tv_sec, MONOTIME_TV_SEC);
    assert_false(swl_timespec_isZero(&tsZero));
    swl_timespec_reset(&tsZero);
    assert_int_equal(tsZero.tv_sec, 0);
    assert_int_equal(tsZero.tv_nsec, 0);
    assert_true(swl_timespec_isZero(&tsZero));
}

static void test_swl_timespec_addTime(void** state _UNUSED) {
    struct timespec testSpec = {.tv_sec = 0, .tv_nsec = 0};
    swl_timespec_addTime(&testSpec, 111, 2222);
    assert_int_equal(testSpec.tv_sec, 111);
    assert_int_equal(testSpec.tv_nsec, 2222);

    swl_timespec_addTime(&testSpec, 111, SWL_TIMESPEC_NANO_PER_SEC);
    assert_int_equal(testSpec.tv_sec, 223);
    assert_int_equal(testSpec.tv_nsec, 2222);

    swl_timespec_addTime(&testSpec, -222, -111);
    assert_int_equal(testSpec.tv_sec, 1);
    assert_int_equal(testSpec.tv_nsec, 2111);

    swl_timespec_addTime(&testSpec, -1, -3000);
    assert_int_equal(testSpec.tv_sec, -1);
    assert_int_equal(testSpec.tv_nsec, SWL_TIMESPEC_NANO_PER_SEC - 889);

    swl_timespec_addTime(&testSpec, 0, (3 * (uint64_t) SWL_TIMESPEC_NANO_PER_SEC) + 3000);
    assert_int_equal(testSpec.tv_sec, 3);
    assert_int_equal(testSpec.tv_nsec, 2111);

    swl_timespec_addTime(&testSpec, 0, (-3 * (uint64_t) SWL_TIMESPEC_NANO_PER_SEC) - 3000);
    assert_int_equal(testSpec.tv_sec, -1);
    assert_int_equal(testSpec.tv_nsec, SWL_TIMESPEC_NANO_PER_SEC - 889);
}

static void s_moveSetAdd(struct timespec* result, struct timespec* spec1, struct timespec* spec2, int64_t sec, int64_t nsec) {
    memcpy(spec1, result, sizeof(struct timespec));
    spec2->tv_sec = sec;
    spec2->tv_nsec = nsec;
    swl_timespec_add(result, spec1, spec2);
}

static void test_swl_timespec_add(void** state _UNUSED) {
    struct timespec testSpec1 = {.tv_sec = 0, .tv_nsec = 0};
    struct timespec testSpec2 = {.tv_sec = 111, .tv_nsec = 2222};
    struct timespec result = {.tv_sec = 0, .tv_nsec = 0};

    swl_timespec_add(&result, &testSpec1, &testSpec2);
    assert_int_equal(result.tv_sec, 111);
    assert_int_equal(result.tv_nsec, 2222);

    s_moveSetAdd(&result, &testSpec1, &testSpec2, 111, SWL_TIMESPEC_NANO_PER_SEC);
    assert_int_equal(result.tv_sec, 223);
    assert_int_equal(result.tv_nsec, 2222);

    s_moveSetAdd(&result, &testSpec1, &testSpec2, -222, -111);
    assert_int_equal(result.tv_sec, 1);
    assert_int_equal(result.tv_nsec, 2111);

    s_moveSetAdd(&result, &testSpec1, &testSpec2, -1, -3000);
    assert_int_equal(result.tv_sec, -1);
    assert_int_equal(result.tv_nsec, SWL_TIMESPEC_NANO_PER_SEC - 889);

    s_moveSetAdd(&result, &testSpec1, &testSpec2, 0, (3 * (uint64_t) SWL_TIMESPEC_NANO_PER_SEC) + 3000);
    assert_int_equal(result.tv_sec, 3);
    assert_int_equal(result.tv_nsec, 2111);

    s_moveSetAdd(&result, &testSpec1, &testSpec2, 0, (-3 * (uint64_t) SWL_TIMESPEC_NANO_PER_SEC) - 3000);
    assert_int_equal(result.tv_sec, -1);
    assert_int_equal(result.tv_nsec, SWL_TIMESPEC_NANO_PER_SEC - 889);
}

#define ASSERT_TS_EQUAL(ts1, ts2) \
    assert_int_equal(swl_timespec_getTimeInNanoSec(&ts1), swl_timespec_getTimeInNanoSec(&ts2))

#define PRINT_TS(str, ts) printf("%s:%lu\n", str, swl_timespec_getTimeInNanoSec(&ts));

static void test_swl_timespec_monoToReal(void** state _UNUSED) {

    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        if(vals[i].monoBefore) {
            continue;
        }
        swl_timeSpecReal_t tmp;
        swl_timespec_monoToReal(&tmp, &vals[i].monoTime);
        ASSERT_TS_EQUAL(vals[i].realTime, tmp);
    }

}

static void test_swl_timespec_realToMono(void** state _UNUSED) {

    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        swl_timeSpecMono_t tmp;
        swl_timespec_realToMono(&tmp, &vals[i].realTime);
        ASSERT_TS_EQUAL(vals[i].monoTime, tmp);
    }
}

static void test_swl_timespec_monoToDate(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        if(vals[i].monoBefore) {
            continue;
        }
        char buffer[50];
        swl_timespec_monoToDate(buffer, sizeof(buffer), &vals[i].monoTime);
        assert_string_equal(buffer, vals[i].str);
    }
}
static void test_swl_timespec_realToDate(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        char buffer[50];
        swl_timespec_realToDate(buffer, sizeof(buffer), &vals[i].realTime);
        assert_string_equal(buffer, vals[i].str);
    }
}

static void test_swl_timespec_monoToDatetime(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        if(vals[i].monoBefore) {
            continue;
        }
        swl_datetime_t dt;
        swl_timespec_monoToDatetime(&dt, &vals[i].monoTime);
        assert_int_equal(0, swl_datetime_delta(&dt, &vals[i].timeStamp));
    }
}
static void test_swl_timespec_realToDatetime(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        swl_datetime_t dt;
        swl_timespec_realToDatetime(&dt, &vals[i].realTime);
        assert_int_equal(0, swl_datetime_delta(&dt, &vals[i].timeStamp));
    }
}

static void test_swl_timespec_datetimeToMono(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        if(vals[i].monoBefore) {
            continue;
        }
        swl_timeSpecMono_t tmp;
        swl_timespec_datetimeToMono(&tmp, &vals[i].timeStamp);
        ASSERT_TS_EQUAL(tmp, vals[i].monoTime);
    }
}
static void test_swl_timespec_datetimeToReal(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(vals); i++) {
        swl_timeSpecMono_t tmp;
        swl_timespec_datetimeToReal(&tmp, &vals[i].timeStamp);
        ASSERT_TS_EQUAL(tmp, vals[i].realTime);
    }
}

static void test_swl_timespec_getRealOfMonoBaseTime(void** state _UNUSED) {
    struct timespec expectedBase;
    swl_timespec_diff(&expectedBase, &vals[1].monoTime, &vals[1].realTime);

    struct timespec tmp;


    for(int i = 0; i < 10; i++) {
        swl_timespec_monoToReal(&tmp, &vals[1].monoTime);
        ASSERT_TS_EQUAL(tmp, vals[1].realTime);
        swl_timespec_realToMono(&tmp, &vals[1].realTime);
        ASSERT_TS_EQUAL(tmp, vals[1].monoTime);


        swl_timespec_getRealOfMonoBaseTime(&tmp);
        ASSERT_TS_EQUAL(tmp, expectedBase);


        swl_timespec_addTime(&s_baseRealtime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);
        swl_timespec_addTime(&s_baseMonoTime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);
    }
    s_resetTime();


}


static void test_swl_timespec_getRealOfMonoBaseTimeConstant(void** state _UNUSED) {
    ttb_mockClock_setIntercept(false);

    swl_timeSpecReal_t base = swl_timespec_getRealOfMonoBaseTimeVal();

    for(int i = 0; i < 20; i++) {
        swl_timeSpecReal_t testVal = swl_timespec_getRealOfMonoBaseTimeVal();
        ASSERT_TS_EQUAL(base, testVal);

        swl_timeSpecReal_t testPtr = {.tv_sec = 0, .tv_nsec = 0};
        swl_timespec_getRealOfMonoBaseTime(&testPtr);
        ASSERT_TS_EQUAL(base, testPtr);
    }

    ttb_mockClock_setIntercept(true);
}


#define NR_MONO_TEST_VALUES 3
swl_timeSpecMono_t monoBase[NR_MONO_TEST_VALUES] = {
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
    {.tv_sec = TEST2_MONOTIME, .tv_nsec = TEST2_MONOTIME_NSEC},
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC},
};
size_t monoCount[NR_MONO_TEST_VALUES] = {2, 1, 2};
swl_timeSpecMono_t monoNotContains[NR_MONO_TEST_VALUES] = {
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC + 1},
    {.tv_sec = TEST2_MONOTIME + 1, .tv_nsec = TEST2_MONOTIME_NSEC},
    {.tv_sec = TEST1_MONOTIME, .tv_nsec = TEST1_MONOTIME_NSEC - 1},
};
#define monoStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define monoStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* monotestStrValuesChar[NR_MONO_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

#define NR_REAL_TEST_VALUES 3
swl_timeSpecReal_t realBase[NR_REAL_TEST_VALUES] = {
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
    {.tv_sec = TEST2_REALTIME, .tv_nsec = TEST2_REALTIME_NSEC},
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC},
};
size_t realCount[NR_REAL_TEST_VALUES] = {2, 1, 2};
swl_timeSpecReal_t realNotContains[NR_REAL_TEST_VALUES] = {
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC + 1},
    {.tv_sec = TEST2_REALTIME + 1, .tv_nsec = TEST2_REALTIME_NSEC},
    {.tv_sec = TEST1_REALTIME, .tv_nsec = TEST1_REALTIME_NSEC - 1},
};
#define realStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define realStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* realtestStrValuesChar[NR_REAL_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

swlc_type_testCommonData_t timeMonoData = {
    .name = "monoTime",
    .testType = swl_type_timeSpecMono,
    .refType = &gtSwl_type_timeSpecMonoPtr.type,
    .serialStr = monoStrChar,
    .serialStr2 = monoStr2Char,
    .strValues = monotestStrValuesChar,
    .baseValues = &monoBase,
    .valueCount = monoCount,
    .notContains = &monoNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
};

swlc_type_testCommonData_t timeRealData = {
    .name = "realTime",
    .testType = swl_type_timeSpecReal,
    .refType = &gtSwl_type_timeSpecRealPtr.type,
    .serialStr = realStrChar,
    .serialStr2 = realStr2Char,
    .strValues = realtestStrValuesChar,
    .baseValues = &realBase,
    .valueCount = realCount,
    .notContains = &realNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlTimeSpec ");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_timespec_getMono),
        cmocka_unit_test(test_swl_timespec_getMonoVal),
        cmocka_unit_test(test_swl_timespec_getReal),
        cmocka_unit_test(test_swl_timespec_getRealVal),
        cmocka_unit_test(test_swl_timespec_reset),
        cmocka_unit_test(test_swl_timespec_getTime),
        cmocka_unit_test(test_swl_timespec_diff),
        cmocka_unit_test(test_swl_timespec_equals),
        cmocka_unit_test(test_swl_timespec_isZero),
        cmocka_unit_test(test_swl_timespec_addTime),
        cmocka_unit_test(test_swl_timespec_add),
        cmocka_unit_test(test_swl_timespec_monoToReal),
        cmocka_unit_test(test_swl_timespec_realToMono),
        cmocka_unit_test(test_swl_timespec_monoToDate),
        cmocka_unit_test(test_swl_timespec_realToDate),
        cmocka_unit_test(test_swl_timespec_getRealOfMonoBaseTime),
        cmocka_unit_test(test_swl_timespec_getRealOfMonoBaseTimeConstant),
        cmocka_unit_test(test_swl_timespec_monoToDatetime),
        cmocka_unit_test(test_swl_timespec_realToDatetime),
        cmocka_unit_test(test_swl_timespec_datetimeToMono),
        cmocka_unit_test(test_swl_timespec_datetimeToReal),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    s_resetTime();

    runCommonTypeTest(&timeMonoData);
    runCommonTypeTest(&timeRealData);

    sahTraceClose();
    return rc;
}
