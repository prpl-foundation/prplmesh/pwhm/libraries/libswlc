/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>
#include <unistd.h>

#include <debug/sahtrace.h>
#include "swl/ttb/swl_ttb.h"
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_namedTupleType.h"
#include "swl/fileOps/swl_fileUtils.h"

#define NR_TYPES 6
#define NR_VALUES 4


SWL_TYPE_CHARBUF_IMPL(17);
typedef char gtSwl_type_charBuf17_type[17];

#define MY_TEST_TABLE_SUBSUBVAR(X, Y) \
    X(Y, gtSwl_type_charPtr, val, "Val")

SWL_NTT(tMyTestSubSubTupleType, swl_myTestSubSubTT_t, MY_TEST_TABLE_SUBSUBVAR, )

#define MY_TEST_TABLE_SUBVAR(X, Y) \
    X(Y, gtSwl_type_int8, test1, "Test1") \
    X(Y, tMyTestSubSubTupleTypePtr, sub, "Sub") \
    X(Y, gtSwl_type_int64, key, "Key") \
    X(Y, gtSwl_type_charPtr, val, "Val")

SWL_NTT(tMyTestSubTupleType, swl_myTestSubTT_t, MY_TEST_TABLE_SUBVAR, )



#define MY_TEST_TABLE_VAR(X, Y) \
    /**
     * Some documentation
     */ \
    X(Y, gtSwl_type_charBuf17, charBuf, "CharBuf") \
    X(Y, gtSwl_type_int8, test1, "Test1") \
    X(Y, gtSwl_type_int8Ptr, test2, "Test2") \
    /**
     \ * Here some index documenation
     */\
    X(Y, gtSwl_type_int16, index, "Index") \
    /* And some extra here */ \
    X(Y, gtSwl_type_int64, key, "Key") \
    X(Y, tMyTestSubTupleType, sub, "Sub") \
    X(Y, gtSwl_type_charPtr, val, "Val")



SWL_NTT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )

#define MY_TEST_TABLE_SUBVAR_ANOT(X, Y) \
    X(Y, gtSwl_type_int64, key, "Key") \
    X(Y, gtSwl_type_charPtr, val, "Val")
SWL_NTT_ANNOTATE(tMyTestAnnotSubTT, swl_myTestSubTT_t, MY_TEST_TABLE_SUBVAR_ANOT)
#define NR_TEST_TABLE_SUBVAR_ANNOT 2
uint32_t annotSubFields[NR_TEST_TABLE_SUBVAR_ANNOT] = {tMyTestSubTupleType_key, tMyTestSubTupleType_val};

#define MY_TEST_TABLE_VAR_ANNOT(X, Y) \
    X(Y, gtSwl_type_int16, index, "Index") \
    X(Y, gtSwl_type_int64, key, "Key") \
    X(Y, tMyTestAnnotSubTT, sub, "Sub") \
    X(Y, gtSwl_type_charPtr, val, "Val")
#define NR_TEST_TABLE_VAR_ANNOT 4
uint32_t annotFields[NR_TEST_TABLE_VAR_ANNOT] = {tMyTestTupleType_index, tMyTestTupleType_key, tMyTestTupleType_sub, tMyTestTupleType_val};
SWL_NTT_ANNOTATE(tMyTestAnnotTT, swl_myTestTT_t, MY_TEST_TABLE_VAR_ANNOT)


static int8_t testTablePtrData[NR_VALUES] = {2, 4, 6, -1};
static swl_myTestSubSubTT_t testTableSubData[NR_VALUES] = {{.val = "bla"}, {.val = "do"}, {.val = "\\\\cba"}, {.val = "\\,{}}}[]]]"}};

swl_myTestTT_t myTestTableValues[NR_VALUES] = {
    {"Bla", 1, &testTablePtrData[0], 1, 100, {1, &testTableSubData[0], 2, "test1"}, "test1"},
    {"Foo", 3, &testTablePtrData[1], 2, -200, {31, &testTableSubData[1], 14, "no do"}, "test2"},
    {"No", 5, &testTablePtrData[2], -8852, 289870, {111, &testTableSubData[2], 2222, "\\\\abc"}, "foobar"},
    {"Yes", 0, &testTablePtrData[3], 8851, -289869, {-1, &testTableSubData[3], -2, "][}{,"}, "barfoo"},
};


#define NR_OTHER_DATA 2
static int8_t testOtherTablePtrData[NR_OTHER_DATA] = {100, -100};
static swl_myTestSubSubTT_t testOtherSubData[NR_OTHER_DATA] = {{"def"}, {"fed"}};

swl_myTestTT_t otherData[NR_OTHER_DATA] = {
    {"QQ", 50, &testOtherTablePtrData[0], 5, 500, {11, &testOtherSubData[0], 12, "testa1"}, "notHere"},
    {"BB", -50, &testOtherTablePtrData[1], -5, -500, {51, &testOtherSubData[1], 552, "test231"}, "alsoNotHere"},
};

static void test_swl_ntt_toFromChar(void** state _UNUSED) {
    char buffer[200];
    for(size_t i = 0; i < NR_VALUES; i++) {
        ssize_t size = swl_type_toChar((swl_type_t*) &tMyTestTupleType, buffer, sizeof(buffer), &myTestTableValues[i]);
        assert_true(size > 0);
        printf("%zu - %s\n", i, buffer);

        swl_myTestTT_t testVal;
        memset(&testVal, 0, sizeof(swl_myTestTT_t));
        swl_type_fromChar((swl_type_t*) &tMyTestTupleType, &testVal, buffer);

        swl_ttb_assertBigTypeEquals((swl_type_t*) &tMyTestTupleType, &testVal, &myTestTableValues[i]);
        swl_type_cleanup((swl_type_t*) &tMyTestTupleType, &testVal);
    }
}

static void test_swl_ntt_toFromCharAnnot(void** state _UNUSED) {
    char buffer[200];
    for(size_t i = 0; i < NR_VALUES; i++) {
        ssize_t size = swl_type_toChar(&tMyTestAnnotTT.type.type, buffer, sizeof(buffer), &myTestTableValues[i]);
        assert_true(size > 0);

        swl_myTestTT_t testVal;
        memset(&testVal, 0, sizeof(swl_myTestTT_t));
        swl_type_fromChar(&tMyTestAnnotTT.type.type, &testVal, buffer);

        swl_ttb_assertTypeEquals(&tMyTestAnnotTT.type.type, &testVal, &myTestTableValues[i]);
        swl_ttb_assertTypeNotEquals(&tMyTestTupleType.type.type, &testVal, &myTestTableValues[i]);

        size_t index = 0;
        for(uint32_t j = 0; i < tMyTestTupleType__max; i++) {
            if(swl_typeUInt32_arrayContains(annotFields, NR_TEST_TABLE_VAR_ANNOT, j)) {
                swl_ttb_assertTypeEquals(tMyTestAnnotTTTypes[index],
                                         swl_ntt_getValByIndex(&tMyTestAnnotTT, &testVal, index),
                                         swl_ntt_getValByIndex(&tMyTestTupleType, &myTestTableValues[i], j));
                index++;
            } else {
                swl_ttb_assertTypeEmpty(tMyTestTupleTypeTypes[j], swl_ntt_getValByIndex(&tMyTestTupleType, &testVal, j));
            }
        }

        swl_type_cleanup(&tMyTestTupleType.type.type, &testVal);
    }
}

static void test_swl_ntt_copy(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        swl_myTestTT_t* tmpVal = (swl_myTestTT_t*) swl_type_copy((swl_type_t*) &tMyTestTupleType, &myTestTableValues[i]);

        assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

        swl_type_cleanupPtr((swl_type_t*) &tMyTestTupleType, (void**) &tmpVal);
        assert_null(tmpVal);
    }
}


static void test_swl_ntt_copyAnnot(void** state _UNUSED) {
    for(size_t i = 0; i < NR_VALUES; i++) {
        swl_myTestTT_t* tmpVal = (swl_myTestTT_t*) swl_type_copy(&tMyTestAnnotTT.type.type, &myTestTableValues[i]);

        swl_ttb_assertTypeEquals(&tMyTestAnnotTT.type.type, tmpVal, &myTestTableValues[i]);
        swl_ttb_assertTypeNotEquals(&tMyTestTupleType.type.type, tmpVal, &myTestTableValues[i]);

        size_t index = 0;
        for(uint32_t j = 0; i < tMyTestTupleType__max; i++) {
            if(swl_typeUInt32_arrayContains(annotFields, NR_TEST_TABLE_VAR_ANNOT, j)) {
                swl_ttb_assertTypeEquals(tMyTestAnnotTTTypes[index],
                                         swl_ntt_getValByIndex(&tMyTestAnnotTT, tmpVal, index),
                                         swl_ntt_getValByIndex(&tMyTestTupleType, &myTestTableValues[i], j));
                index++;
            } else {
                swl_ttb_assertTypeEmpty(tMyTestTupleTypeTypes[j], swl_ntt_getValByIndex(&tMyTestTupleType, tmpVal, j));
            }
        }
        swl_type_cleanupPtr((swl_type_t*) &tMyTestTupleType, (void**) &tmpVal);
        assert_null(tmpVal);
    }
}

static void test_swl_ntt_equals(void** state _UNUSED) {
    swl_myTestTT_t* tmpVal;
    for(size_t i = 0; i < NR_VALUES; i++) {
        tmpVal = (swl_myTestTT_t*) swl_type_copy((swl_type_t*) &tMyTestTupleType, &myTestTableValues[i]);

        for(size_t j = 0; j < tMyTestTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestTupleType, tmpVal, j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestTupleType, &otherData[0], j);
            swl_type_copyTo(tMyTestTupleType.type.types[j], tgtRef, srcData);
            assert_false(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestTupleType, &myTestTableValues[i], j);
            swl_type_copyTo(tMyTestTupleType.type.types[j], tgtRef, srcData);
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
        }

        for(size_t j = 0; j < tMyTestSubTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));


            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestSubTupleType, &(tmpVal->sub), j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestSubTupleType, &(otherData[0].sub), j);

            swl_type_copyTo(tMyTestSubTupleType.type.types[j], tgtRef, srcData);

            assert_false(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestSubTupleType, &myTestTableValues[i].sub, j);

            swl_type_copyTo(tMyTestSubTupleType.type.types[j], tgtRef, srcData);


            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
        }

        for(size_t j = 0; j < tMyTestSubSubTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));


            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestSubSubTupleType, tmpVal->sub.sub, j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestSubSubTupleType, otherData[0].sub.sub, j);

            swl_type_copyTo(tMyTestSubSubTupleType.type.types[j], tgtRef, srcData);

            assert_false(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestSubSubTupleType, myTestTableValues[i].sub.sub, j);

            swl_type_copyTo(tMyTestSubSubTupleType.type.types[j], tgtRef, srcData);


            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
        }

        swl_type_cleanupPtr((swl_type_t*) &tMyTestTupleType, (void**) &tmpVal);
    }
}

static void test_swl_ntt_equalsAnnot(void** state _UNUSED) {
    swl_myTestTT_t* tmpVal;
    for(size_t i = 0; i < NR_VALUES; i++) {
        tmpVal = (swl_myTestTT_t*) swl_type_copy(&tMyTestTupleType.type.type, &myTestTableValues[i]);

        for(size_t j = 0; j < tMyTestTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestTupleType, tmpVal, j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestTupleType, &otherData[0], j);
            swl_type_copyTo(tMyTestTupleType.type.types[j], tgtRef, srcData);
            assert_false(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestTupleType, &myTestTableValues[i], j);
            swl_type_copyTo(tMyTestTupleType.type.types[j], tgtRef, srcData);
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
        }

        for(size_t j = 0; j < tMyTestSubTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));


            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestSubTupleType, &(tmpVal->sub), j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestSubTupleType, &(otherData[0].sub), j);

            swl_type_copyTo(tMyTestSubTupleType.type.types[j], tgtRef, srcData);

            assert_false(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestSubTupleType, &myTestTableValues[i].sub, j);

            swl_type_copyTo(tMyTestSubTupleType.type.types[j], tgtRef, srcData);


            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));
        }

        for(size_t j = 0; j < tMyTestSubSubTupleType.type.nrTypes; j++) {
            assert_true(swl_type_equals((swl_type_t*) &tMyTestTupleType, tmpVal, &myTestTableValues[i]));


            swl_typeEl_t* tgtRef = swl_ntt_getRefByIndex(&tMyTestSubSubTupleType, tmpVal->sub.sub, j);
            swl_typeData_t* srcData = swl_ntt_getValByIndex(&tMyTestSubSubTupleType, otherData[0].sub.sub, j);

            swl_type_copyTo(tMyTestSubSubTupleType.type.types[j], tgtRef, srcData);

            assert_false(swl_type_equals(&tMyTestTupleType.type.type, tmpVal, &myTestTableValues[i]));

            srcData = swl_ntt_getValByIndex(&tMyTestSubSubTupleType, myTestTableValues[i].sub.sub, j);

            swl_type_copyTo(tMyTestSubSubTupleType.type.types[j], tgtRef, srcData);


            assert_true(swl_type_equals(&tMyTestTupleType.type.type, tmpVal, &myTestTableValues[i]));
        }

        swl_type_cleanupPtr(&tMyTestTupleType.type.type, (void**) &tmpVal);
    }
}


static void test_swl_ntt_toFile(void** state _UNUSED) {
    char* bufName = "test_tmpBase.txt";
    for(size_t i = 0; i < NR_VALUES; i++) {
        char tmpName[100];
        snprintf(tmpName, sizeof(tmpName), "test_%zi.txt", i);

        FILE* fp = fopen(bufName, "w");
        swl_type_toFile((swl_type_t*) &tMyTestTupleType, fp, &myTestTableValues[i], &g_swl_print_json);
        fclose(fp);
        assert_true(swl_fileUtils_contentMatches(bufName, tmpName));
        unlink(bufName);
    }
}


static void test_swl_ntt_toFileAnnot(void** state _UNUSED) {
    char* bufName = "test_tmpAnnot.txt";
    for(size_t i = 0; i < NR_VALUES; i++) {
        char tmpName[100];
        snprintf(tmpName, sizeof(tmpName), "testAnnot_%zi.txt", i);

        FILE* fp = fopen(bufName, "w");
        swl_type_toFile(&tMyTestAnnotTT.type.type, fp, &myTestTableValues[i], &g_swl_print_json);
        fclose(fp);
        assert_true(swl_fileUtils_contentMatches(bufName, tmpName));
        unlink(bufName);
    }
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_ntt_toFromChar),
        cmocka_unit_test(test_swl_ntt_toFromCharAnnot),
        cmocka_unit_test(test_swl_ntt_copy),
        cmocka_unit_test(test_swl_ntt_copyAnnot),
        cmocka_unit_test(test_swl_ntt_equals),
        cmocka_unit_test(test_swl_ntt_equalsAnnot),
        cmocka_unit_test(test_swl_ntt_toFile),
        cmocka_unit_test(test_swl_ntt_toFileAnnot),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
