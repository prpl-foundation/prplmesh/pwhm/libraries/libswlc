/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_namedTupleType.h"

#define NR_TYPES 3
#define NR_VALUES 4


#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String")

SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, )


swl_myTestNTT_t testVals[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-8852, 289870, "foobar"},
    {8851, -289869, "barfoo"},
};

swl_myTestNTT_t testValsInverse[NR_VALUES] = {
    {8851, -289869, "barfoo"},
    {-8852, 289870, "foobar"},
    {2, -200, "test2"},
    {1, 100, "test1"},
};


char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

/**
 * This should be first test, checking some init
 * Tests:
 * * swl_ntt_check
 * * swl_ntt_size
 */
static void test_swl_namedTupleType_checkContentFirst(void** state _UNUSED) {
    for(int i = 0; i < NR_TYPES; i++) {
        assert_string_equal(names[i], myTestNTTType.names[i]);
    }
    assert_int_equal(myTestNTTType_index, 0);
    assert_int_equal(myTestNTTType_key, 1);
    assert_int_equal(myTestNTTType_val, 2);
    assert_int_equal(myTestNTTType__max, NR_TYPES);

    assert_int_equal(m_myTestNTTType_index, 1);
    assert_int_equal(m_myTestNTTType_key, 2);
    assert_int_equal(m_myTestNTTType_val, 4);
    assert_int_equal(m_myTestNTTType__all, 7);

    assert_int_equal(swl_ntt_size(&myTestNTTType), sizeof(swl_myTestNTT_t));

    assert_true(swl_ntt_check(&myTestNTTType));
    assert_false(swl_ntt_check(NULL));

    assert_int_equal(myTestNTTType.type.type.size, sizeof(swl_myTestNTT_t));
    assert_int_equal(swl_ntt_size(&myTestNTTType), sizeof(swl_myTestNTT_t));

}

static void test_swl_ntt_getValByName(void** state _UNUSED) {
    for(int i = 0; i < NR_VALUES; i++) {
        assert_int_equal(testVals[i].index, *(int32_t*) swl_ntt_getValByName(&myTestNTTType, &testVals[i], "SmallInt"));
        assert_int_equal(testVals[i].key, *(int64_t*) swl_ntt_getValByName(&myTestNTTType, &testVals[i], "BigInt"));
        assert_string_equal(testVals[i].val, (char*) swl_ntt_getValByName(&myTestNTTType, &testVals[i], "String"));
    }
}

static void test_swl_ntt_getRefByName(void** state _UNUSED) {
    for(int i = 0; i < NR_VALUES; i++) {
        assert_int_equal(testVals[i].index, *(int32_t*) swl_ntt_getRefByName(&myTestNTTType, &testVals[i], "SmallInt"));
        assert_int_equal(testVals[i].key, *(int64_t*) swl_ntt_getRefByName(&myTestNTTType, &testVals[i], "BigInt"));
        assert_string_equal(testVals[i].val, *(char**) swl_ntt_getRefByName(&myTestNTTType, &testVals[i], "String"));
    }
}

static void test_swl_ntt_getValByIndex(void** state _UNUSED) {
    for(int i = 0; i < NR_VALUES; i++) {
        assert_int_equal(testVals[i].index, *(int32_t*) swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], myTestNTTType_index));
        assert_int_equal(testVals[i].key, *(int64_t*) swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], myTestNTTType_key));
        assert_string_equal(testVals[i].val, (char*) swl_ntt_getValByIndex(&myTestNTTType, &testVals[i], myTestNTTType_val));
    }
}

static void test_swl_ntt_getRefByIndex(void** state _UNUSED) {
    for(int i = 0; i < NR_VALUES; i++) {
        assert_int_equal(testVals[i].index, *(int32_t*) swl_ntt_getRefByIndex(&myTestNTTType, &testVals[i], myTestNTTType_index));
        assert_int_equal(testVals[i].key, *(int64_t*) swl_ntt_getRefByIndex(&myTestNTTType, &testVals[i], myTestNTTType_key));
        assert_string_equal(testVals[i].val, *(char**) swl_ntt_getRefByIndex(&myTestNTTType, &testVals[i], myTestNTTType_val));
    }
}


static void test_swl_ntt_equals(void** state _UNUSED) {
    for(int i = 0; i < NR_VALUES; i++) {
        for(int j = 0; j < NR_VALUES; j++) {
            bool isEqual = (i == j);
            assert_int_equal(isEqual, swl_ntt_equals(&myTestNTTType, &testVals[i], &testVals[j]));

            bool isInvEqual = ((i + j) == NR_VALUES - 1);
            assert_int_equal(isInvEqual, swl_ntt_equals(&myTestNTTType, &testVals[i], &testValsInverse[j]));
        }
    }
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_namedTupleType_checkContentFirst),
        cmocka_unit_test(test_swl_ntt_getValByName),
        cmocka_unit_test(test_swl_ntt_getRefByName),
        cmocka_unit_test(test_swl_ntt_getValByIndex),
        cmocka_unit_test(test_swl_ntt_getRefByIndex),
        cmocka_unit_test(test_swl_ntt_equals),

    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}

