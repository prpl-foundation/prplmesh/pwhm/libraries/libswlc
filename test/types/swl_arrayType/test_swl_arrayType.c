/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/swl_common_tupleType.h"

#define NR_TYPES 6
#define NR_VALUES 4


#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)

SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )

SWL_ARRAY_TYPE(tMyTestArrayType, tMyTestTupleType, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayTypeNull, tMyTestTupleType, 4, true, false);

swl_arrayType_t* testArrType;
const char* filePrefix;
/**
 * Defining a tuple type using a table
 */
swl_myTestTT_t myTestValues[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-88, 289870, "foobar"},
    {88, -289869, "barfoo"},
};


tMyTestArrayType_type testData[] = {
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        } },
    {.data = {
            {2, 100, "test1"}, // 2 changed here
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }},
    {.data = {
            {2, 100, "test1"},
            {2, -202, "test2"}, // -202 changed here
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }},
    {.data = {
            {2, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobars"}, // fobars changed here
            {88, -289869, "barfoo"},
        }},
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {0, 0, NULL},
        }},
    {.data = {
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
        }},
};


static void test_swl_arrayToFromChar(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        char buffer[256] = {0};
        ssize_t tmpSize = swl_type_toChar((swl_type_t*) testArrType, buffer, sizeof(buffer), &testData[i]);
        tMyTestArrayType_type dataBuff;
        memset(&dataBuff, 0, sizeof(dataBuff));
        printf("%s\n", buffer);
        assert_true(tmpSize > 0);
        assert_true(swl_type_fromChar((swl_type_t*) testArrType, &dataBuff, buffer));

        char buffer2[256] = {0};
        swl_type_toChar((swl_type_t*) testArrType, buffer2, sizeof(buffer2), &dataBuff);
        printf("%s\n", buffer2);

        assert_true(swl_type_equals((swl_type_t*) testArrType, &dataBuff, &testData[i]));

        swl_type_cleanup((swl_type_t*) testArrType, &dataBuff);
    }
}


static void test_swl_arrayCopyEquals(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        swl_typeData_t* data = swl_type_copy((swl_type_t*) testArrType, &testData[i]);
        assert_ptr_not_equal(data, &testData[i]);

        for(size_t j = 0; j < SWL_ARRAY_SIZE(testData); j++) {
            if(i == j) {
                assert_true(swl_type_equals((swl_type_t*) testArrType, &testData[j], data));
            } else {
                assert_false(swl_type_equals((swl_type_t*) testArrType, &testData[j], data));
            }
        }

        swl_type_cleanupPtr((swl_type_t*) testArrType, &data);
    }
}
static void test_swl_arrayToFile(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        char fileNameBuf[128] = {0};
        snprintf(fileNameBuf, sizeof(fileNameBuf), "../common/%s_data_%zu.txt", filePrefix, i);

        char* tmpFile = "tmp.txt";
        FILE* fp = fopen(tmpFile, "w");
        swl_type_toFile((swl_type_t*) testArrType, fp, &testData[i], &g_swl_print_json);
        fclose(fp);
        assert_true(swl_fileUtils_contentMatches(fileNameBuf, tmpFile));
        unlink(tmpFile);
    }
}

static int setup_suite(void** state _UNUSED) {
    printf("TYPE INIT %p\n", &tMyTestTupleType);

    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_arrayToFromChar),
        cmocka_unit_test(test_swl_arrayCopyEquals),
        cmocka_unit_test(test_swl_arrayToFile),
    };

    testArrType = &tMyTestArrayType;
    filePrefix = "norm";
    int rc1 = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    testArrType = &tMyTestArrayTypeNull;
    filePrefix = "null";
    int rc2 = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    sahTraceClose();
    return rc1 + rc2;
}
