/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <test-toolbox/ttb.h>
#include "swl/ttb/swl_ttb.h"
#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_arrayType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_typeMath.h"
#include "swl/types/swl_mapType.h"
#include "swl/swl_maps.h"

#define NR_TEST_TYPES 4

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)

SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )

SWL_ARRAY_TYPE(tMyTestArrayType, gtSwl_type_int64, 4, true, false);



typedef enum {
    TEST_ITEM_ADD,
    TEST_ITEM_SUB,
    TEST_ITEM_MUL,
    TEST_ITEM_DIV,
    TEST_ITEM_MOD,
    TEST_ITEM_MAX
} testItem_e;

tMyTestArrayType_type testArrayVals[2] = {
    {.data = {100, 200, -100, 1000}},
    {.data = {50, -75, -1000, 1234}},
};

tMyTestArrayType_type testArrayResults[TEST_ITEM_MAX] = {
    {.data = {150, 125, -1100, 2234}},
    {.data = {50, 275, 900, -234}},
    {.data = {5000, -15000, 100000, 1234000}},
    {.data = {2, -2, 0, 0}},
    {.data = {0, 50, -100, 1000}},
};

SWL_MAP_TYPE(myMapType, &gtSwl_type_charPtr, &gtSwl_type_int32);

swl_mapCharInt32_t testMapVal[2];
swl_mapCharInt32_t testMapResult[TEST_ITEM_MAX];
swl_mapCharInt32_t zeroMap;

/**
 * Defining a tuple type using a table
 */
swl_myTestTT_t myTestValues[2] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
};

swl_myTestTT_t myTestResults[TEST_ITEM_MAX] = {
    {3, -100, NULL},
    {-1, 300, NULL},
    {2, -20000, NULL},
    {0, 0, NULL},
    {1, 100, NULL},
};

uint32_t uint32TestData[2] = { 5, 11};
uint32_t uint32ResultData[TEST_ITEM_MAX] = {16, -6, 55, 0, 5};


typedef struct {
    swl_type_t* type;
    swl_typeData_t* testData;
    swl_typeData_t* resultData;
    swl_typeData_t* zeroVal;
} typeMathTest_t;

typeMathTest_t* curTestData;

typeMathTest_t testData[NR_TEST_TYPES] = {
    {.type = &gtSwl_type_uint32,
        .testData = uint32TestData,
        .resultData = uint32ResultData},
    {.type = &tMyTestTupleType.type,
        .testData = myTestValues,
        .resultData = myTestResults},
    {.type = &tMyTestArrayType.type.type,
        .testData = testArrayVals,
        .resultData = testArrayResults},
    {.type = &myMapType.type,
        .testData = testMapVal,
        .resultData = testMapResult,
        .zeroVal = &zeroMap, },
};

static void test_swl_typeMath_add_number(void** state _UNUSED) {
    uint32_t val1 = 5;
    uint32_t val2 = 11;
    uint32_t val3 = 0;
    swl_typeMath_add(&gtSwl_type_uint32, &val3, &val1, &val2);

    assert_int_equal(val3, 16);
}

static void test_swl_typeMath_add_tuple(void** state _UNUSED) {
    swl_myTestTT_t result;
    memset(&result, 0, sizeof(swl_myTestTT_t));
    swl_typeMath_add(&tMyTestTupleType.type, &result, &myTestValues[0], &myTestValues[1]); \
    swl_ttb_assertTypeEquals(&tMyTestTupleType.type, &result, &myTestResults[TEST_ITEM_ADD]);
}

static void s_doValueTest(testItem_e item) {
    swl_bit8_t buf[curTestData->type->size];
    memset(buf, 0, curTestData->type->size);
    swl_typeEl_t* target = &buf[0];
    bool copy = false;
    if(curTestData->zeroVal != NULL) {
        copy = true;
        target = swl_type_copy(curTestData->type, curTestData->zeroVal);
    }

    if(item == TEST_ITEM_ADD) {
        swl_typeMath_add(curTestData->type, target,
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 0),
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 1));
    } else if(item == TEST_ITEM_SUB) {
        swl_typeMath_sub(curTestData->type, target,
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 0),
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 1));
    } else if(item == TEST_ITEM_MUL) {
        swl_typeMath_mul(curTestData->type, target,
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 0),
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 1));
    } else if(item == TEST_ITEM_DIV) {
        swl_typeMath_div(curTestData->type, target,
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 0),
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 1));
    } else if(item == TEST_ITEM_MOD) {
        swl_typeMath_mod(curTestData->type, target,
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 0),
                         swl_type_arrayGetValue(curTestData->type, curTestData->testData, 2, 1));
    } else {
        assert_true(false);
    }

    swl_ttb_assertTypeEquals(curTestData->type, target,
                             swl_type_arrayGetValue(curTestData->type, curTestData->resultData, TEST_ITEM_MAX, item));


    if(copy) {
        swl_type_cleanupPtr(curTestData->type, &target);
    } else {
        swl_type_cleanup(curTestData->type, target);
    }


}

static void test_swl_typeMath_add(void** state _UNUSED) {
    s_doValueTest(TEST_ITEM_ADD);
}

static void test_swl_typeMath_sub(void** state _UNUSED) {
    s_doValueTest(TEST_ITEM_SUB);
}


static void test_swl_typeMath_mul(void** state _UNUSED) {
    s_doValueTest(TEST_ITEM_MUL);
}
static void test_swl_typeMath_div(void** state _UNUSED) {
    s_doValueTest(TEST_ITEM_DIV);
}
static void test_swl_typeMath_mod(void** state _UNUSED) {
    s_doValueTest(TEST_ITEM_MOD);
}



static int setup_suite(void** state _UNUSED) {
    for(size_t i = 0; i < 2; i++) {
        swl_mapCharInt32_init(&testMapVal[i]);
    }
    for(size_t i = 0; i < TEST_ITEM_MAX; i++) {
        swl_mapCharInt32_init(&testMapResult[i]);
    }
    swl_mapCharInt32_init(&zeroMap);
    swl_mapCharInt32_add(&testMapVal[0], "ABC", 5);
    swl_mapCharInt32_add(&testMapVal[1], "ABC", 9);
    swl_mapCharInt32_add(&zeroMap, "ABC", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_ADD], "ABC", 14);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_SUB], "ABC", -4);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MUL], "ABC", 45);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_DIV], "ABC", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MOD], "ABC", 5);

    swl_mapCharInt32_add(&testMapVal[0], "DEF", -50);
    swl_mapCharInt32_add(&testMapVal[1], "DEF", -10);
    swl_mapCharInt32_add(&zeroMap, "DEF", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_ADD], "DEF", -60);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_SUB], "DEF", -40);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MUL], "DEF", 500);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_DIV], "DEF", 5);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MOD], "DEF", 0);

    //Only entry for HIJ
    swl_mapCharInt32_add(&testMapVal[0], "HIJ", 10);
    swl_mapCharInt32_add(&zeroMap, "HIJ", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_ADD], "HIJ", 10);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_SUB], "HIJ", 10);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MUL], "HIJ", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_DIV], "HIJ", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MOD], "HIJ", 0);

    swl_mapCharInt32_add(&zeroMap, "KLM", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_ADD], "KLM", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_SUB], "KLM", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MUL], "KLM", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_DIV], "KLM", 0);
    swl_mapCharInt32_add(&testMapResult[TEST_ITEM_MOD], "KLM", 0);

    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    for(size_t i = 0; i < 2; i++) {
        swl_mapCharInt32_cleanup(&testMapVal[i]);
    }
    for(size_t i = 0; i < TEST_ITEM_MAX; i++) {
        swl_mapCharInt32_cleanup(&testMapResult[i]);
    }
    swl_mapCharInt32_cleanup(&zeroMap);

    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_typeMath_add_number),
        cmocka_unit_test(test_swl_typeMath_add_tuple),
    };

    ttb_util_setFilter();
    int rc1 = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    const struct CMUnitTest typedTests[] = {
        cmocka_unit_test(test_swl_typeMath_add),
        cmocka_unit_test(test_swl_typeMath_sub),
        cmocka_unit_test(test_swl_typeMath_mul),
        cmocka_unit_test(test_swl_typeMath_div),
        cmocka_unit_test(test_swl_typeMath_mod),
    };
    size_t start = 0;
    size_t end = NR_TEST_TYPES - 1;
    if(gTtb_assert_topIndex >= 0) {
        start = end = gTtb_assert_topIndex;
    }

    for(size_t i = start; i <= end; i++) {
        curTestData = &testData[i];
        printf("Test %zu\n", i);
        rc1 += cmocka_run_group_tests(typedTests, setup_suite, teardown_suite);
    }


    sahTraceClose();
    return rc1;
}
