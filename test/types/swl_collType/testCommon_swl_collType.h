/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_TEST_TYPES_SWL_COLLTYPE_TESTCOMMON_SWL_COLLTYPE_H_
#define SRC_TEST_TYPES_SWL_COLLTYPE_TESTCOMMON_SWL_COLLTYPE_H_
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/types/swl_ttaType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/ttb/swl_ttb.h"

typedef struct {
    swl_listSTypeFun_t* fun; //Collection Under Test
    swl_listSType_t* type;   //Collection Under Test
    size_t maxSize;
    bool allowEmpty;
    size_t colSize;
    const char* printFolder; // Folder for print tests
    bool implicitSize;       // Whether the size is implicitly calculated from content
} swl_colTypeTest_t;

int testCommon_swl_collType_runTest(swl_colTypeTest_t* tut);

#define MY_COLL_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT_H(gtSwl_collTypeTestTT, swl_collTypeTestTT_t, MY_COLL_TEST_TABLE_VAR, );

#define NR_COLL_TYPE_TEST_TYPES 3

extern char* swl_colTypeTestNames[];

//Default nr values per collection
#define NR_COLL_TYPE_TEST_VALUES_DEFAULT 4


#define NR_COLL_TYPE_TEST_VALUES 18
//
extern swl_collTypeTestTT_t collTypeTestValues[];

extern void* collTypeTestArrays[];


#define NR_COLL_TYPE_MASK_VALUES (2 << NR_COLL_TYPE_TEST_TYPES)
extern swl_collTypeTestTT_t collTypeMaskValues[];


SWL_ARRAY_BUF_TYPE_H(gtSwl_collTypeTestArrayBuf, gtSwl_collTypeTestTT, 4);

#define NR_COLL_TYPE_TEST_DATA_VALUES 6
extern gtSwl_collTypeTestArrayBuf_type collTypeTestData[];




#endif /* SRC_TEST_TYPES_SWL_COLLTYPE_TESTCOMMON_SWL_COLLTYPE_H_ */
