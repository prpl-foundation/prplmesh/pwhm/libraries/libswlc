/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * Tests specifically to test ref types.
 */


#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/swl_common_tupleType.h"

#define NR_TYPES 6



//No array type where empty is not allowed, as it should not work.
SWL_ARRAY_TYPE(tMyTestArrayCharPtrTypeNull, gtSwl_type_charPtr, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufCharPtrType, gtSwl_type_charPtr, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListCharPtrType, gtSwl_type_charPtr, false);
SWL_VECTOR(tMyTestVectorCharPtrType, gtSwl_type_charPtr, false);


#define DEFAULT_NR_VALUES 4
#define DEFAULT_NR_NON_NULL 2

char* myTestCharPtrEmpty[] = {
    "",
    NULL,
    NULL,
    "",
};


typedef struct {
    swl_listSTypeFun_t* fun; //Collection Under Test
    swl_listSType_t* type;   //Collection Under Test
    size_t maxSize;
    bool allowEmpty;
    size_t colSize;
} swl_colTest_t;

static swl_colTest_t* s_ct;
static swl_type_t* s_elType;


typedef struct testData {
    swl_colTest_t* testArray;
    swl_coll_t* srcCollection;
    size_t srcColSize;
    swl_type_t* type;
} swlc_type_testCommonData_t;



static int setup_suite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->name);
    s_ct->colSize = s_ct->type->size;
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}


swl_colTest_t testColCharPtr[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayCharPtrTypeNull.type.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufCharPtrType.type.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListCharPtrType.type.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorCharPtrType.type.type, },
};


static void test_swl_collTypeRefInit(void** state _UNUSED) {
    //Simple init
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);
    s_ct->fun->cleanup(s_ct->type, col);
    memset(buf, 0, s_ct->colSize);

    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, myTestCharPtrEmpty, DEFAULT_NR_VALUES), SWL_RC_OK);

    if(s_ct->allowEmpty) {
        assert_int_equal(s_ct->fun->size(s_ct->type, col), DEFAULT_NR_NON_NULL);
    } else {
        assert_int_equal(s_ct->fun->size(s_ct->type, col), DEFAULT_NR_VALUES);
    }

    for(size_t i = 0; i < DEFAULT_NR_VALUES; i++) {
        assert_true(swl_type_equals(s_elType, s_ct->fun->getValue(s_ct->type, col, i),
                                    swl_type_arrayGetValue(s_elType, myTestCharPtrEmpty, DEFAULT_NR_VALUES, i)));
    }

    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collTypeRefToFromChar(void** state _UNUSED) {
    //Simple init
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);
    s_ct->fun->cleanup(s_ct->type, col);
    memset(buf, 0, s_ct->colSize);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, myTestCharPtrEmpty, DEFAULT_NR_VALUES), SWL_RC_OK);


    char buffer[256];
    memset(buffer, 0, sizeof(buffer));
    swl_type_toChar((swl_type_t*) s_ct->type, buffer, sizeof(buffer), col);

    printf("%s\n", buffer);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    swl_type_fromChar((swl_type_t*) s_ct->type, col2, buffer);
    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));

    s_ct->fun->cleanup(s_ct->type, col2);



#define NR_PRINTS 4
    const swl_print_args_t* printArgs[4] = {
        swl_print_getDefaultArgs(),
        &g_swl_print_json,
        &g_swl_print_dm,
        &g_swl_print_csv

    };
    const char* names[] = {"default", "json", "dm", "csv"};

    for(size_t i = 0; i < NR_PRINTS; i++) {
        printf("Testing -%s-\n", names[i]);
        char* data = swl_type_toCStringExt((swl_type_t*) s_ct->type, col, printArgs[i]);
        printf("%s\n", data);

        swl_type_fromCharExt((swl_type_t*) s_ct->type, col2, data, printArgs[i]);
        free(data);

        char* data2 = swl_type_toCStringExt((swl_type_t*) s_ct->type, col2, printArgs[i]);
        printf("%s\n", data2);
        free(data2);

        assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));

        s_ct->fun->cleanup(s_ct->type, col2);
    }


    s_ct->fun->cleanup(s_ct->type, col);
}



int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_collTypeRefInit),
        cmocka_unit_test(test_swl_collTypeRefToFromChar),
    };

    int rc = 0;

    s_elType = swl_type_charPtr;
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testColCharPtr); i++) {
        s_ct = &testColCharPtr[i];
        rc += cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    }

    sahTraceClose();
    return rc;
}
