/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/types/swl_ttaType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/ttb/swl_ttb.h"
#include "swl/types/swl_tupleTypeArray.h"

#include "testCommon_swl_collVarFixedValType.h"

/**
 * This file encodes the test of a collection, where each entry of the collection is potentially of different types.
 * Hence it is up to the test struct provided to list types & values
 */


static swl_colVarValTypeTest_t* s_ct;

// Empty initialized collection
static swl_coll_t* s_emptyColl;
//Collection filled with first tuple
static swl_coll_t* s_filledColl;


//For fixedValTypes, add, alloc insert is not supported.
static void test_swl_collectionAddAllocInsert(void** state _UNUSED) {
    for(size_t i = 0; i < s_ct->nrValues; i++) {
        assert_int_equal(s_ct->fun->add(s_ct->type, s_emptyColl,
                                        swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 1, i)), SWL_RC_NOT_IMPLEMENTED);


        assert_true(s_ct->fun->insert(s_ct->type, s_emptyColl,
                                      swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 1, i), i) < 0);


        assert_int_equal(s_ct->fun->find(s_ct->type, s_emptyColl,
                                         swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 1, i)), SWL_RC_NOT_IMPLEMENTED);
    }

    assert_null(s_ct->fun->alloc(s_ct->type, s_emptyColl));
}


static void test_swl_collectionSet(void** state _UNUSED) {
    swl_typeData_t* findKey = NULL;

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        swl_typeData_t* otherValue = swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 1, i);
        swl_typeData_t* tmpValData = swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i);

        findKey = s_ct->fun->getValue(s_ct->type, s_filledColl, i);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[i], findKey, tmpValData);


        s_ct->fun->set(s_ct->type, s_filledColl, otherValue, i);
        findKey = s_ct->fun->getValue(s_ct->type, s_filledColl, i);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[i], findKey, otherValue);

        s_ct->fun->set(s_ct->type, s_filledColl, tmpValData, i);
        findKey = s_ct->fun->getValue(s_ct->type, s_filledColl, i);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[i], findKey, tmpValData);
    }
}


static void test_swl_collectionEqualsDelete(void** state _UNUSED) {
    swl_coll_t* myColl = calloc(1, s_ct->type->size);
    s_ct->fun->init(s_ct->type, myColl);

    assert_true(s_ct->fun->equals(s_ct->type, myColl, s_emptyColl));
    assert_false(s_ct->fun->equals(s_ct->type, myColl, s_filledColl));

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_ct->fun->set(s_ct->type, myColl,
                       swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i),
                       i
                       );
        assert_false(s_ct->fun->equals(s_ct->type, myColl, s_emptyColl));
        if(i < s_ct->nrValues - 1) {
            assert_false(s_ct->fun->equals(s_ct->type, myColl, s_filledColl));
        } else {
            assert_true(s_ct->fun->equals(s_ct->type, myColl, s_filledColl));
        }
    }

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        assert_int_equal(s_ct->fun->size(s_ct->type, myColl), s_ct->nrValues - i);

        s_ct->fun->delete(s_ct->type, myColl, i);
        assert_int_equal(s_ct->fun->size(s_ct->type, myColl), s_ct->nrValues - i - 1);

        assert_false(s_ct->fun->equals(s_ct->type, myColl, s_filledColl));
        if(i < s_ct->nrValues - 1) {
            assert_false(s_ct->fun->equals(s_ct->type, myColl, s_emptyColl));
        } else {
            assert_true(s_ct->fun->equals(s_ct->type, myColl, s_emptyColl));
        }

        for(size_t j = 0; j < s_ct->nrValues; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, myColl, j);
            if(j <= i) {
                assert_true(swl_type_isEmpty(s_ct->valueTupleType->types[j], data));
            } else {
                swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[j],
                                         swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, j),
                                         data);
            }
        }

    }


    s_ct->fun->cleanup(s_ct->type, myColl);
    free(myColl);
}

static void test_swl_collectionGetVal(void** state _UNUSED) {
    swl_typeData_t* findKey = NULL;

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        swl_typeData_t* tmpValData = swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i);

        findKey = s_ct->fun->getValue(s_ct->type, s_filledColl, i);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[i], findKey, tmpValData);
    }
}

static void test_swl_collectionGetRef(void** state _UNUSED) {
    swl_typeData_t* findKey = NULL;

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        swl_typeEl_t* tmpValData = swl_tta_getElementReference(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i);

        findKey = s_ct->fun->getReference(s_ct->type, s_filledColl, i);
        assert_non_null(findKey);
        swl_ttb_assertTypeElEquals(s_ct->valueTupleType->types[i], findKey, tmpValData);
    }

}

static void test_swl_collectionIterate(void** state _UNUSED) {
    swl_listSTypeIt_t it = s_ct->fun->firstIt(s_ct->type, s_filledColl);
    assert_true(it.valid);
    size_t counter = 0;
    while(it.valid) {
        assert_ptr_equal(it.dataType, s_ct->valueTupleType->types[counter]);

        swl_typeEl_t* tmpValData = swl_tta_getElementReference(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, counter);
        swl_ttb_assertTypeElEquals(s_ct->valueTupleType->types[counter], it.data, tmpValData);

        counter++;
        s_ct->fun->nextIt(s_ct->type, &it);
    }
    assert_int_equal(counter, s_ct->nrValues);
}

static void test_swl_collectionDelIt(void** state _UNUSED) {
    swl_coll_t* myColl = calloc(1, s_ct->type->size);
    s_ct->fun->init(s_ct->type, myColl);

    assert_true(s_ct->fun->equals(s_ct->type, myColl, s_emptyColl));
    assert_false(s_ct->fun->equals(s_ct->type, myColl, s_filledColl));

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_ct->fun->set(s_ct->type, myColl,
                       swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i),
                       i
                       );
    }

    swl_listSTypeIt_t it = s_ct->fun->firstIt(s_ct->type, myColl);
    assert_true(it.valid);
    size_t counter = 0;
    while(it.valid) {
        assert_ptr_equal(it.dataType, s_ct->valueTupleType->types[counter]);

        s_ct->fun->delIt(s_ct->type, &it);

        for(size_t j = 0; j < s_ct->nrValues; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, myColl, j);
            if(j <= counter) {
                assert_true(swl_type_isEmpty(s_ct->valueTupleType->types[j], data));
            } else {
                swl_ttb_assertTypeEquals(s_ct->valueTupleType->types[j],
                                         swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, j),
                                         data);
            }
        }

        counter++;
        s_ct->fun->nextIt(s_ct->type, &it);
    }
    assert_int_equal(counter, s_ct->nrValues);

    s_ct->fun->cleanup(s_ct->type, myColl);
    free(myColl);
}


static int setup_suite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->type->typeFun->name);

    s_emptyColl = calloc(1, s_ct->type->size);
    s_ct->fun->init(s_ct->type, s_emptyColl);


    s_filledColl = calloc(1, s_ct->type->size);
    s_ct->fun->init(s_ct->type, s_filledColl);


    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_ct->fun->set(s_ct->type, s_filledColl,
                       swl_tta_getElementValue(s_ct->valueTupleType, s_ct->tupleList, s_ct->nrTuples, 0, i),
                       i
                       );
    }


    assert_int_equal(s_ct->fun->size(s_ct->type, s_emptyColl), 0);
    assert_int_equal(s_ct->fun->size(s_ct->type, s_filledColl), s_ct->nrValues);


    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    assert_int_equal(s_ct->fun->size(s_ct->type, s_emptyColl), 0);
    assert_int_equal(s_ct->fun->size(s_ct->type, s_filledColl), s_ct->nrValues);

    s_ct->fun->cleanup(s_ct->type, s_emptyColl);
    free(s_emptyColl);
    s_emptyColl = NULL;

    s_ct->fun->cleanup(s_ct->type, s_filledColl);
    free(s_filledColl);
    s_filledColl = NULL;

    return 0;
}

int testCommon_swl_collVarValType_runTest(swl_colVarValTypeTest_t* tut) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_collectionAddAllocInsert),
        cmocka_unit_test(test_swl_collectionSet),
        cmocka_unit_test(test_swl_collectionEqualsDelete),
        cmocka_unit_test(test_swl_collectionGetVal),
        cmocka_unit_test(test_swl_collectionGetRef),
        cmocka_unit_test(test_swl_collectionIterate),
        cmocka_unit_test(test_swl_collectionDelIt),

    };

    s_ct = tut;

    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);


    sahTraceClose();
    return rc;
}
