/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/types/swl_ttaType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/ttb/swl_ttb.h"

#include "testCommon_swl_collType.h"

/**
 * This file encodes the test of a collection, where each entry of the collection is of the same type.
 * However, for generic list types, this is not necessary.
 */

#define NR_TYPES 6


SWL_TT_C(gtSwl_collTypeTestTT, swl_collTypeTestTT_t, MY_COLL_TEST_TABLE_VAR)

char* swl_colTypeTestNames[NR_COLL_TYPE_TEST_TYPES] = {"CTest", "CIndex", "CVal"};

/**
 * Defining a tuple type using a table
 *
 * These values are different from any in the test data
 * Note to ensure list exceeds 10, to ensure multi block vector, and exceed default arraylist capacity.
 */
swl_collTypeTestTT_t collTypeTestValues[NR_COLL_TYPE_TEST_VALUES] = {
    {1, 100, "atest1"},
    {2, -200, "atest2"},
    {-88, 289870, "afoobar"},
    {88, -28989, "abarfoo"},
    {99, -2879, "atest5"},
    {101, -2899, "a1234"},
    {-2, -289869, "!&*/?\"`"},
    {-98, -69, ""},
    {11, -2861, "ztest1"},
    {12, -2862, "ztest2"},
    {13, -2863, "ztest3"},
    {14, -2864, "ztest4"},
    {15, -2865, "ztest5"},
    {16, -2866, "ztest6"},
    {17, -2867, "ztest7"},
    {18, -2868, "ztest8"},
    {19, -2869, "ztest9"},
    {10, -2860, "ztest0"},
};

int8_t collTypeTestValues_0[NR_COLL_TYPE_TEST_VALUES] = {1, 2, -88, 88, 99, 101, -2, -98, 11, 12, 13, 14, 15, 16, 17, 18, 19, 10};
int64_t collTypeTestValues_1[NR_COLL_TYPE_TEST_VALUES] = {100, -200, 289870, -28989, -2879, -2899, -289869, -69, -2861, -2862,
    -2863, -2864, -2865, -2866, -2867, -2868, -2869, -2860 };
char* collTypeTestValues_2[NR_COLL_TYPE_TEST_VALUES] = {"atest1", "atest2", "afoobar", "abarfoo", "atest5", "a1234",
    "!&*/?\"`", "", "ztest1", "ztest2", "ztest3", "ztest4", "ztest5",
    "ztest6", "ztest7", "ztest8", "ztest9", "ztest0"};

void* collTypeTestArrays[NR_COLL_TYPE_TEST_TYPES] = {collTypeTestValues_0, collTypeTestValues_1, collTypeTestValues_2};

swl_collTypeTestTT_t collTypeMaskValues[NR_COLL_TYPE_MASK_VALUES] = {
    {2, 200, "test2"},
    {1, 200, "test2"},
    {2, 100, "test2"},
    {1, 100, "test2"},
    {2, 200, "test1"},
    {1, 200, "test1"},
    {2, 100, "test1"},
    {1, 100, "test1"},
};

#define DEFAULT_NR_VALUES 4

SWL_ARRAY_BUF_TYPE_C(gtSwl_collTypeTestArrayBuf, gtSwl_collTypeTestTT, 4, false);


gtSwl_collTypeTestArrayBuf_type collTypeTestData[NR_COLL_TYPE_TEST_DATA_VALUES] = {
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4 },
    {.data = {
            {2, 100, "test1"}, // 2 changed here
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -202, "test2"}, // -202 changed here
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobars"}, // fobars changed here
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {0, 0, NULL},
        }, .size = 3},
    {.data = {
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
        }, .size = 0},
};


static swl_colTypeTest_t* s_ct;
static swl_type_t* s_elType;

static void test_swl_setupClean(void** state _UNUSED) {
    //Simple init
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);
    s_ct->fun->cleanup(s_ct->type, col);


    //Init from array
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestData); i++) {
        memset(buf, 0, s_ct->colSize);
        assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), collTypeTestData[i].size);

        for(size_t j = 0; j < collTypeTestData[i].size; j++) {
            swl_ttb_assertTypeEquals(s_elType, s_ct->fun->getValue(s_ct->type, col, j), &collTypeTestData[i].data[j]);
        }

        s_ct->fun->cleanup(s_ct->type, col);
    }

    //Init clear
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestData); i++) {
        memset(buf, 0, s_ct->colSize);
        assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);

        assert_int_equal(s_ct->fun->size(s_ct->type, col), collTypeTestData[i].size);
        s_ct->fun->clear(s_ct->type, col);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);

        s_ct->fun->cleanup(s_ct->type, col);
    }

    assert_int_equal(s_ct->fun->maxSize(s_ct->type, col), s_ct->maxSize);
}

static void test_swl_collectionAdd(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);


    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);

    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {
        ssize_t index = s_ct->fun->add(s_ct->type, col, &collTypeTestValues[i]);
        if(index < 0) {
            assert_true(s_ct->maxSize != 0 && i >= s_ct->maxSize);
            assert_int_equal(s_ct->fun->size(s_ct->type, col), s_ct->maxSize);
        } else {

            assert_int_equal(index, i);
            if(s_ct->maxSize != 0) {
                assert_true(i < s_ct->maxSize);
            }
            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);

            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
            }
        }
    }
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionAlloc(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);


    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);

    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {
        swl_typeEl_t* ptr = s_ct->fun->alloc(s_ct->type, col);
        if(ptr == NULL) {
            assert_true(s_ct->maxSize != 0 && i >= s_ct->maxSize);
            assert_int_equal(s_ct->fun->size(s_ct->type, col), s_ct->maxSize);
        } else {
            if(s_ct->maxSize != 0) {
                assert_true(i < s_ct->maxSize);
            }
            swl_type_copyTo(s_elType, ptr, &collTypeTestValues[i]);
            // note copy needs before size, as size counts "non NULL" entries
            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);

            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
            }
        }
    }
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionInsert(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);

    //insert 0
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {
        swl_rc_ne ret = s_ct->fun->insert(s_ct->type, col, &collTypeTestValues[i], 0);
        assert_int_equal(ret, SWL_RC_OK);
        if(s_ct->maxSize == 0) {
            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);
            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, i - j);
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
            }
        } else {
            size_t expectedSize = SWL_MIN(s_ct->maxSize, i + 1);
            assert_int_equal(s_ct->fun->size(s_ct->type, col), expectedSize);
            for(size_t j = 0; j < expectedSize; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[i - j]));
            }
        }
    }
    s_ct->fun->clear(s_ct->type, col);

    //insert -1
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {
        ttb_assert_addPrint("value %zu", i);
        swl_rc_ne ret = s_ct->fun->insert(s_ct->type, col, &collTypeTestValues[i], -1);
        assert_int_equal(ret, SWL_RC_OK);
        if(s_ct->maxSize == 0) {
            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);
            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                swl_ttb_assertTypeEquals(s_elType, data, &collTypeTestValues[j]);
            }
        } else if(!s_ct->allowEmpty) {
            size_t expectedSize = SWL_MIN(s_ct->maxSize - 1, i);
            for(size_t j = 0; j < expectedSize && j != i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
            }
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, expectedSize);
            swl_ttb_assertTypeEquals(s_elType, data, &collTypeTestValues[i]);
        } else {
            for(size_t j = 0; j < s_ct->maxSize - 1 && j != i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                assert_true(swl_type_isEmpty(s_elType, data));
            }
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, s_ct->maxSize - 1);
            swl_ttb_assertTypeEquals(s_elType, data, &collTypeTestValues[i]);
        }
        ttb_assert_removeLastPrint();
    }

    s_ct->fun->clear(s_ct->type, col);

    //insert 1
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {
        swl_rc_ne ret = s_ct->fun->insert(s_ct->type, col, &collTypeTestValues[i], SWL_MIN(i, (size_t) 1));
        assert_int_equal(ret, SWL_RC_OK);
        if(s_ct->maxSize == 0) {
            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);

            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                size_t expectedVal = (j == 0) ? (size_t) 0 : (size_t) i - j + 1;
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[expectedVal]));
            }
        } else {
            size_t expectedSize = SWL_MIN(s_ct->maxSize, i + 1);
            assert_int_equal(s_ct->fun->size(s_ct->type, col), expectedSize);
            for(size_t j = 0; j < expectedSize; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);

                size_t expectedVal = (j == 0) ? (size_t) 0 : (size_t) i - j + 1;

                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[expectedVal]));
            }
        }
    }

    s_ct->fun->clear(s_ct->type, col);

    //insert - 1 * i
    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestValues); i++) {


        if(s_ct->maxSize == 0) {
            ssize_t insertIndex = (ssize_t) (i < 2 ? (ssize_t) i : -1 * (ssize_t) i);
            swl_rc_ne ret = s_ct->fun->insert(s_ct->type, col, &collTypeTestValues[i], insertIndex);
            assert_int_equal(ret, SWL_RC_OK);

            assert_int_equal(s_ct->fun->size(s_ct->type, col), i + 1);

            for(size_t j = 0; j <= i; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                size_t expectedVal = (j == 0) ? (size_t) 0 : (size_t) i - j + 1;
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[expectedVal]));
            }
        } else if(!s_ct->allowEmpty) {
            ssize_t insertIndex = SWL_MAX((ssize_t) -3, (ssize_t) (i < 2 ? (ssize_t) i : -1 * (ssize_t) i));
            swl_rc_ne ret = s_ct->fun->insert(s_ct->type, col, &collTypeTestValues[i], insertIndex);
            assert_int_equal(ret, SWL_RC_OK);

            size_t expectedSize = SWL_MIN(s_ct->maxSize, i + 1);
            assert_int_equal(s_ct->fun->size(s_ct->type, col), expectedSize);
            for(size_t j = 0; j < expectedSize; j++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
                size_t expectedVal = (j == 0) ? (size_t) 0 : (size_t) i - j + 1;

                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[expectedVal]));
            }
        }
    }
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionSet(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];


    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestData); i++) {
        ttb_assert_addPrint("%zu", i);
        swl_ttb_assertRcOk(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size));

        for(size_t k = 0; k < collTypeTestData[i].size; k++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, k);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestData[i].data[k]));
        }

        for(size_t j = 0; j < DEFAULT_NR_VALUES; j++) {
            swl_rc_ne ret = s_ct->fun->set(s_ct->type, col, &collTypeTestValues[j], j);
            if(!s_ct->allowEmpty && (j >= collTypeTestData[i].size)) {
                assert_false(swl_rc_isOk(ret));
                continue;
            }
            for(size_t k = 0; k < DEFAULT_NR_VALUES; k++) {
                swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, k);
                if(k < j + 1) {
                    assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[k]));
                } else if(k < collTypeTestData[i].size) {
                    assert_true(swl_type_equals(s_elType, data, &collTypeTestData[i].data[k]));
                } else if(!s_ct->allowEmpty) {
                    assert_null(data);
                } else {
                    assert_true(swl_type_isEmpty(s_elType, data));
                }
            }
        }

        s_ct->fun->cleanup(s_ct->type, col);
        ttb_assert_removeLastPrint();
    }
}


static void test_swl_collectionDelete(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);

    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);


    //delete 0
    for(size_t i = 0; i < targetSize; i++) {
        size_t nrValLeft = targetSize - 1 - i;
        swl_rc_ne ret = s_ct->fun->delete(s_ct->type, col, 0);
        assert_int_equal(ret, SWL_RC_OK);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), nrValLeft);


        for(size_t j = 0; j < nrValLeft; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[1 + i + j]));
        }
    }

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);
    s_ct->fun->cleanup(s_ct->type, col);


    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);


    //delete -1
    for(size_t i = 0; i < targetSize; i++) {
        size_t nrValLeft = targetSize - 1 - i;
        swl_rc_ne ret = s_ct->fun->delete(s_ct->type, col, -1);
        assert_int_equal(ret, SWL_RC_OK);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), nrValLeft);


        for(size_t j = 0; j < nrValLeft; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
        }
    }

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);
    s_ct->fun->cleanup(s_ct->type, col);


    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);


    //delete 1
    for(size_t i = 0; i < targetSize - 1; i++) {
        size_t nrValLeft = targetSize - 1 - i;
        swl_rc_ne ret = s_ct->fun->delete(s_ct->type, col, 1);
        assert_int_equal(ret, SWL_RC_OK);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), nrValLeft);


        for(size_t j = 0; j < nrValLeft; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j == 0 ? 0 : j + i + 1]));
        }
    }

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 1);
    s_ct->fun->cleanup(s_ct->type, col);

    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);


    //delete -2
    for(size_t i = 0; i < targetSize - 1; i++) {
        size_t nrValLeft = targetSize - 1 - i;
        swl_rc_ne ret = s_ct->fun->delete(s_ct->type, col, -2);
        assert_int_equal(ret, SWL_RC_OK);
        assert_int_equal(s_ct->fun->size(s_ct->type, col), nrValLeft);


        for(size_t j = 0; j < nrValLeft; j++) {
            swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, j);
            if(j + 2 < targetSize - i) {
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[j]));
            } else {
                assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[targetSize - 1]));
            }
        }
    }

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 1);
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionFind(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];

    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);

    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues, targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);

    for(size_t i = 0; i < targetSize; i++) {
        for(size_t j = 0; j < targetSize; j++) {
            ssize_t index = s_ct->fun->find(s_ct->type, col, &collTypeTestValues[j]);
            if(j >= i) {
                assert_int_equal(index, j - i);
            } else {
                assert_int_equal(index, -1);
            }
        }
        s_ct->fun->delete(s_ct->type, col, 0);
    }

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionEquals(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* colBase = &buf[0];

    swl_bit8_t bufCmp[s_ct->colSize];
    memset(bufCmp, 0, s_ct->colSize);
    swl_coll_t* colCmp = &bufCmp[0];


    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestData); i++) {
        assert_int_equal(s_ct->fun->initFromArray(s_ct->type, colBase, &collTypeTestData[i].data, collTypeTestData[i].size), SWL_RC_OK);
        for(size_t j = 0; j < SWL_ARRAY_SIZE(collTypeTestData); j++) {
            assert_int_equal(s_ct->fun->initFromArray(s_ct->type, colCmp, &collTypeTestData[j].data, collTypeTestData[j].size), SWL_RC_OK);

            bool equals = s_ct->fun->equals(s_ct->type, colBase, colCmp);
            assert_int_equal(equals, i == j);
            s_ct->fun->cleanup(s_ct->type, colCmp);
        }
        s_ct->fun->cleanup(s_ct->type, colBase);
    }


    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, colBase, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, colCmp, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    for(size_t i = 0; i < targetSize; i++) {
        assert_true(s_ct->fun->equals(s_ct->type, colBase, colCmp));
        assert_int_equal(s_ct->fun->set(s_ct->type, colCmp, &collTypeTestData[0].data, i), SWL_RC_OK);
        assert_false(s_ct->fun->equals(s_ct->type, colBase, colCmp));
        assert_int_equal(s_ct->fun->set(s_ct->type, colCmp, &collTypeTestValues[i], i), SWL_RC_OK);
        assert_true(s_ct->fun->equals(s_ct->type, colBase, colCmp));
    }
    s_ct->fun->cleanup(s_ct->type, colBase);
    s_ct->fun->cleanup(s_ct->type, colCmp);
}

static void test_swl_collectionGetVal(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    for(size_t i = 0; i < targetSize; i++) {
        swl_typeData_t* data = s_ct->fun->getValue(s_ct->type, col, i);
        assert_ptr_not_equal(data, &collTypeTestValues[i]);
        assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[i]));
    }
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionGetRef(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    for(size_t i = 0; i < targetSize; i++) {
        swl_typeEl_t* data = s_ct->fun->getValue(s_ct->type, col, i);
        assert_ptr_not_equal(data, &collTypeTestValues[i]);
        assert_true(swl_type_equals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, data), &collTypeTestValues[i]));
    }
    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionIterate(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    size_t index = 0;
    {swl_listSType_forEachRef(s_ct->type, it, swl_collTypeTestTT_t, data, col) {
            assert_int_equal(index, it.index);
            swl_ttb_assertTypeEquals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, data), &collTypeTestValues[index]);
            index++;
        }}
    assert_int_equal(index, targetSize);

    index = 0;

    swl_listSType_forEachVal(s_ct->type, it, swl_collTypeTestTT_t, valData, col) {
        assert_int_equal(index, it.index);
        assert_true(swl_type_equals(s_elType, &valData, &collTypeTestValues[index]));
        index++;
    }
    assert_int_equal(index, targetSize);

    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);

    index = 0;
    swl_listSType_forEachRef(s_ct->type, it, swl_collTypeTestTT_t, data, col) {

        //we're deleting each entry, so no clean
        if(!s_ct->allowEmpty) {
            assert_int_equal(0, it.index);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[index]));
            swl_listSType_delIt(s_ct->type, &it);
        } else {
            assert_int_equal(index, it.index);
            assert_true(swl_type_equals(s_elType, data, &collTypeTestValues[index]));
            swl_listSType_delIt(s_ct->type, &it);
        }

        index++;
    }

    assert_int_equal(index, targetSize);
    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);

    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_type_print(void** state _UNUSED) {
    if(s_ct->printFolder == NULL) {
        return;
    }

    //Check long print
    char fileBuffer[128];

    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? SWL_ARRAY_SIZE(collTypeTestValues) : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    swl_print_args_t tmpArgs = g_swl_print_jsonCompact;

    snprintf(fileBuffer, sizeof(fileBuffer), "%s/%sPrint.txt", s_ct->printFolder, s_ct->maxSize == 0 ? "long":"short");

    swl_ttb_assertTypeToFileEquals(s_ct->type, fileBuffer, col, &tmpArgs, false);

    s_ct->fun->cleanup(s_ct->type, col);

    for(size_t i = 0; i < SWL_ARRAY_SIZE(collTypeTestData); i++) {

        if(collTypeTestData[i].size == NR_COLL_TYPE_TEST_VALUES_DEFAULT) {
            memset(buf, 0, s_ct->colSize);
            assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);
            snprintf(fileBuffer, sizeof(fileBuffer), "%s/test_%zu.txt", s_ct->printFolder, i);

            swl_ttb_assertTypeToFileEquals(s_ct->type, fileBuffer, col, &tmpArgs, false);

            s_ct->fun->cleanup(s_ct->type, col);
        } else {
            // Test reduced init

            memset(buf, 0, s_ct->colSize);
            assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);

            if((s_ct->maxSize == 0) || (s_ct->implicitSize && !s_ct->allowEmpty)) {
                snprintf(fileBuffer, sizeof(fileBuffer), "%s/testNoEmpty_%zu.txt", s_ct->printFolder, i);
            } else {
                snprintf(fileBuffer, sizeof(fileBuffer), "%s/testWithEmpty_%zu.txt", s_ct->printFolder, i);
            }

            swl_ttb_assertTypeToFileEquals(s_ct->type, fileBuffer, col, &tmpArgs, false);

            s_ct->fun->cleanup(s_ct->type, col);


            // Test empty init
            if((s_ct->maxSize == 0) || s_ct->allowEmpty) {
                memset(buf, 0, s_ct->colSize);
                assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, &collTypeTestData[i].data[0], NR_COLL_TYPE_TEST_VALUES_DEFAULT), SWL_RC_OK);

                snprintf(fileBuffer, sizeof(fileBuffer), "%s/testWithEmpty_%zu.txt", s_ct->printFolder, i);
                swl_ttb_assertTypeToFileEquals(s_ct->type, fileBuffer, col, &tmpArgs, false);

                s_ct->fun->cleanup(s_ct->type, col);
            }
        }
    }
}

static int setup_suite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->name);
    s_ct->colSize = s_ct->type->size;
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int testCommon_swl_collType_runTest(swl_colTypeTest_t* tut) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_setupClean),
        cmocka_unit_test(test_swl_collectionAdd),
        cmocka_unit_test(test_swl_collectionAlloc),
        cmocka_unit_test(test_swl_collectionInsert),
        cmocka_unit_test(test_swl_collectionSet),
        cmocka_unit_test(test_swl_collectionDelete),
        cmocka_unit_test(test_swl_collectionFind),
        cmocka_unit_test(test_swl_collectionEquals),
        cmocka_unit_test(test_swl_collectionGetVal),
        cmocka_unit_test(test_swl_collectionGetRef),
        cmocka_unit_test(test_swl_collectionIterate),
        cmocka_unit_test(test_swl_type_print),

    };

    s_elType = (swl_type_t*) &gtSwl_collTypeTestTT;
    s_ct = tut;

    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);


    sahTraceClose();
    return rc;
}
