/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/swl_common_tupleType.h"

#define NR_TYPES 6



SWL_ARRAY_TYPE(tMyTestArrayType, gtSwl_type_int32, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayTypeNull, gtSwl_type_int32, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufType, gtSwl_type_int32, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListType, gtSwl_type_int32, false);
SWL_VECTOR(tMyTestVectorType, gtSwl_type_int32, false);

SWL_ARRAY_TYPE(tMyTestArrayCharPtrType, gtSwl_type_charPtr, 4, false, false);
SWL_ARRAY_TYPE(tMyTestArrayCharPtrTypeNull, gtSwl_type_charPtr, 4, true, false);
SWL_ARRAY_BUF_TYPE(tMyTestArrayBufCharPtrType, gtSwl_type_charPtr, 4, false);
SWL_ARRAY_LIST_TYPE(tMyTestArrayListCharPtrType, gtSwl_type_charPtr, false);
SWL_VECTOR(tMyTestVectorCharPtrType, gtSwl_type_charPtr, false);

/**
 * Defining a tuple type using a table
 *
 * These values are different from any in the test data
 * Note to ensure list exceeds 10, to ensure multi block vector, and exceed default arraylist capacity.
 */
int32_t myTestValues[] = {
    1,
    2,
    200,
    -50,
    99,
    -2002,
    102012,
    0,
    99182,
    22,
    51,
    108,
    99,
    -1,
    INT32_MAX,
    INT32_MIN,
    0,
    1,
    -1
};

char* myTestCharPtrEmpty[] = {
    "1foa",
    "asdfasdf2",
    "200sdfasd",
    "-50jkasvb",
    "99abc",
    "-2002def",
    "102012",
    "",
    "qwerty",
    "22",
    "51",
    "108",
    "99",
    "-1",
    "INT32_MAX",
    "INT32_MIN",
    "0",
    "1",
    "-1"
};

#define DEFAULT_NR_VALUES 4

typedef struct {
    swl_listSTypeFun_t* fun; //Collection Under Test
    swl_listSType_t* type;   //Collection Under Test
    size_t maxSize;
    bool allowEmpty;
    size_t colSize;
} swl_colTest_t;

static swl_colTest_t* s_ct;
static swl_type_t* s_elType;


typedef struct testData {
    swl_colTest_t* testArray;
    swl_coll_t* srcCollection;
    size_t srcColSize;
    swl_type_t* type;
} collPrimTestData_t;

collPrimTestData_t* curTestData = NULL;



static int setup_suite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->name);
    s_ct->colSize = s_ct->type->size;
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

swl_colTest_t testColInt32[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayType.type.type, },
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayTypeNull.type.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufType.type.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListType.type.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorType.type.type, },
};

swl_colTest_t testColCharPtr[] = {
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayCharPtrType.type.type, },
    {.fun = &swl_arrayCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayCharPtrTypeNull.type.type,
        .allowEmpty = true, },
    {.fun = &swl_arrayBufCollectionType_fun,
        .maxSize = 4,
        .type = &tMyTestArrayBufCharPtrType.type.type, },
    {.fun = &swl_arrayListCollType_fun,
        .maxSize = 0,
        .type = &tMyTestArrayListCharPtrType.type.type, },
    {.fun = &swl_vectorCollectionType_fun,
        .maxSize = 0,
        .type = &tMyTestVectorCharPtrType.type.type, },
};

collPrimTestData_t testDataList[] = {
    { .testArray = testColInt32,
        .srcCollection = myTestValues,
        .srcColSize = SWL_ARRAY_SIZE(myTestValues),
        .type = &gtSwl_type_int32, },
    { .testArray = testColCharPtr,
        .srcCollection = myTestCharPtrEmpty,
        .srcColSize = SWL_ARRAY_SIZE(myTestCharPtrEmpty),
        .type = &gtSwl_type_charPtr, },
};


static void test_swl_setupClean(void** state _UNUSED) {
    //Simple init
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    assert_int_equal(s_ct->fun->init(s_ct->type, col), SWL_RC_OK);
    s_ct->fun->cleanup(s_ct->type, col);

    memset(buf, 0, s_ct->colSize);
    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);


    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);

    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);

    for(size_t i = 0; i < targetSize; i++) {
        assert_true(swl_type_equals(s_elType, s_ct->fun->getValue(s_ct->type, col, i),
                                    swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, i)));
    }

    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionIterate(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);

    size_t index = 0;
    {swl_listSType_forEachRef(s_ct->type, it, swl_typeEl_t, data, col) {
            assert_int_equal(index, it.index);
            assert_true(swl_type_equals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, data),
                                        swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, index)));
            index++;
        }}
    assert_int_equal(index, targetSize);

    index = 0;

    swl_listSType_forEachVal(s_ct->type, it, swl_buf128_t, valData, col) {
        assert_int_equal(index, it.index);
        if(s_elType->typeFun->isValue) {
            assert_true(swl_type_equals(s_elType, &valData,
                                        swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, index)));
        } else {
            swl_typeData_t* typeData = *(swl_typeData_t**) &valData.buf[0];
            assert_true(swl_type_equals(s_elType, typeData,
                                        swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, index)));
        }
        index++;
    }
    assert_int_equal(index, targetSize);

    assert_int_equal(s_ct->fun->size(s_ct->type, col), targetSize);

    index = 0;
    swl_listSType_forEachRef(s_ct->type, it, swl_typeEl_t, data, col) {
        //we're deleting each entry, so no clean
        printf("%zu %zu %zu\n", it.index, targetSize, index);
        assert_true(it.index < targetSize);
        assert_true(index < targetSize);
        if(!s_ct->allowEmpty) {
            assert_int_equal(0, it.index);
            assert_true(swl_type_equals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, data),
                                        swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, index)));
            swl_listSType_delIt(s_ct->type, &it);
        } else {
            assert_int_equal(index, it.index);
            assert_true(swl_type_equals(s_elType, SWL_TYPE_EL_TO_DATA(s_elType, data),
                                        swl_type_arrayGetValue(s_elType, curTestData->srcCollection, targetSize, index)));
            swl_listSType_delIt(s_ct->type, &it);
        }

        index++;
    }
    assert_int_equal(index, targetSize);

    assert_int_equal(s_ct->fun->size(s_ct->type, col), 0);


    s_ct->fun->cleanup(s_ct->type, col);
}

static void test_swl_collectionToFromCsv(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? curTestData->srcColSize : DEFAULT_NR_VALUES);
    assert_int_equal(s_ct->fun->initFromArray(s_ct->type, col, curTestData->srcCollection, targetSize), SWL_RC_OK);

    char* data = swl_type_toCStringExt((swl_type_t*) s_ct->type, col, &g_swl_print_csv);

    assert_non_null(data);

    swl_bit8_t buf2[s_ct->colSize];
    memset(buf2, 0, s_ct->colSize);
    swl_coll_t* col2 = &buf2[0];
    swl_type_fromCharExt((swl_type_t*) s_ct->type, col2, data, &g_swl_print_csv);

    char* data2 = swl_type_toCStringExt((swl_type_t*) s_ct->type, col2, &g_swl_print_csv);
    printf("DATA %s ?= %s\n", data, data2);

    assert_string_equal(data, data2);
    assert_true(swl_type_equals((swl_type_t*) s_ct->type, col, col2));



    free(data);
    free(data2);
    s_ct->fun->cleanup(s_ct->type, col);
    s_ct->fun->cleanup(s_ct->type, col2);
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_setupClean),
        cmocka_unit_test(test_swl_collectionIterate),
        cmocka_unit_test(test_swl_collectionToFromCsv),
    };



    int rc = 0;
    size_t start = 0;
    size_t end = SWL_ARRAY_SIZE(testColCharPtr) - 1;
    //start = end = 0; // Easy comment for partial debug

    for(size_t j = 0; j < 2; j++) {
        curTestData = &testDataList[j];
        s_elType = testDataList[j].type;
        for(size_t i = start; i <= end; i++) {
            s_ct = &testDataList[j].testArray[i];
            rc += cmocka_run_group_tests(tests, setup_suite, teardown_suite);
        }
    }

    sahTraceClose();
    return rc;
}
