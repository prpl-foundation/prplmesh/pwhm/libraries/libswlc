/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/types/swl_ttaType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/ttb/swl_ttb.h"

#include "testCommon_swl_ttCollType.h"


static swl_ttColTypeTest_t* s_ct = NULL;
static swl_type_t* s_elType = NULL;
static swl_coll_t* s_col = NULL;
static size_t s_targetSize = 0;


static int setup_suite(void** state _UNUSED) {
    printf("\n\n Test collection : -%s- \n", s_ct->fun->collTypeFun->name);
    s_ct->colSize = s_ct->type->type.size;

    s_col = calloc(1, s_ct->colSize);
    s_targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES: NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    swl_ttb_assertRcOk(s_ct->colFun->initFromArray(&s_ct->type->type, s_col, &collTypeTestValues[0], s_targetSize));

    return 0;
}

static int teardown_suite(void** state _UNUSED) {

    s_ct->colFun->cleanup(&s_ct->type->type, s_col);
    free(s_col);
    return 0;
}

static void test_swl_ttCollGetElementValue(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES: NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    for(size_t i = 0; i < targetSize; i++) {
        for(size_t j = 0; j < gtSwl_collTypeTestTT.nrTypes; j++) {
            swl_type_t* elType = gtSwl_collTypeTestTT.types[j];

            swl_typeData_t* data = s_ct->fun->getElementValue(s_ct->type, col, i, j);
            swl_typeData_t* targetData = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeTestValues[i], j);
            assert_ptr_not_equal(data, targetData);
            swl_ttb_assertTypeEquals(elType, data, targetData);
        }
    }
    s_ct->colFun->cleanup(&s_ct->type->type, col);
}

static void test_swl_ttCollGetElementRef(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES : NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    for(size_t i = 0; i < targetSize; i++) {
        for(size_t j = 0; j < gtSwl_collTypeTestTT.nrTypes; j++) {
            swl_type_t* elType = gtSwl_collTypeTestTT.types[j];

            swl_typeEl_t* data = s_ct->fun->getElementRef(s_ct->type, col, i, j);
            swl_typeEl_t* targetData = swl_tupleType_getReference(&gtSwl_collTypeTestTT, &collTypeTestValues[i], j);
            assert_ptr_not_equal(data, targetData);
            swl_ttb_assertTypeEquals(elType, SWL_TYPE_EL_TO_DATA(elType, data), SWL_TYPE_EL_TO_DATA(elType, targetData));
        }
    }
    s_ct->colFun->cleanup(&s_ct->type->type, col);
}

static void s_testSetElValueEntry(swl_coll_t* col, ssize_t tupleIndex, size_t arraySize, size_t maskIndex, size_t typeIndex) {
    ttb_assert_addPrint("Test index %zi %zu %zu", tupleIndex, maskIndex, typeIndex);
    swl_type_t* elType = gtSwl_collTypeTestTT.types[typeIndex];

    size_t arrayIndex = SWL_ARRAY_INDEX(tupleIndex, arraySize);

    swl_typeData_t* data = s_ct->fun->getElementValue(s_ct->type, col, tupleIndex, typeIndex);
    swl_typeData_t* srcData = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeTestValues[arrayIndex], typeIndex);
    swl_typeData_t* tgtData = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeMaskValues[maskIndex], typeIndex);
    swl_ttb_assertTypeEquals(elType, data, srcData);
    swl_ttb_assertTypeNotEquals(elType, data, tgtData);

    swl_ttb_assertRcOk(s_ct->fun->setElementValue(s_ct->type, col, tupleIndex, typeIndex, tgtData));

    data = s_ct->fun->getElementValue(s_ct->type, col, tupleIndex, typeIndex);
    swl_ttb_assertTypeEquals(elType, data, tgtData);
    swl_ttb_assertTypeNotEquals(elType, data, srcData);
    ttb_assert_removeLastPrint();
}

static void s_testSetElValueTuple(swl_coll_t* col, ssize_t testindex, size_t maxSize, size_t maskIndex) {
    ttb_assert_addPrint("TestGlobal %zi %zu", testindex, maskIndex);
    size_t arrayIndex = SWL_ARRAY_INDEX(testindex, maxSize);

    for(size_t i = 0; i < 2; i++) {
        ttb_assert_addPrint("TestIndex %zu", i);
        swl_typeData_t* curTuple = s_ct->colFun->getValue(&s_ct->type->type, col, testindex + i);

        swl_ttb_assertTypeEquals(&gtSwl_collTypeTestTT.type, curTuple, &collTypeTestValues[arrayIndex + i]);
        swl_ttb_assertTypeNotEquals(&gtSwl_collTypeTestTT.type, curTuple, &collTypeMaskValues[maskIndex + i]);

        for(size_t j = 0; j < gtSwl_collTypeTestTT.nrTypes; j++) {
            s_testSetElValueEntry(col, testindex + i, maxSize, maskIndex + i, j);
        }

        curTuple = s_ct->colFun->getValue(&s_ct->type->type, col, testindex + i);
        swl_ttb_assertTypeNotEquals(&gtSwl_collTypeTestTT.type, curTuple, &collTypeTestValues[arrayIndex + i]);
        swl_ttb_assertTypeEquals(&gtSwl_collTypeTestTT.type, curTuple, &collTypeMaskValues[maskIndex + i]);

        ttb_assert_removeLastPrint();
    }
    ttb_assert_removeLastPrint();
}

static void test_swl_ttCollSetElementValue(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES: NR_COLL_TYPE_TEST_VALUES_DEFAULT);

    assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);

    // Test first 2, last 2
    s_testSetElValueTuple(col, 0, targetSize, 0);
    s_testSetElValueTuple(col, targetSize - 2, targetSize, 2);


    s_ct->colFun->cleanup(&s_ct->type->type, col);

    assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);
    ssize_t negSize = targetSize;
    negSize = -1 * negSize;
    s_testSetElValueTuple(col, negSize, targetSize, 0);
    s_testSetElValueTuple(col, -2, targetSize, 2);

    s_ct->colFun->cleanup(&s_ct->type->type, col);
}

static void test_swl_ttCollColumnToArray(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES: NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    swl_ttb_assertRcOk(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize));


    for(size_t i = 0; i < NR_COLL_TYPE_TEST_TYPES; i++) {
        swl_type_t* elType = gtSwl_collTypeTestTT.types[i];
        size_t arrayBufSize = elType->size * targetSize;
        swl_bit8_t* arrayBuf[arrayBufSize];
        memset(arrayBuf, 0, arrayBufSize);

        swl_ttb_assertRcOk(s_ct->fun->columnToArray(s_ct->type, arrayBuf, targetSize, col, i));

        void* extraDataArray = collTypeTestArrays[i];
        swl_ttb_assertTypeArrayEquals(elType, arrayBuf, targetSize, extraDataArray, targetSize);

    }


    s_ct->colFun->cleanup(&s_ct->type->type, col);
}


static void test_swl_ttCollColumnToArrayOffset(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES: NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    swl_ttb_assertRcOk(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize));


    for(size_t i = 0; i < NR_COLL_TYPE_TEST_TYPES; i++) {
        swl_type_t* elType = gtSwl_collTypeTestTT.types[i];
        size_t arrayBufSize = elType->size * targetSize;
        swl_bit8_t* arrayBuf[arrayBufSize];
        memset(arrayBuf, 0, arrayBufSize);

        swl_bit8_t* checkArrayBuf[arrayBufSize];
        memset(checkArrayBuf, 0, arrayBufSize);
        swl_type_arrayCopy(elType, checkArrayBuf, collTypeTestArrays[i], targetSize);

        for(size_t j = 0; j < targetSize; j++) {
            swl_ttb_assertRcOk(s_ct->fun->columnToArrayOffset(s_ct->type, arrayBuf, targetSize, col, i, j));
            swl_ttb_assertTypeArrayEquals(elType, arrayBuf, targetSize, checkArrayBuf, targetSize);
            swl_type_arrayCircShift(elType, checkArrayBuf, targetSize, 1);
        }

        swl_type_arrayCleanup(elType, checkArrayBuf, targetSize);
    }

    s_ct->colFun->cleanup(&s_ct->type->type, col);
}

static void test_swl_ttCollGetMatch(void** state _UNUSED) {
    for(size_t i = 0; i < s_targetSize; i++) {
        ttb_assert_addPrint("Target index %zu", i);
        for(size_t j = 0; j < NR_COLL_TYPE_TEST_TYPES; j++) {
            ttb_assert_addPrint("Source Type %zu", j);
            swl_typeData_t* srcData = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeTestValues[i], j);

            for(size_t k = 0; k < s_targetSize + 1; k++) {
                ttb_assert_addPrint("Offset index %zu", k);

                ssize_t index = s_ct->fun->findMatchingTupleIndex(s_ct->type, s_col, k, j, srcData);
                swl_tuple_t* tuple = s_ct->fun->getMatchingTuple(s_ct->type, s_col, k, j, srcData);

                if(k <= i) {
                    ttb_assert_non_null(srcData);
                    ttb_assert_non_null(tuple);
                    swl_ttb_assertTypeEquals(&gtSwl_collTypeTestTT.type, &collTypeTestValues[i], tuple);
                    ttb_assert_int_eq(index, i);
                } else {
                    ttb_assert_null(tuple);
                    ttb_assert_int_eq(index, SWL_RC_INVALID_PARAM);
                }

                for(size_t l = 0; l < NR_COLL_TYPE_TEST_TYPES; l++) {
                    ttb_assert_addPrint("Target type %zu", l);
                    swl_type_t* tgtType = gtSwl_collTypeTestTT.types[l];

                    swl_typeData_t* srcEl = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeTestValues[i], l);
                    swl_typeData_t* el = s_ct->fun->getMatchingElement(s_ct->type, s_col, k, l, j, srcData);

                    if(k <= i) {
                        swl_ttb_assertTypeEquals(tgtType, srcEl, el);
                    } else {
                        ttb_assert_null(el);
                    }
                    ttb_assert_removeLastPrint();
                }
                ttb_assert_removeLastPrint();
            }
            ttb_assert_removeLastPrint();
        }
        ttb_assert_removeLastPrint();
    }
}

swl_collTypeTestTT_t collTypeTestMaskValues[NR_COLL_TYPE_TEST_VALUES_DEFAULT] = {
    {1, 100, "atest1"},
    {2, 100, "atest1"},
    {1, 200, "atest1"},
    {1, 100, "atest2"},
};

static void s_testMask(swl_ttColl_t* col, size_t inputTupleIndex, ssize_t offset, swl_mask_m mask, ssize_t output) {
    ttb_assert_addPrint("%zu %zi %x %zi", inputTupleIndex, offset, mask, output);
    swl_collTypeTestTT_t* inputTuple = &collTypeTestMaskValues[inputTupleIndex];
    swl_collTypeTestTT_t testTuple;
    memset(&testTuple, 0, sizeof(swl_collTypeTestTT_t));
    swl_tupleType_copyByMask(&testTuple, &gtSwl_collTypeTestTT, inputTuple, mask);


    ssize_t index = s_ct->fun->findMatchingTupleIndexByMask(s_ct->type, col, offset, &testTuple, mask);
    ttb_assert_int_eq(output, index);
    swl_tuple_t* tuple = s_ct->fun->getMatchingTupleByMask(s_ct->type, col, offset, &testTuple, mask);

    if(output < 0) {
        ttb_assert_null(tuple);
    } else {
        swl_ttb_assertTypeEquals(&gtSwl_collTypeTestTT.type, &collTypeTestMaskValues[output], tuple);
    }

    for(size_t i = 0; i < NR_COLL_TYPE_TEST_TYPES; i++) {
        ttb_assert_addPrint("Target type %zu", i);
        swl_type_t* tgtType = gtSwl_collTypeTestTT.types[i];

        swl_typeData_t* el = s_ct->fun->getMatchingElementByMask(s_ct->type, col, offset, i, &testTuple, mask);

        if(output < 0) {
            ttb_assert_null(el);
        } else {
            swl_typeData_t* srcEl = swl_tupleType_getValue(&gtSwl_collTypeTestTT, &collTypeTestMaskValues[output], i);
            swl_ttb_assertTypeEquals(tgtType, srcEl, el);
        }
        ttb_assert_removeLastPrint();
    }


    swl_tupleType_cleanup(&gtSwl_collTypeTestTT, &testTuple);

    ttb_assert_removeLastPrint();
}


static void test_swl_ttCollGetMatchByMask(void** state _UNUSED) {
    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = NR_COLL_TYPE_TEST_VALUES_DEFAULT;
    swl_ttb_assertRcOk(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestMaskValues[0], targetSize));


    s_testMask(col, 0, 0, 1, 0);
    s_testMask(col, 0, 0, 3, 0);
    s_testMask(col, 0, 0, 7, 0);

    s_testMask(col, 0, 1, 1, 2);
    s_testMask(col, 0, 1, 2, 1);
    s_testMask(col, 0, 1, 3, 3);
    s_testMask(col, 0, 1, 4, 1);
    s_testMask(col, 0, 1, 7, SWL_RC_INVALID_PARAM);


    s_ct->colFun->cleanup(&s_ct->type->type, col);
}


static void s_doSinglePrintCheck(swl_coll_t* col, const char* folder, const char* fileName, bool listCheck) {
    char fileBuffer[128];
    snprintf(fileBuffer, sizeof(fileBuffer), "%s/%s", folder, fileName);

    char tmpNameBuffer[L_tmpnam] = {0}; \
    tmpnam(tmpNameBuffer); \

    const char* tgtName = !gTtb_assert_printMode ? tmpNameBuffer : fileBuffer;

    swl_print_args_t tmpArgs = g_swl_print_jsonCompact;
    FILE* file = fopen(tgtName, "w");
    ttb_assert_non_null(file);

    if(listCheck) {
        ttb_assert(s_ct->fun->fPrintLists(s_ct->type, file, col, 0, &tmpArgs));
    } else {
        ttb_assert(s_ct->fun->fPrintMaps(s_ct->type, file, col, 0, &tmpArgs, swl_colTypeTestNames, "History"));
    }

    fclose(file);

    if(!gTtb_assert_printMode) {
        swl_ttb_assertFileEquals(tmpNameBuffer, fileBuffer);
        remove(tmpNameBuffer);
    }
}




static void s_doPrintCheck(swl_coll_t* col, const char* fileName) {
    if(s_ct->listPrintFolder != NULL) {
        s_doSinglePrintCheck(col, s_ct->listPrintFolder, fileName, true);
    }
    if(s_ct->mapPrintFolder != NULL) {
        s_doSinglePrintCheck(col, s_ct->mapPrintFolder, fileName, false);
    }
}

static void test_swl_ttCollfPrint(void** state _UNUSED) {
    if(s_ct->listPrintFolder == NULL) {
        return;
    }

    //Check long print
    char fileBuffer[128];

    swl_bit8_t buf[s_ct->colSize];
    memset(buf, 0, s_ct->colSize);
    swl_coll_t* col = &buf[0];
    size_t targetSize = (s_ct->maxSize == 0 ? NR_COLL_TYPE_TEST_VALUES : NR_COLL_TYPE_TEST_VALUES_DEFAULT);
    assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestValues[0], targetSize), SWL_RC_OK);


    snprintf(fileBuffer, sizeof(fileBuffer), "%sPrint.txt", s_ct->maxSize == 0 ? "long":"short");

    s_doPrintCheck(col, fileBuffer);

    s_ct->colFun->cleanup(&s_ct->type->type, col);

    for(size_t i = 0; i < NR_COLL_TYPE_TEST_DATA_VALUES; i++) {

        if(collTypeTestData[i].size == NR_COLL_TYPE_TEST_VALUES_DEFAULT) {
            memset(buf, 0, s_ct->colSize);
            assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);
            snprintf(fileBuffer, sizeof(fileBuffer), "test_%zu.txt", i);

            s_doPrintCheck(col, fileBuffer);

            s_ct->colFun->cleanup(&s_ct->type->type, col);
        } else {
            // Test reduced init

            memset(buf, 0, s_ct->colSize);
            assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestData[i].data[0], collTypeTestData[i].size), SWL_RC_OK);

            if((s_ct->maxSize == 0) || (s_ct->implicitSize && !s_ct->allowEmpty)) {
                snprintf(fileBuffer, sizeof(fileBuffer), "testNoEmpty_%zu.txt", i);
            } else {
                snprintf(fileBuffer, sizeof(fileBuffer), "testWithEmpty_%zu.txt", i);
            }

            s_doPrintCheck(col, fileBuffer);

            s_ct->colFun->cleanup(&s_ct->type->type, col);


            // Test empty init
            if((s_ct->maxSize == 0) || s_ct->allowEmpty) {
                memset(buf, 0, s_ct->colSize);
                assert_int_equal(s_ct->colFun->initFromArray(&s_ct->type->type, col, &collTypeTestData[i].data[0], NR_COLL_TYPE_TEST_VALUES_DEFAULT), SWL_RC_OK);

                snprintf(fileBuffer, sizeof(fileBuffer), "testWithEmpty_%zu.txt", i);

                s_doPrintCheck(col, fileBuffer);

                s_ct->colFun->cleanup(&s_ct->type->type, col);
            }
        }
    }

}


int testCommon_swl_ttCollType_runTest(swl_ttColTypeTest_t* tut) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_ttCollGetElementValue),
        cmocka_unit_test(test_swl_ttCollGetElementRef),
        cmocka_unit_test(test_swl_ttCollSetElementValue),
        cmocka_unit_test(test_swl_ttCollColumnToArray),
        cmocka_unit_test(test_swl_ttCollColumnToArrayOffset),
        cmocka_unit_test(test_swl_ttCollGetMatch),
        cmocka_unit_test(test_swl_ttCollGetMatchByMask),
        cmocka_unit_test(test_swl_ttCollfPrint),
    };

    s_elType = (swl_type_t*) &gtSwl_collTypeTestTT;
    s_ct = tut;
    s_ct->colFun = tut->fun->collTypeFun;

    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    ttb_assert_clearPrint();
    sahTraceClose();
    return rc;
}
