/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/swl_map.h"
#include "swl/types/swl_mapType.h"
#include "testCommon_swl_mapSType_singleValueType.h"

#define NR_VALUES 12



#define MY_TEST_MAP_KEY_VAR(X, Y) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(tMyTestKey, swl_myTestKey_t, MY_TEST_MAP_KEY_VAR, )

#define MY_TEST_MAP_VALUE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(tMyTestValue, swl_myTestValue_t, MY_TEST_MAP_VALUE_VAR, )

SWL_MAP_TYPE(tMyTestMap, &tMyTestKey.type, &tMyTestValue.type);


swl_arrayType_t* testArrType;
const char* filePrefix;
/**
 * Defining a tuple type using a table
 */
swl_myTestKey_t myTestKeyValues[NR_VALUES] = {
    {1001, "test1k"},
    {-2001, "test2k"},
    {2898701, "foobark"},
    {-2898691, "barfook"},
    {1002, "test1kl"},
    {-2002, "test2kl"},
    {2898702, "foobarkl"},
    {-2898692, "barfookl"},
    {1003, "test1km"},
    {-2003, "test2km"},
    {2898703, "foobarkm"},
    {-2898693, "barfookm"},
};

swl_myTestValue_t myTestValues[NR_VALUES] = {
    {11, 1001, "test1a"},
    {21, -2001, "test2a"},
    {-81, 289871, "foobara"},
    {81, -289861, "barfooa"},
    {12, 102, "test1b"},
    {22, -202, "test2b"},
    {-82, 289872, "foobarb"},
    {82, -289862, "barfoob"},
    {12, 103, "test1c"},
    {22, -203, "test2c"},
    {-82, 289873, "foobarc"},
    {82, -289863, "barfooc"},
};


swl_myTestValue_t myTestOtherValues[] = {
    {11, 1001, "test1b"},
};


swl_mapSType_singleValueTest_t myTestData = {
    .mapType = &tMyTestMap.type,
    .keyType = &tMyTestKey.type,
    .valueType = &tMyTestValue.type,
    .keyDataArray = myTestKeyValues,
    .valueDataArray = myTestValues,
    .nrValues = NR_VALUES,
    .nonUsedValueArray = myTestOtherValues,
    .nrUnusedValues = SWL_ARRAY_SIZE(myTestOtherValues),

};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    return testCommon_swl_mapSType_singleValueType("swl_mapType", &myTestData);
}
