/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/swl_map.h"
#include "swl/types/swl_mapType.h"

#define NR_VALUES 12



#define MY_TEST_MAP_KEY_VAR(X, Y) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(tMyTestKey, swl_myTestKey_t, MY_TEST_MAP_KEY_VAR, )

#define MY_TEST_MAP_VALUE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)
SWL_TT(tMyTestValue, swl_myTestValue_t, MY_TEST_MAP_VALUE_VAR, )

SWL_MAP_TYPE(tMyTestMap, &tMyTestKey.type, &tMyTestValue.type);


swl_arrayType_t* testArrType;
const char* filePrefix;
/**
 * Defining a tuple type using a table
 */
swl_myTestKey_t myTestKeyValues[NR_VALUES] = {
    {1001, "test1k"},
    {-2001, "test2k"},
    {2898701, "foobark"},
    {-2898691, "barfook"},
    {1002, "test1kl"},
    {-2002, "test2kl"},
    {2898702, "foobarkl"},
    {-2898692, "barfookl"},
    {1003, "test1km"},
    {-2003, "test2km"},
    {2898703, "foobarkm"},
    {-2898693, "barfookm"},
};

swl_myTestValue_t myTestValues[NR_VALUES] = {
    {11, 1001, "test1a"},
    {21, -2001, "test2a"},
    {-81, 289871, "foobara"},
    {81, -289861, "barfooa"},
    {12, 102, "test1b"},
    {22, -202, "test2b"},
    {-82, 289872, "foobarb"},
    {82, -289862, "barfoob"},
    {12, 103, "test1c"},
    {22, -203, "test2c"},
    {-82, 289873, "foobarc"},
    {82, -289863, "barfooc"},
};

#define NR_TEST_MAPS 4

swl_map_t testMaps[NR_TEST_MAPS];

static void test_swl_mapToFromChar(void** state _UNUSED) {
    for(size_t i = 0; i < NR_TEST_MAPS; i++) {
        char buffer[512] = {0};
        ssize_t tmpSize = swl_type_toChar(&tMyTestMap.type, buffer, sizeof(buffer), &testMaps[i]);

        assert_true(tmpSize > 0);
        printf("%s\n", buffer);

        swl_map_t tmpMap;
        memset(&tmpMap, 0, sizeof(tmpMap));
        assert_true(swl_type_fromChar(&tMyTestMap.type, &tmpMap, buffer));

        char buffer2[512] = {0};
        swl_type_toChar(&tMyTestMap.type, buffer2, sizeof(buffer2), &tmpMap);
        printf("%s\n", buffer2);

        assert_true(swl_type_equals(&tMyTestMap.type, &tmpMap, &testMaps[i]));

        swl_type_cleanup(&tMyTestMap.type, &tmpMap);
    }
}


static void test_swl_mapCopyEquals(void** state _UNUSED) {
    for(size_t i = 0; i < NR_TEST_MAPS; i++) {
        swl_typeData_t* data = swl_type_copy(&tMyTestMap.type, &testMaps[i]);
        assert_ptr_not_equal(data, &testMaps[i]);

        for(size_t j = 0; j < NR_TEST_MAPS; j++) {
            if(i == j) {
                assert_true(swl_type_equals(&tMyTestMap.type, &testMaps[j], data));
            } else {
                assert_false(swl_type_equals(&tMyTestMap.type, &testMaps[j], data));
            }
        }

        swl_type_cleanupPtr(&tMyTestMap.type, &data);
    }

}
static void test_swl_mapToFile(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testMaps); i++) {
        char fileNameBuf[128] = {0};
        snprintf(fileNameBuf, sizeof(fileNameBuf), "data_%zu.txt", i);

        //char* tmpFile = "tmp.txt";
        FILE* fp = fopen(fileNameBuf, "w");
        swl_type_toFile(&tMyTestMap.type, fp, &testMaps[i], &g_swl_print_jsonCompact);
        fclose(fp);
        //assert_true(swl_fileUtils_contentMatches(fileNameBuf, tmpFile));
        //unlink(tmpFile);
    }
}

static int setup_suite(void** state _UNUSED) {
    for(size_t i = 0; i < NR_TEST_MAPS; i++) {
        memset(&testMaps[i], 0, sizeof(swl_map_t));
        swl_map_init(&testMaps[i], &tMyTestKey.type, &tMyTestValue.type);

        if((i % 2) == 1) {
            for(size_t j = 0; j < NR_VALUES / 2; j++) {
                swl_map_add(&testMaps[i], &myTestKeyValues[j], &myTestValues[j]);
            }
        }
        if((i / 2) == 1) {
            for(size_t j = 0; j < NR_VALUES / 2; j++) {
                swl_map_add(&testMaps[i], &myTestKeyValues[j + NR_VALUES / 2], &myTestValues[j + NR_VALUES / 2]);
            }
        }
    }


    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    for(size_t i = 0; i < NR_TEST_MAPS; i++) {
        swl_map_cleanup(&testMaps[i]);
    }
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mapToFromChar),
        cmocka_unit_test(test_swl_mapCopyEquals),
        cmocka_unit_test(test_swl_mapToFile),
    };

    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    sahTraceClose();
    return rc;
}
