/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include "swl/swl_string.h"

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_base64.h"

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}


struct {
    const char* input;
    const char* base64Representation;
    size_t encodingOutputSize;
    size_t decodingOutputSize;
} myTestTable[] = {
    {"1", "MQ==", 4, 1},
    {"Hello", "SGVsbG8=", 8, 5},
    {"123test", "MTIzdGVzdA==", 12, 7},
    {"one", "b25l", 4, 3},
};


static void test_encodingIntoBase64(void** state _UNUSED) {
    char buffer[25] = {0};
    ssize_t result = 0;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(myTestTable); i++) {
        result = swl_base64_encode(buffer, sizeof(buffer), (const swl_bit8_t*) myTestTable[i].input, swl_str_len(myTestTable[i].input));
        assert_string_equal(buffer, myTestTable[i].base64Representation);
        assert_int_equal(result, myTestTable[i].encodingOutputSize);
    }

    //binary data
    swl_bit8_t myBinary[] = {0b01101111, 0b01101110, 0b01100101};
    result = swl_base64_encode(buffer, sizeof(buffer), myBinary, sizeof(myBinary));
    assert_string_equal(buffer, "b25l");
    assert_int_equal(result, 0x4);

    //small buffer
    char bufferSmall[3] = "aab";
    const swl_bit8_t input[] = "TestSmallBuffer";
    result = swl_base64_encode(bufferSmall, sizeof(bufferSmall), input, sizeof(input));
    assert_int_equal(result, 0x19);
    assert_int_equal(bufferSmall[0], 'a');
    assert_int_equal(bufferSmall[1], 'a');
    assert_int_equal(bufferSmall[2], 'b');

    //null ptr
    char* nonValidParam = NULL;
    result = swl_base64_encode(nonValidParam, sizeof(nonValidParam), input, sizeof(input));
    assert_int_equal(result, SWL_RC_INVALID_PARAM);
    const swl_bit8_t* nullInput = NULL;
    result = swl_base64_encode(bufferSmall, sizeof(bufferSmall), nullInput, sizeof(nullInput));
    assert_int_equal(result, SWL_RC_INVALID_PARAM);
}

static void test_decodingBase64(void** state _UNUSED) {
    ssize_t result = 0;
    swl_bit8_t buffer[15] = {0};
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(myTestTable); i++) {
        result = swl_base64_decode(buffer, sizeof(buffer), myTestTable[i].base64Representation);
        assert_string_equal(buffer, myTestTable[i].input);
        assert_int_equal(result, myTestTable[i].decodingOutputSize);
    }

    //small buffer
    swl_bit8_t bufferSmall[2] = {2, 8};
    const char input[] = "SGVsbG8=";
    result = swl_base64_decode(bufferSmall, sizeof(bufferSmall), input);
    assert_int_equal(result, 0x5);
    assert_int_equal(bufferSmall[0], 2);
    assert_int_equal(bufferSmall[1], 8);

    //null ptr
    swl_bit8_t* nonValidParam = NULL;
    result = swl_base64_decode(nonValidParam, sizeof(nonValidParam), input);
    assert_int_equal(result, SWL_RC_INVALID_PARAM);
    const char* nullInput = NULL;
    result = swl_base64_decode(bufferSmall, sizeof(bufferSmall), nullInput);
    assert_int_equal(result, SWL_RC_INVALID_PARAM);

    //Invalid base64
    const char inputInvalidBase64[] = "SG.cVsbG8,=bbcc";
    result = swl_base64_decode(buffer, sizeof(buffer), inputInvalidBase64);
    assert_int_equal(result, SWL_RC_ERROR);

    //Invalid padding
    const char invalidPadding[] = "SGVsbG8gV29ybGQhCg====";
    result = swl_base64_decode(buffer, sizeof(buffer), invalidPadding);
    assert_int_equal(result, SWL_RC_ERROR);

}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_encodingIntoBase64),
        cmocka_unit_test(test_decodingBase64),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
