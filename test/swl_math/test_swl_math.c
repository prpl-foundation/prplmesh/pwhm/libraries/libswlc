/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <math.h>
#include <time.h>
#include <limits.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_math.h"
#include "test-toolbox/ttb.h"

/**
 * Test algorithm to ensure the "length of the number" is mostly random, as opposed to number itself
 * Otherwise it would happen only 1 in 2 ^ 55 cases that a number smaller than 1024 is selected,
 * With this algorithm, it should happen 1 in 55.
 */
static uint64_t s_getLongRand() {
    uint64_t randNr = (uint64_t) rand() + ((uint64_t) rand() << 32);
    uint64_t nrDigit = ((uint64_t) rand() % 55) + 10;
    if(nrDigit < 64) {
        randNr = randNr % ((uint64_t) 1 << nrDigit);
    }
    randNr |= (1 << (nrDigit - 1));
    return randNr;
}


static void test_swl_math_isPowerOf(void** state _UNUSED) {
    assert_true(swl_math_isPowerOf(0, 0));
    assert_true(swl_math_isPowerOf(1, 1));
    assert_true(swl_math_isPowerOf(1, -1));
    assert_true(swl_math_isPowerOf(-1, -1));

    assert_false(swl_math_isPowerOf(1, 0));
    assert_false(swl_math_isPowerOf(0, 1));
    assert_false(swl_math_isPowerOf(-1, 1));

    for(int i = 2; i < 2000; i = i * 2 + 1) {
        assert_true(swl_math_isPowerOf(i, i));
        assert_false(swl_math_isPowerOf(i, 0));
        assert_false(swl_math_isPowerOf(i, 1));
        if(i > 2) {
            assert_false(swl_math_isPowerOf(i - 1, i));
        }
        assert_false(swl_math_isPowerOf(i + 1, i));
        uint64_t testVal = i;
        uint32_t nrBit = swl_bit64_getHighest(i) + 1;
        uint32_t tmpNrBit = nrBit;

        while(tmpNrBit < 64) {
            testVal = testVal * i;
            tmpNrBit = swl_bit64_getHighest(testVal) + 1 + nrBit;
            assert_true(swl_math_isPowerOf(testVal, i));
            assert_false(swl_math_isPowerOf(testVal - 1, i));
            assert_false(swl_math_isPowerOf(testVal + 1, i));
        }
    }
}

static void test_swl_math_isPowerOf2(void** state _UNUSED) {
    assert_false(swl_math_isPowerOf2(0));

    for(int i = 0; i < 64; i++) {
        uint64_t tmpVal = SWL_BIT_SHIFT(i);
        assert_true(swl_math_isPowerOf2(tmpVal));
        if(i > 0) {
            assert_false(swl_math_isPowerOf2(tmpVal + 1));
        }
        if(i > 1) {
            assert_false(swl_math_isPowerOf2(tmpVal - 1));
        }
    }
}

static void test_swl_math_invSumInv(void** state _UNUSED) {
    assert_int_equal(swl_math_invSumInv(2, 2), 1);
    for(int i = 0; i < 63; i++) {
        uint64_t val1 = SWL_BIT_SHIFT(i);
        for(int j = 0; j < 63; j++) {
            uint64_t val2 = SWL_BIT_SHIFT(j);

            uint64_t tmpVal = swl_math_invSumInv(val1, val2);
            int diff = SWL_MATH_ABS(i - j);
            uint64_t max = SWL_MAX(val1, val2);
            uint64_t estVal = max / (1 + SWL_BIT_SHIFT(diff));

            uint64_t sumBit = i + j + 2;
            uint64_t diffSize = 0;
            if(sumBit > 64) {
                diffSize = SWL_BIT_SHIFT(sumBit - 64);
            }

            uint64_t diffRes = SWL_MAX(tmpVal, estVal) - SWL_MIN(tmpVal, estVal);
            assert_true(diffRes <= diffSize);
        }
    }

    uint64_t baseVal = 5; // test with base 5 to have different "base" of calculation
    for(int i = 0; i < 20; i++) {
        uint64_t val1 = pow(baseVal, i);
        for(int j = 0; j < 20; j++) {
            uint64_t val2 = pow(baseVal, j);

            uint64_t tmpVal = swl_math_invSumInv(val1, val2);
            int diff = SWL_MATH_ABS(i - j);
            uint64_t max = SWL_MAX(val1, val2);
            uint64_t estVal = max / (1 + pow(baseVal, diff));
            uint64_t sumBit = swl_bit64_getHighest(val1) + swl_bit64_getHighest(val2) + 2;
            uint64_t diffSize = 1;
            if(sumBit > 64) {
                diffSize = SWL_BIT_SHIFT(sumBit - 64);
            }
            uint64_t diffRes = SWL_MAX(tmpVal, estVal) - SWL_MIN(tmpVal, estVal);
            assert_true(diffRes <= diffSize);
        }
    }

}

static void test_swl_math_interpolate(void** state _UNUSED) {
    uint32_t testVal = 3000;
    for(int i = -2000; i < 4000; i += 2) {
        assert_int_equal(testVal, swl_math_interpolate(4000, 5000, i, 2000));
        testVal += 1;
    }

    testVal = 6000;
    for(int i = -2000; i < 4000; i += 2) {
        assert_int_equal(testVal, swl_math_interpolate(5000, 4000, i, 2000));
        testVal -= 1;
    }
}

void s_testConvertStep(int factor, int nrExpectedSteps) {
    int acc = swl_math_doExpAvgStep(0, 0, factor, true);

    for(int i = 0; i < nrExpectedSteps; i++) {
        assert_int_equal(0, swl_math_getExpAvgResult(acc));
        acc = swl_math_doExpAvgStep(acc, 1, factor, false);
    }
    assert_int_equal(1, swl_math_getExpAvgResult(acc));

    acc = swl_math_doExpAvgStep(0, 0, factor, true);

    for(int i = 0; i < nrExpectedSteps; i++) {
        assert_int_equal(0, swl_math_getExpAvgResult(acc));
        acc = swl_math_doExpAvgStep(acc, -1, factor, false);
    }
    assert_int_equal(-1, swl_math_getExpAvgResult(acc));

}

static void test_swl_math_doExpAvg(void** state _UNUSED) {
    int32_t acc = 0;

    for(int i = -1000; i <= 1000; i++) {
        acc = swl_math_doExpAvgStep(acc, i, 500, true);
        assert_int_equal(i, swl_math_getExpAvgResult(acc));
    }

    s_testConvertStep(1000, 1);
    s_testConvertStep(500, 1);
    s_testConvertStep(490, 2);
    s_testConvertStep(300, 2);
    s_testConvertStep(290, 3);

#define NR_EXP_AVG_VAL 10
    int32_t values[NR_EXP_AVG_VAL] = {100, 300, 200, 100, 300, 200, 100, 300, 200, 100};

    int32_t testVals250[NR_EXP_AVG_VAL] = {100, 150, 163, 147, 185, 189, 167, 200, 200, 175};
    int32_t testVals500[NR_EXP_AVG_VAL] = {100, 200, 200, 150, 225, 213, 156, 228, 214, 157};
    int32_t testVals750[NR_EXP_AVG_VAL] = {100, 250, 213, 128, 257, 214, 129, 257, 214, 129};

    int accNeg = 0;
    for(int i = 0; i < NR_EXP_AVG_VAL; i++) {
        acc = swl_math_doExpAvgStep(acc, values[i], 250, i == 0);
        assert_int_equal(testVals250[i], swl_math_getExpAvgResult(acc));

        accNeg = swl_math_doExpAvgStep(accNeg, -1 * values[i], 250, i == 0);
        assert_int_equal(-1 * testVals250[i], swl_math_getExpAvgResult(accNeg));
    }

    for(int i = 0; i < NR_EXP_AVG_VAL; i++) {
        acc = swl_math_doExpAvgStep(acc, values[i], 500, i == 0);
        assert_int_equal(testVals500[i], swl_math_getExpAvgResult(acc));

        accNeg = swl_math_doExpAvgStep(accNeg, -1 * values[i], 500, i == 0);
        assert_int_equal(-1 * testVals500[i], swl_math_getExpAvgResult(accNeg));
    }

    for(int i = 0; i < NR_EXP_AVG_VAL; i++) {
        acc = swl_math_doExpAvgStep(acc, values[i], 750, i == 0);
        assert_int_equal(testVals750[i], swl_math_getExpAvgResult(acc));

        accNeg = swl_math_doExpAvgStep(accNeg, -1 * values[i], 750, i == 0);
        assert_int_equal(-1 * testVals750[i], swl_math_getExpAvgResult(accNeg));
    }
}

static void test_swl_math_fmod(void** state _UNUSED) {
    assert_int_equal(0, swl_math_fmod(0, 1));
    assert_int_equal(0, swl_math_fmod(10000, 1));
    assert_int_equal(0, swl_math_fmod(-10000, 1));
    assert_int_equal(0, swl_math_fmod(10000, -1));
    assert_int_equal(0, swl_math_fmod(-10000, -1));

    assert_int_equal(5, swl_math_fmod(5, 10));
    assert_int_equal(5, swl_math_fmod(-5, 10));
    assert_int_equal(0, swl_math_fmod(10, 10));
    assert_int_equal(0, swl_math_fmod(-10, 10));
    assert_int_equal(5, swl_math_fmod(15, 10));
    assert_int_equal(5, swl_math_fmod(-15, 10));


    assert_int_equal(5, swl_math_fmod(5, -10));
    assert_int_equal(5, swl_math_fmod(-5, -10));
    assert_int_equal(0, swl_math_fmod(10, -10));
    assert_int_equal(0, swl_math_fmod(-10, -10));
    assert_int_equal(5, swl_math_fmod(15, -10));
    assert_int_equal(5, swl_math_fmod(-15, -10));

}


static void test_swl_math_fdiv(void** state _UNUSED) {
    assert_int_equal(0, swl_math_fdiv(0, 1));
    assert_int_equal(10000, swl_math_fdiv(10000, 1));
    assert_int_equal(-10000, swl_math_fdiv(-10000, 1));
    assert_int_equal(-10000, swl_math_fdiv(10000, -1));
    assert_int_equal(10000, swl_math_fdiv(-10000, -1));

    assert_int_equal(0, swl_math_fdiv(5, 10));
    assert_int_equal(-1, swl_math_fdiv(-5, 10));
    assert_int_equal(1, swl_math_fdiv(10, 10));
    assert_int_equal(-1, swl_math_fdiv(-10, 10));


    assert_int_equal(1, swl_math_fdiv(15, 10));
    assert_int_equal(-2, swl_math_fdiv(-15, 10));


    assert_int_equal(0, swl_math_fdiv(5, -10));
    assert_int_equal(1, swl_math_fdiv(-5, -10));
    assert_int_equal(-1, swl_math_fdiv(10, -10));
    assert_int_equal(1, swl_math_fdiv(-10, -10));
    assert_int_equal(-1, swl_math_fdiv(15, -10));
    assert_int_equal(2, swl_math_fdiv(-15, -10));

    // random rule test
    for(int i = 0; i < 1000; i++) {
        int64_t testDiv = rand();
        // make quot smaller by default, to ensure "most" of time, there is a non zero diff
        int64_t testQuot = rand() / 50;
        if(testQuot == 0) {
            continue;
        }

        int64_t div = swl_math_fdiv(testDiv, testQuot);
        int64_t rem = swl_math_fmod(testDiv, testQuot);
        assert_true(testDiv = div * testQuot + rem);
        assert_true(rem >= 0);
    }
}

static void test_swl_math_udiv(void** state _UNUSED) {
    assert_int_equal(0, swl_math_udiv(0, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(0, swl_math_udiv(1, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(0, swl_math_udiv(4, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(0, swl_math_udiv(5, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(0, swl_math_udiv(9, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(1, swl_math_udiv(10, 10, SWL_MATH_ROUND_OPT_DOWN));
    assert_int_equal(0, swl_math_udiv(0, 10, SWL_MATH_ROUND_OPT_HALF));
    assert_int_equal(0, swl_math_udiv(1, 10, SWL_MATH_ROUND_OPT_HALF));
    assert_int_equal(0, swl_math_udiv(4, 10, SWL_MATH_ROUND_OPT_HALF));
    assert_int_equal(1, swl_math_udiv(5, 10, SWL_MATH_ROUND_OPT_HALF));
    assert_int_equal(1, swl_math_udiv(9, 10, SWL_MATH_ROUND_OPT_HALF));
    assert_int_equal(1, swl_math_udiv(10, 10, SWL_MATH_ROUND_OPT_HALF));


    assert_int_equal(0, swl_math_udiv(0, 10, SWL_MATH_ROUND_OPT_UP));
    assert_int_equal(1, swl_math_udiv(1, 10, SWL_MATH_ROUND_OPT_UP));
    assert_int_equal(1, swl_math_udiv(4, 10, SWL_MATH_ROUND_OPT_UP));
    assert_int_equal(1, swl_math_udiv(5, 10, SWL_MATH_ROUND_OPT_UP));
    assert_int_equal(1, swl_math_udiv(9, 10, SWL_MATH_ROUND_OPT_UP));
    assert_int_equal(1, swl_math_udiv(10, 10, SWL_MATH_ROUND_OPT_UP));

}

static void test_swl_math_multComplement(void** state _UNUSED) {

    assert_int_equal(0, swl_math_multComplement(100, 0, 0));

    assert_int_equal(100, swl_math_multComplement(100, 100, 100));

    assert_int_equal(10, swl_math_multComplement(100, 10, 0));
    assert_int_equal(10, swl_math_multComplement(100, 0, 10));


    assert_int_equal(19, swl_math_multComplement(100, 10, 10));


    assert_int_equal(2, swl_math_multComplement(1000, 1, 1));
    assert_int_equal(190, swl_math_multComplement(1000, 100, 100));
    assert_int_equal(109, swl_math_multComplement(1000, 100, 10));
    assert_int_equal(109, swl_math_multComplement(1000, 10, 100));
    assert_int_equal(505, swl_math_multComplement(1000, 500, 10));
    assert_int_equal(505, swl_math_multComplement(1000, 10, 500));


    assert_int_equal(999, swl_math_multComplement(1000, 900, 990));
    assert_int_equal(999, swl_math_multComplement(1000, 990, 900));
    assert_int_equal(1000, swl_math_multComplement(1000, 999, 999));
}

#define NR_LOG_TEST_VALUES 7
static uint64_t logTestValues[NR_LOG_TEST_VALUES] = {1, 2, 10, 20, ULLONG_MAX, ULLONG_MAX - 1, ULLONG_MAX / 2};

#define NR_LOG_TEST_RANDOM 1000

static void test_swl_math_log10(void** state _UNUSED) {

    for(size_t i = 0; i < NR_LOG_TEST_VALUES; i++) {
        assert_int_equal((uint64_t) log10(logTestValues[i]), swl_math_log10(logTestValues[i]));
    }

    for(size_t i = 0; i < NR_LOG_TEST_RANDOM; i++) {
        uint64_t rng = s_getLongRand();
        if(rng != 0) {
            assert_int_equal((uint64_t) log10(rng), swl_math_log10(rng));
        }
    }
}

static void test_swl_math_log10mult(void** state _UNUSED) {

    for(size_t j = 1; j < 12; j++) {

        for(size_t i = 0; i < NR_LOG_TEST_VALUES; i++) {
            assert_int_equal((uint64_t) j * log10(logTestValues[i]), swl_math_log10Mult(logTestValues[i], j));
        }

        for(size_t i = 0; i < NR_LOG_TEST_RANDOM; i++) {
            uint64_t rng = s_getLongRand();
            if(rng != 0) {
                assert_int_equal((uint64_t) j * log10(rng), swl_math_log10Mult(rng, j));
            }
        }
    }
}

//uint32_t swl_math_getInsertIndexU32(uint32_t* array, uint32_t size, uint32_t val);

static void s_testGetInsertIndexU32(uint32_t* array, uint32_t size, uint32_t val, uint32_t expectedOut) {
    ttb_assert_addPrint("Test %u -- %u == %u", size, val, expectedOut);
    uint32_t outVal = swl_math_getInsertIndexU32(array, size, val);
    ttb_assert_int_eq(outVal, expectedOut);
    ttb_assert_removeLastPrint();
}

static void test_swl_math_getInsertIndexU32(void** state _UNUSED) {
    uint32_t testArray[] = {0, 1, 2, 5, 10};
    uint32_t expectedIndex = 0;

    for(uint32_t i = 0; i < 12; i++) {
        if((expectedIndex < SWL_ARRAY_SIZE(testArray)) && (i > testArray[expectedIndex])) {
            expectedIndex++;
        }
        s_testGetInsertIndexU32(testArray, SWL_ARRAY_SIZE(testArray), i, expectedIndex);
    }

    uint32_t testArray2[] = {1000, 2000, 3000, 4000};
    expectedIndex = 0;

    for(uint32_t i = 0; i < 6000; i += 500) {
        if((expectedIndex < SWL_ARRAY_SIZE(testArray2)) && (i > testArray2[expectedIndex])) {
            expectedIndex++;
        }
        s_testGetInsertIndexU32(testArray2, SWL_ARRAY_SIZE(testArray2), i, expectedIndex);
    }
}


static void test_swl_math_interpolateDArray(void** state _UNUSED) {
    assert_true(swl_math_interpolateDArray(NULL, NULL, 0, 0.0) == 0.0);

    double baseSrcArray[4] = {0.0, 1.0, 2.0, 4.0};
    double baseTgtArray[4] = {10.0, 0.0, 100.0, 1000.0};

    assert_true(swl_math_interpolateDArray(NULL, baseTgtArray, 3, 0.0) == 0.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, NULL, 3, 0.0) == 0.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 0, 0.0) == 0.0);

    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, -1000) == 10.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, -0.5) == 10.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 0.0) == 10.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 0.5) == 5.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 1.0) == 0.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 1.5) == 50.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 2.0) == 100.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 2.5) == 325.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 3) == 550.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 3.5) == 775.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 4) == 1000.0);
    assert_true(swl_math_interpolateDArray(baseSrcArray, baseTgtArray, 4, 100) == 1000.0);

}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    unsigned int seed = time(NULL);
    printf("Test run with seed: %u\n", seed); // print to make possible to re-run failed tests
    srand(seed);

    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    const struct CMUnitTest tests[] = {

        cmocka_unit_test(test_swl_math_isPowerOf),
        cmocka_unit_test(test_swl_math_isPowerOf2),
        cmocka_unit_test(test_swl_math_invSumInv),
        cmocka_unit_test(test_swl_math_interpolate),
        cmocka_unit_test(test_swl_math_doExpAvg),
        cmocka_unit_test(test_swl_math_fmod),
        cmocka_unit_test(test_swl_math_fdiv),
        cmocka_unit_test(test_swl_math_udiv),
        cmocka_unit_test(test_swl_math_multComplement),
        cmocka_unit_test(test_swl_math_log10),
        cmocka_unit_test(test_swl_math_log10mult),
        cmocka_unit_test(test_swl_math_getInsertIndexU32),
        cmocka_unit_test(test_swl_math_interpolateDArray),
    };

    ttb_util_setFilter();
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
