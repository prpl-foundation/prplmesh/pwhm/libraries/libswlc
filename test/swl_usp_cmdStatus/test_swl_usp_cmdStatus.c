/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <time.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_usp_cmdStatus.h"

typedef struct {
    int32_t code;
    const char* str;
} statusMap_t;
static const statusMap_t s_knownMaps[] = {
    {.code = SWL_USP_CMD_STATUS_ERROR_INVALID_MAC, .str = "Error_Invalid_Mac", },
    {.code = SWL_USP_CMD_STATUS_ERROR_INTERFACE_DOWN, .str = "Error_Interface_Down", },
    {.code = SWL_USP_CMD_STATUS_ERROR_TIMEOUT, .str = "Error_Timeout", },
    {.code = SWL_USP_CMD_STATUS_ERROR_OTHER, .str = "Error_Other", },
    {.code = SWL_USP_CMD_STATUS_ERROR_NOT_IMPLEMENTED, .str = "Error_Not_Implemented", },
    {.code = SWL_USP_CMD_STATUS_ERROR_NOT_READY, .str = "Error_Not_Ready", },
    {.code = SWL_USP_CMD_STATUS_ERROR_INVALID_INPUT, .str = "Error_Invalid_Input", },
    {.code = SWL_USP_CMD_STATUS_ERROR, .str = "Error", },
    {.code = SWL_USP_CMD_STATUS_SUCCESS, .str = "Success", },
    {.code = SWL_USP_CMD_STATUS_CANCELED, .str = "Canceled", },
    {.code = SWL_USP_CMD_STATUS_COMPLETE, .str = "Complete", },
};

static void test_swl_uspCmdStatus_toString(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(s_knownMaps); i++) {
        assert_string_equal(swl_uspCmdStatus_toString(s_knownMaps[i].code), s_knownMaps[i].str);
    }
    statusMap_t unknownMaps[] = {
        /* error cases */
        {.code = SWL_USP_CMD_STATUS_MAX, .str = SWL_USP_CMD_STATUS_UNDEFINED_STR, },
        {.code = SWL_USP_CMD_STATUS_MIN, .str = SWL_USP_CMD_STATUS_UNDEFINED_STR, },
        {.code = SWL_USP_CMD_STATUS_COUNT, .str = SWL_USP_CMD_STATUS_UNDEFINED_STR, },
        {.code = -1598, .str = SWL_USP_CMD_STATUS_UNDEFINED_STR, },
        {.code = 25, .str = SWL_USP_CMD_STATUS_UNDEFINED_STR, },
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(unknownMaps); i++) {
        const char* result = swl_uspCmdStatus_toString(unknownMaps[i].code);
        if(unknownMaps[i].str != NULL) {
            assert_string_equal(result, unknownMaps[i].str);
        } else {
            assert_ptr_equal(result, unknownMaps[i].str);
        }
    }
}

static void test_swl_uspCmdStatus_toCode(void** state _UNUSED) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(s_knownMaps); i++) {
        assert_int_equal(swl_uspCmdStatus_toCode(s_knownMaps[i].str), s_knownMaps[i].code);
    }
    statusMap_t unknownMaps[] = {
        /* error cases */
        {.str = "", .code = SWL_USP_CMD_STATUS_UNDEFINED, },
        {.str = NULL, .code = SWL_USP_CMD_STATUS_UNDEFINED, },
        {.str = "Toto", .code = SWL_USP_CMD_STATUS_UNDEFINED, },
        {.str = "Error_Invalid", .code = SWL_USP_CMD_STATUS_UNDEFINED, },
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(unknownMaps); i++) {
        assert_int_equal(swl_uspCmdStatus_toCode(unknownMaps[i].str), unknownMaps[i].code);
    }
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlUspCmdStatus");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_uspCmdStatus_toString),
        cmocka_unit_test(test_swl_uspCmdStatus_toCode),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
