/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_circTable.h"

#define NR_TYPES 3
#define NR_VALUES 4

SWL_TABLE(myTestTable,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"},
              {8851, -289869, "barfoo"},
              )
          );
char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

#define NR_OTHER_DATA 2

static void test_swl_circTable_getMatchingTuple(void** state _UNUSED) {
    swl_circTable_t testTable = {0};
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        swl_circTable_addValues(&testTable, swl_table_getTuple(&myTestTable, i));
    }

    for(size_t i = 0; i < myTestTable.nrTuples; i++) {

        void* tuple1 = swl_table_getTuple(&myTestTable, i);

        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_circTable_getValue(&testTable, i, j);
            void* tuple2 = swl_circTable_getMatchingTuple(&testTable, j, srcData);

            swl_tupleType_equals(myTestTable.tupleType, tuple1, tuple2);
        }
    }

    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_circTable_getValue(&testTable, i, j);
            assert_null(swl_circTable_getMatchingTuple(&testTable, myTestTable.nrTuples, srcData));
            assert_null(swl_circTable_getMatchingTuple(&testTable, -1, srcData));
            assert_null(swl_circTable_getMatchingTuple(NULL, j, srcData));
        }
    }

    swl_circTable_destroy(&testTable);
}

static void test_swl_circTable_getMatchingTupleInRange(void** state _UNUSED) {
    swl_circTable_t testTable = {0};
    swl_circTable_init(&testTable, myTestTable.tupleType, myTestTable.nrTuples);
    swl_circTable_setNames(&testTable, names);
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        swl_circTable_addValues(&testTable, swl_table_getTuple(&myTestTable, i));
    }

    for(size_t i = 0; i < myTestTable.nrTuples; i++) {

        void* tuple1 = swl_table_getTuple(&myTestTable, i);

        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_circTable_getValue(&testTable, i, j);
            void* tuple2 = swl_circTable_getMatchingTuple(&testTable, j, srcData);

            swl_tupleType_equals(myTestTable.tupleType, tuple1, tuple2);

            void* tupleIncr = swl_circTable_getMatchingTupleInRange(&testTable, j, srcData, 0, -1, 1);
            swl_tupleType_equals(myTestTable.tupleType, tupleIncr, tuple2);
            // since values are unique, getMatchingTupleInRange(0, -1, 1) should return the same value

            void* tupleDecr = swl_circTable_getMatchingTupleInRange(&testTable, j, srcData, -1, 0, -1);
            swl_tupleType_equals(myTestTable.tupleType, tupleDecr, tuple2);
            // since values are unique, getMatchingTupleInRange(-1, 0, -1) should return the same value
        }
    }

    for(size_t i = 0; i < NR_OTHER_DATA; i++) {
        for(size_t j = 0; j < myTestTableTupleType.nrTypes; j++) {
            void* srcData = swl_circTable_getValue(&testTable, i, j);
            // trigger assert(srcIndex < type->nrTypes), using nrTuples == 4 as srcIndex;
            assert_null(swl_circTable_getMatchingTupleInRange(&testTable, myTestTable.nrTuples, srcData, 0, -1, 1));
            // trigger assert(srcIndex < type->nrTypes), using (size_t)(-1) == 2^64-2
            assert_null(swl_circTable_getMatchingTupleInRange(&testTable, -1, srcData, -1, 0, -1));
            // trigger ASSERT_NOT_NULL(table)
            assert_null(swl_circTable_getMatchingTupleInRange(NULL, j, srcData, 0, -1, 1));
            //trigger ASSERT_TRUE((stepSize != 0); three integer parameters are startIndex, endIndex, stepSize
            assert_null(swl_circTable_getMatchingTupleInRange(&testTable, j, srcData, 0, -1, 0));
            //trigger ASSERT_TRUE(startIndex < table->table.curNrTuples);
            assert_null(swl_circTable_getMatchingTupleInRange(&testTable, j, srcData, 50, -1, 1));
        }
    }

    swl_circTable_destroy(&testTable);
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_circTable_getMatchingTuple),
        cmocka_unit_test(test_swl_circTable_getMatchingTupleInRange),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
