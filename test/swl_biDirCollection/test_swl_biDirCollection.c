/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>    // needed for cmocka
#include <sys/types.h> // needed for cmocka
#include <setjmp.h>    // needed for cmocka
#include <cmocka.h>
#include "debug/sahtrace.h"
#include "swl/swl_common.h"
#include "swl/swl_biDirCollection.h"
#include <malloc.h>
#include "example/exampleParent.h"
#include "example/exampleChild.h"

#define ME "test"

typedef struct mockParent mockParent_t;
typedef struct mockChild mockChild_t;
typedef void (* onMockParentRemove_t)(mockChild_t* child, mockParent_t* parent);

// This is also test how biDirCollection behaves when the child misbehaves,
// so this does not serve as a proper example of a child.
// For a proper example of a child, see `exampleChild.c`.
struct mockChild {
    mockParent_t* parent;
    char* name;
    int onRemoveCounter;
    bool allowMultiRemove;
    int onAddCounter;
    onMockParentRemove_t customOnRemove;
};
static const char* s_mockChild_name(const void* childUntyped) {
    const mockChild_t* child = childUntyped;
    return child->name;
}
static void* s_mockChild_parent(void* childUntyped) {
    mockChild_t* child = childUntyped;
    return child->parent;
}
static void s_mockChild_onRemove(void* childUntyped, void* parentUntyped) {
    mockChild_t* child = childUntyped;
    mockParent_t* parent = parentUntyped;
    assert_ptr_equal(child->parent, parentUntyped);
    if(child->customOnRemove) {
        child->customOnRemove(child, parent);
    }
    child->parent = NULL;
    child->onRemoveCounter++;
    if(!child->allowMultiRemove) {
        assert_int_equal(1, child->onRemoveCounter);
    }
}
static bool s_mockChild_onAdd(void* childUntyped, void* parentUntyped) {
    mockChild_t* child = childUntyped;
    mockParent_t* parent = parentUntyped;
    child->onAddCounter++;
    ASSERT_NULL(child->parent, false, ME, "Already has a parent");
    child->parent = parent;
    return true;
}
static const swl_bdc_child_type_t mockChildType_mandatoryParent = {
    .getName = s_mockChild_name,
    .getParent = s_mockChild_parent,
    .onRemove = s_mockChild_onRemove,
    .onAdd = NULL,
};
const swl_bdc_child_type_t mockChildType_optionalParent = {
    .getName = s_mockChild_name,
    .getParent = s_mockChild_parent,
    .onRemove = s_mockChild_onRemove,
    .onAdd = s_mockChild_onAdd,
};

/** See `exampleParent.c` for proper example of a parent. */
struct mockParent {
    swl_bdc_t children; // of type mockChild_t
};
bool mockParent_add(mockParent_t* self, mockChild_t* child) {
    return swl_bdc_add(&self->children, child);
}
bool mockParent_remove(mockParent_t* self, mockChild_t* child) {
    return swl_bdc_remove(&self->children, child);
}
mockChild_t* mockParent_childByName(mockParent_t* self, const char* name) {
    return swl_bdc_byName(&self->children, name);
}
mockParent_t* mockParent_create_ext(const swl_bdc_child_type_t* childType) {
    mockParent_t* self = calloc(1, sizeof(mockParent_t));
    bool ok = swl_bdc_init(&self->children, self, childType);
    assert_true(ok);
    return self;
}
/** for children where parenthood is mandatory */
mockParent_t* mockParent_create() {
    return mockParent_create_ext(&mockChildType_mandatoryParent);
}
void mockParent_destroy(mockParent_t* parent) {
    ASSERTS_NOT_NULL(parent, , ME, "NULL");
    swl_bdc_cleanup(&parent->children);
    free(parent);
}

static void test_init_incompleteData(void** state _UNUSED) {
    mockParent_t parent;

    // case: NULL parent
    assert_false(swl_bdc_init(&parent.children, NULL, &mockChildType_mandatoryParent));
    // case: NULL child type
    assert_false(swl_bdc_init(&parent.children, &parent, NULL));
    // case: NULL biDirCollection
    assert_false(swl_bdc_init(NULL, &parent, &mockChildType_mandatoryParent));

    // case: NULL getName function
    const swl_bdc_child_type_t mockChildType_noName = {
        .getName = NULL,
        .getParent = s_mockChild_parent,
        .onRemove = s_mockChild_onRemove,
    };
    assert_false(swl_bdc_init(&parent.children, &parent, &mockChildType_noName));

    // case: NULL getParent function
    const swl_bdc_child_type_t mockChildType_noGetParent = {
        .getName = s_mockChild_name,
        .getParent = NULL,
        .onRemove = s_mockChild_onRemove,
    };
    assert_false(swl_bdc_init(&parent.children, &parent, &mockChildType_noGetParent));

    // case: NULL onRemove function
    const swl_bdc_child_type_t mockChildType_noonRemove = {
        .getName = NULL,
        .getParent = s_mockChild_parent,
        .onRemove = NULL,
    };
    assert_false(swl_bdc_init(&parent.children, &parent, &mockChildType_noonRemove));
}

static void test_add_normalCase(void** state _UNUSED) {
    // GIVEN a parent and an in-progress-associated child
    mockParent_t* parent = mockParent_create();
    mockChild_t child = { .name = "child1", .parent = parent };

    // WHEN adding a child
    bool ok = mockParent_add(parent, &child);

    // THEN it is added
    assert_true(ok);
    assert_ptr_equal(parent, child.parent);
    assert_ptr_equal(&child, mockParent_childByName(parent, "child1"));

    mockParent_destroy(parent);
}

static void test_add_childClaimsNoParent(void** state _UNUSED) {
    // GIVEN a parent and a child that has no parent
    mockParent_t* parent = mockParent_create();
    mockChild_t child = { .name = "child1", .parent = NULL };

    // WHEN adding the child as a third party (so the child does not want the parent)
    bool ok = mockParent_add(parent, &child);

    // THEN it is not added
    assert_false(ok);
    assert_null(mockParent_childByName(parent, "child1"));

    mockParent_destroy(parent);
}

static void test_add_twice(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent = mockParent_create();
    mockChild_t child = { .name = "child2", .parent = parent };
    bool ok = mockParent_add(parent, &child);
    assert_true(ok);

    // WHEN adding the child again
    ok = mockParent_add(parent, &child);

    // THEN it is not added
    assert_false(ok);
    // so after removing it, it's also not there anymore.
    ok = mockParent_remove(parent, &child);
    assert_true(ok);
    assert_null(mockParent_childByName(parent, "child2"));

    mockParent_destroy(parent);
}

static void test_add_nameDup(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent };
    mockParent_add(parent, &child1);

    // WHEN adding another child with the same name
    mockChild_t child2 = { .name = "child", .parent = parent };
    bool ok = mockParent_add(parent, &child2);

    // THEN it is not added
    assert_false(ok);
    // so after removing it, it's also not there anymore.
    mockParent_remove(parent, &child1);
    assert_null(mockParent_childByName(parent, "child"));

    mockParent_destroy(parent);
}

static void test_add_hasDifferentParent(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1 };
    mockParent_add(parent1, &child1);

    // WHEN adding the child to another parent
    mockParent_t* parent2 = mockParent_create();
    mockParent_add(parent2, &child1);
    bool ok = mockParent_add(parent2, &child1);

    // THEN it is not added
    assert_false(ok);
    assert_ptr_equal(&child1, mockParent_childByName(parent1, "child"));
    assert_null(mockParent_childByName(parent2, "child"));
    mockParent_remove(parent1, &child1);
    assert_null(mockParent_childByName(parent1, "child"));

    mockParent_destroy(parent1);
    mockParent_destroy(parent2);
}

static void test_add_nullName(void** state _UNUSED) {
    // GIVEN a parent without children, and a child with NULL name
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = NULL, .parent = parent1 };

    // WHEN adding the child
    bool ok = mockParent_add(parent1, &child1);

    // THEN it was refused
    assert_false(ok);
    assert_int_equal(0, child1.onAddCounter);
    assert_int_equal(0, child1.onRemoveCounter);
    assert_null(mockParent_childByName(parent1, NULL));

    mockParent_destroy(parent1);
}

static void test_add_optionalParent_normalcase(void** state _UNUSED) {
    // GIVEN a parent and a child without parent
    mockParent_t* parent = mockParent_create_ext(&mockChildType_optionalParent);
    mockChild_t child = {
        .name = "child",
        .parent = NULL,     // <-- note this.
    };

    // WHEN adding the child
    bool ok = mockParent_add(parent, &child);

    // THEN the child is added
    assert_true(ok);
    assert_ptr_equal(&child, mockParent_childByName(parent, "child"));
    // AND THEN the child was informed it was added
    assert_int_equal(1, child.onAddCounter);
    assert_ptr_equal(parent, child.parent);

    mockParent_destroy(parent);
}

static bool s_onAdd_returnFalse(void* childUntyped, void* parentUntyped _UNUSED) {
    mockChild_t* child = childUntyped;
    child->onAddCounter++;
    return false;
}

static void test_add_optionalParent_rejected(void** state _UNUSED) {
    // GIVEN a parent and a child without parent, and child has onAdd.
    const swl_bdc_child_type_t mockChildType_onAddReturnFalse = {
        .getName = s_mockChild_name,
        .getParent = s_mockChild_parent,
        .onRemove = s_mockChild_onRemove,
        .onAdd = s_onAdd_returnFalse, // <-- note this.
    };
    mockParent_t* parent = mockParent_create_ext(&mockChildType_onAddReturnFalse);
    mockChild_t child = { .name = "child", .parent = NULL };

    // WHEN adding the child, where the child refuses
    bool ok = mockParent_add(parent, &child);

    // THEN the child is not added
    assert_false(ok);
    assert_null(mockParent_childByName(parent, "child"));
    assert_int_equal(1, child.onAddCounter);
    assert_null(child.parent);

    mockParent_destroy(parent);
}

static void test_add_optionalParent_move(void** state _UNUSED) {
    // GIVEN a parent with a child, and a parent without child. The child allows having no parent.
    mockParent_t* parent1 = mockParent_create_ext(&mockChildType_optionalParent);
    mockParent_t* parent2 = mockParent_create_ext(&mockChildType_optionalParent);
    mockChild_t child = {
        .name = "child",
        .parent = NULL,
        .allowMultiRemove = true,
    };
    assert_true(mockParent_add(parent1, &child));

    // WHEN moving the child by removing it first from its parent and then adding it to the second
    //      parent
    bool okRemove = mockParent_remove(parent1, &child);
    bool okAdd = mockParent_add(parent2, &child);

    // THEN the child is moved
    assert_true(okRemove);
    assert_true(okAdd);
    assert_int_equal(2, child.onAddCounter);
    assert_int_equal(1, child.onRemoveCounter);
    assert_null(mockParent_childByName(parent1, "child"));
    assert_ptr_equal(&child, mockParent_childByName(parent2, "child"));

    // cleanup
    mockParent_destroy(parent1);
    mockParent_destroy(parent2);
    assert_int_equal(2, child.onAddCounter);
    assert_int_equal(2, child.onRemoveCounter);
}

static void test_remove_normalCase(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1 };
    mockParent_add(parent1, &child1);

    // WHEN removing the parent from the child
    bool ok = mockParent_remove(parent1, &child1);

    // THEN it is removed
    assert_true(ok);
    assert_null(mockParent_childByName(parent1, "child"));

    mockParent_destroy(parent1);
}

static void test_remove_notAssociated(void** state _UNUSED) {
    // GIVEN a parent and a not-completely-associated-child
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1 };

    // WHEN removing the parent from the child
    bool ok = mockParent_remove(parent1, &child1);

    // THEN an error is returned
    assert_false(ok);

    mockParent_destroy(parent1);
}

static void test_remove_twice(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1 };
    mockParent_add(parent1, &child1);

    // WHEN removing the parent from the child, twice
    mockParent_remove(parent1, &child1);
    bool ok = mockParent_remove(parent1, &child1);

    // THEN an error is returned (but it's still removed)
    assert_false(ok);
    assert_null(mockParent_childByName(parent1, "child"));

    mockParent_destroy(parent1);
}

static void test_search_wrongName(void** state _UNUSED) {
    // GIVEN a parent with a child
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1 };
    mockParent_add(parent1, &child1);

    // WHEN searching the child by an incorrect name
    mockChild_t* childFound = mockParent_childByName(parent1, "oops typo");

    // THEN it is not found
    assert_null(childFound);

    mockParent_destroy(parent1);
}

static void test_destroy(void** state _UNUSED) {
    // GIVEN a parent with children
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child", .parent = parent1, .onRemoveCounter = 0 };
    mockChild_t child2 = { .name = "child2", .parent = parent1, .onRemoveCounter = 0 };
    mockParent_add(parent1, &child1);
    mockParent_add(parent1, &child2);

    // WHEN destroying the parent
    mockParent_destroy(parent1);

    // THEN the children could do their part of cleaning up the bidir assoc or destroy themselves
    assert_int_equal(1, child1.onRemoveCounter);
    assert_int_equal(1, child2.onRemoveCounter);
}

static void test_destroy_empty(void** state _UNUSED) {
    // GIVEN a parent without children
    mockParent_t* parent1 = mockParent_create();

    // WHEN destroying the parent
    mockParent_destroy(parent1);

    // THEN it didn't crash or leak according to valgrind.
}

static void s_onRemove_add(mockChild_t* child _UNUSED, mockParent_t* parent) {
    // GIVEN a parent with a child while the biDirCollection is being destroyed

    // WHEN adding children to the parent
    mockChild_t child4 = { .name = "child4", .parent = parent, };
    bool ok = mockParent_add(parent, &child4);

    // THEN it is not added
    assert_false(ok);
    assert_null(swl_bdc_byName(&parent->children, "child4"));
}

static void test_add_duringDestruction(void** state _UNUSED) {
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent1 };
    mockChild_t child2 = { .name = "child2", .parent = parent1, .customOnRemove = s_onRemove_add };
    mockChild_t child3 = { .name = "child3", .parent = parent1};
    mockParent_add(parent1, &child1);
    mockParent_add(parent1, &child2);
    mockParent_add(parent1, &child3);

    mockParent_destroy(parent1);
}

static void s_onRemove_byName(mockChild_t* child _UNUSED, mockParent_t* parent) {
    // GIVEN a parent with a child while the biDirCollection is being destroyed

    // WHEN looking up by name
    // THEN the biDirCollection appears empty
    assert_null(swl_bdc_byName(&parent->children, "child1"));
    assert_null(swl_bdc_byName(&parent->children, "child2"));
    assert_null(swl_bdc_byName(&parent->children, "child3"));
}

static void test_byName_duringDestruction(void** state _UNUSED) {
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent1 };
    mockChild_t child2 = { .name = "child2", .parent = parent1, .customOnRemove = s_onRemove_byName };
    mockChild_t child3 = { .name = "child3", .parent = parent1};
    mockParent_add(parent1, &child1);
    mockParent_add(parent1, &child2);
    mockParent_add(parent1, &child3);

    mockParent_destroy(parent1);
}

static void s_onRemove_remove(mockChild_t* child, mockParent_t* parent) {
    // GIVEN a parent with a child while the biDirCollection is being destroyed

    // WHEN removing a child
    // THEN nothing happens...
    swl_bdc_remove(&parent->children, child);
}

static void test_remove_duringDestruction(void** state _UNUSED) {
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent1 };
    mockChild_t child2 = { .name = "child2", .parent = parent1, .customOnRemove = s_onRemove_remove};
    mockChild_t child3 = { .name = "child3", .parent = parent1};
    mockParent_add(parent1, &child1);
    mockParent_add(parent1, &child2);
    mockParent_add(parent1, &child3);

    mockParent_destroy(parent1);
}

static void s_onRemove_destroy(mockChild_t* child _UNUSED, mockParent_t* parent) {
    // GIVEN a parent with a child while the biDirCollection is being destroyed

    // WHEN destroying the biDirCollection again
    // THEN valgrind is still happy.
    swl_bdc_cleanup(&parent->children);
}

static void test_destroy_duringDestruction(void** state _UNUSED) {
    mockParent_t* parent1 = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent1 };
    mockChild_t child2 = { .name = "child2", .parent = parent1, .customOnRemove = s_onRemove_destroy};
    mockChild_t child3 = { .name = "child3", .parent = parent1};
    mockParent_add(parent1, &child1);
    mockParent_add(parent1, &child2);
    mockParent_add(parent1, &child3);

    mockParent_destroy(parent1);
}

static void test_example(void** state _UNUSED) {
    exampleParent_t* parent = exampleParent_create();
    assert_non_null(parent);

    exampleChild_t* child1 = exampleChild_create("child1", parent);
    exampleChild_t* child2 = exampleChild_create("child2", parent);
    assert_non_null(child1);
    assert_ptr_equal(child1, exampleParent_childByName(parent, "child1"));
    assert_non_null(child2);
    assert_ptr_equal(child2, exampleParent_childByName(parent, "child2"));
    exampleChild_destroy(child1);
    assert_null(exampleParent_childByName(parent, "child1"));

    exampleParent_destroy(parent);
}

static void test_iterator(void** state _UNUSED) {
    // GIVEN a biDirCollection with some items
    mockParent_t* parent = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent };
    mockChild_t child2 = { .name = "child2", .parent = parent };
    mockChild_t child3 = { .name = "child3", .parent = parent };
    mockParent_add(parent, &child1);
    mockParent_add(parent, &child2);
    mockParent_add(parent, &child3);

    // WHEN iterating through them
    int child1SeenCount = 0;
    int child2SeenCount = 0;
    int child3SeenCount = 0;
    swl_bdc_it_t it;
    swl_bdc_for_each(it, &parent->children) {
        mockChild_t* child = *swl_bdc_it_data(&it, mockChild_t * *);
        if(child == &child1) {
            child1SeenCount++;
        } else if(child == &child2) {
            child2SeenCount++;
        } else if(child == &child3) {
            child3SeenCount++;
        } else {
            fail_msg("Unknown child: %p, expected: %p or %p or %p", child, &child1, &child2, &child3);
        }
    }

    // THEN each child was encountered once while iterating
    assert_int_equal(1, child1SeenCount);
    assert_int_equal(1, child2SeenCount);
    assert_int_equal(1, child3SeenCount);

    mockParent_destroy(parent);
}

static void test_for_each_ext(void** state _UNUSED) {
    // GIVEN a biDirCollection with some items
    mockParent_t* parent = mockParent_create();
    mockChild_t child1 = { .name = "child1", .parent = parent };
    mockChild_t child2 = { .name = "child2", .parent = parent };
    mockChild_t child3 = { .name = "child3", .parent = parent };
    mockParent_add(parent, &child1);
    mockParent_add(parent, &child2);
    mockParent_add(parent, &child3);

    // WHEN iterating through them with swl_bdc_for_each_ext
    int child1SeenCount = 0;
    int child2SeenCount = 0;
    int child3SeenCount = 0;
    mockChild_t* child = NULL;
    swl_bdc_it_t it;
    swl_bdc_for_each_ext(it, child, &parent->children) {
        if(child == &child1) {
            child1SeenCount++;
        } else if(child == &child2) {
            child2SeenCount++;
        } else if(child == &child3) {
            child3SeenCount++;
        } else {
            fail_msg("Unknown child: %p, expected: %p or %p or %p", child, &child1, &child2, &child3);
        }
    }

    // THEN each child was encountered once while iterating
    assert_int_equal(1, child1SeenCount);
    assert_int_equal(1, child2SeenCount);
    assert_int_equal(1, child3SeenCount);

    mockParent_destroy(parent);
}

static void test_firstIt_empty(void** state _UNUSED) {
    // GIVEN a biDirCollection without items
    mockParent_t* parent = mockParent_create();

    // WHEN initializing an iterator
    swl_bdc_it_t it;
    mockChild_t* firstChild = swl_bdc_firstIt(&it, &parent->children);

    // THEN the iterator is empty and returned NULL as first element
    assert_null(firstChild);
    assert_false(swl_bdc_it_isValid(&it));

    mockParent_destroy(parent);
}

static void test_it_next_empty(void** state _UNUSED) {
    // GIVEN a biDirCollection iterator without items
    mockParent_t* parent = mockParent_create();
    swl_bdc_it_t it;
    swl_bdc_firstIt(&it, &parent->children);

    // WHEN asking the next child
    mockChild_t* nextChild1 = swl_bdc_it_next(&it);
    mockChild_t* nextChild2 = swl_bdc_it_next(&it);

    // THEN NULL is returned
    assert_null(nextChild1);
    assert_null(nextChild2);
    assert_false(swl_bdc_it_isValid(&it));

    mockParent_destroy(parent);
}


static void test_iteratorAfterRemove(void** state _UNUSED) {
    // GIVEN a biDirCollection with some items, and one removed item
    exampleParent_t* parent = exampleParent_create();
    exampleChild_t* child1 = exampleChild_create("child1", parent);
    exampleChild_t* child2 = exampleChild_create("child2", parent);
    exampleChild_t* child3 = exampleChild_create("child3", parent);
    exampleChild_destroy(child2);
    exampleChild_t* child4 = exampleChild_create("child4", parent);

    // WHEN iterating through them
    int child1SeenCount = 0;
    int child2SeenCount = 0;
    int child3SeenCount = 0;
    int child4SeenCount = 0;
    swl_bdc_it_t it;
    for(exampleParent_childIterator(parent, &it); swl_bdc_it_isValid(&it); swl_bdc_it_next(&it)) {
        exampleChild_t* child = *swl_bdc_it_data(&it, exampleChild_t * *);
        if(child == child1) {
            child1SeenCount++;
        } else if(child == child2) {
            child2SeenCount++;
        } else if(child == child3) {
            child3SeenCount++;
        } else if(child == child4) {
            child4SeenCount++;
        } else {
            fail_msg("Unknown child: %p");
        }
    }

    // THEN each child was encountered once while iterating
    assert_int_equal(1, child1SeenCount);
    assert_int_equal(0, child2SeenCount); // <-- the removed child
    assert_int_equal(1, child3SeenCount);
    assert_int_equal(1, child4SeenCount);

    exampleParent_destroy(parent);
}

int main() {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(TRACE_LEVEL_INFO, "swlNc");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_init_incompleteData),
        cmocka_unit_test(test_add_normalCase),
        cmocka_unit_test(test_add_childClaimsNoParent),
        cmocka_unit_test(test_add_twice),
        cmocka_unit_test(test_add_nameDup),
        cmocka_unit_test(test_add_hasDifferentParent),
        cmocka_unit_test(test_add_nullName),
        cmocka_unit_test(test_add_optionalParent_normalcase),
        cmocka_unit_test(test_add_optionalParent_rejected),
        cmocka_unit_test(test_add_optionalParent_move),
        cmocka_unit_test(test_remove_normalCase),
        cmocka_unit_test(test_remove_notAssociated),
        cmocka_unit_test(test_remove_twice),
        cmocka_unit_test(test_search_wrongName),
        cmocka_unit_test(test_destroy),
        cmocka_unit_test(test_destroy_empty),
        cmocka_unit_test(test_byName_duringDestruction),
        cmocka_unit_test(test_remove_duringDestruction),
        cmocka_unit_test(test_add_duringDestruction),
        cmocka_unit_test(test_destroy_duringDestruction),
        cmocka_unit_test(test_example),
        cmocka_unit_test(test_iterator),
        cmocka_unit_test(test_for_each_ext),
        cmocka_unit_test(test_iteratorAfterRemove),
        cmocka_unit_test(test_firstIt_empty),
        cmocka_unit_test(test_it_next_empty),
    };
    sahTraceClose();
    return cmocka_run_group_tests(tests, NULL, NULL);
}

