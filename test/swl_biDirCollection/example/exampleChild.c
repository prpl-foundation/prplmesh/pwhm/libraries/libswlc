/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "exampleChild.h"
#include "exampleParent.h"
#include "swl/swl_common.h"
#include <malloc.h>
#include <string.h>

#define ME "egChild"

/**
 * \file
 * Example child where the child always has a parent.
 */

struct exampleChild {
    exampleParent_t* parent;
    char* name;
};

exampleChild_t* exampleChild_create(const char* name, exampleParent_t* parent) {
    ASSERT_STR(name, NULL, ME, "EMPTY");
    ASSERT_NOT_NULL(parent, NULL, ME, "NULL");

    exampleChild_t* self = calloc(1, sizeof(exampleChild_t));
    ASSERT_NOT_NULL(self, NULL, ME, "out of mem");
    self->name = strdup(name);
    self->parent = parent;

    bool ok = exampleParent_add(parent, self);
    if(!ok) {
        SAH_TRACEZ_ERROR(ME, "Refused by parent");
        free(self->name);
        free(self);
        return NULL;
    }

    return self;
}

const char* exampleChild_name(const exampleChild_t* self) {
    ASSERT_NOT_NULL(self, NULL, ME, "NULL");
    return self->name;
}

exampleParent_t* exampleChild_parent(exampleChild_t* self) {
    ASSERT_NOT_NULL(self, NULL, ME, "NULL");
    return self->parent;
}

void exampleChild_destroy(exampleChild_t* self) {
    ASSERTS_NOT_NULL(self, , ME, "NULL");

    // calls #s_exampleChild_onParentRemove(), which calls #s_destroyAfterUnlink()
    exampleParent_remove(self->parent, self);
}

static void s_destroyAfterUnlink(exampleChild_t* self) {
    ASSERT_NOT_NULL(self, , ME, "NULL");

    self->parent = NULL;
    // ...
    free(self->name);
    free(self);
}

static const char* s_exampleChild_name(const void* child) {
    return exampleChild_name(child);
}
static void* s_exampleChild_parent(void* child) {
    return exampleChild_parent(child);
}
static void s_exampleChild_onParentRemove(void* childUntyped, void* parent _UNUSED) {
    exampleChild_t* child = childUntyped;
    s_destroyAfterUnlink(child);
}
const swl_bdc_child_type_t exampleChildType = {
    .getName = s_exampleChild_name,
    .getParent = s_exampleChild_parent,
    .onRemove = s_exampleChild_onParentRemove,
};


