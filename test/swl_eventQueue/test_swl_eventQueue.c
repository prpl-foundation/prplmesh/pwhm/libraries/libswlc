/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <test-toolbox/ttb_assert.h>
#include <swl/ttb/swl_ttb.h>

#include <cmocka.h>
#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_eventQueue.h"

static swl_eventQueue_t queue0 = SWL_EVENT_QUEUE_NEW("queue0");
static swl_eventQueue_t queue1 = SWL_EVENT_QUEUE_NEW("queue1");

typedef struct {
    uint32_t called;
    const swl_event_t* event;
    void* userData;
} test_handleData_t;

#define NR_HANDLERS 2

static test_handleData_t s_handlers[NR_HANDLERS];

static void s_checkAndClear() {
    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 0);
    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue1), 0);

    for(uint32_t i = 0; i < NR_HANDLERS; i++) {
        s_handlers[i].called = 0;
        s_handlers[i].event = NULL;
        s_handlers[i].userData = NULL;
    }
}

static void s_setData(uint32_t index, const swl_event_t* data, void* userData) {
    s_handlers[index].called++;
    s_handlers[index].event = data;
    s_handlers[index].userData = userData;
}

static void s_handler0(const swl_event_t* data, void* userData) {
    s_setData(0, data, userData);
}


static void s_handler1(const swl_event_t* data, void* userData) {
    s_setData(1, data, userData);
}

static void test_find(void** state _UNUSED) {
    s_checkAndClear();

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));

    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 1);
    ttb_assert_true(swl_eventQueue_hasSubscriber(&queue0, s_handler0, &queue0));

    ttb_assert_false(swl_eventQueue_hasSubscriber(&queue0, s_handler0, &queue1));
    ttb_assert_false(swl_eventQueue_hasSubscriber(&queue0, s_handler1, &queue0));
    ttb_assert_false(swl_eventQueue_hasSubscriber(&queue0, s_handler1, NULL));
    SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, &queue0);

    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 0);
    ttb_assert_false(swl_eventQueue_hasSubscriber(&queue0, s_handler0, &queue0));
}

static void test_handlersDiffQueue(void** state _UNUSED) {
    s_checkAndClear();

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue1, s_handler1, NULL));

    uint32_t test = 0;
    swl_eventQueue_publish(&queue0, &test);
    ttb_assert_int_eq(s_handlers[0].called, 1);
    ttb_assert_ptr_eq(s_handlers[0].event, &test);

    ttb_assert_int_eq(s_handlers[1].called, 0);

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue1, s_handler1, NULL));
    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 0);

}


static void test_twoHandlers(void** state _UNUSED) {
    s_checkAndClear();

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler1, NULL));

    uint32_t test = 0;
    swl_eventQueue_publish(&queue0, &test);
    ttb_assert_true(s_handlers[0].called);
    ttb_assert_ptr_eq(s_handlers[0].event, &test);
    ttb_assert_ptr_eq(s_handlers[0].userData, &queue0);

    ttb_assert_true(s_handlers[1].called);
    ttb_assert_ptr_eq(s_handlers[1].event, &test);
    ttb_assert_ptr_eq(s_handlers[1].userData, NULL);

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler1, NULL));
    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 0);

}

static void test_sameHandler(void** state _UNUSED) {
    s_checkAndClear();

    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue1));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, NULL));

    //Null queue test
    swl_ttb_assertRcEq(SWL_RC_INVALID_PARAM, SWL_EVENT_QUEUE_SUBSCRIBE(NULL, s_handler0, &queue0));
    // Same handler & data test
    swl_ttb_assertRcEq(SWL_RC_INVALID_STATE, SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));

    uint32_t test = 0;
    swl_eventQueue_publish(&queue0, &test);
    ttb_assert_int_eq(s_handlers[0].called, 3);
    ttb_assert_ptr_eq(s_handlers[0].event, &test);
    ttb_assert_ptr_eq(s_handlers[0].userData, NULL);


    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, &queue1));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_CANCEL(&queue0, s_handler0, NULL));
    ttb_assert_int_eq(swl_eventQueue_getNrHandlers(&queue0), 0);

}

static void test_destroyQueue(void** state _UNUSED) {
    s_checkAndClear();
    ttb_assert_int_eq(SWL_RC_INVALID_STATE, swl_eventQueue_init(&queue1, "testName"));


    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue0));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, &queue1));
    swl_ttb_assertRcOk(SWL_EVENT_QUEUE_SUBSCRIBE(&queue0, s_handler0, NULL));

    swl_ttb_assertRcEq(SWL_RC_DONE,
                       swl_eventQueue_destroy(&queue0));
    swl_ttb_assertRcEq(SWL_RC_OK,
                       swl_eventQueue_destroy(&queue1));
    swl_ttb_assertRcEq(SWL_RC_INVALID_STATE,
                       swl_eventQueue_destroy(&queue1));
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlhash");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_find),
        cmocka_unit_test(test_handlersDiffQueue),
        cmocka_unit_test(test_twoHandlers),
        cmocka_unit_test(test_sameHandler),
        cmocka_unit_test(test_destroyQueue),
    };


    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
