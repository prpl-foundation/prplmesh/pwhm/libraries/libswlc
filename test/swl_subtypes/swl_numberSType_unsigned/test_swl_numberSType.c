/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include "test-toolbox/ttb.h"
#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/subtypes/swl_numberSType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/fileOps/swl_fileUtils.h"

typedef struct {
    uint64_t val1;
    uint64_t val2;
} test_unsignedVal;

test_unsignedVal testValues[] = {{0, 0},
    {1, 0},
    {0, 1},
    {5, 2},
    {2, 5},
    {100, 100},
    {100, 50},
    {100, 50},
    {1000, 1000},
    {UINT64_MAX / 4, 1000},
    {1000, UINT64_MAX / 4},
    {0xffffff, 0xffff}, };

static swl_type_t* signedNumTypes[] = {
    &gtSwl_type_uint8,
    &gtSwl_type_uint16,
    &gtSwl_type_uint32,
    &gtSwl_type_uint64,
};

static swl_type_t* curType = NULL;
static swl_numberSTypeFun_t* nFun = NULL;

static int64_t s_toUnsignedBaseValue(uint64_t val, uint8_t bits) {
    if(bits == 64) {
        return val;
    }

    uint64_t base = ((uint64_t) 1) << bits;
    return val % base;
}

static void test_swl_numberType_getSetUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        memset(buffer, 0, curType->size);

        int64_t source = testValues[i].val1;

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1, nFun->nrBits);

        swl_rc_ne ret = nFun->fromUInt64(curType, buffer, testValues[i].val1);
        int64_t out = nFun->toUInt64(curType, buffer);

        ttb_assert_int_eq(target, out);
        if(target == source) {
            ttb_assert_int_eq(ret, SWL_RC_OK);
        } else {
            ttb_assert_int_eq(ret, SWL_RC_RESULT_OUT_OF_BOUNDS);
        }
    }
}

static void test_swl_numberType_addUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer1[curType->size];
    swl_bit8_t buffer2[curType->size];
    swl_bit8_t bufferOut[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        memset(buffer1, 0, curType->size);
        memset(buffer2, 0, curType->size);
        memset(bufferOut, 0, curType->size);

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1 + testValues[i].val2, nFun->nrBits);

        nFun->fromUInt64(curType, buffer1, testValues[i].val1);
        nFun->fromUInt64(curType, buffer2, testValues[i].val2);

        swl_numberSType_add(curType, bufferOut, buffer1, buffer2);
        int64_t out = nFun->toUInt64(curType, bufferOut);

        ttb_assert_int_eq(target, out);
    }
}

static void test_swl_numberType_subUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer1[curType->size];
    swl_bit8_t buffer2[curType->size];
    swl_bit8_t bufferOut[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        memset(buffer1, 0, curType->size);
        memset(buffer2, 0, curType->size);
        memset(bufferOut, 0, curType->size);

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1 - testValues[i].val2, nFun->nrBits);

        nFun->fromUInt64(curType, buffer1, testValues[i].val1);
        nFun->fromUInt64(curType, buffer2, testValues[i].val2);

        swl_numberSType_sub(curType, bufferOut, buffer1, buffer2);
        int64_t out = nFun->toUInt64(curType, bufferOut);

        ttb_assert_int_eq(target, out);
    }
}

static void test_swl_numberType_mulUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer1[curType->size];
    swl_bit8_t buffer2[curType->size];
    swl_bit8_t bufferOut[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        memset(buffer1, 0, curType->size);
        memset(buffer2, 0, curType->size);
        memset(bufferOut, 0, curType->size);

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1 * testValues[i].val2, nFun->nrBits);

        nFun->fromUInt64(curType, buffer1, testValues[i].val1);
        nFun->fromUInt64(curType, buffer2, testValues[i].val2);

        swl_numberSType_mul(curType, bufferOut, buffer1, buffer2);
        int64_t out = nFun->toUInt64(curType, bufferOut);

        ttb_assert_int_eq(target, out);
    }
}


static void test_swl_numberType_divUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer1[curType->size];
    swl_bit8_t buffer2[curType->size];
    swl_bit8_t bufferOut[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        if(testValues[i].val2 == 0) {
            continue;
        }
        if(!swl_numberSType_isIntInRange(curType, testValues[i].val1)
           || !swl_numberSType_isIntInRange(curType, testValues[i].val2)) {
            continue;
        }

        memset(buffer1, 0, curType->size);
        memset(buffer2, 0, curType->size);
        memset(bufferOut, 0, curType->size);

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1 / testValues[i].val2, nFun->nrBits);

        nFun->fromUInt64(curType, buffer1, testValues[i].val1);
        nFun->fromUInt64(curType, buffer2, testValues[i].val2);

        swl_numberSType_div(curType, bufferOut, buffer1, buffer2);
        int64_t out = nFun->toUInt64(curType, bufferOut);

        ttb_assert_int_eq(target, out);
    }
}

static void test_swl_numberType_modUnsigned(void** state _UNUSED) {
    swl_bit8_t buffer1[curType->size];
    swl_bit8_t buffer2[curType->size];
    swl_bit8_t bufferOut[curType->size];

    for(size_t i = 0; i < SWL_ARRAY_SIZE(testValues); i++) {
        if(testValues[i].val2 == 0) {
            continue;
        }
        if(!swl_numberSType_isIntInRange(curType, testValues[i].val1)
           || !swl_numberSType_isIntInRange(curType, testValues[i].val2)) {
            continue;
        }

        memset(buffer1, 0, curType->size);
        memset(buffer2, 0, curType->size);
        memset(bufferOut, 0, curType->size);

        int64_t target = s_toUnsignedBaseValue(testValues[i].val1 % testValues[i].val2, nFun->nrBits);

        nFun->fromUInt64(curType, buffer1, testValues[i].val1);
        nFun->fromUInt64(curType, buffer2, testValues[i].val2);

        swl_numberSType_mod(curType, bufferOut, buffer1, buffer2);
        int64_t out = nFun->toUInt64(curType, bufferOut);

        ttb_assert_int_eq(target, out);
    }
}

static int setup_suite(void** state _UNUSED) {

    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_numberType_getSetUnsigned),
        cmocka_unit_test(test_swl_numberType_addUnsigned),
        cmocka_unit_test(test_swl_numberType_subUnsigned),
        cmocka_unit_test(test_swl_numberType_mulUnsigned),
        cmocka_unit_test(test_swl_numberType_divUnsigned),
        cmocka_unit_test(test_swl_numberType_modUnsigned),
    };

    int rc1 = 0;
    for(size_t i = 0; i < SWL_ARRAY_SIZE(signedNumTypes); i++) {
        curType = signedNumTypes[i];
        nFun = (swl_numberSTypeFun_t*) curType->typeFun->subFun;
        rc1 += cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    }

    sahTraceClose();
    return rc1;
}

