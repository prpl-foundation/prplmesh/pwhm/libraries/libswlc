/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayType.h"
#include "swl/types/swl_vectorType.h"
#include "swl/types/swl_arrayListType.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/types/swl_ttaType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/ttb/swl_ttb.h"

#include "testCommon_swl_mapSType_singleValueType.h"


static swl_mapSType_singleValueTest_t* s_ct = NULL;
static swl_mapSTypeFun_t* s_mapFun;
static swl_mapSType_t* s_mapType;
static swl_mapEl_t* s_mapEmpty;
static swl_mapEl_t* s_mapFilled;



static int setup_suite(void** state _UNUSED) {
    s_mapType = (swl_mapSType_t* ) s_ct->mapType;
    s_mapFun = (swl_mapSTypeFun_t*) s_ct->mapType->typeFun->subFun;
    assert_non_null(s_mapFun);
    s_mapEmpty = calloc(1, s_ct->mapType->size);
    s_mapFun->init(s_mapType, s_mapEmpty);


    s_mapFilled = calloc(1, s_ct->mapType->size);
    s_mapFun->init(s_mapType, s_mapFilled);
    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_mapFun->add(s_mapType, s_mapFilled,
                      swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i),
                      swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i)
                      );
    }

    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), 0);
    s_mapFun->cleanup(s_mapType, s_mapEmpty);
    free(s_mapEmpty);

    assert_int_equal(s_mapFun->size(s_mapType, s_mapFilled), s_ct->nrValues);
    s_mapFun->cleanup(s_mapType, s_mapFilled);
    free(s_mapFilled);
    return 0;
}

static void test_initDestroy(void** state _UNUSED) {
    swl_mapEl_t* myMap = calloc(1, s_ct->mapType->size);
    s_mapFun->init(s_mapType, myMap);
    s_mapFun->cleanup(s_mapType, myMap);
    free(myMap);
}



static void test_addSizeClear(void** state _UNUSED) {

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), i);
        s_mapFun->add(s_mapType, s_mapEmpty,
                      swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i),
                      swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i)
                      );
        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), i + 1);

        for(size_t j = 0; j < s_ct->nrValues; j++) {
            swl_typeData_t* tmpKey = swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, j);
            swl_typeEl_t* tmpValRef = swl_type_arrayGetReference(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);
            swl_typeData_t* tmpValData = swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);

            if(j <= i) {
                swl_typeEl_t* findVal = s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeElEquals(s_ct->valueType, tmpValRef, findVal);

                swl_typeData_t* findValData = s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, findValData);

                swl_mapEntry_t* findKey = s_mapFun->find(s_mapType, s_mapEmpty, tmpValData);
                assert_non_null(findKey);
                swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));
                assert_ptr_equal(s_ct->valueType, s_mapFun->getEntryValType(s_mapType, s_mapEmpty, findKey));

            } else {
                assert_null(s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->find(s_mapType, s_mapEmpty, tmpValData));
            }
        }
    }

    s_mapFun->clear(s_mapType, s_mapEmpty);
    assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), 0);
}


static void test_allocSizeDelete(void** state _UNUSED) {

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), i);
        swl_mapEntry_t* entry = s_mapFun->alloc(s_mapType, s_mapEmpty);

        swl_type_copyTo(s_ct->keyType, s_mapFun->getEntryKeyRef(s_mapType, s_mapEmpty, entry),
                        swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i));
        swl_type_copyTo(s_ct->valueType, s_mapFun->getEntryValRef(s_mapType, s_mapEmpty, entry),
                        swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i));

        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), i + 1);

        for(size_t j = 0; j < s_ct->nrValues; j++) {
            swl_typeData_t* tmpKey = swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, j);
            swl_typeEl_t* tmpValRef = swl_type_arrayGetReference(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);
            swl_typeData_t* tmpValData = swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);

            if(j <= i) {
                swl_typeEl_t* findVal = s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeElEquals(s_ct->valueType, tmpValRef, findVal);

                swl_typeData_t* findValData = s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, findValData);

                swl_mapEntry_t* findKey = s_mapFun->find(s_mapType, s_mapEmpty, tmpValData);
                assert_non_null(findKey);
                swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));
                assert_ptr_equal(s_ct->valueType, s_mapFun->getEntryValType(s_mapType, s_mapEmpty, findKey));

            } else {
                assert_null(s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->find(s_mapType, s_mapEmpty, tmpValData));
            }
        }
    }

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), s_ct->nrValues - i);


        s_mapFun->delete(s_mapType, s_mapEmpty,
                         swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i));
        assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), s_ct->nrValues - i - 1);

        for(size_t j = 0; j < s_ct->nrValues; j++) {
            swl_typeData_t* tmpKey = swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, j);
            swl_typeEl_t* tmpValRef = swl_type_arrayGetReference(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);
            swl_typeData_t* tmpValData = swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, j);

            if(j > i) {
                swl_typeEl_t* findVal = s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeElEquals(s_ct->valueType, tmpValRef, findVal);

                swl_typeData_t* findValData = s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey);
                assert_non_null(findVal);
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, findValData);

                swl_mapEntry_t* findKey = s_mapFun->find(s_mapType, s_mapEmpty, tmpValData);
                assert_non_null(findKey);
                swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
                swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));
                assert_ptr_equal(s_ct->valueType, s_mapFun->getEntryValType(s_mapType, s_mapEmpty, findKey));

            } else {
                assert_null(s_mapFun->getReference(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->getValue(s_mapType, s_mapEmpty, tmpKey));
                assert_null(s_mapFun->find(s_mapType, s_mapEmpty, tmpValData));
            }
        }
    }

    assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), 0);
}

static void test_set(void** state _UNUSED) {
    swl_typeData_t* otherValue = swl_type_arrayGetValue(s_ct->valueType, s_ct->nonUsedValueArray, s_ct->nrUnusedValues, 0);
    swl_mapEntry_t* findKey = NULL;

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        swl_typeData_t* tmpKey = swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i);
        swl_typeData_t* tmpValData = swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i);

        assert_null(s_mapFun->find(s_mapType, s_mapFilled, otherValue));
        findKey = s_mapFun->find(s_mapType, s_mapFilled, tmpValData);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
        swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));


        s_mapFun->set(s_mapType, s_mapFilled, tmpKey, otherValue);
        assert_null(s_mapFun->find(s_mapType, s_mapFilled, tmpValData));
        findKey = s_mapFun->find(s_mapType, s_mapFilled, otherValue);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
        swl_ttb_assertTypeEquals(s_ct->valueType, otherValue, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));

        s_mapFun->set(s_mapType, s_mapFilled, tmpKey, tmpValData);
        findKey = s_mapFun->find(s_mapType, s_mapFilled, tmpValData);
        assert_non_null(findKey);
        swl_ttb_assertTypeEquals(s_ct->keyType, tmpKey, s_mapFun->getEntryKey(s_mapType, s_mapEmpty, findKey));
        swl_ttb_assertTypeEquals(s_ct->valueType, tmpValData, s_mapFun->getEntryVal(s_mapType, s_mapEmpty, findKey));
    }
}


static void test_equals(void** state _UNUSED) {
    swl_mapEl_t* myMap = calloc(1, s_ct->mapType->size);
    s_mapFun->init(s_mapType, myMap);

    assert_true(s_mapFun->equals(s_mapType, myMap, s_mapEmpty));
    assert_false(s_mapFun->equals(s_mapType, myMap, s_mapFilled));

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_mapFun->add(s_mapType, myMap,
                      swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i),
                      swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i)
                      );
        if(i < s_ct->nrValues - 1) {
            assert_false(s_mapFun->equals(s_mapType, myMap, s_mapEmpty));
            assert_false(s_mapFun->equals(s_mapType, myMap, s_mapFilled));
        }
    }

    assert_false(s_mapFun->equals(s_mapType, myMap, s_mapEmpty));
    assert_true(s_mapFun->equals(s_mapType, myMap, s_mapFilled));


    s_mapFun->clear(s_mapType, myMap);

    assert_true(s_mapFun->equals(s_mapType, myMap, s_mapEmpty));
    assert_false(s_mapFun->equals(s_mapType, myMap, s_mapFilled));

    s_mapFun->cleanup(s_mapType, myMap);
    free(myMap);
}

static void test_iterate(void** state _UNUSED) {
    swl_listSTypeIt_t it = s_mapFun->firstIt(s_mapType, s_mapFilled);
    assert_true(it.valid);
    size_t counter = 0;
    while(it.valid) {
        swl_mapEntry_t* entry = it.data;

        swl_ttb_assertTypeEquals(s_ct->keyType, swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, counter)
                                 , s_mapFun->getEntryKey(s_mapType, s_mapFilled, entry));
        swl_ttb_assertTypeEquals(s_ct->valueType, swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, counter),
                                 s_mapFun->getEntryVal(s_mapType, s_mapFilled, entry));
        assert_ptr_equal(s_ct->valueType, s_mapFun->getEntryValType(s_mapType, s_mapFilled, entry));

        counter++;
        s_mapFun->nextIt(s_mapType, &it);
    }
}

static void test_delIt(void** state _UNUSED) {

    swl_mapEl_t* myMap = calloc(1, s_ct->mapType->size);
    s_mapFun->init(s_mapType, myMap);

    for(size_t i = 0; i < s_ct->nrValues; i++) {
        s_mapFun->add(s_mapType, myMap,
                      swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, i),
                      swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, i)
                      );
    }

    swl_listSTypeIt_t it = s_mapFun->firstIt(s_mapType, myMap);
    assert_true(it.valid);
    size_t counter = 0;

    while(it.valid) {
        swl_mapEntry_t* entry = it.data;

        assert_int_equal(s_mapFun->size(s_mapType, myMap), s_ct->nrValues - counter);


        swl_ttb_assertTypeEquals(s_ct->keyType, swl_type_arrayGetValue(s_ct->keyType, s_ct->keyDataArray, s_ct->nrValues, counter)
                                 , s_mapFun->getEntryKey(s_mapType, myMap, entry));
        swl_ttb_assertTypeEquals(s_ct->valueType, swl_type_arrayGetValue(s_ct->valueType, s_ct->valueDataArray, s_ct->nrValues, counter),
                                 s_mapFun->getEntryVal(s_mapType, myMap, entry));
        assert_ptr_equal(s_ct->valueType, s_mapFun->getEntryValType(s_mapType, myMap, entry));

        s_mapFun->delIt(s_mapType, &it);

        assert_int_equal(s_mapFun->size(s_mapType, myMap), s_ct->nrValues - counter - 1);


        counter++;
        s_mapFun->nextIt(s_mapType, &it);
    }
    assert_int_equal(s_mapFun->size(s_mapType, s_mapEmpty), 0);
    s_mapFun->cleanup(s_mapType, myMap);
    free(myMap);
}



static void test_metaData(void** state _UNUSED) {
    assert_ptr_equal(s_mapFun->getKeyType(s_mapType, s_mapEmpty), s_ct->keyType);
    assert_ptr_equal(s_mapFun->getValueType(s_mapType, s_mapEmpty, NULL), s_ct->valueType);
    assert_int_equal(s_mapFun->maxSize(s_mapType, s_mapEmpty), s_ct->maxSize);
}

int testCommon_swl_mapSType_singleValueType(char* name, swl_mapSType_singleValueTest_t* tut) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_initDestroy),
        cmocka_unit_test(test_addSizeClear),
        cmocka_unit_test(test_allocSizeDelete),
        cmocka_unit_test(test_metaData),
        cmocka_unit_test(test_set),
        cmocka_unit_test(test_equals),
        cmocka_unit_test(test_iterate),
        cmocka_unit_test(test_delIt),
    };

    s_ct = tut;

    printf("\n\n Test collection : -%s- \n", name);
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    ttb_assert_clearPrint();
    sahTraceClose();
    return rc;
}
