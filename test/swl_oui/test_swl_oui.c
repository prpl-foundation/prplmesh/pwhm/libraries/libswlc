/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_oui.h"


#include "test_swlc_type_testCommon.h"
#define NR_OUI_TEST_VALUES 6
swl_oui_t oui_base[NR_OUI_TEST_VALUES] = {{{00, 01, 03}}, {{0xab, 0xce, 0xff}}, {{07, 0x1a, 0x2b}},
    {{0xa, 0xb, 0xc}}, {{0xd, 0xe, 0xf}}, {{0xab, 0xce, 0xff}}};
size_t oui_count[NR_OUI_TEST_VALUES] = {1, 2, 1, 1, 1, 2};
swl_oui_t oui_notContains[NR_OUI_TEST_VALUES] = {{{1, 2, 3}}, {{0, 1, 0}}, {{255, 255, 255}},
    {{0, 0, 0}}, {{0xaa, 0xaa, 0xaa}}, {{0xbb, 0xbb, 0xbb}}};

const char* testStrValuesOui[NR_OUI_TEST_VALUES] = {
    "00:01:03", "AB:CE:FF", "07:1A:2B",
    "0A:0B:0C", "0D:0E:0F", "AB:CE:FF"
};

swlc_type_testCommonData_t ouiTestData = {
    .name = "oui",
    .testType = swl_type_oui,
    .refType = &gtSwl_type_ouiPtr.type,
    .serialStr = "00:01:03,AB:CE:FF,07:1A:2B,0A:0B:0C,0D:0E:0F,AB:CE:FF",
    .serialStr2 = "00:01:03;;AB:CE:FF;;07:1A:2B;;0A:0B:0C;;0D:0E:0F;;AB:CE:FF",
    .strValues = testStrValuesOui,
    .baseValues = &oui_base,
    .valueCount = oui_count,
    .notContains = &oui_notContains,
    .nrTestValues = NR_OUI_TEST_VALUES,
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);

    runCommonTypeTest(&ouiTestData);

    sahTraceClose();
    return rc;
}




