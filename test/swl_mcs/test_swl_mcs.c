/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"

#include "swl/swl_common_mcs.h"
#include "swl/swl_string.h"
#include "test-toolbox/ttb.h"

#include "test_swlc_type_testCommon.h"

// Format: standard_mcsndex_bandwidth_antenna_interval

// NOTE: swl_mcs_checkMcsIndexes may overwrite bw and sgi of the checked structure.
// Work with structure copies when using the same structure multiple times in the tests.

void test_swl_mcs_equals(void** state _UNUSED) {
    // Valgrind forces this kind of initialization (memcmp in swl_type_equals)
    swl_mcs_t mcsIn_1 = {0};
    swl_mcs_t mcsIn_2 = {0};
    swl_mcs_t mcsIn_3 = {0};

    mcsIn_1.standard = SWL_MCS_STANDARD_LEGACY;
    mcsIn_1.mcsIndex = 2;
    mcsIn_1.bandwidth = SWL_BW_AUTO;
    mcsIn_1.numberOfSpatialStream = 1;
    mcsIn_1.guardInterval = SWL_SGI_800;

    mcsIn_2.standard = SWL_MCS_STANDARD_LEGACY;
    mcsIn_2.mcsIndex = 2;
    mcsIn_2.bandwidth = SWL_BW_AUTO;
    mcsIn_2.numberOfSpatialStream = 1;
    mcsIn_2.guardInterval = SWL_SGI_800;

    mcsIn_3.standard = SWL_MCS_STANDARD_HT;
    mcsIn_3.mcsIndex = 3;
    mcsIn_3.bandwidth = SWL_BW_80MHZ;
    mcsIn_3.numberOfSpatialStream = 1;
    mcsIn_3.guardInterval = SWL_SGI_400;

    assert_true(swl_type_equals(swl_type_mcs, &mcsIn_1, &mcsIn_2));
    assert_false(swl_type_equals(swl_type_mcs, &mcsIn_1, &mcsIn_3));
    assert_false(swl_type_equals(swl_type_mcs, &mcsIn_2, &mcsIn_3));
}

void test_swl_mcs_mcsCopy(void** state _UNUSED) {

    swl_mcs_t source = { 0 };
    source.standard = SWL_MCS_STANDARD_HT;
    source.mcsIndex = 3;
    source.bandwidth = SWL_BW_80MHZ;
    source.numberOfSpatialStream = 1;
    source.guardInterval = SWL_SGI_400;

    swl_mcs_t* mcsCopy = NULL;
    mcsCopy = swl_type_copy(swl_type_mcs, &source);
    assert_true(swl_type_equals(swl_type_mcs, mcsCopy, &source));
    free(mcsCopy);
}

void test_swl_mcs_mcsToCharUnknown(void** state _UNUSED) {

    swl_mcs_t mcs_toCharUnknownStandard[] = {
        { SWL_MCS_STANDARD_UNKNOWN, 2, SWL_BW_AUTO, 1, SWL_SGI_800, },
        { SWL_MCS_STANDARD_UNKNOWN, 2, SWL_BW_20MHZ, 1, SWL_SGI_AUTO, },
    };

    char* result = "unk_0_0_0_0";
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_toCharUnknownStandard); i++) {

        char buffer[32] = {0};
        assert_int_equal(swl_mcs_toChar(buffer, sizeof(buffer), &mcs_toCharUnknownStandard[i]), strlen(result));
        assert_true(swl_str_matches(buffer, "unk_0_0_0_0"));
    }
}

// swl_guardinterval_e tests
// unknown: only SWL_SGI_AUTO (0)
// Legacy: only SWL_SGI_AUTO (0)
// ht/vht: 400 and 800
// he: 800, 1600, 3200

// Legacy can not have any sgi value but 0
swl_mcs_t mcs_sgiLegacyZero[] = {
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_20MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_40MHZ, 1, SWL_SGI_800, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_80MHZ, 1, SWL_SGI_1600, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_160MHZ, 1, SWL_SGI_3200, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_160MHZ, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_LEGACY, 2, 0, 1, SWL_SGI_MAX, },
};

swl_mcs_t mcs_sgiAuto[] = {
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_20MHZ, 1, SWL_SGI_AUTO, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_40MHZ, 1, SWL_SGI_AUTO, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_80MHZ, 1, SWL_SGI_AUTO, },
};

// Not applicable standard/sgi combinations
swl_mcs_t mcs_badSgi[] = {
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_160MHZ, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_80MHZ, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_20MHZ, 1, SWL_SGI_MAX, },

    { SWL_MCS_STANDARD_HT, 2, SWL_BW_40MHZ, 1, SWL_SGI_1600, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_80MHZ, 1, SWL_SGI_3200, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_80MHZ, 1, SWL_SGI_MAX, },

    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_40MHZ, 1, SWL_SGI_1600, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_80MHZ, 1, SWL_SGI_3200, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_80MHZ, 1, SWL_SGI_MAX, },

    { SWL_MCS_STANDARD_HE, 2, SWL_BW_80MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_80MHZ, 1, SWL_SGI_MAX, },
};

// Applicable standard/sgi combinations
swl_mcs_t mcs_sgiApplicable[] = {
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_20MHZ, 1, SWL_SGI_AUTO, },

    { SWL_MCS_STANDARD_HT, 2, SWL_BW_20MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_20MHZ, 1, SWL_SGI_800, },

    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_20MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_20MHZ, 1, SWL_SGI_800, },

    { SWL_MCS_STANDARD_HE, 2, SWL_BW_20MHZ, 1, SWL_SGI_800, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_20MHZ, 1, SWL_SGI_1600, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_20MHZ, 1, SWL_SGI_3200, },
};

swl_mcs_t mcs_shouldNotHappen[] = {
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_MAX, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_VHT, 2, 10, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_MAX, 1, SWL_SGI_MAX, },

    { SWL_MCS_STANDARD_HE, 2, SWL_BW_MAX, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_HE, 2, 10, 1, 10, },

    { SWL_MCS_STANDARD_HT, 2, SWL_BW_MAX, 1, SWL_SGI_MAX, },
    { SWL_MCS_STANDARD_HT, 2, 10, 1, 10, },
};

// bandwidth tests
// unknown: only SWL_BW_AUTO (0)
// legacy, ht, vht and he: 0, 20,40,80,160
// he: 10, 5, 2 in addition
swl_mcs_t mcs_badBandWidth[] = {
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_10MHZ, 1, SWL_SGI_AUTO, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_5MHZ, 1, SWL_SGI_AUTO, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_2MHZ, 1, SWL_SGI_AUTO, },

    { SWL_MCS_STANDARD_HT, 2, SWL_BW_10MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_5MHZ, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_2MHZ, 1, SWL_SGI_400, },

    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_10MHZ, 1, SWL_SGI_800, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_5MHZ, 1, SWL_SGI_800, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_2MHZ, 1, SWL_SGI_800, },
};

swl_mcs_t mcs_bandWidth[] = {
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_AUTO, 1, SWL_SGI_AUTO, },
    { SWL_MCS_STANDARD_LEGACY, 2, SWL_BW_160MHZ, 1, SWL_SGI_AUTO, },

    { SWL_MCS_STANDARD_HT, 2, SWL_BW_AUTO, 1, SWL_SGI_400, },
    { SWL_MCS_STANDARD_HT, 2, SWL_BW_160MHZ, 1, SWL_SGI_400, },

    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_AUTO, 1, SWL_SGI_800, },
    { SWL_MCS_STANDARD_VHT, 2, SWL_BW_160MHZ, 1, SWL_SGI_800, },

    { SWL_MCS_STANDARD_HE, 2, SWL_BW_80MHZ, 1, SWL_SGI_3200, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_10MHZ, 1, SWL_SGI_3200, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_5MHZ, 1, SWL_SGI_1600, },
    { SWL_MCS_STANDARD_HE, 2, SWL_BW_2MHZ, 1, SWL_SGI_800, },
};

// work with copy of the structure as index checking may change the structure's bw and sgi
void test_swl_mcs_checkMcsIndexes(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_badSgi); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_badSgi[i]);
        assert_false(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiAuto); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiAuto[i]);
        assert_true(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiLegacyZero); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiLegacyZero[i]);
        assert_false(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiApplicable); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiApplicable[i]);
        assert_true(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_shouldNotHappen); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_shouldNotHappen[i]);
        assert_false(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_badBandWidth); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_badBandWidth[i]);
        assert_false(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_bandWidth); i++) {
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_bandWidth[i]);
        assert_true(swl_mcs_checkMcsIndexes(mcsCopyOrig));
        free(mcsCopyOrig);
    }
}

// Serialize structure to string, then string back to structure and compare in and out.
// work with copy of the structure...
void test_swl_mcs_mcsToCharToMcs(void** state _UNUSED) {

    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiApplicable); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiApplicable[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_true(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_sgiApplicable[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_badSgi); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_badSgi[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_false(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_badSgi[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiAuto); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiAuto[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_true(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_sgiAuto[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    // Bad SGI values reset for sure ...
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_sgiLegacyZero); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_sgiLegacyZero[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_false(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_sgiLegacyZero[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    // Good values
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_bandWidth); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_bandWidth[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_true(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_bandWidth[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_shouldNotHappen); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_shouldNotHappen[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        assert_false(swl_type_equals(swl_type_mcs, &mcsOut, &mcs_shouldNotHappen[i]));
        assert_false(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcs_shouldNotHappen[i]));
        assert_true(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcsOut));
        free(mcsCopyOrig);
    }
    // Bad bw values, reset...
    for(size_t i = 0; i < SWL_ARRAY_SIZE(mcs_badBandWidth); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsOut = {0};
        swl_mcs_t* mcsCopyOrig = swl_type_copy(swl_type_mcs, &mcs_badBandWidth[i]);
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), mcsCopyOrig));
        assert_false(swl_type_equals(swl_type_mcs, mcsCopyOrig, &mcs_badBandWidth[i]));
        assert_true(swl_mcs_fromChar(&mcsOut, buffer));
        free(mcsCopyOrig);
    }
}

// Data fully conform with spec.
const char* dataSet[] = {
    "leg_1_80_2_0",
    "ht_1_20_4_800",
    "ht_1_160_3_400",
    "ht_1_80_4_800",
    "vht_1_80_4_400",
    "vht_3_20_4_800",
    "he_1_2_4_800",
    "he_4_5_4_800",
    "he_4_10_4_800",
    "he_2_20_4_800",
    "he_6_80_4_1600",
    "he_7_160_4_3200",
};

const char* badDataSet[] = {
    "leggge_1_80___",
    "ht_1_204_800_",
    "_ht_19=4_801_",
    "ht_19=4__801_",
    "_ht_1_9_4_1800",
    "____400",
    "1_80__4_800",
    "ht_1_9-_4_801",
    "ht_1M_94__801",
    "ht_1M_9_4_8]01",
    "ht_1M_9_4_8]01ht_1M_9_4_8]01ht_1M_9_4_8]01ht_1M_9_4_8]01ht_1M_9_4_8]01ht_1M_9_4_8]01",
};

void test_swl_mcs_checkCharSerialized(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(dataSet); i++) {
        assert_true(swl_mcs_checkCharSerialized(dataSet[i]));
    }
    for(size_t i = 0; i < SWL_ARRAY_SIZE(badDataSet); i++) {
        assert_false(swl_mcs_checkCharSerialized(badDataSet[i]));
    }
}

// String data into mcs and back into string...
void test_swl_mcs_charToMcsToChar(void** state _UNUSED) {
    // good char data input
    for(size_t i = 0; i < SWL_ARRAY_SIZE(dataSet); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsTarget = {0};
        assert_true(swl_mcs_fromChar(&mcsTarget, (char*) dataSet[i]));
        assert_true(swl_mcs_toChar(buffer, sizeof(buffer), &mcsTarget));
        assert_true(swl_str_matches((char*) dataSet[i], buffer));
    }
    // bad char data input shall not produce valid structures
    for(size_t i = 0; i < SWL_ARRAY_SIZE(badDataSet); i++) {
        char buffer[32] = {0};
        swl_mcs_t mcsTarget = {0};
        assert_false(swl_mcs_fromChar(&mcsTarget, (char*) &badDataSet[i][0]));
        swl_mcs_toChar(buffer, sizeof(buffer), &mcsTarget);
        assert_true(swl_str_matches(buffer, "unk_0_0_0_0"));
    }
}


typedef struct {
    uint8_t mcsIndex;
    swl_bandwidth_e bandwidth;
    uint8_t numberOfSpatialStream;
    swl_guardinterval_e guardInterval;
    uint32_t rate;
    swl_mcsStandard_e standard;
} swlTest_t;

swlTest_t testMcsToRate[] = {
    { 1, SWL_BW_20MHZ, 1, SWL_SGI_800, 13000, SWL_MCS_STANDARD_HT},
    { 2, SWL_BW_40MHZ, 2, SWL_SGI_400, 90000, SWL_MCS_STANDARD_HT },
    { 3, SWL_BW_80MHZ, 3, SWL_SGI_800, 351000, SWL_MCS_STANDARD_HT},
    { 4, SWL_BW_160MHZ, 4, SWL_SGI_400, 1560000, SWL_MCS_STANDARD_HT },
    { 5, SWL_BW_80MHZ, 3, SWL_SGI_800, 702000, SWL_MCS_STANDARD_HT},
    { 6, SWL_BW_40MHZ, 2, SWL_SGI_400, 270000, SWL_MCS_STANDARD_HT },
    { 7, SWL_BW_20MHZ, 1, SWL_SGI_800, 65000, SWL_MCS_STANDARD_HT },
    { 8, SWL_BW_40MHZ, 2, SWL_SGI_400, 360000, SWL_MCS_STANDARD_HT},
    { 9, SWL_BW_80MHZ, 3, SWL_SGI_800, 1170000, SWL_MCS_STANDARD_HT},
    { 0, SWL_BW_AUTO, 4, SWL_SGI_400, 0, SWL_MCS_STANDARD_HT},

    { 1, SWL_BW_20MHZ, 1, SWL_SGI_800, 17205, SWL_MCS_STANDARD_HE},
    { 2, SWL_BW_40MHZ, 2, SWL_SGI_400, 106363, SWL_MCS_STANDARD_HE },
    { 3, SWL_BW_80MHZ, 3, SWL_SGI_800, 432352, SWL_MCS_STANDARD_HE},
    { 4, SWL_BW_160MHZ, 4, SWL_SGI_400, 1781818, SWL_MCS_STANDARD_HE },
    { 5, SWL_BW_80MHZ, 3, SWL_SGI_800, 864705, SWL_MCS_STANDARD_HE},
    { 6, SWL_BW_40MHZ, 2, SWL_SGI_400, 319090, SWL_MCS_STANDARD_HE },
    { 7, SWL_BW_20MHZ, 1, SWL_SGI_800, 86029, SWL_MCS_STANDARD_HE },
    { 8, SWL_BW_40MHZ, 2, SWL_SGI_400, 425454, SWL_MCS_STANDARD_HE},
    { 9, SWL_BW_80MHZ, 3, SWL_SGI_800, 1441176, SWL_MCS_STANDARD_HE},

    { 11, SWL_BW_80MHZ, 1, SWL_SGI_800, 600441, SWL_MCS_STANDARD_HE},
    { 13, SWL_BW_80MHZ, 1, SWL_SGI_800, 720588, SWL_MCS_STANDARD_EHT },
    { 15, SWL_BW_80MHZ, 1, SWL_SGI_800, 18014, SWL_MCS_STANDARD_EHT },
    { 15, SWL_BW_40MHZ, 1, SWL_SGI_800, 8602, SWL_MCS_STANDARD_EHT },
    { 15, SWL_BW_40MHZ, 1, SWL_SGI_3200, 7312, SWL_MCS_STANDARD_EHT },
    { 15, SWL_BW_80MHZ, 1, SWL_SGI_800, 18014, SWL_MCS_STANDARD_EHT },
    { 15, SWL_BW_80MHZ, 1, SWL_SGI_3200, 15312, SWL_MCS_STANDARD_EHT },
    { 14, SWL_BW_40MHZ, 1, SWL_SGI_800, 4301, SWL_MCS_STANDARD_EHT },
    { 14, SWL_BW_40MHZ, 1, SWL_SGI_3200, 3656, SWL_MCS_STANDARD_EHT },
    { 14, SWL_BW_80MHZ, 1, SWL_SGI_800, 9007, SWL_MCS_STANDARD_EHT },
    { 14, SWL_BW_80MHZ, 1, SWL_SGI_3200, 7656, SWL_MCS_STANDARD_EHT },
};

void test_swl_mcs(void** state _UNUSED) {
    swl_mcs_t testmcs;
    memset(&testmcs, 0, sizeof(swl_mcs_t));
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testMcsToRate); i++) {
        testmcs.mcsIndex = testMcsToRate[i].mcsIndex;
        testmcs.bandwidth = testMcsToRate[i].bandwidth;
        testmcs.numberOfSpatialStream = testMcsToRate[i].numberOfSpatialStream;
        testmcs.guardInterval = testMcsToRate[i].guardInterval;
        testmcs.standard = testMcsToRate[i].standard;
        ttb_assert_int_eq(swl_mcs_toRate(&testmcs), testMcsToRate[i].rate);
    }
}

void test_swl_mcs_mcsGiToDuration(void** state _UNUSED) {
    struct {
        swl_guardinterval_e giEnu;
        uint32_t giDuration;
    } testGiDurTable[] = {
        {SWL_SGI_400, 400, },
        {SWL_SGI_800, 800, },
        {SWL_SGI_1600, 1600, },
        {SWL_SGI_3200, 3200, },
        {SWL_SGI_AUTO, 0, },
        {SWL_SGI_MAX, 0, },
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testGiDurTable); i++) {
        assert_int_equal(swl_mcs_guardIntervalToInt(testGiDurTable[i].giEnu), testGiDurTable[i].giDuration);
    }
}

void test_swl_mcs_mcsStdToFromRadStd(void** state _UNUSED) {
    struct {
        swl_mcsStandard_e mcsStd;
        swl_freqBandExt_e freqBandExt;
        swl_radStd_e radStd;
    } testMcsStdTable[] = {
        /* success cases */
        {SWL_MCS_STANDARD_HT, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_HT, SWL_FREQ_BAND_EXT_5GHZ, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_VHT, SWL_FREQ_BAND_EXT_5GHZ, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HE, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_HE, SWL_FREQ_BAND_EXT_5GHZ, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_HE, SWL_FREQ_BAND_EXT_6GHZ, SWL_RADSTD_AX, },
        /* error cases */
        /* 1) legacy rad std (wt freqBand) overrides invalid mcs std conversion. */
        {SWL_MCS_STANDARD_UNKNOWN, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_LEGACY, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_VHT, SWL_FREQ_BAND_EXT_2_4GHZ, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_FREQ_BAND_EXT_5GHZ, SWL_RADSTD_A, },
        {SWL_MCS_STANDARD_LEGACY, SWL_FREQ_BAND_EXT_5GHZ, SWL_RADSTD_A, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_FREQ_BAND_EXT_6GHZ, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_LEGACY, SWL_FREQ_BAND_EXT_6GHZ, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_HT, SWL_FREQ_BAND_EXT_6GHZ, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_VHT, SWL_FREQ_BAND_EXT_6GHZ, SWL_RADSTD_AX, },
        /* 2) mcs std always converted when frequency band is undefined. */
        {SWL_MCS_STANDARD_HT, SWL_FREQ_BAND_EXT_AUTO, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_HT, SWL_FREQ_BAND_EXT_NONE, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_VHT, SWL_FREQ_BAND_EXT_AUTO, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_VHT, SWL_FREQ_BAND_EXT_NONE, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HE, SWL_FREQ_BAND_EXT_AUTO, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_HE, SWL_FREQ_BAND_EXT_NONE, SWL_RADSTD_AX, },
        /* 3) Auto rad std when not possible to do any mapping */
        {SWL_MCS_STANDARD_UNKNOWN, SWL_FREQ_BAND_EXT_NONE, SWL_RADSTD_AUTO, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_FREQ_BAND_EXT_AUTO, SWL_RADSTD_AUTO, },
        {SWL_MCS_STANDARD_LEGACY, SWL_FREQ_BAND_EXT_NONE, SWL_RADSTD_AUTO, },
        {SWL_MCS_STANDARD_LEGACY, SWL_FREQ_BAND_EXT_AUTO, SWL_RADSTD_AUTO, },
    };
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testMcsStdTable); i++) {
        swl_radStd_e radStd = swl_mcs_radStdFromMcsStd(testMcsStdTable[i].mcsStd, testMcsStdTable[i].freqBandExt);
        assert_int_equal(radStd, testMcsStdTable[i].radStd);
    }

    struct {
        swl_mcsStandard_e mcsStd;
        swl_radStd_e radStd;
    } testFalseMcsRadStdTable[] = {
        {SWL_MCS_STANDARD_UNKNOWN, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_RADSTD_G, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_UNKNOWN, SWL_RADSTD_AC },
        {SWL_MCS_STANDARD_HT, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_HT, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HT, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_VHT, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_VHT, SWL_RADSTD_AX, },
        {SWL_MCS_STANDARD_HE, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HE, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_HE, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HE, SWL_RADSTD_AUTO, },
    };

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testFalseMcsRadStdTable); i++) {
        assert_false(swl_mcs_matchesRadStandard(testFalseMcsRadStdTable[i].mcsStd, testFalseMcsRadStdTable[i].radStd));
    }

    struct {
        swl_mcsStandard_e mcsStd;
        swl_radStd_e radStd;
    } testTrueMcsRadStdTable[] = {
        {SWL_MCS_STANDARD_UNKNOWN, SWL_RADSTD_AUTO },
        {SWL_MCS_STANDARD_LEGACY, SWL_RADSTD_A, },
        {SWL_MCS_STANDARD_LEGACY, SWL_RADSTD_B, },
        {SWL_MCS_STANDARD_LEGACY, SWL_RADSTD_G, },
        {SWL_MCS_STANDARD_HT, SWL_RADSTD_N, },
        {SWL_MCS_STANDARD_VHT, SWL_RADSTD_AC, },
        {SWL_MCS_STANDARD_HE, SWL_RADSTD_AX, },
    };

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testTrueMcsRadStdTable); i++) {
        assert_true(swl_mcs_matchesRadStandard(testTrueMcsRadStdTable[i].mcsStd, testTrueMcsRadStdTable[i].radStd));
        swl_mcsStandard_e mcsStd = swl_mcs_mcsStdFromRadStd(testTrueMcsRadStdTable[i].radStd);
        assert_int_equal(mcsStd, testTrueMcsRadStdTable[i].mcsStd);
    }
}

#define NR_MCS_TEST_VAL 4

swl_mcs_t mcsBase[NR_MCS_TEST_VAL] = {
    {.standard = SWL_MCS_STANDARD_HT, .mcsIndex = 1, .bandwidth = SWL_BW_40MHZ, .numberOfSpatialStream = 2, .guardInterval = SWL_SGI_800},
    {.standard = SWL_MCS_STANDARD_HE, .mcsIndex = 7, .bandwidth = SWL_BW_160MHZ, .numberOfSpatialStream = 4, .guardInterval = SWL_SGI_3200},
    {.standard = SWL_MCS_STANDARD_VHT, .mcsIndex = 3, .bandwidth = SWL_BW_20MHZ, .numberOfSpatialStream = 4, .guardInterval = SWL_SGI_800},
    {.standard = SWL_MCS_STANDARD_LEGACY, .mcsIndex = 1, .bandwidth = SWL_BW_80MHZ, .numberOfSpatialStream = 2, .guardInterval = SWL_SGI_AUTO}
};

size_t mcsCount[NR_MCS_TEST_VAL] = {1, 1, 1, 1};

swl_mcs_t mcsNotContains[NR_MCS_TEST_VAL] = {
    {.standard = SWL_MCS_STANDARD_HT, .mcsIndex = 2, .bandwidth = SWL_BW_40MHZ, .numberOfSpatialStream = 2, .guardInterval = SWL_SGI_800},
    {.standard = SWL_MCS_STANDARD_VHT, .mcsIndex = 7, .bandwidth = SWL_BW_80MHZ, .numberOfSpatialStream = 4, .guardInterval = SWL_SGI_800},
    {.standard = SWL_MCS_STANDARD_VHT, .mcsIndex = 3, .bandwidth = SWL_BW_20MHZ, .numberOfSpatialStream = 4, .guardInterval = SWL_SGI_400},
    {.standard = SWL_MCS_STANDARD_LEGACY, .mcsIndex = 1, .bandwidth = SWL_BW_160MHZ, .numberOfSpatialStream = 1, .guardInterval = SWL_SGI_AUTO}
};

const char* mcsStrVal[NR_MCS_TEST_VAL] = { "ht_1_40_2_800", "he_7_160_4_3200", "vht_3_20_4_800", "leg_1_80_2_0"};



swlc_type_testCommonData_t testMcsData = {
    .name = "mcs",
    .testType = swl_type_mcs,
    .serialStr = "ht_1_40_2_800,he_7_160_4_3200,vht_3_20_4_800,leg_1_80_2_0",
    .serialStr2 = "ht_1_40_2_800;;he_7_160_4_3200;;vht_3_20_4_800;;leg_1_80_2_0",
    .strValues = mcsStrVal,
    .baseValues = &mcsBase,
    .valueCount = mcsCount,
    .notContains = &mcsNotContains,
    .nrTestValues = NR_MCS_TEST_VAL,
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mcs),
        cmocka_unit_test(test_swl_mcs_equals),
        cmocka_unit_test(test_swl_mcs_mcsCopy),
        cmocka_unit_test(test_swl_mcs_checkMcsIndexes),
        cmocka_unit_test(test_swl_mcs_mcsToCharUnknown),
        cmocka_unit_test(test_swl_mcs_mcsToCharToMcs),
        cmocka_unit_test(test_swl_mcs_charToMcsToChar),
        cmocka_unit_test(test_swl_mcs_checkCharSerialized),
        cmocka_unit_test(test_swl_mcs_mcsGiToDuration),
        cmocka_unit_test(test_swl_mcs_mcsStdToFromRadStd),
    };
    ttb_util_setFilter();

    int rc = cmocka_run_group_tests(tests, NULL, NULL);

    runCommonTypeTest(&testMcsData);

    sahTraceClose();
    return rc;
}




