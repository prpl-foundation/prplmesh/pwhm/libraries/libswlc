/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/ttb/swl_ttb.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/swl_common_tupleType.h"

#define NR_TYPES 6
#define NR_VALUES 4


#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int64, index) \
    X(Y, gtSwl_type_charPtr, val)

SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )

SWL_ARRAY_BUF_TYPE(tMyTestArrayBufType, tMyTestTupleType, 4, false);

/**
 * Defining a tuple type using a table
 */
swl_myTestTT_t myTestValues[NR_VALUES] = {
    {1, 100, "test1"},
    {2, -200, "test2"},
    {-88, 289870, "foobar"},
    {88, -289869, "barfoo"},
};

const char* strData[NR_VALUES] = {
    "[1,100,test1]",
    "[2,-200,test2]",
    "[-88,289870,foobar]",
    "[88,-289869,barfoo]"
};


tMyTestArrayBufType_type testData[] = {
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4 },
    {.data = {
            {2, 100, "test1"}, // 2 changed here
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -202, "test2"}, // -202 changed here
            {-88, 289870, "foobar"},
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {2, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobars"}, // fobars changed here
            {88, -289869, "barfoo"},
        }, .size = 4},
    {.data = {
            {1, 100, "test1"},
            {2, -200, "test2"},
            {-88, 289870, "foobar"},
            {0, 0, NULL},
        }, .size = 3},
    {.data = {
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
            {0, 0, NULL},
        }, .size = 0},
};



static void test_swl_ttb_val(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(myTestValues); i++) {
        swl_myTestTT_t tmpVal;
        memset(&tmpVal, 0, sizeof(swl_myTestTT_t));
        swl_type_copyTo(&tMyTestTupleType.type, &tmpVal, &myTestValues[i]);

        for(size_t j = 0; j < SWL_ARRAY_SIZE(myTestValues); j++) {
            if(i == j) {
                swl_ttb_assertTypeEquals(&tMyTestTupleType.type, &myTestValues[j], &tmpVal);
                swl_ttb_assertBigTypeEquals(&tMyTestTupleType.type, &myTestValues[j], &tmpVal);

                swl_ttb_assertTypeValEquals(&tMyTestTupleType.type, myTestValues[j], tmpVal);
                swl_ttb_assertBigTypeValEquals(&tMyTestTupleType.type, myTestValues[j], tmpVal);

                swl_ttb_assertTypeBuf32StrMatches(&tMyTestTupleType.type, &myTestValues[j], strData[i]);
                swl_ttb_assertTypeValBuf32StrMatches(&tMyTestTupleType.type, myTestValues[j], strData[i]);
            } else {
                swl_ttb_assertTypeNotEquals(&tMyTestTupleType.type, &myTestValues[j], &tmpVal);
                swl_ttb_assertBigTypeNotEquals(&tMyTestTupleType.type, &myTestValues[j], &tmpVal);

                swl_ttb_assertTypeValNotEquals(&tMyTestTupleType.type, myTestValues[j], tmpVal);
                swl_ttb_assertBigTypeValNotEquals(&tMyTestTupleType.type, myTestValues[j], tmpVal);

                swl_ttb_assertTypeBuf32StrNotMatches(&tMyTestTupleType.type, &myTestValues[j], strData[i]);
                swl_ttb_assertTypeValBuf32StrNotMatches(&tMyTestTupleType.type, myTestValues[j], strData[i]);
            }
            swl_ttb_assertTypeNotEmpty(&tMyTestTupleType.type, &tmpVal);
            swl_ttb_assertTypeValNotEmpty(&tMyTestTupleType.type, tmpVal);
        }
        swl_type_cleanup(&tMyTestTupleType.type, &tmpVal);
    }

    swl_myTestTT_t tmpVal;
    memset(&tmpVal, 0, sizeof(swl_myTestTT_t));
    swl_ttb_assertTypeEmpty(&tMyTestTupleType.type, &tmpVal);
    swl_ttb_assertTypeValEmpty(&tMyTestTupleType.type, tmpVal);
}

static void test_swl_ttb_array(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        tMyTestArrayBufType_type tmpVal;
        memset(&tmpVal, 0, sizeof(tMyTestArrayBufType_type));
        swl_type_copyTo(&tMyTestArrayBufType.type.type, &tmpVal, &testData[i]);

        printf("%zu %s\n", i, swl_type_toBuf128(&tMyTestArrayBufType.type.type, &tmpVal).buf);
        for(size_t j = 0; j < SWL_ARRAY_SIZE(myTestValues); j++) {
            if(i == j) {
                swl_ttb_assertTypeArrayEquals(&tMyTestTupleType.type, testData[j].data, testData[j].size, tmpVal.data, tmpVal.size);
            } else {
                swl_ttb_assertTypeArrayNotEquals(&tMyTestTupleType.type, testData[j].data, testData[j].size, tmpVal.data, tmpVal.size);
            }
        }

        swl_type_cleanup(&tMyTestArrayBufType.type.type, &tmpVal);
    }
}

static void test_swl_ttb_file(void** state _UNUSED) {
    swl_ttb_assertFileEquals("testfile_0_0.txt", "testfile_0_1.txt");

    for(size_t i = 0; i < SWL_ARRAY_SIZE(myTestValues); i++) {
        char buffer[128];
        snprintf(buffer, sizeof(buffer), "typeTestPrint/file_%zu.txt", i);
        swl_ttb_assertTypeToFileEquals(&tMyTestTupleType.type, buffer, &myTestValues[i], NULL, false);
    }
}


static int setup_suite(void** state _UNUSED) {
    printf("TYPE INIT %p\n", &tMyTestTupleType);
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testData); i++) {
        testData[i].maxSize = 4;
        testData[i].type = (swl_type_t*) &tMyTestTupleType;
    }
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_ttb_val),
        cmocka_unit_test(test_swl_ttb_array),
        cmocka_unit_test(test_swl_ttb_file),
    };


    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);


    sahTraceClose();
    return rc;
}
