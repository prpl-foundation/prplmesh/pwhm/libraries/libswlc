/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common_table.h"
#include "swl/swl_security.h"


// Stuctures
typedef struct {
    swl_security_apMode_e mode;
    swl_80211_cipher_m cipherSuiteParams;
    swl_80211_akm_m akmSuiteParams;

} secModTest_t;


static int setup_suite(void** state) {
    (void) state;
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}

SWL_TABLE(testApModeTable,
          ARR(char* val; swl_security_apMode_e mode; swl_security_apModeFmt_e format; ),
          ARR(swl_type_charPtr, swl_type_uint32, swl_type_uint32),
          ARR(
              { "None", SWL_SECURITY_APMODE_NONE, SWL_SECURITY_APMODEFMT_DEFAULT},
              { "WPA2-Personal", SWL_SECURITY_APMODE_WPA2_P, SWL_SECURITY_APMODEFMT_DEFAULT},
              { "WPA3-Personal", SWL_SECURITY_APMODE_WPA3_P, SWL_SECURITY_APMODEFMT_DEFAULT},
              { "WPA-WPA2-Personal", SWL_SECURITY_APMODE_WPA_WPA2_P, SWL_SECURITY_APMODEFMT_DEFAULT},
              {"WPA2-PSK-WPA3-SAE", SWL_SECURITY_APMODE_WPA2_WPA3_P, SWL_SECURITY_APMODEFMT_ALTERNATE},
              { "WPA2-WPA3-Personal", SWL_SECURITY_APMODE_WPA2_WPA3_P, SWL_SECURITY_APMODEFMT_LEGACY},
              {"WPA3-SAE", SWL_SECURITY_APMODE_WPA3_P, SWL_SECURITY_APMODEFMT_ALTERNATE}
              )
          );


static void test_swl_security_toFromString(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testApModeTableValues); i++) {
        assert_int_equal(swl_security_apModeFromString(testApModeTableValues[i].val), testApModeTableValues[i].mode);

        assert_string_equal(swl_security_apModeToString(testApModeTableValues[i].mode, testApModeTableValues[i].format), testApModeTableValues[i].val);
        if(testApModeTableValues[i].format != SWL_SECURITY_APMODEFMT_DEFAULT) {
            assert_string_not_equal(swl_security_apModeToString(testApModeTableValues[i].mode, SWL_SECURITY_APMODEFMT_DEFAULT), testApModeTableValues[i].val);
        }
    }
}

typedef struct {
    swl_security_apMode_e apMode; swl_security_conMode_m staMode; bool supports;
} test_staMatchesAp_t;

test_staMatchesAp_t testMatchVals[] = {
    {SWL_SECURITY_APMODE_WPA2_WPA3_P, M_SWL_SECURITY_CONMODE_WPA2_P | M_SWL_SECURITY_CONMODE_WPA_P, true},
    {SWL_SECURITY_APMODE_WPA2_WPA3_P, M_SWL_SECURITY_CONMODE_WPA2_P, true},
    {SWL_SECURITY_APMODE_WPA2_WPA3_P, M_SWL_SECURITY_CONMODE_WPA3_P, true},
    {SWL_SECURITY_APMODE_WPA2_P, M_SWL_SECURITY_CONMODE_WPA2_P | M_SWL_SECURITY_CONMODE_WPA3_P, true},
    {SWL_SECURITY_APMODE_WPA3_P, M_SWL_SECURITY_CONMODE_WPA2_P | M_SWL_SECURITY_CONMODE_WPA3_P, true},
    {SWL_SECURITY_APMODE_WPA_WPA2_P, M_SWL_SECURITY_CONMODE_WPA_P, true},
    {SWL_SECURITY_APMODE_WPA_WPA2_P, M_SWL_SECURITY_CONMODE_WPA2_P, true},
    {SWL_SECURITY_APMODE_WPA_WPA2_P, M_SWL_SECURITY_CONMODE_WPA_P | M_SWL_SECURITY_CONMODE_WPA2_P, true},

    {SWL_SECURITY_APMODE_WPA3_P, M_SWL_SECURITY_CONMODE_WPA2_P | M_SWL_SECURITY_CONMODE_WPA_P, false},
    {SWL_SECURITY_APMODE_WPA3_P, M_SWL_SECURITY_CONMODE_WPA2_P, false},
    {SWL_SECURITY_APMODE_WPA2_P, M_SWL_SECURITY_CONMODE_WPA3_P, false},
    {SWL_SECURITY_APMODE_WPA_WPA2_P, M_SWL_SECURITY_CONMODE_WPA3_P, false},
    {-1, M_SWL_SECURITY_CONMODE_WPA3_P, false},
    {SWL_SECURITY_APMODE_WPA_WPA2_P, -1, false},
    {-1, -1, false},
};


static void test_swl_security_staMatchesAp(void** state _UNUSED) {
    for(size_t i = 0; i < SWL_ARRAY_SIZE(testMatchVals); i++) {
        assert_int_equal(testMatchVals[i].supports, swl_security_staMatchesAp(testMatchVals[i].staMode, testMatchVals[i].apMode));
    }
}

static void test_security_getMode(void** state _UNUSED) {

    // Expected results
    swl_security_apMode_e mode = SWL_SECURITY_APMODE_UNKNOWN;
    swl_security_apMode_e secModeExpected[SWL_SECURITY_APMODE_MAX];

    // Init Expected Results
    for(uint8_t i = 0; i < SWL_SECURITY_APMODE_MAX; i++) {
        secModeExpected[i] = mode;
        mode++;
    }

    // Init
    secModTest_t secModData[SWL_SECURITY_APMODE_MAX];
    memset(&secModData, 0, sizeof(secModData));


    //.mode = SWL_SECURITY_APMODE_WPA_P,
    secModData[SWL_SECURITY_APMODE_WPA_P].cipherSuiteParams = M_SWL_80211_CIPHER_TKIP;
    secModData[SWL_SECURITY_APMODE_WPA_P].akmSuiteParams = (M_SWL_80211_AKM_PSK | M_SWL_80211_AKM_PSK_SHA_256);

    //.mode = SWL_SECURITY_APMODE_WPA2_P,
    secModData[SWL_SECURITY_APMODE_WPA2_P].cipherSuiteParams = (M_SWL_80211_CIPHER_CCMP256 | M_SWL_80211_CIPHER_CCMP);
    secModData[SWL_SECURITY_APMODE_WPA2_P].akmSuiteParams = (M_SWL_80211_AKM_PSK | M_SWL_80211_AKM_PSK_SHA_256);

    //.mode = SWL_SECURITY_APMODE_WPA3_P,
    secModData[SWL_SECURITY_APMODE_WPA3_P].cipherSuiteParams = 0;
    secModData[SWL_SECURITY_APMODE_WPA3_P].akmSuiteParams = (M_SWL_80211_AKM_FT_SAE | M_SWL_80211_AKM_SAE);

    //.mode = SWL_SECURITY_APMODE_WPA_WPA2_P,
    secModData[SWL_SECURITY_APMODE_WPA_WPA2_P].cipherSuiteParams = (M_SWL_80211_CIPHER_TKIP | M_SWL_80211_CIPHER_CCMP256 | M_SWL_80211_CIPHER_CCMP);
    secModData[SWL_SECURITY_APMODE_WPA_WPA2_P].akmSuiteParams = (M_SWL_80211_AKM_PSK | M_SWL_80211_AKM_PSK_SHA_256);

    //.mode = SWL_SECURITY_APMODE_WPA2_WPA3_P,
    secModData[SWL_SECURITY_APMODE_WPA2_WPA3_P].cipherSuiteParams = (M_SWL_80211_CIPHER_CCMP256 | M_SWL_80211_CIPHER_CCMP);
    secModData[SWL_SECURITY_APMODE_WPA2_WPA3_P].akmSuiteParams = (M_SWL_80211_AKM_FT_SAE | M_SWL_80211_AKM_SAE | M_SWL_80211_AKM_PSK | M_SWL_80211_AKM_PSK_SHA_256);


    //.mode = SWL_SECURITY_APMODE_WEP64,
    secModData[SWL_SECURITY_APMODE_WEP64].cipherSuiteParams = M_SWL_80211_CIPHER_WEP40;
    secModData[SWL_SECURITY_APMODE_WEP64].akmSuiteParams = 0;

    //.mode = SWL_SECURITY_APMODE_WEP128,
    secModData[SWL_SECURITY_APMODE_WEP128].cipherSuiteParams = M_SWL_80211_CIPHER_WEP104;
    secModData[SWL_SECURITY_APMODE_WEP128].akmSuiteParams = 0;


    for(uint8_t i = 0; i < SWL_SECURITY_APMODE_MAX; i++) {

        if(secModData[i].mode == ( SWL_SECURITY_APMODE_WPA_P ||
                                   SWL_SECURITY_APMODE_WPA2_P ||
                                   SWL_SECURITY_APMODE_WPA3_P ||
                                   SWL_SECURITY_APMODE_WPA_WPA2_P ||
                                   SWL_SECURITY_APMODE_WPA2_WPA3_P ||
                                   SWL_SECURITY_APMODE_WEP64 ||
                                   SWL_SECURITY_APMODE_WEP128)) {
            swl_security_getMode(&secModData[i].mode, &secModData[i].cipherSuiteParams, &secModData[i].akmSuiteParams);
            assert_int_equal(&secModeExpected[i], &secModData[i].mode);
        }
    }
}

static void test_swl_security_isApModeValid(void** state _UNUSED) {
    struct {
        swl_security_apMode_e mode;
        bool expectedResult;
    } tests[] = {
        {SWL_SECURITY_APMODE_UNKNOWN, false},
        {SWL_SECURITY_APMODE_NONE, true},
        {SWL_SECURITY_APMODE_WEP64, true},
        {SWL_SECURITY_APMODE_WEP128, true},
        {SWL_SECURITY_APMODE_WEP128IV, true},
        {SWL_SECURITY_APMODE_WPA_P, true},
        {SWL_SECURITY_APMODE_WPA2_P, true},
        {SWL_SECURITY_APMODE_WPA_WPA2_P, true},
        {SWL_SECURITY_APMODE_WPA3_P, true},
        {SWL_SECURITY_APMODE_WPA2_WPA3_P, true},
        {SWL_SECURITY_APMODE_WPA_E, true},
        {SWL_SECURITY_APMODE_WPA2_E, true},
        {SWL_SECURITY_APMODE_WPA_WPA2_E, true},
        {SWL_SECURITY_APMODE_WPA3_E, true},
        {SWL_SECURITY_APMODE_WPA2_WPA3_E, true},
        {SWL_SECURITY_APMODE_OWE, true},
        {SWL_SECURITY_APMODE_AUTO, false},
        {SWL_SECURITY_APMODE_UNSUPPORTED, false},
        {SWL_SECURITY_APMODE_MAX, false},
        {SWL_SECURITY_APMODE_MAX + 100, false},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_security_isApModeValid(tests[i].mode), tests[i].expectedResult);
    }
}

static void test_swl_security_apModeMaskToStringExt(void** state _UNUSED) {
    struct {
        swl_security_apMode_m mask;
        swl_security_apModeFmt_e format;
        bool onlyValidModes;
        const char* expectedResult;
    } tests[] = {
        {0, SWL_SECURITY_APMODEFMT_DEFAULT, false, ""},
        {M_SWL_SECURITY_APMODE_UNKNOWN, SWL_SECURITY_APMODEFMT_DEFAULT, false, "Unknown"},
        {M_SWL_SECURITY_APMODE_AUTO, SWL_SECURITY_APMODEFMT_DEFAULT, false, "Auto"},
        {M_SWL_SECURITY_APMODE_UNKNOWN | M_SWL_SECURITY_APMODE_AUTO, SWL_SECURITY_APMODEFMT_DEFAULT, true, ""},
        {M_SWL_SECURITY_APMODE_NONE, SWL_SECURITY_APMODEFMT_DEFAULT, false, "None"},
        {M_SWL_SECURITY_APMODE_WEP64, SWL_SECURITY_APMODEFMT_LEGACY, true, "WEP-64"},
        {M_SWL_SECURITY_APMODE_WEP_ALL_TYPES, SWL_SECURITY_APMODEFMT_DEFAULT, true, "WEP-64,WEP-128,WEP-128iv"},
        {M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES, SWL_SECURITY_APMODEFMT_DEFAULT, true, "WPA3-Personal,WPA3-Personal-Transition"},
        {M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES, SWL_SECURITY_APMODEFMT_LEGACY, true, "WPA3-Personal,WPA2-WPA3-Personal"},
        {M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES, SWL_SECURITY_APMODEFMT_ALTERNATE, true, "WPA3-SAE,WPA2-PSK-WPA3-SAE"},
    };
    char buffer[128] = {0};
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        buffer[0] = 0;
        assert_true(swl_security_apModeMaskToStringExt(buffer, SWL_ARRAY_SIZE(buffer), tests[i].format, tests[i].mask, tests[i].onlyValidModes));
        assert_string_equal(buffer, tests[i].expectedResult);
    }
}

static void test_swl_security_apModeMaskFromStringExt(void** state _UNUSED) {
    struct {
        const char* input;
        bool onlyValidModes;
        swl_security_apMode_m expectedResult;
    } tests[] = {
        {"", false, M_SWL_SECURITY_APMODE_UNKNOWN},
        {"toto", false, M_SWL_SECURITY_APMODE_UNKNOWN},
        {"toto", true, 0},
        {"WPA2-WPA3-Personal,WPA3-SAE", true, M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES},
        {"WPA2-Personal,WPA3-Personal", true, M_SWL_SECURITY_APMODE_WPA2_P | M_SWL_SECURITY_APMODE_WPA3_P},
        {"Auto,WEP-64,,WPA2-Personal", false, M_SWL_SECURITY_APMODE_UNKNOWN | M_SWL_SECURITY_APMODE_WEP64 | M_SWL_SECURITY_APMODE_WPA2_P | M_SWL_SECURITY_APMODE_AUTO},
        {"Auto,WEP-128,,WPA2-Personal", true, M_SWL_SECURITY_APMODE_WEP128 | M_SWL_SECURITY_APMODE_WPA2_P},
        {"WEP,toto,WPA3-SAE", true, M_SWL_SECURITY_APMODE_WPA3_P},
        {"Unknown,Auto,Unsupported", false, M_SWL_SECURITY_APMODE_UNKNOWN | M_SWL_SECURITY_APMODE_AUTO | M_SWL_SECURITY_APMODE_UNSUPPORTED},
        {"Unknown,Auto,Unsupported", true, 0},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_security_apModeMaskFromStringExt(tests[i].input, tests[i].onlyValidModes), tests[i].expectedResult);
    }
}

static void test_swl_security_mfpModeToString(void** state _UNUSED) {
    struct {
        swl_security_mfpMode_e mode;
        const char* expectedResult;
    } tests[] = {
        {SWL_SECURITY_MFPMODE_DISABLED, "Disabled"},
        {SWL_SECURITY_MFPMODE_OPTIONAL, "Optional"},
        {SWL_SECURITY_MFPMODE_REQUIRED, "Required"},
        {SWL_SECURITY_MFPMODE_MAX, "Disabled"},
        {SWL_SECURITY_MFPMODE_MAX + 10, "Disabled"},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_string_equal(swl_security_mfpModeToString(tests[i].mode), tests[i].expectedResult);
    }
}

static void test_swl_security_mfpModeFromString(void** state _UNUSED) {
    struct {
        const char* input;
        swl_security_mfpMode_e expectedResult;
    } tests[] = {
        {"", SWL_SECURITY_MFPMODE_DISABLED},
        {"Disabled", SWL_SECURITY_MFPMODE_DISABLED},
        {"Optional", SWL_SECURITY_MFPMODE_OPTIONAL},
        {"Required", SWL_SECURITY_MFPMODE_REQUIRED},
        {"toto", SWL_SECURITY_MFPMODE_DISABLED},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_security_mfpModeFromString(tests[i].input), tests[i].expectedResult);
    }
}

static void test_swl_security_targetMfpMode(void** state _UNUSED) {
    struct {
        swl_security_mfpMode_e cfgMfpMode;
        swl_security_apMode_e secMode;
        swl_security_mfpMode_e expectedResult;
    } tests[] = {
        {SWL_SECURITY_MFPMODE_DISABLED, SWL_SECURITY_APMODE_WPA2_P, SWL_SECURITY_MFPMODE_DISABLED},
        {SWL_SECURITY_MFPMODE_OPTIONAL, SWL_SECURITY_APMODE_WPA2_P, SWL_SECURITY_MFPMODE_OPTIONAL},
        {SWL_SECURITY_MFPMODE_DISABLED, SWL_SECURITY_APMODE_WPA3_P, SWL_SECURITY_MFPMODE_REQUIRED},
        {SWL_SECURITY_MFPMODE_DISABLED, SWL_SECURITY_APMODE_WPA2_WPA3_P, SWL_SECURITY_MFPMODE_OPTIONAL},
        {SWL_SECURITY_MFPMODE_REQUIRED, SWL_SECURITY_APMODE_WPA2_WPA3_P, SWL_SECURITY_MFPMODE_REQUIRED},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(swl_security_getTargetMfpMode(tests[i].secMode, tests[i].cfgMfpMode), tests[i].expectedResult);
    }
}

static void test_swl_security_checkApModeType(void** state _UNUSED) {
    struct {
        swl_security_apMode_e secMode;
        bool (* checkFn)(swl_security_apMode_e mode);
        bool expectedResult;
    } tests[] = {
        {SWL_SECURITY_APMODE_UNKNOWN, swl_security_isApModeWEP, false},
        {SWL_SECURITY_APMODE_WEP64, swl_security_isApModeWEP, true},
        {SWL_SECURITY_APMODE_WEP128, swl_security_isApModeWEP, true},
        {SWL_SECURITY_APMODE_WEP128IV, swl_security_isApModeWEP, true},
        {SWL_SECURITY_APMODE_WPA2_P, swl_security_isApModeWEP, false},
        {SWL_SECURITY_APMODE_AUTO, swl_security_isApModeWPAPersonal, false},
        {SWL_SECURITY_APMODE_WEP128, swl_security_isApModeWPAPersonal, false},
        {SWL_SECURITY_APMODE_WPA2_P, swl_security_isApModeWPAPersonal, true},
        {SWL_SECURITY_APMODE_WPA2_E, swl_security_isApModeWPAPersonal, false},
        {SWL_SECURITY_APMODE_WPA2_WPA3_P, swl_security_isApModeWPAPersonal, true},
        {SWL_SECURITY_APMODE_WPA2_P, swl_security_isApModeWPA3Personal, false},
        {SWL_SECURITY_APMODE_WPA2_WPA3_P, swl_security_isApModeWPA3Personal, true},
        {SWL_SECURITY_APMODE_WPA3_P, swl_security_isApModeWPA3Personal, true},
    };
    for(size_t i = 0; i < SWL_ARRAY_SIZE(tests); i++) {
        assert_int_equal(tests[i].checkFn(tests[i].secMode), tests[i].expectedResult);
    }
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_security_toFromString),
        cmocka_unit_test(test_swl_security_staMatchesAp),
        cmocka_unit_test(test_security_getMode),
        cmocka_unit_test(test_swl_security_isApModeValid),
        cmocka_unit_test(test_swl_security_apModeMaskToStringExt),
        cmocka_unit_test(test_swl_security_apModeMaskFromStringExt),
        cmocka_unit_test(test_swl_security_checkApModeType),
        cmocka_unit_test(test_swl_security_mfpModeToString),
        cmocka_unit_test(test_swl_security_mfpModeFromString),
        cmocka_unit_test(test_swl_security_targetMfpMode),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
