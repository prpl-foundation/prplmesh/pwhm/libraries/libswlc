/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_tupleType.h"

#include "testCommon_swl_collVarFixedValType.h"


#define NR_TYPES 6
#define NR_VALUES 4


SWL_TYPE_CHARBUF_IMPL(17);
typedef char gtSwl_type_charBuf17_type[17];

#define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_charBuf17, charBuf) \
    X(Y, gtSwl_type_int8, test1) \
    X(Y, gtSwl_type_int8, test2) \
    X(Y, gtSwl_type_int16, index) \
    X(Y, gtSwl_type_int64, key) \
    X(Y, gtSwl_type_charPtr, val)

SWL_TT(tMyTestTupleType, swl_myTestTT_t, MY_TEST_TABLE_VAR, )


/**
 * Defining a tuple type using a table
 */
swl_myTestTT_t myTestTableValues[NR_VALUES] = {
    {"Bla", 1, 2, 1, 100, "test1"},
    {"Foo", 3, 4, 2, -200, "test2"},
    {"No", 5, 6, -8852, 289870, "foobar"},
    {"Yes", 0, -1, 8851, -289869, "barfoo"},
};

static swl_colVarValTypeTest_t s_myData = {
    .type = &tMyTestTupleType.type,
    .valueTupleType = &tMyTestTupleType,
    .fun = &swl_tupleTypeCollType_fun,
    .nrValues = tMyTestTupleType__max,
    .nrTuples = NR_VALUES,
    .tupleList = myTestTableValues

};


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    return testCommon_swl_collVarValType_runTest(&s_myData);
}
