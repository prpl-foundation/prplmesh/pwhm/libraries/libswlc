/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_table.h"

#define NR_TYPES 3
#define NR_VALUES 4

/**
 * Defining a tuple type using a table
 */
SWL_TABLE(myTestTable,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"},
              {8851, -289869, "barfoo"},
              )
          );

/**
 * Example of defining a tuple type
 */
SWL_TUPLE_TYPE(myTestTuple, ARR(swl_type_int32, swl_type_int64, swl_type_charPtr));

#define NR_OTHER_DATA 2

myTestTableStruct_t otherData[NR_OTHER_DATA] = {
    {5, 500, "notHere"},
    {-5, -500, "alsoNotHere"},
};

char* names[NR_TYPES] = {"SmallInt", "BigInt", "String"};

static void test_swl_tupleType_getValFromTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 0);
        assert_ptr_equal(&myTestTableValues[i].index, testData);

        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 1);
        assert_ptr_equal(&myTestTableValues[i].key, testData);

        //ptr logic => returned data is pointer itself
        testData = swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], 2);
        assert_ptr_equal(myTestTableValues[i].val, testData);


        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], myTestTable.nrTuples));
        assert_null(swl_tupleType_getValue(&myTestTableTupleType, &myTestTableValues[i], -1));
    }
}

static void test_swl_tupleType_getRefFromTuple(void** state _UNUSED) {
    for(size_t i = 0; i < myTestTable.nrTuples; i++) {
        void* testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 0);
        assert_ptr_equal(&myTestTableValues[i].index, testData);

        testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 1);
        assert_ptr_equal(&myTestTableValues[i].key, testData);

        //ptr logic => returned data is reference still
        testData = swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], 2);
        assert_ptr_equal(&myTestTableValues[i].val, testData);


        assert_null(swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], myTestTable.nrTuples));
        assert_null(swl_tupleType_getReference(&myTestTableTupleType, &myTestTableValues[i], -1));
    }
}

static void test_swl_tupleType_offsetAndSize(void** state _UNUSED) {

    size_t counter = 0;
    for(size_t i = 0; i < NR_TYPES; i++) {
        assert_int_equal(counter, swl_tupleType_getOffset(&myTestTableTupleType, i));
        counter += myTestTableTypes[i]->size;
    }

    assert_int_equal(counter, swl_tupleType_size(&myTestTableTupleType));
}

static void test_swl_tupleType_check(void** state _UNUSED) {
    assert_true(swl_tupleType_check(&myTestTableTupleType));
    assert_int_not_equal(0, myTestTableTupleType.type.size);

    assert_false(swl_tupleType_check(NULL));


    assert_int_equal(0, myTestTupleTupleType.type.size);
    assert_true(swl_tupleType_check(&myTestTupleTupleType));
    assert_int_not_equal(0, myTestTupleTupleType.type.size);

}


#define NR_MASK_VALUES (2 << (NR_TYPES - 1))
#define ALL_MASK (NR_MASK_VALUES - 1)

/**
 * Defining a tuple type using a table
 */
SWL_TABLE(myTestTableMasked,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR(
              {2, 200, "test2"},
              {1, 200, "test2"},
              {2, 100, "test2"},
              {1, 100, "test2"},
              {2, 200, "test1"},
              {1, 200, "test1"},
              {2, 100, "test1"},
              {1, 100, "test1"},
              )
          );

myTestTableMaskedStruct_t myBaseVal = {.index = 1, .key = 100, .val = "test1"};


static void test_swl_tupleType_equalsByMask(void** state _UNUSED) {
    for(size_t i = 0; i < (2 << (NR_TYPES - 1)); i++) {
        for(size_t j = 0; j < (2 << (NR_TYPES - 1)); j++) {
            assert_int_equal(((j & i) == i)
                             , swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, &myBaseVal, &myTestTableMaskedValues[j], i));
        }
    }
}


static void test_swl_tupleType_copyClean(void** state _UNUSED) {
    size_t typeSize = swl_tupleType_size(&myTestTableTupleType);
    char buffer[typeSize];
    char zeroBuffer[typeSize];
    memset(zeroBuffer, 0, sizeof(zeroBuffer));
    for(size_t i = 0; i < NR_MASK_VALUES; i++) {
        memset(buffer, 0, sizeof(buffer));
        swl_tupleType_copyByMask(buffer, &myTestTableMaskedTupleType, &myTestTableMaskedValues[i], i);

        assert_true(swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, buffer, &myBaseVal, i));

        size_t oppositeMask = ALL_MASK & (~i);
        assert_true(swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, buffer, zeroBuffer, oppositeMask));

        for(size_t j = 0; j < NR_MASK_VALUES; j++) {
            assert_int_equal(((j & i) == i)
                             , swl_tupleType_equalsByMask(&myTestTableMaskedTupleType, &myBaseVal, &myTestTableMaskedValues[j], i));
        }

        swl_tupleType_cleanupByMask(&myTestTableMaskedTupleType, buffer, i);
    }
}


static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_tupleType_getValFromTuple),
        cmocka_unit_test(test_swl_tupleType_getRefFromTuple),
        cmocka_unit_test(test_swl_tupleType_offsetAndSize),
        cmocka_unit_test(test_swl_tupleType_check),
        cmocka_unit_test(test_swl_tupleType_copyClean),
        cmocka_unit_test(test_swl_tupleType_equalsByMask),
    };
    int rc = 0;
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
