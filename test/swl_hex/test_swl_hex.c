/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_hex.h"

static void s_helper_swl_hex_toBytes(size_t targetSize, const char* str, size_t strSize,
                                     bool expectedOk, const swl_bit8_t* expectedTarget, size_t expectedTargetSize) {
    swl_bit8_t buffer[targetSize];
    bool ok = swl_hex_toBytes(buffer, targetSize, str, strSize);
    assert_true(expectedOk == ok);
    if(expectedOk) {
        assert_memory_equal(buffer, expectedTarget, expectedTargetSize);
    }
}

static void test_swl_hex_toBytes(void** state _UNUSED) {
    // Normal case
    char* input = "000102030405060708090a0b0c0d0e0f";
    uint8_t expected1[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected1, sizeof(expected1));

    // Normal case
    input = "102030405060708090a0b0c0d0e0f0";
    uint8_t expected2[15] = {16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224,
        240};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected2, sizeof(expected2));

    // Odd length ("f" instead of "0f" at the end)
    input = "0112233445566778899aabbccddeeffedcba9876543210f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Normal case
    input = "0112233445566778899aabbccddeeffedcba9876543210";
    uint8_t expected3[23] = {1, 18, 35, 52, 69, 86, 103, 120, 137, 154, 171, 188, 205, 222,
        239, 254, 220, 186, 152, 118, 84, 50, 16};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected3, sizeof(expected3));

    // Empty
    input = "";
    uint8_t expected4[] = {};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected4, sizeof(expected4));

    // Length 1.
    input = "f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Odd length, due to `strlen`, not on first character.
    input = "111";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Odd length, due to length parameter and not `strlen`.
    input = "11111111";
    s_helper_swl_hex_toBytes(3, input, strlen(input), false, NULL, 0);

    // Uppercase
    input = "01020AA0F0BBCCDDEEFF";
    uint8_t expected7[] = {1, 2, 10, 160, 240, 187, 204, 221, 238, 255};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected7, sizeof(expected7));

    // Mixed case
    input = "abCDEfaB";
    uint8_t expected8[] = {171, 205, 239, 171};
    s_helper_swl_hex_toBytes(64, input, strlen(input), true, expected8, sizeof(expected8));

    // Beginning "0x"
    input = "0x11";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Beginning "0X"
    input = "0X11";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Middle "0x"
    input = "000102030x0405060708090a0b0c0d0e0f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Middle "0X"
    input = "000102030X0405060708090a0b0c0d0e0f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Space begin
    input = " 000102030405060708090a0b0c0d0e0f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Space middle
    input = "000102030405 060708090a0b0c0d0e0f";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Space end
    input = "000102030405060708090a0b0c0d0e0f ";
    s_helper_swl_hex_toBytes(64, input, strlen(input), false, NULL, 0);

    // Input length includes '\0'
    input = "00010aa0ff";
    uint8_t expected9[] = {0, 1, 10, 160, 255};
    s_helper_swl_hex_toBytes(64, input, strlen(input) + 1, true, expected9, sizeof(expected9));

    // Input length much longer
    s_helper_swl_hex_toBytes(64, input, strlen(input) + 100, true, expected9, sizeof(expected9));

    // Output buffer just long enough
    s_helper_swl_hex_toBytes(sizeof(expected9), input, strlen(input), true, expected9, sizeof(expected9));

    // Output buffer one too short
    s_helper_swl_hex_toBytes(sizeof(expected9) - 1, input, strlen(input), false, NULL, 0);

    // NULL output buffer
    bool ok = swl_hex_toBytes(NULL, 100, input, strlen(input));
    assert_false(ok);

    // NULL input string
    swl_bit8_t buffer[100];
    ok = swl_hex_toBytes(buffer, sizeof(buffer), NULL, 20);
    assert_false(ok);
}

static void s_helper_swl_hex_fromBytes(size_t strSize, swl_bit8_t* bytes, size_t bytesSize, bool upperCase,
                                       bool expectedOk, const char* expectedStr) {
    char str[strSize];
    bool ok = swl_hex_fromBytes(str, strSize, bytes, bytesSize, upperCase);
    assert_true(expectedOk == ok);
    if(expectedOk) {
        assert_true(0 == strcmp(str, expectedStr));
    }
}

static void test_swl_hex_fromBytes(void** state _UNUSED) {
    // Normal case
    uint8_t input1[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    s_helper_swl_hex_fromBytes(64, input1, sizeof(input1), false, true,
                               "000102030405060708090a0b0c0d0e0f");

    // Normal case
    uint8_t input2[15] = {16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224,
        240};
    s_helper_swl_hex_fromBytes(64, input2, sizeof(input2), false, true,
                               "102030405060708090a0b0c0d0e0f0");

    // Normal case
    uint8_t input3[23] = {1, 18, 35, 52, 69, 86, 103, 120, 137, 154, 171, 188, 205, 222,
        239, 254, 220, 186, 152, 118, 84, 50, 16};
    s_helper_swl_hex_fromBytes(64, input3, sizeof(input3), false, true,
                               "0112233445566778899aabbccddeeffedcba9876543210");

    // Empty
    uint8_t input4[0] = {};
    s_helper_swl_hex_fromBytes(64, input4, sizeof(input4), false, true, "");

    // null-padding of last digit with only one byte
    uint8_t input5[1] = {15};
    s_helper_swl_hex_fromBytes(64, input5, sizeof(input5), false, true, "0f");

    // null-padding of last digit with more than one byte.
    uint8_t input6[] = {1, 1};
    s_helper_swl_hex_fromBytes(64, input6, sizeof(input6), false, true, "0101");

    // Uppercase
    uint8_t input7[] = {1, 2, 10, 160, 240, 187, 204, 221, 238, 255};
    s_helper_swl_hex_fromBytes(64, input7, sizeof(input7), true, true, "01020AA0F0BBCCDDEEFF");

    // Trailing zeroes does not make string shorter
    uint8_t input8[] = {0, 1, 10, 160, 255, 0, 0, 0};
    const char* expected8 = "00010aa0ff000000";
    s_helper_swl_hex_fromBytes(64, input8, sizeof(input8), false, true, expected8);

    // Output buffer just long enough
    s_helper_swl_hex_fromBytes(strlen(expected8) + 1, input8, sizeof(input8), false, true, expected8);

    // Output buffer one too short
    s_helper_swl_hex_fromBytes(strlen(expected8), input8, sizeof(input8), false, false, NULL);

    // Output buffer half too short
    s_helper_swl_hex_fromBytes(strlen(expected8) / 2, input8, sizeof(input8), false, false, NULL);

    // NULL output buffer
    bool ok = swl_hex_fromBytes(NULL, 100, input8, sizeof(input8), false);
    assert_false(ok);

    // NULL input buffer
    char buffer[100];
    ok = swl_hex_fromBytes(buffer, sizeof(buffer), NULL, 20, false);
    assert_false(ok);
}

static void test_swl_hex_fromBytesSep(void** state) {
    (void) state;
    struct {
        bool uppercase;
        char separator;
        size_t dataLen;
        union {
            uint32_t num;
            uint8_t data[16];
        };
        size_t strSize;
        bool expectedOk;
        char expectedStr[16 * 3];
    } testSeqs[] = {
        // Normal case: upper case , with separator
        {.uppercase = false, .separator = '-', .strSize = 16,
            .num = htonl(0x200a), .dataLen = sizeof(int),
            .expectedStr = {"00-00-20-0a"}, .expectedOk = true, },
        // Normal case: upper case , without separator
        {.uppercase = true, .separator = 0, .strSize = 16,
            .num = htonl(0x200a), .dataLen = sizeof(int),
            .expectedStr = {"0000200A"}, .expectedOk = true, },
        // Normal case: lower case , with separator
        {.uppercase = false, .separator = ':', .strSize = 16,
            .data = {1, 2, 3, 0xfa, 0xbe}, .dataLen = 5,
            .expectedStr = {"01:02:03:fa:be"}, .expectedOk = true, },
        // Error case: with separator, but str buff too short (only 3 bytes printed, no sep)
        {.uppercase = true, .separator = '_', .strSize = 10,
            .data = {1, 2, 3, 0xfa, 0xbe}, .dataLen = 5,
            .expectedStr = {"01_02_03"}, .expectedOk = false, },
        // Error case: with separator, but str buff too short (still 3 bytes printed, no sep)
        {.uppercase = true, .separator = '_', .strSize = 11,
            .data = {1, 2, 3, 0xfa, 0xbe}, .dataLen = 5,
            .expectedStr = {"01_02_03"}, .expectedOk = false, },
        // Error case: with separator, but str buff too short (only 4 bytes printed, no sep)
        {.uppercase = true, .separator = '_', .strSize = 12,
            .data = {1, 2, 3, 0xfa, 0xbe}, .dataLen = 5,
            .expectedStr = {"01_02_03_FA"}, .expectedOk = false, },
        // Normal case: without separator => str buff becomes long enough
        {.uppercase = true, .separator = 0, .strSize = 11,
            .data = {1, 2, 0, 0xfa, 0}, .dataLen = 5,
            .expectedStr = {"010200FA00"}, .expectedOk = true, },
        // Error case: no separator, but str buf too short
        {.uppercase = true, .separator = 0, .strSize = 2,
            .data = {1, 2}, .dataLen = 2,
            .expectedStr = {""}, .expectedOk = false, },
        // Error case: no separator, but str buf still too short
        {.uppercase = true, .separator = 0, .strSize = 3,
            .data = {1, 2}, .dataLen = 2,
            .expectedStr = {"01"}, .expectedOk = false, },
        // Error case: empty data
        {.uppercase = true, .separator = 0, .strSize = 11,
            .data = {}, .dataLen = 0,
            .expectedStr = {""}, .expectedOk = true, },
    };
    bool ok;
    size_t lenWritten;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        char strBuf[testSeqs[i].strSize];
        ok = swl_hex_fromBytesSep(strBuf, sizeof(strBuf), testSeqs[i].data, testSeqs[i].dataLen, testSeqs[i].uppercase, testSeqs[i].separator, &lenWritten);
        assert_true(testSeqs[i].expectedOk == ok);
        assert_string_equal(strBuf, testSeqs[i].expectedStr);
        assert_int_equal(lenWritten, strlen(testSeqs[i].expectedStr));
    }
    uint32_t num = 0;
    char numStr[16] = {0};
    ok = swl_hex_fromBytesSep(NULL, sizeof(numStr), (uint8_t*) &num, sizeof(num), false, 0, NULL);
    assert_true(ok == false);
    ok = swl_hex_fromBytesSep(numStr, 0, (uint8_t*) &num, sizeof(num), false, 0, NULL);
    assert_true(ok == false);
    numStr[0] = 'x';
    ok = swl_hex_fromBytesSep(numStr, sizeof(numStr), NULL, sizeof(num), false, 0, NULL);
    assert_true(ok == false);
    assert_string_equal(numStr, "");
    numStr[0] = 'x';
    ok = swl_hex_fromBytesSep(numStr, sizeof(numStr), (uint8_t*) &num, 0, false, 0, NULL);
    assert_true(ok == true);
    assert_string_equal(numStr, "");
}

static void test_swl_hex_toBytesSep(void** state) {
    (void) state;
    struct {
        const char separator;
        char str[16 * 3];
        bool expectedOk;
        size_t expectedWrite;
        size_t dataSize;
        union {
            uint32_t num;
            uint8_t data[16];
        };
    } testSeqs[] = {
        //Normal case: null separator
        {.separator = 0, .dataSize = sizeof(int),
            .str = {"0000200a"},
            .num = htonl(0x200a), .expectedWrite = sizeof(int), .expectedOk = true, },
        //Normal case: separator matched
        {.separator = ':', .dataSize = sizeof(int),
            .str = {"00:00:2F:ea"},
            .num = htonl(0x2fea), .expectedWrite = sizeof(int), .expectedOk = true, },
        //Error case: invalid hex digit (with separator)
        {.separator = ':', .dataSize = sizeof(int),
            .str = {"00:00:20:Ga"},
            .data = {0, 0, 0x20}, .expectedWrite = 3, .expectedOk = false, },
        //Error case: invalid hex digit (without separator)
        {.separator = 0, .dataSize = sizeof(int),
            .str = {"000020Ga"},
            .data = {0, 0, 0x20}, .expectedWrite = 3, .expectedOk = false, },
        //Error case: separator unmatched
        {.separator = '-', .dataSize = sizeof(int),
            .str = {"00/00/20/0a"},
            .data = {0}, .expectedWrite = 0, .expectedOk = false, },
        //Error case: separator expected but not found after 1st byte
        {.separator = '-', .dataSize = sizeof(int),
            .str = {"0000200a"},
            .data = {0}, .expectedWrite = 0, .expectedOk = false, },
        //Error case: separator expected but not found after next bytes
        {.separator = ':', .dataSize = sizeof(int),
            .str = {"00:00:200a"},
            .data = {0, 0}, .expectedWrite = 2, .expectedOk = false, },
        //Error case: mixed separators
        {.separator = '-', .dataSize = sizeof(int),
            .str = {"fA-00:20_0a"},
            .data = {0xfa}, .expectedWrite = 1, .expectedOk = false, },
        //Error case: odd length
        {.separator = 0, .dataSize = 4,
            .str = {"68007"},
            .data = {'h', '\0', }, .expectedWrite = 2, .expectedOk = false, },
        //Normal case: byte with only one nibble, at the begging, delimited by one separator
        {.separator = '-', .dataSize = 4,
            .str = {"7-68-31"},
            .data = {'\a', 'h', '1'}, .expectedWrite = 3, .expectedOk = true, },
        //Normal case: byte with only one nibble, delimited by two separators
        {.separator = '-', .dataSize = 4,
            .str = {"68-7-31"},
            .data = {'h', '\a', '1'}, .expectedWrite = 3, .expectedOk = true, },
        //Normal case: byte with only one nibble, delimited by separator and null char
        {.separator = '-', .dataSize = 4,
            .str = {"68-00-7"},
            .data = {'h', '\0', '\a'}, .expectedWrite = 3, .expectedOk = true, },
        //Error case: target buffer too short
        {.separator = ':', .dataSize = 4,
            .str = {"31:32:3:34:5:FA"},
            .data = {'1', '2', 3, '4'}, .expectedWrite = 4, .expectedOk = false, },
    };
    bool ok;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        size_t lenByteWritten;
        uint8_t data[testSeqs[i].dataSize];
        ok = swl_hex_toBytesSep(data, testSeqs[i].dataSize, testSeqs[i].str, strlen(testSeqs[i].str) + 1, testSeqs[i].separator, &lenByteWritten);
        assert_true(testSeqs[i].expectedOk == ok);
        assert_int_equal(lenByteWritten, testSeqs[i].expectedWrite);
        assert_memory_equal(data, testSeqs[i].data, testSeqs[i].expectedWrite);
    }
}

static void test_swl_hex_findValidSepChar(void** state) {
    (void) state;
    struct testInf {
        char str[32];
        size_t strSize;
        bool expectedOk;
        char expectedSep;
    };
    size_t strSize = sizeof(((struct testInf*) 0)->str);
    struct testInf testSeqs[] = {
        {.str = {"00:Ab:c:52"}, .strSize = strSize, .expectedOk = true, .expectedSep = ':', },
        {.str = {"00-Ab-cC-5-00"}, .strSize = strSize, .expectedOk = true, .expectedSep = '-', },
        {.str = {"0_Ab_cC_5_0"}, .strSize = strSize, .expectedOk = true, .expectedSep = '_', },
        {.str = {"00 Ab cC 52 f"}, .strSize = strSize, .expectedOk = true, .expectedSep = ' ', },
        {.str = {"00AbcC52fF"}, .strSize = strSize, .expectedOk = true, .expectedSep = 0, },
        {.str = {"00AbcC52f"}, .strSize = strSize, .expectedOk = true, .expectedSep = 0, },
        {.str = {"00Ab-cC-52-fF"}, .strSize = strSize, .expectedOk = false, .expectedSep = 0, },
        {.str = {"00-AbcC-52-fF"}, .strSize = strSize, .expectedOk = false, .expectedSep = 0, },
        {.str = {"00-Ab-cC:52-fF"}, .strSize = strSize, .expectedOk = false, .expectedSep = 0, },
        {.str = {"00-Ab-cC:52-fF"}, .strSize = 8, .expectedOk = true, .expectedSep = '-', },
    };
    bool ok;
    char sep;
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(testSeqs); i++) {
        ok = swl_hex_findValidSepChar(&sep, testSeqs[i].str, testSeqs[i].strSize);
        assert_true(testSeqs[i].expectedOk == ok);
        assert_int_equal(sep, testSeqs[i].expectedSep);
    }
    assert_false(swl_hex_findValidSepChar(NULL, "", 0));
    assert_false(swl_hex_findValidSepChar(&sep, NULL, 0));
    assert_true(swl_hex_findValidSepChar(&sep, "", 0));
}

static void test_swl_hex_isSepOneChar(void** state _UNUSED) {
    assert_true(swl_hex_isSepOneChar(':'));
    assert_true(swl_hex_isSepOneChar('-'));
    assert_true(swl_hex_isSepOneChar('_'));
    assert_true(swl_hex_isSepOneChar('!'));
    assert_true(swl_hex_isSepOneChar(' '));
    assert_true(swl_hex_isSepOneChar('`'));
    assert_true(swl_hex_isSepOneChar('@'));
    assert_true(swl_hex_isSepOneChar(' '));

    assert_false(swl_hex_isSepOneChar('0'));
    assert_false(swl_hex_isSepOneChar('5'));
    assert_false(swl_hex_isSepOneChar('9'));
    assert_false(swl_hex_isSepOneChar('a'));
    assert_false(swl_hex_isSepOneChar('A'));
    assert_false(swl_hex_isSepOneChar('F'));
    assert_false(swl_hex_isSepOneChar('f'));
    assert_false(swl_hex_isSepOneChar('\0'));
    assert_false(swl_hex_isSepOneChar('\t'));
    assert_false(swl_hex_isSepOneChar('x'));
    assert_false(swl_hex_isSepOneChar('g'));
    assert_false(swl_hex_isSepOneChar('G'));
}

static void test_swl_hex_isHexChar(void** state _UNUSED) {
    // Empty length.
    assert_true(swl_hex_isHexChar("", 0));

    // Empty length, but NULL encountered first
    assert_true(swl_hex_isHexChar("", 100));

    // Normal lowercase
    assert_true(swl_hex_isHexChar("0123456789abcdef0a0b0c0d0e0fa1b2c3d4e5f6", 100));

    // Normal uppercase
    assert_true(swl_hex_isHexChar("0123456789ABCDEF0A0B0C0D0E0FA1B2C3D4E5F6", 100));

    // Mixed case
    assert_true(swl_hex_isHexChar("AaBbCcDdEeFfaAbBcCdDeEfFAAaaAfFa", 100));

    // Length limited
    assert_true(swl_hex_isHexChar("000aa0This is not hex anymore", 6));

    // Odd length due to `strlen`
    assert_false(swl_hex_isHexChar("444", 100));

    // Odd length due to length parameter
    assert_false(swl_hex_isHexChar("44444444444", 5));

    // Not hex
    assert_false(swl_hex_isHexChar("010g", 100));
    assert_false(swl_hex_isHexChar("00!!", 100));
    assert_false(swl_hex_isHexChar("!!0011", 100));

    // Spaces
    assert_false(swl_hex_isHexChar(" 01", 100));
    assert_false(swl_hex_isHexChar("01 ", 100));
    assert_false(swl_hex_isHexChar("01 02", 100));

    // "0x"
    assert_false(swl_hex_isHexChar("0x11", 100));
    assert_false(swl_hex_isHexChar("110x", 100));
    assert_false(swl_hex_isHexChar("110xaa", 100));

    // NULL char buffer
    assert_false(swl_hex_isHexChar(NULL, 100));
}

static void test_swl_hex_isHexOneChar(void** state _UNUSED) {
    assert_true(swl_hex_isHexOneChar('0'));
    assert_true(swl_hex_isHexOneChar('5'));
    assert_true(swl_hex_isHexOneChar('9'));
    assert_true(swl_hex_isHexOneChar('a'));
    assert_true(swl_hex_isHexOneChar('A'));
    assert_true(swl_hex_isHexOneChar('F'));
    assert_true(swl_hex_isHexOneChar('f'));

    assert_false(swl_hex_isHexOneChar('\0'));
    assert_false(swl_hex_isHexOneChar('x'));
    assert_false(swl_hex_isHexOneChar('!'));
    assert_false(swl_hex_isHexOneChar(' '));
    assert_false(swl_hex_isHexOneChar('`'));
    assert_false(swl_hex_isHexOneChar('@'));
    assert_false(swl_hex_isHexOneChar('g'));
    assert_false(swl_hex_isHexOneChar('G'));
}

static void s_helper_swl_hex_toUint(int bits, char* src, size_t srcSize, bool expectOk, uint32_t expectVal) {
    uint32_t actualVal;
    bool ok;
    if(bits == 8) {
        uint8_t uint8 = 0;
        ok = swl_hex_toUint8(&uint8, src, srcSize);
        actualVal = uint8;
    } else if(bits == 16) {
        uint16_t uint16 = 0;
        ok = swl_hex_toUint16(&uint16, src, srcSize);
        actualVal = uint16;
    } else if(bits == 32) {
        ok = swl_hex_toUint32(&actualVal, src, srcSize);
    } else {
        assert_false(true);
    }

    assert_true(ok == expectOk);
    if(expectOk) {
        assert_int_equal(expectVal, actualVal);
    }
}

static void test_swl_hex_toUint8(void** state _UNUSED) {
    s_helper_swl_hex_toUint(8, "00", 100, true, 0);
    s_helper_swl_hex_toUint(8, "11", 100, true, 17);
    s_helper_swl_hex_toUint(8, "0f", 100, true, 15);
    s_helper_swl_hex_toUint(8, "f0", 100, true, 240);
    s_helper_swl_hex_toUint(8, "fg", 100, false, 0);
    s_helper_swl_hex_toUint(8, "ff", 100, true, 255);
    s_helper_swl_hex_toUint(8, "FF", 100, true, 255);
    s_helper_swl_hex_toUint(8, "0x", 100, false, 0);
    s_helper_swl_hex_toUint(8, "0xff", 100, false, 0);
    s_helper_swl_hex_toUint(8, "ffff", 100, true, 255);
    s_helper_swl_hex_toUint(8, "ffffffffffff", 0, false, 0);
    s_helper_swl_hex_toUint(8, "f", 100, false, 0);
    s_helper_swl_hex_toUint(8, "", 100, false, 0);
}

static void test_swl_hex_toUint16(void** state _UNUSED) {
    s_helper_swl_hex_toUint(16, "0000", 100, true, 0);
    s_helper_swl_hex_toUint(16, "a1b2", 100, true, 41394);
    s_helper_swl_hex_toUint(16, "1234", 100, true, 4660);
    s_helper_swl_hex_toUint(16, "000f", 100, true, 15);
    s_helper_swl_hex_toUint(16, "00f0", 100, true, 240);
    s_helper_swl_hex_toUint(16, "0f00", 100, true, 3840);
    s_helper_swl_hex_toUint(16, "f000", 100, true, 61440);
    s_helper_swl_hex_toUint(16, "ffff", 100, true, 65535);
    s_helper_swl_hex_toUint(16, "ffgf", 100, false, 0);
    s_helper_swl_hex_toUint(16, "FffF", 100, true, 65535);
    s_helper_swl_hex_toUint(16, "0xff", 100, false, 0);
    s_helper_swl_hex_toUint(16, "0xffff", 100, false, 0);
    s_helper_swl_hex_toUint(16, "ff0xff", 100, false, 0);
    s_helper_swl_hex_toUint(16, "ffffffff", 100, true, 65535);
    s_helper_swl_hex_toUint(16, "ffffffffffff", 3, false, 0);
    s_helper_swl_hex_toUint(16, "fff", 100, false, 0);
    s_helper_swl_hex_toUint(16, "ff", 100, false, 0);
    s_helper_swl_hex_toUint(16, "f", 100, false, 0);
    s_helper_swl_hex_toUint(16, "", 100, false, 0);
}

static void test_swl_hex_toUint32(void** state _UNUSED) {
    s_helper_swl_hex_toUint(32, "00000000", 100, true, 0);
    s_helper_swl_hex_toUint(32, "0000a1b2", 100, true, 41394);
    s_helper_swl_hex_toUint(32, "00001234", 100, true, 4660);
    s_helper_swl_hex_toUint(32, "0000000f", 100, true, 15);
    s_helper_swl_hex_toUint(32, "000000f0", 100, true, 240);
    s_helper_swl_hex_toUint(32, "00000f00", 100, true, 3840);
    s_helper_swl_hex_toUint(32, "0000f000", 100, true, 61440);
    s_helper_swl_hex_toUint(32, "0000ffff", 100, true, 65535);
    s_helper_swl_hex_toUint(32, "0000g000", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ffffffff", 100, true, 4294967295);
    s_helper_swl_hex_toUint(32, "FFffFffF", 100, true, 4294967295);
    s_helper_swl_hex_toUint(16, "0xffffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "0xffffffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ff0xffffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ffffffffffff", 100, true, 4294967295);
    s_helper_swl_hex_toUint(32, "ffffffffffff", 7, false, 0);
    s_helper_swl_hex_toUint(32, "fffffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ffffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "fffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ffff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "fff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "ff", 100, false, 0);
    s_helper_swl_hex_toUint(32, "f", 100, false, 0);
    s_helper_swl_hex_toUint(32, "", 100, false, 0);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlHex");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_hex_toBytes),
        cmocka_unit_test(test_swl_hex_fromBytes),
        cmocka_unit_test(test_swl_hex_toBytesSep),
        cmocka_unit_test(test_swl_hex_fromBytesSep),
        cmocka_unit_test(test_swl_hex_findValidSepChar),
        cmocka_unit_test(test_swl_hex_isSepOneChar),
        cmocka_unit_test(test_swl_hex_isHexChar),
        cmocka_unit_test(test_swl_hex_isHexOneChar),
        cmocka_unit_test(test_swl_hex_toUint8),
        cmocka_unit_test(test_swl_hex_toUint16),
        cmocka_unit_test(test_swl_hex_toUint32),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
