/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define CHECK_ENTRIES(map, data, checkSize, dataSize) \
    entriesMatch(map, data, checkSize, dataSize, __FILE__, __LINE__)

static void entriesMatch(swl_mapGeneric_t* map, myTestData* data, uint32_t mapSize, uint32_t dataSize, char* file _UNUSED, uint32_t line _UNUSED) {
    assert_int_equal(swl_map_size(map), mapSize);

    size_t i = 0;
    for(i = 0; i < mapSize; i++) {
        ASSERT_VAL_EQUAL(swl_mapGeneric_get(map, data[i].key), data[i].val);
    }
    for(i = mapSize; i < dataSize; i++) {
        assert_false(swl_mapGeneric_has(map, data[i].key));
        ASSERT_VAL_DEFAULT(swl_mapGeneric_get(map, data[i].key));
    }
    return;
}

static void test_swl_mapGeneric_addGet(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        assert_false(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_DEFAULT(swl_mapGeneric_get(&map, dataSet[i].key));
        swl_mapGeneric_add(&map, dataSet[i].key, dataSet[i].val);
        assert_true(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_EQUAL(swl_mapGeneric_get(&map, dataSet[i].key), dataSet[i].val);
        assert_int_equal(swl_map_size(&map), i + 1);
    }

    for(i = 0; i < DATASET_SIZE; i++) {
        assert_true(swl_mapGeneric_has(&map, dataSet[i].key));
        swl_mapGeneric_delete(&map, dataSet[i].key);
        assert_false(swl_mapGeneric_has(&map, dataSet[i].key));
        assert_int_equal(swl_map_size(&map), DATASET_SIZE - i - 1);
    }

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_addOrSet(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        assert_false(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_DEFAULT(swl_mapGeneric_get(&map, dataSet[i].key));
        swl_mapGeneric_addOrSet(&map, dataSet[i].key, dataSet[i].val);
        assert_true(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_EQUAL(swl_mapGeneric_get(&map, dataSet[i].key), dataSet[i].val);
        assert_int_equal(swl_map_size(&map), i + 1);
    }

    for(i = 0; i < DATASET_SIZE; i++) {
        assert_int_equal(swl_map_size(&map), DATASET_SIZE);
        assert_true(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_EQUAL(swl_mapGeneric_get(&map, dataSet[i].key), dataSet[i].val);
        swl_mapGeneric_addOrSet(&map, dataSet[i].key, dataSet_overwrite[i].val);
        assert_true(swl_mapGeneric_has(&map, dataSet[i].key));
        ASSERT_VAL_EQUAL(swl_mapGeneric_get(&map, dataSet[i].key), dataSet_overwrite[i].val);
        assert_int_equal(swl_map_size(&map), DATASET_SIZE);
    }

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_getCharFromChar(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        swl_mapGeneric_add(&map, dataSet[i].key, dataSet[i].val);
    }

    char keyBuffer[128];
    char valueBuffer[128];
    char testBuffer[128];

    for(i = 0; i < DATASET_SIZE; i++) {
        map.keyType->typeFun->toChar(map.keyType, keyBuffer, sizeof(keyBuffer), swl_type_toPtr(map.keyType, &dataSet[i].key));
        map.valueType->typeFun->toChar(map.valueType, valueBuffer, sizeof(valueBuffer), swl_type_toPtr(map.valueType, &dataSet[i].val));
        assert_true(swl_map_getCharFromChar(testBuffer, sizeof(testBuffer), &map, keyBuffer));
        assert_string_equal(valueBuffer, testBuffer);
    }

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_toChar(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_PART_SIZE; i++) {
        swl_mapGeneric_add(&map, dataSet[i].key, dataSet[i].val);
    }
    char buffer[128];
    assert_true(swl_map_toChar(buffer, sizeof(buffer), &map));
    assert_string_equal(buffer, dataSetPartStr);

    for(i = 0; i < DATASET_SIZE; i++) {
        bool add = swl_mapGeneric_add(&map, dataSet[i].key, dataSet[i].val);
        if(i < DATASET_PART_SIZE) {
            assert_false(add);
        } else {
            assert_true(add);
        }
    }

    swl_map_toChar(buffer, sizeof(buffer), &map);
    assert_string_equal(buffer, dataSetStr);

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_toCharInvalid(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < DATASET_PART_SIZE; i++) {
        swl_mapGeneric_add(&map, dataSet[i].key, dataSet[i].val);
    }

    char buffer[1];
    assert_false(swl_map_toChar(buffer, sizeof(buffer), &map));

    char largeBuf[128];
    assert_false(swl_map_toChar(largeBuf, sizeof(largeBuf), NULL));

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_fromChar(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetPartStr, true);
    CHECK_ENTRIES(&map, dataSet, DATASET_PART_SIZE, DATASET_SIZE);

    swl_map_fromChar(&map, dataSetStr, false);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_fromCharInvalid(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    int i = 0;
    for(i = 0; i < NR_INVALID_STR; i++) {
        assert_false(swl_map_fromChar(&map, invalidStr[i], true));
    }

    assert_false(swl_map_fromChar(NULL, dataSetStr, true));

    swl_mapGeneric_cleanup(&map);
}

static void test_swl_mapGeneric_fromCharOverWrite(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetStr, true);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);

    swl_map_fromChar(&map, dataSetOverWriteStr, true);
    CHECK_ENTRIES(&map, dataSet_overwrite, DATASET_SIZE, DATASET_SIZE);

    swl_mapGeneric_cleanup(&map);
}


static void test_swl_mapGeneric_delete(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetStr, true);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);
    int i = 0;

    for(i = 0; i < DATASET_SIZE; i++) {
        swl_mapGeneric_delete(&map, dataSet[DATASET_SIZE - i - 1].key);
        CHECK_ENTRIES(&map, dataSet, DATASET_SIZE - i - 1, DATASET_SIZE);
    }

    swl_map_cleanup(&map);
}


static void test_swl_mapGeneric_clear(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetStr, false);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);

    swl_map_clear(&map);
    CHECK_ENTRIES(&map, dataSet, 0, DATASET_SIZE);

    swl_map_fromChar(&map, dataSetOverWriteStr, false);
    CHECK_ENTRIES(&map, dataSet_overwrite, DATASET_SIZE, DATASET_SIZE);

    swl_map_clear(&map);
    CHECK_ENTRIES(&map, dataSet, 0, DATASET_SIZE);

    swl_map_cleanup(&map);
}

static void test_swl_mapGeneric_forEach(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetStr, false);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);

    char keyBuffer[128];
    char testKeyBuffer[128];
    char valueBuffer[128];
    char testValueBuffer[128];

    int i = 0;
    swl_mapIt_t mapIt;
    swl_map_for_each(mapIt, &map) {
        ASSERT_KEY_EQUAL(swl_mapGeneric_itKey(&mapIt), dataSet[i].key);
        ASSERT_VAL_EQUAL(swl_mapGeneric_itValue(&mapIt), dataSet[i].val);

        map.keyType->typeFun->toChar(map.keyType, keyBuffer, sizeof(keyBuffer), swl_type_toPtr(map.keyType, &dataSet[i].key));
        map.valueType->typeFun->toChar(map.valueType, valueBuffer, sizeof(valueBuffer), swl_type_toPtr(map.valueType, &dataSet[i].val));

        swl_map_itKeyChar(testKeyBuffer, sizeof(testKeyBuffer), &mapIt);
        assert_string_equal(keyBuffer, testKeyBuffer);

        swl_map_itValueChar(testValueBuffer, sizeof(testValueBuffer), &mapIt);
        assert_string_equal(valueBuffer, testValueBuffer);


        i++;
    }

    assert_int_equal(i, DATASET_SIZE);
    swl_map_cleanup(&map);
}

static void test_swl_mapGeneric_set(void** state _UNUSED) {
    swl_mapGeneric_t map;
    swl_mapGeneric_init(&map);

    swl_map_fromChar(&map, dataSetStr, false);
    CHECK_ENTRIES(&map, dataSet, DATASET_SIZE, DATASET_SIZE);

    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        swl_mapGeneric_set(&map, dataSet_overwrite[i].key, dataSet_overwrite[i].val);
    }
    CHECK_ENTRIES(&map, dataSet_overwrite, DATASET_SIZE, DATASET_SIZE);

    swl_map_cleanup(&map);
}

static void test_swl_mapGeneric_compare(void** state _UNUSED) {
    swl_mapGeneric_t map1;
    swl_mapGeneric_init(&map1);

    swl_map_fromChar(&map1, dataSetPartStr, true);
    CHECK_ENTRIES(&map1, dataSet, DATASET_PART_SIZE, DATASET_SIZE);

    swl_map_fromChar(&map1, dataSetStr, false);
    CHECK_ENTRIES(&map1, dataSet, DATASET_SIZE, DATASET_SIZE);

    swl_mapGeneric_t map2;
    swl_mapGeneric_init(&map2);
    swl_map_fromChar(&map2, dataSetPartStr, true);
    CHECK_ENTRIES(&map2, dataSet, DATASET_PART_SIZE, DATASET_SIZE);

    swl_map_fromChar(&map2, dataSetStr, false);
    CHECK_ENTRIES(&map2, dataSet, DATASET_SIZE, DATASET_SIZE);

    assert_true(swl_map_equals(&map2, &map1));

    swl_map_fromChar(&map2, dataSetOverWriteStr, true);
    CHECK_ENTRIES(&map2, dataSet_overwrite, DATASET_SIZE, DATASET_SIZE);
    assert_false(swl_map_equals(&map2, &map1));

    swl_map_clear(&map1);
    CHECK_ENTRIES(&map1, dataSet, 0, DATASET_SIZE);

    swl_map_fromChar(&map1, dataSetPartStr, true);
    CHECK_ENTRIES(&map1, dataSet, DATASET_PART_SIZE, DATASET_SIZE);
    assert_false(swl_map_equals(&map2, &map1));

    swl_map_clear(&map1);
    swl_map_clear(&map2);

    swl_map_fromChar(&map1, dataSetStr, true);
    swl_map_fromChar(&map2, dataSetPartStr, true);
    assert_false(swl_map_equals(&map1, &map2));
    swl_map_clear(&map2);

    assert_false(swl_map_equals(&map1, &map2));

    swl_map_cleanup(&map1);
    swl_map_cleanup(&map2);
}

static void test_swl_mapGeneric_alloc(void** state _UNUSED) {
    swl_mapGeneric_t map1;
    swl_mapGeneric_init(&map1);

    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        swl_mapEntry_t* entry = swl_mapGeneric_alloc(&map1);
        assert_non_null(entry);
        swl_type_copyTo(map1.keyType, swl_map_getEntryKeyRef(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.keyType, &dataSet[i].key));
        swl_type_copyTo(map1.valueType, swl_map_getEntryValueRef(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.valueType, &dataSet[i].val));

        swl_ttb_assertTypeEquals(map1.keyType, swl_map_getEntryKeyValue(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.keyType, &dataSet[i].key));
        swl_ttb_assertTypeEquals(map1.valueType, swl_map_getEntryValueValue(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.valueType, &dataSet[i].val));

        CHECK_ENTRIES(&map1, dataSet, i + 1, DATASET_SIZE);
    }

    swl_map_cleanup(&map1);
}

static void test_swl_mapGeneric_getDeleteEntry(void** state _UNUSED) {
    swl_mapGeneric_t map1;
    swl_mapGeneric_init(&map1);

    swl_map_fromChar(&map1, dataSetStr, false);


    int i = 0;
    for(i = 0; i < DATASET_SIZE; i++) {
        CHECK_ENTRIES(&map1, &dataSet[i], DATASET_SIZE - i, DATASET_SIZE - i);

        swl_mapEntry_t* entry = swl_mapGeneric_getEntry(&map1, dataSet[i].key);
        assert_non_null(entry);
        swl_ttb_assertTypeEquals(map1.keyType, swl_map_getEntryKeyValue(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.keyType, &dataSet[i].key));
        swl_ttb_assertTypeEquals(map1.valueType, swl_map_getEntryValueValue(&map1, entry), SWL_TYPE_EL_TO_DATA(map1.valueType, &dataSet[i].val));

        swl_mapGeneric_deleteEntry(&map1, entry);
        entry = swl_mapGeneric_getEntry(&map1, dataSet[i].key);
        assert_null(entry);
        CHECK_ENTRIES(&map1, &dataSet[i + 1], DATASET_SIZE - i - 1, DATASET_SIZE - i - 1);
    }

    swl_map_cleanup(&map1);
}

static void test_swl_mapGeneric_copy(void** state _UNUSED) {
    swl_mapGeneric_t map1;
    swl_mapGeneric_init(&map1);

    swl_map_fromChar(&map1, dataSetPartStr, true);
    CHECK_ENTRIES(&map1, dataSet, DATASET_PART_SIZE, DATASET_SIZE);

    swl_mapGeneric_t map2;
    swl_mapGeneric_init(&map2);
    swl_map_copyTo(&map2, &map1);
    CHECK_ENTRIES(&map2, dataSet, DATASET_PART_SIZE, DATASET_SIZE);

    assert_true(swl_map_equals(&map2, &map1));

    swl_map_cleanup(&map1);
    swl_map_cleanup(&map2);
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_mapGeneric_addGet),
        cmocka_unit_test(test_swl_mapGeneric_addOrSet),
        cmocka_unit_test(test_swl_mapGeneric_getCharFromChar),
        cmocka_unit_test(test_swl_mapGeneric_toChar),
        cmocka_unit_test(test_swl_mapGeneric_toCharInvalid),
        cmocka_unit_test(test_swl_mapGeneric_fromChar),
        cmocka_unit_test(test_swl_mapGeneric_fromCharInvalid),
        cmocka_unit_test(test_swl_mapGeneric_fromCharOverWrite),
        cmocka_unit_test(test_swl_mapGeneric_delete),
        cmocka_unit_test(test_swl_mapGeneric_clear),
        cmocka_unit_test(test_swl_mapGeneric_forEach),
        cmocka_unit_test(test_swl_mapGeneric_set),
        cmocka_unit_test(test_swl_mapGeneric_compare),
        cmocka_unit_test(test_swl_mapGeneric_alloc),
        cmocka_unit_test(test_swl_mapGeneric_getDeleteEntry),
        cmocka_unit_test(test_swl_mapGeneric_copy),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
