/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

typedef struct {
    int32_t key;
    int32_t val;
} myTestData;

#define DATASET_SIZE 5

myTestData dataSet[DATASET_SIZE] = {
    {.key = 1, .val = 10},
    {.key = 2, .val = 20},
    {.key = -11, .val = 50},
    {.key = 940, .val = -101},
    {.key = -5, .val = -99},
};
const char* dataSetStr = "1=10,2=20,-11=50,940=-101,-5=-99";
#define DATASET_PART_SIZE 2

const char* dataSetPartStr = "1=10,2=20";
#define DATASET_OVERWRITE_SIZE 5

static myTestData dataSet_overwrite[DATASET_OVERWRITE_SIZE] = {
    {.key = 1, .val = 40},
    {.key = 2, .val = 1},
    {.key = -11, .val = 242},
    {.key = 940, .val = -10001},
    {.key = -5, .val = -90},
};
const char* dataSetOverWriteStr = "1=40,2=1,-11=242,940=-10001,-5=-90";

#define NR_INVALID_STR 3
const char* invalidStr[NR_INVALID_STR] = {
    "1=40=,2=1,-11=242,940=-10001,-5=-90",
    "1=40,2,=1,-11=242,940=-10001,-5=-90",
    NULL,
};

#define ASSERT_KEY_EQUAL assert_int_equal
#define ASSERT_VAL_EQUAL assert_int_equal
#define ASSERT_VAL_DEFAULT(val) assert_int_equal(0, val)
