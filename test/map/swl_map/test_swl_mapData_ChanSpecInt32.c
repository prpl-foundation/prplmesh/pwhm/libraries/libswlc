/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

typedef struct {
    swl_chanspec_t key;
    int val;
} myTestData;

#define DATASET_SIZE 5

static myTestData dataSet[DATASET_SIZE] = {
    {.key.channel = 1, .key.bandwidth = SWL_BW_20MHZ, .val = 100},
    {.key.channel = 5, .key.bandwidth = SWL_BW_40MHZ, .val = 200},
    {.key.channel = 6, .key.bandwidth = SWL_BW_80MHZ, .val = 300},
    {.key.channel = 11, .key.bandwidth = SWL_BW_160MHZ, .val = 400},
    {.key.channel = 13, .key.bandwidth = SWL_BW_20MHZ, .val = 500},
};


const char* dataSetStr = "1/20=100,5/40=200,6/80=300,11/160=400,13/20=500";

#define DATASET_PART_SIZE 2

const char* dataSetPartStr = "1/20=100,5/40=200";
#define DATASET_OVERWRITE_SIZE 5

const char* sample = "";
static myTestData dataSet_overwrite[DATASET_OVERWRITE_SIZE] = {
    {.key.channel = 1, .key.bandwidth = SWL_BW_20MHZ, .val = 100},
    {.key.channel = 5, .key.bandwidth = SWL_BW_40MHZ, .val = 200},
    {.key.channel = 6, .key.bandwidth = SWL_BW_80MHZ, .val = 50},
    {.key.channel = 11, .key.bandwidth = SWL_BW_160MHZ, .val = 400},
    {.key.channel = 13, .key.bandwidth = SWL_BW_20MHZ, .val = 10},
};
const char* dataSetOverWriteStr = "1/20=100,5/40=200,6/80=50,11/160=400,13/20=10";

#define NR_INVALID_STR 4
const char* invalidStr[NR_INVALID_STR] = {
    "1/20=100=,5/40=200,6/80=50,11/160=400,13/20=10",
    "1/20=100,5,/40=200,6/80=50,11/160=400,13/20=10",
    "1-20=100,5,/40=200,6/80=50,11/160=400,13/20=10",
    NULL,
};

void assert_chanspec_equal(swl_chanspec_t spec1, swl_chanspec_t spec2) {
    assert_int_equal(spec1.channel, spec2.channel);
    assert_int_equal(spec1.bandwidth, spec2.bandwidth);
}

#define ASSERT_KEY_EQUAL assert_chanspec_equal
#define ASSERT_VAL_EQUAL assert_int_equal
#define ASSERT_VAL_DEFAULT(val) assert_int_equal(0, val)
