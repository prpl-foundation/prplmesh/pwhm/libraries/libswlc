/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_maps.h"
#include "swl/swl_common_type.h"
#include "swl/map/swl_mapCharGen.h"

typedef struct {
    char* key;
    char* value;
} testStruct_t;

typedef struct {
    char* charVal1;
    uint32_t uintVal1;
    char* charVal2;
    uint32_t uintVal2;
    char charVal3[16];
} testObject_t;

static testObject_t testObj1 = {"first", 11, "second", 2222, "third"};
static testObject_t testObj2 = {"Other", 911, "Next", 92222, "Last"};


bool s_entry(char* buffer, size_t bufSize, const char* keyName, void* userData _UNUSED) {
    snprintf(buffer, bufSize, "%s_%s", keyName, "testVal");
    return true;
}

bool s_printTest(char* buffer, size_t bufSize, const char* keyName _UNUSED, testObject_t* data) {
    snprintf(buffer, bufSize, "%s_%u_%s_%u", data->charVal1, data->uintVal1, data->charVal2, data->uintVal2);
    return true;
}

static swl_mapCharGen_entry_t s_entryList1[] = {
    {.name = "test", .val = "1234"},
    {.name = "test2", .val = "6789"},
    {.name = "foo", .val = "bar"},
    {.name = "funTest1", .callbackFun = s_entry},
    {.name = "!@#$%^&*()_+}{\":?><,./;'[]\\|", .callbackFun = s_entry},
    {.name = "entries", .callbackFun = (swl_mapCharGen_cbFun_t) s_printTest},
    {.name = "offset1", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal1)},
    {.name = "offset2", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal1)},
    {.name = "offset3", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal2)},
    {.name = "offset4", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal2)},
    {.name = "offset5", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal3), .isEmbeddedObj = true},
};

static testStruct_t s_testList1[] = {
    {"test", "1234"},
    {"test2", "6789"},
    {"foo", "bar"},
    {"funTest1", "funTest1_testVal"},
    {"!@#$%^&*()_+}{\":?><,./;'[]\\|", "!@#$%^&*()_+}{\":?><,./;'[]\\|_testVal"},
    {"entries", "first_11_second_2222"},
    {"offset1", "first"},
    {"offset2", "11"},
    {"offset3", "second"},
    {"offset4", "2222"},
    {"offset5", "third"},
};

static testStruct_t s_testList1Obj2[] = {
    {"test", "1234"},
    {"test2", "6789"},
    {"foo", "bar"},
    {"funTest1", "funTest1_testVal"},
    {"!@#$%^&*()_+}{\":?><,./;'[]\\|", "!@#$%^&*()_+}{\":?><,./;'[]\\|_testVal"},
    {"entries", "Other_911_Next_92222"},
    {"offset1", "Other"},
    {"offset2", "911"},
    {"offset3", "Next"},
    {"offset4", "92222"},
    {"offset5", "Last"},
};


static swl_mapCharGen_entry_t s_entryList2[] = {
    {.name = "other_test", .val = "1234"},
    {.name = "test2", .val = "9876"},
    {.name = "other_foo", .val = "bar"},
    {.name = "other_funTest1", .callbackFun = s_entry},
    {.name = "offset1", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal1)},
    {.name = "offset2", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal1)},
    {.name = "offset3", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal2)},
    {.name = "offset4", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal2)},
    {.name = "offset5", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal3), .isEmbeddedObj = true},
};

static testStruct_t s_testList2[] = {
    {"other_test", "1234"},
    {"test2", "9876"},
    {"other_foo", "bar"},
    {"other_funTest1", "other_funTest1_testVal"},
    {"offset1", "Other"},
    {"offset2", "911"},
    {"offset3", "Next"},
    {"offset4", "92222"},
    {"offset5", "Last"},
};

static swl_mapCharGen_data_t s_test1Data = {
    .entries = s_entryList1,
    .nrEntries = SWL_ARRAY_SIZE(s_entryList1),
    .renameKey = NULL,
};

static swl_mapCharGen_data_t s_test2Data = {
    .entries = s_entryList2,
    .nrEntries = SWL_ARRAY_SIZE(s_testList2),
    .renameKey = NULL,
};


static swl_mapCharGen_entry_t s_entryListReplace[] = {
    {.name = "test_<rad>", .val = "1234"},
    {.name = "test2_<rad>", .val = "6789"},
    {.name = "foo_<rad>", .val = "bar"},
    {.name = "funTest1_<rad>", .callbackFun = s_entry},
    {.name = "!@#$%^&*()_+}{\":?><,./;'[]\\|_<rad>", .callbackFun = s_entry},
    {.name = "entries_<rad>", .callbackFun = (swl_mapCharGen_cbFun_t) s_printTest},
    {.name = "offset1_<rad>", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal1)},
    {.name = "offset2_<rad>", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal1)},
    {.name = "offset3_<rad>", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal2)},
    {.name = "offset4_<rad>", .valueType = swl_type_uint32, .valueOffset = offsetof(testObject_t, uintVal2)},
    {.name = "offset5_<rad>", .valueType = swl_type_charPtr, .valueOffset = offsetof(testObject_t, charVal3), .isEmbeddedObj = true},
};

static swl_mapCharGen_data_t s_testDataReplace = {
    .entries = s_entryListReplace,
    .nrEntries = SWL_ARRAY_SIZE(s_entryListReplace),
    .renameKey = "<rad>",
};


static testStruct_t s_testListReplace1[] = {
    {"test_wifi0", "1234"},
    {"test2_wifi0", "6789"},
    {"foo_wifi0", "bar"},
    {"funTest1_wifi0", "funTest1_<rad>_testVal"},
    {"!@#$%^&*()_+}{\":?><,./;'[]\\|_wifi0", "!@#$%^&*()_+}{\":?><,./;'[]\\|_<rad>_testVal"},
    {"entries_wifi0", "first_11_second_2222"},
    {"offset1_wifi0", "first"},
    {"offset2_wifi0", "11"},
    {"offset3_wifi0", "second"},
    {"offset4_wifi0", "2222"},
    {"offset5_wifi0", "third"},
};

static testStruct_t s_testListReplace2[] = {
    {"test_wifi1", "1234"},
    {"test2_wifi1", "6789"},
    {"foo_wifi1", "bar"},
    {"funTest1_wifi1", "funTest1_<rad>_testVal"},
    {"!@#$%^&*()_+}{\":?><,./;'[]\\|_wifi1", "!@#$%^&*()_+}{\":?><,./;'[]\\|_<rad>_testVal"},
    {"entries_wifi1", "Other_911_Next_92222"},
    {"offset1_wifi1", "Other"},
    {"offset2_wifi1", "911"},
    {"offset3_wifi1", "Next"},
    {"offset4_wifi1", "92222"},
    {"offset5_wifi1", "Last"},
};

static void checkMap(testStruct_t* testList, uint32_t nrEntry, swl_mapChar_t* map) {
    assert_int_equal(nrEntry, swl_map_size(map));

    for(uint32_t i = 0; i < nrEntry; i++) {
        char* val = swl_mapChar_get(map, testList[i].key);
        assert_non_null(val);
        assert_string_equal(val, testList[i].value);
    }
}



static void test_generateMapBasic(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);

    printf("check1\n");
    swl_mapCharGen_fillMapFromEntries(&map, &s_test1Data, &testObj1, false, NULL);
    checkMap(s_testList1, s_test1Data.nrEntries, &map);

    printf("check2\n");
    swl_mapCharGen_fillMapFromEntries(&map, &s_test1Data, &testObj2, false, NULL);
    checkMap(s_testList1Obj2, s_test1Data.nrEntries, &map);


    swl_mapChar_cleanup(&map);
}

static void test_generateMapClear(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);


    swl_mapCharGen_fillMapFromEntries(&map, &s_test1Data, &testObj1, false, NULL);
    swl_mapCharGen_fillMapFromEntries(&map, &s_test2Data, &testObj2, true, NULL);

    checkMap(s_testList2, SWL_ARRAY_SIZE(s_entryList2), &map);

    swl_mapChar_cleanup(&map);

}



static void test_generateMapReplace(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);

    printf("check1\n");
    swl_mapCharGen_fillMapFromEntries(&map, &s_testDataReplace, &testObj1, true, "wifi0");
    checkMap(s_testListReplace1, s_test1Data.nrEntries, &map);

    printf("check2\n");
    swl_mapCharGen_fillMapFromEntries(&map, &s_testDataReplace, &testObj2, true, "wifi1");
    checkMap(s_testListReplace2, s_test1Data.nrEntries, &map);


    swl_mapChar_cleanup(&map);
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_generateMapBasic),
        cmocka_unit_test(test_generateMapClear),
        cmocka_unit_test(test_generateMapReplace),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
