/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_maps.h"
#include "swl/swl_common_type.h"
#include "swl/map/swl_mapCharFmt.h"

#define CHECK_VAL(pMap, key, expecValStr) \
    { \
        char* valStr = (char*) swl_mapChar_get(pMap, key); \
        assert_non_null(valStr); \
        assert_string_equal(valStr, expecValStr); \
    }
#define CHECK_VAL_STR(pMap, key, expecValStr, valStrFmt, ...) \
    { \
        assert_true(swl_mapCharFmt_addValStr(pMap, key, valStrFmt, __VA_ARGS__)); \
        CHECK_VAL(pMap, key, expecValStr); \
    }
#define CHECK_VAL_INT32(pMap, key, expecValStr, valInt32) \
    { \
        assert_true(swl_mapCharFmt_addValInt32(pMap, key, valInt32)); \
        CHECK_VAL(pMap, key, expecValStr); \
    }

static void test_formatMapValStr(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);
    CHECK_VAL_STR(&map, "key1", "dec/255/hex/ff", "%s/%d/%s/%02x", "dec", 255, "hex", 255);
    CHECK_VAL_STR(&map, "key2", "1+1=2", "%d%c%d=%d", 1, '+', 1, (1 + 1));
    swl_mapChar_cleanup(&map);
}

static void test_formatMapValInt32(void** state _UNUSED) {
    swl_mapChar_t map;
    swl_mapChar_init(&map);
    CHECK_VAL_INT32(&map, "key1", "12345678", 12345678);
    CHECK_VAL_INT32(&map, "key1", "-12345678", -12345678);
    CHECK_VAL_INT32(&map, "key2", "-1", 0xffffffff);
    swl_mapChar_cleanup(&map);
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_formatMapValStr),
        cmocka_unit_test(test_formatMapValInt32),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
