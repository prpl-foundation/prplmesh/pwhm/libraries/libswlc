/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <openssl/md5.h>

#include <debug/sahtrace.h>

#include "swl/swl_common.h"
#include "swl/swl_hash.h"
#include "swl/swl_hex.h"

static void test_md5(void** state _UNUSED) {
    const swl_bit8_t frame[] = "AABBCCDDEE";
    swl_bit8_t hash[MD5_DIGEST_LENGTH] = {'\0'};

    swl_rc_ne ret = swl_hash_rawToMD5(NULL, MD5_DIGEST_LENGTH, frame, strlen((char*) frame));
    assert_int_equal(ret, SWL_RC_ERROR);
    ret = swl_hash_rawToMD5(hash, MD5_DIGEST_LENGTH + 5, frame, strlen((char*) frame));
    assert_int_equal(ret, SWL_RC_OK);
    ret = swl_hash_rawToMD5(hash, MD5_DIGEST_LENGTH - 5, frame, strlen((char*) frame));
    assert_int_equal(ret, SWL_RC_ERROR);
    ret = swl_hash_rawToMD5(hash, MD5_DIGEST_LENGTH, NULL, strlen((char*) frame));
    assert_int_equal(ret, SWL_RC_ERROR);
    ret = swl_hash_rawToMD5(hash, MD5_DIGEST_LENGTH, frame, 0);
    assert_int_equal(ret, SWL_RC_ERROR);

    ret = swl_hash_rawToMD5(hash, sizeof(hash), frame, strlen((char*) frame));
    assert_int_equal(ret, SWL_RC_OK);

    char md5HashStr[33] = {'\0'};
    swl_hex_fromBytes(md5HashStr, sizeof(md5HashStr), hash, MD5_DIGEST_LENGTH, 0);
    assert_string_equal(md5HashStr, "92890464a544a3900d075df17b920445");
}

static void test_hexToMd5Hex(void** state _UNUSED) {
    const char frame[] = "665522";
    char md5HashStr[33] = {'\0'};

    bool ret = swl_hash_hexToMD5Hex(NULL, md5HashStr, sizeof(md5HashStr), false);
    assert_false(ret);
    ret = swl_hash_hexToMD5Hex(frame, NULL, sizeof(md5HashStr), false);
    assert_false(ret);
    ret = swl_hash_hexToMD5Hex(frame, md5HashStr, 0, false);
    assert_false(ret);
    ret = swl_hash_hexToMD5Hex(frame, md5HashStr, sizeof(md5HashStr), false);
    assert_true(ret);
    assert_string_equal(md5HashStr, "c2aa0486cdf59138de28a009d617361d");
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlhash");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_md5),
        cmocka_unit_test(test_hexToMd5Hex),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
