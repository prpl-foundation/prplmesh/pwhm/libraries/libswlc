/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_unLiList.h"

#include "swl/swl_common.h"
#include "swl/swl_common_tupleType.h"

static void createUnLiList(swl_unLiList_t* unLiList, uint32_t blockSize, uint32_t nrElem) {

    swl_unLiList_initExt(unLiList, blockSize, sizeof(uint32_t));
    uint32_t i = 0;
    for(i = 0; i < nrElem; i++) {
        swl_unLiList_add(unLiList, &i);
        assert_int_equal(i + 1, swl_unLiList_size(unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(unLiList));
    }
}

static void test_swl_unLiList_simpleCreate(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    swl_unLiList_initExt(&unLiList, 5, sizeof(uint32_t));
    assert_int_equal(0, swl_unLiList_size(&unLiList));
    assert_int_equal(0, swl_unLiList_getNrBlocks(&unLiList));
    uint32_t val1 = 5;
    swl_unLiList_add(&unLiList, &val1);
    assert_int_equal(1, swl_unLiList_size(&unLiList));
    assert_int_equal(1, swl_unLiList_getNrBlocks(&unLiList));

    uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
    assert_int_equal(val1, *data);

    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(0, swl_unLiList_size(&unLiList));
    assert_int_equal(0, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_longCreate(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 21;
    uint32_t blockSize = 3;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(uint32_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &i);
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 0; i < nrElToCreate; i++) {
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_int_equal(i, *data);

        swl_unLiList_remove(&unLiList, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
    }

    swl_unLiList_destroy(&unLiList);
}



static void test_swl_unLiList_testCompressFirst(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 8;
    uint32_t blockSize = 4;

    createUnLiList(&unLiList, blockSize, nrElToCreate);

    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 7);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 6);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));

    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(1, swl_unLiList_getNrBlocks(&unLiList));


    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testCompressLast(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 8;
    uint32_t blockSize = 4;

    createUnLiList(&unLiList, blockSize, nrElToCreate);

    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));

    swl_unLiList_remove(&unLiList, 5);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 4);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 3);
    assert_int_equal(1, swl_unLiList_getNrBlocks(&unLiList));


    swl_unLiList_destroy(&unLiList);
}


static void test_swl_unLiList_testCompressMiddleSplit(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 18;
    uint32_t blockSize = 6;

    createUnLiList(&unLiList, blockSize, nrElToCreate);

    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 0);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));

    swl_unLiList_remove(&unLiList, -1);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, -1);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));

    // 4 in first, 6 in second, 4 in last => 3rd remove from second should trigger cleanup
    swl_unLiList_remove(&unLiList, 4);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 4);
    assert_int_equal(3, swl_unLiList_getNrBlocks(&unLiList));
    swl_unLiList_remove(&unLiList, 4);
    assert_int_equal(2, swl_unLiList_getNrBlocks(&unLiList));

    swl_unLiList_destroy(&unLiList);
}

typedef struct {
    uint32_t a;
    uint8_t b;
    char buf[18];
} test_unLiListData_t;

test_unLiListData_t emptyData = {0};

#define NR_TYPES 3
#define NR_VALUES 12

static const test_unLiListData_t testData[NR_VALUES] = {
    {0, 1, "abc"},
    {10, 2, "cde"},
    {20, 4, "fg"},
    {30, 8, "h"},
    {40, 16, "ijk"},
    {50, 32, "l"},
    {60, 64, "mno"},
    {70, 2, "pq"},
    {80, 3, "rst"},
    {90, 5, "uvw"},
    {1000, 9, "xyz"},
    {500, 100, "abcklmxyz"},
};



static void test_swl_unLiList_testStruct(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 12;
    uint32_t blockSize = 3;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(test_unLiListData_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &testData[i]);
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 0; i < nrElToCreate; i++) {
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_memory_equal(data, &testData[i], sizeof(test_unLiListData_t));

        swl_unLiList_remove(&unLiList, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
    }
    swl_unLiList_destroy(&unLiList);
}


static void test_swl_unLiList_testStructHalfDel(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 12;
    uint32_t blockSize = 3;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(test_unLiListData_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &testData[i]);
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 6; i < nrElToCreate; i++) {
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 6);
        assert_memory_equal(data, &testData[i], sizeof(test_unLiListData_t));

        swl_unLiList_remove(&unLiList, 6);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1 + 6, size);
        if(size >= 1) {
            assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
        } else {
            assert_int_equal(1, swl_unLiList_getNrBlocks(&unLiList));
        }
    }
    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testCreate(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 12;
    uint32_t blockSize = 3;
    test_unLiListData_t emptyData = {0};
    swl_unLiList_initExt(&unLiList, blockSize, sizeof(test_unLiListData_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        void* data = swl_unLiList_allocElement(&unLiList);
        assert_memory_equal(data, &emptyData, sizeof(test_unLiListData_t));
        memcpy(data, &testData[i], sizeof(test_unLiListData_t));
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 0; i < nrElToCreate; i++) {
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_memory_equal(data, &testData[i], sizeof(test_unLiListData_t));

        swl_unLiList_remove(&unLiList, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
    }

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testRemoveByPointer(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 21;
    uint32_t blockSize = 3;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(uint32_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &i);
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 0; i < nrElToCreate; i++) {

        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_int_equal(i, *data);

        uint32_t index = swl_unLiList_removeByPointer(&unLiList, data);
        assert_int_equal(index, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
    }

    swl_unLiList_destroy(&unLiList);
}
static void test_swl_unLiList_testRemoveByData(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 12;
    uint32_t blockSize = 3;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(test_unLiListData_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &testData[i]);
        assert_int_equal(i + 1, swl_unLiList_size(&unLiList));
        assert_int_equal(i / blockSize + 1, swl_unLiList_getNrBlocks(&unLiList));
    }

    for(i = 0; i < nrElToCreate; i++) {
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_memory_equal(data, &testData[i], sizeof(test_unLiListData_t));

        int32_t index = swl_unLiList_removeByData(&unLiList, &testData[i]);
        assert_int_equal(index, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));
    }
    swl_unLiList_destroy(&unLiList);
}


static void test_swl_unLiList_testRemove(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    uint32_t nrElToCreate = 5;
    uint32_t blockSize = 5;

    swl_unLiList_initExt(&unLiList, blockSize, sizeof(test_unLiListData_t));

    uint32_t i = 0;
    for(i = 0; i < nrElToCreate; i++) {
        swl_unLiList_add(&unLiList, &testData[i]);
    }

    for(i = 0; i < nrElToCreate; i++) {
        // note that blockSize needs to match nrElToCreate, otherwise clean will not be last element
        uint32_t* data = (uint32_t*) swl_unLiList_get(&unLiList, 0);
        assert_memory_equal(data, &testData[i], sizeof(test_unLiListData_t));

        uint32_t* lastData = (uint32_t*) swl_unLiList_get(&unLiList, -1);
        assert_memory_equal(lastData, &testData[nrElToCreate - 1], sizeof(test_unLiListData_t));

        swl_unLiList_remove(&unLiList, 0);
        uint32_t size = swl_unLiList_size(&unLiList);
        assert_int_equal(nrElToCreate - i - 1, size);
        assert_int_equal((size + blockSize - 1) / blockSize, swl_unLiList_getNrBlocks(&unLiList));

        if(i < nrElToCreate - 1) {
            assert_memory_equal(lastData, &emptyData, sizeof(test_unLiListData_t));
        }
    }

    swl_unLiList_destroy(&unLiList);
}


static void test_swl_unLiList_iterateEmpty(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    createUnLiList(&unLiList, 5, 0);

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &unLiList) {
        //should not come here
        assert_true(false);
    }
    assert_false(it.valid);

    swl_unLiList_destroy(&unLiList);
}
static void test_swl_unLiList_iterateElements(void** state _UNUSED) {

    swl_unLiList_t unLiList;
    createUnLiList(&unLiList, 5, 12);
    uint32_t index = 0;
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &unLiList) {
        assert_true(it.valid);
        uint32_t* data = it.data;
        assert_int_equal(*data, index);
        index++;
    }
    assert_false(it.valid);
    assert_int_equal(index, 12);

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_set(void** state _UNUSED) {

    swl_unLiList_t unLiList;
    uint32_t nrEl = 12;
    createUnLiList(&unLiList, 5, nrEl);

    uint32_t offSet = 50;
    uint32_t* val;

    uint32_t i = 0;
    for(i = 0; i < nrEl; i++) {
        val = swl_unLiList_get(&unLiList, i);
        assert_int_equal(i, *val);
        uint32_t setVal = i + offSet;
        swl_unLiList_set(&unLiList, i, &setVal);

        val = swl_unLiList_get(&unLiList, i);
        assert_int_equal(i + offSet, *val);
    }

    for(i = 0; i < nrEl; i++) {
        val = swl_unLiList_get(&unLiList, i);
        assert_int_equal(i + offSet, *val);
    }


    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_insertFirst(void** state _UNUSED) {

    swl_unLiList_t list;
    swl_unLiList_initExt(&list, 5, sizeof(uint32_t));
    uint32_t i = 0;
    uint32_t nrEl = 12;
    swl_unLiList_add(&list, &i);
    for(i = 1; i < nrEl; i++) {
        swl_unLiList_insert(&list, 0, &i);
    }

    for(i = 0; i < nrEl; i++) {
        uint32_t* val = swl_unLiList_get(&list, i);
        assert_non_null(val);
        assert_int_equal(nrEl - i - 1, *val);
    }

    swl_unLiList_destroy(&list);
}

static void test_swl_unLiList_insertLast(void** state _UNUSED) {
    swl_unLiList_t list;
    swl_unLiList_initExt(&list, 5, sizeof(uint32_t));
    uint32_t i = 0;
    uint32_t nrEl = 12;
    for(i = 0; i < nrEl; i++) {
        swl_unLiList_insert(&list, -1, &i);
    }

    for(i = 0; i < nrEl; i++) {
        uint32_t* val = swl_unLiList_get(&list, i);
        assert_non_null(val);
        assert_int_equal(i, *val);
    }

    swl_unLiList_destroy(&list);
}

static void test_swl_unLiList_insertMiddle(void** state _UNUSED) {
    swl_unLiList_t list;
    uint32_t i = 0;
    uint32_t nrEl = 12;
    uint32_t blockSize = 5;
    createUnLiList(&list, blockSize, nrEl);

    for(i = 0; i < blockSize; i++) {
        uint32_t val = i + nrEl + blockSize;
        swl_unLiList_insert(&list, i + blockSize, &val);
    }

    for(i = 0; i < blockSize; i++) {
        uint32_t* val = swl_unLiList_get(&list, i);
        assert_non_null(val);
        assert_int_equal(i + ( i >= blockSize && i < 2 * blockSize ? nrEl : 0), *val);
    }

    swl_unLiList_destroy(&list);
}

static void test_swl_unLiList_testContainsEmpty(void** state _UNUSED) {
    // Given an empty unLiList
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, sizeof(uint32_t));

    // When asking whether it contains stuff
    // Then the answer is always no.
    uint32_t number123 = 123;
    uint32_t number0 = 0;
    assert_false(swl_unLiList_contains(&unLiList, &number123));
    assert_false(swl_unLiList_contains(&unLiList, &number0));
    assert_false(swl_unLiList_contains(&unLiList, NULL));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testContainsNormalYesCase(void** state _UNUSED) {
    // Given an unLiList with some items in it
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    swl_unLiList_add(&unLiList, "red  ");
    swl_unLiList_add(&unLiList, "green");
    swl_unLiList_add(&unLiList, "blue ");

    // When asking whether it contains the items we added
    // Then the answer is yes
    assert_true(swl_unLiList_contains(&unLiList, "red   "));
    assert_true(swl_unLiList_contains(&unLiList, "greenDifferently"));
    assert_true(swl_unLiList_contains(&unLiList, "blue  "));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testContainsNormalNoCase(void** state _UNUSED) {
    // Given an unLiList with some items in it
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    swl_unLiList_add(&unLiList, "red  ");
    swl_unLiList_add(&unLiList, "green");
    swl_unLiList_add(&unLiList, "blue ");

    // When asking whether it contains items we did not add
    // Then the answer is no
    assert_false(swl_unLiList_contains(&unLiList, "yello"));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testContainsDeleted(void** state _UNUSED) {
    // Given an unLiList with an item deleted
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    swl_unLiList_add(&unLiList, "red  ");
    swl_unLiList_add(&unLiList, "green");
    swl_unLiList_add(&unLiList, "blue ");
    swl_unLiList_removeByData(&unLiList, "red  ");
    swl_unLiList_removeByData(&unLiList, "green1");

    // When asking whether it contains the items we deleted
    // Then the answer is no
    assert_false(swl_unLiList_contains(&unLiList, "red  "));
    assert_false(swl_unLiList_contains(&unLiList, "red  2"));
    assert_false(swl_unLiList_contains(&unLiList, "green"));
    assert_false(swl_unLiList_contains(&unLiList, "green1"));
    assert_false(swl_unLiList_contains(&unLiList, "green2"));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testContainsDuplicate(void** state _UNUSED) {
    // Given an unLiList with duplicate items
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    const char* red = "red  ";
    swl_unLiList_add(&unLiList, red);
    swl_unLiList_add(&unLiList, "green");
    swl_unLiList_add(&unLiList, "pink ");
    swl_unLiList_add(&unLiList, "blue ");
    swl_unLiList_add(&unLiList, red);
    swl_unLiList_add(&unLiList, "green");

    // When asking whether it contains the duplicate item
    // Then the answer is yes.
    assert_true(swl_unLiList_contains(&unLiList, "green"));
    assert_true(swl_unLiList_contains(&unLiList, red));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testContainsNull(void** state _UNUSED) {
    // Given an unLiList with some items
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    swl_unLiList_add(&unLiList, "green");
    swl_unLiList_add(&unLiList, "pink ");

    // When asking whether it contains NULL
    // Then the answer is no.
    assert_false(swl_unLiList_contains(&unLiList, NULL));

    swl_unLiList_destroy(&unLiList);
}

static void test_swl_unLiList_testKeepLastBlock(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    swl_unLiList_init(&unLiList, 5);
    swl_unLiList_setKeepsLastBlock(&unLiList, false);
    assert_true(swl_llist_size(&unLiList.blocks) == 0);
    assert_int_equal(swl_unLiList_size(&unLiList), 0);

    void* data = swl_unLiList_allocElement(&unLiList);
    assert_true(swl_llist_size(&unLiList.blocks) == 1);
    assert_int_equal(swl_unLiList_size(&unLiList), 1);
    swl_unLiList_removeByPointer(&unLiList, data);
    assert_true(swl_llist_size(&unLiList.blocks) == 0);
    assert_int_equal(swl_unLiList_size(&unLiList), 0);

    swl_unLiList_setKeepsLastBlock(&unLiList, true);
    assert_true(swl_llist_size(&unLiList.blocks) == 0);
    assert_int_equal(swl_unLiList_size(&unLiList), 0);

    data = swl_unLiList_allocElement(&unLiList);
    assert_true(swl_llist_size(&unLiList.blocks) == 1);
    assert_int_equal(swl_unLiList_size(&unLiList), 1);
    swl_unLiList_removeByPointer(&unLiList, data);
    assert_true(swl_llist_size(&unLiList.blocks) == 1);
    assert_int_equal(swl_unLiList_size(&unLiList), 0);



    swl_unLiList_setKeepsLastBlock(&unLiList, false);
    assert_true(swl_llist_size(&unLiList.blocks) == 0);
    assert_int_equal(swl_unLiList_size(&unLiList), 0);
}

static void test_swl_unLiList_testKeepLastBlockIt(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    createUnLiList(&unLiList, 2, 0);
    swl_unLiList_setKeepsLastBlock(&unLiList, true);

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &unLiList) {
        //should not come here
        assert_true(false);
    }
    assert_false(it.valid);

    int nrVal = 5;

    for(int i = 0; i < nrVal; i++) {
        uint32_t data = i;
        swl_unLiList_add(&unLiList, &data);
    }

    uint32_t count = 0;
    swl_unLiList_for_each(it, &unLiList) {
        count += 1;
    }
    assert_int_equal(count, nrVal);

    for(int i = 0; i < nrVal; i++) {
        swl_unLiList_remove(&unLiList, 0);
    }

    assert_int_equal(swl_unLiList_size(&unLiList), 0);
    swl_unLiList_for_each(it, &unLiList) {
        //should not come here
        assert_true(false);
    }

    swl_unLiList_destroy(&unLiList);
}

void containsElements(swl_unLiList_t* unLiList, int* elementList, int nrElement) {
    assert_int_equal(swl_unLiList_size(unLiList), nrElement);
    for(int i = 0; i < nrElement; i++) {
        assert_true(swl_unLiList_contains(unLiList, &elementList[i]));
    }
}

static void test_swl_unLiList_testDelIt(void** state _UNUSED) {
    swl_unLiList_t unLiList;
    createUnLiList(&unLiList, 5, 12);
    int index = 0;

    // test delete middle

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &unLiList) {

        assert_int_equal(*(uint32_t*) it.data, index);
        if(index == 6) {
            swl_unLiList_delIt(&it);
        }
        index++;
    }
    assert_int_equal(index, 12);

    int presentList1[11] = {0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11};
    containsElements(&unLiList, presentList1, 11);

    // test rm first
    index = 0;
    swl_unLiList_for_each(it, &unLiList) {
        assert_int_equal(*(uint32_t*) it.data, presentList1[index]);
        if(index == 0) {
            swl_unLiList_delIt(&it);
        }
        index++;
    }
    assert_int_equal(index, 11);
    int presentList2[10] = {1, 2, 3, 4, 5, 7, 8, 9, 10, 11};
    containsElements(&unLiList, presentList2, 10);

    // test rm last
    index = 0;
    swl_unLiList_for_each(it, &unLiList) {
        assert_int_equal(*(uint32_t*) it.data, presentList2[index]);
        if(index == 9) {
            swl_unLiList_delIt(&it);
        }
        index++;
    }
    assert_int_equal(index, 10);
    int presentList3[9] = {1, 2, 3, 4, 5, 7, 8, 9, 10};
    containsElements(&unLiList, presentList3, 9);

    // delete all
    swl_unLiList_for_each(it, &unLiList) {
        swl_unLiList_delIt(&it);
    }
    assert_int_equal(swl_unLiList_size(&unLiList), 0);


    swl_unLiList_destroy(&unLiList);
}

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlConv");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_unLiList_simpleCreate),
        cmocka_unit_test(test_swl_unLiList_longCreate),
        cmocka_unit_test(test_swl_unLiList_testCompressFirst),
        cmocka_unit_test(test_swl_unLiList_testCompressLast),
        cmocka_unit_test(test_swl_unLiList_testCompressMiddleSplit),
        cmocka_unit_test(test_swl_unLiList_testStruct),
        cmocka_unit_test(test_swl_unLiList_testStructHalfDel),
        cmocka_unit_test(test_swl_unLiList_testCreate),
        cmocka_unit_test(test_swl_unLiList_testRemoveByPointer),
        cmocka_unit_test(test_swl_unLiList_testRemoveByData),
        cmocka_unit_test(test_swl_unLiList_testRemove),
        cmocka_unit_test(test_swl_unLiList_iterateEmpty),
        cmocka_unit_test(test_swl_unLiList_iterateElements),
        cmocka_unit_test(test_swl_unLiList_set),
        cmocka_unit_test(test_swl_unLiList_insertFirst),
        cmocka_unit_test(test_swl_unLiList_insertLast),
        cmocka_unit_test(test_swl_unLiList_insertMiddle),
        cmocka_unit_test(test_swl_unLiList_testContainsEmpty),
        cmocka_unit_test(test_swl_unLiList_testContainsNormalYesCase),
        cmocka_unit_test(test_swl_unLiList_testContainsNormalNoCase),
        cmocka_unit_test(test_swl_unLiList_testContainsDeleted),
        cmocka_unit_test(test_swl_unLiList_testContainsDuplicate),
        cmocka_unit_test(test_swl_unLiList_testContainsNull),
        cmocka_unit_test(test_swl_unLiList_testKeepLastBlock),
        cmocka_unit_test(test_swl_unLiList_testKeepLastBlockIt),
        cmocka_unit_test(test_swl_unLiList_testDelIt),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
