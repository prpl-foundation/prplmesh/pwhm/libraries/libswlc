/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include <cmocka.h>
#include <swl/swl_80211.h>
#include <swl/swl_common.h>

// Helper functions to visualize bitmasks, from right to left, of 8, 16, etc length data.
void printBits(uint8_t numbits, char* whatdata, uint64_t value) {
    printf("\n=== bits of %s:\n", whatdata);
    for(int i = numbits - 1; i > -1; i--) {
        if(!(i % 2 )) {
            printf("%d ", SWL_BIT_IS_SET(value, i));
        } else {
            printf("%2d", SWL_BIT_IS_SET(value, i));
        }
        if((!(i % 8)) && (i > 0 )) {
            printf(" - ");
        }
    }
    printf("\n");
}

void printBits32(char* whatdata, uint32_t value) {
    printBits(32, whatdata, value);
}

void printBits16(char* whatdata, uint16_t value) {
    printBits(16, whatdata, value);
}

void printBits8(char* whatdata, uint8_t value) {
    printBits(8, whatdata, value);
}

#define BUFF_SIZE 32     // max is around 28
uint8_t buf[BUFF_SIZE];

static uint32_t loadFrame(uint8_t buf[BUFF_SIZE], char* binFileName) {
    memset(buf, 0, BUFF_SIZE);
    FILE* fp = fopen(binFileName, "rb");
    assert_non_null(fp);

    fseek(fp, 0, SEEK_END);
    long flen = ftell(fp);
    assert_true(flen < BUFF_SIZE);
    rewind(fp);

    fread(buf, flen, 1, fp);
    fclose(fp);
    return flen;
}


/**
 * NOTE:
 * we compare different structure members' values with hardcoded ones.
 * Hardcoded values are the values in the binary frame snippets,
 * as shown by wireshark, hence, the expected ones.
 */

void test_swl_80211_parseHtCap(void** state _UNUSED) {
    memset(buf, 0, BUFF_SIZE);
    uint32_t flen = loadFrame(buf, "11axProbeHT.bin"); loadFrame(buf, "11axProbeHT.bin");
    assert_int_equal(flen, 28);

    /** Wireshark capinfo snapshot bits on/off => value 0x6f:
     * 0    0    0    0    0    0    0    0   -   0   1    1    0    1    1    1    1
     * bits 1, 5 and 6: all in the snapshot. M_SWL_80211_HTCAPINFO_CAP_40 0x0002, etc..
     */
    swl_80211_htCapIE_t swlCap = {0};
    swl_80211_parseHtCap(&swlCap, buf);

    assert_int_equal(swlCap.htCapInfo, 0x6f);

    assert_int_equal(swlCap.supMCSSet[0], 0xff);
    for(size_t i = 1; i < 8; i++) {
        assert_int_equal(swlCap.supMCSSet[i], 0x00);
    }
}


void test_swl_80211_parseVhtCap(void** state _UNUSED) {
    memset(buf, 0, BUFF_SIZE);
    uint32_t flen = 0;
    flen = loadFrame(buf, "11axProbeVHT.bin");
    assert_int_equal(flen, 14);

    swl_80211_vhtCapIE_t swlCap = {0};
    swl_80211_parseVhtCap(&swlCap, buf);

    // capinfo in WS snapshot makes 0x0f807032:
    // 0 0  0 0  1 1  1 1  -  1 0  0 0  0 0  0 0  -  0 1  1 1  0 0  0 0  -  0 0  1 1  0 0  1 0
    assert_int_equal(swlCap.vhtCapInfo, 0x0f807032);

    // WireShark Rx and TX masks are 0xfffe, rate maps are 0x0000
    assert_int_equal(0xfffe, swlCap.rxMcsMap);
    assert_int_equal(0xfffe, swlCap.txMcsMap);
    assert_int_equal(0x0, swlCap.rxRateMap);
    assert_int_equal(0x0, swlCap.txRateMap);
}


void test_swl_80211_parseHeCap(void** state _UNUSED) {
    memset(buf, 0, BUFF_SIZE);
    uint32_t flen = 0;
    flen = loadFrame(buf, "11axProbeHE.bin");
    assert_int_equal(flen, 28);

    swl_80211_heCapIE_t heCapIE = {0};
    swl_80211_parseHeCap(&heCapIE, buf);

    assert_int_equal(heCapIE.heCaps, 0x23);

    assert_int_equal(heCapIE.macCap.cap[0], 0x1);
    assert_int_equal(heCapIE.macCap.cap[1], 0x08);
    assert_int_equal(heCapIE.macCap.cap[2], 0x08);
    assert_int_equal(heCapIE.macCap.cap[3], 0x00);
    assert_int_equal(heCapIE.macCap.cap[4], 0x00);
    assert_int_equal(heCapIE.macCap.cap[5], 0x80);

    assert_int_equal(heCapIE.phyCap.cap[0], 0x44);
    assert_int_equal(*(uint16_t*) &heCapIE.phyCap.cap[1], 0x0230);
    assert_int_equal(*(uint16_t*) &heCapIE.phyCap.cap[3], 0x1d00);
    assert_int_equal(*(uint16_t*) &heCapIE.phyCap.cap[5], 0x9f00);
    assert_int_equal(*(uint16_t*) &heCapIE.phyCap.cap[7], 0x0008);
    assert_int_equal(*(uint16_t*) &heCapIE.phyCap.cap[9], 0x000c);

    assert_int_equal(heCapIE.mcsCaps[0].rxMcsMap, 0xfffe);
    assert_int_equal(heCapIE.mcsCaps[0].txMcsMap, 0xfffe);
    assert_int_equal(heCapIE.mcsCaps[1].rxMcsMap, 0);
    assert_int_equal(heCapIE.mcsCaps[1].txMcsMap, 0);
    assert_int_equal(heCapIE.mcsCaps[2].rxMcsMap, 0);
    assert_int_equal(heCapIE.mcsCaps[2].txMcsMap, 0);
}

#define TEST_FILE "/tmp/test.txt"

static int setup_suite(void** state _UNUSED) {
    return 0;
}

static int teardown_suite(void** state _UNUSED) {
    return 0;
}


int main(int argc _UNUSED, char* argv[] _UNUSED) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_80211_parseHtCap),
        cmocka_unit_test(test_swl_80211_parseVhtCap),
        cmocka_unit_test(test_swl_80211_parseHeCap),
    };
    return cmocka_run_group_tests(tests, setup_suite, teardown_suite);
}
