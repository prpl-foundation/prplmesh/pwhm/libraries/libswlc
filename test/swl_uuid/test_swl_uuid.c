/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/swl_uuid.h"

static void test_swl_uuid_isValid(void** state _UNUSED) {
    assert_true(swl_uuid_isValidChar("21a3c520-9e98-11ea-b37a-b1532bdb851d"));    // v1
    assert_true(swl_uuid_isValidChar("96b50e81-1250-4a08-a59f-5cf164982b92"));    // v4
    assert_true(swl_uuid_isValidChar("F7C82F4E-6722-41E8-9F94-993A7475C691"));    // Uppercase
    assert_true(swl_uuid_isValidChar("00000000-0000-0000-0000-000000000000"));    // the nil UUID

    assert_false(swl_uuid_isValidChar("69a8cff0-9e98-11ea-b37a-b1532bdb851"));    // too short
    assert_false(swl_uuid_isValidChar("69a8cff0-9e98-11ea-b37a-b1532bdb851d1"));  // too long
    assert_false(swl_uuid_isValidChar("7e75-ef04f818-48ce-b70e-1c6f820df63b"));   // bad "-" position
    assert_false(swl_uuid_isValidChar("7e75ef04f81848ceb70e1c6f820df63b"));       // no dashes
    assert_false(swl_uuid_isValidChar("7e75ef04-f818-48ce-b70e-1c6f82-0df63b"));  // too many dashes
    assert_false(swl_uuid_isValidChar("f818-48ce-b70e-7e75ef04-1c6f820df63b"));   // too many dashes
    assert_false(swl_uuid_isValidChar("-f81848ce-b70e-7e75ef04-1c6f820df63b"));   // too many dashes
    assert_false(swl_uuid_isValidChar("f81848ce-b70e-7e75ef04-1c6f820df63b-"));   // too many dashes
    assert_false(swl_uuid_isValidChar("1829f0e0485f-5016-43df-ab54-bae3421e"));   // bad group order
    assert_false(swl_uuid_isValidChar(" ca1f4738-4610-4d41-a407-798abaed1dd7"));  // " " begin
    assert_false(swl_uuid_isValidChar("  1f4738-4610-4d41-a407-798abaed1dd7"));   // " " begin len ok
    assert_false(swl_uuid_isValidChar("ca1f4738-4610-4d41-a407-798abaed1dd7 "));  // " " end
    assert_false(swl_uuid_isValidChar("ca1f4738-4610-4d41-a407-798abaed1d  "));   // " " end len ok
    assert_false(swl_uuid_isValidChar("ca1f4738-4610-4d41-a407-798abaed1dg7"));   // "g" not hex
    assert_false(swl_uuid_isValidChar("ca1f47381461014d411a4071798abaed1dd7"));   // dash not '-'
    assert_false(swl_uuid_isValidChar("ca1f4738:4610:4d41:a407:798abaed1dd7"));   // dash not '-'
    assert_false(swl_uuid_isValidChar(""));                                       // empty string
    assert_false(swl_uuid_isValidChar(NULL));                                     // null
    assert_false(swl_uuid_isValidChar("0x96b50e81-1250-4a08-a59f-5cf164982b92")); // "0x" begin
    assert_false(swl_uuid_isValidChar("0x96b50e81-12-4a08-a59f-5cf164982b92"));   // "0x" len ok
    assert_false(swl_uuid_isValidChar("96b50e81-0x1250-4a08-a59f-5cf164982b92")); // "0x"@groupstart
    assert_false(swl_uuid_isValidChar("96b50e81-0x50-4a08-a59f-5cf164982b92"));   // "0x"@groupstart
    assert_false(swl_uuid_isValidChar("96b50e81-1250-4a08-a59f-5c0xf164982b92")); // "0x" in middle
    assert_false(swl_uuid_isValidChar("96b50e81-1250-4a08-a59f-5c0xf164982b"));   // "0x" in middle
}

static void test_swl_uuid_toChar(void** state _UNUSED) {
    // Normal case:
    char str[37];
    swl_uuid_t binary = { { 160, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 255 } };
    memset(str, 0, sizeof(str));
    bool ok = swl_uuid_toChar(str, sizeof(str), &binary);
    assert_true(ok);
    assert_true(0 == strcmp(str, "a0010203-0405-0607-0809-0a0b0c0d0eff"));

    // length of str one too short:
    ok = swl_uuid_toChar(str, sizeof(str) - 1, &binary);
    assert_false(ok);

    // length of str half too short:
    ok = swl_uuid_toChar(str, sizeof(str) / 2, &binary);
    assert_false(ok);

    // length of str zero:
    ok = swl_uuid_toChar(str, 0, &binary);
    assert_false(ok);
}

static void test_swl_uuid_fromChar(void** state _UNUSED) {
    // Normal case:
    swl_uuid_t expected = { { 160, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 255 } };
    swl_uuid_t target;
    bool ok = swl_uuid_fromChar(&target, "a0010203-0405-0607-0809-0a0b0c0d0eff");
    assert_true(ok);
    assert_true(swl_uuid_eq(&expected, &target));

    // Upper case:
    memset(&target, 0, sizeof(swl_uuid_t));
    ok = swl_uuid_fromChar(&target, "A0010203-0405-0607-0809-0A0B0C0D0EFF");
    assert_true(ok);
    assert_true(swl_uuid_eq(&expected, &target));

    // Too short:
    assert_false(swl_uuid_fromChar(&target, "a0010203-0405-0607-0809-0a0b0c0d0ef"));
    // Too long
    assert_false(swl_uuid_fromChar(&target, "a0010203-0405-0607-0809-0a0b0c0d0effab"));
    // Stray separators:
    assert_false(swl_uuid_fromChar(&target, "a0010203-0405-0607-0809-0a0b0-c0d0eff"));
    // No separators:
    assert_false(swl_uuid_fromChar(&target, "a00102030405060708090a0b0c0d0eff"));
    // Empty:
    assert_false(swl_uuid_fromChar(&target, ""));
    // Null input:
    assert_false(swl_uuid_fromChar(&target, NULL));
    // Null output:
    assert_false(swl_uuid_fromChar(NULL, "a0010203-0405-0607-0809-0a0b0c0d0eff"));
    // Group sizes swapped:
    assert_false(swl_uuid_fromChar(&target, "0405-a0010203-0607-0809-0a0b0c0d0eff"));
}

static void test_swl_uuid_fromMacAddress_differentMac(void** state _UNUSED) {
    // GIVEN two different MAC addresses
    const char mac1[] = "E8:F1:B0:CE:07:D4";
    const char mac2[] = "E8:F1:B0:CE:07:D5";

    // WHEN generating a UUID for each MAC
    swl_uuid_t uuid1;
    swl_uuid_t uuid2;
    bool ok1 = swl_uuid_fromMacAddress(&uuid1, mac1, 0);
    bool ok2 = swl_uuid_fromMacAddress(&uuid2, mac2, 0);

    // THEN the UUIDs are different
    assert_false(swl_uuid_eq(&uuid1, &uuid2));
    assert_true(ok1);
    assert_true(ok2);
}

static void test_swl_uuid_fromMacAddress_differentOffset(void** state _UNUSED) {
    // GIVEN two different offsets
    int offset1 = 200;
    int offset2 = 201;
    const char mac[] = "5C:DC:96:3D:DF:E0";

    // WHEN generating a UUID for each offset
    swl_uuid_t uuid1;
    swl_uuid_t uuid2;
    bool ok1 = swl_uuid_fromMacAddress(&uuid1, mac, offset1);
    bool ok2 = swl_uuid_fromMacAddress(&uuid2, mac, offset2);

    // THEN the UUIDs are different
    assert_false(swl_uuid_eq(&uuid1, &uuid2));
    assert_true(ok1);
    assert_true(ok2);
}

static void test_swl_uuid_fromMacAddress_invalidMac(void** state _UNUSED) {
    // GIVEN MAC addresses that must be considered invalid
    const char macTooShort[] = "5C:DC:96:3D:DF:E";
    const char macTooLong[] = "5C:DC:96:3D:DF:EF1";

    // WHEN generating a UUID for them
    swl_uuid_t uuid;
    bool ok1 = swl_uuid_fromMacAddress(&uuid, macTooShort, 0);
    bool ok2 = swl_uuid_fromMacAddress(&uuid, macTooLong, 0);

    // THEN an error was returned
    assert_false(ok1);
    assert_false(ok2);
}

static void test_swl_uuid_eq(void** state _UNUSED) {
    swl_uuid_t uuid1 = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} };
    swl_uuid_t uuid1copy = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} };
    swl_uuid_t uuid2 = { {1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 11, 12, 13, 14, 15, 16} };
    swl_uuid_t uuid3 = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0} };

    assert_true(swl_uuid_eq(&uuid1, &uuid1copy));
    assert_false(swl_uuid_eq(&uuid1, &uuid2));
    assert_false(swl_uuid_eq(&uuid3, &uuid1));
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlUuid");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_uuid_isValid),
        cmocka_unit_test(test_swl_uuid_toChar),
        cmocka_unit_test(test_swl_uuid_fromChar),
        cmocka_unit_test(test_swl_uuid_fromMacAddress_differentMac),
        cmocka_unit_test(test_swl_uuid_fromMacAddress_differentOffset),
        cmocka_unit_test(test_swl_uuid_fromMacAddress_invalidMac),
        cmocka_unit_test(test_swl_uuid_eq),
    };
    int rc = cmocka_run_group_tests(tests, NULL, NULL);
    sahTraceClose();
    return rc;
}
