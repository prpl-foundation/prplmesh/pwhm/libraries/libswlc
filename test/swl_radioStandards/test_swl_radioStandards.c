/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>

#include <debug/sahtrace.h>

#include "swl/swl_common_radioStandards.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "test-toolbox/ttb_assert.h"

// All currently known radio standards (but not "auto")
const swl_radioStandard_m allStandards =
    M_SWL_RADSTD_A
    | M_SWL_RADSTD_B
    | M_SWL_RADSTD_G
    | M_SWL_RADSTD_N
    | M_SWL_RADSTD_AC
    | M_SWL_RADSTD_AX
    | M_SWL_RADSTD_BE;

static void helper_test_fromCharAndValidate(
    bool expectSuccess,
    swl_radioStandard_m expectedValueOnSuccess,
    const char* string,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards) {
    swl_radioStandard_m parseResult = 999999;
    swl_radioStandard_m expectedValue;
    if(expectSuccess) {
        expectedValue = expectedValueOnSuccess;
    } else {
        expectedValue = parseResult;
    }
    bool success = swl_radStd_fromCharAndValidate(&parseResult, string, format, supportedStandards,
                                                  "helper_test_parseAndValidate");
    assert_true(expectSuccess == success);
    ttb_assert_int_eq(expectedValue, parseResult);
}

static void test_fromCharAndValidate(void** state _UNUSED) {

    swl_radioStandard_m bgn = M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N;

    // Single standard:
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A, "a", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B, "b", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_G, "g", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_AC, "ac", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_AX, "ax", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, comma-separated, supported
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G, "g,b", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G, "b,g", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "g,n,b", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "a,b,g,n", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_N | M_SWL_RADSTD_AC, "ac,n", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, nothing-separated(legacy), supported
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G, "bg", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "bgn", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "abgn", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, comma-separated, unsupported by nothing-separated
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_N, "a,n", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_N, "n,a", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_N | M_SWL_RADSTD_AC, "n,ac", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, comma-separated, allStandards currently known standards
    helper_test_fromCharAndValidate(true, allStandards, "a,b,g,n,ac,ax,be", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, nothing-separated, unsupported
    helper_test_fromCharAndValidate(false, 0, "ag", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, "aac", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, "abg", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // List with one unknown, comma-separated
    helper_test_fromCharAndValidate(false, 0, "a,z", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, "g,z,n", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Multiple standards, nothing-separated, unsupported
    helper_test_fromCharAndValidate(false, 0, "az", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, "gzn", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Empty string
    helper_test_fromCharAndValidate(false, 0, "", SWL_RADSTD_FORMAT_STANDARD, allStandards);

    // Nonsense
    helper_test_fromCharAndValidate(false, 0, "Hello world!", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, ",,,", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(false, 0, "blabla", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(false, 0, " ", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(false, 0, "!!!", SWL_RADSTD_FORMAT_LEGACY, bgn);
    helper_test_fromCharAndValidate(false, 0, ",,,", SWL_RADSTD_FORMAT_LEGACY, bgn);

    // Unsupported standards
    helper_test_fromCharAndValidate(false, 0, "ac", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(false, 0, "a", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(false, 0, "b,a,g,n", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(false, 0, "b,g,n,ax", SWL_RADSTD_FORMAT_STANDARD, bgn);

    // Unsupported combinations by some wifi chip vendors are not enforced by this function
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_G, "a,g", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_N, "a,b,n", SWL_RADSTD_FORMAT_STANDARD, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_G | M_SWL_RADSTD_AX, "g,ax", SWL_RADSTD_FORMAT_LEGACY, allStandards);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_AX | M_SWL_RADSTD_B, "ax,b", SWL_RADSTD_FORMAT_LEGACY,
                                    M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);

    // Normal case, standard format
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "b,g,n", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "bgn", SWL_RADSTD_FORMAT_STANDARD, bgn);

    // legacy format should drag in some lower but not higher standards
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC, "ac",
                                    SWL_RADSTD_FORMAT_LEGACY,
                                    M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AX | M_SWL_RADSTD_AC);

    // legacy format should drag in some lower but not unsupported standards
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_N | M_SWL_RADSTD_AC, "ac",
                                    SWL_RADSTD_FORMAT_LEGACY,
                                    M_SWL_RADSTD_B | M_SWL_RADSTD_N | M_SWL_RADSTD_AX | M_SWL_RADSTD_AC);

    // legacy format overlap with explicitly asked standards
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX,
                                    "ax", SWL_RADSTD_FORMAT_LEGACY,
                                    M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);
    helper_test_fromCharAndValidate(true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE,
                                    "be", SWL_RADSTD_FORMAT_LEGACY,
                                    M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE);

    // Combining "auto" with a standard must be disallowed
    helper_test_fromCharAndValidate(false, 0, "auto,ax", SWL_RADSTD_FORMAT_LEGACY, allStandards);
    helper_test_fromCharAndValidate(false, 0, "b,auto,g", SWL_RADSTD_FORMAT_STANDARD, bgn | M_SWL_RADSTD_AUTO);
}

static void helper_test_fromChar(
    bool expectSuccess,
    swl_radioStandard_m expectedValueOnSuccess,
    const char* string,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards) {
    swl_radioStandard_m parseResult = 999999;
    swl_radioStandard_m expectedValue;
    if(expectSuccess) {
        expectedValue = expectedValueOnSuccess;
    } else {
        expectedValue = parseResult;
    }
    bool success = swl_radStd_fromChar(&parseResult, string, format, supportedStandards, "helper_test_fromChar");
    assert_true(expectSuccess == success);
    assert_int_equal(expectedValue, parseResult);
}

static void test_fromChar(void** state _UNUSED) {

    swl_radioStandard_m bgn = M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N;

    // Normal case:
    helper_test_fromChar(true, M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC, "ac",
                         SWL_RADSTD_FORMAT_LEGACY,
                         M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AX | M_SWL_RADSTD_AC);
    helper_test_fromChar(true, M_SWL_RADSTD_AC, "ac",
                         SWL_RADSTD_FORMAT_STANDARD,
                         M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AX | M_SWL_RADSTD_AC);

    // Unsupported standards
    helper_test_fromChar(true, M_SWL_RADSTD_AC, "ac", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromChar(true, M_SWL_RADSTD_A, "a", SWL_RADSTD_FORMAT_LEGACY, bgn);

    // Combination not supported by all chip vendors
    helper_test_fromChar(true, M_SWL_RADSTD_A | M_SWL_RADSTD_G, "a,g", SWL_RADSTD_FORMAT_STANDARD, bgn);
    helper_test_fromChar(true, M_SWL_RADSTD_A | M_SWL_RADSTD_G, "a,g", SWL_RADSTD_FORMAT_LEGACY, bgn);
}


static void helper_test_toChar(
    swl_radioStandard_m bitmaskToConvert,
    const char* expectedString,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards,
    bool expectSuccess) {

    char buffer[32];
    bool success = swl_radStd_toChar(buffer, sizeof(buffer), bitmaskToConvert, format, supportedStandards);
    ttb_assert_int_eq(success, expectSuccess);
    if(expectSuccess) {
        ttb_assert_str_eq(buffer, expectedString);
    }
}

static void test_toChar(void** state _UNUSED) {

    helper_test_toChar(M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_STANDARD, 0, true);

    // Single radio standard:
    helper_test_toChar(M_SWL_RADSTD_A, "a", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_B, "b", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_G, "g", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_N, "n", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_AC, "ac", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_AX, "ax", SWL_RADSTD_FORMAT_STANDARD, 0, true);

    // Multiple standards
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G, "b,g", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "b,g,n", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "bgn", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "bgn", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "bgn", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N, "a,n", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N, "an", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "a,b,g,n", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G, "bg", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_G | M_SWL_RADSTD_N, "gn", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N, "an", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, "abgn", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G, "a,b,g", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_G, "a,g", SWL_RADSTD_FORMAT_LEGACY, 0, true);

    // Multiple standards, not supported by all chip vendors
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_G, "a,g", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_AC, "a,ac", SWL_RADSTD_FORMAT_STANDARD, 0, true);
    helper_test_toChar(M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G, "a,b,g", SWL_RADSTD_FORMAT_STANDARD, 0, true);

    // Multiple standards, all currently known standards
    helper_test_toChar(allStandards, "a,b,g,n,ac,ax,be", SWL_RADSTD_FORMAT_STANDARD, 0, true);

    // Empty (invalid):
    helper_test_toChar(0, "", SWL_RADSTD_FORMAT_STANDARD, 0, false);
    helper_test_toChar(0, "", SWL_RADSTD_FORMAT_LEGACY, 0, false);

    // Unknown standard
    helper_test_toChar(M_SWL_RADSTD_A | (M_SWL_RADSTD_LAST << 1), "", SWL_RADSTD_FORMAT_STANDARD, 0, false);
    helper_test_toChar((M_SWL_RADSTD_LAST << 1), "", SWL_RADSTD_FORMAT_STANDARD, 0, false);
    helper_test_toChar((M_SWL_RADSTD_LAST << 6), "", SWL_RADSTD_FORMAT_STANDARD, 0, false);
    helper_test_toChar(1 << 31, "", SWL_RADSTD_FORMAT_STANDARD, 0, false);

    // Hiding lower standards kicks in
    helper_test_toChar(M_SWL_RADSTD_AC, "ac", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AC, true);
    helper_test_toChar(M_SWL_RADSTD_AC | M_SWL_RADSTD_N, "ac", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AC | M_SWL_RADSTD_N, true);
    helper_test_toChar(M_SWL_RADSTD_AC | M_SWL_RADSTD_N, "ac", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AC | M_SWL_RADSTD_N | M_SWL_RADSTD_AX, true);
    helper_test_toChar(M_SWL_RADSTD_AX, "ax", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AX, true);
    helper_test_toChar(allStandards, "be", SWL_RADSTD_FORMAT_LEGACY, allStandards, true);
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_AX, "ax", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_AX, true);
    helper_test_toChar(M_SWL_RADSTD_AC, "ac", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AC | M_SWL_RADSTD_AX, true);

    // Not Hiding lower standards format because not all lower standards are in
    helper_test_toChar(M_SWL_RADSTD_AC | M_SWL_RADSTD_A, "a,ac", SWL_RADSTD_FORMAT_LEGACY, allStandards, true);
    helper_test_toChar(M_SWL_RADSTD_AC | M_SWL_RADSTD_N, "n,ac", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AC | M_SWL_RADSTD_A | M_SWL_RADSTD_N, true);
    helper_test_toChar(M_SWL_RADSTD_AX | M_SWL_RADSTD_N, "n,ax", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AX | M_SWL_RADSTD_A | M_SWL_RADSTD_N, true);

    // Not Hiding lower standards  because no ac/ax
    helper_test_toChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G, "bg", SWL_RADSTD_FORMAT_LEGACY, allStandards, true);
    helper_test_toChar(M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_LEGACY, allStandards, true);

    // Not Hiding lower standards because M_SWL_RADSTD_AUTO must always be formatted as "auto".
    helper_test_toChar(M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_LEGACY, 0, true);
    helper_test_toChar(M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_LEGACY, allStandards, true);
    helper_test_toChar(M_SWL_RADSTD_AUTO, "auto", SWL_RADSTD_FORMAT_LEGACY, M_SWL_RADSTD_AUTO, true);

    // Combining auto with other radioStandard must be considered invalid.
    helper_test_toChar(M_SWL_RADSTD_AUTO | M_SWL_RADSTD_A, "", SWL_RADSTD_FORMAT_STANDARD, SWL_RADSTD_AUTO | SWL_RADSTD_A, false);
    helper_test_toChar(M_SWL_RADSTD_AUTO | M_SWL_RADSTD_B | M_SWL_RADSTD_G, "", SWL_RADSTD_FORMAT_LEGACY, allStandards, false);

    // Buffer too small, standard format
    char buffer3[3];
    bool success = swl_radStd_toChar(buffer3, sizeof(buffer3), M_SWL_RADSTD_B | M_SWL_RADSTD_G, SWL_RADSTD_FORMAT_STANDARD, allStandards);
    assert_false(success);

    // Buffer too small, legacy format
    char buffer4[3];
    success = swl_radStd_toChar(buffer4, sizeof(buffer4), M_SWL_RADSTD_AUTO, SWL_RADSTD_FORMAT_LEGACY, allStandards);
    assert_false(success);
}

static void test_isValid(void** state _UNUSED) {

    // True cases
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_A, NULL));
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_A | M_SWL_RADSTD_B, "test_isValid"));
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G, "test_isValid"));
    assert_true(swl_radStd_isValid(allStandards, NULL));
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_B | M_SWL_RADSTD_G, NULL));
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_B | M_SWL_RADSTD_AC, NULL));
    assert_true(swl_radStd_isValid(M_SWL_RADSTD_AUTO, NULL));

    // Empty set must be invalid.
    assert_false(swl_radStd_isValid(0, NULL));

    // Unknown standards must be invalid.
    assert_false(swl_radStd_isValid(0xffffff, NULL));
    assert_false(swl_radStd_isValid(0x010000, NULL));

    // Combining "auto" with anything else must be invalid.
    assert_false(swl_radStd_isValid(M_SWL_RADSTD_AUTO | M_SWL_RADSTD_A, NULL));
    assert_false(swl_radStd_isValid(M_SWL_RADSTD_AUTO | M_SWL_RADSTD_AX, NULL));
    assert_false(swl_radStd_isValid(M_SWL_RADSTD_AUTO | M_SWL_RADSTD_B | M_SWL_RADSTD_G, NULL));
}

static void helper_test_supportedStandardsToChar(
    swl_radioStandard_m supportedStandards,
    swl_radStd_format_e format,
    uint32_t bufsize,
    bool expectSuccess,
    const char* expectedString) {
    char buffer[128] = "";
    assert_true(bufsize <= sizeof(buffer));
    bool success = swl_radStd_supportedStandardsToChar(buffer, bufsize, supportedStandards, format);
    assert_int_equal(success, expectSuccess);
    if(expectSuccess) {
        assert_true(0 == strncmp(expectedString, buffer, bufsize));
    }
}

static void test_supportedStandardsToChar(void** state _UNUSED) {

    // Legacy, normal cases
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "b,g,n,bg,gn,bgn");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "b,g,n,bg,gn,bgn,ax");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "a,n,an,ac");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "a,n,an,ac,ax");
    helper_test_supportedStandardsToChar(0, SWL_RADSTD_FORMAT_LEGACY, 64, true, "");

    // Legacy format, empty:
    helper_test_supportedStandardsToChar(0, SWL_RADSTD_FORMAT_LEGACY, 64, true, "");
    // Legacy format, empty but no space for null byte:
    helper_test_supportedStandardsToChar(0, SWL_RADSTD_FORMAT_LEGACY, 0, false, "");
    // Legacy format, just within space, short:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A, SWL_RADSTD_FORMAT_LEGACY, 2, true, "a");
    // Legacy format, no space for null byte, short:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A, SWL_RADSTD_FORMAT_LEGACY, 1, false, "");
    // Legacy format, no space for null byte, long:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_LEGACY, strlen("b,g,n,bg,gn,bgn"), false, "");
    // Legacy format, no space for half of it:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_LEGACY, strlen("b,g,n,bg,gn,bgn") / 2, false, "");
    // Legacy format, just within space, long:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_LEGACY, strlen("b,g,n,bg,gn,bgn") + 1, true, "b,g,n,bg,gn,bgn");
    // Legacy format, uncommon combination of radio standards:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "a,ax");
    // Legacy format, uncommon combination of radio standards:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_B,
                                         SWL_RADSTD_FORMAT_LEGACY, 64, true, "a,b");

    // Standard, normal cases
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_STANDARD, 64, true, "b,g,n");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_STANDARD, 64, true, "b,g,n,ax");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC,
                                         SWL_RADSTD_FORMAT_STANDARD, 64, true, "a,n,ac");
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_STANDARD, 64, true, "a,n,ac,ax");

    // Standard format, empty:
    helper_test_supportedStandardsToChar(0, SWL_RADSTD_FORMAT_STANDARD, 64, true, "");
    // Standard format, empty but no space for null byte:
    helper_test_supportedStandardsToChar(0, SWL_RADSTD_FORMAT_STANDARD, 0, false, "");
    // Standard format, just within space, short:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A, SWL_RADSTD_FORMAT_STANDARD, 2, true, "a");
    // Standard format, no space for null byte, short:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A, SWL_RADSTD_FORMAT_STANDARD, 1, false, "");
    // Standard format, no space for null byte, long:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_STANDARD, strlen("b,g,n"), false, "");
    // Standard format, no space for half of it:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_STANDARD, strlen("b,g,n") / 2, false, "");
    // Standard format, just within space, long:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,
                                         SWL_RADSTD_FORMAT_STANDARD, strlen("b,g,n") + 1, true, "b,g,n");
    // Standard format, uncommon combination of radio standards:
    helper_test_supportedStandardsToChar(M_SWL_RADSTD_A | M_SWL_RADSTD_AX,
                                         SWL_RADSTD_FORMAT_STANDARD, 64, true, "a,ax");
}

static void helper_test_supportedStandardsFromChar(
    const char* srcString,
    bool expectSuccess,
    swl_radioStandard_m expectedMask) {

    swl_radioStandard_m actualMask = 0;
    bool actualSuccess = swl_radStd_supportedStandardsFromChar(&actualMask, srcString);

    if(actualSuccess != expectSuccess) {
        fail_msg("srcString='%s', expectedSucces=%d, actualSuccess=%d", srcString, expectSuccess, actualSuccess);
    }
    if(expectSuccess && (expectedMask != actualMask)) {
        fail_msg("srcString='%s', expectedMask=%x, actualMask=%x", srcString, expectedMask, actualMask);
    }
}

static void test_supportedStandardsFromChar(void** state _UNUSED) {

    // Normal case
    helper_test_supportedStandardsFromChar("b,g,n,bg,gn,bgn",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("bg,gn,bgn",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("b,g,n",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("b,n,g",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("bg,n",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("n",
                                           true, M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("b,n",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("bg",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G);
    helper_test_supportedStandardsFromChar("bg,bg",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G);
    helper_test_supportedStandardsFromChar("a,bg",
                                           true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G);
    helper_test_supportedStandardsFromChar("b,g,n,ac,ax",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);
    helper_test_supportedStandardsFromChar("b,g,n,ac,ax,bg,gn,bgn",
                                           true, M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);

    // WHEN empty THEN not parseable
    helper_test_supportedStandardsFromChar("",
                                           false, 0);

    // WHEN disallowed characters THEN not parseable
    helper_test_supportedStandardsFromChar(" ",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,g, ,n",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,g ,n",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,g_,n",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,g,_,n",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,g,n ",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b;g;n",
                                           false, 0);
    helper_test_supportedStandardsFromChar("@",
                                           false, 0);

    // WHEN empty items THEN not parseable
    helper_test_supportedStandardsFromChar("b,,g",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,,g",
                                           false, 0);
    helper_test_supportedStandardsFromChar(",g",
                                           false, 0);
    helper_test_supportedStandardsFromChar("g,",
                                           false, 0);

    // WHEN nonsense items THEN not parseable
    helper_test_supportedStandardsFromChar("nonsense,b",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,ns",
                                           false, 0);
    helper_test_supportedStandardsFromChar("b,ns,g",
                                           false, 0);

    // WHEN "ax" or "ac" in legacy format, THEN do *not* include older standards.
    helper_test_supportedStandardsFromChar("ax",
                                           true, M_SWL_RADSTD_AX);
    helper_test_supportedStandardsFromChar("ac",
                                           true, M_SWL_RADSTD_AC);
    helper_test_supportedStandardsFromChar("n,ax",
                                           true, M_SWL_RADSTD_AX | M_SWL_RADSTD_N);
    helper_test_supportedStandardsFromChar("ac,ax",
                                           true, M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);

    // WHEN input has everything we can parse, THEN parsed as such
    helper_test_supportedStandardsFromChar("a,b,g,n,ac,ax",
                                           true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);
    helper_test_supportedStandardsFromChar("a,b,g,n,ac,ax,bg,gn,bgn,an,abgn",
                                           true, M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX);

}


static void test_supportedStandardsFromChar_allStandardsIncluded(void** state _UNUSED) {

    // This test is important to enforce that WHEN adding a radio standard in the list
    // of radio standards swl_radStd_str, THEN it can also be used when parsing SupportedStandards.

    int32_t i = 0;
    for(i = 0; i < SWL_RADSTD_MAX; i++) {
        // GIVEN a string that is in the list of radio standards
        const char* radStd = swl_radStd_str[i];
        if(swl_str_matches(radStd, "auto")) {
            break;
        }

        // WHEN parsing that as a SupportedStandards
        swl_radioStandard_m actualMask;
        bool actualSuccess = swl_radStd_supportedStandardsFromChar(&actualMask, radStd);

        // THEN it is recognized.
        assert_true(actualSuccess);
        assert_int_equal(actualMask, 1 << i);
    }
}

static int setup_suite(void** state) {
    (void) state;
    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlRStd");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_fromCharAndValidate),
        cmocka_unit_test(test_fromChar),
        cmocka_unit_test(test_toChar),
        cmocka_unit_test(test_isValid),
        cmocka_unit_test(test_supportedStandardsToChar),
        cmocka_unit_test(test_supportedStandardsFromChar),
        cmocka_unit_test(test_supportedStandardsFromChar_allStandardsIncluded),
    };
    int rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);
    sahTraceClose();
    return rc;
}
