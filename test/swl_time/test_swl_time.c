/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE // for strptime
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmocka.h>
#include <time.h>

#include <debug/sahtrace.h>
#include "test-toolbox/ttb_mockClock.h"
#include "swl/swl_common_time.h"
#include "swl/swl_common_time_spec.h"
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"

#include "test_swlc_type_testCommon.h"

// assume system up for 100k seconds
#define MONOTIME_BASE 100000
#define REALTIME_BASE 1588169613
#define REALTIME_STR "2020-04-29T14:13:33Z"

struct tm nullTime;
struct tm curTime;
struct tm timeTest1;
struct tm timeTest2;
struct tm timeTest3;

#define TEST1_NULLTIME_STR "0001-01-01T00:00:00Z"

//test 1 old time still in monotime
#define TEST1_MONOTIME 64654
#define TEST1_REALTIME 1588134267
#define TEST1_REALTIME_STR "2020-04-29T04:24:27Z"

//time in future
#define TEST2_MONOTIME 33804636
#define TEST2_REALTIME 1621874249
#define TEST2_REALTIME_STR "2021-05-24T16:37:29Z"

//very old time, before start of mono
#define TEST3_REALTIME 1564365271
#define TEST3_REALTIME_STR "2019-07-29T01:54:31Z"

typedef struct testVal {
    swl_timeMono_t monoTime;
    swl_timeReal_t realTime;
    const char* str;
    const char* localStr;
    struct tm* timeStamp;
} testVal_t;

//Only first 3 test vals are for mono conversion, as 4th is an error case
#define MONO_TESTS 4
#define REAL_TESTS 5

testVal_t vals[5] = {
    {.monoTime = 0,
        .realTime = 0,
        .str = TEST1_NULLTIME_STR,
        .localStr = "0001-01-01T00:00:00Z",
        .timeStamp = &nullTime},
    {.monoTime = MONOTIME_BASE,
        .realTime = REALTIME_BASE,
        .str = REALTIME_STR,
        .localStr = "2020-04-29T16:13:33Z",
        .timeStamp = &curTime},
    {.monoTime = TEST1_MONOTIME,
        .realTime = TEST1_REALTIME,
        .str = TEST1_REALTIME_STR,
        .localStr = "2020-04-29T06:24:27Z",
        .timeStamp = &timeTest1},
    {.monoTime = TEST2_MONOTIME,
        .realTime = TEST2_REALTIME,
        .str = TEST2_REALTIME_STR,
        .localStr = "2021-05-24T18:37:29Z",
        .timeStamp = &timeTest2},
    {.monoTime = 1,
        .realTime = TEST3_REALTIME,
        .str = TEST3_REALTIME_STR,
        .localStr = "2019-07-29T01:54:31Z",
        .timeStamp = &timeTest3},
};

struct timespec s_baseMonoTime = {.tv_sec = MONOTIME_BASE, .tv_nsec = 0};
struct timespec s_baseRealtime = {.tv_sec = REALTIME_BASE, .tv_nsec = 0};

static void s_resetTime() {
    s_baseMonoTime.tv_sec = MONOTIME_BASE;
    s_baseMonoTime.tv_nsec = 0;
    s_baseRealtime.tv_sec = REALTIME_BASE;
    s_baseRealtime.tv_nsec = 0;

    ttb_mockClock_init(&s_baseMonoTime, &s_baseRealtime);
}

static int setup_suite(void** state) {
    (void) state;
    s_resetTime();
    strptime(TEST1_NULLTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &nullTime);
    strptime(REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &curTime);
    strptime(TEST1_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest1);
    strptime(TEST2_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest2);
    strptime(TEST3_REALTIME_STR, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &timeTest3);

    return 0;
}

static int teardown_suite(void** state) {
    (void) state;
    return 0;
}

static void s_assert_tm_equal(struct tm* tm1, struct tm* tm2) {
    // only look at the date values, and not the other as they can differ depending on format
    assert_memory_equal(tm1, tm2, offsetof(struct tm, tm_wday));
}


static void test_swl_time_getMonoSec(void** state _UNUSED) {
    swl_timeMono_t time = swl_time_getMonoSec();
    assert_int_equal(time, MONOTIME_BASE);
}

static void test_swl_time_getRealSec(void** state _UNUSED) {
    swl_timeMono_t time = swl_time_getRealSec();
    assert_int_equal(time, REALTIME_BASE);
}

static void test_swl_time_getRealTm(void** state _UNUSED) {
    struct tm testVal;
    swl_time_getRealTm(&testVal);
    s_assert_tm_equal(&testVal, &curTime);
}

static void test_swl_time_monoToReal(void** state _UNUSED) {
    assert_int_equal(0, swl_time_monoToReal(0));

    for(int i = 0; i < MONO_TESTS; i++) {
        assert_int_equal(vals[i].realTime, swl_time_monoToReal(vals[i].monoTime));
    }

}

static void test_swl_time_realToMono(void** state _UNUSED) {
    assert_int_equal(0, swl_time_monoToReal(0));

    for(int i = 0; i < REAL_TESTS; i++) {
        assert_int_equal(vals[i].monoTime, swl_time_realToMono(vals[i].realTime));
    }
}

static void test_swl_time_monoToTm(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        struct tm testTm;
        swl_time_monoToTm(&testTm, vals[i].monoTime);
        s_assert_tm_equal(&testTm, vals[i].timeStamp);
    }
}

static void test_swl_time_realToTm(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        struct tm testTm;
        swl_time_realToTm(&testTm, vals[i].realTime);
        s_assert_tm_equal(&testTm, vals[i].timeStamp);
    }
}

static void test_swl_time_tmToMono(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        assert_int_equal(vals[i].monoTime, swl_time_tmToMono(vals[i].timeStamp));
    }
}

static void test_swl_time_tmToReal(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        assert_int_equal(vals[i].realTime, swl_time_tmToReal(vals[i].timeStamp));
    }
}

static void test_swl_time_monoToDate(void** state _UNUSED) {
    for(int i = 0; i < MONO_TESTS; i++) {
        char buffer[50];
        swl_time_monoToDate(buffer, sizeof(buffer), vals[i].monoTime);
        assert_string_equal(buffer, vals[i].str);
    }
}
static void test_swl_time_realToDate(void** state _UNUSED) {
    for(int i = 0; i < REAL_TESTS; i++) {
        char buffer[50];
        swl_time_realToDate(buffer, sizeof(buffer), vals[i].realTime);
        assert_string_equal(buffer, vals[i].str);
    }
}

void print_zone() {
    char* tz;

    printf("TZ: %s\n", (tz = getenv("TZ")));
    printf("  daylight: %d\n", daylight);
    printf("  timezone: %ld\n", timezone);
    printf("  time zone names: %s %s\n",
           tzname[0], tzname[1]);
}

static void test_swl_time_monoToLocalDate(void** state _UNUSED) {
    char* oldTz = getenv("TZ");
    setenv("TZ", "Europe/Brussels", 1);
    tzset();
    for(int i = 0; i < MONO_TESTS; i++) {
        char buffer[50];
        swl_time_monoToLocalDate(buffer, sizeof(buffer), vals[i].monoTime);
        assert_string_equal(buffer, vals[i].localStr);
    }
    if(oldTz == NULL) {
        unsetenv("TZ");
    } else {
        setenv("TZ", oldTz, 1);
    }
}

static void test_swl_time_realToLocalDate(void** state _UNUSED) {
    char* oldTz = getenv("TZ");
    setenv("TZ", "Europe/Brussels", 1);
    tzset();
    for(int i = 0; i < MONO_TESTS; i++) {
        char buffer[50];
        swl_time_realToLocalDate(buffer, sizeof(buffer), vals[i].realTime);
        assert_string_equal(buffer, vals[i].localStr);
    }
    if(oldTz == NULL) {
        unsetenv("TZ");
    } else {
        setenv("TZ", oldTz, 1);
    }
}

/**
 * Testing potential bug where monoToReal would toggle between x and x+1 because one
 * clock's second value would be incremented sooner than the other one, due to
 * the diff between mono and real time not being a whole number of seconds, but also
 * a significant amount of ns difference.
 */
static void test_swl_time_testNano(void** state _UNUSED) {
    swl_timespec_addTime(&s_baseRealtime, 0, SWL_TIMESPEC_NANO_PER_SEC / 2);

    for(int i = 0; i < 10; i++) {
        assert_int_equal(TEST1_REALTIME, swl_time_monoToReal(TEST1_MONOTIME));
        assert_int_equal(TEST1_MONOTIME, swl_time_realToMono(TEST1_REALTIME));
        swl_timespec_addTime(&s_baseRealtime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);
        swl_timespec_addTime(&s_baseMonoTime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);


        struct tm testTm;
        swl_time_monoToTm(&testTm, vals[1].monoTime);
        s_assert_tm_equal(&testTm, vals[1].timeStamp);
    }
    s_resetTime();
}

static void test_swl_time_getRealOfMonoBaseTime(void** state _UNUSED) {
    assert_int_equal(swl_time_getRealOfMonoBaseTime(), REALTIME_BASE - MONOTIME_BASE);
    swl_timespec_addTime(&s_baseRealtime, 0, SWL_TIMESPEC_NANO_PER_SEC / 2);
    assert_int_equal(swl_time_getRealOfMonoBaseTime(), REALTIME_BASE - MONOTIME_BASE);

    for(int i = 0; i < 10; i++) {
        assert_int_equal(TEST1_REALTIME, swl_time_monoToReal(TEST1_MONOTIME));
        assert_int_equal(TEST1_MONOTIME, swl_time_realToMono(TEST1_REALTIME));

        assert_int_equal(swl_time_getRealOfMonoBaseTime(), REALTIME_BASE - MONOTIME_BASE);
        swl_timespec_addTime(&s_baseRealtime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);
        swl_timespec_addTime(&s_baseMonoTime, 0, SWL_TIMESPEC_NANO_PER_SEC / 10);
    }
    s_resetTime();
}

#define NR_MONO_TEST_VALUES 3
swl_timeMono_t monoBase[NR_MONO_TEST_VALUES] = {TEST1_MONOTIME, TEST2_MONOTIME, TEST1_MONOTIME};
size_t monoCount[NR_MONO_TEST_VALUES] = {2, 1, 2};
swl_timeMono_t monoNotContains[NR_MONO_TEST_VALUES] = {TEST1_MONOTIME - 1, TEST1_MONOTIME + 1, TEST2_MONOTIME + 1};
#define monoStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define monoStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* monotestStrValuesChar[NR_MONO_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};

#define NR_REAL_TEST_VALUES 3
swl_timeMono_t realBase[NR_REAL_TEST_VALUES] = {TEST1_REALTIME, TEST2_REALTIME, TEST1_REALTIME};
size_t realCount[NR_REAL_TEST_VALUES] = {2, 1, 2};
swl_timeMono_t realNotContains[NR_REAL_TEST_VALUES] = {TEST1_REALTIME - 1, TEST1_REALTIME + 1, TEST2_REALTIME + 1};
#define realStrChar TEST1_REALTIME_STR ","TEST2_REALTIME_STR ","TEST1_REALTIME_STR
#define realStr2Char TEST1_REALTIME_STR ";;"TEST2_REALTIME_STR ";;"TEST1_REALTIME_STR
const char* realtestStrValuesChar[NR_REAL_TEST_VALUES] = {TEST1_REALTIME_STR, TEST2_REALTIME_STR, TEST1_REALTIME_STR};


swlc_type_testCommonData_t timeMonoData = {
    .name = "monoTime",
    .testType = swl_type_timeMono,
    .refType = &gtSwl_type_timeMonoPtr.type,
    .serialStr = monoStrChar,
    .serialStr2 = monoStr2Char,
    .strValues = monotestStrValuesChar,
    .baseValues = &monoBase,
    .valueCount = monoCount,
    .notContains = &monoNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
};

swlc_type_testCommonData_t timeRealData = {
    .name = "realTime",
    .testType = swl_type_timeReal,
    .refType = &gtSwl_type_timeRealPtr.type,
    .serialStr = realStrChar,
    .serialStr2 = realStr2Char,
    .strValues = realtestStrValuesChar,
    .baseValues = &realBase,
    .valueCount = realCount,
    .notContains = &realNotContains,
    .nrTestValues = NR_MONO_TEST_VALUES,
};

int main(int argc _UNUSED, char* argv[] _UNUSED) {
    int rc = -1;
    sahTraceOpen(__FILE__, TRACE_TYPE_STDERR);
    if(!sahTraceIsOpen()) {
        fprintf(stderr, "FAILED to open SAH TRACE\n");
    }
    sahTraceSetLevel(TRACE_LEVEL_WARNING);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);
    sahTraceAddZone(sahTraceLevel(), "swlRStd");
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_swl_time_getMonoSec),
        cmocka_unit_test(test_swl_time_getRealSec),
        cmocka_unit_test(test_swl_time_getRealTm),
        cmocka_unit_test(test_swl_time_monoToReal),
        cmocka_unit_test(test_swl_time_realToMono),
        cmocka_unit_test(test_swl_time_monoToTm),
        cmocka_unit_test(test_swl_time_realToTm),
        cmocka_unit_test(test_swl_time_tmToMono),
        cmocka_unit_test(test_swl_time_tmToReal),
        cmocka_unit_test(test_swl_time_monoToDate),
        cmocka_unit_test(test_swl_time_realToDate),
        cmocka_unit_test(test_swl_time_monoToLocalDate),
        cmocka_unit_test(test_swl_time_realToLocalDate),
        cmocka_unit_test(test_swl_time_testNano),
        cmocka_unit_test(test_swl_time_getRealOfMonoBaseTime),
    };
    rc = cmocka_run_group_tests(tests, setup_suite, teardown_suite);

    s_resetTime();
    // run type tests
    runCommonTypeTest(&timeMonoData);
    runCommonTypeTest(&timeRealData);


    sahTraceClose();

    return rc;
}
