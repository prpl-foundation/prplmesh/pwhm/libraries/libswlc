# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v5.30.1 - 2024-11-18(10:01:12 +0000)

### Other

- : Add debug info

## Release v5.30.0 - 2024-11-13(10:02:41 +0000)

### Other

- - Fix handling of 6GHz channel 2

## Release v5.29.9 - 2024-11-12(14:06:42 +0000)

### Other

- - Add MLO modes

## Release v5.29.8 - 2024-10-23(08:42:11 +0000)

### Other

- Add wifiDm header file installation

## Release v5.29.7 - 2024-10-21(14:10:59 +0000)

### Other

- - Add Radio Detailed State

## Release v5.29.6 - 2024-10-21(07:40:08 +0000)

### Other

- : Add Multi AP type

## Release v5.29.5 - 2024-10-17(15:07:26 +0000)

### Other

- : Compilation issue

## Release v5.29.4 - 2024-10-10(14:24:44 +0000)

### Other

- EHT Capabilities

## Release v5.29.3 - 2024-10-01(14:22:45 +0000)

### Other

- : Add safe llist for each

## Release v5.29.2 - 2024-10-01(07:39:20 +0000)

### Other

- add generic event queue
- : Log spam

## Release v5.29.1 - 2024-09-13(07:46:03 +0000)

### Other

- - detect EML and MLD Links

## Release v5.29.0 - 2024-09-04(14:45:55 +0000)

### Other

- - Minor fixes in VHTCapabilities

## Release v5.28.1 - 2024-08-29(13:46:48 +0000)

### Other

- [UNIT-TEST] Coverage report generation issue
- - add support to parse EHT Multi-Link IE

## Release v5.28.0 - 2024-08-09(08:58:51 +0000)

### Other

- parse country IE

## Release v5.27.4 - 2024-07-17(10:57:45 +0000)

## Release v5.27.3 - 2024-07-02(08:09:38 +0000)

### Other

- : Log spam related to MCS stats

## Release v5.27.2 - 2024-06-13(10:09:06 +0000)

### Other

- : 320MHz bandwidth changes is not working

## Release v5.27.1 - 2024-06-07(07:40:23 +0000)

### Other

- : 320MHz bandwidth changes is not working

## Release v5.27.0 - 2024-06-03(09:59:50 +0000)

### Other

- - add Mlo mode enum

## Release v5.26.0 - 2024-05-29(10:15:08 +0000)

### Other

- - 802.11b device not reported as such

## Release v5.25.0 - 2024-05-29(08:04:31 +0000)

### Other

- - country code not supported

## Release v5.24.0 - 2024-05-23(10:29:57 +0000)

### Other

- - Add Deauth and Disassoc parsing

## Release v5.23.0 - 2024-05-06(13:42:45 +0000)

### Other

- - add short bw strings for shorter debug

## Release v5.22.0 - 2024-04-26(08:41:55 +0000)

### Other

- - add EHT capabilities parsing.

## Release v5.21.0 - 2024-04-22(09:28:00 +0000)

### Other

- - add operclass 137 for 320MHz eht

## Release v5.20.5 - 2024-04-16(07:56:45 +0000)

### Other

- Implement swl_conv_encodeAsUrl and swl_conv_decodeAsUrl APIs.

## Release v5.20.4 - 2024-04-03(07:56:42 +0000)

### Other

- : Invalid Manufacturer parsing

## Release v5.20.3 - 2024-03-15(10:00:03 +0000)

### Other

- fix circTable indexing function

## Release v5.20.2 - 2024-03-11(13:55:36 +0000)

### Other

- : Add WPS Password ID

## Release v5.20.1 - 2024-03-08(10:37:49 +0000)

### Other

- : Add ModelName, ModelNumber and Manufacturer in parser

## Release v5.20.0 - 2024-02-26(09:43:18 +0000)

### Other

- - add int to/from trilean support

## Release v5.19.0 - 2024-02-21(12:23:58 +0000)

### Other

- - add a decrementing-index search function

## Release v5.18.1 - 2024-01-17(15:10:21 +0000)

### Other

- - NTT parsing of array issue

## Release v5.18.0 - 2024-01-09(10:10:52 +0000)

### Other

- add api to validate 6g PSC channels

## Release v5.17.0 - 2024-01-09(09:40:31 +0000)

### Other

- ad apis to deduce chanspec from global/local operClass

## Release v5.16.2 - 2024-01-04(08:26:43 +0000)

### Other

- : 52-64 is not a high power band in US

## Release v5.16.1 - 2023-12-11(13:06:38 +0000)

### Other

- 802.11b mode implementation

## Release v5.16.0 - 2023-11-17(07:24:55 +0000)

### Other

- - Add MBO capability

## Release v5.15.3 - 2023-11-16(15:53:23 +0000)

### Other

- - add defines for last radio bw and radio cap

## Release v5.15.2 - 2023-11-16(12:53:22 +0000)

### Other

- : Adapt CSM band flags for US

## Release v5.15.1 - 2023-11-14(14:21:21 +0000)

### Other

- - add "be" legacy format

## Release v5.15.0 - 2023-11-13(15:44:41 +0000)

### Other

- - add proper modelling for 320MHz

## Release v5.14.0 - 2023-11-13(14:14:35 +0000)

### Other

- : Add TLVs support

## Release v5.13.0 - 2023-11-10(14:21:24 +0000)

### Other

- Add support get bw / chanspec from ctrl / control channel

## Release v5.12.0 - 2023-11-07(08:57:13 +0000)

### Other

- - add WiFi 7 defines

## Release v5.11.0 - 2023-10-23(11:25:24 +0000)

### Other

- - Add swl_fileUtils_findStrInFile

## Release v5.10.5 - 2023-10-23(10:22:50 +0000)

### Other

- : Add WPS Request to Enroll parsing

## Release v5.10.4 - 2023-10-17(07:53:09 +0000)

### Other

- : Add swl_hash_hexToMD5Hex

## Release v5.10.3 - 2023-10-11(13:17:00 +0000)

### Other

- Align scan result structure with legacy components

## Release v5.10.2 - 2023-10-10(15:22:06 +0000)

### Other

- : Add vendor specific action frame parsing

## Release v5.10.1 - 2023-10-02(14:08:39 +0000)

### Other

- - notify and transmit EasyMesh 802.11 tunneled messages

## Release v5.10.0 - 2023-09-29(11:24:39 +0000)

### Other

- - add some memory api to swl

## Release v5.9.0 - 2023-09-20(12:04:02 +0000)

### Other

- - add tuple type diff to mask function

## Release v5.8.0 - 2023-09-15(11:49:18 +0000)

### Other

- add apis to dump KVP map to string buffer

## Release v5.7.2 - 2023-09-08(14:11:09 +0000)

### Other

- : Add BTM capability

## Release v5.7.1 - 2023-09-05(08:14:49 +0000)

### Other

- - Fix SWL_BIT_IS_SET_UNTIL

## Release v5.7.0 - 2023-09-05(07:42:40 +0000)

### Other

- - add intf status and name define

## Release v5.6.1 - 2023-08-28(08:08:39 +0000)

### Other

- : Add Ht/Vht/He capabilities from assoc request

## Release v5.6.0 - 2023-08-10(14:01:11 +0000)

### Other

- add mbo assoc disallow subelement

## Release v5.5.0 - 2023-07-17(09:51:55 +0000)

### Other

- - add free macro to free and NULL

## Release v5.4.0 - 2023-07-06(14:52:58 +0000)

### Other

- - add type find in array

## Release v5.3.0 - 2023-07-04(13:46:16 +0000)

### Other

- - add interpolate Array for double, define for pi and e

## Release v5.2.4 - 2023-07-04(06:46:56 +0000)

### Other

- Fix potential NULL pointer dereferencing

## Release v5.2.3 - 2023-06-28(10:02:05 +0000)

### Other

- : Add function to remove whitespaces and new line characters from a string.

## Release v5.2.2 - 2023-06-19(09:47:53 +0000)

### Other

- : Add a new category for Vendor specific action

## Release v5.2.1 - 2023-06-14(14:22:14 +0000)

### Other

- Update TBTT length values

## Release v5.2.0 - 2023-06-08(14:24:07 +0000)

### Other

- add ref type fun for val types

## Release v5.1.2 - 2023-06-06(08:47:46 +0000)

### Other

- - sub second timestamps sometimes float in dm

## Release v5.1.1 - 2023-06-01(14:00:52 +0000)

### Other

- Generate gcovr txt report and upload reports to documentation server

## Release v5.1.0 - 2023-05-23(12:57:08 +0000)

### Other

- - Fix ME in header

## Release v5.0.0 - 2023-05-22(08:03:27 +0000)

### Other

- - rewrite swl_crypto for openssl 3.0 support

## Release v4.3.2 - 2023-05-05(08:31:53 +0000)

### Other

- - Fix ME in header

## Release v4.3.1 - 2023-05-04(07:24:48 +0000)

### Other

- - rewrite swl_crypto for openssl 3.0 support

## Release v4.3.0 - 2023-03-28(14:05:54 +0000)

### Other

- - [prplMesh WHM] Exposing VHT,HT and HE radio capabilities through the datamodel.

## Release v4.2.0 - 2023-03-22(10:57:22 +0000)

### Other

- - [prplMesh WHM] Exposing VHT,HT and HE radio capabilities through the datamodel.

## Release v4.1.0 - 2023-03-21(15:08:38 +0000)

### Other

- - add default bandwidth

## Release v4.0.0 - 2023-03-20(15:16:43 +0000)

### Other

- - Add support for type math, and subtypes

## Release v3.27.0 - 2023-03-15(11:06:05 +0000)

### Other

- manage calling handlers for 80211 mgmt frame types

## Release v3.26.0 - 2023-03-08(16:11:10 +0000)

### Other

- define 80211 management action wnm frame

## Release v3.25.0 - 2023-03-08(12:44:49 +0000)

## Release v3.24.1 - 2023-03-07(14:08:58 +0000)

### Other

- Add Operating Classes for United States and Japan

## Release v3.24.0 - 2023-03-07(11:25:18 +0000)

### Other

- - test issue due to uninitialized memory

## Release v3.23.0 - 2023-03-03(09:03:17 +0000)

### Other

- add api to locate IEs in 80211 management frame

## Release v3.22.2 - 2023-02-21(15:53:17 +0000)

## Release v3.22.1 - 2023-02-20(12:10:50 +0000)

### Other

- SecurityModesSupported not filled, impacting band steering in WPA2-WPA3

## Release v3.22.0 - 2023-02-13(11:26:05 +0000)

### Other

- - add radStd to Mcs & checkMatches function

## Release v3.21.3 - 2023-02-09(08:34:06 +0000)

### Other

- - add reproting detail element id

## Release v3.21.2 - 2023-02-07(09:42:34 +0000)

### Other

- : add certificate validation

## Release v3.21.1 - 2023-02-06(16:21:19 +0000)

### Other

- : add certificate validation

## Release v3.21.0 - 2023-02-03(10:39:19 +0000)

### Other

- - add more options for IEEE80211v BTM request

## Release v3.20.1 - 2023-02-01(13:52:33 +0000)

### Other

- : add RSSI from RCPI function

## Release v3.20.0 - 2023-01-31(16:10:35 +0000)

### Other

- - add more options for IEEE80211v BTM request

## Release v3.19.2 - 2023-01-31(15:56:20 +0000)

### Other

- - Fix testing dependencies

## Release v3.19.1 - 2023-01-31(09:39:50 +0000)

### Other

- Add MFP required constraint for OWE security mode

## Release v3.19.0 - 2023-01-30(10:55:55 +0000)

### Other

- add apis to check apMode type to deduce mfp mode

## Release v3.18.0 - 2023-01-30(09:07:33 +0000)

### Other

- add apis to convert mfp modes to/from string

## Release v3.17.0 - 2023-01-27(09:21:46 +0000)

### Other

- add apis to convert security modes mask to/from string

## Release v3.16.1 - 2023-01-23(11:32:43 +0000)

### Other

- - add SWL prefix to avoid redefinition

## Release v3.16.0 - 2023-01-17(14:30:45 +0000)

### Other

- add api to count differences in two strings

## Release v3.15.0 - 2023-01-12(14:00:37 +0000)

### Other

- add apis to map wps config method IDs and names

## Release v3.14.0 - 2023-01-12(13:33:07 +0000)

### Other

- - RRM request fields and report parsing

## Release v3.13.1 - 2023-01-11(08:37:43 +0000)

### Other

- : add new swl fileUtils function

## Release v3.13.0 - 2023-01-10(10:40:24 +0000)

### Other

- improve management frame IEs parsing

## Release v3.12.5 - 2023-01-09(09:02:13 +0000)

### Other

- - Add RRM Capabilties string flags

## Release v3.12.4 - 2023-01-05(11:24:26 +0000)

### Other

- - Fix swlc debug when empty type array

## Release v3.12.2 - 2023-01-04(13:35:45 +0000)

### Other

- - IEEE80211k capabilities per station

## Release v3.12.1 - 2022-12-21(13:47:41 +0000)

### Other

- Extract utils from WLD

## Release v3.12.0 - 2022-12-20(14:21:46 +0000)

### Other

- - fix circ table

## Release v3.11.0 - 2022-12-20(09:41:32 +0000)

### Other

- - add tuple type array

## Release v3.10.0 - 2022-12-16(10:49:37 +0000)

### Other

- - add zero deauth code

## Release v3.9.0 - 2022-12-16(10:25:32 +0000)

### Other

- - [nl80211] scan/scanResults commands

## Release v3.8.0 - 2022-12-09(14:39:23 +0000)

### Other

- - Add nice histogram init, and update tests

## Release v3.7.0 - 2022-12-06(17:13:56 +0000)

### Other

- - [prplMesh WHM] Adding operating_class and channel for the NonAssociatedDevice object

## Release v3.6.3 - 2022-12-06(14:42:54 +0000)

### Other

- - add assert ok, and print iso assert update

## Release v3.6.2 - 2022-12-05(09:50:57 +0000)

### Other

- - Too many logs
- remove squash commits as no needed anymore

## Release v3.6.1 - 2022-11-29(14:02:20 +0000)

### Fixes

- : Add swl_strlst_copyStringAtIndex util function

## Release v3.6.0 - 2022-11-25(13:26:00 +0000)

### New

- - Add round and multComplement functions

## Release v3.5.0 - 2022-11-25(12:40:06 +0000)

### Other

- - add file compare function

## Release v3.4.1 - 2022-11-21(15:38:55 +0000)

### Other

- - Compile issue due to FILE not known

## Release v3.4.0 - 2022-11-21(08:20:54 +0000)

### Other

- - add ref type, and initial read from file support

## Release v3.3.0 - 2022-11-17(15:20:17 +0000)

### Other

- - add annotated tuple type, and allow more than 32 fields.

## Release v3.2.1 - 2022-11-08(15:37:39 +0000)

### Other

- - Fix arrayFromCharSep with empty string

## Release v3.2.0 - 2022-11-04(09:04:44 +0000)

### Other

- - Add type for trilean

## Release v3.1.2 - 2022-11-03(12:44:00 +0000)

### Other

- Add functions to validate channel for a specific band

## Release v3.1.1 - 2022-11-03(09:48:41 +0000)

### Other

- Define default channels set

## Release v3.1.0 - 2022-11-03(09:22:57 +0000)

### Other

- - Update map type

## Release v3.0.0 - 2022-11-02(14:56:56 +0000)

### Other

- - Add collection type

## Release v2.10.12 - 2022-10-28(12:14:02 +0000)

### Other

- Add append option for WriteKVP file updating

## Release v2.10.11 - 2022-10-28(11:41:48 +0000)

### Other

- : Add encoding data function from X509 data

## Release v2.10.9 - 2022-10-06(12:57:52 +0000)

### Other

- - format security build option fails to build

## Release v2.10.8 - 2022-10-05(09:54:28 +0000)

### Other

- - Add type to tupleType

## Release v2.10.7 - 2022-10-03(15:03:21 +0000)

### Other

- update all files to BSD-2 license

## Release v2.10.6 - 2022-09-27(12:53:11 +0000)

### Other

- - add array diff

## Release v2.10.5 - 2022-09-26(07:52:49 +0000)

### Other

- [SSL] upstep to openssl 3.x.x

## Release v2.10.4 - 2022-09-21(10:06:54 +0000)


- - OUI not handled right

## Release v2.10.3 - 2022-09-20(13:49:27 +0000)

### Other

- - header wrong
- Add SSID string length definition

## Release v2.10.2 - 2022-09-20(07:36:37 +0000)

### Other

- fix compatibilty with old openssl versions (<1.1.1)

## Release v2.10.1 - 2022-09-19(09:24:31 +0000)

### Other

- - Remove need for packed

## Release v2.10.0 - 2022-09-15(14:29:07 +0000)

### Other

- - Add named tuple type

## Release v2.9.1 - 2022-09-08(10:28:35 +0000)

### Other

- Update RNR information

## Release v2.9.0 - 2022-09-02(10:38:55 +0000)

### Other

- define USP tr181 command status codes and strings

## Release v2.8.6 - 2022-09-02(09:45:15 +0000)

### Other

- : Add private key decoding using EVP_PKEY

## Release v2.8.5 - 2022-08-30(13:20:50 +0000)

### Other

- Add FortyMHz Intolerant bit in datamodel per STA

## Release v2.8.4 - 2022-08-30(12:23:04 +0000)

### Other

- - Update the deauth enum with new reason code

## Release v2.8.3 - 2022-08-30(09:03:44 +0000)

### Other

- : Add encode/decode data function using openssl

## Release v2.8.2 - 2022-08-29(08:44:37 +0000)

### Other

- : EndPoint traffic not taken into account in APRoaming

## Release v2.8.1 - 2022-08-29(08:15:09 +0000)

### Other

- : Add TWT capability per STA in the datamodel

## Release v2.8.0 - 2022-08-02(12:53:46 +0000)

### Other

- - add apis to get radio standard from mcs standard

## Release v2.7.3 - 2022-07-26(10:01:46 +0000)

### Other

- fix dep to ssl in tmssw feed

## Release v2.7.2 - 2022-07-26(07:45:29 +0000)

### Other

- - swlc v2.7.1 does not build

## Release v2.7.1 - 2022-07-25(07:59:56 +0000)

### Other

- : Add TWT capability per STA in the datamodel

## Release v2.7.0 - 2022-07-20(09:47:54 +0000)

### Other

- - Add toFile option for type

## Release v2.6.2 - 2022-07-19(09:46:31 +0000)

### Other

- - add extra support function to swl_timespec

## Release v2.6.1 - 2022-07-18(09:39:24 +0000)

### Other

- - Add tests

## Release v2.6.0 - 2022-06-29(09:36:45 +0000)

### Other

- add mapChar value formatting apis

## Release v2.5.0 - 2022-06-27(12:59:38 +0000)

### Other

- - Fix type toChar return

## Release v2.4.1 - 2022-06-24(11:59:50 +0000)

### Other

- fix link to ssl

## Release v2.4.0 - 2022-06-23(13:10:02 +0000)

### Other

- - add IEEE80211 mgmt frame defines

## Release v2.3.0 - 2022-06-21(13:49:36 +0000)

### Other

- - updating PACKED, OUI and type defines
- - Add swl_bit*_t, swl_enum_e and swl_mask_m types

## Release v2.2.2 - 2022-06-17(12:57:12 +0000)

### Other

- : Detect a probing station

## Release v2.2.1 - 2022-06-17(11:47:54 +0000)

### Other

- - [prplMesh_WHM] provide STA capabilities for STA connected BWL Event

## Release v2.2.0 - 2022-06-17(09:09:42 +0000)

### Other

- [amx] ambiorix swlc awla and wld/pwhm structure

## Release v2.1.0 - 2022-06-16(11:50:57 +0000)

### Other

- Eye's on wifi : Adding bit swapper macros and mcs flag enums/strings.

## Release v2.0.6 - 2022-06-16(09:52:42 +0000)

### Other

- : multi-ep support for ap-roaming

## Release v2.0.5 - 2022-06-14(15:30:22 +0000)

### Other

- - Add subtype constants of frame control field

## Release v2.0.4 - 2022-06-09(09:24:33 +0000)

### Other

- revert ambiorix template

## Release v2.0.3 - 2022-06-08(08:24:50 +0000)

### Other

- include ambiorix template for ambiorix components

## Release v2.0.2 - 2022-06-03(11:52:22 +0000)

### Other

- : multi-ep support for ap-roaming
- [prpl] opensource pwhm swla and swlc
