# libswlc

Library for common Wifi Team functionality.

## swl_tta: tuple type array

### swl_tta_getMatchingTupleInRange()

Iterates upon the tuple type array in a manner defined by {startElem, endElem, stepSize}. Returns first encountered element that matches the search criteria.
Parameters {startElem, endElem} are signed, with negative values implying `counting from last`.
The direction is encoded in the sign of the stepSize parameter, not in the relative values of firstIndex vs lastIndex.
Depending on the position of endElem element relative to startElem and the direction of search, the iteration may wrap-around the first or last element of the table, at most once.

## swl_table

### swl_table_getMatchingTupleInRange()

Uses the swl_tta_getMatchingTupleInRange on a table type, which is a speciatlization of the tuple type array data structure.


## swl_circTable

### swl_circTable_getMatchingTupleInRange()

Uses the swl_table_getMatchingTupleInRange on a circular table data structure, which is a speciatlization of the table data structure.
