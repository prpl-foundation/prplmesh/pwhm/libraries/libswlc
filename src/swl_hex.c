/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlHex"
#include <string.h>
#include <ctype.h>
#include <arpa/inet.h>
#include "swl/swl_common.h"
#include "swl/swl_hex.h"
#include "swl/swl_string.h"
#include "swl/swl_assert.h"

/**
 * Whether the given character is considered usable in hex notation.
 *
 * @return True if and only if the character is one of 0123456789abcdefABCDEF
 */
bool swl_hex_isHexOneChar(const char c) {
    return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

/**
 * Whether `str` for at most `len` bytes is in hexadecimal format.
 *
 * Must not start with "0x" or "0X" and must be of even length (according to `len` reached or null
 * byte, whichever comes first).
 *
 * Can be lower case, upper case, or mixed case.
 *
 * @param len: Look at most at `len` bytes of `str`. In case a null byte is encountered,
 *   stops looking even if there are more bytes available according to `len`.
 */
bool swl_hex_isHexChar(const char* str, size_t strSize) {
    ASSERT_NOT_NULL(str, false, ME, "NULL");
    size_t i = 0;
    for(i = 0; i < strSize && str[i] != '\0'; i++) {
        if(!swl_hex_isHexOneChar(str[i])) {
            return false;
        }
    }
    return i % 2 == 0;
}

/**
 * Whether the given character is considered as valid separator:
 * @return True if the character is:
 * - punctuation character: printable character which is not a space or an alphanumeric character.
 * - printable space
 * which is one of:  !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~  and SPACE
 */
bool swl_hex_isSepOneChar(const char c) {
    return (ispunct(c) || (isspace(c) && isprint(c)));
}

/**
 * Find a valid separator in hex string:
 * - regular and periodic separator after each 1 or 2 hex digits
 * - none (0) if the string is full hex.
 * @param pSep: output pointer to char, filled with found separator
 * @param str: source hex string
 * @param strSize: source hex string size
 * @return True if went through all the source string, False otherwise:
 * - invalid character found: not hex neither separator
 * - mixed separators
 * - invalid hex digit sequence length (at most 2 hex digits per byte)
 */
bool swl_hex_findValidSepChar(char* pSep, const char* str, size_t strSize) {
    ASSERT_NOT_NULL(pSep, false, ME, "NULL");
    *pSep = 0;
    ASSERT_NOT_NULL(str, false, ME, "NULL");
    ASSERTS_NOT_EQUALS(str[0], 0, true, ME, "empty src");
    ASSERTS_TRUE(strSize > 0, true, ME, "null srcSize");
    char activeSep = 0;
    uint32_t hexSeq = 0;

    for(uint32_t i = 0; i < strSize && str[i]; i++) {
        char curChar = str[i];
        if(swl_hex_isHexOneChar(curChar)) {
            hexSeq++;
            if(hexSeq == 3) {
                activeSep = -1;
                hexSeq = 1;
            }
        } else if(swl_hex_isSepOneChar(curChar)) {
            ASSERT_TRUE(hexSeq > 0, false, ME, "null hex digit sequence");
            ASSERT_TRUE(activeSep == 0 || activeSep == curChar, false, ME, "separator changed");
            activeSep = curChar;
            hexSeq = 0;
        } else {
            SAH_TRACEZ_ERROR(ME, "Unexpected '%c' not hex digit not separator char", curChar);
            return false;
        }
    }
    if(activeSep > 0) {
        *pSep = activeSep;
    }
    return true;
}

/**
 * Encodes a series of bytes to a hex string.
 *
 * @param srcBytesSize: the size in bytes of the input buffer `bytes`.
 */
bool swl_hex_fromBytes(char* tgtStr, size_t tgtStrSize, const swl_bit8_t* srcBytes,
                       size_t srcBytesSize, bool upperCase) {
    return swl_hex_fromBytesSep(tgtStr, tgtStrSize, srcBytes, srcBytesSize, upperCase, 0, NULL);
}

/**
 * Encodes as much as possible bytes to a sized hex string, with optional separator character
 *
 * @param tgtStr: target string buffer (output)
 * @param tgtStrSize: the size in bytes of target string buffer
 * @param srcBytes: source byte buffer (input)
 * @param srcBytesSize: the size in bytes of the input buffer `bytes`.
 * @param upperCase: option to select capitalization of the formatted hex digit
 * @param separator: optional character to separate formatted hex bytes (i.e every two hex digits)
 *   if 0, then no separator is added.
 * @param pLenWritten: optional result, filled with the number of written chars
 * @return True on success, false on error:
 *   - target buffer is null, or too short
 *   - source buffer is null
 */
bool swl_hex_fromBytesSep(char* tgtStr, size_t tgtStrSize, const swl_bit8_t* srcBytes,
                          size_t srcBytesSize, bool upperCase, char separator, size_t* pLenWritten) {
    W_SWL_SETPTR(pLenWritten, 0);

    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_TRUE(tgtStrSize > 0, false, ME, "null tgtStrSize");
    memset(tgtStr, 0, tgtStrSize);
    ASSERTS_TRUE(srcBytesSize > 0, true, ME, "null srcBytesSize");
    ASSERT_NOT_NULL(srcBytes, false, ME, "NULL");

    uint32_t srcPos = 0;
    uint32_t tgtPos = 0;
    const char* hexChar = (upperCase) ? "0123456789ABCDEF" : "0123456789abcdef";
    size_t requiredStrSize = (srcBytesSize * 2) + 1;
    if(separator) {
        requiredStrSize += (srcBytesSize - 1);
    }
    while((srcPos < srcBytesSize) && ((tgtStrSize - tgtPos) > 1)) {
        size_t chunkSize = 2;
        if(tgtPos && separator) {
            chunkSize += 1;
        }
        ASSERT_TRUE(((tgtStrSize - tgtPos) > chunkSize), false, ME, "fail to append hex byte[%d] of %d (pos:%d size:%d sep(%c) need:%d)",
                    srcPos, (int) srcBytesSize, tgtPos, (int) tgtStrSize, separator, (int) requiredStrSize);
        if(tgtPos && separator) {
            tgtStr[tgtPos] = separator;
            tgtPos += 1;
            W_SWL_SETPTR(pLenWritten, tgtPos);

        }
        tgtStr[tgtPos] = hexChar[(srcBytes[srcPos] >> 4) & 0xf];
        tgtStr[tgtPos + 1] = hexChar[srcBytes[srcPos] & 0xf];
        tgtPos += 2;
        W_SWL_SETPTR(pLenWritten, tgtPos);
        srcPos += 1;
    }
    return (srcPos == srcBytesSize);
}

/**
 * Convert one hex character ('0', '1', ..., 'f', 'F') to half a byte.
 */
static bool s_toNibble(uint8_t* target, char source) {
    if((source >= '0') && (source <= '9')) {
        *target = source - '0';
        return true;
    } else if((source >= 'A') && (source <= 'F')) {
        *target = source - 'A' + 10;
        return true;
    } else if((source >= 'a') && (source <= 'f')) {
        *target = source - 'a' + 10;
        return true;
    } else {
        return false;
    }
}

/**
 * Return segment size of byte. Either returns 2 if next 2 values are characters, 1 if
 * next value is character and value after that is separator, 0 if next char is separator,
 * or -1 in other cases.
 */
static int32_t s_getSegSize(const char* srcStr, size_t srcStrLen, char separator) {
    if((srcStrLen == 0) || (srcStr[0] == separator) || (srcStr[0] == '\0')) {
        return 0;
    }
    if(!isxdigit(srcStr[0])) {
        return -1;
    }
    if((srcStrLen == 1) || (srcStr[1] == separator) || (srcStr[1] == '\0')) {
        return 1;
    }
    if(!isxdigit(srcStr[1])) {
        return -1;
    }
    if((srcStrLen == 2) || (srcStr[2] == separator) || (srcStr[2] == '\0') || (separator == '\0')) {
        return 2;
    }
    return -1;
}

/**
 * Convert a string in hex format (with separator char list) to bytes.
 *
 * For example, inputs "680007", "68-00-7", "68:0:7" and "68-0-07" give output "h\0\a" (without null termination).
 *
 * @param target: buffer to write to. Can contain null bytes in the middle.
 *   No extra null-termination byte is added.
 * @param targetSize: size of `target` in bytes.
 * @param srcStr: input string
 *   Must not start with "0x" or "0X".
 *   Can be lowercase, uppercase, or mixed.
 *   Not looked at after `strSize` bytes or after first null byte.
 *   Without using byte separators, src string must be of odd length (`strSize` or null-terminated, whichever comes first).
 *   With using byte separators, src string chunks are converted into bytes.
 * @param strSize: size of `srcStr` in bytes. If `srcStr` is null-terminated and `srcStrSize` is longer
 *    than `strlen(srcStr)+1`, behaves the same as if `srcStrSize` was `strlen(srcStr)+1`.
 * @param separator: optional separator character splitting str source into chunks of 1 or 2 hex digits.
 *    If it is not null, then it becomes required.
 * @param pLenWritten: optional result, filled with the number of written Bytes
 * @return True on success, false on error:
 *   - target buffer size too small
 *   - input is not hex (e.g. "1z")
 *   - input starts with "0x" or "0X".
 *   - input of odd number of hex digits, unless using separators to split hex bytes
 */
bool swl_hex_toBytesSep(swl_bit8_t* target, size_t targetSize, const char* srcStr, size_t srcStrSize, char separator, size_t* pLenWritten) {
    W_SWL_SETPTR(pLenWritten, 0);

    ASSERT_NOT_NULL(target, false, ME, "NULL");
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");
    size_t indexSrc = 0;
    size_t indexTgt = 0;
    size_t strLength = swl_str_nlen(srcStr, srcStrSize);
    while(indexSrc < strLength && indexTgt < targetSize) {
        int32_t segmentSize = s_getSegSize(&srcStr[indexSrc], strLength - indexSrc, separator);
        ASSERT_TRUE(segmentSize >= 0, false, ME, "parse error -%s- @ %zu", srcStr, indexSrc);
        uint8_t accVal = 0;
        uint8_t tmp = 0;
        if(segmentSize == 2) {
            ASSERT_TRUE(s_toNibble(&tmp, srcStr[indexSrc]), false, ME, "char parse error -%s- @ %zu", srcStr, indexSrc);
            accVal += tmp << 4;
            indexSrc++;
        }
        if(segmentSize >= 1) {
            ASSERT_TRUE(s_toNibble(&tmp, srcStr[indexSrc]), false, ME, "char parse error -%s- @ %zu", srcStr, indexSrc);
            indexSrc++;
            accVal += tmp;
        }
        target[indexTgt] = accVal;
        indexTgt++;
        W_SWL_SETPTR(pLenWritten, indexTgt);
        if(separator != '\0') {
            ASSERT_TRUE(srcStr[indexSrc] == '\0' || srcStr[indexSrc] == separator, false,
                        ME, "separator parse error -%s- @ %zu", srcStr, indexSrc);
            indexSrc++;
        }
    }
    if((indexTgt == targetSize) && (indexSrc < strLength)) {
        return false;
    }
    return true;
}

/**
 * Convert a string in hex format to bytes.
 *
 * For example, input "680007" gives output "h\0\a" (without null termination).
 *
 * @param target: buffer to write to. Can contain null bytes in the middle.
 *   No extra null-termination byte is added.
 * @param targetSize: size of `target` in bytes.
 * @param srcStr: input string
 *   Must not start with "0x" or "0X".
 *   Can be lowercase, uppercase, or mixed.
 *   Not looked at after `strSize` bytes or after first null byte.
 *   If no separator is used, then always an even amount of characters must be provided.
 * @param strSize: size of `srcStr` in bytes. If `srcStr` is null-terminated and `srcStrSize` is longer
 *    than `strlen(srcStr)+1`, behaves the same as if `srcStrSize` was `strlen(srcStr)+1`.
 * @return True on success, false on error:
 *   - target buffer size too small
 *   - input is not hex (e.g. "1z")
 *   - input starts with "0x" or "0X".
 *   - input of odd number of hex digits.
 */
bool swl_hex_toBytes(swl_bit8_t* target, size_t targetSize, const char* srcStr, size_t srcStrSize) {
    return swl_hex_toBytesSep(target, targetSize, srcStr, srcStrSize, 0, NULL);
}

/**
 * Parses a hex string to unsigned 8 bit number.
 *
 * @param src: Must begin with 2 bytes that are not the '\0' character. Longer is allowed.
 *   Can be uppercase, lowercase, mixed. Must not start with "0x".
 * @return true on success, false on failure: insufficient input length, unparseable text.
 */
bool swl_hex_toUint8(uint8_t* target, const char* src, size_t srcSize) {
    ASSERT_NOT_NULL(src, NULL, ME, "NULL");
    ASSERT_NOT_NULL(target, NULL, ME, "NULL");
    ASSERT_TRUE(srcSize >= 2 && NULL == memchr(src, 0, 2), false, ME, "Input too small");
    return swl_hex_toBytes(target, 1, src, 2);
}

/**
 * Parses a hex to unsigned 16bit number.
 *
 * @param src: Must begin with 4 bytes that are not the '\0' character. Longer is allowed.
 *   Can be uppercase, lowercase, mixed. Must not start with "0x". In network order.
 * @return true on success, false on failure: insufficient input length, unparseable text.
 */
bool swl_hex_toUint16(uint16_t* target, const char* src, size_t srcSize) {
    ASSERT_NOT_NULL(src, NULL, ME, "NULL");
    ASSERT_NOT_NULL(target, NULL, ME, "NULL");
    uint16_t networkInt = 0;
    size_t needChars = sizeof(networkInt) * 2;
    ASSERT_TRUE(srcSize >= needChars && NULL == memchr(src, 0, needChars), false,
                ME, "Input too small");
    bool ok = swl_hex_toBytes((swl_bit8_t*) &networkInt, sizeof(networkInt), src, needChars);
    *target = ntohs(networkInt);
    return ok;
}

/**
 * Parses a hex to unsigned 32bit number.
 *
 * @param src: Must begin with 8 bytes that are not the '\0' character. Longer is allowed.
 *   Can be uppercase, lowercase, mixed. Must not start with "0x". In network order.
 * @return true on success, false on failure: insufficient input length, unparseable text.
 */
bool swl_hex_toUint32(uint32_t* target, const char* src, size_t srcSize) {
    ASSERT_NOT_NULL(src, NULL, ME, "NULL");
    ASSERT_NOT_NULL(target, NULL, ME, "NULL");
    uint32_t networkInt = 0;
    size_t needChars = sizeof(networkInt) * 2;
    ASSERT_TRUE(srcSize >= needChars && NULL == memchr(src, 0, needChars), false,
                ME, "Input too small");
    bool ok = swl_hex_toBytes((swl_bit8_t*) &networkInt, sizeof(networkInt), src, needChars);
    *target = ntohl(networkInt);
    return ok;
}

