/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_math.h"
#include "swl/swl_assert.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/types/swl_collType.h"
#include "swl/subtypes/swl_mapSType.h"

#define ME "swlType"

const char* swl_typeFlag_str[] = {"Value", "List", "Map", "Basic", "Number", "Ref"};
SWL_ASSERT_STATIC(SWL_TYPE_FLAG_MAX == SWL_ARRAY_SIZE(swl_typeFlag_str), "swl_typeFlag_str is not correctly defined");

/**
 * Type library, offering generic type support.
 *
 * For this library, we distinguish between reference and value types, so that reference type pointers can be immediately
 * used in this library.
 *
 * To work with this difference, this module defines two typedefs for void:
 * * swl_typeData_t means the actual data the type points to, so e.g. "char" for a char* ref type, and "uint16_t" for a uint16_t value type
 * * swl_typeEl_t means an actual element of the given type, so swl_typeEl_t* would be a pointer to a char*, or pointer to uint16_t
 */

/**
 * Check whether two values are equal. If an equals operator is defined on the type, that
 * operator will be called, otherwise the memory contents will be compared.
 * If type is NULL, false shall be returned
 * If either value is NULL, then equals shall return true only if both values are NULL.
 */
bool swl_type_equals(swl_type_t* type, const swl_typeData_t* val0, const swl_typeData_t* val1) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    if(type->typeFun->equals != NULL) {
        return type->typeFun->equals(type, val0, val1);
    } else {
        if((val0 == NULL) || (val1 == NULL)) {
            return val0 == NULL && val1 == NULL;
        }
        return memcmp(val0, val1, type->size) == 0;
    }
}

/**
 * Clean up the value of the given type.
 * This will set the contents of value to zero, and deallocate memory if needed.
 *
 * Content of value must be pointer to memory containing the type data.
 *
 * note that the block of memory to which the typeEl itself points will NOT be cleaned.
 *
 */
void swl_type_cleanup(swl_type_t* type, swl_typeEl_t* value) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(value, , ME, "NULL type %d %s", (int) type->size, (type->typeFun != NULL) ? type->typeFun->name : "unknown");
    if(type->typeFun->cleanup != NULL) {
        type->typeFun->cleanup(type, value);
        memset(value, 0, type->size);
    } else {
        if(type->typeFun->isValue) {
            memset(value, 0, type->size);
        } else {
            swl_typeData_t** data = (swl_typeData_t**) value;
            if(*data != NULL) {
                free(*data);
            }
            *data = NULL;
        }
    }
}

/**
 * Clean up and frees the data referenced to by value.
 *
 * This is the inverse action of swl_type_copy. So one can do
 * swl_typeData_t* data = swl_type_copy(&gswl_type_test, oldData);
 * swl_type_cleanupPtr(&gswl_type_test, &data);
 *
 * The result shall be that data contains NULL and nothing is allocated.
 */
void swl_type_cleanupPtr(swl_type_t* type, swl_typeData_t** value) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(value, , ME, "NULL");
    if(!type->typeFun->isValue) {
        swl_type_cleanup(type, value);
    } else {
        swl_type_cleanup(type, *value);
    }
    free(*value);
    *value = NULL;

}

/**
 * Allocate a copy of a given value. If the copy operator is defined, that operator will
 * be called. Otherwise new memory will be allocated and the memory of type src will
 * be copied in the new memory.
 * If either type or src is NULL, NULL shall be returned.
 *
 * To clean up the data, swl_type_cleanupPtr should be used
 */
swl_typeData_t* swl_type_copy(swl_type_t* type, const swl_typeData_t* src) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(src, NULL, ME, "NULL");
    if(type->typeFun->copy != NULL) {
        return type->typeFun->copy(type, src);
    } else {
        swl_typeData_t* data = calloc(1, type->size);
        memcpy(data, src, type->size);
        return data;
    }
}

/**
 * copy the source data to a given target. If the copy operator is defined, that operator will
 * be called and result stored in tgt, free'ing the memory allocated by the copy.
 *
 * if there is anything in tgt, then this will be cleaned.
 *
 * If no copy is defined, a memcpy will be executed
 */
void swl_type_copyTo(swl_type_t* type, swl_typeEl_t* tgt, const swl_typeData_t* src) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(src, , ME, "NULL");
    ASSERT_NOT_NULL(tgt, , ME, "NULL");
    if(type->typeFun->copy != NULL) {
        swl_typeData_t* data = type->typeFun->copy(type, src);

        swl_type_cleanup(type, tgt);

        if(type->typeFun->isValue) {
            memcpy(tgt, data, type->size);
            free(data);
        } else {
            swl_typeData_t** tmpTgt = (swl_typeData_t**) tgt;
            *tmpTgt = data;
        }
    } else {
        memcpy(tgt, src, type->size);
    }
}

/**
 * For value types, it returns the pointer to the value.
 * For pointer types, it returns directly the pointer value.
 */
swl_typeData_t* swl_type_toPtr(swl_type_t* type, const swl_typeEl_t* ptr) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(ptr, NULL, ME, "NULL");
    return SWL_TYPE_TO_PTR(type, ptr);
}

/**
 * Returns the extenstion function table for an extension UID
 */
swl_typeExtFun_t* swl_type_getExtFun(swl_type_t* type, swl_typeExtUID_t id) {
    swl_llist_iterator_t* it;
    swl_llist_for_each(it, &type->typeFun->extensions) {
        swl_typeExtIt_t* data = swl_llist_item_data(it, swl_typeExtIt_t, it);
        if(data->id == id) {
            return data->extFun;
        }
    }
    return NULL;
}

/**
 * Convert the src data to a char array, ending with '0'.
 * This function will return the number of bytes that would have been written if the buffer were big
 * enough, excluding final '0' char. So basically the swl_str_len of what was printed.
 * If data does not fit, then function will still try to write as much as possible, and end will '0' char
 * If there is an issue unrelated to buffer size, i.e. invalid data or unknown type,
 * a negative number will be returned, matching errno.h encoding to allow snprintf to directly return.
 *
 * So execution of toChar can be considered successful if a positive number is returned that is strictly
 * smaller
 */
ssize_t swl_type_toChar(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    ASSERT_NOT_NULL(type, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTI_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    if(type->typeFun->toChar != NULL) {
        return type->typeFun->toChar(type, tgtStr, tgtStrSize, srcData);
    } else {
        return -ENOSYS;
    }
}


ssize_t swl_type_toCharEsc(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData, const swl_print_args_t* args) {
    ssize_t curSize = swl_type_toChar(type, tgtStr, tgtStrSize, srcData);
    if((curSize < 0) && (srcData == NULL) && (args->nullEncode != NULL)) {
        return snprintf(tgtStr, tgtStrSize, "%s", args->nullEncode);
    }

    if((swl_type_getFlag(type) == SWL_TYPE_FLAG_VALUE) && (curSize > 0)) {
        return swl_str_addEscapeCharPrint(tgtStr, tgtStrSize, curSize, args->charsToEscape, args->escapeChar);
    } else {
        return curSize;
    }
}

bool swl_type_toFilePriv(swl_type_t* type, FILE* stream, const swl_typeData_t* srcData, swl_print_args_t* args) {
    if(type->typeFun->toFile != NULL) {
        return type->typeFun->toFile(type, stream, srcData, args);
    }
    // no to file function => print

    char buffer[128];
    char* printBuf = buffer;
    memset(buffer, 0, sizeof(buffer));
    ssize_t printLen = swl_type_toChar(type, buffer, sizeof(buffer), srcData);
    if(printLen < 0) {
        return false;
    }
    if(printLen >= (ssize_t) sizeof(buffer)) {
        char bigEnoughBuffer[printLen + 1];
        memset(bigEnoughBuffer, 0, sizeof(bigEnoughBuffer));
        printLen = swl_type_toChar(type, bigEnoughBuffer, sizeof(bigEnoughBuffer), srcData);
        if(printLen < 0) {
            return false;
        }
        printBuf = bigEnoughBuffer;
    }
    if(printLen == 0) {
        return true;
    }
    if(printLen < 0) {
        return false;
    }
    swl_print_toStreamBuf(stream, args, printBuf, printLen);
    return true;
}

bool swl_type_toFilePrivPrint(swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    bool success = true;
    swl_typeFlag_e flag = swl_type_getFlag(type);


    if(flag == SWL_TYPE_FLAG_VALUE) {
        if(srcData != NULL) {
            swl_print_startValue(file, args);
            success = swl_type_toFilePriv(type, file, srcData, args);
            swl_print_stopValue(file, args);
        } else if(args->nullEncode != NULL) {
            fprintf(file, "%s", args->nullEncode);
        }
    } else {
        success = swl_type_toFilePriv(type, file, srcData, args);
    }
    return success;
}

bool swl_type_toFile(swl_type_t* type, FILE* stream, const swl_typeData_t* srcData, const swl_print_args_t* inArgs) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(srcData, false, ME, "NULL");

    swl_print_args_t args = inArgs != NULL ? *inArgs : *swl_print_getDefaultArgs();
    return swl_type_toFilePriv(type, stream, srcData, &args);
}

/**
 * Print a given type to the file with given file name.
 */
bool swl_type_toFileName(swl_type_t* type, const char* fileName, const swl_typeData_t* srcData, const swl_print_args_t* inArgs, bool append) {
    FILE* file = fopen(fileName, append ? "a" : "w");
    ASSERT_NOT_NULL(file, false, ME, "Fail to open file %s append %u", fileName, append);
    bool ret = swl_type_toFile(type, file, srcData, inArgs);
    fclose(file);
    return ret;
}


bool swl_type_fromFilePriv(swl_type_t* type, swl_typeEl_t* tgtData, FILE* stream, uint32_t size, const swl_print_args_t* inArgs) {
    char buffer[size + 1];
    memset(buffer, 0, size);
    size_t readSize = fread(buffer, size, 1, stream);
    if((readSize == 0) || (readSize > size)) {
        return false;
    }
    buffer[size] = 0;
    return swl_type_fromCharExt(type, tgtData, buffer, inArgs);
}

/**
 * Read a type from a given file stream.
 *
 * If size is 0, we shall seek the leftover size.
 */
bool swl_type_fromFile(swl_type_t* type, swl_typeEl_t* tgtData, FILE* stream, uint32_t size, const swl_print_args_t* inArgs) {
    swl_print_args_t args = inArgs != NULL ? *inArgs : *swl_print_getDefaultArgs();
    size_t finalSize = (size != 0 ? size : swl_fileUtils_readLeftoverFileSize(stream));
    return swl_type_fromFilePriv(type, tgtData, stream, finalSize, &args);
}

/**
 * Read a type from a file with given file name.
 */
bool swl_type_fromFileName(swl_type_t* type, swl_typeEl_t* tgtData, const char* fileName, const swl_print_args_t* inArgs) {
    uint32_t size = swl_fileUtils_getFileSize(fileName);
    FILE* file = fopen(fileName, "r");
    ASSERT_NOT_NULL(file, false, ME, "Fail to open file %s", fileName);
    bool ret = swl_type_fromFile(type, tgtData, file, size, inArgs);
    fclose(file);
    return ret;
}

char* swl_type_toCString(swl_type_t* type, const swl_typeData_t* srcData) {
    return swl_type_toCStringExt(type, srcData, &g_swl_print_json);
}

char* swl_type_toCStringExt(swl_type_t* type, const swl_typeData_t* srcData, const swl_print_args_t* args) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, NULL, ME, "NULL");

    char* str = NULL;
    size_t len = 0;
    FILE* stream = open_memstream(&str, &len);
    ASSERT_NOT_NULL(stream, NULL, ME, "NULL");

    bool ret = swl_type_toFile(type, stream, srcData, args);
    fclose(stream);

    if(ret) {
        return str;
    } else {
        free(str);
        return NULL;
    }
}

/**
 * Read a type from srcStr.
 */
bool swl_type_fromChar(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    if(type->typeFun->fromChar != NULL) {
        return type->typeFun->fromChar(type, tgtData, srcStr);
    } else if(type->typeFun->fromCharExt != NULL) {
        const swl_print_args_t* args = swl_print_getDefaultArgs();
        return type->typeFun->fromCharExt(type, tgtData, srcStr, args);
    } else {
        return false;
    }
}

bool swl_type_fromCharExt(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    if(type->typeFun->fromCharExt != NULL) {
        return type->typeFun->fromCharExt(type, tgtData, srcStr, args);
    } else {
        return type->typeFun->fromChar(type, tgtData, srcStr);
    }
}

ssize_t swl_type_fromCharPrint(swl_type_t* type, swl_typeEl_t* data, const char* srcStr, const swl_print_args_t* args, swl_print_delim_e delim) {
    ssize_t elSize = swl_print_getNextSize(args, srcStr, delim);
    if(elSize < 0) {
        return SWL_RC_ERROR;
    } else if(elSize == 0) {
        swl_typeFlag_e flag = swl_type_getFlag(type);
        if((flag != SWL_TYPE_FLAG_LIST) && (flag != SWL_TYPE_FLAG_MAP)) {
            ASSERTS_TRUE(swl_type_fromChar(type, data, ""),
                         SWL_RC_ERROR, ME, "ERR");
        }
    } else {
        char buffer[elSize + 1];
        memcpy(buffer, srcStr, elSize);
        buffer[elSize] = 0;
        swl_str_removeWhitespaceEdges(buffer);

        if(swl_str_matches(buffer, args->nullEncode) && !type->typeFun->isValue) {
            swl_typeData_t** ppData = (swl_typeData_t**) data;
            *ppData = NULL;
        } else {
            if((swl_type_getFlag(type) == SWL_TYPE_FLAG_VALUE)) {
                if(args->valueEncap != '\0') {
                    ASSERT_TRUE(swl_str_stripChar(buffer, args->valueEncap), SWL_RC_ERROR, ME, "ERR");
                }

                if(!swl_str_isEmpty(args->charsToEscape)) {
                    ASSERT_TRUE(swl_str_removeEscapeChar(buffer, elSize + 1, args->charsToEscape, args->escapeChar),
                                SWL_RC_ERROR, ME, "ERR");
                }
            }
            ASSERT_TRUE(swl_type_fromCharExt(type, data, buffer, args),
                        SWL_RC_ERROR, ME, "ERR");
        }
    }
    return elSize;
}

/**
 * Returns whether the type data is the empty or NULL data.
 * Shall be true if everything is considered empty in the type.
 * By default, shall return true if all memory is set to zero, or if data is NULL
 */
bool swl_type_isEmpty(swl_type_t* type, swl_typeData_t* data) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    if(data == NULL) {
        return true;
    }
    if(type->typeFun->isValue) {
        swl_bit8_t tmpData[type->size];
        memset(tmpData, 0, type->size);
        return memcmp(data, &tmpData[0], type->size) == 0;
    } else {
        // Reference types shall currently only be considered empty if ptr is NULL
        return false;
    }
}

/**
 * Write a type to a buffer, with special debug code:
 * * (err) if something wrong happens with to char
 * * ... as last characters, if toChar prints more characters than fit in buffer
 * Should be mostly used for debugging.
 */
void swl_type_toBuf(swl_type_t* type, char* buf, size_t bufSize, const swl_typeData_t* srcData) {
    char tmpBuf[bufSize];
    memset(tmpBuf, 0, bufSize);
    ssize_t charLen = swl_type_toCharEsc(type, tmpBuf, bufSize, srcData, &g_swl_print_csv);
    memset(buf, 0, bufSize);
    if(charLen < 0) {
        snprintf(buf, bufSize, "(err)");
    } else if((size_t) charLen >= bufSize) {
        swl_str_copy(buf, bufSize - 3, tmpBuf);
        swl_str_cat(buf, bufSize, "...");
    } else {
        swl_str_copy(buf, bufSize, tmpBuf);
    }
}


/**
 * Return a buf32, containing the swl_type_toBuf print of the type.
 * Can be used for debug printing.
 */
swl_buf32_t swl_type_toBuf32(swl_type_t* type, const swl_typeData_t* srcData) {
    swl_buf32_t output;
    swl_type_toBuf(type, output.buf, sizeof(output.buf), srcData);
    return output;
}

/**
 * Return a buf128, containing the swl_type_toBuf print of the type.
 * Can be used for debug printing.
 */
swl_buf128_t swl_type_toBuf128(swl_type_t* type, const swl_typeData_t* srcData) {
    swl_buf128_t output;
    swl_type_toBuf(type, output.buf, sizeof(output.buf), srcData);
    return output;
}

/**
 * Returns the type flag of a given type.
 * If null is provided, SWL_TYPE_FLAG_REF is returned.
 * Otherwise, the ref type is resolved, and the underlying type is returned.
 */
swl_typeFlag_e swl_type_getFlag(swl_type_t* type) {
    ASSERT_NOT_NULL(type, SWL_TYPE_FLAG_REF, ME, "NULL");
    if(type->typeFun->flag != SWL_TYPE_FLAG_REF) {
        return type->typeFun->flag;
    }
    swl_refType_t* refType = (swl_refType_t*) type;
    return swl_type_getFlag(refType->refType);
}

/**
 * Set the data of a value in a type array to given value.
 * The provided data shall be "deep copied" to target space.
 */
swl_rc_ne swl_type_arraySet(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");

    size_t targetIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(targetIndex < arraySize, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_typeEl_t* target = array + targetIndex * type->size;
    swl_type_copyTo(type, target, data);
    return SWL_RC_OK;
}

/**
 * Adds data to array at given index.
 * This data will be added, at index == SWL_ARRAY_INDEX(index,arraySize)
 * The remaining values will be pushed to the left, so the value at index 0 will be lost.
 * So append on back can be done with index -1.
 *
 * A copy of the data to add will be made when adding
 *
 * returns the unsigned index where data
 */
swl_rc_ne swl_type_arrayAppend(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");

    size_t targetIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(targetIndex < arraySize, SWL_RC_INVALID_PARAM, ME, "SIZE %zi => %zu / %zu", index, targetIndex, arraySize);

    uint32_t elSize = type->size;
    swl_type_cleanup(type, array);
    memmove(array, array + elSize, targetIndex * elSize);
    memset(array + targetIndex * elSize, 0, elSize);
    swl_type_copyTo(type, array + targetIndex * elSize, data);

    return SWL_RC_OK;
}

/**
 * Adds data to array at given index.
 * This data will be added, at index == SWL_ARRAY_INDEX(index,arraySize)
 * The remaining values will be pushed to the right, so the value at index arraySize -1 will be lost.
 * So prepend in front can be done with index 0.
 *
 * A copy of the data to add will be made when adding
 */
swl_rc_ne swl_type_arrayPrepend(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");

    size_t targetIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(targetIndex < arraySize, SWL_RC_INVALID_PARAM, ME, "SIZE %zi => %zu / %zu", index, targetIndex, arraySize);

    uint32_t elSize = type->size;

    swl_type_cleanup(type, array + (arraySize - 1) * type->size);
    uint32_t nrElToShift = arraySize - targetIndex - 1;
    swl_typeEl_t* src = array + targetIndex * elSize;
    memmove(src + elSize, src, nrElToShift * elSize);
    memset(array + targetIndex * elSize, 0, elSize);

    swl_type_copyTo(type, array + targetIndex * elSize, data);
    return SWL_RC_OK;
}

/**
 * Clean the data of a value in a type array to given value.
 * This will free the data at that spot, and make it zero, but it will keep the index available.
 */
swl_rc_ne swl_type_arrayCleanAt(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t index) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");

    size_t targetIndex = SWL_ARRAY_INDEX(index, arraySize);
    swl_typeEl_t* target = array + targetIndex * type->size;
    swl_type_cleanup(type, target);
    return SWL_RC_OK;
}

/**
 * Delete the data of a value in a type array
 * This will free the data at that spot, and shift all remaining elements after the target element one left.
 */
swl_rc_ne swl_type_arrayDelete(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t index) {
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");

    size_t targetIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(targetIndex < arraySize, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_typeEl_t* target = array + targetIndex * type->size;
    swl_type_arrayShift(type, target, arraySize - targetIndex, -1);
    return SWL_RC_OK;
}

/**
 * Shift the array over shiftSize values to the right in a circular fashion. So shiftSize 1 would mean value of index 0 would go
 * to index 1.
 * Values that fall of on one side, will be reappended on the other.
 * Negative shiftSize will shift to left (which is actually equivalent of shifting shiftSize % arraySize)
 * Any value of shiftSize will be allowed, since shifting over arraySize is basically same as not shifting at all.
 */
void swl_type_arrayCircShift(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t shiftSize) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, , ME, "ZERO ARRAY");

    size_t targetShiftSize = swl_math_fmod(shiftSize, arraySize);
    if(targetShiftSize == 0) {
        return;
    }

    size_t elSize = type->size;
    size_t nrElFirst = targetShiftSize % arraySize;
    size_t firstBlockSize = nrElFirst * elSize;
    size_t nrElSecond = arraySize - targetShiftSize;
    size_t secondBlockSize = nrElSecond * elSize;

    char secondBlockData[secondBlockSize];
    memset(secondBlockData, 0, secondBlockSize);

    memcpy(secondBlockData, array + firstBlockSize, secondBlockSize);
    memmove(array + secondBlockSize, array, firstBlockSize);
    memcpy(array, secondBlockData, secondBlockSize);
}

/**
 * Shift the array over shiftSize values to the right. So shiftSize 1 would mean value of index 0 would go
 * to index 1.
 * Values that fall of on one side, will be cleaned up.
 * Negative shiftSize will shift to left (which is actually equivalent of shifting shiftSize % arraySize)
 * Any value of shiftSize will be allowed, but if abs(shiftSize) > arraySize, all data is essentially
 * pushed out.
 */
void swl_type_arrayShift(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t shiftSize) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, , ME, "ZERO ARRAY");
    size_t targetShiftSize = SWL_MATH_ABS(shiftSize);


    if(targetShiftSize >= arraySize) {
        swl_type_arrayCleanup(type, array, arraySize);
        return;
    }
    if(shiftSize == 0) {
        return;
    }

    size_t leftoverNrEl = arraySize - targetShiftSize;
    size_t elSize = type->size;

    if(shiftSize < 0) {

        swl_typeEl_t* subArrayStart = array + elSize * targetShiftSize;
        swl_type_arrayCleanup(type, array, targetShiftSize);
        memmove(array, subArrayStart, leftoverNrEl * elSize);

        swl_typeEl_t* subArrayEnd = array + elSize * leftoverNrEl;
        memset(subArrayEnd, 0, elSize * targetShiftSize);

    } else {
        swl_typeEl_t* cleanArrayStart = array + elSize * leftoverNrEl;
        swl_type_arrayCleanup(type, cleanArrayStart, targetShiftSize);
        swl_typeEl_t* tgtArrayStart = array + elSize * targetShiftSize;

        memmove(tgtArrayStart, array, leftoverNrEl * elSize);
        memset(array, 0, elSize * targetShiftSize);
    }
}

/**
 * Copy the elements from one array to the other. If the target is not empty, the elements shall be cleaned up.
 * @param type
 *  the type of the elements
 * @param tgtArray.
 *  the target to which to copy
 * @param srcArray
 *  the array from where to copy
 * @param size
 *  the number of elements to copy
 * @return
 *  SWL_RC_OK if all ok, an error code otherwise.
 */
swl_rc_ne swl_type_arrayCopy(swl_type_t* type, swl_typeEl_t* tgtArray, const swl_typeEl_t* srcArray, size_t size) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    for(size_t i = 0; i < size; i++) {
        swl_typeEl_t* tgtData = tgtArray + i * type->size;
        const swl_typeData_t* srcData = SWL_TYPE_EL_TO_DATA(type, srcArray + i * type->size);
        swl_type_copyTo(type, tgtData, srcData);
    }
    return SWL_RC_OK;
}

/**
 * Return the pointer to the data at the given index. So this will be different for value vs reference types !
 * This value is directly referenced, so do NOT free it.
 *
 * This function will allow all values of indices, and will return the element matching at index modulo arraySize.
 * So if index is -1, it will return the last value;
 */
swl_typeData_t* swl_type_arrayGetValue(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, ssize_t index) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, NULL, ME, "ZERO ARRAY");

    size_t baseIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(baseIndex < arraySize, NULL, ME, "SIZE %zi => %zu / %zu", index, baseIndex, arraySize);

    return (swl_typeData_t*) SWL_TYPE_TO_PTR(type, array + baseIndex * type->size);
}

/**
 * Return the pointer to the element at given index. So this will be different for value vs reference types !
 * This value is directly referenced, so do NOT free it.
 *
 * This function will allow all values of indices, and will return the element matching to modulo operator.
 * So if index is -1, it will return the last value;
 */
swl_typeEl_t* swl_type_arrayGetReference(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, ssize_t index) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, NULL, ME, "ZERO ARRAY");

    size_t baseIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(baseIndex < arraySize, NULL, ME, "SIZE %zi => %zu / %zu", index, baseIndex, arraySize);

    return (swl_typeEl_t*) array + baseIndex * type->size;
}

/**
 * Return a "deep copy" as defined by the type.
 *
 * This function will allow all values of indices, and will return the element matching to modulo operator.
 * So if index is -1, it will return the last value;
 */
swl_rc_ne swl_type_arrayGet(swl_type_t* type, swl_typeEl_t* tgt, const swl_typeEl_t* array, size_t arraySize, ssize_t index) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tgt, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");

    size_t baseIndex = SWL_ARRAY_INDEX(index, arraySize);
    ASSERT_TRUE(baseIndex < arraySize, SWL_RC_INVALID_PARAM, ME, "SIZE %zi => %zu / %zu", index, baseIndex, arraySize);

    const swl_typeEl_t* targetElement = array + baseIndex * type->size;
    swl_type_copyTo(type, tgt, SWL_TYPE_TO_PTR(type, targetElement));
    return SWL_RC_OK;
}

/**
 * Clean up an array, cleaning up all values inside the array.
 * This will also revert the array to a "zero" state
 */
swl_rc_ne swl_type_arrayCleanup(swl_type_t* type, swl_typeEl_t* array, size_t arraySize) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTI_NOT_EQUALS(arraySize, 0, SWL_RC_INVALID_PARAM, ME, "ZERO ARRAY");


    for(size_t i = 0; i < arraySize; i++) {
        swl_type_cleanup(type, array + i * type->size);
    }

    return SWL_RC_OK;
}

/**
 * Return the "non empty size" of the array
 */
size_t swl_type_arraySize(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize) {
    for(size_t i = 0; i < arraySize; i++) {
        if(swl_type_isEmpty(type, SWL_TYPE_EL_TO_DATA(type, array + i * type->size))) {
            return i;
        }
    }
    return arraySize;
}

/**
 * Returns the number of elements that are not empty
 */
size_t swl_type_arrayNotEmpty(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize) {
    size_t nrEl = 0;
    for(size_t i = 0; i < arraySize; i++) {
        if(!swl_type_isEmpty(type, SWL_TYPE_EL_TO_DATA(type, array + i * type->size))) {
            nrEl++;
        }
    }
    return nrEl;
}

/**
 * Returns the index of the first empty element
 */
ssize_t swl_type_getFirstEmpty(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize) {
    for(size_t i = 0; i < arraySize; i++) {
        if(swl_type_isEmpty(type, SWL_TYPE_EL_TO_DATA(type, array + i * type->size))) {
            return (ssize_t) i;
        }
    }
    return -1;
}

/**
 * Returns the index of the index't element that is not empty.
 * If no such element exists, then -1 is returned;
 */
ssize_t swl_type_getNonEmptyIndex(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, size_t index) {
    size_t nrEl = 0;
    for(size_t i = 0; i < arraySize; i++) {
        if(!swl_type_isEmpty(type, SWL_TYPE_EL_TO_DATA(type, array + i * type->size))) {
            if(index == nrEl) {
                return (ssize_t) i;
            } else {
                nrEl++;
            }
        }
    }
    return -1;
}


static void s_callOnBaseRecursive(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, swl_type_callOnBaseTypes_f callFun, void* userData, swl_typeCallInfo_t* callInfo) {
    swl_typeFlag_e flag = type->typeFun->flag;

    swl_typeCallInfo_t curInfo;
    if(callInfo == NULL) {
        memset(&curInfo, 0, sizeof(swl_typeCallInfo_t));
    } else {
        memcpy(&curInfo, callInfo, sizeof(swl_typeCallInfo_t));
    }

    if((flag == SWL_TYPE_FLAG_VALUE) || (flag == SWL_TYPE_FLAG_NUMBER) || (flag == SWL_TYPE_FLAG_BASIC)) {
        callFun(type, array, arraySize, userData, &curInfo);
    } else if(flag == SWL_TYPE_FLAG_REF) {
        swl_refType_t* refType = (swl_refType_t*) type;
        swl_typeEl_t* newArray[arraySize];
        for(size_t i = 0; i < arraySize; i++) {
            swl_typeEl_t** refData = array[i];
            newArray[i] = (swl_typeEl_t*) *refData;
        }
        callFun(refType->refType, newArray, arraySize, userData, &curInfo);
    } else if(flag == SWL_TYPE_FLAG_LIST) {
        curInfo.depth++;
        swl_listSType_t* collType = (swl_listSType_t*) type;
        swl_listSTypeIt_t itArray[arraySize];
        memset(itArray, 0, sizeof(itArray));
        for(size_t i = 0; i < arraySize; i++) {
            itArray[i] = swl_listSType_getFirstIt(collType, array[i]);
        }
        while(itArray[0].valid) {
            swl_typeEl_t* newArray[arraySize];
            memset(newArray, 0, sizeof(newArray));
            swl_type_t* curType = itArray[0].dataType;
            curInfo.index[curInfo.depth - 1] = itArray[0].index;
            for(size_t i = 0; i < arraySize; i++) {
                newArray[i] = itArray[i].data;
                swl_listSType_nextIt(collType, &itArray[i]);
            }
            s_callOnBaseRecursive(curType, newArray, arraySize, callFun, userData, &curInfo);
        }
    } else if(flag == SWL_TYPE_FLAG_MAP) {
        swl_mapSTypeFun_t* typeFun = type->typeFun->subFun;
        if(typeFun->areKeysFixed) {
            swl_listSTypeIt_t itArray[arraySize];
            memset(itArray, 0, sizeof(itArray));
            for(size_t i = 0; i < arraySize; i++) {
                itArray[i] = typeFun->firstIt(type, array[i]);
            }
            while(itArray[0].valid) {
                swl_typeEl_t* newArray[arraySize];
                memset(newArray, 0, sizeof(newArray));
                swl_type_t* curType = typeFun->getEntryValType(type, array[0], itArray[0].data);
                curInfo.index[curInfo.depth - 1] = itArray[0].index;
                for(size_t i = 0; i < arraySize; i++) {
                    newArray[i] = typeFun->getEntryValRef(type, array[i], itArray[i].data);
                    typeFun->nextIt(type, &itArray[i]);
                }
                s_callOnBaseRecursive(curType, newArray, arraySize, callFun, userData, &curInfo);
            }
        } else {
            swl_listSTypeIt_t it = typeFun->firstIt(type, array[0]);
            while(it.valid) {
                swl_typeEl_t* newArray[arraySize];
                memset(newArray, 0, sizeof(newArray));
                newArray[0] = typeFun->getEntryVal(type, array[0], it.data);
                swl_typeData_t* key = typeFun->getEntryKey(type, array[0], it.data);
                for(size_t i = 1; i < arraySize; i++) {
                    newArray[i] = typeFun->getReference(type, array[i], key);
                }
                curInfo.index[curInfo.depth - 1] = it.index;
                curInfo.key[curInfo.depth - 1] = key;

                s_callOnBaseRecursive(it.dataType, newArray, arraySize, callFun, userData, &curInfo);
                typeFun->nextIt(type, &it);
            }
        }
    }
}


/**
 * Call the callFun on all base types with swl_type_callOnBaseTypes_f.
 * The array presented will be a list of same arraySize as provided, but the array contains the "unrolled base type"
 * For example, if the array contains 3 tuples, of a tuple type made up of charPtr, int8, uint32, then
 * the callFun will be called trice, once for charPtr, once for the int8, once for the uint32.
 *
 * If the type is a map, then for each key entry of the first element in array, this function will be called, with
 * as other locations in the map, the matching entries in the other elements, or NULL if not available.
 *
 * If the type is a list of int8, so suppose the array contains 2 int8 lists then the callFun will be called for each
 * element in the first (index 0) list in the source array. The callFun array will contain the element of the other lists
 * that have the same index as the first element, or NULL if not existing.
 */
void swl_type_callOnBaseTypes(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, swl_type_callOnBaseTypes_f callFun, void* userData) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");
    ASSERT_TRUE(arraySize > 0, , ME, "NULL");

    s_callOnBaseRecursive(type, array, arraySize, callFun, userData, NULL);

}

/**
 * Read an array of types, from a char array, with entries separated by sep.
 */
size_t swl_type_arrayFromCharSep(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const char* srcStr, const char* sep) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, 0, ME, "NULL");
    ASSERT_NOT_NULL(srcStr, 0, ME, "NULL");
    ASSERT_STR(sep, 0, ME, "NULL");
    ASSERTI_STR(srcStr, 0, ME, "Empty");

    size_t nrEl = 0;
    size_t strLen = strlen(srcStr);
    size_t lastSplit = 0;
    size_t splitLen = strlen(sep);
    const char* nextSplitLoc = strstr(srcStr, sep);
    size_t nextSplit = nextSplitLoc != NULL ? (size_t) (nextSplitLoc - srcStr) : strLen;


    memset(tgtArray, 0, tgtArraySize * type->size);
    swl_typeEl_t* curTgtPtr = tgtArray;

    char buffer[SWL_TYPE_BUF_SIZE];

    while(nextSplit + lastSplit <= strLen && nrEl < tgtArraySize) {
        ASSERT_TRUE(nextSplit < sizeof(buffer), nrEl, ME, "Buffer too small");

        swl_str_copy(buffer, nextSplit + 1, &srcStr[lastSplit]);
        if(!swl_type_fromChar(type, curTgtPtr, buffer)) {
            SAH_TRACEZ_ERROR(ME, "conversion failure");
            return nrEl;
        }
        nrEl++;
        curTgtPtr += type->size;
        lastSplit += nextSplit + splitLen;
        if(lastSplit >= strLen) {
            nextSplit = 0;
        } else {
            nextSplitLoc = strstr(&srcStr[lastSplit], sep);
            nextSplit = (nextSplitLoc != NULL ? (size_t) (nextSplitLoc - &srcStr[lastSplit]) : strLen - lastSplit);
        }
    }
    return nrEl;
}

/**
 * Convert an array from a comma separated string. Returns the number of elements in tgtArray.
 *
 * If an element in srcStr can not be correctly converted, an error is printed, and the current
 * number of properly converted elements is returned.
 */
size_t swl_type_arrayFromChar(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const char* srcStr) {
    return swl_type_arrayFromCharSep(type, tgtArray, tgtArraySize, srcStr, ",");
}

/**
 * Convert an array of types, to a char array, with entries seperated by sep.
 */
size_t swl_type_arrayToCharSep(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeEl_t* srcArray, size_t srcArraySize, const char* sep) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, 0, ME, "NULL");
    ASSERT_NOT_NULL(srcArray, 0, ME, "NULL");
    ASSERT_STR(sep, 0, ME, "NULL");

    const swl_typeEl_t* curTgtPtr = srcArray;
    memset(tgtStr, 0, tgtStrSize);

    char buffer[tgtStrSize];
    size_t i = 0;
    ssize_t finalSize = 0;
    ssize_t maxSize = tgtStrSize;
    for(i = 0; i < srcArraySize; i++) {
        memset(buffer, 0, tgtStrSize);
        ssize_t nrBytes = type->typeFun->toChar(type, buffer, tgtStrSize, SWL_TYPE_TO_PTR(type, curTgtPtr));
        if(nrBytes < 0) {
            return nrBytes;
        }
        finalSize += snprintf(&tgtStr[finalSize], SWL_MAX(0, maxSize - finalSize), "%s%s", (i > 0 ? sep : ""), buffer);
        curTgtPtr += type->size;
    }

    return (size_t) finalSize;
}

/**
 * Convert an array to a comma separated string.
 *
 * If an element can not be properly converted, an error is printed, and the current length of the printed string
 * is returned.
 */
size_t swl_type_arrayToChar(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeEl_t* srcArray, size_t srcArraySize) {
    return swl_type_arrayToCharSep(type, tgtStr, tgtStrSize, srcArray, srcArraySize, ",");
}


/**
 * Write a type array to a buffer, with special debug code:
 * * (err) if something wrong happens with to swl_type_arrayToChar
 * * ... as last characters, if toChar prints more characters than fit in buffer
 * Should be mostly used for debugging.
 */
void swl_type_arrayToBuf(swl_type_t* type, char* buf, size_t bufSize, const swl_typeEl_t* srcArray, size_t srcArraySize) {
    char tmpBuf[bufSize];
    memset(tmpBuf, 0, bufSize);
    ssize_t charLen = swl_type_arrayToChar(type, tmpBuf, bufSize, srcArray, srcArraySize);
    memset(buf, 0, bufSize);
    if(charLen < 0) {
        snprintf(buf, bufSize, "(err)");
    } else if((size_t) charLen >= bufSize) {
        swl_str_copy(buf, bufSize - 3, tmpBuf);
        swl_str_cat(buf, bufSize, "...");
    } else {
        swl_str_copy(buf, bufSize, tmpBuf);
    }
}


/**
 * Return a buf32, containing the swl_type_toBuf print of the type.
 * Can be used for debug printing.
 */
swl_buf32_t swl_type_arrayToBuf32(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize) {
    swl_buf32_t output;
    swl_type_arrayToBuf(type, output.buf, sizeof(output.buf), srcArray, srcArraySize);
    return output;
}

/**
 * Return a buf128, containing the swl_type_toBuf print of the type.
 * Can be used for debug printing.
 */
swl_buf128_t swl_type_arrayToBuf128(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize) {
    swl_buf128_t output;
    swl_type_arrayToBuf(type, output.buf, sizeof(output.buf), srcArray, srcArraySize);
    return output;
}

static bool s_canElBePrinted(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, size_t index, bool printEmpty) {
    if(index >= arraySize) {
        return false;
    }
    if(printEmpty) {
        return true;
    } else {
        swl_typeData_t* typeData = swl_type_arrayGetValue(type, array, arraySize, index);

        return !swl_type_isEmpty(type, typeData);
    }
}

ssize_t swl_type_arrayToCharPrint(swl_type_t* type, char* tgtStr, size_t tgtStrSize,
                                  const swl_typeEl_t* srcArray, size_t srcArraySize, bool printEmpty) {

    const swl_print_args_t* args = swl_print_getDefaultArgs();

    ssize_t curSize = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_LIST_OPEN]);
    ssize_t totalSize = curSize;
    ssize_t maxSize = tgtStrSize;


    for(size_t i = 0; s_canElBePrinted(type, srcArray, srcArraySize, i, printEmpty); i++) {
        swl_typeData_t* data = swl_type_arrayGetValue(type, srcArray, srcArraySize, i);
        bool dataCanBePrinted = true;

        swl_typeFlag_e flag = swl_type_getFlag(type);

        if(data == NULL) {
            dataCanBePrinted = false;
            if(args->nullEncode != NULL) {
                ssize_t nullSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->nullEncode);
                ASSERT_TRUE(nullSize > 0, nullSize, ME, "ERR");
                totalSize += nullSize;
            }
        } else if((flag == SWL_TYPE_FLAG_LIST) || (flag == SWL_TYPE_FLAG_MAP)) {
            dataCanBePrinted = !swl_type_isEmpty(type, data);
        }

        if(dataCanBePrinted) {
            ssize_t elSize = swl_type_toCharEsc(type, &tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize),
                                                data, args);
            ASSERT_TRUE(elSize >= 0, elSize, ME, "ERR %p %zi", data, elSize);
            totalSize += elSize;
        }
        if(s_canElBePrinted(type, srcArray, srcArraySize, i + 1, printEmpty)) {
            ssize_t nextSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_NEXT]);
            ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
            totalSize += nextSize;
        }
    }
    curSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_CLOSE]);
    ASSERT_TRUE(curSize > 0, curSize, ME, "ERR");

    totalSize += curSize;
    return totalSize;
}

bool swl_type_arrayToFilePrint(swl_type_t* type, FILE* file,
                               const swl_typeEl_t* srcArray, size_t srcArraySize, bool printEmpty, swl_print_args_t* args) {
    swl_print_addOpen(file, args, true);

    for(size_t i = 0; i < srcArraySize && s_canElBePrinted(type, srcArray, srcArraySize, i, printEmpty); i++) {

        ASSERT_TRUE(swl_type_toFilePrivPrint(type, file, swl_type_arrayGetValue(type, srcArray, srcArraySize, i), args),
                    false, ME, "FAIL");
        if(s_canElBePrinted(type, srcArray, srcArraySize, i + 1, printEmpty)) {
            swl_print_addNext(file, args, true);
        }
    }
    swl_print_addClose(file, args, true);

    return true;
}


char* swl_type_arrayToCStringExt(swl_type_t* type,
                                 const swl_typeEl_t* srcArray, size_t srcArraySize,
                                 bool printEmpty, swl_print_args_t* args) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcArray, NULL, ME, "NULL");


    char* str = NULL;
    size_t len = 0;
    FILE* stream = open_memstream(&str, &len);
    ASSERT_NOT_NULL(stream, NULL, ME, "NULL");

    bool ret = swl_type_arrayToFilePrint(type, stream, srcArray, srcArraySize, printEmpty, args);
    fclose(stream);


    if(ret) {
        return str;
    } else {
        free(str);
        return NULL;
    }
}

char* swl_type_arrayToCString(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize) {
    swl_print_args_t printArg = g_swl_print_json;
    return swl_type_arrayToCStringExt(type, srcArray, srcArraySize, true, &printArg);
}


bool swl_type_arrayFromCharExt(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                               const char* srcStr, const swl_print_args_t* args, size_t* nrParsed) {

    size_t index = strlen(args->delim[SWL_PRINT_DELIM_LIST_OPEN]);
    size_t nextSize = strlen(args->delim[SWL_PRINT_DELIM_LIST_NEXT]);

    size_t i = 0;
    for(i = 0; i < tgtArraySize; i++) {
        if(!swl_print_hasNext(args, &srcStr[index], true)) {
            break;
        }
        if(i > 0) {
            index += nextSize;
        }
        if(swl_type_getFlag(type) != SWL_TYPE_FLAG_VALUE) {
            while(isspace(srcStr[index])) {
                index++;
            }
        }

        swl_typeEl_t* el = swl_type_arrayGetReference(type, tgtArray, tgtArraySize, i);

        ssize_t elSize = swl_type_fromCharPrint(type, el, &srcStr[index], args, SWL_PRINT_DELIM_LIST_NEXT);
        ASSERT_TRUE(elSize >= 0, false, ME, "ERR %zi", elSize);
        index += elSize;
    }

    if(nrParsed != NULL) {
        *nrParsed = i;
    }

    if((i < tgtArraySize) && SWL_BIT_IS_SET(args->parseOptions, SWL_PRINT_PARSE_OPTION_CLEAN_LIST)) {
        for(size_t j = i; j < tgtArraySize; j++) {
            swl_type_cleanup(type, swl_type_arrayGetReference(type, tgtArray, tgtArraySize, j));
        }
    } else if(i == tgtArraySize) {
        if(SWL_BIT_IS_SET(args->parseOptions, SWL_PRINT_PARSE_OPTION_FAIL_ON_SMALL_TGT) &&
           swl_print_hasNext(args, &srcStr[index], true)) {
            return false;
        }
    }

    return true;
}


bool swl_type_arrayFromFilePriv(swl_type_t* type,
                                swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                FILE* stream, size_t size,
                                const swl_print_args_t* inArgs, size_t* nrParsed) {
    char buffer[size + 1];
    memset(buffer, 0, size);
    size_t readSize = fread(buffer, size, 1, stream);
    if((readSize == 0) || (readSize > size)) {
        return false;
    }
    buffer[size] = 0;
    return swl_type_arrayFromCharExt(type, tgtArray, tgtArraySize, buffer, inArgs, nrParsed);
}

bool swl_type_arrayFromFile(swl_type_t* type,
                            swl_typeEl_t* tgtArray, size_t tgtArraySize,
                            FILE* stream, size_t fileSize,
                            const swl_print_args_t* inArgs, size_t* nrParsed) {
    swl_print_args_t args = inArgs != NULL ? *inArgs : *swl_print_getDefaultArgs();
    size_t finalSize = (fileSize != 0 ? fileSize : swl_fileUtils_readLeftoverFileSize(stream));
    return swl_type_arrayFromFilePriv(type, tgtArray, tgtArraySize, stream, finalSize, &args, nrParsed);
}

bool swl_type_arrayFromFileName(swl_type_t* type,
                                swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                const char* fileName,
                                const swl_print_args_t* inArgs, size_t* nrParsed) {
    uint32_t size = swl_fileUtils_getFileSize(fileName);
    FILE* file = fopen(fileName, "r");
    ASSERT_NOT_NULL(file, false, ME, "Fail to open file %s", fileName);
    bool ret = swl_type_arrayFromFile(type, tgtArray, tgtArraySize, file, size, inArgs, nrParsed);
    fclose(file);
    return ret;
}

/**
 * Test whether two arrays are identical, meaning
 * Sizes are equal
 * All elements on the same place are equal
 */
bool swl_type_arrayEquals(swl_type_t* type, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(array1, false, ME, "NULL");
    ASSERT_NOT_NULL(array1, false, ME, "NULL");

    ASSERTS_EQUALS(array1Size, array2Size, false, ME, "Different size %zu %zu", array1Size, array2Size);
    const swl_typeEl_t* val1 = array1;
    const swl_typeEl_t* val2 = array2;

    for(size_t i = 0; i < array1Size; i++) {
        if(!swl_type_equals(type, SWL_TYPE_TO_PTR(type, val1), SWL_TYPE_TO_PTR(type, val2))) {
            return false;
        }
        val1 += type->size;
        val2 += type->size;
    }
    return true;
}

/**
 * count how often value occurs in array
 */
size_t swl_type_arrayCount(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(array, 0, ME, "NULL");

    const swl_typeEl_t* value = array;
    size_t count = 0;

    size_t i = 0;
    for(i = 0; i < arraySize; i++) {
        if(swl_type_equals(type, SWL_TYPE_TO_PTR(type, value), val)) {
            count++;
        }
        value += type->size;
    }

    return count;
}

/**
 * Return the index of the first occurrence of the data type after or equal to offset.
 *
 * if offset is negative, then search will run backward starting at index arraySize + offset, backwards.
 * If offset == -1, the whole array will be searched backward. If offset == -arraySize, it will only look at first element.
 * So the valid offset range is [-arraySize , arraySize -1 ].
 * If offset is out of this range no occurrence can be found.

 * Returns a negative value (-1) if no occurrence can be found.
 *
 */
ssize_t swl_type_arrayFindOffset(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val, ssize_t offset) {
    ASSERT_NOT_NULL(type, -1, ME, "NULL");
    ASSERT_NOT_NULL(array, -1, ME, "NULL");
    ASSERTS_TRUE(arraySize > 0, -1, ME, "EMPTY");
    ASSERT_TRUE(offset < (ssize_t) arraySize, -1, ME, "NULL");

    ASSERT_TRUE(offset >= -1 * ((ssize_t) arraySize), -1, ME, "NULL");

    if(offset >= 0) {
        const swl_typeEl_t* testVal = array + offset * type->size;
        for(size_t i = offset; i < arraySize; i++) {
            if(swl_type_equals(type, SWL_TYPE_TO_PTR(type, testVal), val)) {
                return i;
            }
            testVal += type->size;
        }
    } else {
        ssize_t posOffset = offset + arraySize;
        ASSERT_TRUE(posOffset >= 0, -1, ME, "OUT OF BOUNDS");

        const swl_typeEl_t* testVal = array + posOffset * type->size;

        for(ssize_t i = posOffset; i >= 0; i--) {
            if(swl_type_equals(type, SWL_TYPE_TO_PTR(type, testVal), val)) {
                return i;
            }
            testVal -= type->size;
        }
    }
    return -1;
}

/**
 * Return the index of the first occurrence of val.
 *
 * Returns a negative value (-1) if no occurrence can be found.
 */
ssize_t swl_type_arrayFind(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val) {
    ASSERT_NOT_NULL(type, -1, ME, "NULL");
    ASSERT_NOT_NULL(array, -1, ME, "NULL");
    return swl_type_arrayFindOffset(type, array, arraySize, val, 0);
}

/**
 * Returns a reference to the first occurance of the given data in the array.
 */
swl_typeEl_t* swl_type_arrayGetByData(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val) {
    ssize_t index = swl_type_arrayFind(type, array, arraySize, val);
    if(index < 0) {
        return NULL;
    }
    return swl_type_arrayGetReference(type, array, arraySize, index);
}

/**
 * Return the index of the first occurance of val.
 *
 * If val can not be found, defaultVal will be returned
 */
size_t swl_type_arrayFindDefault(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* val, size_t defaultVal) {
    ssize_t outVal = swl_type_arrayFind(type, array, arraySize, val);
    if(outVal >= 0) {
        return outVal;
    } else {
        return defaultVal;
    }
}

/**
 * Check whether an array of a certain type contains the given value.
 * val can be NULL, when dealing with value types.
 */
bool swl_type_arrayContains(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val) {
    return swl_type_arrayFind(type, array, arraySize, val) >= 0;
}

/**
 * Calculate the diff between array1 and array2, and put the resulting diff in tgtArray, meaning it will do a shallow copy of all elements in array1 that are not in array 2, untill
 * tgtArraySize is filled. It will return the total number of diff, so if return value > tgtArraySize, not all elements were copied.
 *
 * If two elements in array1 are equal, and not present in array2, then this element shall be counted twice in the result.
 * Then, if tgtArray has enough space, a shallow copy is done of both elements.
 * If tgtArray is NULL, tgtArraySize must be 0. In this case, the function will just return the number of elements present in array1, but nor array2.
 * Elements are added to tgtArray in order of array1. This function only looks at presence, so if there are two copies of an element in array1, but only
 * one in array2, these still shall NOT be counted.
 *
 * @param type: the type of array to compare
 * @param tgtArray: the array to put the diff, can be NULL.
 * @param tgtArraySize: the size of tgtArray. Must be 0 if tgtArray is NULL.
 * @param array1: the array to compare from. Must not be NULL.
 * @param array1Size: the size of array 1.
 * @param array2: the array to remove.
 * @param array2Size: the size of array 2.
 *
 * @return the number of elements difference between array1 and array2.
 * If either type, array1, or array2 is NULL, 0 will be returned, and no copy will be done.
 */
size_t swl_type_arrayDiff(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    ASSERT_NOT_NULL(array1, 0, ME, "NULL");
    ASSERT_NOT_NULL(array2, 0, ME, "NULL");
    if(tgtArray == NULL) {
        ASSERT_EQUALS(tgtArraySize, 0, 0, ME, "Illegal Size");
    }
    size_t nrDiff = 0;
    for(size_t i = 0; i < array1Size; i++) {
        const swl_typeEl_t* testVal = array1 + i * type->size;
        if(!swl_type_arrayContains(type, array2, array2Size, SWL_TYPE_EL_TO_DATA(type, testVal))) {
            if(nrDiff < tgtArraySize) {
                swl_typeEl_t* tgtVal = tgtArray + nrDiff * type->size;
                memcpy(tgtVal, testVal, type->size);
            }
            nrDiff++;
        }
    }
    return nrDiff;
}


/**
 * Test whether two arrays match, but are not necessarily equal, meaning
 * * sizes are equal.
 * * each identical element occurs as many times in array1, as in array2. Order does not matter.
 * Basically this checks whether array1 and array2 are permutations of each other.
 */
bool swl_type_arrayMatches(swl_type_t* type, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(array1, false, ME, "NULL");
    ASSERT_NOT_NULL(array1, false, ME, "NULL");

    ASSERTS_EQUALS(array1Size, array2Size, false, ME, "Different size");

    const swl_typeEl_t* testVal = array1;

    size_t i = 0;
    for(i = 0; i < array1Size; i++) {
        size_t count1 = swl_type_arrayCount(type, array1, array1Size, SWL_TYPE_TO_PTR(type, testVal));
        size_t count2 = swl_type_arrayCount(type, array2, array2Size, SWL_TYPE_TO_PTR(type, testVal));
        if(count1 != count2) {
            return false;
        }
        testVal += type->size;
    }
    return true;
}


/**
 * Test whether the superarray contains the subarray, meaning that one can create the subArray from the superArray.
 * This checks that each element in subArray occurs at least as many times in superArray as in subArray
 * Returns true if this is the case, false otherwise.
 * If subArraySize is zero, this condition is always met, so true is returned.
 *
 * This function will return false if type, superArray or subArray are NULL.
 */
bool swl_type_arrayIsSuperset(swl_type_t* type, const swl_typeEl_t* superArray, size_t superArraySize, const swl_typeEl_t* subArray, size_t subArraySize) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    ASSERT_NOT_NULL(superArray, false, ME, "NULL");
    ASSERT_NOT_NULL(subArray, false, ME, "NULL");

    ASSERTS_TRUE(superArraySize >= subArraySize, false, ME, "Sub array larger than super array");

    const swl_typeEl_t* val1 = subArray;

    size_t i = 0;
    for(i = 0; i < subArraySize; i++) {
        size_t count1 = swl_type_arrayCount(type, superArray, superArraySize, SWL_TYPE_TO_PTR(type, val1));
        size_t count2 = swl_type_arrayCount(type, subArray, subArraySize, SWL_TYPE_TO_PTR(type, val1));
        if(count1 < count2) {
            return false;
        }
        val1 += type->size;
    }
    return true;
}

/**
 * extract an array from a given type from an array of structs, containing that type at an offset.
 * also adjust the array based on curIndex, where the first entry in the array will be curIndex.
 *
 * This will allow to extract a chronological window of a single column, from a cyclically filled table.
 *
 * @param type: the type of the element in array.
 * @param tgtArray: the target array, an array of type type.
 * @param srcStructArray: the source array, where the type is in
 * @param offset: the offset of the item in the srcStructArray
 * @param structSize: the size of a single entry of srcStructArray
 * @param curIndex: the index that will be filled in next, so the last filled in entry is (curIndex - 1) % arraySize
 * @param nrValues: the number of values in srcStructArray, and should be equal or smaller then the size of tgtArray.
 *
 * @return the number of values copied, generally equal to nrValues.
 *
 */
size_t swl_type_extractValueArrayFromStructArray(swl_type_t* type, swl_typeEl_t* tgtArray, const swl_typeEl_t* srcStructArray, size_t offset, size_t structSize, size_t curIndex, size_t nrValues) {
    uint32_t index = curIndex;

    if(curIndex >= nrValues) {
        index = 0;
    }

    swl_typeEl_t* tgtPointer = tgtArray;

    for(uint8_t i = 0; i < nrValues; i++) {
        const swl_typeEl_t* srcPointer = srcStructArray + offset + (index * structSize);

        memcpy(tgtPointer, srcPointer, type->size);
        tgtPointer += type->size;

        index = (index + 1) % nrValues;
    }
    return nrValues;
}
