/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_type.h"
#include "swl/swl_common_tupleType.h"
#include "swl/swl_math.h"
#include "swl/swl_string.h"
#include "swl/types/swl_tupleTypeArray.h"

#define ME "swlTta"

/**
 * This file implements a "tuple type array", i.e. an array of tuple types.
 * This allows several extra fuctionality, such as searching elements by index to index etc.
 */

static swl_tuple_t* s_getTuple(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, ssize_t tupleIndex) {
    size_t baseIndex = SWL_ARRAY_INDEX(tupleIndex, arraySize);
    ASSERT_TRUE(baseIndex < arraySize, NULL, ME, "SIZE %zi => %zu / %zu", tupleIndex, baseIndex, arraySize);
    return array + baseIndex * type->type.size;
}

/**
 * Returns a pointer to the tuple in the array at the requested tupleIndex
 *
 * @param type : the type of the tuples
 * @param array: the array to retrieve from
 * @param arraySize: the size of the array
 * @param tupleIndex: the index of the tuple
 */
swl_tuple_t* swl_tta_getTuple(swl_tupleType_t* type, const swl_tuple_t* array, size_t arraySize, ssize_t tupleIndex) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    return swl_type_arrayGetValue(&type->type, array, arraySize, tupleIndex);
}


/**
 * Returns a pointer to the type element in the table. Note that referenced values are not dereferenced.
 *
 * @param type : the type of the tuples
 * @param array: the array to retrieve from
 * @param arraySize: the size of the array
 * @param tupleIndex: the index of the tuple to get. Can be negative according to SWL_ARRAY_INDEX
 * @param typeIndex: the index of the type to get.
 *
 * @return the type element at SWL_ARRAY_INDEX(tupleIndex, table->nrTuples), NULL otherwise
 */
swl_typeEl_t* swl_tta_getElementReference(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_TRUE(typeIndex < type->nrTypes, NULL, ME, "SIZE");

    swl_tuple_t* tuple = swl_tta_getTuple(type, array, arraySize, tupleIndex);
    ASSERT_NOT_NULL(tuple, NULL, ME, "NULL");
    return swl_tupleType_getReference(type, tuple, typeIndex);
}

/**
 * Get a pointer to the type data in the tuple type array. Note that referenced values are not dereferenced.
 *
 * @param type : the type of the tuples
 * @param array: the array to retrieve from
 * @param arraySize: the size of the array
 * @param tupleIndex: the index of the tuple to get. Can be negative according to SWL_ARRAY_INDEX
 * @param typeIndex: the index of the type to get.
 *
 * @return the type data at SWL_ARRAY_INDEX(tupleIndex, table->nrTuples), NULL otherwise
 */
swl_typeData_t* swl_tta_getElementValue(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_TRUE(typeIndex < type->nrTypes, NULL, ME, "SIZE");

    swl_tuple_t* tuple = swl_tta_getTuple(type, array, arraySize, tupleIndex);
    ASSERT_NOT_NULL(tuple, NULL, ME, "NULL");
    return swl_tupleType_getValue(type, tuple, typeIndex);
}

/**
 * Set the value of a given location in the tuple type array.
 *
 * @param type : the type of the tuples
 * @param array: the array to set
 * @param arraySize: the size of the array
 * @param tupleIndex: the index of the tuple to set. Can be negative according to SWL_ARRAY_INDEX
 * @param typeIndex: the index of the type to set.
 * @param data: the data to set.
 *
 * @return SWL_RC_OK if set successfully, SWL_RC_INVALID_PARAM otherwise
 */
swl_rc_ne swl_tta_setElementValue(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                  ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(typeIndex < type->nrTypes, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_typeEl_t* tgt = swl_tta_getElementReference(type, array, arraySize, tupleIndex, typeIndex);
    ASSERTS_NOT_NULL(tgt, SWL_RC_INVALID_PARAM, ME, "INVALID");
    swl_type_copyTo(type->types[typeIndex], tgt, data);
    return SWL_RC_OK;
}

/**
 * Set a row of data in the tuple type array.
 * The old values will be cleaned, and the new values will be copied.
 * A deep copy is used for reference types.
 *
 * @param type : the type of the tuples
 * @param array: the array to set
 * @param arraySize: the size of the array
 * @param tupleIndex: the index of the tuple to set. Can be negative according to SWL_ARRAY_INDEX
 * @param data: the data tuple to set.
 *
 * @return SWL_RC_OK if set successfully, SWL_RC_INVALID_PARAM otherwise
 */
swl_rc_ne swl_tta_setTuple(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                           ssize_t tupleIndex, swl_tuple_t* data) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_typeEl_t* tgt = swl_tta_getTuple(type, array, arraySize, tupleIndex);
    ASSERTS_NOT_NULL(tgt, SWL_RC_INVALID_PARAM, ME, "INVALID");
    swl_type_copyTo(&type->type, tgt, data);
    return SWL_RC_OK;
}

/**
 * Cleanup all data in a given column
 *
 * @param type : the type of the tuples
 * @param array: the array to clean
 * @param arraySize: the size of the array
 * @param typeIndex: the index of the type in the table tuple type, which needs to be cleaned.
 */
void swl_tta_cleanupColumn(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t typeIndex) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");

    swl_type_t* targetType = type->types[typeIndex];
    size_t offSet = swl_tupleType_getOffset(type, typeIndex);

    swl_typeEl_t* element = (array) + offSet;
    for(size_t i = 0; i < arraySize; i++) {
        swl_type_cleanup(targetType, element);
        element += type->type.size;
    }
}

/**
 * Cleanup all data in the columns set by mask.
 *
 * @param type : the type of the tuples
 * @param array: the array to clean
 * @param arraySize: the size of the array
 * @param typeMask: the mask containing the bits of the column indices that need to be cleaned.
 */
void swl_tta_cleanupByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t typeMask) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");

    for(size_t i = 0; i < type->nrTypes; i++) {
        if(SWL_BIT_IS_SET(typeMask, i)) {
            swl_tta_cleanupColumn(type, array, arraySize, i);
        }
    }
}

/**
 * Cleanup all data in the array. This basically resets the array to all zeros
 * Note, this does NOT deallocate the array itself.
 *
 * @param type : the type of the tuples
 * @param array: the array to clean
 * @param arraySize: the size of the array
 */
void swl_tta_cleanup(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize) {
    ASSERT_NOT_NULL(type, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");

    swl_type_arrayCleanup(&type->type, array, arraySize);
}


/**
 * Do a shallow copy of the column of an array of tuples of type targetType, to an array of type matching targetIndex of targetType->types, starting at
 * offset and cycling around if needed.
 *
 * It will copy as much as possible.
 * On failure, SLC_RC_INVALID_PARAM is returned if one of the input parameters are not as expected (NULL or typeIndex or offset out of range).
 * On success tgtArraySize is smaller than srcArraySize, SWL_RC_CONTINUE is returned upon successfully copy (indicating more data is present), otherwise SWL_RC_OK is returned
 */
swl_rc_ne swl_tta_columnToArrayOffset(swl_tupleType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize, const swl_typeEl_t* srcArray, size_t srcArraySize, size_t typeIndex, size_t offSet) {

    ASSERT_NOT_NULL(targetType, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(typeIndex < targetType->nrTypes, SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_TRUE(offSet < srcArraySize, SWL_RC_INVALID_PARAM, ME, "INVALID");


    size_t typeOffset = swl_tupleType_getOffset(targetType, typeIndex);

    swl_type_t* colType = targetType->types[typeIndex];

    size_t typeSize = colType->size;
    size_t ttSize = targetType->type.size;

    const swl_typeEl_t* srcDataPtr = srcArray + typeOffset + (offSet * ttSize);
    swl_typeEl_t* tgtDataPtr = tgtArray;
    for(size_t i = 0; i < srcArraySize && i < tgtArraySize; i++) {
        memcpy(tgtDataPtr, srcDataPtr, typeSize);
        if((i + offSet + 1) == srcArraySize) {
            srcDataPtr = srcArray + typeOffset;
        } else {
            srcDataPtr += ttSize;
        }
        tgtDataPtr += typeSize;
    }
    if(srcArraySize > tgtArraySize) {
        return SWL_RC_CONTINUE;
    }

    return SWL_RC_OK;
}

/**
 * Do a shallow copy the typeIndex'th column of a the tupleTypeArray srcArray, to the tgtArray.
 *
 * It will copy as much as possible.
 * On failure, SLC_RC_INVALID_PARAM is returned if one of the input parameters are not as expected (NULL or typeIndex or offset out of range).
 * On success tgtArraySize is smaller than srcArraySize, SWL_RC_CONTINUE is returned upon successfully copy (indicating more data is present), otherwise SWL_RC_OK is returned
 */
swl_rc_ne swl_tta_columnToArray(swl_tupleType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize, const swl_typeEl_t* srcArray, size_t srcArraySize, size_t typeIndex) {
    return swl_tta_columnToArrayOffset(targetType, tgtArray, tgtArraySize, srcArray, srcArraySize, typeIndex, 0);
}

/**
 * Check whether two tuple type arrays are equal.
 * They match if:
 * * nrTuples is identical
 * * each tuple is identical (so each value in table 1 matches each value in table 2 based on swl_type_matches)
 */
bool swl_tta_equals(swl_tupleType_t* targetType, const swl_typeEl_t* array0, size_t arraySize0, const swl_typeEl_t* array1, size_t arraySize1) {
    ASSERT_NOT_NULL(targetType, false, ME, "NULL");
    ASSERT_NOT_NULL(array0, false, ME, "NULL");
    ASSERT_NOT_NULL(array1, false, ME, "NULL");

    ASSERTS_EQUALS(arraySize0, arraySize1, false, ME, "SIZE MISMATCH");

    for(size_t i = 0; i < arraySize0; i++) {
        ASSERTS_TRUE(swl_tupleType_equals(targetType,
                                          swl_tta_getTuple(targetType, array0, arraySize0, i),
                                          swl_tta_getTuple(targetType, array1, arraySize1, i)),
                     false, ME, "ENTRY MISMATCH");
    }
    return true;
}


/**
 * Returns the first tuple in table, where the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param the data to check
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_tta_getMatchingTuple(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                      size_t offset, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, NULL, ME, "NULL");
    ASSERT_TRUE(srcIndex < type->nrTypes, NULL, ME, "SIZE %zu %zu", srcIndex, type->nrTypes);

    swl_type_t* tgtType = type->types[srcIndex];

    for(size_t i = offset; i < arraySize; i++) {
        swl_tuple_t* tuple = s_getTuple(type, array, arraySize, i);
        swl_typeData_t* data = swl_tupleType_getValue(type, tuple, srcIndex);

        if(swl_type_equals(tgtType, srcValue, data)) {
            return tuple;
        }
    }
    return NULL;
}

/**
 * Returns the first encountered tuple in the array in the direction of search, where srcIndex element of the tuple equals srcValue.
 * {startElem, endElem, stepSize} define the (sub)set and direction of the search. if needed, wraps around the first or last element, at most once.
 *
 * @warning
 * The getMatchingTupleInRange() function has been tested to work for arraySize values inferior to `2^63` and <b>tested to not work for values in
 * [2^63+1..2^64-1]</b>, i.e., the values that map to the negative 64bit integers;
 *
 * @param type: the type of tuples
 * @param array: the array of tuples
 * @param arraySize: the size of the array
 * @param srcIndex: the type index of the tuple, which matches the type of @srcValue
 * @param srcValue: the data against which the tuple content at @srcIndex is checked
 * @param startElem: index of the first tuple in the table to check; negative value implies 'counting from the end'
 * @param endElem: index of the last tuple in the table to check; negative value implies 'counting from the end'
 * @param stepSize: signed step size for the iteration; iteration direction is encoded in the sign
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_tta_getMatchingTupleInRange(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t srcIndex,
                                             const swl_typeData_t* srcValue, ssize_t startElem, ssize_t endElem, ssize_t stepSize) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, NULL, ME, "NULL");
    ASSERT_TRUE(srcIndex < type->nrTypes, NULL, ME, "SIZE %zu %zu", srcIndex, type->nrTypes);

    ASSERT_TRUE((startElem >= (ssize_t) arraySize * (-1) && startElem < (ssize_t) arraySize), NULL, ME, "startElem %zd out of range", startElem);
    ASSERT_TRUE((endElem >= (ssize_t) arraySize * (-1) && endElem < (ssize_t) arraySize), NULL, ME, "endElem %zd out of range", endElem);
    ASSERT_TRUE((stepSize != 0), NULL, ME, "stepSize should not be 0 (curr %zd)", stepSize);

    size_t firstIndex = SWL_ARRAY_INDEX(startElem, arraySize);
    size_t lastIndex = SWL_ARRAY_INDEX(endElem, arraySize);

    size_t absPointCount = ((lastIndex - firstIndex) * (SWL_MATH_SIGN(stepSize)) + arraySize) % arraySize + 1;

    size_t subSampledPointCount = ((absPointCount - 1) / SWL_MATH_ABS(stepSize)) + 1;
    // -1 to convert PointCount to EdgeCount; +1 to convert back to PointCount
    // for the array [ * - * - * - * ] : PointCount == 4; EdgeCount == 3; stepSize is counting Edges

    SAH_TRACEZ_INFO(ME, "subSampledPointCount %zu, (first %zu), (last %zu), (step %zd), (arraySize %zu)",
                    subSampledPointCount, firstIndex, lastIndex, stepSize, arraySize);

    size_t iterIndex = firstIndex;
    size_t absStep = SWL_MATH_ABS(stepSize);

    swl_type_t* tgtType = type->types[srcIndex];

    for(size_t i = 0; i < subSampledPointCount; i++) {
        swl_tuple_t* tuple = s_getTuple(type, array, arraySize, iterIndex);
        swl_typeData_t* data = swl_tupleType_getValue(type, tuple, srcIndex);

        SAH_TRACEZ_INFO(ME, "iteration %zu, table index %zu", i, iterIndex);
        if(swl_type_equals(tgtType, srcValue, data)) {
            return tuple;
        }
        if(stepSize > 0) {
            iterIndex = SWL_ARRAY_INDEX_ADD(iterIndex, absStep, arraySize);
        } else {
            iterIndex = SWL_ARRAY_INDEX_SUB(iterIndex, absStep, arraySize);
        }
    }
    return NULL;
}

/**
 * Returns the type data of the first tuple in table, for which the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param the data to check
 *
 * @return the type data of type tgtTypeIndex of the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists.
 */
swl_typeData_t* swl_tta_getMatchingElement(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                           size_t offset, size_t tgtTypeIndex, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    size_t nrTypes = type->nrTypes;
    ASSERT_TRUE(srcIndex < nrTypes, NULL, ME, "SIZE %zu %zu", srcIndex, nrTypes);
    ASSERT_TRUE(tgtTypeIndex < nrTypes, NULL, ME, "SIZE %zu %zu", tgtTypeIndex, nrTypes);

    swl_tuple_t* tuple = swl_tta_getMatchingTuple(type, array, arraySize, offset, srcIndex, srcValue);
    if(tuple == NULL) {
        return NULL;
    }
    return swl_tupleType_getValue(type, tuple, tgtTypeIndex);
}


/**
 * Find the tuple index of the first tuple after offset, where the element at srcIndex is equal to srcValue
 */
ssize_t swl_tta_findMatchingTupleIndex(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                       size_t offset, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(srcIndex < type->nrTypes, SWL_RC_INVALID_PARAM, ME, "SIZE %zu %zu", srcIndex, type->nrTypes);

    for(size_t i = offset; i < arraySize; i++) {
        swl_tuple_t* tuple = s_getTuple(type, array, arraySize, i);
        swl_typeData_t* data = swl_tupleType_getValue(type, tuple, srcIndex);

        if(swl_type_equals(type->types[srcIndex], srcValue, data)) {
            return i;
        }
    }
    return SWL_RC_INVALID_PARAM;
}


/**
 * Find the tuple index in the array, for the first tuple after offset where the row is equal
 * by mask with srcValue.
 */
ssize_t swl_tta_findMatchingTupleIndexByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                             size_t offset, const swl_tuple_t* srcValue, size_t mask) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(array, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, SWL_RC_INVALID_PARAM, ME, "NULL");

    for(size_t i = offset; i < arraySize; i++) {
        swl_tuple_t* tuple = s_getTuple(type, array, arraySize, i);
        if(swl_tupleType_equalsByMask(type, srcValue, tuple, mask)) {
            return i;
        }
    }

    return SWL_RC_INVALID_PARAM;
}



/**
 * Returns the first tuple in tuple type array, where the elements of the tuple match the given srcValue over a given mask
 *
 * @param table: the table which to check for a match
 * @param srcValue: the source value tuple
 * @param mask: the mask over which to check
 *
 * @return the first swl_tuple_t, which is equal over mask with srcValue
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_tta_getMatchingTupleByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset,
                                            const swl_tuple_t* srcValue, size_t mask) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    ASSERT_NOT_NULL(array, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, NULL, ME, "NULL");

    for(size_t i = offset; i < arraySize; i++) {
        swl_tuple_t* tuple = s_getTuple(type, array, arraySize, i);
        if(swl_tupleType_equalsByMask(type, srcValue, tuple, mask)) {
            return tuple;
        }
    }
    return NULL;
}


/**
 * Returns the type data of the first tuple in table, for which the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param mask: the mask over which to check
 *
 * @return the type data of type tgtTypeIndex of the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists.
 */
swl_typeData_t* swl_tta_getMatchingElementByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                                 size_t offset, size_t tgtTypeIndex, const swl_tuple_t* srcValue, size_t mask) {
    size_t nrTypes = type->nrTypes;
    ASSERT_TRUE(tgtTypeIndex < nrTypes, NULL, ME, "SIZE %zu %zu", tgtTypeIndex, nrTypes);
    // Other asserts delegated to swl_tta_getMatchingTupleByMask

    swl_tuple_t* tuple = swl_tta_getMatchingTupleByMask(type, array, arraySize, offset, srcValue, mask);
    if(tuple == NULL) {
        return NULL;
    }

    size_t dstOffset = swl_tupleType_getOffset(type, tgtTypeIndex);
    return SWL_TYPE_TO_PTR(type->types[tgtTypeIndex], tuple + dstOffset);
}

/**
 * Print the array to char buffer as a list of lists, with each list containing the values in a single column.
 */
ssize_t swl_tta_printLists(swl_tupleType_t* targetType, char* tgtStr, size_t tgtStrSize,
                           const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset) {
    ASSERT_NOT_NULL(targetType, -1, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, -1, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, -1, ME, "NULL");

    const swl_print_args_t* args = swl_print_getDefaultArgs();


    ssize_t maxSize = tgtStrSize;
    ssize_t index = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_LIST_OPEN]);

    for(size_t i = 0; i < targetType->nrTypes; i++) {
        if(i > 0) {
            ssize_t nextSize = snprintf(&tgtStr[index], SWL_MAX(0, maxSize - index), "%s", args->delim[SWL_PRINT_DELIM_LIST_NEXT]);
            ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
            index += nextSize;
        }

        swl_type_t* tmpType = targetType->types[i];
        size_t bufSize = tgtArraySize * tmpType->size;
        char buffer[bufSize];
        swl_typeEl_t* tmpArray = &buffer[0];
        memset(buffer, 0, bufSize);
        swl_tta_columnToArrayOffset(targetType, tmpArray, tgtArraySize, tgtArray, tgtArraySize, i, offset);

        ssize_t tmpIndex = SWL_MIN(index, maxSize);
        ssize_t tmpSize = tgtStrSize - tmpIndex;
        ssize_t output = swl_type_arrayToCharPrint(tmpType, &tgtStr[tmpIndex], tmpSize, tmpArray, tgtArraySize, true);
        if(output < 0) {
            return output;
        }
        index += (size_t) output;
    }

    ssize_t nextSize = snprintf(&tgtStr[index], SWL_MAX(0, maxSize - index), "%s", args->delim[SWL_PRINT_DELIM_LIST_CLOSE]);
    ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
    index += nextSize;

    return index;
}

/**
 * Print the array to file stream as a list of lists, with each list containing the values in a single column.
 */
bool swl_tta_fprintLists(swl_tupleType_t* targetType, FILE* file,
                         const swl_typeEl_t* tgtArray, size_t tgtArraySize,
                         size_t offset, bool printEmpty, swl_print_args_t* args) {

    ASSERT_NOT_NULL(targetType, false, ME, "NULL");
    ASSERT_NOT_NULL(file, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, false, ME, "NULL");

    swl_print_addOpen(file, args, true);

    size_t finalListSize = tgtArraySize;

    if(!printEmpty) {
        ssize_t emptyIndex = swl_type_getFirstEmpty(&targetType->type, tgtArray, tgtArraySize);
        if(emptyIndex >= 0) {
            finalListSize = emptyIndex;
        }
    }


    for(size_t i = 0; i < targetType->nrTypes; i++) {
        if(i > 0) {
            swl_print_addNext(file, args, true);
        }

        swl_type_t* tmpType = targetType->types[i];
        size_t bufSize = finalListSize * tmpType->size;
        char buffer[bufSize];
        swl_typeEl_t* tmpArray = &buffer[0];
        memset(buffer, 0, bufSize);
        swl_tta_columnToArrayOffset(targetType, tmpArray, finalListSize, tgtArray, finalListSize, i, offset);

        ASSERT_TRUE(swl_type_arrayToFilePrint(tmpType, file,
                                              tmpArray, finalListSize, printEmpty, args),
                    false, ME, "FAIL");

    }
    swl_print_addClose(file, args, true);

    return true;
}


/**
 * Print the array to char buffer as a map of lists, with each map entry containing the values in a single column, with as
 * key the value in tgtArray, appended with suffix.
 */
ssize_t swl_tta_printMaps(swl_tupleType_t* targetType, char* tgtStr, size_t tgtStrSize,
                          const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset,
                          char** names, char* suffix) {
    ASSERT_NOT_NULL(targetType, -1, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, -1, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, -1, ME, "NULL");
    ASSERT_NOT_NULL(names, -1, ME, "NULL");
    const swl_print_args_t* args = swl_print_getDefaultArgs();

    ssize_t maxSize = tgtStrSize;
    ssize_t index = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_MAP_OPEN]);

    for(size_t i = 0; i < targetType->nrTypes; i++) {
        if(i > 0) {
            ssize_t nextSize = snprintf(&tgtStr[index], SWL_MAX(0, maxSize - index), "%s", args->delim[SWL_PRINT_DELIM_MAP_NEXT]);
            ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
            index += nextSize;
        }

        swl_type_t* tmpType = targetType->types[i];
        size_t bufSize = tgtArraySize * tmpType->size;
        char arrayBuffer[bufSize];
        swl_typeEl_t* tmpArray = &arrayBuffer[0];
        memset(arrayBuffer, 0, bufSize);

        ssize_t tmpIndex = SWL_MIN(index, maxSize);
        ssize_t tmpSize = tgtStrSize - tmpIndex;
        ssize_t output = snprintf(&tgtStr[tmpIndex], tmpSize, "%s%s", names[i], suffix != NULL ? suffix : "");
        if(output < 0) {
            return output;
        }
        index += (size_t) output;

        ssize_t assignSize = snprintf(&tgtStr[index], SWL_MAX(0, maxSize - index), "%s", args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
        ASSERT_TRUE(assignSize > 0, assignSize, ME, "ERR");
        index += assignSize;

        swl_tta_columnToArrayOffset(targetType, tmpArray, tgtArraySize, tgtArray, tgtArraySize, i, offset);

        tmpIndex = SWL_MIN(index, maxSize);
        tmpSize = tgtStrSize - tmpIndex;
        output = swl_type_arrayToCharPrint(tmpType, &tgtStr[tmpIndex], tmpSize, tmpArray, tgtArraySize, true);
        if(output < 0) {
            return output;
        }
        index += (size_t) output;


    }

    ssize_t nextSize = snprintf(&tgtStr[index], SWL_MAX(0, maxSize - index), "%s", args->delim[SWL_PRINT_DELIM_MAP_CLOSE]);
    ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
    index += nextSize;

    return index;
}
/**
 * Print the array to stream as a map of lists, with each map entry containing the values in a single column, with as
 * key the value in tgtArray, appended with suffix.
 */
bool swl_tta_fprintMaps(swl_tupleType_t* targetType, FILE* file, const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset,
                        bool printEmpty, swl_print_args_t* args,
                        char** names, char* suffix) {

    ASSERT_NOT_NULL(targetType, false, ME, "NULL");
    ASSERT_NOT_NULL(file, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, false, ME, "NULL");

    size_t suffixLen = swl_str_len(suffix);

    swl_print_addOpen(file, args, false);

    for(size_t i = 0; i < targetType->nrTypes; i++) {
        if(i > 0) {
            swl_print_addNext(file, args, false);
        }

        swl_print_startValue(file, args);
        ASSERT_TRUE(swl_print_toStreamBuf(file, args, names[i], swl_str_len(names[i])), false, ME, "FAIL");
        if(suffix != NULL) {
            ASSERT_TRUE(swl_print_toStreamBuf(file, args, suffix, suffixLen), false, ME, "FAIL");
        }
        swl_print_stopValue(file, args);

        swl_print_addAssign(file, args);


        swl_type_t* tmpType = targetType->types[i];
        // Buffer should be min size 1, otherwise code check complains.
        size_t bufSize = SWL_MAX((size_t) 1, tgtArraySize * tmpType->size);
        char buffer[bufSize];
        swl_typeEl_t* tmpArray = &buffer[0];
        memset(buffer, 0, bufSize);
        if(tgtArraySize > 0) {
            ASSERT_TRUE(swl_rc_isOk(
                            swl_tta_columnToArrayOffset(targetType, tmpArray, tgtArraySize, tgtArray, tgtArraySize, i, offset)),
                        false, ME, "FAIL");
        }

        ASSERT_TRUE(swl_type_arrayToFilePrint(tmpType, file,
                                              tmpArray, tgtArraySize, printEmpty, args),
                    false, ME, "FAIL");

    }
    swl_print_addClose(file, args, false);

    return true;
}


