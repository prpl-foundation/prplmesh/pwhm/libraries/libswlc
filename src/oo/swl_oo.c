/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>

#include <swl/swl_common.h>
#include "swl/oo/swl_oo.h"

#define ME "swl_oo"

swl_oo_class_t_fun* s_getFun(swl_oo_class_t_vft* data, size_t offset) {
    ASSERT_NOT_NULL(data, NULL, ME, "NULL");

    if(offset >= data->vftFunSize) {
        return NULL;
    }
    swl_oo_class_t_fun* fun = &data->vft;
    void** funPtr = (void**) ((char*) fun + offset);
    if(*(funPtr) != NULL) {
        return fun;
    }
    if(data->superPtr != NULL) {
        return s_getFun(data->superPtr, offset);
    }
    return NULL;
}


swl_oo_class_t_fun* swl_oo_getFun(swl_oo_class_t* data, size_t offset) {
    ASSERT_NOT_NULL(data, NULL, ME, "NULL");
    return s_getFun(data->vft, offset);
}

bool s_isInstanceOf(swl_oo_class_t_vft* checkVft, swl_oo_class_t_vft* objVft) {
    ASSERT_NOT_NULL(checkVft, NULL, ME, "NULL");
    ASSERT_NOT_NULL(objVft, NULL, ME, "NULL");

    if(checkVft == objVft) {
        return true;
    }
    if(objVft->superPtr == NULL) {
        return false;
    }
    return s_isInstanceOf(checkVft, objVft->superPtr);
}



uint32_t s_getDepth(swl_oo_class_t_vft* checkVft) {
    if((checkVft->depth != 0) || ((checkVft->depth == 0) && (checkVft->superPtr == NULL))) {
        return checkVft->depth;
    }
    uint32_t depth = s_getDepth(checkVft->superPtr);
    checkVft->depth = depth + 1;
    return checkVft->depth;
}

bool swl_oo_isInstanceOf(swl_oo_class_t_vft* vft, swl_oo_class_t* obj) {
    bool retVal = s_isInstanceOf(vft, obj->vft);
    return retVal;
}

/**
 * Find common vft. Note that this assumes depth is equal
 */
swl_oo_class_t_vft* s_findCommonTogether(swl_oo_class_t_vft* vft1, swl_oo_class_t_vft* vft2) {
    // Depth MUST be equal, NOT checking for static method as we recursively call
    if(vft1 == vft2) {
        return vft1;
    }
    if((vft1->superPtr == NULL) || (vft2->superPtr == NULL)) {
        return NULL;
    }
    return s_findCommonTogether(vft1->superPtr, vft2->superPtr);
}

swl_oo_class_t_vft* s_goUp(swl_oo_class_t_vft* vft1, uint32_t nrUp) {
    if((nrUp == 0) || (vft1->superPtr == NULL)) {
        return vft1;
    }
    return s_goUp(vft1->superPtr, nrUp - 1);
}

swl_oo_class_t_vft* swl_oo_findCommonSuperClass(swl_oo_class_t_vft* vft1, swl_oo_class_t_vft* vft2) {
    ASSERT_NOT_NULL(vft1, NULL, ME, "NULL");
    ASSERT_NOT_NULL(vft2, NULL, ME, "NULL");

    uint32_t depth1 = s_getDepth(vft1);
    uint32_t depth2 = s_getDepth(vft2);
    if(depth1 == depth2) {
        return s_findCommonTogether(vft1, vft2);
    } else if(depth2 > depth1) {
        return s_findCommonTogether(vft1, s_goUp(vft2, depth2 - depth1));
    } else {
        return s_findCommonTogether(s_goUp(vft1, depth1 - depth2), vft2);
    }
}

swl_trait_trt* s_findClassVft(swl_oo_class_t_vft* class, swl_traitId_t* traitId) {
    for(uint32_t i = 0; i < class->nrTraits; i++) {
        if(class->traits[i].traitId == traitId) {
            return class->traits[i].vft;
        }
    }
    if(class->superPtr == NULL) {
        return NULL;
    }
    return s_findClassVft(class->superPtr, traitId);
}

swl_trait_trt* swl_trait_findVft(swl_oo_class_t* obj, swl_traitId_t* traitId) {
    ASSERT_NOT_NULL(obj, NULL, ME, "NULL");
    ASSERT_NOT_NULL(traitId, NULL, ME, "NULL");

    return s_findClassVft(obj->vft, traitId);
}

swl_trait_trt* swl_trait_findCommonVft(swl_oo_class_t* obj1, swl_oo_class_t* obj2, swl_traitId_t* traitId) {
    ASSERT_NOT_NULL(obj1, NULL, ME, "NULL");
    ASSERT_NOT_NULL(obj2, NULL, ME, "NULL");

    swl_oo_class_t_vft* commonVft = swl_oo_findCommonSuperClass(obj1->vft, obj2->vft);
    if(commonVft == NULL) {
        return NULL;
    }
    return s_findClassVft(commonVft, traitId);
}

/**
 * Root class implementation
 */

static void s_destroy(swl_ooRoot_t* selfi _UNUSED) {

}

swl_trait_trt_t swl_ooRoot_t_traitArr[] = {};

swl_ooRoot_t_vft swl_ooRoot_t_vfti = {
    .name = "swl_ooRoot_t",
    .size = sizeof(swl_ooRoot_t),
    .vftSize = sizeof(swl_ooRoot_t_fun),
    .vft = {
        .destroy = s_destroy,
    },
    .nrTraits = SWL_ARRAY_SIZE(swl_ooRoot_t_traitArr),
    .traits = swl_ooRoot_t_traitArr,
};
