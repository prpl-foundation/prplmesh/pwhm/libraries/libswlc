/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/oo/swl_compare_trait.h"

#include "swl/swl_common.h"
#include "swl/oo/swl_oo.h"

/**
 * Implement a comparator trait.
 * The comparator shall compare two objects, using the lowest "common" class that implements
 * the comparator vft.
 *
 * Note that the "direct comparators" that return bool, will always return false if the objects can not be compared.
 */

SWL_OO_TRT_ID(swl_compare_trt);

swl_comparison_e swl_compare_doCompare(swl_oo_pclass_t* selfi, swl_oo_pclass_t* targeti) {
    swl_oo_class_t* self = (swl_oo_class_t*) selfi;
    swl_oo_class_t* target = (swl_oo_class_t*) targeti;
    swl_compare_trt* trait = (swl_compare_trt*) swl_trait_findCommonVft(self, target, &swl_compare_trt_uid);
    if(trait == NULL) {
        return SWL_COMPARISON_NOT_MATCHING;
    }
    return trait->compare(self, target);
}

/**
 * Compare two objects, returning true if they are equal
 * according to the lowest shared implementation of comparator trait, false otherwise
 */
bool swl_compare_areEqual(swl_oo_pclass_t* self, swl_oo_pclass_t* target) {
    return swl_compare_doCompare(self, target) == SWL_COMPARISON_EQUAL;
}

/**
 * Compare two objects, returning true if self is less than target
 * according to the lowest shared implementation of comparator trait, false otherwise
 */
bool swl_compare_isLess(swl_oo_pclass_t* self, swl_oo_pclass_t* target) {
    return swl_compare_doCompare(self, target) == SWL_COMPARISON_LESS;
    return false;

}

/**
 * Compare two objects, returning true if self is more than target
 * according to the lowest shared implementation of comparator trait, false otherwise
 */
bool swl_compare_isMore(swl_oo_pclass_t* self, swl_oo_pclass_t* target) {
    return swl_compare_doCompare(self, target) == SWL_COMPARISON_MORE;

}
bool swl_compare_isLessOrEqual(swl_oo_pclass_t* self, swl_oo_pclass_t* target) {
    swl_comparison_e compareVal = swl_compare_doCompare(self, target);
    return (compareVal == SWL_COMPARISON_EQUAL) || (SWL_COMPARISON_EQUAL == SWL_COMPARISON_LESS);

}
bool swl_compare_isMoreOrEqual(swl_oo_pclass_t* self, swl_oo_pclass_t* target) {
    swl_comparison_e compareVal = swl_compare_doCompare(self, target);
    return (compareVal == SWL_COMPARISON_EQUAL) || (SWL_COMPARISON_EQUAL == SWL_COMPARISON_MORE);
}
