/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include "swl/swl_genericFrameParser.h"
#include "swl/swl_common_table.h"
#include "swl/swl_common_mcs.h"
#include "swl/swl_hex.h"

#define ME "swlParser"

static void s_parseWEP40(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "M_SWL_80211_WEP_40");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_WEP40;
}

static void s_parseTKIP(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "M_SWL_80211_TKIP");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_TKIP;
}

static void s_parsCCMP128(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "M_SWL_80211_CCMP_128");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_CCMP;
}

static void s_parseWEP104(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_WEP_104");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_WEP104;
}

static void s_parseCMAC128(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_BIP_CMAC_128");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_BIP_CMAC;
}

static void s_parseGCMP128(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_GCMP_128");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_GCMP;
}

static void s_parseGCMP256(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_GCMP_256");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_GCMP256;
}

static void s_parseCCMP256(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_CCMP_256");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_CCMP256;
}

static void s_parseGMAC128(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_CIPHER_BIP_GMAC_128");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_BIP_GMAC;
}

static void s_parseGMAC256(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_BIP_GMAC_256");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_BIP_GMAC256;
}

static void s_parseCMAC256(swl_80211_cipher_m* cipherSuiteParams) {
    ASSERTS_NOT_NULL(cipherSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_BIP_CMAC_256");
    *cipherSuiteParams |= M_SWL_80211_CIPHER_BIP_CMAC256;
}

SWL_TABLE(dataCipherSuiteTable,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR({SWL_80211_CIPHER_SUITE_WEP_40, (void*) s_parseWEP40},
              {SWL_80211_CIPHER_SUITE_TKIP, (void*) s_parseTKIP},
              {SWL_80211_CIPHER_SUITE_CCMP_128, (void*) s_parsCCMP128},
              {SWL_80211_CIPHER_SUITE_WEP_104, (void*) s_parseWEP104},
              {SWL_80211_CIPHER_SUITE_BIP_CMAC_128, (void*) s_parseCMAC128},
              {SWL_80211_CIPHER_SUITE_GCMP_128, (void*) s_parseGCMP128},
              {SWL_80211_CIPHER_SUITE_GCMP_256, (void*) s_parseGCMP256},
              {SWL_80211_CIPHER_SUITE_CCMP_256, (void*) s_parseCCMP256},
              {SWL_80211_CIPHER_SUITE_BIP_GMAC_128, (void*) s_parseGMAC128},
              {SWL_80211_CIPHER_SUITE_BIP_GMAC_256, (void*) s_parseGMAC256},
              {SWL_80211_CIPHER_SUITE_BIP_CMAC_256, (void*) s_parseCMAC256}, )
          );
typedef void (* cipherParseFun_f) (swl_80211_cipher_m* cipherSuiteParams);


swl_rc_ne swl_80211_getDataCipherSuite(uint8_t type, swl_80211_cipher_m* cipherSuiteParams) {
    ASSERT_NOT_NULL(cipherSuiteParams, SWL_RC_INVALID_PARAM, ME, "NULL");

    void** func = (void**) swl_table_getMatchingValue(&dataCipherSuiteTable, 1, 0, (swl_80211_elId_ne*) &type);
    if(func != NULL) {
        cipherParseFun_f parseFunc = (cipherParseFun_f) (*func);
        parseFunc(cipherSuiteParams);
    } else {
        SAH_TRACEZ_INFO(ME, "Unsupported data cipher type %d", type);
    }

    return SWL_RC_OK;
}

static void s_parseIEEE_802_1X(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_IEEE_802_1X");
    *akmSuiteParams |= M_SWL_80211_AKM_IEEE_802_1X;
}

static void s_parsePSK(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_PSK");
    *akmSuiteParams |= M_SWL_80211_AKM_PSK;
}

static void s_parseFT_IEEE_802_1X(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_FT_IEEE_802_1X");
    *akmSuiteParams |= M_SWL_80211_AKM_FT_IEEE_802_1X;
}

static void s_parseFT_PSK(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_FT_PSK");
    *akmSuiteParams |= M_SWL_80211_AKM_FT_PSK;
}

static void s_parseIEEE_802_1X_SHA_256(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_IEEE_802_1X_SHA_256");
    *akmSuiteParams |= M_SWL_80211_AKM_IEEE_802_1X_SHA_256;
}

static void s_parsePSK_SHA_256(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_PSK_SHA_256");
    *akmSuiteParams |= M_SWL_80211_AKM_PSK_SHA_256;
}

static void s_parseTDLS_TPK(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_TDLS_TPK");
    *akmSuiteParams |= M_SWL_80211_AKM_TDLS_TPK;
}

static void s_parseSAE(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_SAE");
    *akmSuiteParams |= M_SWL_80211_AKM_SAE;
}

static void s_parseFT_SAE(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_FT_SAE");
    *akmSuiteParams |= M_SWL_80211_AKM_FT_SAE;
}

static void s_parseAES_256(swl_80211_akm_m* akmSuiteParams) {
    ASSERTS_NOT_NULL(akmSuiteParams, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_AES_256");
    *akmSuiteParams |= M_SWL_80211_AKM_AES_256;
}

SWL_TABLE(dataAKMsuite,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR({SWL_80211_AKM_SUITES_IEEE_802_1X, (void*) s_parseIEEE_802_1X},
              {SWL_80211_AKM_SUITES_PSK, (void*) s_parsePSK},
              {SWL_80211_AKM_SUITES_FT_IEEE_802_1X, (void*) s_parseFT_IEEE_802_1X},
              {SWL_80211_AKM_SUITES_FT_PSK, (void*) s_parseFT_PSK},
              {SWL_80211_AKM_SUITES_IEEE_802_1X_SHA_256, (void*) s_parseIEEE_802_1X_SHA_256},
              {SWL_80211_AKM_SUITES_PSK_SHA_256, (void*) s_parsePSK_SHA_256},
              {SWL_80211_AKM_SUITES_TDLS_TPK, (void*) s_parseTDLS_TPK},
              {SWL_80211_AKM_SUITES_SAE, (void*) s_parseSAE},
              {SWL_80211_AKM_SUITES_FT_SAE, (void*) s_parseFT_SAE},
              {SWL_80211_AKM_SUITES_AES_256, (void*) s_parseAES_256}, )
          );
typedef void (* akmParseFun_f) (swl_80211_akm_m* akmSuiteParams);


swl_rc_ne swl_80211_getDataAKMsuite(uint8_t type, swl_80211_akm_m* akmSuiteParams) {
    ASSERT_NOT_NULL(akmSuiteParams, SWL_RC_INVALID_PARAM, ME, "NULL");

    void** func = (void**) swl_table_getMatchingValue(&dataAKMsuite, 1, 0, (swl_80211_elId_ne*) &type);
    if(func != NULL) {
        akmParseFun_f parseFunc = (akmParseFun_f) (*func);
        parseFunc(akmSuiteParams);
    } else {
        SAH_TRACEZ_INFO(ME, "Unsupported akm type %d", type);
    }

    return SWL_RC_OK;
}


static void s_parseHtCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    swl_80211_htCapIE_t* htCap = (swl_80211_htCapIE_t*) (frm);
    ASSERTS_NOT_NULL(htCap, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    bool sgi = false;
    if(htCap->htCapInfo & M_SWL_80211_HTCAPINFO_CAP_40) {
        pWirelessDevIE->htCapabilities |= M_SWL_STACAP_HT_40MHZ;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HT_40MHZ cap");
    }
    if(htCap->htCapInfo & M_SWL_80211_HTCAPINFO_SGI_20) {
        pWirelessDevIE->htCapabilities |= M_SWL_STACAP_HT_SGI20;
        sgi = true;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HT_SGI20 cap");
    }
    if(htCap->htCapInfo & M_SWL_80211_HTCAPINFO_SGI_40) {
        pWirelessDevIE->htCapabilities |= M_SWL_STACAP_HT_SGI40;
        sgi = true;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HT_SGI40 cap");
    }
    if(htCap->htCapInfo & M_SWL_80211_HTCAPINFO_INTOL_40) {
        pWirelessDevIE->htCapabilities |= M_SWL_STACAP_HT_40MHZ_INTOL;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HT_40MHZ_INTOL cap");
    }

    // Number of streams
    uint16_t nss = 0;
    for(uint16_t j = SWL_80211_HT_MCSSET_20_40_NSS1; j <= SWL_80211_HT_MCSSET_20_40_NSS4; j++) {
        if(htCap->supMCSSet[j]) {
            nss++;
        }
    }

    if(nss != 0) {
        pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_N;
    }

    pWirelessDevIE->maxRxSpatialStreamsSupported = nss;
    pWirelessDevIE->maxTxSpatialStreamsSupported = nss;

    swl_mcs_t maxmcs;
    memset(&maxmcs, 0, sizeof(swl_mcs_t));
    maxmcs.numberOfSpatialStream = nss;
    maxmcs.bandwidth = (htCap->htCapInfo & M_SWL_80211_HTCAPINFO_CAP_40) ? SWL_BW_40MHZ : SWL_BW_20MHZ;
    maxmcs.mcsIndex = SWL_MCS_MAX_HT_MCS;
    maxmcs.guardInterval = (sgi) ? SWL_SGI_400 : SWL_SGI_800;

    if((pWirelessDevIE->maxDownlinkRateSupported == 0) && (pWirelessDevIE->maxUplinkRateSupported == 0)) {
        pWirelessDevIE->maxDownlinkRateSupported = swl_mcs_toRate(&maxmcs);
        pWirelessDevIE->maxUplinkRateSupported = pWirelessDevIE->maxDownlinkRateSupported;
    }

    memset(&pWirelessDevIE->supportedHtMCS, 0, sizeof(swl_mcs_supMCS_t));
    uint32_t* mcsFlags = (uint32_t*) (htCap->supMCSSet);
    // 0 to 31 - the first 4 bytes/32 bits
    for(uint32_t i = 0; i < SWL_80211_MCS_HT_NSS_SET_NUM_BITS; i++) {
        if(SWL_BIT_IS_SET(*mcsFlags, i)) {
            pWirelessDevIE->supportedHtMCS.mcs[i] = i;
            pWirelessDevIE->supportedHtMCS.mcsNbr++;
        }
        // currently not more then 32
        if(pWirelessDevIE->supportedHtMCS.mcsNbr == SWL_80211_MCS_HT_NSS_SET_NUM_BITS) {
            break;
        }
    }

    SAH_TRACEZ_INFO(ME, "Parsing HT Capabilities from %02x to %x", htCap->htCapInfo, pWirelessDevIE->htCapabilities);
}

static uint8_t s_getNss(uint32_t nssMask) {
    uint8_t nss = 0;
    for(uint32_t i = 0; i < (SWL_BIT_SIZE(nssMask)); i += 2) {
        if(((nssMask >> i) & 0x3) == 0x3) {
            //0x3: SS Not supported
            //can leave as soon as detecting nSS not supported value
            break;
        }
        nss++;
    }
    return nss;
}

static void s_parseVhtCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    swl_80211_vhtCapIE_t* vhtCap = (swl_80211_vhtCapIE_t*) (frm);
    ASSERTS_NOT_NULL(vhtCap, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_AC;
    bool sgi = false;
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_SGI_80) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_SGI80;
        sgi = true;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_SGI80 cap");
    }
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_SGI_160) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_SGI160;
        sgi = true;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_SGI160 cap");
    }
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_SU_BFR) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_SU_BFR;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_SU_BFR cap");
    }
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_SU_BFE) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_SU_BFE;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_SU_BFE cap");
    }
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_MU_BFR) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_MU_BFR;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_MU_BFR cap");
    }
    if(vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_MU_BFE) {
        pWirelessDevIE->vhtCapabilities |= M_SWL_STACAP_VHT_MU_BFE;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_VHT_MU_BFE cap");
    }

    swl_mcs_t maxmcs;
    memset(&maxmcs, 0, sizeof(swl_mcs_t));
    maxmcs.bandwidth = (vhtCap->vhtCapInfo & M_SWL_80211_VHTCAPINFO_SGI_160) ? SWL_BW_160MHZ : SWL_BW_80MHZ;
    maxmcs.mcsIndex = SWL_MCS_MAX_VHT_MCS;
    maxmcs.guardInterval = (sgi) ? SWL_SGI_400 : SWL_SGI_800;

    maxmcs.numberOfSpatialStream = s_getNss(vhtCap->rxMcsMap);
    pWirelessDevIE->maxRxSpatialStreamsSupported = maxmcs.numberOfSpatialStream;
    pWirelessDevIE->maxDownlinkRateSupported = swl_mcs_toRate(&maxmcs);

    maxmcs.numberOfSpatialStream = s_getNss(vhtCap->txMcsMap);
    pWirelessDevIE->maxTxSpatialStreamsSupported = maxmcs.numberOfSpatialStream;
    pWirelessDevIE->maxUplinkRateSupported = swl_mcs_toRate(&maxmcs);

    swl_mcs_supMCS_adv_t* suppVhtRxMcs = &pWirelessDevIE->supportedVhtMCS[SWL_COM_DIR_RECEIVE];
    swl_mcs_supMCS_adv_t* suppVhtTxMcs = &pWirelessDevIE->supportedVhtMCS[SWL_COM_DIR_TRANSMIT];
    memset(suppVhtRxMcs, 0, sizeof(swl_mcs_supMCS_adv_t));
    memset(suppVhtTxMcs, 0, sizeof(swl_mcs_supMCS_adv_t));

    for(uint32_t nss = 0; nss < SWL_80211_MCS_VHT_NSS_SET_LEN; nss++) {
        uint16_t rxMcsCode = SWL_80211_HE_CAP_MCS_NSS_GET_MCS(nss, vhtCap->rxMcsMap);
        uint16_t txMcsCode = SWL_80211_HE_CAP_MCS_NSS_GET_MCS(nss, vhtCap->txMcsMap);
        // SWL_80211_HE_CAP_MAX_MCS_NONE - end of record
        if((rxMcsCode == SWL_80211_HE_CAP_MAX_MCS_NONE) || (txMcsCode == SWL_80211_HE_CAP_MAX_MCS_NONE)) {
            break;
        }
        if(rxMcsCode < SWL_80211_HE_CAP_MAX_MCS_NONE) {
            suppVhtRxMcs->nssMcsNbr[nss] = rxMcsCode + 7;
            suppVhtRxMcs->nssNbr++;
        }
        if(txMcsCode < SWL_80211_HE_CAP_MAX_MCS_NONE) {
            suppVhtTxMcs->nssMcsNbr[nss] = txMcsCode + 7;
            suppVhtTxMcs->nssNbr++;
        }
    }

    SAH_TRACEZ_INFO(ME, "Parsing VHT Capabilities from %04x to %x", vhtCap->vhtCapInfo, pWirelessDevIE->vhtCapabilities);
}

static void s_saveFreqBand(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_freqBandExt_e freqBandExt) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERTS_TRUE(swl_chanspec_isValidBandExt(freqBandExt), , ME, "Invalid");
    pWirelessDevIE->operChanInfo.band = freqBandExt;
    pWirelessDevIE->freqCapabilities |= SWL_BIT_SHIFT(freqBandExt);
    pWirelessDevIE->operatingStandards |= SWL_BIT_SHIFT(swl_mcs_radStdFromMcsStd(SWL_MCS_STANDARD_LEGACY, freqBandExt));
}

static void s_parseHtOp(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_N;
    swl_80211_htOpIE_t* htOp = (swl_80211_htOpIE_t*) frm;
    ASSERTS_NOT_NULL(htOp, , ME, "NULL");
    pWirelessDevIE->operChanInfo.channel = htOp->primChan;
    swl_freqBandExt_e freqBandExt = swl_chanspec_freqBandExtFromBaseChannel(pWirelessDevIE->operChanInfo.channel);
    s_saveFreqBand(pWirelessDevIE, freqBandExt);

    swl_bandwidth_e bandwidth = pWirelessDevIE->operChanInfo.bandwidth;
    if((htOp->secChanOffset == SWL_80211_HT_OPER_SEC_CHAN_NONE) ||
       (htOp->staChanWidth == SWL_80211_HT_OPER_STA_CHAN_WIDTH_20_ONLY)) {
        bandwidth = SWL_BW_20MHZ;
    } else if((htOp->staChanWidth == SWL_80211_HT_OPER_STA_CHAN_WIDTH_ANY) &&
              ((htOp->secChanOffset == SWL_80211_HT_OPER_SEC_CHAN_ABOVE) ||
               (htOp->secChanOffset == SWL_80211_HT_OPER_SEC_CHAN_BELOW))) {
        ASSERTS_EQUALS(pWirelessDevIE->operChanInfo.bandwidth, SWL_BW_AUTO, , ME, "bw already filled");
        bandwidth = SWL_BW_40MHZ;
    }
    pWirelessDevIE->operChanInfo.bandwidth = SWL_MAX(bandwidth, pWirelessDevIE->operChanInfo.bandwidth);
}

static void s_parseVhtOp(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_AC;
    swl_80211_vhtOpIE_t* vhtOp = (swl_80211_vhtOpIE_t*) frm;
    ASSERTS_NOT_NULL(vhtOp, , ME, "NULL");
    swl_bandwidth_e bandwidth = pWirelessDevIE->operChanInfo.bandwidth;
    if(vhtOp->chanWidth == SWL_80211_VHT_OPER_CHAN_WIDTH_20_40) {
        if(!vhtOp->chanCenterSeg0) {
            bandwidth = SWL_BW_20MHZ;
        } else {
            bandwidth = SWL_BW_40MHZ;
        }
    } else if(vhtOp->chanWidth == SWL_80211_VHT_OPER_CHAN_WIDTH_80_160_8080) {
        if(!vhtOp->chanCenterSeg1) {
            bandwidth = SWL_BW_80MHZ;
        } else {
            bandwidth = SWL_BW_160MHZ;
        }
    } else if(vhtOp->chanWidth == SWL_80211_VHT_OPER_CHAN_WIDTH_160) {
        bandwidth = SWL_BW_160MHZ;
    }
    pWirelessDevIE->operChanInfo.bandwidth = SWL_MAX(bandwidth, pWirelessDevIE->operChanInfo.bandwidth);
}

static void s_parseExtHeOp(swl_wirelessDevice_infoElements_t* pWirelessDevIE _UNUSED, swl_80211_elIdExt_ne elIdExt _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    swl_80211_heOpIE_t* heopcap = (swl_80211_heOpIE_t*) &frm[1];
    ASSERTS_NOT_NULL(heopcap, , ME, "NULL");
}

static void s_setHeMcs(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_mcs_supMCS_adv_t* mcsList, swl_80211_heCapIE_t* swlHeCaps, uint8_t index) {
    ASSERTS_NOT_NULL(swlHeCaps, , ME, "NULL");
    ASSERTS_NOT_NULL(mcsList, , ME, "NULL");

    swl_mcs_t maxmcs;
    memset(&maxmcs, 0, sizeof(swl_mcs_t));
    maxmcs.standard = SWL_MCS_STANDARD_HE;
    maxmcs.bandwidth = (index == SWL_80211_HECAP_MCS_LT80_ELM) ? SWL_BW_80MHZ : SWL_BW_160MHZ;
    maxmcs.mcsIndex = SWL_MCS_MAX_HE_MCS;
    maxmcs.guardInterval = SWL_SGI_800;

    maxmcs.numberOfSpatialStream = s_getNss(swlHeCaps->mcsCaps[index].rxMcsMap);
    pWirelessDevIE->maxRxSpatialStreamsSupported = maxmcs.numberOfSpatialStream;
    pWirelessDevIE->maxDownlinkRateSupported = swl_mcs_toRate(&maxmcs);

    maxmcs.numberOfSpatialStream = s_getNss(swlHeCaps->mcsCaps[index].txMcsMap);
    pWirelessDevIE->maxTxSpatialStreamsSupported = maxmcs.numberOfSpatialStream;
    pWirelessDevIE->maxUplinkRateSupported = swl_mcs_toRate(&maxmcs);

    memset(&mcsList[SWL_COM_DIR_RECEIVE], 0, sizeof(swl_mcs_supMCS_adv_t));
    memset(&mcsList[SWL_COM_DIR_TRANSMIT], 0, sizeof(swl_mcs_supMCS_adv_t));

    // Max 8 (SWL_80211_MCS_VHT_NSS_SET_LEN) nss are possible - the same as in VHT
    for(uint32_t nss = 0; nss < SWL_80211_MCS_VHT_NSS_SET_LEN; nss++) {
        uint16_t rxMcsCode = SWL_80211_HE_CAP_MCS_NSS_GET_MCS(nss, swlHeCaps->mcsCaps[index].rxMcsMap);
        uint16_t txMcsCode = SWL_80211_HE_CAP_MCS_NSS_GET_MCS(nss, swlHeCaps->mcsCaps[index].txMcsMap);
        // SWL_80211_HE_CAP_MAX_MCS_NONE - in either sequence means end of record
        if((rxMcsCode == SWL_80211_HE_CAP_MAX_MCS_NONE) || (txMcsCode == SWL_80211_HE_CAP_MAX_MCS_NONE)) {
            break;
        }
        if(rxMcsCode < SWL_80211_HE_CAP_MAX_MCS_NONE) {
            mcsList[SWL_COM_DIR_RECEIVE].nssMcsNbr[nss] = 7 + 2 * rxMcsCode;
            mcsList[SWL_COM_DIR_RECEIVE].nssNbr++;
        }
        if(txMcsCode < SWL_80211_HE_CAP_MAX_MCS_NONE) {
            mcsList[SWL_COM_DIR_TRANSMIT].nssMcsNbr[nss] = 7 + 2 * txMcsCode;
            mcsList[SWL_COM_DIR_TRANSMIT].nssNbr++;
        }
    }
}

static void s_parseExtHeCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_80211_elIdExt_ne elIdExt _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_AX;
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    swl_80211_heCapIE_t hecap;
    swl_80211_parseHeCap(&hecap, frm);

    char phy_cap_hex[(SWL_80211_HECAP_PHY_CAP_INFO_SIZE * 2) + 1] = {'\0'};
    for(uint8_t i = 0; i < SWL_80211_HECAP_PHY_CAP_INFO_SIZE; i++) {
        swl_str_catFormat(phy_cap_hex, sizeof(phy_cap_hex), "%02x", hecap.phyCap.cap[i]);
    }
    if(hecap.phyCap.cap[3] & 0x80) {
        pWirelessDevIE->heCapabilities |= M_SWL_STACAP_HE_SU_BFR;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HE_SU_BFR cap");
    }
    if(hecap.phyCap.cap[4] & 0x01) {
        pWirelessDevIE->heCapabilities |= M_SWL_STACAP_HE_SU_AND_MU_BFE;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HE_SU_AND_MU_BFE cap");
    }
    if((hecap.phyCap.cap[4] & 0x02) >> 1) {
        pWirelessDevIE->heCapabilities |= M_SWL_STACAP_HE_MU_BFR;
        SAH_TRACEZ_INFO(ME, "Adding M_SWL_STACAP_HE_MU_BFR cap");
    }

    if(SWL_BIT_IS_SET(hecap.phyCap.cap[0], 2)) {
        s_setHeMcs(pWirelessDevIE, pWirelessDevIE->supportedHeMCS, &hecap, SWL_80211_HECAP_MCS_LT80_ELM);
    }
    if(SWL_BIT_IS_SET(hecap.phyCap.cap[0], 3)) {
        s_setHeMcs(pWirelessDevIE, pWirelessDevIE->supportedHe160MCS, &hecap, SWL_80211_HECAP_MCS_160_ELM);
    }
    if(SWL_BIT_IS_SET(hecap.phyCap.cap[0], 4)) {
        s_setHeMcs(pWirelessDevIE, pWirelessDevIE->supportedHe80x80MCS, &hecap, SWL_80211_HECAP_MCS_80P80_ELM);
    }

    SAH_TRACEZ_INFO(ME, "Parsing HE Capabilities from %s to %x", phy_cap_hex, pWirelessDevIE->heCapabilities);
}

static void s_parseExtEhtOp(swl_wirelessDevice_infoElements_t* pWirelessDevIE _UNUSED,
                            swl_80211_elIdExt_ne elIdExt,
                            uint8_t len _UNUSED, swl_bit8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERT_NOT_NULL(frm, , ME, "frame is null");
    ASSERT_EQUALS(elIdExt, SWL_80211_EL_IDEXT_EHT_OPER, , ME, "not matching ExtTag(%u/%u)", elIdExt, SWL_80211_EL_IDEXT_EHT_OPER);

    SAH_TRACEZ_INFO(ME, "Parsed EHT Operation");
}

static void s_ehtMlEmlCapabilitiesParser(uint8_t* data, size_t len, void* priv) {
    ASSERTS_NOT_NULL(data, , ME, "no data");
    ASSERT_FALSE(len < sizeof(swl_80211_ehtEmlCapabilitiesIE_t), , ME, "not enough len %zu/%zu", len, sizeof(swl_80211_ehtEmlCapabilitiesIE_t));
    swl_80211_ehtEmlCapabilitiesIE_t* emlCaps = (swl_80211_ehtEmlCapabilitiesIE_t*) data;
    swl_wirelessDevice_infoElements_t* pWirelessDevIE = priv;
    if(emlCaps->emlsrSupport != 0) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtEmlCapabilities, SWL_STACAP_EHT_EML_EMLSR);
    }
    if(emlCaps->emlmrSupport != 0) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtEmlCapabilities, SWL_STACAP_EHT_EML_EMLMR);
    }
}

static void s_ehtMlMldAddressParser(uint8_t* data, size_t len, void* priv) {
    ASSERTS_NOT_NULL(data, , ME, "no data");
    ASSERTS_FALSE(len < sizeof(swl_macBin_t), , ME, "not enough len %zu/%zu", len, sizeof(swl_macBin_t));
    swl_macBin_t* mldMac = (swl_macBin_t*) data;
    swl_wirelessDevice_infoElements_t* pWirelessDevIE = priv;
    memcpy(&pWirelessDevIE->ehtMldMacAddress, mldMac, sizeof(swl_macBin_t));
}

typedef struct {
    bool present;
    size_t len;
    void (* parser)(uint8_t* data, size_t len, void* priv);
    void* priv;
} ehtVariantInfoParserCtx_t;

static uint8_t* s_parseEhtMlBasic(swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE) {
    ehtVariantInfoParserCtx_t commonInfoParser[] = {
        {ml->mlCtrl.basic.linkIdInfo, sizeof(uint8_t), NULL, NULL},
        {ml->mlCtrl.basic.bssParametersChangeCount, sizeof(uint8_t), NULL, NULL},
        {ml->mlCtrl.basic.mediumSynchronizationDelayInformation, sizeof(uint16_t), NULL, NULL},
        {ml->mlCtrl.basic.emlCapabilities, sizeof(uint16_t), &s_ehtMlEmlCapabilitiesParser, pWirelessDevIE},
        {ml->mlCtrl.basic.mldCapabilitiesAndOperations, sizeof(uint16_t), NULL, NULL},
        {ml->mlCtrl.basic.apMldId, sizeof(uint8_t), NULL, NULL},
        {ml->mlCtrl.basic.extendedMLDCapabilitiesAndOperations, sizeof(uint16_t), NULL, NULL},
    };
    uint8_t* data = ml->mlCommonInfo.basic.data;
    size_t len = ml->mlCommonInfo.basic.length - 1;

    pWirelessDevIE->ehtMldMacAddress = ml->mlCommonInfo.basic.mldMacAddress;
    len -= sizeof(swl_macBin_t);

    for(uint8_t i = 0; i < SWL_ARRAY_SIZE(commonInfoParser); i++) {
        if(commonInfoParser[i].present) {
            SWL_CALL(commonInfoParser[i].parser, data, len, pWirelessDevIE);
            data += commonInfoParser[i].len;
            len -= commonInfoParser[i].len;
        }
    }

    return data;
}

static uint8_t* s_parseEhtMlProbe(swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE) {
    /*TODO*/
    _UNUSED_(ml);
    _UNUSED_(pWirelessDevIE);
    return NULL;
}

static uint8_t* s_parseEhtMlReconf(swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE) {
    ehtVariantInfoParserCtx_t commonInfoParser[] = {
        {ml->mlCtrl.reconf.mldMacAddress, sizeof(swl_macBin_t), &s_ehtMlMldAddressParser, pWirelessDevIE},
        {ml->mlCtrl.reconf.emlCapabilities, sizeof(uint16_t), &s_ehtMlEmlCapabilitiesParser, pWirelessDevIE},
        {ml->mlCtrl.reconf.mldCapabilitiesAndOperations, sizeof(uint16_t), NULL, NULL},
    };

    uint8_t* data = ml->mlCommonInfo.reconf.data;
    size_t len = ml->mlCommonInfo.length;
    for(uint8_t i = 0; i < SWL_ARRAY_SIZE(commonInfoParser); i++) {
        if(commonInfoParser[i].present) {
            SWL_CALL(commonInfoParser[i].parser, data, len, pWirelessDevIE);
            data += commonInfoParser[i].len;
            len -= commonInfoParser[i].len;
        }
    }
    return data;
}

static uint8_t* s_parseEhtMlTdls(swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE) {
    pWirelessDevIE->ehtMldMacAddress = ml->mlCommonInfo.tdls.mldMacAddress;
    return (uint8_t*) ml->mlCommonInfo.tdls.linkInfo;
}

static uint8_t* s_parseEhtMlEpcs(swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE) {
    pWirelessDevIE->ehtMldMacAddress = ml->mlCommonInfo.epcs.mldMacAddress;
    return (uint8_t*) ml->mlCommonInfo.epcs.linkInfo;
}

static void s_ehtMlLinkAddressParser(uint8_t* data, size_t len, void* mac _UNUSED) {
    ASSERTS_NOT_NULL(data, , ME, "no data");
    ASSERTS_FALSE(len < sizeof(swl_macBin_t), , ME, "not enough len %zu/%zu", len, sizeof(swl_macBin_t));
    swl_macBin_t* linkMac = (swl_macBin_t*) data;
    memcpy((swl_macBin_t*) mac, linkMac, sizeof(swl_macBin_t));
}

static void s_parseEhtMlLinkInfo(swl_wirelessDevice_infoElements_t* pWirelessDevIE,
                                 swl_80211_ehtMlLinkInfoIE_t* pLinkInfo, uint8_t len) {
    ASSERTS_NOT_NULL(pLinkInfo, , ME, "");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "");
    ASSERTS_TRUE(len > 0, , ME, "no enough data");
    ASSERTS_TRUE(pLinkInfo->length > 0, , ME, "no enough data");
    ASSERTS_NOT_EQUALS(pLinkInfo->length, 255, , ME, "Fragmented Link-Info not supported");

    ASSERTS_EQUALS(pLinkInfo->subElemId, 0, , ME, "only STA-Profile is supported");
    swl_80211_ehtMlPerStaProfileIE_t* pStaProfile = (swl_80211_ehtMlPerStaProfileIE_t*) pLinkInfo->data;

    W_SWL_BIT_SET(pWirelessDevIE->ehtLinksMask, pStaProfile->staCtrl.linkId);
    ehtVariantInfoParserCtx_t staInfoParser[] = {
        {pStaProfile->staCtrl.staMacAddress, sizeof(swl_macBin_t), &s_ehtMlLinkAddressParser,
            &pWirelessDevIE->ehtLinksMacAddress[pStaProfile->staCtrl.linkId]},
        {pStaProfile->staCtrl.beaconInterval, sizeof(uint16_t), NULL, NULL},
        {pStaProfile->staCtrl.tsfOffset, sizeof(uint64_t), NULL, NULL},
        {pStaProfile->staCtrl.dtimInfo, sizeof(uint16_t), NULL, NULL},
        {pStaProfile->staCtrl.nstrBitmap, sizeof(uint16_t), NULL, NULL},
        {pStaProfile->staCtrl.bssParameterChangeCount, sizeof(uint8_t), NULL, NULL},
    };

    uint8_t* data = pStaProfile->staInfo.data;
    size_t staInfolen = pStaProfile->staInfo.length;
    for(uint8_t i = 0; i < SWL_ARRAY_SIZE(staInfoParser); i++) {
        if(staInfoParser[i].present) {
            SWL_CALL(staInfoParser[i].parser, data, staInfolen, staInfoParser[i].priv);
            data += staInfoParser[i].len;
            staInfolen -= staInfoParser[i].len;
        }
    }

    s_parseEhtMlLinkInfo(pWirelessDevIE, (swl_80211_ehtMlLinkInfoIE_t*) (pLinkInfo->data + pLinkInfo->length),
                         len > pLinkInfo->length ? len - pLinkInfo->length : 0);
}

SWL_TABLE(tyepParseEhtMlTable,
          ARR(uint8_t type; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR({SWL_80211_EHT_MLCTRL_BASIC_TYPE, (void*) s_parseEhtMlBasic},
              {SWL_80211_EHT_MLCTRL_PROBE_TYPE, (void*) s_parseEhtMlProbe},
              {SWL_80211_EHT_MLCTRL_RECONF_TYPE, (void*) s_parseEhtMlReconf},
              {SWL_80211_EHT_MLCTRL_TDLS_TYPE, (void*) s_parseEhtMlTdls},
              {SWL_80211_EHT_MLCTRL_EPCS_TYPE, (void*) s_parseEhtMlEpcs}, )
          );
typedef uint8_t* (* typeParseEhtMlFun_f) (swl_80211_ehtMlIE_t* ml, swl_wirelessDevice_infoElements_t* pWirelessDevIE);

static void s_parseExtEhtMl(swl_wirelessDevice_infoElements_t* pWirelessDevIE,
                            swl_80211_elIdExt_ne elIdExt,
                            uint8_t len _UNUSED, swl_bit8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERT_NOT_NULL(frm, , ME, "frame is null");
    ASSERT_EQUALS(elIdExt, SWL_80211_EL_IDEXT_EHT_ML, , ME, "not matching ExtTag(%u/%u)", elIdExt, SWL_80211_EL_IDEXT_EHT_ML);

    swl_80211_ehtMlIE_t* ml = (swl_80211_ehtMlIE_t*) frm;

    uint8_t* data = NULL;
    uint8_t type = ml->mlCtrl.type;
    void** func = (void**) swl_table_getMatchingValue(&tyepParseEhtMlTable, 1, 0,
                                                      &type);
    if(func != NULL) {
        typeParseEhtMlFun_f myFun = (typeParseEhtMlFun_f) (*func);
        data = myFun(ml, pWirelessDevIE);
    } else {
        SAH_TRACEZ_INFO(ME, "Unsupported Multi-Link type %d", ml->mlCtrl.type);
    }

    s_parseEhtMlLinkInfo(pWirelessDevIE, (swl_80211_ehtMlLinkInfoIE_t*) data,
                         len - (data - frm));

    SAH_TRACEZ_INFO(ME, "Parsed EHT Multi-Link");
}

static void s_parseExtEhtCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE,
                             swl_80211_elIdExt_ne elIdExt,
                             uint8_t len, swl_bit8_t* frm) {
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERT_NOT_NULL(frm, , ME, "frame is null");
    ASSERT_EQUALS(elIdExt, SWL_80211_EL_IDEXT_EHT_CAP, , ME, "not matching ExtTag(%u/%u)", elIdExt, SWL_80211_EL_IDEXT_EHT_CAP);
    ASSERT_FALSE(len < sizeof(swl_80211_ehtCapIE_t), , ME, "not supported length(%u/%zu)", len, sizeof(swl_80211_ehtCapIE_t));

    swl_80211_ehtCapIE_t* ehtcap = (swl_80211_ehtCapIE_t*) frm;
    ASSERT_EQUALS(ehtcap->tag, elIdExt, , ME, "not matching ExtTag in frame(%u/%u)", ehtcap->tag, elIdExt);

    pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_BE;

    if(ehtcap->phyCap.support_320mhz) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_320MHZ_6GHZ);
    }
    if(ehtcap->phyCap.su_beamformer) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_SU_BEAMFORMER);
    }
    if(ehtcap->phyCap.su_beamformee) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_SU_BEAMFORMEE);
    }
    if(ehtcap->phyCap.mu_beamformer_80mhz) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_MU_BEAMFORMER_80MHZ);
    }
    if(ehtcap->phyCap.mu_beamformer_160mhz) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_MU_BEAMFORMER_160MHZ);
    }
    if(ehtcap->phyCap.mu_beamformer_320mhz) {
        W_SWL_BIT_SET(pWirelessDevIE->ehtCapabilities, SWL_STACAP_EHT_MU_BEAMFORMER_320MHZ);
    }

    char ehtCapHex[(sizeof(swl_80211_ehtCapIE_t) * 2) + 1] = {'\0'};
    for(uint8_t i = 0; i < sizeof(swl_80211_ehtCapIE_t); i++) {
        swl_str_catFormat(ehtCapHex, sizeof(ehtCapHex), "%02x", frm[i]);
    }
    SAH_TRACEZ_INFO(ME, "Parsed EHT Capabilities from 0x%s: 0x%x", ehtCapHex, pWirelessDevIE->ehtCapabilities);
}

SWL_TABLE(ieParseExtTable,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR({SWL_80211_EL_IDEXT_HE_OP, (void*) s_parseExtHeOp},
              {SWL_80211_EL_IDEXT_HE_CAP, (void*) s_parseExtHeCap},
              {SWL_80211_EL_IDEXT_EHT_OPER, (void*) s_parseExtEhtOp},
              {SWL_80211_EL_IDEXT_EHT_ML, (void*) s_parseExtEhtMl},
              {SWL_80211_EL_IDEXT_EHT_CAP, (void*) s_parseExtEhtCap}, )
          );
typedef void (* elementParseExtFun_f) (swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_80211_elIdExt_ne elId, uint8_t len, swl_bit8_t* frm);


static void s_parseExt(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERT_TRUE(len != 0, , ME, "ZERO LEN");

    swl_80211_elIdExt_ne elIdExt = frm[0];


    void** func = (void**) swl_table_getMatchingValue(&ieParseExtTable, 1, 0, &elIdExt);
    if(func != NULL) {
        elementParseExtFun_f myFun = (elementParseExtFun_f) (*func);
        myFun(pWirelessDevIE, elIdExt, len, frm);
    } else {
        SAH_TRACEZ_INFO(ME, "Unsupported element id ext %d", elIdExt);
    }

}

static void s_parseExtCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    SAH_TRACEZ_INFO(ME, "Parsing Extra Capabilities %p %d", frm, (int) frm[1]);
    if(frm[2] & 0x08) {
        pWirelessDevIE->capabilities |= M_SWL_STACAP_BTM;
    }
    if(frm[4] & 0x01) { // Just check if QoS MAP is set.
        pWirelessDevIE->capabilities |= M_SWL_STACAP_QOS_MAP;
    }
}

// TODO: see later how to modify
static void s_parseVendor(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    swl_80211_vendorIdEl_t* vendor = (swl_80211_vendorIdEl_t*) frm;
    ASSERTS_NOT_NULL(vendor, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "Parse associated STA IEs");
    swl_oui_list_t* vendorOUI = &pWirelessDevIE->vendorOUI;
    ASSERTS_NOT_NULL(vendorOUI, , ME, "NULL");

    swl_oui_t newOui;
    memcpy(newOui.ouiBytes, vendor->oui, SWL_OUI_BYTE_LEN);

    if(!swl_typeOui_arrayContains(vendorOUI->oui, vendorOUI->count, newOui)) {
        ASSERT_TRUE(vendorOUI->count < SWL_OUI_MAX_NUM, , ME, "Max number of OUI reached");
        memcpy(vendorOUI->oui[vendorOUI->count].ouiBytes, newOui.ouiBytes, SWL_OUI_BYTE_LEN);
        vendorOUI->count++;
    }

    SAH_TRACEZ_INFO(ME, "Parsing Vendor ID=%d OUI=%s", elId, swl_typeOui_toBuf32(newOui).buf);

    // Extract security mode
    uint32_t oui = SWL_OUI_GET(frm);
    if((len >= 4) && (oui == SWL_OUI_MS)) {

        SAH_TRACEZ_INFO(ME, "Vendor specific: Microsoft WPS, OUI type = %x", frm[3]);
        // OUI Type == 4 (WPS Supported)
        if(frm[3] == SWL_OUI_TYPE_WPS) {
            pWirelessDevIE->vendorCapabilities |= M_SWL_STACAP_VENDOR_MS_WPS;

            // Shift data pointer by 4 bytes (OUI + Type)
            frm += 4;
            len -= 4;

            /* The following Data is code as multiple TLV with:
             * Type:   2 bytes
             * Length: 2 bytes
             * Value:  x bytes
             */
            while(len >= 4 && (uint16_t) len >= (frm[3] | (frm[2] << 8))) {
                uint16_t taglen = (uint16_t) (frm[3] | (frm[2] << 8));
                uint8_t* tagData = (uint8_t*) (frm + 4);

                /* The tag type is on two bytes
                 * they all respect this format: 0x10xx (with xx the changing byte)
                 * So we juste need to test the second byte value
                 */
                switch(frm[1]) {
                case 0x53: // Selected Registrar Config Methods
                    SAH_TRACEZ_INFO(ME, "Selected Registrar Config Methods: (%x)", (tagData[1] | (tagData[0] << 8)));
                    pWirelessDevIE->WPS_ConfigMethodsEnabled = (tagData[1] | (tagData[0] << 8));
                    break;

                case 0x49: // Vendor Extension
                    SAH_TRACEZ_INFO(ME, "Vendor Extension");
                    uint8_t* vendorExtension = &tagData[0];
                    vendorExtension += 3;
                    if(*vendorExtension == SWL_80211_WFA_ELEMENT_VERSION2) {
                        vendorExtension += 3;
                    }
                    if(*vendorExtension == SWL_80211_WFA_ELEMENT_REQUEST_TO_ENROLL) {
                        vendorExtension += 2;
                        pWirelessDevIE->wpsRequestToEnroll = (*vendorExtension) ? true : false;
                    }
                    break;

                case 0x12: // PasswordID
                    SAH_TRACEZ_INFO(ME, "Password ID");
                    if(taglen == sizeof(uint16_t)) {
                        pWirelessDevIE->wpsPasswordID = (tagData[1] | (tagData[0] << 8));
                    }
                    break;

                case 0x21: // Manufacturer
                    SAH_TRACEZ_INFO(ME, "Manufacturer");
                    memset(&pWirelessDevIE->manufacturer, 0, sizeof(pWirelessDevIE->manufacturer));
                    if(taglen < sizeof(pWirelessDevIE->manufacturer)) {
                        memcpy(pWirelessDevIE->manufacturer, &tagData[0], taglen);
                    } else {
                        SAH_TRACEZ_INFO(ME, "Manufacturer too long %d", taglen);
                    }
                    break;

                case 0x23: // Model Name
                    SAH_TRACEZ_INFO(ME, "Model Name");
                    memset(&pWirelessDevIE->modelName, 0, sizeof(pWirelessDevIE->modelName));
                    if(taglen < sizeof(pWirelessDevIE->modelName)) {
                        memcpy(pWirelessDevIE->modelName, &tagData[0], taglen);
                    } else {
                        SAH_TRACEZ_INFO(ME, "Model Name too long %d", taglen);
                    }
                    break;

                case 0x24: // Model Number
                    SAH_TRACEZ_INFO(ME, "Model Number");
                    memset(&pWirelessDevIE->modelNumber, 0, sizeof(pWirelessDevIE->modelNumber));
                    if(taglen < sizeof(pWirelessDevIE->modelNumber)) {
                        memcpy(pWirelessDevIE->modelNumber, &tagData[0], taglen);
                    } else {
                        SAH_TRACEZ_INFO(ME, "Model Number too long %d", taglen);
                    }
                    break;

                default:
                    SAH_TRACEZ_INFO(ME, "Case not theated !");
                    break;
                }

                // Shift data pointer by 5 bytes
                frm += (taglen + 4);
                len -= (taglen + 4);
            }
        }
    } else if((len >= 4) && (oui == SWL_OUI_WFA)) {
        //OUI TYPE == 22 (MBO Supported)
        if(frm[3] == SWL_OUI_TYPE_MBO) {
            pWirelessDevIE->vendorCapabilities |= M_SWL_STACAP_VENDOR_WFA_MBO;
        }
    }
}


static void s_parseMobDom(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    swl_80211_mobDomEl_t* mdie = (swl_80211_mobDomEl_t*) frm;
    ASSERTS_NOT_NULL(mdie, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    if((mdie->cap & SWL_80211_MOB_DOM_CAP_FBSS_TRANS_DS) == SWL_80211_MOB_DOM_CAP_FBSS_TRANS_DS) {
        pWirelessDevIE->capabilities |= M_SWL_STACAP_FBT;
    }
}

static void s_parseRegClass(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    swl_opClassCountry_e locReg = pWirelessDevIE->operClassRegion;
    swl_opClassCountry_e altReg = swl_chanspec_regulatoryDomainToCountry(pWirelessDevIE->country);
    swl_opClassCountry_e selReg = locReg;
    for(uint8_t i = 0; i < len; i++) {
        if(!frm[i]) {
            ASSERTS_NOT_EQUALS(i, 0, , ME, "invalid current operClass, skip next entries");
            continue;
        }
        //try to identify and convert operClass from local to global
        swl_channel_t channel = (i == 0) ? pWirelessDevIE->operChanInfo.channel : 0;
        swl_chanspec_t chspec = SWL_CHANSPEC_EMPTY;
        const swl_operatingClassInfo_t* pOpcInfo = NULL;
        swl_operatingClassCountryInfo_t results[SWL_OP_CLASS_COUNTRY_MAX];
        uint32_t nResults = swl_chanspec_findAllOperClassCountryInfo(results, SWL_ARRAY_SIZE(results), frm[i], selReg, channel);
        if(nResults > 1) {
            for(uint32_t r = 0; r < nResults; r++) {
                if((results[r].countryRegion == locReg) || (results[r].countryRegion == altReg)) {
                    selReg = results[r].countryRegion;
                    pOpcInfo = results[r].operClassInfo;
                    break;
                }
            }
        } else if(nResults == 1) {
            selReg = results[0].countryRegion;
            pOpcInfo = results[0].operClassInfo;
        }
        if(pOpcInfo != NULL) {
            chspec = (swl_chanspec_t) SWL_CHANSPEC_NEW(channel, pOpcInfo->bandwidth, pOpcInfo->band);
        }
        if(chspec.band == SWL_FREQ_BAND_EXT_NONE) {
            ASSERTS_NOT_EQUALS(i, 0, , ME, "invalid current operClass, skip next entries");
            continue;
        }
        //0: current operating class
        //1..: alternate operating classes, including the current one
        pWirelessDevIE->freqCapabilities |= SWL_BIT_SHIFT(chspec.band);
        if(i == 0) {
            s_saveFreqBand(pWirelessDevIE, chspec.band);
            pWirelessDevIE->operChanInfo.bandwidth = chspec.bandwidth;
        }
        swl_operatingClass_t globOpclass = swl_chanspec_getGlobalOperClassfromLocal(frm[i], selReg);
        pWirelessDevIE->uniiBandsCapabilities |= swl_chanspec_operClassToUniiMask(globOpclass);
        SAH_TRACEZ_INFO(ME, "locOpc[%d]:%d chspec:%s locReg:%d altReg:%d selReg:%d globOpc:%d",
                        i, frm[i], swl_typeChanspecExt_toBuf32(chspec).buf,
                        locReg, altReg, selReg, globOpclass);
    }
}

static void s_parseSupRates(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    if(pWirelessDevIE->operChanInfo.band == SWL_FREQ_BAND_EXT_2_4GHZ) {
        for(uint8_t i = 0; i < len; i++) {
            uint8_t rate = frm[i] & 0x7f; // remove MSB which only indicates whether the rate is part of the BSSBasicRateSet
            switch(rate) {
            case M_SWL_80211_RATE_5_5MBPS:
            case M_SWL_80211_RATE_11MBPS:
                pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_B;
                break;
            case M_SWL_80211_RATE_6MBPS:
            case M_SWL_80211_RATE_9MBPS:
            case M_SWL_80211_RATE_12MBPS:
            case M_SWL_80211_RATE_18MBPS:
            case M_SWL_80211_RATE_24MBPS:
            case M_SWL_80211_RATE_36MBPS:
            case M_SWL_80211_RATE_48MBPS:
            case M_SWL_80211_RATE_54MBPS:
                pWirelessDevIE->operatingStandards |= M_SWL_RADSTD_G;
                break;
            default:
                break;
            }
        }
    }
}

static void s_parseSupChan(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    swl_chanspec_t chanspec = SWL_CHANSPEC_EMPTY;
    if(pWirelessDevIE->freqCapabilities) {
        chanspec.band = pWirelessDevIE->operChanInfo.band;
    }

    uint8_t nrTuples = len / 2;
    //start from last tuple (easier deduction of freq band)
    for(int i = (nrTuples - 1); i >= 0; i--) {
        //take channel byte and skip subBand id byte
        chanspec.channel = frm[i * 2];
        /*
         * (80211 sec 9.4.2.17 Supported Channels element)
         * The Supported Channels element is included in Association Request frame
         *
         * so assume current band on which pairing occurred
         * so try to deduce band from channel number (when possible).
         * Otherwise, skip the channel tuple
         */
        if(!swl_chanspec_isValidBandExt(chanspec.band)) {
            chanspec.band = swl_chanspec_freqBandExtFromBaseChannel(chanspec.channel);
            if(!swl_chanspec_isValidBandExt(chanspec.band)) {
                continue;
            }
            s_saveFreqBand(pWirelessDevIE, chanspec.band);
        }
        pWirelessDevIE->uniiBandsCapabilities |= swl_chanspec_toUniiMask(&chanspec);
    }
}

static void s_parseSSID(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_EL_ID_SSID");
    if(len > SWL_80211_SSID_STR_LEN - 1) {
        SAH_TRACEZ_ERROR(ME, "SSID length incorrect");
        return;
    }
    memcpy(pWirelessDevIE->ssid, frm, len);
    pWirelessDevIE->ssidLen = len;
}

static void s_parseCountry(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERTI_TRUE(len >= 3, , ME, "not enough data");
    // 9.4.2.8 Country element / dot11CountryString (see Annex C). /
    /*
     * If it is a country, the first two octets of this string
     * is the two character country code as described in document ISO 3166-1
     */
    memcpy(pWirelessDevIE->country, &frm[0], 2);
    /*
     * The third octet is one of the following:
     * <SPACE> (all env), 'I' (indoor), 'O' (outdoor), 'X' (non-country), <hex> (operating class table number)
     */
    swl_bit8_t regionId = frm[2];
    if(regionId < SWL_OP_CLASS_COUNTRY_MAX) {
        pWirelessDevIE->operClassRegion = (swl_opClassCountry_e) regionId;
    } else if(!swl_str_isEmpty(pWirelessDevIE->country) && !swl_str_matches(pWirelessDevIE->country, "XX") && (regionId != 'X')) {
        pWirelessDevIE->operClassRegion = swl_chanspec_regulatoryDomainToCountry(pWirelessDevIE->country);
    }
    SAH_TRACEZ_INFO(ME, ">> Country(%s), regulRegion(%d)",
                    pWirelessDevIE->country, pWirelessDevIE->operClassRegion);
}

static void s_parseRsn(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "Parsing RSN IE len %d", len);
    ASSERT_TRUE(len >= 8, , ME, "RSN too short");
    ASSERT_TRUE(frm[0] == 1, , ME, "RSN version not supported");
    /* skip len + version */
    frm += 2;
    len -= 2;

    swl_80211_cipher_m cipherSuiteParams = SWL_80211_USE_GRP_CIPHER_SUITE;
    swl_80211_akm_m akmSuiteParams = SWL_80211_AKM_SUITES_RESERVED;
    /* groupDataCipher == (00 0F AC xx) with xx the only changing byte.
     * See Table 9-149 in nl80211 standard for mor information
     */
    uint8_t groupDataCipher = frm[3];
    SAH_TRACEZ_INFO(ME, "groupDataCipher 00 0F AC %X", frm[3]);
    swl_80211_getDataCipherSuite(groupDataCipher, &cipherSuiteParams);
    frm += 4;
    len -= 4;

    /* skip pairwise cipher suite */
    uint8_t n_uciphers = *frm;
    uint32_t size_ciphers = n_uciphers * 4 + 2;
    ASSERT_TRUE(len >= size_ciphers, , ME, "ucast cipher data too short");

    /* skip pairwise cipher counter */
    frm += 2;
    len -= 2;

    // Get Pairwise Cipher suite list (4*counter bytes)
    for(int i = 0; i < n_uciphers; i++) {
        uint8_t pairwiseCipher = frm[3 + (i * 4)];
        swl_80211_getDataCipherSuite(pairwiseCipher, &cipherSuiteParams);
    }
    frm += (size_ciphers - 2);
    len -= (size_ciphers - 2);

    /* skip authentication key mgt suite*/
    uint8_t n_keymgmt = *frm;
    uint32_t size_keymgmt = n_keymgmt * 4 + 2;
    ASSERT_TRUE(len >= size_keymgmt, , ME, "keygmt data too short");

    /* skip AKM Suite counter */
    frm += 2;
    len -= 2;

    /* Get AKM Suitelist (4*counter bytes) */
    for(int i = 0; i < n_keymgmt; i++) {
        uint8_t akmSuiteList = frm[3 + (i * 4)];
        swl_80211_getDataAKMsuite(akmSuiteList, &akmSuiteParams);
    }
    frm += (size_keymgmt - 2);
    len -= (size_keymgmt - 2);

    /* rsn capabilities */
    ASSERT_TRUE(len >= 2, , ME, "RSN capabilities  data too short %d", len);
    SAH_TRACEZ_INFO(ME, "BCM RSN Capabilites (0x%02x): \n", frm[0]);
    /* PMF capability 1... ....*/
    if(SWL_BIT_IS_SET(frm[0], 7)) {
        pWirelessDevIE->capabilities |= M_SWL_STACAP_PMF;
    }

    /* Get Security mods */
    swl_security_getMode(&pWirelessDevIE->secModeEnabled, &cipherSuiteParams, &akmSuiteParams);

}

static void s_parseRmCap(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len _UNUSED, uint8_t* frm) {
    ASSERTS_NOT_NULL(frm, , ME, "NULL");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");
    ASSERT_FALSE(len < SWL_80211_EL_RM_ENAB_CAP_LEN, , ME, "short IE payload size");

    /* RM Enable Capabilities bits */
    swl_staCapRrmCap_e capBit[] = {
        SWL_STACAP_RRM_LINK_ME,                   /* bit0 */
        SWL_STACAP_RRM_NEIGHBOR_RE,               /* bit1 */
        SWL_STACAP_RRM_PRALLAL_ME,                /* bit2 */
        SWL_STACAP_RRM_REPEATED_ME,               /* bit3 */
        SWL_STACAP_RRM_BEACON_PASSIVE_ME,         /* bit4 */
        SWL_STACAP_RRM_BEACON_ACTIVE_ME,          /* bit5 */
        SWL_STACAP_RRM_BEACON_TABLE_ME,           /* bit6 */
        SWL_STACAP_RRM_BEACON_ME_RE_COND,         /* bit7 */
        SWL_STACAP_RRM_FRAME_ME,                  /* bit8 */
        SWL_STACAP_RRM_CHAN_LOAD_ME,              /* bit9 */
        SWL_STACAP_RRM_NOISE_HISTOGRAM_ME,        /* bit10 */
        SWL_STACAP_RRM_STATS_ME,                  /* bit11 */
        SWL_STACAP_RRM_LCI_ME,                    /* bit12 */
        SWL_STACAP_RRM_LCI_AZIMUTH_ME,            /* bit13 */
        SWL_STACAP_RRM_TRANS_SC_ME,               /* bit14 */
        SWL_STACAP_RRM_TRIG_SC_ME,                /* bit15 */
        SWL_STACAP_RRM_AP_CHAN_RE,                /* bit16 */
        0,                                        /* bit17 */
        /* bit 18-20 On-Channel Max Duration */
        0,                                        /* bit18 */
        0,                                        /* bit19 */
        0,                                        /* bit20 */
        /* bit 21-23 Off-Channel Max Duration */
        0,                                        /* bit21 */
        0,                                        /* bit22 */
        0,                                        /* bit23 */
        /* POLT */
        0,                                        /* bit24 */
        0,                                        /* bit25 */
        0,                                        /* bit26 */
        0,                                        /* bit27 */
        SWL_STACAP_RRM_NEIG_RE_TSF_OFFSET,        /* bit28 */
        SWL_STACAP_RRM_RCPI_ME,                   /* bit29 */
        SWL_STACAP_RRM_RSNI_ME,                   /* bit30 */
        SWL_STACAP_RRM_BSS_AVG_ACCESS_DELAY,      /* bit31 */
        SWL_STACAP_RRM_BSS_AVAIL_ADMISS_CAPACITY, /* bit32 */
        SWL_STACAP_RRM_ANTENNA,                   /* bit33 */
        SWL_STACAP_RRM_FTM_RANGE_RE,              /* bit34 */
        SWL_STACAP_RRM_CIVIC_LOCATION_ME,         /* bit35 */
        SWL_STACAP_RRM_IDENT_LOCATION_ME,         /* bit36 (extension) */
        /* bit 37-39 reserved */
    };

    SAH_TRACEZ_INFO(ME, "Parsing RM Enable Capabilities %p", frm);
    swl_bit8_t* rmCapabilities = frm;

    /* Set detected capabilities */
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(capBit); i++) {
        if(SWL_BIT_IS_SET(rmCapabilities[i / 8], (i % 8))) {
            W_SWL_BIT_SET(pWirelessDevIE->rrmCapabilities, capBit[i]);
        }
    }

    /* On-Channel Max Duration, in Time Unit (TU) */
    pWirelessDevIE->rrmOnChannelMaxDuration = ((rmCapabilities[2] & 0x1c) >> 2);


    /* Off-Channel Max Duration in Time Unit (TU)
     * 11.10.4 Measurement duration @ IEEE Std 802.11-2020
     * If this attribute is 0, the STA does not support RM measurements on nonoperating channels */
    pWirelessDevIE->rrmOffChannelMaxDuration = ((rmCapabilities[2] & 0xe0) >> 5);

    if(pWirelessDevIE->rrmCapabilities != 0) {
        W_SWL_BIT_SET(pWirelessDevIE->capabilities, SWL_STACAP_RRM);
    }
}

static void s_parseBssLoad(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs _UNUSED, swl_80211_elId_ne elId _UNUSED, uint8_t len, uint8_t* frm) {

    swl_80211_bssLoadIE_t* bssLoad = (swl_80211_bssLoadIE_t*) (frm);

    ASSERTS_NOT_NULL(bssLoad, , ME, "NULL");
    ASSERTS_EQUALS(len, sizeof(*bssLoad), , ME, "invalid length");
    ASSERTS_NOT_NULL(pWirelessDevIE, , ME, "NULL");

    SAH_TRACEZ_INFO(ME, "SWL_80211_EL_ID_BSSLOAD");

    pWirelessDevIE->stationCount = bssLoad->stationCount;
    pWirelessDevIE->channelUtilization = bssLoad->channelUtilization;
    pWirelessDevIE->availAdmissionCapacity = bssLoad->availAdmissionCapacity;

    SAH_TRACEZ_INFO(ME, "Parsed BSS Load Element - Utilization: %u%%, Station Count: %u, Available Admission Capacity: %u",
                    pWirelessDevIE->channelUtilization, pWirelessDevIE->stationCount, pWirelessDevIE->availAdmissionCapacity);
}


SWL_TABLE(ieParseTable,
          ARR(uint8_t index; void* val; ),
          ARR(swl_type_uint8, swl_type_voidPtr),
          ARR({SWL_80211_EL_ID_SSID, (void*) s_parseSSID},
              {SWL_80211_EL_ID_COUNTRY, (void*) s_parseCountry},
              {SWL_80211_EL_ID_SUP_CHAN, (void*) s_parseSupChan},
              {SWL_80211_EL_ID_HT_CAP, (void*) s_parseHtCap},
              {SWL_80211_EL_ID_RSN, (void*) s_parseRsn},
              {SWL_80211_EL_ID_MOB_DOM, (void*) s_parseMobDom},
              {SWL_80211_EL_ID_SUP_OP_CLASS, (void*) s_parseRegClass},
              {SWL_80211_EL_ID_HT_OP, (void*) s_parseHtOp},
              {SWL_80211_EL_ID_RM_ENAB_CAP, (void*) s_parseRmCap},
              {SWL_80211_EL_ID_EXT_CAP, (void*) s_parseExtCap},
              {SWL_80211_EL_ID_VHT_CAP, (void*) s_parseVhtCap},
              {SWL_80211_EL_ID_VHT_OP, (void*) s_parseVhtOp},
              {SWL_80211_EL_ID_VENDOR_SPECIFIC, (void*) s_parseVendor},
              {SWL_80211_EL_ID_EXT, (void*) s_parseExt},
              {SWL_80211_EL_ID_SUP_RATES, (void*) s_parseSupRates},
              {SWL_80211_EL_ID_EXT_CAP_RATE_BSS_MEMB, (void*) s_parseSupRates},
              {SWL_80211_EL_ID_BSS_LOAD, (void*) s_parseBssLoad},
              )
          );
typedef void (* elementParseFun_f) (swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs, swl_80211_elId_ne elId, uint8_t len, uint8_t* frm);
typedef void (* customParseFun_f) (void* pUserData, swl_parsingArgs_t* pParsingArgs, swl_80211_elId_ne elId, uint8_t len, uint8_t* frm);


swl_rc_ne swl_80211_processCustomFrame(void* pUserData, swl_table_t* pCustomParseTable, swl_parsingArgs_t* pParsingArgs, uint8_t elementId, int8_t len, uint8_t* data) {
    ASSERT_NOT_NULL(pUserData, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_rc_ne rc = SWL_RC_OK;

    void** func = (void**) swl_table_getMatchingValue(pCustomParseTable, 1, 0, &elementId);
    if(func != NULL) {

        // They should be the same address since they are the same statically allocated variable
        if(pCustomParseTable == &ieParseTable) {
            // Use IE parse function
            elementParseFun_f myFun = (elementParseFun_f) (*func);
            myFun((swl_wirelessDevice_infoElements_t*) pUserData, pParsingArgs, elementId, len, data);
        } else {
            // Use custom (void pointer) parse function
            customParseFun_f myFun = (customParseFun_f) (*func);
            myFun(pUserData, pParsingArgs, elementId, len, data);
        }

    } else {
        SAH_TRACEZ_INFO(ME, "Unsupported element id %d", elementId);
        rc = SWL_RC_INVALID_PARAM;
    }

    return rc;
}

swl_rc_ne swl_80211_processInfoElementFrame(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs, uint8_t elementId, int8_t len, uint8_t* data) {
    ASSERT_NOT_NULL(pWirelessDevIE, SWL_RC_INVALID_PARAM, ME, "NULL");
    memset(pWirelessDevIE, 0, sizeof(*pWirelessDevIE));
    return swl_80211_processCustomFrame(pWirelessDevIE, &ieParseTable, pParsingArgs, elementId, len, data);
}

/*
 * @brief Parse 80211 Info Elements buffer (including TLVs) into user data structure with custom parsing table
 *
 * @param pWirelessDevIE: (out) pointer to device info to be filled
 * @param pCustomParseTable (in) A custom defined parsing table for custom handling of different IEs
 * @param pParsingArgs: (in) pointer to optional parsing argument, that may be used to initialize some result values
 * @param iesLen: (in) Information Elements buffer length
 * @param iesData: (in) Information Elements buffer data
 *
 * @return number of processed bytes of the input buffer (shall be equal to iesLen when all data has been processed)
 *         or negative SWL_RC error code when major error happens.
 *         It is up to application to consider error when parsing is partial (i.e returning < iesLen)
 */
ssize_t swl_80211_parseCustomInfoElementsBuffer(void* pUserData, swl_table_t* pCustomParseTable, swl_parsingArgs_t* pParsingArgs, size_t iesLen, uint8_t* iesData) {
    ASSERT_NOT_NULL(pUserData, 0, ME, "NULL");
    ASSERTI_TRUE(iesLen > 0, 0, ME, "No data");
    ASSERT_NOT_NULL(iesData, 0, ME, "NULL");

    size_t index = 0;
    swl_80211_elId_ne elementId;
    swl_bit8_t elementLen = 0;
    const swl_bit8_t* elementData = NULL;
    //assume all IEs are TLVs: so at least "Type" and "Length" byte fields
    while((index + 2) < iesLen) {
        elementId = iesData[index];
        index++;
        elementLen = iesData[index];
        index++;
        if((index + elementLen) > iesLen) {
            SAH_TRACEZ_WARNING(ME, "Too short: abort parsing IE(ID:%u,Len:%u) at index %zu (/%zu)",
                               elementId, elementLen, index, iesLen);
            break;
        }
        elementData = &iesData[index];
        swl_80211_processCustomFrame(pUserData, pCustomParseTable, pParsingArgs, elementId, elementLen, (swl_bit8_t*) elementData);
        index += elementLen;
    }

    return index;
}





/*
 * @brief Parse 80211 Info Elements buffer (including TLVs) into user data structure
 *
 * @param pWirelessDevIE: (out) pointer to device info to be filled
 * @param pParsingArgs: (in) pointer to optional parsing argument, that may be used to initialize some result values
 * @param iesLen: (in) Information Elements buffer length
 * @param iesData: (in) Information Elements buffer data
 *
 * @return number of processed bytes of the input buffer (shall be equal to iesLen when all data has been processed)
 *         or negative SWL_RC error code when major error happens.
 *         It is up to application to consider error when parsing is partial (i.e returning < iesLen)
 */
ssize_t swl_80211_parseInfoElementsBuffer(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs, size_t iesLen, uint8_t* iesData) {

    ASSERT_NOT_NULL(pWirelessDevIE, 0, ME, "NULL");
    memset(pWirelessDevIE, 0, sizeof(swl_wirelessDevice_infoElements_t));
    pWirelessDevIE->operChanInfo.band = SWL_FREQ_BAND_EXT_NONE;
    pWirelessDevIE->operClassRegion = SWL_OP_CLASS_COUNTRY_UNKNOWN;
    ASSERTI_TRUE(iesLen > 0, 0, ME, "No data");
    ASSERT_NOT_NULL(iesData, 0, ME, "NULL");

    if(pParsingArgs != NULL) {
        if(swl_channel_isInChanspec(&pParsingArgs->seenOnChanspec, pParsingArgs->seenOnChanspec.channel)) {
            pWirelessDevIE->operChanInfo.channel = pParsingArgs->seenOnChanspec.channel;
            s_saveFreqBand(pWirelessDevIE, pParsingArgs->seenOnChanspec.band);
        }
    }
    return swl_80211_parseCustomInfoElementsBuffer(pWirelessDevIE, &ieParseTable, pParsingArgs, iesLen, iesData);
}

swl_80211_mgmtFrame_t* swl_80211_getMgmtFrame(swl_bit8_t* frameData, size_t frameLen) {
    ASSERT_NOT_NULL(frameData, NULL, ME, "NULL");
    ASSERT_FALSE(frameLen < SWL_80211_MGMT_FRAME_HEADER_LEN, NULL, ME, "frame shorter that header size");
    swl_80211_mgmtFrame_t* frame = (swl_80211_mgmtFrame_t*) frameData;
    ASSERT_EQUALS(frame->fc.type, 0, NULL, ME, "not mgmt frame");
    return frame;
}

/*
 * @brief table of data offset in 80211 management frame sub types
 */
SWL_TABLE(frameDataInfoTable,
          ARR(uint8_t type; uint32_t dataOffset; bool hasIEs; ),
          ARR(swl_type_uint8, swl_type_uint32, swl_type_bool, ),
          ARR({SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST, offsetof(swl_80211_assocReqFrameBody_t, data), true},
              {SWL_80211_MGT_FRAME_TYPE_REASSOC_REQUEST, offsetof(swl_80211_reassocReqFrameBody_t, data), true},
              {SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST, offsetof(swl_80211_probeReqFrameBody_t, data), true},
              {SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE, offsetof(swl_80211_probeRespFrameBody_t, data), true},
              {SWL_80211_MGT_FRAME_TYPE_BEACON, offsetof(swl_80211_beaconFrameBody_t, data), true},
              {SWL_80211_MGT_FRAME_TYPE_ACTION, offsetof(swl_80211_actionFrame_t, data), false},
              {SWL_80211_MGT_FRAME_TYPE_DISASSOCIATION, offsetof(swl_80211_disassocFrameBody_t, data), false},
              {SWL_80211_MGT_FRAME_TYPE_DEAUTHENTICATION, offsetof(swl_80211_deauthFrameBody_t, data), false},
              )
          );
typedef frameDataInfoTableStruct_t frameDataInfoTableTupleType_type;

/*
 * @brief table of data offset in 80211 action frame sub types
 */
SWL_TABLE_FIXED(actionFrameDataInfoTable, frameDataInfoTableTupleType,
                ARR({SWL_80211_MGT_FRAME_ACTION_CATEGORY_WNM, offsetof(swl_80211_wnmActionFrameBody_t, data), false},
                    {SWL_80211_MGT_FRAME_ACTION_CATEGORY_VENDOR_SPECIFIC, offsetof(swl_80211_vendorIdEl_t, data), false},
                    {SWL_80211_MGT_FRAME_ACTION_CATEGORY_PUBLIC, offsetof(swl_80211_actionFrame_t, data), false},
                    )
                );

/*
 * @brief table of data offset in 80211 public action frame sub types
 */
SWL_TABLE_FIXED(publicActionFrameDataInfoTable, frameDataInfoTableTupleType,
                ARR({SWL_80211_MGT_FRAME_PUBLIC_ACTION_CATEGORY_VENDOR_SPECIFIC, offsetof(swl_80211_vendorIdEl_t, data), false},
                    )
                );
/*
 * @brief table of data offset in 80211 wnm frame sub types
 */
SWL_TABLE_FIXED(wnmFrameDataInfoTable, frameDataInfoTableTupleType,
                ARR({SWL_80211_WNM_ACTION_PURPOSE_BTM_QUERY, offsetof(swl_80211_wnmActionBTMQueryFrameBody_t, data), false},
                    {SWL_80211_WNM_ACTION_PURPOSE_BTM_RESPONSE, offsetof(swl_80211_wnmActionBTMResponseFrameBody_t, data), false},
                    )
                );

static swl_rc_ne s_skipSubFrameHead(swl_table_t* table, uint8_t type, ssize_t* pRemainingLen) {
    ASSERT_NOT_NULL(table, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(pRemainingLen, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_tuple_t* tuple = swl_table_getMatchingTuple(table, 0, &type);
    ASSERTS_NOT_NULL(tuple, SWL_RC_ERROR, ME, "unsupported type (0x%x) ", type);
    uint32_t* pDataOffset = (uint32_t*) swl_tupleType_getValue(&frameDataInfoTableTupleType, tuple, 1);
    ASSERT_NOT_NULL(pDataOffset, SWL_RC_ERROR, ME, "unknown data offset of frame type (0x%x) ", type);
    ASSERTW_TRUE(*pRemainingLen >= (ssize_t) *pDataOffset, SWL_RC_ERROR, ME, "remaining data of frame (0x%x) too short to be parsed", type);
    *pRemainingLen -= *pDataOffset; /* skip frame header */
    return SWL_RC_OK;
}

swl_bit8_t* swl_80211_getInfoElementsOfMgmtFrame(size_t* pIesLen, swl_bit8_t* frameData, size_t frameLen) {
    if(pIesLen != NULL) {
        *pIesLen = 0;
    }
    ssize_t remainingLen = frameLen;
    swl_80211_mgmtFrame_t* frame = swl_80211_getMgmtFrame(frameData, frameLen);
    swl_bit8_t* iesData = NULL;
    ASSERT_NOT_NULL(frame, iesData, ME, "invalid mgmt frame");
    remainingLen -= SWL_80211_MGMT_FRAME_HEADER_LEN;
    uint8_t mgtFrameType = swl_80211_mgtFrameType(&frame->fc);
    swl_rc_ne rc = s_skipSubFrameHead(&frameDataInfoTable, mgtFrameType, &remainingLen);
    ASSERTS_FALSE(rc < SWL_RC_OK, NULL, ME, "Fail to parse mgmt frame");
    bool* pHasIEs = (bool*) swl_table_getMatchingValue(&frameDataInfoTable, 2, 0, &mgtFrameType);
    ASSERT_TRUE((pHasIEs != NULL) && (*pHasIEs), iesData, ME, "mgmt frame type (0x%x) has no Tagged IEs", mgtFrameType);
    ASSERT_TRUE(remainingLen > 0, iesData, ME, "empty IEs in frame type (0x%x)", mgtFrameType);
    iesData = &frameData[frameLen - remainingLen];
    if(pIesLen != NULL) {
        *pIesLen = remainingLen;
    }
    return iesData;
}

swl_rc_ne swl_80211_handleMgmtFrame(void* userData, swl_bit8_t* frameBin, size_t frameLen, swl_80211_mgmtFrameHandlers_cb* handlers) {
    ASSERTS_NOT_NULL(handlers, SWL_RC_INVALID_PARAM, ME, "NULL");
    ssize_t remainingLen = frameLen;
    swl_80211_mgmtFrame_t* frame = swl_80211_getMgmtFrame((swl_bit8_t*) frameBin, frameLen);
    ASSERT_NOT_NULL(frame, SWL_RC_INVALID_PARAM, ME, "invalid mgmt frame");
    remainingLen -= SWL_80211_MGMT_FRAME_HEADER_LEN; /* skip sub frame header (mgmt header)*/
    SWL_CALL(handlers->fProcMgmtFrame, userData, frame, frameLen);
    uint8_t mgtFrameType = swl_80211_mgtFrameType(&frame->fc);
    swl_rc_ne rc = s_skipSubFrameHead(&frameDataInfoTable, mgtFrameType, &remainingLen);
    ASSERTS_FALSE(rc < SWL_RC_OK, rc, ME, "Fail to parse mgmt frame");
    switch(mgtFrameType) {
    case SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST: {
        SWL_CALL(handlers->fProcAssocReq, userData, frame, frameLen, (swl_80211_assocReqFrameBody_t*) frame->data, (size_t) remainingLen);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_REASSOC_REQUEST: {
        SWL_CALL(handlers->fProcReAssocReq, userData, frame, frameLen, (swl_80211_reassocReqFrameBody_t*) frame->data, (size_t) remainingLen);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST: {
        SWL_CALL(handlers->fProcProbeReq, userData, frame, frameLen, (swl_80211_probeReqFrameBody_t*) frame->data, (size_t) remainingLen);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE: {
        SWL_CALL(handlers->fProcProbeResp, userData, frame, frameLen, (swl_80211_probeRespFrameBody_t*) frame->data, (size_t) remainingLen);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_BEACON: {
        SWL_CALL(handlers->fProcBeacon, userData, frame, frameLen, (swl_80211_beaconFrameBody_t*) frame->data, (size_t) remainingLen);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_DISASSOCIATION: {
        SWL_CALL(handlers->fProcDisassoc, userData, frame, (swl_80211_disassocFrameBody_t*) frame->data);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_DEAUTHENTICATION: {
        SWL_CALL(handlers->fProcDeauth, userData, frame, (swl_80211_deauthFrameBody_t*) frame->data);
        break;
    }
    case SWL_80211_MGT_FRAME_TYPE_ACTION: {
        swl_80211_actionFrame_t* action = (swl_80211_actionFrame_t*) frame->data;
        SWL_CALL(handlers->fProcActionFrame, userData, frame, frameLen, action, (size_t) remainingLen);
        rc = s_skipSubFrameHead(&actionFrameDataInfoTable, action->category, &remainingLen);
        ASSERTS_FALSE(rc < SWL_RC_OK, rc, ME, "Fail to parse action frame");
        switch(action->category) {
        case SWL_80211_MGT_FRAME_ACTION_CATEGORY_WNM: {
            swl_80211_wnmActionFrameBody_t* wnm = (swl_80211_wnmActionFrameBody_t*) action->data;
            rc = s_skipSubFrameHead(&wnmFrameDataInfoTable, wnm->purpose, &remainingLen);
            ASSERTS_FALSE(rc < SWL_RC_OK, rc, ME, "Fail to parse wnm frame");
            switch(wnm->purpose) {
            case SWL_80211_WNM_ACTION_PURPOSE_BTM_QUERY: {
                SWL_CALL(handlers->fProcBtmQuery, userData, frame, frameLen, (swl_80211_wnmActionBTMQueryFrameBody_t*) wnm->data, (size_t) remainingLen);
                break;
            }
            case SWL_80211_WNM_ACTION_PURPOSE_BTM_RESPONSE: {
                SWL_CALL(handlers->fProcBtmResp, userData, frame, frameLen, (swl_80211_wnmActionBTMResponseFrameBody_t*) wnm->data, (size_t) remainingLen);
                break;
            }
            default: break;
            }
            break;
        }
        case SWL_80211_MGT_FRAME_ACTION_CATEGORY_VENDOR_SPECIFIC: {
            SWL_CALL(handlers->fProcActionVs, userData, frame, frameLen, (swl_80211_vendorIdEl_t*) action->data, (size_t) remainingLen);
            break;
        }
        case SWL_80211_MGT_FRAME_ACTION_CATEGORY_PUBLIC: {
            swl_80211_actionFrame_t* pubAction = (swl_80211_actionFrame_t*) action->data;
            rc = s_skipSubFrameHead(&publicActionFrameDataInfoTable, pubAction->category, &remainingLen);
            ASSERTS_FALSE(rc < SWL_RC_OK, rc, ME, "Fail to parse public action frame %d", pubAction->category);
            switch(pubAction->category) {
            case SWL_80211_MGT_FRAME_PUBLIC_ACTION_CATEGORY_VENDOR_SPECIFIC: {
                SWL_CALL(handlers->fProcActionVs, userData, frame, frameLen, (swl_80211_vendorIdEl_t*) pubAction->data, (size_t) remainingLen);
                break;
            }
            default: break;
            }
        }
        default: break;
        }
        break;
    }
    default: break;
    }
    return SWL_RC_OK;
}

