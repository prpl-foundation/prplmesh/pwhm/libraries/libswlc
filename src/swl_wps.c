/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlWps"
#include "swl/swl_wps.h"
#include "swl/swl_common_conversion.h"

#include "swl/swl_assert.h"
#include "swl/swl_common.h"

/** Human-readable version of #swl_wps_startRc_e */
const char* const swl_wps_startRcChar[] = {
    "OK",
    "Bug encountered. See logs for details.",
    "WPS disabled",
    "WPS already started",
    "No APs available on which WPS can be started",
    "All APs/EPs failed to start WPS",
    "No EPs available on which WPS can be started",
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_wps_startRcChar) == SWL_WPS_STARTRC_MAX,
                  "swl_wps_startRcChar not correctly defined");

static const char* const swl_wps_notifNames[] = {
    SWL_WPS_NOTIF_PAIRING_STARTED,
    SWL_WPS_NOTIF_PAIRING_ENDED,
    SWL_WPS_NOTIF_PAIRING_STARTERROR
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_wps_notifNames) == SWL_WPS_NOTIF_TYPE_PAIRING_MAX - SWL_WPS_NOTIF_TYPE_BASE,
                  "swl_wps_notifNames not correctly defined");

/**
 * Conversion from value such as #SWL_WPS_NOTIF_PAIRING_STARTED to value such as #SWL_WPS_NOTIF_TYPE_PAIRING_STARTED
 *
 * Returns 0 on invalid input.
 */
swl_wps_notif_type_ne swl_wps_notifNameToType(const char* name) {
    ASSERT_NOT_NULL(name, 0, ME, "NULL");
    uint32_t enumIdx = swl_conv_charToEnum(name, swl_wps_notifNames, SWL_ARRAY_SIZE(swl_wps_notifNames), UINT32_MAX);
    ASSERTS_NOT_EQUALS(enumIdx, UINT32_MAX, 0, ME, "Invalid notif name '%s'", name);
    return enumIdx + SWL_WPS_NOTIF_TYPE_BASE;
}

/**
 * Conversion from value such as #SWL_WPS_NOTIF_TYPE_PAIRING_STARTED to value such as #SWL_WPS_NOTIF_PAIRING_STARTED
 *
 * Returns NULL on invalid input.
 */
const char* swl_wps_notifTypeToName(swl_wps_notif_type_ne notifType) {
    ASSERTS_TRUE(notifType >= SWL_WPS_NOTIF_TYPE_BASE, NULL, ME, "Invalid notif type");
    ASSERTS_TRUE(notifType < SWL_WPS_NOTIF_TYPE_PAIRING_MAX, NULL, ME, "Invalid notif type");
    uint32_t enumIdx = notifType - SWL_WPS_NOTIF_TYPE_BASE;
    return swl_wps_notifNames[enumIdx];
}

/**
 * Mappings of wps config methods between IDS in WPS-v2.0.8 and Names in tr-181-2-15-1-usp
 */
static const char* wpsCfgMthdsTypeNameMaps[SWL_WPS_CFG_MTHD_MAX] = {
    "USBFlashDrive", "Ethernet", "Label", "Display", "ExternalNFCToken", "IntegratedNFCToken", "NFCInterface", "PushButton", "PIN",
    /* WPS 2.x added config methods */
    "VirtualPushButton", "PhysicalPushButton",
    "", "", /* shift wps config methods: offset between IDs in ieee802.11 and WPS-v2.0.8 and names order in tr-181-2-15-1-usp */
    "VirtualDisplay", "PhysicalDisplay"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(wpsCfgMthdsTypeNameMaps) == SWL_WPS_CFG_MTHD_MAX,
                  "swl_wps_cfgMthdsIdNameMaps not correctly defined");

/**
 * Conversion from wps config method type (as in WPS-v2.0.8) to Name (as in tr-181-2-15-1-usp)
 *
 * Returns Empty string on invalid input.
 */
const char* swl_wps_configMethodTypeToName(swl_wps_cfgMethod_ne type) {
    ASSERTS_TRUE(type < SWL_WPS_CFG_MTHD_MAX, "", ME, "Invalid");
    return wpsCfgMthdsTypeNameMaps[type];
}

/**
 * Conversion from wps config method Name (as in tr-181-2-15-1-usp) to type (as in WPS-v2.0.8)
 *
 * Returns SWL_WPS_CFG_MTHD_MAX on invalid input.
 */
swl_wps_cfgMethod_ne swl_wps_configMethodNameToType(const char* name) {
    ASSERTS_STR(name, SWL_WPS_CFG_MTHD_MAX, ME, "Empty");
    return swl_conv_charToEnum(name, wpsCfgMthdsTypeNameMaps, SWL_WPS_CFG_MTHD_MAX, SWL_WPS_CFG_MTHD_MAX);
}

/**
 * Conversion from wps config method name list (as in tr-181-2-15-1-usp) to types bit mask
 * (bit offset matching method IDs in WPS-v2.0.8)
 *
 * Returns M_SWL_WPS_CFG_MTHD_NONE on invalid input.
 */
swl_wps_cfgMethod_m swl_wps_configMethodNamesToMask(const char* names) {
    ASSERTS_STR(names, M_SWL_WPS_CFG_MTHD_NONE, ME, "Empty");
    return swl_conv_charToMask(names, wpsCfgMthdsTypeNameMaps, SWL_WPS_CFG_MTHD_MAX) & M_SWL_WPS_CFG_MTHD_ALL;
}

/**
 * Conversion from wps config method types bit mask (bit offset matching method IDs in WPS-v2.0.8)
 * to name list (as in tr-181-2-15-1-usp) with comma char as default separator
 *
 * Returns false on error.
 */
bool swl_wps_configMethodMaskToNames(char* buffer, size_t bufferSize, swl_wps_cfgMethod_m mask) {
    return swl_conv_maskToChar(buffer, bufferSize, (mask & M_SWL_WPS_CFG_MTHD_ALL), wpsCfgMthdsTypeNameMaps, SWL_WPS_CFG_MTHD_MAX);
}

