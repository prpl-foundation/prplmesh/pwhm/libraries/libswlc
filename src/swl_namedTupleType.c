/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <ctype.h>

#include "swl/swl_common_namedTupleType.h"
#include "swl/swl_common_tupleType.h"
#include "swl/swl_string.h"

#define ME "swlNTT"

ssize_t swl_ntt_getIndexOfName(swl_ntt_t* type, const char* name) {
    for(size_t i = 0; i < type->type.nrTypes; i++) {
        if(swl_str_matches(name, type->names[i])) {
            return i;
        }
    }
    return -1;
}


swl_typeEl_t* swl_ntt_getRefByIndex(swl_ntt_t* type, const swl_tuple_t* tuple, size_t index) {
    return swl_tupleType_getReference(&type->type, tuple, index);
}
swl_typeData_t* swl_ntt_getValByIndex(swl_ntt_t* type, const swl_tuple_t* tuple, size_t index) {
    return swl_tupleType_getValue(&type->type, tuple, index);
}
swl_typeEl_t* swl_ntt_getRefByName(swl_ntt_t* type, const swl_tuple_t* tuple, const char* name) {
    ssize_t index = swl_ntt_getIndexOfName(type, name);
    if(index < 0) {
        return NULL;
    }
    return swl_tupleType_getReference(&type->type, tuple, index);
}
swl_typeData_t* swl_ntt_getValByName(swl_ntt_t* type, const swl_tuple_t* tuple, const char* name) {
    ssize_t index = swl_ntt_getIndexOfName(type, name);
    if(index < 0) {
        return NULL;
    }
    return swl_tupleType_getValue(&type->type, tuple, index);
}
size_t swl_ntt_getOffset(swl_ntt_t* type, size_t index) {
    return swl_tupleType_getOffset(&type->type, index);
}


bool swl_ntt_check(swl_ntt_t* type) {
    return swl_tupleType_check(&type->type);
}
size_t swl_ntt_size(swl_ntt_t* type) {
    return swl_tupleType_size(&type->type);
}

swl_type_t* swl_ntt_type(swl_ntt_t* type) {
    return &type->type.type;
}

bool swl_ntt_equals(swl_ntt_t* type, swl_tuple_t* tuple1, swl_tuple_t* tuple2) {
    return swl_tupleType_equals(&type->type, tuple1, tuple2);
}

bool swl_ntt_equalsByMask(swl_ntt_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2, size_t typeMask) {
    return swl_tupleType_equalsByMask(&tplType->type, tuple1, tuple2, typeMask);
}

size_t swl_ntt_getDiffMask(swl_ntt_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2) {
    return swl_tupleType_getDiffMask(&tplType->type, tuple1, tuple2);
}



void swl_ntt_cleanup(swl_ntt_t* type, swl_tuple_t* tuple) {
    swl_tupleType_cleanup(&type->type, tuple);
}


static ssize_t s_ntt_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = &ntt->type;
    ssize_t curSize = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_MAP_OPEN]);
    ssize_t totalSize = curSize;
    ssize_t maxSize = tgtStrSize;
    for(size_t i = 0; i < ntt->type.nrTypes && curSize > 0; i++) {
        size_t keyLen = strlen(ntt->names[i]) + strlen(args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
        keyLen = keyLen * 2 + 1;  // allow for escaping and null termination
        char buffer[keyLen];
        memset(buffer, 0, keyLen);

        curSize = snprintf(buffer, keyLen, "%s%s", ntt->names[i], args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
        if(curSize < 0) {
            return curSize;
        }

        bool success = swl_str_addEscapeChar(buffer, keyLen, args->charsToEscape, args->escapeChar);
        if(!success) {
            return -1;
        } else {
            curSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", buffer);
        }
        totalSize += curSize;

        swl_type_t* type = ntt->type.types[i];
        curSize = swl_type_toCharEsc(type, &tgtStr[totalSize],
                                     SWL_MAX(0, maxSize - totalSize), swl_tupleType_getValue(tt, srcData, i), args);
        if(curSize < 0) {
            return curSize;
        } else {
            totalSize += curSize;
        }
        if(i < ntt->type.nrTypes - 1) {
            curSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_MAP_NEXT]);
            if(curSize < 0) {
                return curSize;
            } else {
                totalSize += curSize;
            }
        }
    }
    curSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_MAP_CLOSE]);
    if(curSize < 0) {
        return curSize;
    }
    totalSize += curSize;
    return totalSize;
}

static bool s_ntt_fromCharExt_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = (swl_tupleType_t*) &ntt->type;

    size_t index = strlen(args->delim[SWL_PRINT_DELIM_MAP_OPEN]);
    size_t assignSize = strlen(args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
    size_t nextSize = strlen(args->delim[SWL_PRINT_DELIM_MAP_NEXT]);

    size_t i = 0;
    while(true) {
        if(!swl_print_hasNext(args, &srcStr[index], false)) {
            break;
        }
        if(i > 0) {
            index += nextSize;
        }
        char* keyName = NULL;
        ssize_t keySize = swl_type_fromCharPrint(&gtSwl_type_charPtr, &keyName, &srcStr[index], args, SWL_PRINT_DELIM_MAP_ASSIGN);

        if(keyName == NULL) {
            return false;
        }
        if(keySize < 0) {
            free(keyName);
            return false;
        }

        index += keySize + assignSize;
        ssize_t typeIndex = swl_ntt_getIndexOfName(ntt, keyName);

        if(typeIndex < 0) {
            SAH_TRACEZ_WARNING(ME, "Unknown key %s", keyName);
            free(keyName);
            continue;
        }
        free(keyName);
        size_t posIndex = (size_t) typeIndex;

        swl_typeEl_t* el = swl_ntt_getRefByIndex(ntt, tgtData, posIndex);

        swl_type_t* targetType = tt->types[posIndex];

        if(swl_type_getFlag(targetType) != SWL_TYPE_FLAG_VALUE) {
            while(isspace(srcStr[index])) {
                index++;
            }
        }

        ssize_t valueSize = swl_type_fromCharPrint(targetType, el, &srcStr[index], args, SWL_PRINT_DELIM_MAP_NEXT);
        ASSERTS_TRUE(valueSize >= 0, false, ME, "ERR");

        index += valueSize;
        i++;
    }

    return true;
}

static bool s_ntt_fromChar_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    return s_ntt_fromCharExt_cb(type, tgtData, srcStr, args);
}


static bool s_ntt_equals_cb(swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = &ntt->type;
    for(size_t i = 0; i < tt->nrTypes; i++) {
        if(!swl_type_equals(tt->types[i], swl_tupleType_getValue(tt, key0, i), swl_tupleType_getValue(tt, key1, i))) {
            return false;
        }
    }
    return true;
}


static swl_typeData_t* s_ntt_copy_cb(swl_type_t* type, const swl_typeData_t* src) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = &ntt->type;
    swl_tuple_t* data = calloc(1, swl_ntt_size(ntt));

    ASSERT_NOT_NULL(data, NULL, ME, "ALLOC FAIL");

    for(size_t i = 0; i < tt->nrTypes; i++) {
        swl_type_copyTo(tt->types[i], swl_tupleType_getReference(tt, data, i), swl_tupleType_getValue(tt, src, i));
    }
    return (swl_typeData_t*) data;
}

static bool s_ntt_toFile_cb(swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = &ntt->type;
    swl_print_addOpen(file, args, false);

    for(size_t i = 0; i < tt->nrTypes; i++) {
        bool success = true;

        swl_print_startValue(file, args);
        success = swl_print_toStreamBuf(file, args, ntt->names[i], strlen(ntt->names[i]));
        swl_print_stopValue(file, args);

        swl_print_addAssign(file, args);

        swl_typeFlag_e flag = swl_type_getFlag(tt->types[i]);

        if(flag == SWL_TYPE_FLAG_VALUE) {
            swl_print_startValue(file, args);
            success = swl_type_toFilePriv(tt->types[i], file, swl_tupleType_getValue(tt, srcData, i), args);
            swl_print_stopValue(file, args);
        } else if((flag == SWL_TYPE_FLAG_NUMBER) ||
                  (flag == SWL_TYPE_FLAG_BASIC)) {
            success = swl_type_toFilePriv(tt->types[i], file, swl_tupleType_getValue(tt, srcData, i), args);
        } else if((flag == SWL_TYPE_FLAG_MAP) || (flag == SWL_TYPE_FLAG_LIST)) {
            success = swl_type_toFilePriv(tt->types[i], file, swl_tupleType_getValue(tt, srcData, i), args);
        } else {
            SAH_TRACEZ_ERROR(ME, "Unknown flag %u", flag);
        }

        if(!success) {
            SAH_TRACEZ_ERROR(ME, "FAIL");
            return false;
        }
        if(i < tt->nrTypes - 1) {
            swl_print_addNext(file, args, false);
        }
    }
    swl_print_addClose(file, args, false);

    return true;
}

static void s_ntt_cleanup_cb(swl_type_t* type, swl_typeEl_t* tgtData) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_t* tt = &ntt->type;
    for(size_t i = 0; i < tt->nrTypes; i++) {
        swl_type_cleanup(tt->types[i], swl_tupleType_getReference(tt, tgtData, i));
    }
}

swl_typeFun_t swl_namedTupleType_fun = {
    .name = "swl_namedTupleType",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_MAP,
    .toChar = (swl_type_toChar_cb) s_ntt_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_ntt_fromChar_cb,
    .fromCharExt = (swl_type_fromCharExt_cb) s_ntt_fromCharExt_cb,
    .equals = (swl_type_equals_cb) s_ntt_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_ntt_cleanup_cb,
    .copy = (swl_type_copy_cb) s_ntt_copy_cb,
    .toFile = (swl_type_toFile_cb) s_ntt_toFile_cb,
    .subFun = &swl_namedTupleType_mapSTypeFun,
};




static swl_rc_ne s_init_cb (swl_mapSType_t* type, swl_mapEl_t* tuple) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    memset(tuple, 0, ntt->type.type.size);
    return SWL_RC_OK;
}

/**
 * Clear the map, removing the contents but keeping the map initialized
 */
static void s_clear_cb(swl_mapSType_t* type, swl_mapEl_t* tuple) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_cleanup(&ntt->type, tuple);
}

/**
 * Cleanup the map, deallocating whatever needs to be deallocated. The map should
 * be reinitialized before further use.
 */
static void s_mapCleanup_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    swl_tupleType_cleanup(&ntt->type, tuple);
}

/**
 * Return the maximum size of this map type.
 * If there is no maximum size, other than "dynamic memory allocation allowance", 0 shall be returned.
 */
static size_t s_maxSize_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    return ntt->type.nrTypes;
}

/**
 * Return the type of the keys of this collection.
 * A map currently only supports having a single key type.
 */
static swl_type_t* s_getKeyType_cb (swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED) {
    return swl_type_charPtr;
}

/**
 * Return the type of the value of the given key. If isSingleValueMap is set to true, then
 * this must return said type regardsless of key input (i.e. NULL or non-existant key).
 * If isSingleValueMap is set to false, it must return NULL for keys that are not present, and the relevant type if the
 * value exists that matches the given key.
 */
static swl_type_t* s_getValueType_cb (swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED, swl_typeData_t* key) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;

    ssize_t index = swl_ntt_getIndexOfName(ntt, key);
    if(index < 0) {
        return NULL;
    }

    return ntt->type.types[index];
}


/**
 * The current size of this map.
 *
 * For fixed size maps, this shall return the number of non-zero elements.
 * These maps may have an allowEmpty flag, which basically means whether "empty" is a valid value,
 * and size counting should continue after.
 */
static size_t s_size_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;

    ASSERTI_TRUE(swl_tupleType_check(&ntt->type), 0, ME, "NULL");
    size_t nrNotEmpty = 0;

    for(size_t i = 0; i < ntt->type.nrTypes; i++) {
        swl_type_t* elType = ntt->type.types[i];
        if(!swl_type_isEmpty(elType, swl_ntt_getValByIndex(ntt, tuple, i))) {
            nrNotEmpty++;
        }
    }
    return nrNotEmpty;
}

/**
 * Add data to given map
 *
 * For fixed size maps, data may be written to first empty space, if possible.
 */
static bool s_add_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED,
                     swl_typeData_t* key _UNUSED, swl_typeData_t* value _UNUSED) {
    return false;
}

/**
 * Provide reference to an "empty" element, if possible.
 * For dynamic maps, it shall create a new empty element.
 */
static swl_typeEl_t* s_alloc_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED) {
    return NULL;
}

/**
 * Write data at given index, removing whatever data may have been there before.
 * Provided data shall be deep copied into target space.
 */
static swl_rc_ne s_set_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple, swl_typeData_t* key, swl_typeData_t* data) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    ssize_t index = swl_ntt_getIndexOfName(ntt, key);
    if(index < 0) {
        return SWL_RC_INVALID_PARAM;
    }
    swl_type_copyTo(ntt->type.types[index], swl_ntt_getRefByIndex(ntt, tuple, index), data);
    return SWL_RC_OK;
}

/**
 * Delete the data at given index. All other data with index higher then provided index shall be shifted one down, if
 * it's a "same type" map.
 */
static swl_rc_ne s_delete_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple, swl_typeData_t* key) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    ssize_t index = swl_ntt_getIndexOfName(ntt, key);
    if(index < 0) {
        return SWL_RC_INVALID_PARAM;
    }
    swl_tupleType_cleanupIndex(&ntt->type, tuple, index);
    return SWL_RC_OK;
}

/**
 * Find the element of the given data value
 */
static swl_typeEl_t* s_find_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple _UNUSED, swl_typeData_t* data _UNUSED) {
    return NULL;
}

/**
 * Check whether two maps are equal
 */
static bool s_mapEquals_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple1, swl_mapEl_t* tuple2) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;

    return swl_ntt_equals(ntt, tuple1, tuple2);
}

/**
 * Get the value data pointer of the value at the given index.
 */
static swl_typeData_t* s_getValue_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple, swl_typeData_t* key) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    return swl_ntt_getValByName(ntt, tuple, key);
}

/**
 * Provide the element pointer of the value at the given index.
 */
static swl_typeEl_t* s_getReference_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* tuple, swl_typeData_t* key) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    return swl_ntt_getRefByName(ntt, tuple, key);
}

/**
 * Get an iterator, initialized at the first element
 */
static swl_listSTypeIt_t s_getFirstIt_cb(swl_mapSType_t* type _UNUSED, const swl_mapEl_t* tuple) {
    swl_listSTypeIt_t it;
    memset(&it, 0, sizeof(it));
    it.index = 0;
    it.coll = (swl_coll_t*) tuple;
    it.data = (swl_typeEl_t*) it.index;
    it.valid = true;
    return it;
}

/**
 * Move iterator to next element
 */
static void s_map_nextIt_cb(swl_mapSType_t* type _UNUSED, swl_listSTypeIt_t* it) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;

    it->index++;
    if(it->index >= ntt->type.nrTypes) {
        it->valid = false;
    } else {
        it->valid = true;
    }

    it->data = (swl_typeEl_t*) it->index;

}

/**
 * Delete current element. Note that this will NOT move the iterator to next element, so
 * you MUST still call nextIt.
 * Calling  delIt a second time shall result in no change.
 */
static void s_map_delIt_cb(swl_mapSType_t* type, swl_listSTypeIt_t* it) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;

    swl_tupleType_cleanupIndex(&ntt->type, it->coll, it->index);
}



static swl_typeData_t* s_key_cb(swl_mapSType_t* type, const swl_mapEl_t* tuple _UNUSED, swl_mapEl_t* el) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    size_t index = (size_t) el;

    return ntt->names[index];
}

static swl_typeData_t* s_value_cb(swl_mapSType_t* type, const swl_mapEl_t* tuple _UNUSED, swl_mapEl_t* el) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    size_t index = (size_t) el;

    return swl_ntt_getValByIndex(ntt, tuple, index);
}

static swl_typeEl_t* s_keyRef_cb(swl_mapSType_t* type _UNUSED, const swl_mapEl_t* tuple _UNUSED, swl_mapEl_t* el _UNUSED) {
    return NULL;
}

static swl_typeEl_t* s_valueRef_cb(swl_mapSType_t* type, const swl_mapEl_t* tuple _UNUSED, swl_mapEl_t* el) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    size_t index = (size_t) el;
    return swl_ntt_getRefByIndex(ntt, tuple, index);
}

static swl_type_t* s_valueType_cb(swl_mapSType_t* type, const swl_mapEl_t* tuple _UNUSED, swl_mapEl_t* el _UNUSED) {
    swl_ntt_t* ntt = (swl_ntt_t*) type;
    size_t index = (size_t) el;
    return ntt->type.types[index];
}



swl_mapSTypeFun_t swl_namedTupleType_mapSTypeFun = {
    .areKeysFixed = true,
    .hasVarValType = true,
    .init = s_init_cb,
    .clear = s_clear_cb,
    .cleanup = s_mapCleanup_cb,
    .maxSize = s_maxSize_cb,
    .getKeyType = s_getKeyType_cb,
    .getValueType = s_getValueType_cb,
    .size = s_size_cb,
    .add = s_add_cb,
    .alloc = s_alloc_cb,
    .set = s_set_cb,
    .delete = s_delete_cb,
    .find = s_find_cb,
    .equals = s_mapEquals_cb,
    .getValue = s_getValue_cb,
    .getReference = s_getReference_cb,
    .firstIt = s_getFirstIt_cb,
    .nextIt = s_map_nextIt_cb,
    .delIt = s_map_delIt_cb,
    .getEntryKey = s_key_cb,
    .getEntryVal = s_value_cb,
    .getEntryKeyRef = s_keyRef_cb,
    .getEntryValRef = s_valueRef_cb,
    .getEntryValType = s_valueType_cb,
};



