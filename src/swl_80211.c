/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <swl/swl_80211.h>
#include <swl/swl_staCap.h>
#include <swl/swl_common_table.h>

#define ME "swl802x"

const char* swl_multiApType_str[] = {
    "Default",
    "FronthaulBSS",
    "BackhaulBSS",
    "BackhaulSTA"
};

SWL_TABLE_UINT32STR(s_frametype_str_List, ARR(
                        {SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST, "AssocReq"},
                        {SWL_80211_MGT_FRAME_TYPE_ASSOC_RESPONSE, "AssocResp"},
                        {SWL_80211_MGT_FRAME_TYPE_REASSOC_REQUEST, "ReassocReq"},
                        {SWL_80211_MGT_FRAME_TYPE_REASSOC_RESPONSE, "ReassocResp"},
                        {SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST, "ProbeReq"},
                        {SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE, "ProbeResp"},
                        {SWL_80211_MGT_FRAME_TYPE_BEACON, "Beacon"},
                        {SWL_80211_MGT_FRAME_TYPE_ATIM, "Atim"},
                        {SWL_80211_MGT_FRAME_TYPE_DISASSOCIATION, "Disassociation"},
                        {SWL_80211_MGT_FRAME_TYPE_AUTHENTICATION, "Authentication"},
                        {SWL_80211_MGT_FRAME_TYPE_DEAUTHENTICATION, "Deauthentication"},
                        {SWL_80211_MGT_FRAME_TYPE_ACTION, "Action"},
                        {SWL_80211_MGT_FRAME_TYPE_NOACKACTION, "Noackaction"},
                        {SWL_80211_MGT_FRAME_TYPE_RESERVED, "Reserved"},
                        {SWL_80211_MGT_FRAME_TYPE_INVALID, "Invalid"},
                        )
                    );

/*
 * Sets an swl_80211_htCapIE_t structure from a ht byte frame
 */
void swl_80211_parseHtCap(swl_80211_htCapIE_t* htcap_ie, uint8_t* frm) {
    swl_80211_htCapIE_t* frame_ie = (swl_80211_htCapIE_t*) (frm + 2);
    ASSERTS_NOT_NULL(htcap_ie, , ME, "NULL");
    ASSERTS_NOT_NULL(frame_ie, , ME, "NULL");
    SAH_TRACEZ_INFO(ME, "Parsing HT Capabilities %p", frm);
    memset(htcap_ie, 0, sizeof(swl_80211_htCapIE_t));

    htcap_ie->htCapInfo = frame_ie->htCapInfo;
    htcap_ie->ampduParam = frame_ie->ampduParam;
    for(int i = 0; i < SWL_80211_MCS_SET_LEN; i++) {
        htcap_ie->supMCSSet[i] = frame_ie->supMCSSet[i];
    }
    htcap_ie->htExtCap = frame_ie->htExtCap;
    htcap_ie->txBfCap = frame_ie->txBfCap;
    htcap_ie->ASELCap = frame_ie->ASELCap;
    SAH_TRACEZ_INFO(ME, "Parsing HT Capabilities from %p to %p", frame_ie, &htcap_ie->htCapInfo);
}

/*
 * Sets an swl_80211_vhtCapIE_t structure from a vht byte frame
 */
void swl_80211_parseVhtCap(swl_80211_vhtCapIE_t* vhtCapIE, uint8_t* frm) {
    SAH_TRACEZ_INFO(ME, "swl_80211_parseVhtCap");
    ASSERT_NOT_NULL(vhtCapIE, , ME, "NULL");
    ASSERT_NOT_NULL(frm, , ME, "NULL");
    memset(vhtCapIE, 0, sizeof(swl_80211_vhtCapIE_t));

    vhtCapIE->vhtCapInfo = *(swl_80211_vhtCapInfo_m*) (frm + 2);
    vhtCapIE->rxMcsMap = *(uint16_t*) (frm + SWL_80211_VHT_MCS_OFFSET);
    vhtCapIE->rxRateMap = ((*((uint16_t*) (frm + SWL_80211_VHT_MCS_OFFSET + 2))) >> 3);
    vhtCapIE->txMcsMap = *(uint16_t*) (frm + SWL_80211_VHT_MCS_OFFSET + 4);
    vhtCapIE->txRateMap = ((*((uint16_t*) (frm + SWL_80211_VHT_MCS_OFFSET + 6))) >> 3);
}


/*
 * Sets an swl_80211_heCapIE_t structure from a he byte frame
 */
void swl_80211_parseHeCap(swl_80211_heCapIE_t* heCapIEDest, uint8_t* frm) {
    SAH_TRACEZ_INFO(ME, "Parsing HE Capabilities %p", heCapIEDest);
    ASSERT_NOT_NULL(heCapIEDest, , ME, "NULL");
    ASSERT_NOT_NULL(frm, , ME, "NULL");

    swl_80211_heCapIE_t* frameStruct = NULL;
    frameStruct = (swl_80211_heCapIE_t*) frm;
    ASSERT_NOT_NULL(frameStruct, , ME, "NULL");
    memset(heCapIEDest, 0, sizeof(swl_80211_heCapIE_t));

    heCapIEDest->heCaps = frm[0];
    heCapIEDest->macCap = frameStruct->macCap;
    heCapIEDest->phyCap = frameStruct->phyCap;

    if(SWL_BIT_IS_SET(heCapIEDest->phyCap.cap[0], 2)) {
        heCapIEDest->mcsCaps[0] = frameStruct->mcsCaps[SWL_80211_HECAP_MCS_LT80_ELM];
    }
    if(SWL_BIT_IS_SET(heCapIEDest->phyCap.cap[0], 3)) {
        heCapIEDest->mcsCaps[1] = frameStruct->mcsCaps[SWL_80211_HECAP_MCS_160_ELM];
    }
    if(SWL_BIT_IS_SET(heCapIEDest->phyCap.cap[0], 4)) {
        heCapIEDest->mcsCaps[2] = frameStruct->mcsCaps[SWL_80211_HECAP_MCS_80P80_ELM];
    }
}

const char* swl_80211_mgtFrameTypeToChar(swl_80211_mgtFrameType_ne type) {
    return swl_tableUInt32Char_toValue(&s_frametype_str_List, type);
}

/*
 * @brief return management frame type/subtype from frame control struct
 *
 * @param pFrameControl pointer to management frame control struct
 *
 * @return type as one of swl_80211_mgtFrameType_ne values
 *         or byte value when not defined in this enum
 *         SWL_80211_MGT_FRAME_TYPE_INVALID when frame is not a valid mgmt frame
 */
uint8_t swl_80211_mgtFrameType(swl_80211_mgmtFrameControl_t* pFrameControl) {
    ASSERT_NOT_NULL(pFrameControl, SWL_80211_MGT_FRAME_TYPE_INVALID, ME, "NULL");
    ASSERT_EQUALS(pFrameControl->type, 0, SWL_80211_MGT_FRAME_TYPE_INVALID, ME, "Not a mgmt frame");
    return (pFrameControl->subType << 4);
}

SWL_TABLE_UINT64STR(swl_80211_htCapInfoStrTable, ARR(
                        {M_SWL_80211_HTCAPINFO_LDPC, "LPDC"},
                        {M_SWL_80211_HTCAPINFO_CAP_40, "CAP_40"},
                        {M_SWL_80211_HTCAPINFO_POW_SAVE, "POWER_SAVE"},
                        {M_SWL_80211_HTCAPINFO_GREENFEELD, "GREEN_FIELD"},
                        {M_SWL_80211_HTCAPINFO_SGI_20, "SHORT_GI_20"},
                        {M_SWL_80211_HTCAPINFO_SGI_40, "SHORT_GI_40"},
                        {M_SWL_80211_HTCAPINFO_TX_STBC, "TX_STBC"},
                        {M_SWL_80211_HTCAPINFO_HT_DELAY_BA, "HT_DELAY_BA"},
                        {M_SWL_80211_HTCAPINFO_MAX_AMSDU, "MAX_AMSDU"},
                        {M_SWL_80211_HTCAPINFO_MODE_40, "MODE_40"},
                        {M_SWL_80211_HTCAPINFO_INTOL_40, "INTOL_40"},
                        {M_SWL_80211_HTCAPINFO_LSIG_PROT, "LSIG_PROT"},
                        )
                    );
ssize_t swl_80211_htCapMaskToChar (char* htCapStr, size_t htCapStrSize,
                                   swl_80211_htCapInfo_m mask) {
    return swl_tableUInt64Char_maskToCharSep(&swl_80211_htCapInfoStrTable, htCapStr, htCapStrSize, mask, ',');
}

SWL_TABLE_UINT64STR(swl_80211_vhtCapInfoStrTable, ARR(
                        {M_SWL_80211_VHTCAPINFO_RX_LDPC, "RX_LDPC"},
                        {M_SWL_80211_VHTCAPINFO_SGI_80, "SGI_80"},
                        {M_SWL_80211_VHTCAPINFO_SGI_160, "SGI_160"},
                        {M_SWL_80211_VHTCAPINFO_TX_STBC, "TX_STBC"},
                        {M_SWL_80211_VHTCAPINFO_RX_STBC, "RX_STBC"},
                        {M_SWL_80211_VHTCAPINFO_SU_BFR, "SU_BFR"},
                        {M_SWL_80211_VHTCAPINFO_SU_BFE, "SU_BFE"},
                        {M_SWL_80211_VHTCAPINFO_MU_BFR, "MU_BFR"},
                        {M_SWL_80211_VHTCAPINFO_MU_BFE, "MU_BFE"},
                        {M_SWL_80211_VHTCAPINFO_TXOP_PS, "TXOP_PS"},
                        {M_SWL_80211_VHTCAPINFO_HTC_CAP, "HTC_VHT"},
                        {M_SWL_80211_VHTCAPINFO_LINK_ADAPT_CAP, "LINK_ADAPT_CAP"},
                        {M_SWL_80211_VHTCAPINFO_RX_ANT_PAT_CONS, "RX_ANT_PAT_CONS"},
                        {M_SWL_80211_VHTCAPINFO_TX_ANT_PAT_CONS, "TX_ANT_PAT_CONS"},
                        )
                    );

ssize_t swl_80211_vhtCapMaskToChar(char* vhtCapStr, size_t vhtCapStrSize,
                                   swl_80211_vhtCapInfo_m mask) {
    return swl_tableUInt64Char_maskToCharSep(&swl_80211_vhtCapInfoStrTable, vhtCapStr, vhtCapStrSize, mask, ',');
}

//80211 He capabilities
SWL_TABLE_UINT64STR(swl_80211_heCapPhysStrTable, ARR(
                        {M_SWL_80211_HE_PHY_B0, "PHY_B0"},
                        {M_SWL_80211_HE_PHY_40MHZ_2_4GHZ, "40MHZ_2_4GHZ"},
                        {M_SWL_80211_HE_PHY_40_80MHZ_5GHZ, "40_80MHZ_5GHZ"},
                        {M_SWL_80211_HE_PHY_160MHZ_5GHZ, "160MHZ_5GHZ"},
                        {M_SWL_80211_HE_PHY_160_80_80_MHZ_5GHZ, "160_80_80_MHZ_5GHZ"},
                        {M_SWL_80211_HE_PHY_SU_BEAMFORMER, "SU_BEAMFORMER"},
                        {M_SWL_80211_HE_PHY_SU_BEAMFORMEE, "SU_BEAMFORMEE"},
                        {M_SWL_80211_HE_PHY_MU_BEAMFORMEE, "MU_BEAMFORMEE"},
                        {M_SWL_80211_HE_PHY_BEAMFORMEE_STS_LE_80MHZ, "BEAMFORMEE_STS_LE_80MHZ"},
                        {M_SWL_80211_HE_PHY_BEAMFORMEE_STD_GT_80MHZ, "BEAMFORMEE_STD_GT_80MHZ"},
                        )
                    );
ssize_t swl_80211_hePhyCapMaskToChar(char* hePhysCapStr, size_t hePhysCapStrSize,
                                     swl_80211_hePhyCapInfo_m mask) {
    return swl_tableUInt64Char_maskToCharSep(&swl_80211_heCapPhysStrTable, hePhysCapStr, hePhysCapStrSize, mask, ',');
}
