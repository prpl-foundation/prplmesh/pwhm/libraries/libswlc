/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlStr"
#include <stddef.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>

#include <swl/swl_common.h>
#include <swl/swl_string.h>
#include "swl/swl_assert.h"

/****************************************************************************
* SWL String library
*
* This library attempts to provide helpful functions to handle char* strings
* in a memory safe and compact way.
****************************************************************************/


/**
 * @brief write all characters in string to lower case, until strLen characters have been turned to lower or
 * null character is encountered
 *
 * @param string: the string of which the characters need to be converted to lower.
 * @param nrChar: the number of characters that will be examined to be turned to upper. Note that if
 * the length of the string is smaller then nrChar, the function shall inspect all characters
 */
void swl_str_toLower(char* str, size_t nrChar) {
    if(str == NULL) {
        return;
    }
    size_t i = 0;
    for(i = 0; i < nrChar && str[i] != 0; i++) {
        str[i] = tolower(str[i]);
    }
}

/**
 * @brief Remove whitespaces characters from a string.
 *
 * @param str: the string of which the whitespaces characters need to be removed.
 */
size_t swl_str_removeWhitespace(char* str) {
    ASSERTS_NOT_NULL(str, 0, ME, "NULL str");
    size_t strLen = strlen(str);
    ASSERTS_NOT_EQUALS(strLen, 0, 0, ME, "Invalid strLen");

    size_t i = 0;
    while(i < strLen) {
        if(isspace(str[i])) {
            memmove(&str[i], &str[i + 1], strLen - i);
        } else {
            i++;
        }
    }
    return strlen(str);
}

/**
 * @brief write all characters in string to upper, until strLen characters have been turned to upper or
 * null character is encountered
 *
 * @param string: the string of which the characters need to be converted to upper.
 * @param nrChar: the number of characters that will be examined to be turned to upper. Note that if
 * the length of the string is smaller then nrChar, the function shall inspect all characters
 */
void swl_str_toUpper(char* str, size_t nrChar) {
    if(str == NULL) {
        return;
    }
    size_t i = 0;
    for(i = 0; i < nrChar && str[i] != 0; i++) {
        str[i] = toupper(str[i]);
    }
}

/**
 * @brief A safe version to copy a sub string to a destination buffer
 *
 * The downside of strncpy is that is will not terminate the string,
 * when the source sub-string is larger than destination, we end up with a buffer that is not null terminated,
 * i.e not a valid string
 * This version will do strncpy and write a terminating NULL character at the end
 *
 * @param tgtStr destination string buffer
 * @param tgtStrSize size of the destination buffer
 * @param srcStr source string. If NULL, interpreted as the empty string.
 * @param srcSubStrSize size of the source sub-string buffer
 *
 * @return true on success, false on failure
 *   - `tgtStr` buffer too short (also if too short for the trailing `\0`).
 *   - `tgtStr` is NULL.
 */
bool swl_str_ncopy(char* tgtStr, size_t tgtStrSize, const char* srcStr, size_t srcSubStrSize) {
    if(!tgtStr || !tgtStrSize) {
        return false;
    }
    if(!srcSubStrSize) {
        return true;
    }
    if(!srcStr || !srcStr[0]) {
        tgtStr[0] = '\0';
        return true;
    }

    size_t safeSrcLen = 0;
    while((safeSrcLen < srcSubStrSize) && srcStr[safeSrcLen]) {
        safeSrcLen++;
    }
    size_t sizeToCopy = SWL_MIN(safeSrcLen, (tgtStrSize - 1));
    strncpy(tgtStr, srcStr, sizeToCopy);
    tgtStr[sizeToCopy] = '\0';
    return safeSrcLen + 1 <= tgtStrSize;
}

/**
 * @brief A safe version to copy a string to a destination buffer
 *
 * This version will copy as much source characters for the source, that fit the target string size and write a terminating NULL character at the end
 *
 * @param tgtStr destination string buffer
 * @param tgtStrSize size of the destination buffer
 * @param srcStr source string. If NULL, interpreted as the empty string.
 * @return true on success, false on failure
 *   - `tgtStr` buffer too short (also if too short for the trailing `\0`).
 *   - `tgtStr` is NULL.
 */
bool swl_str_copy(char* tgtStr, size_t tgtStrSize, const char* srcStr) {
    if(!tgtStr || !tgtStrSize) {
        return false;
    }
    if(!srcStr) {
        tgtStr[0] = '\0';
        return true;
    }

    strncpy(tgtStr, srcStr, tgtStrSize - 1);
    tgtStr[tgtStrSize - 1] = '\0';
    return strlen(srcStr) + 1 <= tgtStrSize;
}

/**
 * @brief Copy `srcStr` to `tgtStr` and free `tgtStr` first if not-null
 *
 * @param tgtStr Pointer to to-be-overwritten pointer to new string
 *   If points to non-null, is freed first.
 * @param srcStr source string. If NULL, *tgtStr is made to point to NULL as well.
 *   If same as *tgtStr, then nothing happens.
 * @return true on success, false on
 *   - out of memory
 *   - tgtStr==NULL (not *tgtStr==NULL, that's okay)
 */
bool swl_str_copyMalloc(char** tgtStr, const char* srcStr) {
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERTS_TRUE(*tgtStr != srcStr, true, ME, "no-op");
    if(*tgtStr) {
        free(*tgtStr);
        *tgtStr = NULL;
    }
    ASSERTS_NOT_NULL(srcStr, true, ME, "tgtStr already (made) NULL");

    *tgtStr = strdup(srcStr);
    ASSERT_NOT_NULL(*tgtStr, false, ME, "Out of mem");

    return true;
}

/**
 * @brief A safe version to concatenate a sub-string to a target string
 *
 * The downside of strncat is that is will not terminate the string,
 * when the size of the src sub-string is larger then destination, we end up with a buffer that is not null terminated,
 * i.e not a valid string
 * This version will append the source sub-string to its destination,
 * by calling swl_str_ncopy at the end of the destination string
 *
 * @param tgtStr destination string buffer
 * @param srcStr source string to append. Interpreted as empty string if NULL.
 * @param tgtStrSize size of the destination buffer
 * @param srcSubStrSize size of the source sub-string buffer
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL
 *   - `tgtStr` is too short (also if too short for the trailing `\0` byte).
 */
bool swl_str_ncat(char* tgtStr, size_t tgtStrSize, const char* srcStr, size_t srcSubStrSize) {
    if(!tgtStr || !tgtStrSize) {
        return false;
    }

    tgtStr[tgtStrSize - 1] = '\0';
    size_t curDestLen = strlen(tgtStr);

    bool ret = swl_str_ncopy(&tgtStr[curDestLen], (tgtStrSize - curDestLen), srcStr, srcSubStrSize);
    if(ret && srcStr && srcStr[0] && srcSubStrSize) {
        ret = (curDestLen + srcSubStrSize + 1) <= tgtStrSize;
    }
    return ret;
}

/**
 * @brief A safe version to concatenate two strings
 *
 * The downside of strncat is that is will not terminate the string,
 * when the size of the src is larger then destination, we end up with a buffer that is not null terminated,
 * i.e not a valid string
 * This version will append the source string to its destination,
 * by calling swl_str_copy at the end of the destination string
 *
 * @param tgtStr destination string buffer
 * @param tgtStrSize size of the destination buffer
 * @param srcStr source string to append. Interpreted as empty string if NULL.
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL
 *   - `tgtStr` is too short (also if too short for the trailing `\0` byte).
 */
bool swl_str_cat(char* tgtStr, size_t tgtStrSize, const char* srcStr) {
    if(!tgtStr || !tgtStrSize) {
        return false;
    }

    tgtStr[tgtStrSize - 1] = '\0';
    size_t curDestLen = strlen(tgtStr);

    return swl_str_copy(&tgtStr[curDestLen], (tgtStrSize - curDestLen), srcStr);
}


/**
 * @brief A safe version to append a char to a string
 * *
 * @param tgtStr destination string buffer
 * @param srcChar source char to append.
 * @param tgtStrSize size of the destination buffer
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL
 *   - `tgtStr` is too short. tgtStrSize must be at least strlen(tgtStr) + 2.
 */
bool swl_str_catChar(char* tgtStr, size_t tgtStrSize, char srcChar) {
    if(!tgtStr || !tgtStrSize) {
        return false;
    }

    size_t curDestLen = strlen(tgtStr);
    if(curDestLen + 1 >= tgtStrSize) {
        return false;
    }

    tgtStr[curDestLen] = srcChar;
    tgtStr[curDestLen + 1] = '\0';

    return true;
}

/**
 * @brief A safe version to concatenate formatted strings.
 *
 * In case the tgtStr is too small, the output shall be written to the extend possible, ensuring
 * terminating NULL character.
 *
 * @param tgtStr destination buffer
 * @param tgtStrSize size of destination buffer
 * @param format output string format
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL.
 *   - `tgtStr` is too short (also if too short for the trailing `\0` byte).
 *   - `format` is NULL.
 */
bool swl_str_catFormat(char* tgtStr, size_t tgtStrSize, const char* format, ...) {
    va_list args;
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_NOT_NULL(format, false, ME, "NULL");
    ASSERT_TRUE(tgtStrSize > 0, false, ME, "null destsize");
    size_t curDestLen = strlen(tgtStr);
    ASSERT_TRUE(tgtStrSize > curDestLen, false, ME, "dest already full or not initialized");

    va_start(args, format);
    int ret = vsnprintf(&tgtStr[curDestLen], (tgtStrSize - curDestLen), format, args);
    va_end(args);
    ASSERTI_FALSE(ret >= (int) (tgtStrSize - curDestLen), false, ME, "output was truncated");
    return true;
}

/**
 * @brief Concatenate `srcStr` to `tgtStr` with `separator` inbetween if `tgtStr` is nonempty.
 *
 * Example: `swl_strlst_cat(tgtStr, 1000, ",", "Wednesday")`
 * - If 'tgtStr' was "Monday,Tuesday", this makes `tgtStr` "Monday,Tuesday,Wednesday".
 * - If 'tgtStr' was "", this makes `tgtStr` "Wednesday" (and not ",Wednesday").
 *
 * In case the tgtStr is too small, the output shall be written to the extend possible, ensuring
 * terminating NULL character.
 *
 * @param tgtStr destination string buffer
 * @param tgtStrSize size of the destination buffer
 * @param separator what to put inbetween the already existing string in `tgtStr` and `srcStr`.
     NULL is interpreted as empty string.
 * @param srcStr source string to append. Interpreted as empty string if NULL.
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL.
 *   - `tgtStr` is too short (also if too short for the trailing `\0` byte).
 */
bool swl_strlst_cat(char* tgtStr, size_t tgtStrSize, const char* separator, const char* srcStr) {
    if((tgtStr == NULL) || (tgtStrSize == 0)) {
        return false;
    }
    if(tgtStr[0] == '\0') {
        return swl_str_copy(tgtStr, tgtStrSize, srcStr);
    } else {
        return
            swl_str_cat(tgtStr, tgtStrSize, separator)
            && swl_str_cat(tgtStr, tgtStrSize, srcStr);
    }
}

/**
 * @brief Concatenate `formatted string` to `tgtStr` with `separator` inbetween if `tgtStr` is nonempty.
 *
 * Example: `swl_strlst_catFormat(tgtStr, 1000, ",", "%u",3)`
 * - If 'tgtStr' was "1,2", this makes `tgtStr` "1,2,3".
 * - If 'tgtStr' was "", this makes `tgtStr` "3".
 *
 * In case the tgtStr is too small, the output shall be written to the extend possible, ensuring
 * terminating NULL character.
 *
 * @param tgtStr destination string buffer
 * @param tgtStrSize size of the destination buffer
 * @param separator what to put inbetween the already existing string in `tgtStr` and `srcStr`.
     NULL is interpreted as empty string.
 * @param format the format used to interpret the following args
 * @param args arguments filled into the format
 * @return true on success, false on failure:
 *   - `tgtStr` is NULL.
 *   - `tgtStr` is too short (also if too short for the trailing `\0` byte).
 *   - `format` is NULL.
 */
bool swl_strlst_catFormat(char* tgtStr, size_t tgtStrSize, const char* separator, const char* format, ...) {
    va_list args;
    int ret = -1;
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_NOT_NULL(format, false, ME, "NULL");
    ASSERT_TRUE(tgtStrSize > 0, false, ME, "null destsize");
    size_t curDestLen = strlen(tgtStr);
    ASSERT_TRUE(tgtStrSize > curDestLen, false, ME, "dest already full or not initialized");

    if(curDestLen != 0) {
        swl_str_cat(tgtStr, tgtStrSize, separator);
        curDestLen = strlen(tgtStr);
    }

    va_start(args, format);
    ret = vsnprintf(&tgtStr[curDestLen], (tgtStrSize - curDestLen), format, args);
    va_end(args);
    ASSERTI_FALSE(ret >= (int) (tgtStrSize - curDestLen), false, ME, "output was truncated");
    return true;
}

/**
 * Whether `hayStack` considered as `sep`-separated list contains `needle`.
 *
 * For simplicity, empty string (`needle` being "") is never considered to be an element.
 *
 * @param hayStack: list represented as a string
 * @param sep: separator used in `hayStack`
 * @param needle: potential element of `hayStack` (when considered as list)
 */
bool swl_strlst_contains(const char* hayStack, const char* sep, const char* needle) {
    ASSERTS_NOT_NULL(hayStack, false, ME, "empty string considered empty list");
    ASSERT_STR(sep, false, ME, "NULL/EMPTY");
    ASSERT_NOT_NULL(needle, false, ME, "NULL/EMPTY");
    ASSERTS_STR(needle, false, ME, "empty needle considered never an element");

    size_t sepLen = strlen(sep);
    size_t needleLen = strlen(needle);
    const char* lookingAt = hayStack;
    while(*lookingAt != '\0') {
        char* sepPtr = strstr(lookingAt, sep);
        if(sepPtr == NULL) {
            // Last element.
            return swl_str_matches(lookingAt, needle);
        }
        size_t candidateLen = sepPtr - lookingAt;
        if((candidateLen == needleLen) && swl_str_nmatches(lookingAt, needle, needleLen)) {
            return true;
        }
        lookingAt = sepPtr + sepLen;
    }
    return false;
}

swl_rc_ne swl_strlst_copyStringAtIndexToBuffer(char* outBuffer, size_t* outBufferLen, const char* string, const char* sep, ssize_t index, bool ignoreEmptySpace) {
    ASSERTS_NOT_NULL(string, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(sep, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(outBuffer, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_EQUALS(*outBufferLen, 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    const char* data = NULL;
    size_t dataLen = 0;
    memset(outBuffer, 0, *outBufferLen);

    if(index == 0) {
        const char* token = strstr(string, sep);
        dataLen = token - string;
        data = string;
    } else {
        int pos = 0;
        const char* token = strstr(string, sep);
        while(token != NULL) {
            char* nextToken = strstr(token + 1, sep);
            if(nextToken != NULL) {
                dataLen = nextToken - token - 1;
            } else {
                dataLen = strlen(token + 1);
            }
            if((dataLen != 0) || !ignoreEmptySpace) {
                pos++;
            }
            if(((index < 0) && ((nextToken == NULL) || (ignoreEmptySpace && (strlen(nextToken + 1) == 0))))
               || ((pos == index) || (nextToken == NULL))) {
                break;
            }
            token = nextToken;
        }
        if((pos == index) || (index < 0)) {
            data = token + 1;
        } else {
            data = NULL;
        }
    }
    ASSERTS_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_EQUALS(dataLen, 0, SWL_RC_OK, ME, "NULL");
    *outBufferLen = dataLen;
    swl_str_ncopy(outBuffer, dataLen + 1, data, dataLen);
    return SWL_RC_OK;
}

/**
 * create a copy of the string at the index'th location of this array of strings, with sep being the separator.
 * If ignoreEmptySpace is true, empty entries in the array will be ignored.
 * Will return NULL if no such element exists.
 *
 * Note that the return value is allocated, so it must be freed.
 */
char* swl_strlst_copyStringAtIndex(const char* string, const char* sep, ssize_t index, bool ignoreEmptySpace) {
    ASSERTS_NOT_NULL(string, NULL, ME, "NULL");
    ASSERTS_NOT_NULL(sep, NULL, ME, "NULL");

    char data[32] = {'\0'};
    size_t dataLen = sizeof(data);
    swl_rc_ne rc = swl_strlst_copyStringAtIndexToBuffer(data, &dataLen, string, sep, index, ignoreEmptySpace);
    ASSERTS_EQUALS(rc, SWL_RC_OK, NULL, ME, "NULL");
    ASSERTS_FALSE(swl_str_isEmpty(data), strdup(""), ME, "NULL");
    ASSERTS_NOT_EQUALS(dataLen, 0, strdup(""), ME, "NULL");
    char* dataStr = calloc(1, dataLen + 1);
    swl_str_ncopy(dataStr, dataLen + 1, data, dataLen);
    return dataStr;
}

/**
 * @brief Count the number of times a given character appears in string.
 *
 * @param srcStr source string
 * @param filterChar the character to count
 * @return uint32_t number of times tgt appears in src
 */
uint32_t swl_str_countChar(const char* srcStr, char filterChar) {
    ASSERT_NOT_NULL(srcStr, 0, ME, "NULL");

    size_t count = 0;
    size_t i = 0;

    while(srcStr[i] != '\0') {
        if(srcStr[i] == filterChar) {
            count++;
        }
        i++;
    }

    return count;
}

/**
 * Checks whether str starts with prefix.
 * Returns true if so, false otherwise.
 *
 * If both str and prefix are NULL, we also return true;
 *
 */
bool swl_str_startsWith(const char* str, const char* prefix) {
    if((str == NULL) || (prefix == NULL)) {
        return (str == NULL) && (prefix == NULL);
    }
    return strncmp(str, prefix, strlen(prefix)) == 0;
}


/**
 * Returns whether two strings match.
 *
 * Returns true if both strings are null, or if both strings point
 * to identical strings, as compared with strcmp.
 * Note that this assumes zero terminated strings!
 */
bool swl_str_matches(const char* str1, const char* str2) {
    if((str1 == NULL) || (str2 == NULL)) {
        return str1 == NULL && str2 == NULL;
    }
    return (0 == strcmp(str1, str2));
}

/**
 * Returns whether two strings in their first size characters.
 *
 * Returns true if both strings are null, or if both strings point
 * to identical strings, as compared with strncmp.
 * Note that this assumes zero terminated strings!
 */
bool swl_str_nmatches(const char* str1, const char* str2, size_t size) {
    if((str1 == NULL) || (str2 == NULL)) {
        return str1 == NULL && str2 == NULL;
    }
    return (0 == strncmp(str1, str2, size));
}

/*
 * Returns the index of the first mismatch between str1 and str2.
 * Returns SWL_RC_ERROR if no such mismatch exists
 * Returns SWL_RC_INVALID_PARAM if one or both strings are NULL;
 */
ssize_t swl_str_getMismatchIndex(const char* str1, const char* str2) {
    if((str1 == NULL) || (str2 == NULL)) {
        return SWL_RC_INVALID_PARAM;
    }
    ssize_t index = 0;
    while(str1[index] == str2[index] && str1[index] != 0 && index >= 0) {
        index++;
    }
    if(index < 0) {
        return SWL_RC_INVALID_PARAM;
    }
    if(str1[index] == str2[index]) {
        return SWL_RC_ERROR;
    } else {
        return index;
    }
}

/*
 * Returns the index of the first mismatch between str1 and str2 in the first size bytes.
 * Returns SWL_RC_ERROR if no such mismatch exists
 * Returns SWL_RC_INVALID_PARAM if one or both strings are NULL, or size is 0;
 */
ssize_t swl_str_ngetMismatchIndex(const char* str1, const char* str2, ssize_t size) {
    if((str1 == NULL) || (str2 == NULL)) {
        return SWL_RC_INVALID_PARAM;
    }
    if(size <= 0) {
        return SWL_RC_INVALID_PARAM;
    }
    ssize_t index = 0;
    while(str1[index] == str2[index] && str1[index] != 0 && index >= 0 && index < size - 1) {
        index++;
    }
    if(str1[index] == str2[index]) {
        return SWL_RC_ERROR;
    } else {
        return index;
    }
}


/**
 * Add the escape character before the all the characters to escape inline in buffer. Shall
 * only overwrite buffer if all escaping can be done, and shall return true in that case, false otherwise
 *
 * It is required that the escapeChar is parts of charsToEscape
 */
bool swl_str_addEscapeChar(char* buffer, uint32_t maxBufSize, const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(buffer, false, ME, "NULL");
    ASSERT_NOT_NULL(charsToEscape, false, ME, "NULL");
    ASSERT_NOT_NULL(strchr(charsToEscape, escapeChar), false, ME, "escapeChar must be in charsToEscape");
    ASSERTI_TRUE(maxBufSize > 0, false, ME, "ZERO SIZE");

    char tmpBuffer[maxBufSize];
    memset(tmpBuffer, 0, maxBufSize);

    char* curPtr = buffer;
    size_t leftover = strlen(buffer);
    size_t toEscape = strcspn(curPtr, charsToEscape);

    bool success = true;

    while(toEscape < leftover && success) {
        char fndChar = curPtr[toEscape];
        curPtr[toEscape] = '\0';
        success &= swl_str_cat(tmpBuffer, maxBufSize, curPtr);
        curPtr[toEscape] = fndChar;

        success &= swl_str_catChar(tmpBuffer, maxBufSize, escapeChar);
        success &= swl_str_catChar(tmpBuffer, maxBufSize, fndChar);
        curPtr += toEscape + 1;
        leftover -= (toEscape + 1);
        toEscape = strcspn(curPtr, charsToEscape);
    }

    if(toEscape != 0) {
        success &= swl_str_cat(tmpBuffer, maxBufSize, curPtr);
    }

    if(success) {
        memcpy(buffer, tmpBuffer, maxBufSize);
    }

    return success;
}

/**
 * Add escape chars to a "printed char".
 * This will return the estimated string size after adding escape.
 * If escape chars don't fit, then ... is printed at the end of bufSize
 */
ssize_t swl_str_addEscapeCharPrint(char* buffer, size_t maxBufSize, size_t totalStrSize, const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(buffer, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(charsToEscape, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(strchr(charsToEscape, escapeChar), SWL_RC_INVALID_PARAM, ME, "escapeChar must be in charsToEscape");
    ASSERTI_TRUE(maxBufSize > 0, 0, ME, "ZERO SIZE");

    size_t tmpBufSize = maxBufSize * 2;

    size_t curStrLen = swl_str_len(buffer);
    if(totalStrSize > curStrLen) {
        curStrLen = totalStrSize;
    }

    char tmpBuffer[tmpBufSize];
    memset(tmpBuffer, 0, tmpBufSize);

    char* curPtr = buffer;
    size_t leftover = strlen(buffer);
    size_t toEscape = strcspn(curPtr, charsToEscape);

    bool success = true;

    while(toEscape < leftover && success) {
        char fndChar = curPtr[toEscape];
        curPtr[toEscape] = '\0';
        success &= swl_str_cat(tmpBuffer, tmpBufSize, curPtr);
        curPtr[toEscape] = fndChar;

        success &= swl_str_catChar(tmpBuffer, tmpBufSize, escapeChar);
        success &= swl_str_catChar(tmpBuffer, tmpBufSize, fndChar);
        curPtr += toEscape + 1;
        leftover -= (toEscape + 1);
        toEscape = strcspn(curPtr, charsToEscape);
        curStrLen++;
    }

    if(toEscape != 0) {
        swl_str_cat(tmpBuffer, maxBufSize, curPtr);
    }

    if(curStrLen >= maxBufSize) {
        swl_str_copy(buffer, maxBufSize - 3, tmpBuffer);
        swl_str_cat(buffer, maxBufSize, "...");
    } else {
        swl_str_copy(buffer, maxBufSize, tmpBuffer);
    }

    return curStrLen;
}

/**
 * Remove all escape chars inline in buffer, and checks that all charsToEscape are actually escaped.
 * It shall only overwrite buffer, if all chars to escape are properly escaped.
 *
 * It is required that the escapeChar is parts of charsToEscape
 */
bool swl_str_removeEscapeChar(char* buffer, uint32_t maxBufSize, const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(buffer, false, ME, "NULL");
    ASSERT_NOT_NULL(charsToEscape, false, ME, "NULL");

    char tmpBuffer[maxBufSize];
    memset(tmpBuffer, 0, maxBufSize);

    char* curPtr = buffer;
    size_t leftover = strlen(buffer);

    char* tgt = strchr(curPtr, escapeChar);

    bool success = true;


    while(tgt != NULL && success) {
        uint32_t sizeToCopy = (tgt - curPtr);
        uint32_t firstEscape = strcspn(curPtr, charsToEscape);

        ASSERT_NOT_EQUALS(sizeToCopy, leftover, false, ME, "last char is escape char");
        ASSERT_NOT_NULL(strchr(charsToEscape, tgt[1]), false, ME, "Char %c not in escape chars %s", tgt[1], charsToEscape);
        ASSERT_TRUE((sizeToCopy <= firstEscape), false, ME, "non escaped char to escape");

        *tgt = '\0';
        success &= swl_str_cat(tmpBuffer, maxBufSize, curPtr);
        *tgt = escapeChar;

        success &= swl_str_catChar(tmpBuffer, maxBufSize, tgt[1]);

        curPtr = &tgt[2];
        leftover = leftover - (sizeToCopy + 2);
        tgt = strchr(curPtr, escapeChar);
    }

    if(leftover != 0) {
        success &= swl_str_cat(tmpBuffer, maxBufSize, curPtr);
    }

    if(success) {
        memcpy(buffer, tmpBuffer, maxBufSize);
    }

    return success;
}

/**
 * Remove all whitespace from the edges of the buffer
 */
void swl_str_removeWhitespaceEdges(char* buffer) {
    size_t size = strlen(buffer);
    size_t startSize = 0;
    size_t endSize = 0;

    for(startSize = 0; startSize < size; startSize++) {
        if(!isspace(buffer[startSize])) {
            break;
        }
    }
    if(startSize == size) {
        buffer[0] = 0;
        return;
    }
    for(endSize = size; endSize > 0; endSize--) {
        if(!isspace(buffer[endSize - 1])) {
            break;
        }
    }
    memmove(buffer, &buffer[startSize], size - startSize);
    buffer[endSize - startSize] = 0;
}

/**
 * Strip a character from the edges of the buffer.
 * Will return true if the edges actually contained the stripChar. If not, false is returned and nothing is changed.
 */
bool swl_str_stripChar(char* buffer, char stripChar) {
    size_t size = strlen(buffer);
    ASSERT_TRUE(size >= 2, false, ME, "SIZE");
    ASSERT_TRUE(buffer[0] == stripChar && buffer[size - 1] == stripChar, false, ME, "INVALID CHAR");

    memmove(buffer, &buffer[1], size - 1);
    buffer[size - 2] = 0;
    return true;
}


/**
 * Return the first location of the target char in buffer, ignoring escaped chars.
 * If target is not in buffer, strLen is returned, which will be the location of the NULL char, if char
 * is properly null terminated.
 */
size_t swl_str_getNonEscCharLoc(const char* buffer, char target, char escape) {
    ASSERT_STR(buffer, 0, ME, "INVALID");
    size_t retSize = 0;

    const char* curPtr = buffer;
    size_t len = strlen(buffer);
    char escapeBuf[3] = {0};
    escapeBuf[0] = target;
    escapeBuf[1] = escape;

    size_t subStrSize = strcspn(curPtr, escapeBuf);

    while(subStrSize + retSize < len && (curPtr[subStrSize] != target)) {

        if(curPtr[subStrSize] == escape) {
            retSize += subStrSize + 2;
        } else {
            retSize += subStrSize + 1;
        }
        curPtr = &buffer[retSize]; // skip escaped char
        subStrSize = strcspn(curPtr, escapeBuf);
    }
    retSize += subStrSize;
    //no limit found, return str len
    return retSize;
}

/**
 * Returns whether two strings match (case insensitive) .
 *
 * Returns true if both strings are null, or if both strings point
 * to identical strings, as compared with strcasecmp.
 * Note that this assumes zero terminated strings!
 */
bool swl_str_matchesIgnoreCase(const char* str1, const char* str2) {
    if((str1 == NULL) || (str2 == NULL)) {
        return str1 == NULL && str2 == NULL;
    }
    return (0 == strcasecmp(str1, str2));
}

/**
 * Returns whether two strings in their first size characters (case insensitive) .
 *
 * Returns true if both strings are null, or if both strings point
 * to identical strings, as compared with strncasecmp.
 * Note that this assumes zero terminated strings!
 */
bool swl_str_nmatchesIgnoreCase(const char* str1, const char* str2, size_t size) {
    if((str1 == NULL) || (str2 == NULL)) {
        return str1 == NULL && str2 == NULL;
    }
    return (0 == strncasecmp(str1, str2, size));
}


/**
 * Replace all occurence of old in srcStr to new.
 * If baseString is NULL, tgtStr will be used. tgtStr and baseString may be the same,
 * or baseString may point to a part of tgtStr.
 *
 * Returns false if tgtStr or srcStr is NULL, or old is NULL or empty string.
 * Will also return false if the string does not fit in buffer.
 * Returns true otherwise.
 */
bool swl_str_replace(char* tgtStr, size_t tgtStrSize, const char* srcStr, const char* old, const char* new) {
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_STR(old, false, ME, "NULL");
    ASSERT_NOT_NULL(new, false, ME, "NULL");

    if(swl_str_matches(old, new)) {
        if((srcStr != NULL) && (srcStr != tgtStr)) {
            swl_str_copy(tgtStr, tgtStrSize, srcStr);
        }
        return true;
    }

    uint32_t size = 0;
    if(srcStr != NULL) {
        size = strlen(srcStr) + 1;
    } else {
        size = strlen(tgtStr) + 1;
    }

    char buffer[size];
    memset(buffer, 0, size);
    if(srcStr != NULL) {
        swl_str_copy(buffer, size, srcStr);
    } else {
        swl_str_copy(buffer, size, tgtStr);
    }

    memset(tgtStr, 0, tgtStrSize);

    size_t toReplaceLen = strlen(old);
    size_t replaceTgtLen = strlen(new);

    size_t index = 0;
    char* tgt = strstr(&buffer[index], old);
    size_t writeLoc = 0;

    while(tgt != NULL && writeLoc < tgtStrSize) {
        size_t offset = tgt - &buffer[0];
        size_t len = offset - index;
        size_t writeLen = SWL_MIN(tgtStrSize - writeLoc - 1, len);
        memcpy(&tgtStr[writeLoc], &buffer[index], writeLen);
        writeLoc += writeLen;
        writeLen = SWL_MIN(tgtStrSize - writeLoc - 1, replaceTgtLen);
        memcpy(&tgtStr[writeLoc], new, writeLen);
        index += len + toReplaceLen;
        writeLoc += replaceTgtLen;
        tgt = strstr(&buffer[index], old);
    }
    size_t len = size - index;
    size_t writeLen = SWL_MIN(tgtStrSize - writeLoc - 1, len);
    memcpy(&tgtStr[writeLoc], &buffer[index], writeLen);
    writeLoc += writeLen;
    bool success = (writeLoc < tgtStrSize);
    if(success) {
        tgtStr[writeLoc] = '\0';
    } else {
        tgtStr[tgtStrSize - 1] = '\0';
    }
    return success;
}

bool swl_str_isEmpty(const char* str) {
    if((str == NULL) || swl_str_matches(str, "")) {
        return true;
    }
    return false;
}

/**
 * Returns the length of printable byte sequence
 * with option for white-space characters ('space', '\f', '\n', '\r' , '\t', '\v')
 * printable byte sequence is valid, if matching one of these conditions:
 * - the sequence must end with null char (or white-space characters, if not included)
 * - or the sequence fits the whole buffer size
 * otherwise 0 is returned.
 */
uint32_t swl_str_printableByteSequenceLen(const char* buffer, size_t size, bool withSpaces) {
    ASSERTS_NOT_NULL(buffer, 0, ME, "NULL");
    ASSERTS_TRUE(size > 0, 0, ME, "NULL");
    for(uint32_t index = 0; index < size; index++) {
        if((isgraph(buffer[index])) || (withSpaces && isspace(buffer[index]))) {
            if(index == (size - 1)) {
                return size;
            }
            continue;
        }
        if((buffer[index] == 0) || (!withSpaces && isspace(buffer[index]))) {
            return index;
        }
        break;
    }
    return 0;
}

/**
 * Returns the number of occurrences of a char into a byte buffer:
 * - If continuous is set, we only count the number of occurrences in the first group of that char.
 * - Else, all occurrences of the char in string are counted.
 */
uint32_t swl_str_nrCharOccurances(char tgt, const char* buffer, size_t size, bool continuous) {
    ASSERTS_NOT_NULL(buffer, 0, ME, "NULL");
    ASSERTS_TRUE(size > 0, 0, ME, "NULL");
    int32_t count = 0;
    for(uint32_t index = 0; index < size; index++) {
        if(buffer[index] == tgt) {
            count++;
        } else if(count && continuous) {
            break;
        }
    }
    return count;
}

/*
 * Dump a byte data buffer into a string buffer, following this format:
 * - printable byte sequence (null terminated or ending at the buffer edge) printed as a string
 * - escaping  printable chars '\' '#' and '/' using '\'
 * - all other bytes (not-printable, short printable sequence) printed as dual hex digits
 * - duplication count of non-printable byte sequence "/00#n"
 *   (sequence larger than 2, is compound to first byte, followed by count)
 * - separator "//" between bytes (w/o count) and printable byte sequences
 * @param dataBuffer source data buffer
 * @param dataSize size of the source buffer
 * @param dumpBuf destination string buffer.
 * @param dumpBufSize size of the destination string buffer
 * @param minPrintable option to set the minimum number of successive printable bytes, that will be dumped as chars instead of hex value.
 *        possible values are:
 *        - setting it to -1 would always print hex code
 *        - setting it to 0 would always print a character if printable
 *        - setting it to greater non null positive values, would display printable bytes, only if not followed by termination char:
 *        (i.e null char, buffer end, white-space char if not excluded from printable sequence)
 * @param showSpaces option to print white-space chars (to allow or not, breaking sequence display with 'space', '\n', '\r', ...)
 * @param overflowString option: if buffer is too small (i.e. would overflow), the last bytes will be replaced by overflowString.
 * @return true on success, false on failure
 *   - `dumpBuf` buffer too short (i.e not fitting all formatted source bytes).
 *   - `dataBuffer` is NULL.
 *   - `dumpBuf` is NULL.
 */
bool swl_str_dumpByteBuffer(char* dumpBuf, size_t dumpBufSize, const char* dataBuffer, size_t dataSize, int32_t minPrintable, bool showSpaces, const char* overflowString) {
    ASSERTS_NOT_NULL(dumpBuf, false, ME, "NULL");
    ASSERTS_TRUE(dumpBufSize > 0, false, ME, "Empty display buffer");
    memset(dumpBuf, 0, dumpBufSize);
    ASSERTS_NOT_NULL(dataBuffer, false, ME, "NULL");
    ASSERTS_TRUE(dataSize > 0, true, ME, "Empty data");
    bool ret = false;
    for(uint32_t index = 0; index < dataSize; index++) {
        uint32_t printableSeqLen = 0;
        //control mixed display (char, hex)
        if(minPrintable > 0) {
            printableSeqLen = swl_str_printableByteSequenceLen(&dataBuffer[index], (dataSize - index), showSpaces);
        } else if(minPrintable == 0) {
            printableSeqLen = swl_str_printableByteSequenceLen(&dataBuffer[index], 1, showSpaces);
        }
        //avoid displaying as printable string, a short fake char sequence
        //but dump it as hex bytes
        if((printableSeqLen > 0) && ((int32_t) printableSeqLen >= minPrintable)) {
            char printableSeq[(printableSeqLen * 2) + 1];
            memset(printableSeq, 0, sizeof(printableSeq));
            swl_str_ncopy(printableSeq, sizeof(printableSeq), &dataBuffer[index], printableSeqLen);
            swl_str_addEscapeChar(printableSeq, sizeof(printableSeq), "\\/#", '\\');
            //separate not printable and printable bytes with a double slash
            if((index > 0) && (swl_str_printableByteSequenceLen(&dataBuffer[index - 1], 1, showSpaces) == 0)) {
                swl_str_cat(dumpBuf, dumpBufSize, "//");
            }
            ret = swl_str_cat(dumpBuf, dumpBufSize, printableSeq);
            if(!ret) {
                break;
            }
            index += printableSeqLen;
            //always display the ending null char as a hex byte
            if(index <= (dataSize - 1)) {
                index -= 1;
            }
            continue;
        }
        ret = swl_strlst_catFormat(dumpBuf, dumpBufSize, "", "/%.2x", (swl_bit8_t) dataBuffer[index]);
        if(!ret) {
            break;
        }
        //count duplicated bytes , for a compact display
        uint32_t dupByteSeqLen = swl_str_nrCharOccurances(dataBuffer[index], &dataBuffer[index], (dataSize - index), true);
        //only compound more than 2 duplicated bytes
        if(dupByteSeqLen > 2) {
            ret = swl_strlst_catFormat(dumpBuf, dumpBufSize, "", "#%d", dupByteSeqLen);
            if(!ret) {
                break;
            }
            index += (dupByteSeqLen - 1);
            continue;
        }
    }
    if(!ret && overflowString && overflowString[0]) {
        size_t overflowStringSize = strlen(overflowString) + 1;
        if(dumpBufSize >= overflowStringSize) {
            swl_str_copy(&dumpBuf[dumpBufSize - overflowStringSize], overflowStringSize, overflowString);
        }
    }
    return ret;
}

/**
 * Return size safe length for an array of characters (not a string !)
 * it will return the number of characters before the first '\0' character, up to
 * a maximum of maxLen characters.
 *
 * Note that this functionality is identical to strnlen, but strnlen is currently
 * not available on all target toolchains, so our own function is needed. Also strnlen
 * is not NULL safe, while this function is.
 *
 */
size_t swl_str_nlen(const char* str, size_t maxLen) {
    if(str == NULL) {
        return 0;
    }
    for(size_t i = 0; i < maxLen; i++) {
        if(str[i] == '\0') {
            return i;
        }
    }
    return maxLen;
}

/**
 * Return size safe length for a string
 * it will return the number of characters before the first '\0' character.
 *
 * Note that this functionality is identical to strlen, but is null pointer safe
 * If str is NULL, it will return 0 length
 *
 */
size_t swl_str_len(const char* str) {
    if(str == NULL) {
        return 0;
    }
    return strlen(str);
}

/**
 * Performs string pointer assignment with safety check against target being NULL.
 *
 * - If `tgtStr` is NULL, does nothing
 * - Otherwise, makes the pointer that `tgtStr` points to, point to the same string as that
 *   `srcStr` points to. If `srcStr` is NULL, the pointer that tgtStr points to will also be NULL.
 */
void swl_str_safeAssign(const char** tgtStr, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtStr, , ME, "NULL");
    *tgtStr = srcStr;
}

/**
 * @brief returns a param value in a config buffer
 *
 * @param paramName: the param to look for its value in src buffer
 * @param srcStr: the source buffer is composed of many parameters having the format: param=val\n
 * @param tgtStr: the buffer containing the param value
 * @param tgtStrSize: the size of the tgtStr buffer
 * @param tgtStrSize: the size of the tgtStr buffer
 * @param paramNamePos: optional, considered only if not NULL; equal to the paramName position in srcStr
 * If the paramName exists then paramNamePos will be set to that position.
 * This will happen even when inres is false because paramName does not have a value.
 *
 * @return true on success, false on failure
 *   - tgtStr buffer too short (also if too short for the trailing `\0`).
 *   - paramName, srcStr, tgtStr, paramNamePos is NULL.
 *   - paramName is not found
 *   - paramName having not a value
 */
bool swl_str_getParameterValue(char* tgtStr, size_t tgtStrSize, const char* srcStr, const char* paramName, char** paramNamePos) {
    SAH_TRACEZ_IN(ME);
    // Check valid input
    ASSERT_NOT_NULL(paramName, false, ME, "NULL");
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    char* startString;
    char* endString;
    char* equalString;

    W_SWL_SETPTR(paramNamePos, NULL);

    // Find the paramName string in src buf
    startString = strstr(srcStr, paramName);
    while(startString) {
        if((startString > srcStr) && (*(startString - 1) != '\n')) {
            startString = strstr(startString + 1, paramName);
            continue;
        }

        equalString = strchr(startString, '=');
        endString = strchr(startString, '\n');

        // case param is present at end, but no value
        if(((endString == NULL) && (equalString == NULL))) {
            if(!swl_str_matches(paramName, startString)) {
                SAH_TRACEZ_OUT(ME);
                return false;
            }
            W_SWL_SETPTR(paramNamePos, startString);
            SAH_TRACEZ_OUT(ME);
            return false;
        }

        // case param is present in middle, but no value
        if((endString != NULL) && ((endString < equalString) || (equalString == NULL))) {
            if(!swl_str_nmatches(paramName, startString, (size_t) (endString - startString))) {
                startString = strstr(endString, paramName);
                continue;
            }
            W_SWL_SETPTR(paramNamePos, startString);
            SAH_TRACEZ_OUT(ME);
            return false;
        }

        if(equalString == NULL) {
            startString = NULL;
            continue;
        }

        // default case: check if param matches start
        if(!swl_str_nmatches(paramName, startString, (size_t) (equalString - startString))) {
            startString = endString != NULL ? strstr(endString, paramName) : NULL;
            continue;
        }

        W_SWL_SETPTR(paramNamePos, startString);
        size_t sizeToCopy = (endString != NULL ? (size_t) (endString - equalString) : strlen(equalString)) - 1;
        size_t sizeCanCopy = SWL_MIN(tgtStrSize - 1, sizeToCopy);
        // Copy all we're allowed in tgtStr buffer}
        strncpy(tgtStr, equalString + 1, sizeCanCopy);
        tgtStr[sizeCanCopy] = '\0';
        SAH_TRACEZ_OUT(ME);
        return (sizeCanCopy >= sizeToCopy);
    }
    SAH_TRACEZ_OUT(ME);
    return false;
}

/**
 * try to find the needle in the haystack
 * Will return negative number if either of the haystack or needle is NULL, or needle does not occur in haystack.
 * Otherwise, returns retVal so that swl_str_startsWith(&hayStack[retVal],needle) is true.
 * It will return the smallest positive integer for which this condition is true.
 */
ssize_t swl_str_find(const char* haystack, const char* needle) {
    if((haystack == NULL) || (needle == NULL)) {
        return -2;
    }
    const char* index = strstr(haystack, needle);
    if(index != NULL) {
        return index - haystack;
    }
    return -1;
}

ssize_t swl_str_findChar(const char* haystack, char needle) {
    if(haystack == NULL) {
        return -2;
    }
    const char* index = strchr(haystack, needle);
    if(index != NULL) {
        return index - haystack;
    }
    return -1;
}

/**
 * try to find the needle in the haystack, where there is no escape char before the needle
 * Will return negative number if either of the haystack or needle is NULL, or needle without escape char before it does not occur in haystack.
 * Otherwise, returns retVal so that swl_str_startsWith(&hayStack[retVal],needle) is true.
 * It will return the smallest positive integer for which this condition is true.
 */
ssize_t swl_str_findNoEscape(const char* haystack, const char* needle, char escape) {
    ssize_t retVal = swl_str_find(haystack, needle);

    if(retVal <= 0) {
        return retVal;
    }
    ssize_t escapeTgt = swl_str_findChar(haystack, escape);
    ssize_t baseIndex = 0;

    while(escapeTgt >= 0 && escapeTgt < retVal && retVal >= 0) {
        baseIndex += escapeTgt + 2;

        retVal = swl_str_find(&haystack[baseIndex], needle);
        escapeTgt = swl_str_findChar(&haystack[baseIndex], escape);
        if(retVal < 0) {
            return retVal;
        }
    }


    return retVal + baseIndex;
}

/**
 * Return whether the first len element of str are only whitespace, or if
 * null char is encountered;
 */
bool swl_str_isOnlyWhitespace(const char* str, size_t len) {
    for(size_t i = 0; i < len; i++) {
        if(str[i] == 0) {
            return true;
        }
        if(!isspace(str[i])) {
            return false;
        }
    }
    return true;

}

/**
 * Returns the amount of characters on which two character arrays differ, over the given length.
 */
size_t swl_str_nrStrDiff(const char* test1, const char* test2, size_t len) {
    ASSERTS_FALSE(swl_str_nmatches(test1, test2, len), 0, ME, "same");
    ASSERTS_STR(test1, swl_str_nlen(test2, len), ME, "Empty")
    ASSERTS_STR(test2, swl_str_nlen(test1, len), ME, "Empty")
    size_t diff = 0;
    for(size_t i = 0; i < len; i++) {
        if(*test1 != *test2) {
            if(*test1 == 0) {
                diff += swl_str_nlen(test2, len - i);
                break;
            }
            if(*test2 == 0) {
                diff += swl_str_nlen(test1, len - i);
                break;
            }
            diff++;
        } else if(*test1 == 0) {
            break;
        }
        test1++;
        test2++;
    }

    return diff;
}

