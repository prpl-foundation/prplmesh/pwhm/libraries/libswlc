/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_assert.h"
#include "swl/swl_common_table.h"
#include "swl/swl_security.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_string.h"

#define ME "swlSec"

const char* swl_security_apMode_str[] = {
    "Unknown",
    "None",
    "WEP-64",
    "WEP-128",
    "WEP-128iv",
    "WPA-Personal",
    "WPA2-Personal",
    "WPA-WPA2-Personal",
    "WPA3-Personal",
    "WPA3-Personal-Transition",
    "WPA-Enterprise",
    "WPA2-Enterprise",
    "WPA-WPA2-Enterprise",
    "WPA3-Enterprise",
    "WPA3-Enterprise-Transition",
    "OWE",
    "Auto",
    "Unsupported"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_apMode_str) == SWL_SECURITY_APMODE_MAX, "swl_security_apMode_str not correctly defined");

const char* swl_security_apModeAlt_str[] = {
    "Unknown",
    "None",
    "WEP-64",
    "WEP-128",
    "WEP-128iv",
    "WPA-Personal",
    "WPA2-Personal",
    "WPA-WPA2-Personal",
    "WPA3-SAE",
    "WPA2-PSK-WPA3-SAE",
    "WPA-Enterprise",
    "WPA2-Enterprise",
    "WPA-WPA2-Enterprise",
    "WPA3-Enterprise",
    "WPA2-WPA3-Enterprise",
    "OWE",
    "Auto",
    "Unsupported"
};

const char* swl_security_diagMode_str[] = {
    "Unknown",
    "None",
    "WEP",
    "WEP",
    "WEP",
    "WPA",
    "WPA2",
    "WPA-WPA2",
    "WPA3-SAE",
    "WPA2-PSK-WPA3-SAE",
    "WPA-Enterprise",
    "WPA2-Enterprise",
    "WPA-WPA2-Enterprise",
    "WPA3-Enterprise",
    "WPA2-WPA3-Enterprise",
    "OWE",
    "Auto",
    "Unsupported"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_diagMode_str) == SWL_SECURITY_APMODE_MAX, "swl_security_apModeAlt2_str not correctly defined");


const char* swl_security_apModeLeg_str[] = {
    "Unknown",
    "None",
    "WEP-64",
    "WEP-128",
    "WEP-128iv",
    "WPA-Personal",
    "WPA2-Personal",
    "WPA-WPA2-Personal",
    "WPA3-Personal",
    "WPA2-WPA3-Personal",
    "WPA-Enterprise",
    "WPA2-Enterprise",
    "WPA-WPA2-Enterprise",
    "WPA3-Enterprise",
    "WPA2-WPA3-Enterprise",
    "OWE",
    "Auto",
    "Unsupported"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_apModeLeg_str) == SWL_SECURITY_APMODE_MAX, "swl_security_apModeLeg_str not correctly defined");


const char* swl_security_conMode_str[] = {
    "Unknown",
    "None",
    "WEP-64",
    "WEP-128",
    "WEP-128iv",
    "WPA-Personal",
    "WPA2-Personal",
    "WPA3-Personal",
    "WPA-Enterprise",
    "WPA2-Enterprise",
    "WPA3-Enterprise",
    "OWE"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_conMode_str) == SWL_SECURITY_CONMODE_MAX, "swl_security_conMode_str not correctly defined");

const swl_security_conMode_m swl_security_apModeToConModeMask[] = {
    M_SWL_SECURITY_CONMODE_UNKNOWN,
    M_SWL_SECURITY_CONMODE_NONE,
    M_SWL_SECURITY_CONMODE_WEP64,
    M_SWL_SECURITY_CONMODE_WEP128,
    M_SWL_SECURITY_CONMODE_WEP128IV,
    M_SWL_SECURITY_CONMODE_WPA_P,
    M_SWL_SECURITY_CONMODE_WPA2_P,
    M_SWL_SECURITY_CONMODE_WPA_P | M_SWL_SECURITY_CONMODE_WPA2_P,
    M_SWL_SECURITY_CONMODE_WPA3_P,
    M_SWL_SECURITY_CONMODE_WPA2_P | M_SWL_SECURITY_CONMODE_WPA3_P,
    M_SWL_SECURITY_CONMODE_WPA_E,
    M_SWL_SECURITY_CONMODE_WPA2_E,
    M_SWL_SECURITY_CONMODE_WPA_E | M_SWL_SECURITY_CONMODE_WPA2_E,
    M_SWL_SECURITY_CONMODE_WPA3_E,
    M_SWL_SECURITY_CONMODE_WPA2_E | M_SWL_SECURITY_CONMODE_WPA3_E,
    M_SWL_SECURITY_CONMODE_OWE,
    M_SWL_SECURITY_CONMODE_UNKNOWN,
    M_SWL_SECURITY_CONMODE_UNKNOWN
};

const char* swl_security_encMode_str[] = {"Default", "AES", "TKIP", "TKIP-AES"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_encMode_str) == SWL_SECURITY_ENCMODE_MAX, "swl_security_encMode_str not correctly defined");

const char* swl_security_mfpMode_str[SWL_SECURITY_MFPMODE_MAX] = {"Disabled", "Optional", "Required"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_security_mfpMode_str) == SWL_SECURITY_MFPMODE_MAX, "swl_security_mfpMode_str not correctly defined");


SWL_TABLE(alternateSecurityModes,
          ARR(swl_security_apMode_e mode; char* name;  swl_security_apModeFmt_e format; ),
          ARR(swl_type_uint32, swl_type_charPtr, swl_type_uint32),
          ARR({SWL_SECURITY_APMODE_WPA2_WPA3_P, "WPA2-PSK-WPA3-SAE", SWL_SECURITY_APMODEFMT_ALTERNATE},
              {SWL_SECURITY_APMODE_WPA2_WPA3_P, "WPA2-WPA3-Personal", SWL_SECURITY_APMODEFMT_LEGACY},
              {SWL_SECURITY_APMODE_WPA2_WPA3_E, "WPA2-WPA3-Enterprise", SWL_SECURITY_APMODEFMT_LEGACY},
              {SWL_SECURITY_APMODE_WPA3_P, "WPA3-SAE", SWL_SECURITY_APMODEFMT_ALTERNATE})
          );

/**
 * Convert a string to an AP mode.
 * This will try all different possible formats available Device.WiFi.AccessPoint.{i}.Security to go to a security mode.
 */
swl_security_apMode_e swl_security_apModeFromString(char* srcStr) {
    swl_security_apMode_e tgtMode = swl_conv_charToEnum(srcStr, swl_security_apMode_str, SWL_SECURITY_APMODE_MAX, SWL_SECURITY_APMODE_UNKNOWN);
    if(tgtMode == SWL_SECURITY_APMODE_UNKNOWN) {
        swl_security_apMode_e* ref = (swl_security_apMode_e*) swl_table_getMatchingValue(&alternateSecurityModes, 0, 1, srcStr);
        if(ref != NULL) {
            tgtMode = *ref;
        }
    }
    return tgtMode;
}

/**
 * Encode an ap security mode to a string based on a given format.
 */
const char* swl_security_apModeToString(swl_security_apMode_e srcMode, swl_security_apModeFmt_e fmt) {
    alternateSecurityModesStruct_t testVal = {.mode = srcMode, .format = fmt};
    const char* test = swl_table_getMatchingValueByTuple(&alternateSecurityModes, 1, &testVal, 1 | (1 << 2), 0);
    if(test != NULL) {
        return test;
    }

    return swl_security_apMode_str[srcMode];
}

/**
 * @brief check whether security mode Id is one of known and valid
 * as defined in usp tr-181-2-15-1 Device.WiFi.AccessPoint.{i}.Security.ModesSupported enum strings
 * It shall be one of: Open, WEPx, WPAx, WPAx-Transition, OWE ,
 * for Personal and Enterprise(802.1x) cases
 *
 * @param mode security mode Id
 *
 * @return true when mode is valid
 */
bool swl_security_isApModeValid(swl_security_apMode_e mode) {
    return ((mode >= SWL_SECURITY_APMODE_NONE) && (mode < SWL_SECURITY_APMODE_AUTO));
}

/**
 * @brief extended function to convert a bitmask of security modes enums to comma separated list of security mode names
 * considering a printing format
 *
 * @param buffer output buffer
 * @param bufferSize output buffer size
 * @param fmt names printing format
 * @param mask input bitmask of mode enums
 * @param onlyValidModes flag to select whether printing only matched and valid security modes
 *
 * @return true when conversion is successful, false otherwise
 */
bool swl_security_apModeMaskToStringExt(char* buffer, size_t bufferSize, swl_security_apModeFmt_e fmt, swl_security_apMode_m mask, bool onlyValidModes) {
    ASSERT_NOT_NULL(buffer, false, ME, "NULL");
    ASSERT_TRUE(bufferSize > 0, false, ME, "Empty");
    buffer[0] = 0;
    size_t minId = SWL_SECURITY_APMODE_UNKNOWN;
    size_t maxId = SWL_MIN(SWL_BIT_SIZE(mask), (size_t) SWL_SECURITY_APMODE_MAX);
    for(size_t i = minId; i < maxId; i++) {
        if(SWL_BIT_IS_SET(mask, i)) {
            swl_security_apMode_e apModeId = (swl_security_apMode_e) i;
            if(onlyValidModes && !swl_security_isApModeValid(apModeId)) {
                continue;
            }
            const char* apModeName = swl_security_apModeToString(apModeId, fmt);
            swl_strlst_cat(buffer, bufferSize, ",", apModeName);
        }
    }
    return true;
}

/**
 * @brief function to convert valid security modes bitmask to comma separated list of security mode names
 * considering a printing format
 *
 * @param buffer output buffer
 * @param bufferSize output buffer size
 * @param fmt names printing format
 * @param mask input bitmask of mode enums
 *
 * @return true when conversion is successful, false otherwise
 */
bool swl_security_apModeMaskToString(char* buffer, size_t bufferSize, swl_security_apModeFmt_e fmt, swl_security_apMode_m mask) {
    return swl_security_apModeMaskToStringExt(buffer, bufferSize, fmt, mask, true);
}

/**
 * @brief function to convert comma separated list of security mode names to enums bitmask
 * trying to match alternate then legacy name format.
 *
 * @param buffer input buffer
 * @param onlyValidModes flag to only match valid security mode names
 *
 * @return bitmask of security modes
 */
swl_security_apMode_m swl_security_apModeMaskFromStringExt(const char* buffer, bool onlyValidModes) {
    swl_security_apMode_m modes = 0;
    char* tokens[128] = {0};
    size_t nElt = swl_type_arrayFromChar(swl_type_charPtr, tokens, SWL_ARRAY_SIZE(tokens), buffer);
    for(size_t i = 0; i < nElt; i++) {
        swl_security_apMode_e modeId = swl_security_apModeFromString(tokens[i]);
        if((!onlyValidModes) || (swl_security_isApModeValid(modeId))) {
            W_SWL_BIT_SET(modes, modeId);
        }
        free(tokens[i]);
    }
    if((!modes) && (!onlyValidModes)) {
        W_SWL_BIT_SET(modes, SWL_SECURITY_APMODE_UNKNOWN);
    }
    return modes;
}

/**
 * @brief function to convert comma separated list of valid security mode names to enums bitmask
 * trying to match alternate then legacy name format.
 *
 * @param buffer input buffer
 *
 * @return bitmask of security modes, null when no valid security mode names is included in input string
 */
swl_security_apMode_m swl_security_apModeMaskFromString(const char* buffer) {
    return swl_security_apModeMaskFromStringExt(buffer, true);
}

/**
 * Check whether an ap supports at least one station mode.
 */
bool swl_security_staMatchesAp(swl_security_conMode_m supportedModes, swl_security_apMode_e apMode) {
    ASSERT_TRUE(supportedModes < (1 << SWL_SECURITY_CONMODE_MAX), false, ME, "OOB");
    ASSERT_TRUE(apMode < SWL_SECURITY_APMODE_MAX, false, ME, "OOB");
    swl_security_conMode_m apModes = swl_security_apModeToConModeMask[apMode];

    return (supportedModes & apModes) != 0;
}

swl_rc_ne swl_security_getMode(swl_security_apMode_e* pSecModeEnabled, swl_80211_cipher_m* cipherSuiteParams, swl_80211_akm_m* akmSuiteParams) {
    ASSERT_NOT_NULL(pSecModeEnabled, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(cipherSuiteParams, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(akmSuiteParams, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_rc_ne rc = SWL_RC_OK;

    bool isWPA3 = (*akmSuiteParams & M_SWL_80211_AKM_FT_SAE) || (*akmSuiteParams & M_SWL_80211_AKM_SAE);
    bool isWPA2 = ((*akmSuiteParams & M_SWL_80211_AKM_PSK) || (*akmSuiteParams & M_SWL_80211_AKM_PSK_SHA_256)) && ((*cipherSuiteParams & M_SWL_80211_CIPHER_CCMP) || (*cipherSuiteParams & M_SWL_80211_CIPHER_CCMP256));
    bool isWPA = ((*akmSuiteParams & M_SWL_80211_AKM_PSK) || (*akmSuiteParams & M_SWL_80211_AKM_PSK_SHA_256)) && (*cipherSuiteParams & M_SWL_80211_CIPHER_TKIP);


    // MixedMode management
    if(isWPA3 && isWPA2) {
        *pSecModeEnabled = SWL_SECURITY_APMODE_WPA2_WPA3_P;

    } else if(isWPA2 && isWPA) {
        *pSecModeEnabled = SWL_SECURITY_APMODE_WPA_WPA2_P;

    } else if(isWPA3) {
        *pSecModeEnabled = SWL_SECURITY_APMODE_WPA3_P;

    } else if(isWPA2) {
        *pSecModeEnabled = SWL_SECURITY_APMODE_WPA2_P;

    } else if(isWPA) {
        *pSecModeEnabled = SWL_SECURITY_APMODE_WPA_P;
    } else {

        if(*cipherSuiteParams & M_SWL_80211_CIPHER_WEP40) {
            *pSecModeEnabled = SWL_SECURITY_APMODE_WEP64;
        }

        if(*cipherSuiteParams & M_SWL_80211_CIPHER_WEP104) {
            *pSecModeEnabled = SWL_SECURITY_APMODE_WEP128;
        }
    }

    SAH_TRACEZ_INFO(ME, "Security mode enabled: %d", *pSecModeEnabled);

    return rc;
}

/**
 * @brief isModeWEP
 *
 * Check if given mode is one of the WEP modes.
 *
 * @param mode The security mode.
 * @return true if the mode is a WEP mode, false otherwise.
 */
bool swl_security_isApModeWEP(swl_security_apMode_e mode) {
    return SWL_BIT_IS_SET(M_SWL_SECURITY_APMODE_WEP_ALL_TYPES, mode);
}

/**
 * @brief isModeWPAPersonal
 *
 * Check if given mode is one of the WPA personal modes.
 *
 * @param mode The security mode.
 * @return true if the mode is a WPA personal mode, false otherwise.
 */
bool swl_security_isApModeWPAPersonal(swl_security_apMode_e mode) {
    return SWL_BIT_IS_SET(M_SWL_SECURITY_APMODE_WPA_ALL_PERSONAL_TYPES, mode);
}

bool swl_security_isApModeWPA3Personal(swl_security_apMode_e mode) {
    return SWL_BIT_IS_SET(M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES, mode);
}

/**
 * @brief Convert a string to an MFP mode.
 *
 * @param buffer input string
 *
 * @return MFP mode, or default SWL_SECURITY_MFPMODE_DISABLED in case of error
 */
swl_security_mfpMode_e swl_security_mfpModeFromString(const char* buffer) {
    return swl_conv_charToEnum(buffer, swl_security_mfpMode_str, SWL_SECURITY_MFPMODE_MAX, SWL_SECURITY_MFPMODE_DISABLED);
}

/**
 * @brief Convert an MFP mode to string
 *
 * @param srcMode input mfp mode
 *
 * @return MFP mode name, or default "Disabled" if input value is invalid
 */
const char* swl_security_mfpModeToString(swl_security_mfpMode_e srcMode) {
    ASSERT_TRUE(srcMode < SWL_SECURITY_MFPMODE_MAX, swl_security_mfpMode_str[SWL_SECURITY_MFPMODE_DISABLED], ME, "invalid value(%d), assume disabled", srcMode);
    return swl_security_mfpMode_str[srcMode];
}

/**
 * get the target MFPMode based on configured mfp mode, and enabled security mode
 */
swl_security_mfpMode_e swl_security_getTargetMfpMode(swl_security_apMode_e securityMode, swl_security_mfpMode_e mfpConfig) {
    ASSERTS_NOT_EQUALS(mfpConfig, SWL_SECURITY_MFPMODE_REQUIRED, mfpConfig, ME, "MFP already required");
    ASSERTS_NOT_EQUALS(securityMode, SWL_SECURITY_APMODE_WPA3_P, SWL_SECURITY_MFPMODE_REQUIRED, ME, "MFP required for WPA3");
    ASSERTS_NOT_EQUALS(securityMode, SWL_SECURITY_APMODE_OWE, SWL_SECURITY_MFPMODE_REQUIRED, ME, "MFP required for OWE");
    ASSERTS_NOT_EQUALS(securityMode, SWL_SECURITY_APMODE_WPA2_WPA3_P, SWL_SECURITY_MFPMODE_OPTIONAL, ME, "MFP optional for WPA3 transition mode");
    return mfpConfig;
}

