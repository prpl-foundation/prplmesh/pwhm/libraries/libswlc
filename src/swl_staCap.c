/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_staCap.h"
#include "swl/swl_common.h"

const char* const swl_staCapStdNot_str[] = {"80211k", "80211v", "80211r", "80211u", "80211w"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapStdNot_str) == SWL_STACAP_MAX, "swl_staCapStdNot_str not correctly defined");
const char* const swl_staCap_str[] = {"RRM", "BTM", "FBT", "QOS_MAP", "PMF"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCap_str) == SWL_STACAP_MAX, "swl_staCap_str not correctly defined");

const char* const swl_staCapVendor_str[] = {"MS-WPS", "WFA-MBO"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapVendor_str) == SWL_STACAP_VENDOR_MAX, "swl_staCapVendor_str not correctly defined");

const char* swl_staCapHt_str[] = {"40MHz", "SGI20", "SGI40", "40MHz-INTOL"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapHt_str) == SWL_STACAP_HT_MAX, "swl_staCapHt_str not correctly defined");

const char* swl_staCapVht_str[] = {"SGI80", "SGI160", "SU-BFR", "SU-BFE", "MU-BFR", "MU-BFE"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapVht_str) == SWL_STACAP_VHT_MAX, "swl_staCapVht_str not correctly defined");

const char* swl_staCapHe_str[] = {"SU-BFR", "SU&MU-BFE", "MU-BFR", "TWT_REQ", "TWT_RESP", "TWT_BCAST", "TWT_SCHED"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapHe_str) == SWL_STACAP_HE_MAX, "swl_staCapHe_str not correctly defined");

const char* swl_staCapRrm_str[] = {"LINK", "NEIGHBOR", "PRALLAL", "REPEATED", "BEACON_ACTIVE", "BEACON_PASSIVE", "BEACON_TABLE",
    "BEACON_COND", "FRAME", "CHANNEL_LOAD", "NOISE_HISTOGRAM", "STATS", "LCI", "LCI_AZIMUTH",
    "TSC", "TRSC", "AP_CHANNEL", "MIB", "PILOT", "TSF_OFFSET", "RCPI", "RSNI", "ACCESS_DELAY",
    "ADMISS_CAPACITY", "ANTENNA", "FTM", "CIVIC_LOCATION", "IDENT_LOCATION"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapRrm_str) == SWL_STACAP_RRM_MAX, "swl_staCapRrm_str not correctly defined");

const char* swl_staCapEht_str[] = {"320MHZ", "SU-BFR", "SU-BFE", "MU-BFR-80", "MU-BFR-160", "MU-BFR-320"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapEht_str) == SWL_STACAP_EHT_MAX, "swl_staCapEht_str not correctly defined");

const char* swl_staCapEhtEml_str[] = {"EMLSR", "EMLMR"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_staCapEhtEml_str) == SWL_STACAP_EHT_EML_MAX, "swl_staCapEhtEml_str not correctly defined");
