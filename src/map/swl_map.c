/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_map.h"
#include "swl/swl_string.h"
#include "debug/sahtrace.h"
#include "swl/swl_unLiList.h"
#include "swl/swl_common_tupleType.h"

#define ME "swlMap"

#define SWL_MAP_CHARS_TO_ESCAPE "\\=,"
#define SWL_MAP_ESCAPE_CHAR '\\'

/**
 * Map containing both value and and reference data.
 * The API shall always work with pointers. This means that always a pointer to a value is required.
 * The map requires a swl_type_t for both key and value. Types can be reference types or value types.
 * When adding reference types, if the swl_type_t has a copy function set, a copy is made and the pointer
 * to that copy stored in map. If no copy function is set, the pointer is stored as is.
 * When adding value types, the data is copied from the pointer location into the map itself.
 */

/**
 * Provide pointer to the entry, with a given key.
 */
static swl_mapEntry_t* s_getEntry(const swl_map_t* map, const swl_typeData_t* key) {
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &map->dataVector) {
        if(swl_type_equals(map->keyType, swl_map_getEntryKeyValue(map, it.data), key)) {
            return it.data;
        }
    }
    return NULL;
}

/**
 * Initialize a given map
 * @param map
 *  the map to initialize
 * @param keyType
 *  the type of key
 * @param valueType
 *  the type of the value
 */
swl_rc_ne swl_map_init(swl_map_t* map, swl_type_t* keyType, swl_type_t* valueType) {
    return swl_map_initExt(map, keyType, valueType, SWL_MAP_DEFAULT_BLOCK_SIZE);
}

/**
 * Initialize a given map
 * @param map
 *  the map to initialize
 * @param keyType
 *  the type of key
 * @param valueType
 *  the type of the value
 * @param blockSize
 *  the blockSize of the underlying data structure. Must be greater than 2, and smaller or equal to 32
 * @return
 *  SWL_RC_OK if properly initialized. Error code if not.
 *
 */
swl_rc_ne swl_map_initExt(swl_map_t* map, swl_type_t* keyType, swl_type_t* valueType, uint32_t blockSize) {
    map->keyType = keyType;
    map->valueType = valueType;

    return swl_unLiList_initExt(&map->dataVector, blockSize, map->keyType->size + map->valueType->size);
}

bool swl_map_isInitialized(const swl_map_t* map) {
    if(!map->keyType) {
        return false;
    }
    if(!map->valueType) {
        return false;
    }
    return swl_unLiList_isInitialized(&map->dataVector);
}

/**
 * Clear the target map, removing all entries
 */
void swl_map_clear(swl_map_t* map) {
    ASSERTI_NOT_NULL(map, , ME, "NULL");
    swl_mapEntry_t* entry = swl_unLiList_get(&map->dataVector, 0);
    while(entry != NULL) {
        swl_type_cleanup(map->keyType, swl_map_getEntryKeyRef(map, entry));
        swl_type_cleanup(map->valueType, swl_map_getEntryValueRef(map, entry));

        swl_unLiList_remove(&map->dataVector, 0);
        entry = swl_unLiList_get(&map->dataVector, 0);
    }
}

/**
 * Clean up the map, freeing all allocated memory. This includes memory allocated for referenced data.
 */
void swl_map_cleanup(swl_map_t* map) {
    swl_map_clear(map);
    map->keyType = NULL;
    map->valueType = NULL;
    swl_unLiList_destroy(&map->dataVector);
}

static void s_setKey(swl_map_t* map, swl_mapEntry_t* entry, swl_typeData_t* key) {
    swl_type_copyTo(map->keyType, entry, key);
}

static void s_setValue(swl_map_t* map, swl_mapEntry_t* entry, swl_typeData_t* value) {
    swl_type_copyTo(map->valueType, entry + map->keyType->size, value);
}

/**
 * Check whether map as a given key.
 */
bool swl_map_has(const swl_map_t* map, swl_typeData_t* key) {
    return swl_map_get(map, key) != NULL;
}

/**
 * Add element to map with given key and value.
 */
bool swl_map_add(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* val) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(key, false, ME, "NULL");
    ASSERT_NOT_NULL(val, false, ME, "NULL");
    ASSERT_FALSE(swl_map_has(map, key), false, ME, "EXISTS");

    swl_mapEntry_t* entry = swl_unLiList_allocElement(&map->dataVector);
    s_setKey(map, entry, key);
    s_setValue(map, entry, val);
    return true;
}

/**
 * Delete element with given key.
 */
void swl_map_delete(swl_map_t* map, const swl_typeData_t* key) {
    swl_mapEntry_t* entry = s_getEntry(map, key);
    swl_type_cleanup(map->keyType, swl_map_getEntryKeyRef(map, entry));
    swl_type_cleanup(map->valueType, swl_map_getEntryValueRef(map, entry));
    swl_unLiList_removeByPointer(&map->dataVector, entry);
}

/**
 * Return the value associated with the given key.
 *
 * Note that map stores a copy of the value, so pointer values will NOT match exactly.
 *
 * Return NULL if key not present.
 */

swl_typeData_t* swl_map_get(const swl_map_t* map, const swl_typeData_t* key) {
    swl_mapEntry_t* entry = s_getEntry(map, key);
    return swl_map_getEntryValueValue(map, entry);
}

/**
 * Return the value element pointer associated with the given key.
 *
 * Return NULL if key not present.
 */

swl_typeEl_t* swl_map_getRef(const swl_map_t* map, const swl_typeData_t* key) {
    swl_mapEntry_t* entry = s_getEntry(map, key);
    return swl_map_getEntryValueRef(map, entry);
}

/**
 * Return the first map entry where the data matches the provided data;
 */
swl_mapEntry_t* swl_map_find(const swl_map_t* map, const swl_typeData_t* data) {
    ASSERT_NOT_NULL(map, NULL, ME, "NULL");
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &map->dataVector) {
        if(swl_type_equals(map->valueType, swl_map_getEntryValueValue(map, it.data), data)) {
            return it.data;
        }
    }
    return NULL;
}

bool swl_map_getCharFromChar(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, const char* key) {
    char keyBuffer[map->keyType->size];
    memset(keyBuffer, 0, map->keyType->size);
    swl_typeEl_t* tmpEl = &keyBuffer;

    ASSERT_TRUE(swl_type_fromChar(map->keyType, tmpEl, key), false, ME, "Parse Error");
    swl_typeData_t* keyData = SWL_TYPE_EL_TO_DATA(map->keyType, tmpEl);
    swl_typeData_t* data = swl_map_get(map, keyData);

    swl_type_cleanup(map->keyType, tmpEl);

    ASSERTS_NOT_NULL(data, false, ME, "NULL");
    map->valueType->typeFun->toChar(map->valueType, tgtStr, tgtStrSize, data);
    return true;
}

bool s_setInternal(swl_map_t* map, swl_mapEntry_t* entry, swl_typeData_t* data) {
    s_setValue(map, entry, data);
    return true;
}

/**
 * Set the value of the given key in the map, to the given value
 * Note that this only works if entry already exists.
 */
bool swl_map_set(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* data) {
    swl_mapEntry_t* entry = s_getEntry(map, key);
    if(entry == NULL) {
        return false;
    }
    return s_setInternal(map, entry, data);
}

/**
 * Set the value of the given key in the map, to the given value if that value exists
 * If value not exists, it will be added.
 */
bool swl_map_addOrSet(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* data) {
    swl_mapEntry_t* entry = s_getEntry(map, key);
    if(entry == NULL) {
        return swl_map_add(map, key, data);
    } else {
        return s_setInternal(map, entry, data);
    }
}

/**
 * The size of this map.
 */
uint32_t swl_map_size(const swl_map_t* map) {
    ASSERT_NOT_NULL(map, 0, ME, "NULL");
    return swl_unLiList_size(&map->dataVector);
}

bool swl_map_equals(const swl_map_t* map1, const swl_map_t* map2) {
    ASSERT_NOT_NULL(map2, false, ME, "NULL");
    ASSERT_NOT_NULL(map1, false, ME, "NULL");
    ASSERT_EQUALS(map1->keyType, map2->keyType, false, ME, "DIFF");

    if(swl_map_size(map2) != swl_map_size(map1)) {
        return false;
    }

    swl_mapIt_t mapIt;
    swl_map_for_each(mapIt, map1) {

        swl_typeData_t* srcVal = swl_map_itValue(&mapIt);
        swl_typeData_t* dstVal = swl_map_get(map2, swl_map_itKey(&mapIt));
        if(dstVal == NULL) {
            return false;
        }
        if(!swl_type_equals(map1->valueType, srcVal, dstVal)) {
            return false;
        }
    }

    return true;
}

/**
 * @brief serialize a map to target string in key=value,key=value... format.
 *
 * @param tgtStr the string to which to serialize the map
 * @param tgtStrSize the size of tgtStr
 * @param map the map to serialize
 *
 * @return true when the serialization was successful. Otherwise, false.
 */
bool swl_map_toChar(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map) {
    return swl_map_toCharSep(tgtStr, tgtStrSize, map, '=', ',');
}

/**
 * @brief serialize a map to target string in key[keyValSep]value[tupleSep]key[keyValSep]value... format.
 *
 * @param tgtStr the string to which to serialize the map
 * @param tgtStrSize the size of tgtStr
 * @param map the map to serialize
 * @param keyValSep the character to separate the key and the value of a map item
 * @param tupleSep the character to separate the map items
 *
 * @return true when the serialization was successful. Otherwise, false.
 */
bool swl_map_toCharSep(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, char keyValSep, char tupleSep) {
    return swl_map_toCharSepEsc(tgtStr, tgtStrSize, map, keyValSep, tupleSep, SWL_MAP_CHARS_TO_ESCAPE, SWL_MAP_ESCAPE_CHAR);
}

/**
 * @brief serialize a map to target string in key[keyValSep]value[tupleSep]key[keyValSep]value... format.
 *
 * @param tgtStr the string to which to serialize the map
 * @param tgtStrSize the size of tgtStr
 * @param map the map to serialize
 * @param keyValSep the character to separate the key and the value of a map item
 * @param tupleSep the character to separate the map items
 * @param charsToEscape the characters to escape
 * @param escapeChar the character to put in front of charsToEscape when found
 *
 * @return true when the serialization was successful. Otherwise, false.
 */
bool swl_map_toCharSepEsc(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, char keyValSep, char tupleSep,
                          const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_TRUE(tgtStrSize > 0, false, ME, "Empty");

    char buffer[128] = {0};
    ASSERT_TRUE(tgtStrSize > 0, false, ME, "Zero len string");
    tgtStr[0] = '\0';
    swl_unLiListIt_t it;
    uint32_t vectorSize = swl_map_size(map);
    bool success = true;
    swl_unLiList_for_each(it, &map->dataVector) {
        map->keyType->typeFun->toChar(map->keyType, buffer, sizeof(buffer), swl_map_getEntryKeyValue(map, it.data));
        success &= swl_str_addEscapeChar(buffer, sizeof(buffer), charsToEscape, escapeChar);
        success &= swl_str_catFormat(tgtStr, tgtStrSize, "%s%c", buffer, keyValSep);
        map->valueType->typeFun->toChar(map->valueType, buffer, sizeof(buffer), swl_map_getEntryValueValue(map, it.data));
        success &= swl_str_addEscapeChar(buffer, sizeof(buffer), charsToEscape, escapeChar);
        success &= swl_str_catFormat(tgtStr, tgtStrSize, "%s%c", buffer, it.index < vectorSize - 1 ? tupleSep : '\0');
    }
    return success;
}

typedef struct {
    const char* curPtr;
    uint32_t strLen;
    uint32_t nextSplit;
    bool endFound;
    bool entryExists;
    swl_mapEntry_t* entry;
} parseData_t;

static bool s_createOrFindEntryFromKey(swl_map_t* map, parseData_t* data) {

    char buffer[128] = {0};
    char keyBuffer[map->keyType->size];
    memset(keyBuffer, 0, map->keyType->size);
    swl_typeEl_t* tmpData = &keyBuffer;

    ASSERT_FALSE(data->nextSplit > sizeof(buffer),
                 false, ME, "Buffer too small %u / %zu", data->nextSplit, sizeof(buffer));
    //copy will fail intentionally as we only want to copy part
    swl_str_copy(buffer, data->nextSplit + 1, data->curPtr);
    ASSERT_TRUE(swl_str_removeEscapeChar(buffer, sizeof(buffer), SWL_MAP_CHARS_TO_ESCAPE, SWL_MAP_ESCAPE_CHAR),
                false, ME, "FAIL");

    data->curPtr += data->nextSplit + 1;
    data->strLen = data->strLen - data->nextSplit - 1;

    ASSERT_TRUE(swl_type_fromChar(map->keyType, tmpData, buffer), false, ME, "Parse Error");
    data->entry = s_getEntry(map, SWL_TYPE_EL_TO_DATA(map->keyType, tmpData));
    if(data->entry != NULL) {
        data->entryExists = true;
        swl_type_cleanup(map->keyType, tmpData);
    } else {
        data->entry = swl_unLiList_allocElement(&map->dataVector);
        memcpy(data->entry, keyBuffer, map->keyType->size);
    }
    return true;
}

static bool s_writeValOfEntry(swl_map_t* map, parseData_t* data, bool overwrite) {

    char buffer[128] = {0};

    if(data->nextSplit >= data->strLen) {
        data->endFound = true;
        data->nextSplit = data->strLen;
    }

    ASSERT_FALSE((data->nextSplit == 0) || (data->nextSplit > sizeof(buffer)),
                 false, ME, "Buffer too small %u / %zu", data->nextSplit, sizeof(buffer));
    //copy will fail intentionally as we only want to copy part
    swl_str_copy(buffer, data->nextSplit + 1, data->curPtr);
    ASSERT_TRUE(swl_str_removeEscapeChar(buffer, sizeof(buffer), SWL_MAP_CHARS_TO_ESCAPE, SWL_MAP_ESCAPE_CHAR),
                false, ME, "FAIL");

    data->curPtr += data->nextSplit + 1;
    data->strLen = data->strLen - data->nextSplit - 1;

    if(!data->entryExists || overwrite) {
        swl_type_fromChar(map->valueType, data->entry + map->keyType->size, buffer);
    }

    return true;
}

//limit must be in chars to escape
static uint32_t s_getNonEscapedStr(const char* buffer, char limit) {
    uint32_t retSize = 0;

    const char* curPtr = buffer;
    uint32_t len = strlen(buffer);
    uint32_t subStrSize = strcspn(curPtr, SWL_MAP_CHARS_TO_ESCAPE);

    while(subStrSize + retSize < len && (curPtr[subStrSize] != limit)) {

        if(curPtr[subStrSize] == SWL_MAP_ESCAPE_CHAR) {
            retSize += subStrSize + 2;
        } else {
            retSize += subStrSize + 1;
        }
        curPtr = &buffer[retSize]; // skip escaped char
        subStrSize = strcspn(curPtr, SWL_MAP_CHARS_TO_ESCAPE);
    }
    retSize += subStrSize;
    //no limit found, return str len
    return retSize;
}

/**
 * Populate this map from the given string. Set overwrite to allow
 * data to be overwritten.
 */
bool swl_map_fromChar(swl_map_t* map, const char* srcStr, bool overwrite) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");

    parseData_t data;
    memset(&data, 0, sizeof(parseData_t));
    data.curPtr = srcStr;
    data.strLen = strlen(srcStr);
    data.nextSplit = s_getNonEscapedStr(srcStr, '=');
    data.endFound = false;

    while(data.nextSplit != 0 && !data.endFound && data.nextSplit < data.strLen) {

        data.entryExists = false;

        bool success = s_createOrFindEntryFromKey(map, &data);
        if(!success) {
            SAH_TRACEZ_ERROR(ME, "done");
            return false;
        }
        data.nextSplit = s_getNonEscapedStr(data.curPtr, ',');
        size_t testSplit = s_getNonEscapedStr(data.curPtr, '=');

        if(testSplit < data.nextSplit) {
            return false;
        }

        success = s_writeValOfEntry(map, &data, overwrite);
        if(!success) {
            return false;
        }
        data.nextSplit = s_getNonEscapedStr(data.curPtr, '=');
        testSplit = s_getNonEscapedStr(data.curPtr, ',');

        if(testSplit < data.nextSplit) {
            return false;
        }
    }
    return true;
}

/**
 * Allocate a new entry to be added to the map
 * Note that there are no protections against duplication here.
 */
swl_mapEntry_t* swl_map_alloc(swl_map_t* map) {
    ASSERT_NOT_NULL(map, NULL, ME, "NULL");
    return swl_unLiList_allocElement(&map->dataVector);
}

swl_mapEntry_t* swl_map_getEntry(const swl_map_t* map, const swl_typeData_t* key) {
    return s_getEntry(map, key);
}

void swl_map_deleteEntry(swl_map_t* map, swl_mapEntry_t* entry) {
    swl_type_cleanup(map->keyType, swl_map_getEntryKeyRef(map, entry));
    swl_type_cleanup(map->valueType, swl_map_getEntryValueRef(map, entry));
    swl_unLiList_removeByPointer(&map->dataVector, entry);
}

/**
 * Provide a pointer to the key data
 */
swl_typeEl_t* swl_map_getEntryKeyRef(const swl_map_t* map _UNUSED, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return (swl_typeEl_t*) entry;
}

/**
 * Provide a pointer to the value data
 */
swl_typeEl_t* swl_map_getEntryValueRef(const swl_map_t* map, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return (swl_typeEl_t*) entry + map->keyType->size;
}

/**
 * Provide a pointer to the key data
 */
swl_typeData_t* swl_map_getEntryKeyValue(const swl_map_t* map, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return SWL_TYPE_EL_TO_DATA(map->keyType, entry);
}

/**
 * Provide a pointer to the value data
 */
swl_typeData_t* swl_map_getEntryValueValue(const swl_map_t* map, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return SWL_TYPE_EL_TO_DATA(map->valueType, entry + map->keyType->size);
}

/**
 * Puts the first map iterator of map in mapIt.
 */
void swl_map_firstIt(swl_mapIt_t* mapIt, const swl_map_t* map) {
    memset(mapIt, 0, sizeof(swl_mapIt_t));
    swl_unLiList_firstIt(&mapIt->mapIt, &map->dataVector);
    mapIt->map = (swl_map_t*) map;
}

void swl_map_nextIt(swl_mapIt_t* mapIt) {
    swl_unLiList_nextIt(&mapIt->mapIt);
}

/**
 * Return the key of the map iterator
 */
swl_typeData_t* swl_map_itKey(swl_mapIt_t* mapIt) {
    return swl_map_getEntryKeyValue(mapIt->map, mapIt->mapIt.data);
}

/**
 * Return the value of the map iterator
 */
swl_typeData_t* swl_map_itValue(swl_mapIt_t* mapIt) {
    return swl_map_getEntryValueValue(mapIt->map, mapIt->mapIt.data);
}

uint32_t swl_map_itIndex(swl_mapIt_t* mapIt) {
    return mapIt->mapIt.index;
}

/**
 * Write char representation of key to targetBuffer
 */
ssize_t swl_map_itKeyChar(char* tgtBuffer, uint32_t tgtSize, swl_mapIt_t* mapIt) {
    swl_typeData_t* data = swl_map_itKey(mapIt);
    if(data == NULL) {
        return 0;
    }
    return mapIt->map->keyType->typeFun->toChar(mapIt->map->keyType, tgtBuffer, tgtSize, data);
}

/**
 * Write char representation of value to tgt buffer
 */
ssize_t swl_map_itValueChar(char* tgtBuffer, uint32_t tgtSize, swl_mapIt_t* mapIt) {
    swl_typeData_t* data = swl_map_itValue(mapIt);
    if(data == NULL) {
        return 0;
    }
    return mapIt->map->valueType->typeFun->toChar(mapIt->map->keyType, tgtBuffer, tgtSize, data);
}

/**
 * Write char representation of key to file
 */
ssize_t swl_map_keyToFile(FILE* file, swl_mapIt_t* mapIt, const swl_print_args_t* printArg) {
    swl_typeData_t* data = swl_map_itKey(mapIt);
    if(data == NULL) {
        return 0;
    }
    return swl_type_toFile(mapIt->map->keyType, file, data, printArg);
}

/**
 * Write char representation of value to tgt buffer
 */
ssize_t swl_map_valueToFile(FILE* file, swl_mapIt_t* mapIt, const swl_print_args_t* printArg) {
    swl_typeData_t* data = swl_map_itValue(mapIt);
    if(data == NULL) {
        return 0;
    }
    return swl_type_toFile(mapIt->map->valueType, file, data, printArg);
}

/**
 * Copy a source map content to a destination map
 * Destination map will be overwritten
 */
bool swl_map_copyTo(swl_map_t* destMap, swl_map_t* srcMap) {
    ASSERT_NOT_NULL(destMap, false, ME, "NULL");
    ASSERT_NOT_NULL(srcMap, false, ME, "NULL");

    swl_map_clear(destMap);

    swl_mapIt_t mapIt;
    swl_map_for_each(mapIt, srcMap) {
        swl_typeData_t* srcKey = swl_map_itKey(&mapIt);
        swl_typeData_t* srcVal = swl_map_itValue(&mapIt);
        swl_map_add(destMap, srcKey, srcVal);
    }
    return true;
}
