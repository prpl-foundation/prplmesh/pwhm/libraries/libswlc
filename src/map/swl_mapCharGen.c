/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>

#include "swl/swl_common.h"
#include "swl/map/swl_mapCharGen.h"

#define ME "mapCGen"

swl_rc_ne swl_mapCharGen_fillMapFromEntries(swl_mapChar_t* map, swl_mapCharGen_data_t* data,
                                            void* userData, bool clear, char* tgtKeyFormat, ...) {
    char* tgtKey = NULL;
    if(tgtKeyFormat != NULL) {
        va_list args;
        va_start(args, tgtKeyFormat);
        int ret = vasprintf(&tgtKey, tgtKeyFormat, args);
        va_end(args);

        if((ret < 0) || (tgtKey == NULL)) {
            if(tgtKey != NULL) {
                free(tgtKey);
            }
            return SWL_RC_ERROR;
        }
    }

    char nameBuffer[128];
    char buffer[128];
    if(clear) {
        swl_map_clear(map);
    }
    swl_mapCharGen_entry_t* entryList = data->entries;

    for(size_t i = 0; i < data->nrEntries; i++) {
        swl_str_copy(nameBuffer, sizeof(nameBuffer), entryList[i].name);
        if((data->renameKey != NULL) && (tgtKey != NULL)) {
            swl_str_replace(nameBuffer, sizeof(nameBuffer), NULL, data->renameKey, tgtKey);
        }

        if(entryList[i].callbackFun != NULL) {
            bool success = entryList[i].callbackFun(buffer, sizeof(buffer), entryList[i].name, userData);
            if(success) {
                swl_mapChar_addOrSet(map, nameBuffer, buffer);
            } else {
                SAH_TRACEZ_INFO(ME, "failed callback %s", entryList[i].name);
            }
        } else if(entryList[i].val != NULL) {
            swl_str_copy(buffer, sizeof(buffer), entryList[i].val);
        } else if(entryList[i].valueType != NULL) {

            void* dataPointer = userData + entryList[i].valueOffset;
            void* finalPointer = dataPointer;
            if(entryList[i].isEmbeddedObj) {
                finalPointer = &dataPointer;
            }
            void* actualData = swl_type_toPtr(entryList[i].valueType, finalPointer);
            entryList[i].valueType->typeFun->toChar(entryList[i].valueType, buffer, sizeof(buffer), actualData);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failure to generate map");
            continue;
        }

        swl_mapChar_addOrSet(map, nameBuffer, buffer);
        SAH_TRACEZ_INFO(ME, "update %s : %s", nameBuffer, buffer);
    }
    if(tgtKey != NULL) {
        free(tgtKey);
    }

    return SWL_RC_OK;
}


