/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * \file A collection of children of a parent, where the child also knows the parent and has a name.
 *
 * Duplicates and multiple children with the same name are disallowed.
 *
 * It is enforced that third parties do not break the bidirectional association invariant
 * (child knows parent if and only if parent knows child).
 */

#include "swl/swl_biDirCollection.h"
#include "swl/swl_assert.h"
#include "swl/swl_string.h"
#define ME "swlNc"

/**
 * Initializes a biDirCollection.
 *
 * Do not call twice on the same biDirCollection without calling #swl_bdc_cleanup() first.
 */
bool swl_bdc_init(swl_bdc_t* self, void* parent, const swl_bdc_child_type_t* childType) {
    ASSERT_NOT_NULL(self, false, ME, "NULL");
    ASSERT_NOT_NULL(parent, false, ME, "NULL");
    ASSERT_NOT_NULL(childType, false, ME, "NULL");
    ASSERT_NOT_NULL(childType->getName, false, ME, "NULL");
    ASSERT_NOT_NULL(childType->onRemove, false, ME, "NULL");
    ASSERT_NOT_NULL(childType->getParent, false, ME, "NULL");
    self->parent = parent;
    self->childType = childType;
    swl_rc_ne ret = swl_unLiList_init(&self->children, sizeof(void*));
    return swl_rc_isOk(ret);
}

static bool s_isBeingDestroyed(swl_bdc_t* self) {
    ASSERT_NOT_NULL(self, true, ME, "NULL");
    return self->parent == NULL;
}

/**
 *
 * Time complexity: Linear.
 */
void swl_bdc_cleanup(swl_bdc_t* self) {
    ASSERT_NOT_NULL(self, , ME, "NULL");
    ASSERT_FALSE(s_isBeingDestroyed(self), , ME, "BEING DESTROYED");
    self->parent = NULL; // mark as being destroyed.

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &self->children) {
        void* child = *swl_unLiList_data(&it, void**);
        ASSERT_NOT_NULL(child, , ME, "BUG: NULL in collection that cannot contain NULLs");
        self->childType->onRemove(child, self);
    }

    swl_unLiList_destroy(&self->children);
}

/**
 * Add `child` to `parent`'s list of children `list`.
 *
 * Time complexity: Linear (if child's functions are constant).
 *
 * @return true on success, false on failure:
 *   - Child has no `onAdd()` and does not know parent according to `getParent()`
 *     (this is to avoid third parties to secretly add children)
 *   - Child has an `onAdd()` which returned `false`
 *   - Parent already has the child
 *   - Parent already has a child with the same name
 *   - NULL argument
 *   - Parent is being destroyed.
 */
bool swl_bdc_add(swl_bdc_t* self, void* child) {
    ASSERT_NOT_NULL(self, false, ME, "NULL");
    ASSERT_NOT_NULL(child, false, ME, "NULL");
    const char* name = self->childType->getName(child);
    ASSERT_NOT_NULL(name, false, ME, "NULL name");
    ASSERT_FALSE(s_isBeingDestroyed(self), false, ME, "BEING DESTROYED (%s)", name);
    ASSERT_NULL(swl_bdc_byName(self, name), false, ME, "duplicate by name '%s'", name);
    ASSERT_FALSE(swl_unLiList_contains(&self->children, &child), false, ME, "duplicate by pointer (%s)", name);

    if(self->childType->onAdd) {
        bool ok = self->childType->onAdd(child, self->parent);
        ASSERTS_TRUE(ok, false, ME, "Rejected by child");
        ASSERT_EQUALS(self->parent, self->childType->getParent(child), false,
                      ME, "Child has BUG!! onAdd() returned true but did not set parent! (%s)", name);
    } else {
        ASSERT_EQUALS(self->parent, self->childType->getParent(child), false,
                      ME, "Child without onAdd() must initiate bidir assoc! (%s)", name);
    }

    // Child's functions can trigger destruction. So deal with it.
    if(s_isBeingDestroyed(self)) {
        SAH_TRACEZ_ERROR(ME, "BEING DESTROYED (%s)", name);
        self->childType->onRemove(child, self->parent);
        return false;
    }

    // Check duplication again calling child's functions,
    // because child could add during its functions.
    ASSERT_NULL(swl_bdc_byName(self, name), false, ME, "duplicate by name '%s'", name);
    ASSERT_FALSE(swl_unLiList_contains(&self->children, &child), false, ME, "duplicate by pointer (%s)", name);

    swl_unLiList_add(&self->children, &child);
    return true;
}

/**
 * Remove `child` from the children of the parent.
 *
 * Time complexity: Linear.
 *
 * If successful, `onRemove()` is called on `child`. See #swl_bdc_child_type_t.onRemove()
 *
 * @param child: the child to be removed from the parent
 *
 * @return true on success, false on failure:
 *   - parent does not have `child`
 *   - `self` is being destroyed (this is safe and no error is printed)
 *   - NULL argument
 */
bool swl_bdc_remove(swl_bdc_t* self, void* child) {
    ASSERT_NOT_NULL(self, false, ME, "NULL");
    ASSERT_NOT_NULL(child, false, ME, "NULL");
    ASSERTS_FALSE(s_isBeingDestroyed(self), false, ME, "BEING DESTROYED");

    bool ok = -1 != swl_unLiList_removeByData(&self->children, &child);
    ASSERT_TRUE(ok, false, ME, "Error removing");

    self->childType->onRemove(child, self->parent);
    // Do not verify whether child removes its pointer to parent, because child could have destroyed
    // itself.

    return true;
}

/**
 * Searches child in parent's list of children `list`.
 *
 * @param name: name of the child to search for.
 *
 * @return the child that was found, or NULL if:
 *   - The parent has no child with given name
 *   - NULL argument
 *   - `self` is being destroyed (so during destruction, parent is considered to have no children)
 */
void* swl_bdc_byName(swl_bdc_t* self, const char* name) {
    ASSERT_NOT_NULL(self, NULL, ME, "NULL");
    ASSERT_NOT_NULL(name, NULL, ME, "NULL");
    ASSERTS_FALSE(s_isBeingDestroyed(self), NULL, ME, "BEING DESTROYED (%s)", name);

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &self->children) {
        void* child = *swl_unLiList_data(&it, void**);

        // Note: no protection against child invalidating the iterator during child's functions.
        if(swl_str_matches(self->childType->getName(child), name)) {
            return child;
        }
    }
    return NULL;
}

void* swl_bdc_firstIt(swl_bdc_it_t* tgtIterator, swl_bdc_t* self) {
    ASSERT_NOT_NULL(tgtIterator, NULL, ME, "NULL");
    tgtIterator->valid = false;
    ASSERT_NOT_NULL(self, NULL, ME, "NULL");
    ASSERT_FALSE(s_isBeingDestroyed(self), NULL, ME, "BEING DESTROYED");

    swl_unLiList_firstIt(tgtIterator, &self->children);
    ASSERTS_TRUE(swl_bdc_it_isValid(tgtIterator), NULL, ME, "No first item");
    return *swl_bdc_it_data(tgtIterator, void**);
}
bool swl_bdc_it_isValid(swl_bdc_it_t* it) {
    ASSERT_NOT_NULL(it, false, ME, "NULL");
    return it->valid;
}
void* swl_bdc_it_next(swl_bdc_it_t* it) {
    ASSERTS_TRUE(swl_bdc_it_isValid(it), NULL, ME, "No current item, so there will be no next either");
    swl_unLiList_nextIt(it);
    ASSERTS_TRUE(swl_bdc_it_isValid(it), NULL, ME, "No next item");
    return *swl_bdc_it_data(it, void**);
}
