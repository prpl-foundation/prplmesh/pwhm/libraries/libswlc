/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "debug/sahtrace.h"
#include <stdlib.h>
#include "swl/swl_common_histogram.h"
#include "swl/swl_common_type.h"
#include "swl/swl_returnCode.h"

#define ME "swlHstG"

const char* swl_histogram_event_str[SWL_HISTOGRAM_EVENT_MAX] = {
    "Off",
    "Window",
    "History"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_histogram_event_str) == SWL_HISTOGRAM_EVENT_MAX, "swl_histogram_event_str not correctly defined");


const char* swl_histogram_tableNames_str[SWL_HISTOGRAM_WINDOW_ID_MAX] = {
    "Current",
    "Total",
    "Last"
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_histogram_tableNames_str) == SWL_HISTOGRAM_WINDOW_ID_MAX,
                  "swl_histogram_tableNames_str not correctly defined");




/**
 * Reset a single window
 */
static void s_windowReset(swl_histogram_t* hist, swl_histogramWindow_t* window) {
    memset(window->buckets, 0, hist->totalNrBuckets * sizeof(uint16_t));
    window->nrValues = 0;
    window->startTime = 0;
    window->endTime = 0;
}


/**
 * Initialize the histogram
 *
 * @param hist: the histogram to be initialized
 * @param nrValues: the number of different values that have to be sorted in histogram
 * @param nrObsPerWindow: the number of observations in a single window. Can be zero, in which case cycling should
 * be done by user.
 * @param nrWindows: the number of windows in the histogram.
 * @param nrBucketsPerValue: an uint8_t array of len nrValues, which contains for each value, the
 * number of buckets that need to be made for that value. When adding a new observation, the value at
 * the given index, can not exceed the nrBucketsPerValue at that index
 * @param historyLen: the length of the history that must be kept. Can be zero if no history is required.
 */
swl_rc_ne swl_histogram_init(swl_histogram_t* hist, uint32_t nrValues, uint32_t nrObsPerWindow,
                             uint32_t nrWindows, uint8_t* nrBucketsPerValue, uint32_t historyLen) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(nrBucketsPerValue, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(nrValues, 0, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(nrWindows, 0, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT DONE");

    memset(hist, 0, sizeof(swl_histogram_t));

    hist->nrValues = nrValues;

    hist->nrObsPerWindow = nrObsPerWindow;
    hist->nrBucketsPerValue = nrBucketsPerValue;
    hist->offsetList = calloc(nrValues + 1, sizeof(uint16_t));
    for(uint32_t i = 1; i < hist->nrValues; i++) {
        hist->offsetList[i] = hist->offsetList[i - 1] + hist->nrBucketsPerValue[i - 1];
    }
    uint16_t totalNrBuckets = hist->nrBucketsPerValue[nrValues - 1] + hist->offsetList[nrValues - 1];
    hist->totalNrBuckets = totalNrBuckets;

    hist->nrWindows = nrWindows;
    swl_histogram_windowData_t* hgData = &hist->histogramData;

    hgData->lastWindows = calloc(nrWindows, sizeof(swl_histogramWindow_t));
    for(uint32_t i = 0; i < nrWindows; i++) {
        hgData->lastWindows[i].buckets = calloc(totalNrBuckets, sizeof(uint16_t));
    }
    hgData->currentWindow.buckets = calloc(totalNrBuckets, sizeof(uint16_t));
    hgData->totalWindow.buckets = calloc(totalNrBuckets, sizeof(uint16_t));

    swl_histogram_historyData_t* hrData = &hist->historyData;

    hist->historyLen = historyLen;
    if(historyLen != 0) {
        hrData->history = calloc(historyLen * nrValues, sizeof(uint8_t));
    }
    return SWL_RC_OK;
}

/**
 * Initialize the histogram using initData object, recommended to be defined by the SWL_HISTOGRAM macro.
 *
 * @param hist: the histogram to be initialized
 * @param initData: the initialisation data for this histogram, contains names and number of buckets.
 * @param nrObsPerWindow: the number of observations in a single window. Can be zero, in which case cycling should
 * be done by user.
 * @param nrWindows: the number of windows in the histogram.
 * @param historyLen: the length of the history that must be kept. Can be zero if no history is required.
 */

swl_rc_ne swl_histogram_initExt(swl_histogram_t* map, swl_histogramInitData_t* initData,
                                uint32_t nrObsPerWindow, uint32_t nrWindows, uint32_t historyLen) {
    ASSERT_NOT_NULL(initData, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_rc_ne retCode = swl_histogram_init(map, initData->nrValues, nrObsPerWindow, nrWindows, initData->bucketSizes, historyLen);
    if(!swl_rc_isOk(retCode)) {
        return retCode;
    }
    swl_histogram_setNames(map, initData->names);
    return SWL_RC_OK;

}

/**
 * Set the histogram names to be used for retrieval of data, such as getAllHistograms
 * names can be NULL
 * note that swl_histogram will store the reference and will NOT copy names, so names should not
 * be removed.
 */
swl_rc_ne swl_histogram_setNames(swl_histogram_t* hist, char** names) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT DONE");
    hist->names = names;
    return SWL_RC_OK;
}

/**
 * Sum all the last windows to the new total window.
 * assumes nextWindow is set to the "oldest" window
 */
static void s_sumTotal(swl_histogram_t* hist) {

    swl_histogram_windowData_t* hgData = &hist->histogramData;
    if(hgData->filledWindows == 0) {
        s_windowReset(hist, &hgData->totalWindow);
        return;
    }

    for(uint32_t i = 0; i < hist->totalNrBuckets; i++) {
        uint16_t acc = 0;
        for(uint32_t j = 0; j < hgData->filledWindows; j++) {
            acc += hgData->lastWindows[j].buckets[i];
        }

        hgData->totalWindow.buckets[i] = acc;
    }

    uint16_t valueAcc = 0;
    for(uint32_t i = 0; i < hgData->filledWindows; i++) {
        valueAcc += hgData->lastWindows[i].nrValues;
    }
    hgData->totalWindow.nrValues = valueAcc;

    uint16_t firstIndex = hgData->nextWindowIndex;
    uint16_t lastIndex = (firstIndex + hist->nrWindows - 1) % hist->nrWindows;


    if(hgData->filledWindows < hist->nrWindows) {
        firstIndex = 0;
        lastIndex = hgData->nextWindowIndex - 1;
    }
    hgData->totalWindow.startTime = hgData->lastWindows[firstIndex].startTime;
    hgData->totalWindow.endTime = hgData->lastWindows[lastIndex].endTime;

}

/**
 * Manually cycle a window.
 * Allows for different histograms at different places to be cycled at the same time.
 */
swl_rc_ne swl_histogram_cycleWindow(swl_histogram_t* hist) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_histogram_windowData_t* hgData = &hist->histogramData;
    swl_histogramWindow_t* targetWindow = &hgData->lastWindows[hgData->nextWindowIndex];
    uint16_t* nextWindowBuckets = targetWindow->buckets;
    memcpy(targetWindow, &hgData->currentWindow, sizeof(swl_histogramWindow_t));
    hgData->currentWindow.buckets = nextWindowBuckets;

    s_windowReset(hist, &hgData->currentWindow);

    hgData->nextWindowIndex = (hgData->nextWindowIndex + 1) % hist->nrWindows;
    hgData->filledWindows = SWL_MIN(hgData->filledWindows + 1, hist->nrWindows);
    s_sumTotal(hist);

    if(hist->handler != NULL) {
        hist->handler(hist, SWL_HISTOGRAM_EVENT_WINDOW_DONE, hist->handlerData);
        if(hgData->nextWindowIndex == 0) {
            hist->handler(hist, SWL_HISTOGRAM_EVENT_HISTORY_DONE, hist->handlerData);
        }
    }
    return SWL_RC_OK;
}

/**
 * Add a list of indexes to the histogram.
 * Function will check that the provided indexes are below the number of buckets allocated for that index.
 * If index exceeds nrBuckets, then measurement is ignored.
 */
swl_rc_ne swl_histogram_addValues(swl_histogram_t* hist, uint8_t* indexes) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(indexes, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");


    swl_timeMono_t now = swl_time_getMonoSec();

    swl_histogram_historyData_t* hrData = &hist->historyData;
    hrData->lastEntryTime = now;

    if(hist->historyLen != 0) {
        for(uint32_t i = 0; i < hist->nrValues; i++) {
            hrData->history[hrData->nextHistoryIndex + (i * hist->historyLen)] = indexes[i];
        }
        hrData->nextHistoryIndex = (hrData->nextHistoryIndex + 1) % hist->historyLen;
        hrData->filledHistorySize = SWL_MIN(hrData->filledHistorySize + 1, hist->historyLen);
    }


    swl_histogram_windowData_t* hgData = &hist->histogramData;

    swl_histogramWindow_t* curWindow = &hgData->currentWindow;
    for(uint32_t i = 0; i < hist->nrValues; i++) {
        if(indexes[i] >= hist->nrBucketsPerValue[i]) {
            continue;
        }
        uint32_t index = hist->offsetList[i] + indexes[i];
        curWindow->buckets[index]++;
    }
    if(curWindow->nrValues == 0) {
        curWindow->startTime = swl_time_getMonoSec();
    }


    curWindow->endTime = now;

    curWindow->nrValues += 1;

    if((hist->nrObsPerWindow > 0) && (curWindow->nrValues == hist->nrObsPerWindow)) {
        swl_histogram_cycleWindow(hist);
    }


    return SWL_RC_OK;
}

/**
 * Add data by providing a pointer to a histogram data value.
 * The data should basically be a struct containing only uint8_t values,
 * but since we can not provide all names, void is provided here.
 */
swl_rc_ne swl_histogram_addDataValues(swl_histogram_t* hist, void* data) {
    return swl_histogram_addValues(hist, (uint8_t*) data);
}

/**
 * Reset the histogram:
 * * Sets all buckets to 0
 * * Sets the history to 0
 * Also resets any history present.
 */
swl_rc_ne swl_histogram_reset(swl_histogram_t* hist) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");

    swl_histogram_windowData_t* hgData = &hist->histogramData;


    s_windowReset(hist, &hgData->currentWindow);
    s_windowReset(hist, &hgData->totalWindow);
    for(uint32_t i = 0; i < hist->nrWindows; i++) {
        s_windowReset(hist, &hgData->lastWindows[i]);
    }
    hgData->nextWindowIndex = 0;
    hgData->filledWindows = 0;
    hgData->totalWindowsFilled = 0;

    swl_histogram_historyData_t* hrData = &hist->historyData;
    hrData->filledHistorySize = 0;
    hrData->nextHistoryIndex = 0;

    return SWL_RC_OK;
}

/**
 * Sets the callback handler.
 * Callback handler will be called upon filling of window, and if all windows are filled i.e. next filled window
 * will be copied in first "window" in window list.
 */
swl_rc_ne swl_histogram_setHandler(swl_histogram_t* hist, swl_histogramHandler_f handler, void* data) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");
    hist->handler = handler;
    hist->handlerData = data;
    return SWL_RC_OK;
}

/**
 * Destroy the given histogram.
 */
swl_rc_ne swl_histogram_destroy(swl_histogram_t* hist) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");

    swl_histogram_windowData_t* hgData = &hist->histogramData;
    free(hgData->currentWindow.buckets);
    hgData->currentWindow.buckets = NULL;
    free(hgData->totalWindow.buckets);
    hgData->totalWindow.buckets = NULL;
    for(uint32_t i = 0; i < hist->nrWindows; i++) {
        free(hgData->lastWindows[i].buckets);
        hgData->lastWindows[i].buckets = NULL;
    }
    free(hgData->lastWindows);
    hgData->lastWindows = NULL;


    swl_histogram_historyData_t* hrData = &hist->historyData;
    free(hrData->history);
    hrData->history = NULL;

    free(hist->offsetList);
    hist->offsetList = NULL;

    hist->handler = NULL;
    hist->handlerData = NULL;
    return SWL_RC_OK;
}

swl_histogramWindow_t* swl_histogram_getWindow(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex) {
    if(histogramIndex == SWL_HISTOGRAM_WINDOW_ID_CURRENT) {
        return &hist->histogramData.currentWindow;
    } else if(histogramIndex == SWL_HISTOGRAM_WINDOW_ID_TOTAL) {
        return &hist->histogramData.totalWindow;
    } else if(histogramIndex == SWL_HISTOGRAM_WINDOW_ID_LAST) {
        if(hist->histogramData.filledWindows > 0) {
            return &hist->histogramData.lastWindows[SWL_ARRAY_INDEX_SUB(hist->histogramData.nextWindowIndex, 1, hist->nrWindows)];
        } else {
            return &hist->histogramData.lastWindows[0];
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid histogram index %u", histogramIndex);
        return NULL;
    }
}

/**
 * Set the number of observations per window.
 * This will trigger a full histogram wipe.
 * @param hist: the histogram where the number of observations must be changed
 * @param nrObsPerWindow: the new number of observations per window
 */
swl_rc_ne swl_histogram_setNrObsPerWindow(swl_histogram_t* hist, uint32_t nrObsPerWindow) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");
    ASSERT_TRUE(nrObsPerWindow > 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_histogram_reset(hist);
    hist->nrObsPerWindow = nrObsPerWindow;
    return SWL_RC_OK;
}

/**
 * Set the number of windows.
 * The system will attempt to keep as much data as possible.
 *
 * System will keep newest data available
 */
swl_rc_ne swl_histogram_setNrWindows(swl_histogram_t* hist, uint32_t nrWindows) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");
    ASSERT_TRUE(nrWindows > 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    ASSERTI_TRUE(nrWindows != hist->nrWindows, SWL_RC_OK, ME, "NULL");

    swl_histogram_windowData_t* hgData = &hist->histogramData;

    swl_histogramWindow_t* lastWindows = hgData->lastWindows;
    uint32_t oldNrWindows = hist->nrWindows;

    hgData->lastWindows = calloc(nrWindows, sizeof(swl_histogramWindow_t));
    hist->nrWindows = nrWindows;


    uint32_t nrWindowsToCopy = SWL_MIN(nrWindows, oldNrWindows);
    uint32_t copyStartIndex = (oldNrWindows - nrWindowsToCopy);

    uint32_t startIndex = (copyStartIndex + hgData->nextWindowIndex) % oldNrWindows;

    hgData->nextWindowIndex = nrWindowsToCopy % nrWindows;
    hgData->filledWindows = SWL_MIN(hgData->filledWindows, nrWindowsToCopy);

    for(uint32_t i = 0; i < nrWindowsToCopy; i++) {
        memcpy(&hgData->lastWindows[i], &lastWindows[(startIndex + i) % oldNrWindows], sizeof(swl_histogramWindow_t)); //note that we also move buffer here
    }

    if(nrWindowsToCopy < nrWindows) {
        for(uint32_t i = nrWindowsToCopy; i < nrWindows; i++) {
            hgData->lastWindows[i].buckets = calloc(hist->totalNrBuckets, sizeof(uint16_t));
        }
    } else {
        for(uint32_t i = nrWindowsToCopy; i < oldNrWindows; i++) {
            free(lastWindows[(startIndex + i) % oldNrWindows].buckets);
        }
    }

    free(lastWindows);
    s_sumTotal(hist);


    return SWL_RC_OK;
}


/**
 * Copy the histogram values for the given value index in the target array.
 * Will only do so if target array is large enough to contain all values, otherwise SWL_RC_INVALID_PARAM is returned.
 * Will return number of values copied if all is ok.
 */
swl_rc_ne swl_histogram_getHistogramFromValue(swl_histogram_t* hist, uint16_t* tgtArray, uint32_t tgtArraySize,
                                              swl_histogram_windowId_e histogramIndex, uint16_t valueIndex, size_t* elementsCopied) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tgtArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(valueIndex < hist->nrValues, SWL_RC_INVALID_PARAM, ME, "NULL");

    uint8_t nrBuckets = hist->nrBucketsPerValue[valueIndex];
    ASSERT_TRUE(tgtArraySize >= nrBuckets, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, SWL_RC_INVALID_PARAM, ME, "NULL");

    memcpy(tgtArray, &tgtWindow->buckets[hist->offsetList[valueIndex]], nrBuckets * sizeof(uint16_t));

    if(elementsCopied != NULL) {
        *elementsCopied = (uint16_t) nrBuckets;
    }

    return SWL_RC_OK;
}



/**
 * retrieves the history of a given value.
 * If tgtArraySize is smaller than historyLen, then as many values as possible will we put in the array.
 * If tgtArraySize is larger than historyLen, then the first fields will be filled, with everything else zero.
 *
 * If elementsCopied is not NULL, the number of elements put in tgtArray will be stored in that variable.
 */
swl_rc_ne swl_histogram_getHistoryFromValue(swl_histogram_t* hist, uint8_t* tgtArray, size_t tgtArraySize, uint16_t valueIndex, size_t* elementsCopied) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");
    ASSERT_NOT_NULL(tgtArray, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_histogram_historyData_t* hrData = &hist->historyData;

    uint8_t* targetHistory = &hrData->history[hist->historyLen * valueIndex];
    memset(tgtArray, 0, tgtArraySize);

    size_t nrElToCopy = SWL_MIN(tgtArraySize, hrData->filledHistorySize);
    uint16_t startIndex = hrData->filledHistorySize - nrElToCopy; // The sequential index of first element to copy;

    if(nrElToCopy > 0) {
        if(hrData->filledHistorySize < hist->historyLen) {
            memcpy(tgtArray, &targetHistory[startIndex], nrElToCopy);
        } else {
            uint16_t firstChuckIndex = (startIndex + hrData->nextHistoryIndex) % hist->historyLen;
            uint16_t firstChunkSize = SWL_MIN(nrElToCopy, hist->historyLen - firstChuckIndex);
            uint16_t secondChuckSize = nrElToCopy - firstChunkSize;
            memcpy(tgtArray, &targetHistory[firstChuckIndex], firstChunkSize);
            memcpy(&tgtArray[firstChunkSize], targetHistory, secondChuckSize);
        }
    }

    if(elementsCopied != NULL) {
        *elementsCopied = nrElToCopy;
    }

    return (nrElToCopy < hrData->filledHistorySize ? SWL_RC_CONTINUE : SWL_RC_OK);
}

/**
 * Set the length of the history. If the new historyLen is longer, then first oldHistoryLen entries
 * will be filled with the old history. If the newHistoryLen is shorter, as much recent history
 * as possible will be put in the new history.
 *
 * Zero history len is allowed, in which case no history will be kept.
 */
swl_rc_ne swl_histogram_setHistoryLen(swl_histogram_t* hist, uint32_t historyLen) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");

    ASSERTI_NOT_EQUALS(historyLen, hist->historyLen, SWL_RC_OK, ME, "SAME");

    swl_histogram_historyData_t* hrData = &hist->historyData;

    if(historyLen == 0) {
        hrData->nextHistoryIndex = 0;
        hist->historyLen = 0;
        hrData->filledHistorySize = 0;
        free(hrData->history);
        hrData->history = NULL;
        return SWL_RC_OK;
    }


    uint8_t* newHistory = calloc(hist->nrValues, historyLen);

    if(hrData->history != NULL) {

        size_t filledHistory = 0;

        for(uint8_t i = 0; i < hist->nrValues; i++) {
            swl_histogram_getHistoryFromValue(hist, &newHistory[i * historyLen], historyLen, i, &filledHistory);
        }
        hrData->nextHistoryIndex = filledHistory % historyLen;
        hrData->filledHistorySize = filledHistory;
        free(hrData->history);
    }
    hist->historyLen = historyLen;
    hrData->history = newHistory;

    return SWL_RC_OK;
}

/**
 * returns the number of windows that are currently filled, and which
 * are thus included in the total calculation.
 */
size_t swl_histogram_getNrFilledWindows(swl_histogram_t* hist) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(hist->offsetList, SWL_RC_INVALID_STATE, ME, "INIT NOT DONE");
    return hist->histogramData.filledWindows;
}

/**
 * return the number of values in the given window.
 */
size_t swl_histogram_getNrValuesInWindow(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex) {
    ASSERT_NOT_NULL(hist, 0, ME, "NULL");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, 0, ME, "Window not found");

    return tgtWindow->nrValues;
}

/**
 * Return the start time of the window. If window has not started yet, 0 will be returned
 */
swl_timeMono_t swl_histogram_getStartTime(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex) {
    ASSERT_NOT_NULL(hist, 0, ME, "NULL");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, 0, ME, "NULL");

    return tgtWindow->startTime;
}

/**
 * Return the end time of the window. If window has not started yet, 0 will be returned
 */
swl_timeMono_t swl_histogram_getEndTime(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex) {
    ASSERT_NOT_NULL(hist, 0, ME, "NULL");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, 0, ME, "NULL");

    return tgtWindow->endTime;
}

/**
 * Return the bucket index of the bucket that contains the relative sigma offset, times one hunderd,
 * plus the percent value of where the sigma bucket is relative to the number of values in the bucket.
 *
 * Goal is to give a "rough estimate" of how close to next bucket sigma value would actually be,
 * when bucketing continuous variables. Naturally the values can just be divided by 100 to get actual bucket index
 */
void swl_histogram_getBucketOffsets(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex, size_t bucketIndex,
                                    uint16_t* bucketIndexes, const uint16_t* tgtBucketPct, uint16_t nrValues) {
    ASSERT_NOT_NULL(hist, , ME, "NULL");
    ASSERT_NOT_NULL(bucketIndexes, , ME, "NULL");
    ASSERT_NOT_NULL(tgtBucketPct, , ME, "NULL");
    ASSERT_TRUE(nrValues != 0, , ME, "ZERO");
    ASSERT_TRUE(bucketIndex < hist->nrValues, , ME, "SIZE");

    swl_histogramWindow_t* tgtWindow = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(tgtWindow, , ME, "Window not found");
    ASSERT_TRUE(tgtWindow->nrValues > 0, , ME, "EMPTY");

    uint16_t* bucketList = &tgtWindow->buckets[hist->offsetList[bucketIndex]];
    uint8_t nrBuckets = hist->nrBucketsPerValue[bucketIndex];
    uint32_t lastContainPct = 0;
    uint32_t bucketAcc = 0;
    uint8_t tgtPctIndex = 0;
    uint16_t curTgtPct = tgtBucketPct[0];

    for(uint8_t i = 0; i < nrBuckets && tgtPctIndex < nrValues; i++) {
        bucketAcc += bucketList[i];
        uint32_t containPct = bucketAcc * 100 / tgtWindow->nrValues;

        if(containPct == lastContainPct) {
            continue;
        }

        while(( containPct > curTgtPct || (containPct == 100 && curTgtPct == 100))
              && tgtPctIndex < nrValues) {

            bucketIndexes[tgtPctIndex] = i * 100 + (curTgtPct - lastContainPct) * 100 / (containPct - lastContainPct);
            tgtPctIndex++;
            if(tgtPctIndex < nrValues) {
                curTgtPct = tgtBucketPct[tgtPctIndex];
            }
        }

        lastContainPct = containPct;
    }
}

swl_rc_ne swl_histogram_printBucketHistogram(swl_histogram_t* hist, char* array, size_t arraySize, swl_histogram_windowId_e histogramIndex,
                                             uint16_t bucketIndex) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_histogramWindow_t* window = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(window, SWL_RC_INVALID_PARAM, ME, "NULL");

    uint16_t* targetBuckets = &window->buckets[hist->offsetList[bucketIndex]];
    swl_typeUInt16_arrayToChar(array, arraySize, targetBuckets, hist->nrBucketsPerValue[bucketIndex]);

    return SWL_RC_OK;
}



/**
 * Print a window to file
 */
swl_rc_ne swl_histogram_printWindowToFile(swl_histogram_t* hist, FILE* file, swl_histogram_windowId_e histogramIndex, swl_print_args_t* args) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_histogramWindow_t* window = swl_histogram_getWindow(hist, histogramIndex);
    ASSERT_NOT_NULL(window, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_print_addOpen(file, args, false);

    for(size_t i = 0; i < hist->nrValues; i++) {
        if(i > 0) {
            swl_print_addNext(file, args, false);
        }
        if(hist->names != NULL) {
            swl_print_valueToStreamArgs(file, args, "%s", hist->names[i]);
        } else {
            swl_print_valueToStreamArgs(file, args, "%zu", i);
        }

        swl_print_addAssign(file, args);
        ASSERT_TRUE(swl_type_arrayToFilePrint(&gtSwl_type_uint16, file, &window->buckets[hist->offsetList[i]],
                                              hist->nrBucketsPerValue[i], true, args),
                    SWL_RC_ERROR, ME, "FAIL");
    }

    swl_print_addNext(file, args, false);
    swl_print_valueToStreamArgs(file, args, "%s", "StartTime");
    swl_print_addAssign(file, args);
    swl_type_toFile(&gtSwl_type_timeMono, file, &window->startTime, args);

    swl_print_addNext(file, args, false);
    swl_print_valueToStreamArgs(file, args, "%s", "EndTime");
    swl_print_addAssign(file, args);
    swl_type_toFile(&gtSwl_type_timeMono, file, &window->endTime, args);

    swl_print_addClose(file, args, false);
    return SWL_RC_OK;
}

/**
 * Print the window with the given window index to the file with given filename, based on provided print args.
 * Will append if append is set to true.
 */
swl_rc_ne swl_histogram_printWindowToFileName(swl_histogram_t* hist, const char* fileName,
                                              swl_histogram_windowId_e histogramIndex, swl_print_args_t* inArgs, bool append) {
    FILE* file = fopen(fileName, append ? "a" : "w");
    ASSERT_NOT_NULL(file, false, ME, "Fail to open file %s append %u", fileName, append);
    bool ret = swl_histogram_printWindowToFile(hist, file, histogramIndex, inArgs);
    fclose(file);
    return ret;
}

/**
 * Print the history to a file, as a map of lists.
 * The keys of the map shall be the names, if names are set, otherwise it sahll be the index of the bucket.
 */
swl_rc_ne swl_histogram_printHistoryToFile(swl_histogram_t* hist, FILE* file, swl_print_args_t* args) {
    ASSERT_NOT_NULL(hist, SWL_RC_INVALID_PARAM, ME, "NULL");


    swl_print_addOpen(file, args, false);
    size_t len = hist->historyData.filledHistorySize + 1; // Add one to have non zero min size;

    uint8_t historyBuf[len];
    memset(historyBuf, 0, len);

    for(size_t i = 0; i < hist->nrValues; i++) {
        if(i > 0) {
            swl_print_addNext(file, args, false);
        }
        if(hist->names != NULL) {
            swl_print_valueToStreamArgs(file, args, "%s", hist->names[i]);
        } else {
            swl_print_valueToStreamArgs(file, args, "%zu", i);
        }

        swl_print_addAssign(file, args);


        swl_histogram_getHistoryFromValue(hist, historyBuf, len, i, NULL);
        ASSERT_TRUE(swl_type_arrayToFilePrint(&gtSwl_type_uint8, file, historyBuf,
                                              hist->historyData.filledHistorySize, true, args),
                    SWL_RC_ERROR, ME, "FAIL");
    }

    swl_print_addClose(file, args, false);
    return SWL_RC_OK;
}

/**
 * Print history to a file with given filename, based on given args, as a map of lists.
 * The keys of the map shall be the names, if names are set, otherwise it sahll be the index of the bucket.
 * Will append to file if append bool is set to true.
 */
swl_rc_ne swl_histogram_printHistoryToFileName(swl_histogram_t* hist, const char* fileName, swl_print_args_t* args, bool append) {
    FILE* file = fopen(fileName, append ? "a" : "w");
    ASSERT_NOT_NULL(file, false, ME, "Fail to open file %s append %u", fileName, append);
    bool ret = swl_histogram_printHistoryToFile(hist, file, args);
    fclose(file);
    return ret;
}

