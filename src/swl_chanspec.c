/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "swl/swl_common_chanspec.h"
#include "swl/swl_assert.h"
#include "swl/swl_common_table.h"
#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"

#define ME "swlChS"

const char* swl_bandwidth_unknown_str[] = {"Unknown", "20MHz", "40MHz", "80MHz", "160MHz", "320MHz", "10MHz", "5MHz", "2MHz"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_bandwidth_unknown_str) == SWL_BW_MAX, "swl_bandwidth_unknown_str not correctly defined");

const char* swl_bandwidth_str[] = {"Auto", "20MHz", "40MHz", "80MHz", "160MHz", "320MHz", "10MHz", "5MHz", "2MHz"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_bandwidth_str) == SWL_BW_MAX, "swl_bandwidth_str not correctly defined");

const char* swl_bandwidth_intStr[SWL_BW_MAX] = {"0", "20", "40", "80", "160", "320", "10", "5", "2"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_bandwidth_intStr) == SWL_BW_MAX, "swl_bandwidth_intStr not correctly defined");

const uint32_t swl_bandwidth_int[] = {0, 20, 40, 80, 160, 320, 10, 5, 2};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_bandwidth_int) == SWL_BW_MAX, "swl_bandwidth_int not correctly defined");

SWL_TYPE_ENUM_C(gtSwl_type_bandwidthUnknown, swl_bandwidth_e, SWL_BW_AUTO, SWL_BW_MAX, swl_bandwidth_unknown_str);
SWL_TYPE_ENUM_C(gtSwl_type_bandwidth, swl_bandwidth_e, SWL_BW_AUTO, SWL_BW_MAX, swl_bandwidth_str);

const char* swl_freqBand_str[] = {"2.4GHz", "5GHz", "6GHz"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBand_str) == SWL_FREQ_BAND_MAX, "swl_freqBand_str not correctly defined");
const char* swl_freqBandShort_str[] = {"2g", "5g", "6g"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBandShort_str) == SWL_FREQ_BAND_MAX, "swl_freqBandShort_str not correctly defined");
const char* swl_freqBandNumberOnly_str[] = {"2.4", "5", "6"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBandNumberOnly_str) == SWL_FREQ_BAND_MAX, "swl_freqBandNumberOnly_str not correctly defined");
const char* swl_freqBandExt_unknown_str[] = {"2.4GHz", "5GHz", "6GHz", "Unknown", "Auto"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBandExt_unknown_str) == SWL_FREQ_BAND_EXT_MAX, "swl_freqBand_unknown_str not correctly defined");
const char* swl_freqBandExt_str[] = {"2.4GHz", "5GHz", "6GHz", "None", "Auto"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBandExt_str) == SWL_FREQ_BAND_EXT_MAX, "swl_freqBandExt_str not correctly defined");
const uint32_t swl_freqBandExt_int[] = {2, 5, 6, 0, -1};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBandExt_int) == SWL_FREQ_BAND_EXT_MAX, "swl_freqBandExt_str not correctly defined");

const swl_channel_t swl_channel_defaults[SWL_FREQ_BAND_MAX] = {1, 36, 1};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_channel_defaults) == SWL_FREQ_BAND_MAX, "swl_channel_defaults not correctly defined");

const swl_bandwidth_e swl_bandwidth_defaults[SWL_FREQ_BAND_MAX] = {SWL_BW_20MHZ, SWL_BW_80MHZ, SWL_BW_160MHZ};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_bandwidth_defaults) == SWL_FREQ_BAND_MAX, "swl_channel_defaults not correctly defined");

const swl_radioStandard_m swl_freqBand_radStd[] =
{
    M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE,  // #SWL_FREQ_BAND_2_4GHZ
    M_SWL_RADSTD_A | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX | M_SWL_RADSTD_BE, // #SWL_FREQ_BAND_5GHZ
    M_SWL_RADSTD_AX | M_SWL_RADSTD_BE,                                                     // #SWL_FREQ_BAND_6GHZ
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBand_radStd) == SWL_FREQ_BAND_MAX,
                  "update 'swl_radStd_supportedInFreqBand' when adding frequency bands");

/**
 * This is the first 802.11 standard to use a given frequency band.
 * A device operating in that band is assumed to support that standard.
 */
const swl_radStd_e swl_freqBand_legacyRadStd[] =
{
    SWL_RADSTD_B,  // #SWL_FREQ_BAND_2_4GHZ
    SWL_RADSTD_A,  // #SWL_FREQ_BAND_5GHZ
    SWL_RADSTD_AX, // #SWL_FREQ_BAND_6GHZ
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_freqBand_legacyRadStd) == SWL_FREQ_BAND_MAX,
                  "update 'swl_freqBand_legacyRadStd' when adding frequency bands");

const char* swl_uniiBand_str[] = {"Unknown", "U-NII-1", "U-NII-2A", "U-NII-2B", "U-NII-2C", "U-NII-3", "U-NII-4", "U-NII-5", "U-NII-6", "U-NII-7", "U-NII-8"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_uniiBand_str) == SWL_BAND_MAX, "swl_uniiBand_str not correctly defined");

/**
 * The radio bandwidth strings as defined in TR-181.
 */
const char* swl_radBw_str[] = {"Auto", "20MHz", "40MHz", "80MHz", "160MHz", "80+80MHz", "320MHz-1", "320MHz-2", "Max"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_radBw_str) == SWL_RAD_BW_MAX + 1, "swl_radBw_str not correctly defined");


// Shorter version of radBw strings, max 5 characters
const char* swl_radBw_strShort[] = {"Auto", "20", "40", "80", "160", "80+80", "320-1", "320-2", "Max"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_radBw_strShort) == SWL_RAD_BW_MAX + 1, "swl_radBw_strShort not correctly defined");

const swl_bandwidth_e swl_radBw_toBw[] = {SWL_BW_AUTO, SWL_BW_20MHZ, SWL_BW_40MHZ, SWL_BW_80MHZ, SWL_BW_160MHZ, SWL_BW_80MHZ,
    SWL_BW_320MHZ, SWL_BW_320MHZ, SWL_BW_MAX};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_radBw_toBw) == SWL_RAD_BW_MAX + 1, "swl_radBw_toBw not correctly defined");

SWL_TYPE_ENUM_C(gtSwl_type_freqBand, swl_freqBand_e, SWL_FREQ_BAND_2_4GHZ, SWL_FREQ_BAND_MAX, swl_freqBand_str);
SWL_TYPE_ENUM_C(gtSwl_type_freqBandExt, swl_freqBandExt_e, SWL_FREQ_BAND_EXT_NONE, SWL_FREQ_BAND_EXT_MAX, swl_freqBandExt_str);
SWL_TYPE_ENUM_C(gtSwl_type_freqBandExtUnknown, swl_freqBandExt_e, SWL_FREQ_BAND_EXT_NONE, SWL_FREQ_BAND_EXT_MAX, swl_freqBandExt_unknown_str);
SWL_TYPE_ENUM_C(swl_type_uniiBand_impl, swl_uniiBand_e, SWL_BAND_UNII_UNKNOWN, SWL_BAND_MAX, swl_uniiBand_str);

/* Annex E: Table E-4—Global operating classes */
static swl_operatingClassInfo_t sSwl_operatingClassInfoGlobal[] = {
    {81, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, (swl_channel_t[]) {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0
        }},
    {82, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, (swl_channel_t[]) {
            14, 0
        }},
    {83, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, (swl_channel_t[]) {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 0
        }},
    {84, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_2_4GHZ, (swl_channel_t[]) {
            5, 6, 7, 8, 9, 10, 11, 12, 13, 0
        }},
    {115, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            36, 40, 44, 48, 0
        }},
    {116, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            36, 44, 0
        }},
    {117, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            40, 48, 0
        }},
    {118, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            52, 56, 60, 64, 0
        }},
    {119, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            52, 60, 0
        }},
    {120, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            56, 64, 0
        }},
    {121, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, 0
        }},
    {122, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            100, 108, 116, 124, 132, 140, 0
        }},
    {123, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            104, 112, 120, 128, 136, 144, 0
        }},
    {124, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            149, 153, 157, 161, 0
        }},
    {125, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            149, 153, 157, 161, 165, 169, 173, 177, 0
        }},
    {126, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            149, 157, 165, 173, 0
        }},
    {127, false, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            153, 161, 169, 177, 0
        }},
    {128, true, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            42, 58, 106, 122, 138, 155, 171, 0
        }},
    {129, true, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
            50, 114, 163, 0
        }},
    /* {130, true, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_5GHZ, (swl_channel_t[]) {
     *         42, 58, 106, 122, 138, 155, 171, 0
     *    }}, */ /* 80+80 MHz ! */
    {131, false, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
            1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65,
            69, 73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121,
            125, 129, 133, 137, 141, 145, 149, 153, 157, 161, 165, 169, 173,
            177, 181, 185, 189, 193, 197, 201, 205, 209, 213, 217, 221, 225,
            229, 233, 0
        }},
    {132, true, SWL_BW_40MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
            3, 11, 19, 27, 35, 43, 51, 59, 67, 75, 83, 91, 99, 107, 115,
            123, 131, 139, 147, 155, 163, 171, 179, 187, 195, 203, 211,
            219, 227, 0
        }},
    {133, true, SWL_BW_80MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
            7, 23, 39, 55, 71, 87, 103, 119, 135, 151, 167, 183, 199,
            215, 0
        }},
    {134, true, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
            15, 47, 79, 111, 143, 175, 207, 0
        }},
    /* {135, true, SWL_BW_160MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
     *         7, 23, 39, 55, 71, 87, 103, 119, 135, 151, 167, 183, 199, 215, 0
     *    }}, */ /* 80+80 MHz ! */
    /* {136, true, SWL_BW_20MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
     *         2, 0
     *    }}, */ /* start frequency 5925 ! */
    {137, true, SWL_BW_320MHZ, SWL_FREQ_BAND_EXT_6GHZ, (swl_channel_t[]) {
            31, 63, 95, 127, 159, 191, 0
        }},
};

static const swl_operatingClassInfo_t* s_getOpClassInfo_byOpClass(swl_operatingClassInfo_t* table, size_t tableSize, swl_operatingClass_t operClass) {
    for(uint32_t i = 0; table && operClass && i < tableSize; i++) {
        if(table[i].operClass == operClass) {
            return &table[i];
        }
    }
    return NULL;
}

static const swl_operatingClassInfo_t* s_getOpClassInfo_byChSpec(swl_operatingClassInfo_t* table, size_t tableSize, swl_channel_t channel, swl_freqBandExt_e band, swl_bandwidth_e bandwidth) {
    for(uint32_t i = 0; table && i < tableSize; i++) {
        const swl_operatingClassInfo_t* entry = &table[i];
        if((entry->band == band) && (entry->bandwidth == bandwidth)) {
            swl_channel_t* channels = entry->channels;
            if(channels == NULL) {
                continue;
            }
            /* convert 20MHz Base Channel number to Center Channel number
             * if OperatingClass is defined to use so.
             */
            swl_chanspec_t chanSpec = SWL_CHANSPEC_NEW(channel, bandwidth, band);
            chanSpec.channel = !entry->isCenterChannelset ? channel :
                swl_chanspec_getCentreChannel(&chanSpec);
            for(uint8_t c = 0; channels[c] != 0; c++) {
                if(channels[c] == chanSpec.channel) {
                    return entry;
                }
            }
        }
    }
    return NULL;
}

static const swl_operatingClassInfo_t* s_getGlobalOpClassInfo_byOpClass(swl_operatingClass_t operClass) {
    return s_getOpClassInfo_byOpClass(sSwl_operatingClassInfoGlobal, SWL_ARRAY_SIZE(sSwl_operatingClassInfoGlobal), operClass);
}

static const swl_operatingClassInfo_t* s_getGlobalOpClassInfo_byChSpec(swl_channel_t channel, swl_freqBandExt_e band, swl_bandwidth_e chWidth) {
    return s_getOpClassInfo_byChSpec(sSwl_operatingClassInfoGlobal, SWL_ARRAY_SIZE(sSwl_operatingClassInfoGlobal), channel, band, chWidth);
}
/*
 * unii groups
 */
static swl_chanspec_uniiBandGroupInfo_t swl_chanspec_uniiBandGroups[] = {
    {.mask = M_SWL_BAND_UNII_1, .name = "UNII1"},
    {.mask = M_SWL_BAND_UNII_2A, .name = "UNII2A"},
    {.mask = M_SWL_BAND_UNII_2B, .name = "UNII2B"},
    {.mask = M_SWL_BAND_UNII_2C, .name = "UNII2C"},
    {.mask = M_SWL_BAND_UNII_3, .name = "UNII3"},
    {.mask = M_SWL_BAND_UNII_2, .name = "UNII2"},         //combination of UNII2A and UNII2C
    {.mask = M_SWL_BAND_UNII_1_2, .name = "UNII2-INC"},   //combination of UNII1 and UNII2
    {.mask = M_SWL_BAND_UNII_1_2A, .name = "UNII2A-INC"}, //combination of UNII1 and UNII2A
    {.mask = M_SWL_BAND_UNII_4, .name = "UNII4"},
    {.mask = M_SWL_BAND_UNII_5, .name = "UNII5"},
    {.mask = M_SWL_BAND_UNII_6, .name = "UNII6"},
    {.mask = M_SWL_BAND_UNII_7, .name = "UNII7"},
    {.mask = M_SWL_BAND_UNII_8, .name = "UNII8"},
    {.mask = M_SWL_BAND_UNII_ALL_5G, .name = "UNII_ALL_5G"},
    {.mask = M_SWL_BAND_UNII_ALL_6G, .name = "UNII_ALL_6G"},
};

//Operating  classes in Japan  802.11-2020.pdf E-3
static swl_operatingClassTranslation_t s_operClassTranslationTable [] = {
    {81, {12, 4, 30, 7}},
    {82, {0, 0, 31, 0}},
    {83, {32, 11, 56, 8}},
    {84, {33, 12, 57, 9}},
    {115, {1, 1, 1, 1}},
    {116, {22, 5, 36, 4}},
    {117, {27, 8, 41, 0}},
    {118, {2, 2, 32, 2}},
    {119, {23, 6, 37, 5}},
    {120, {28, 9, 42, 0 }},
    {121, {4, 3, 34, 0}},
    {122, {24, 7, 39, 0}},
    {123, {29, 10, 44, 0}},
    {124, {3, 0, 0, 0}},
    {125, {5, 17, 0, 3}},
    {126, {25, 0, 0, 6}},
    {127, {30, 0, 0, 0}},
    {128, {128, 128, 128, 128}},
    {129, {129, 129, 129, 129}},
    {131, {131, 131, 131, 131}},
    {132, {132, 132, 132, 132}},
    {133, {133, 133, 133, 133}},
    {134, {134, 134, 134, 134}},
    {137, {137, 137, 137, 137}},
};

SWL_TABLE(regulatoryDomainToCountry,
          ARR(char* regulatoryDomain; swl_opClassCountry_e country; ),
          ARR(swl_type_charPtr, swl_type_uint32),
          ARR({"00", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"AD", SWL_OP_CLASS_COUNTRY_EU},
              {"AE", SWL_OP_CLASS_COUNTRY_USA},
              {"AF", SWL_OP_CLASS_COUNTRY_EU},
              {"AI", SWL_OP_CLASS_COUNTRY_EU},
              {"AL", SWL_OP_CLASS_COUNTRY_EU},
              {"AM", SWL_OP_CLASS_COUNTRY_EU},
              {"AN", SWL_OP_CLASS_COUNTRY_EU},
              {"AR", SWL_OP_CLASS_COUNTRY_USA},
              {"AS", SWL_OP_CLASS_COUNTRY_USA},
              {"AT", SWL_OP_CLASS_COUNTRY_EU},
              {"AU", SWL_OP_CLASS_COUNTRY_EU},
              {"AW", SWL_OP_CLASS_COUNTRY_EU},
              {"AZ", SWL_OP_CLASS_COUNTRY_EU},
              {"BA", SWL_OP_CLASS_COUNTRY_EU},
              {"BB", SWL_OP_CLASS_COUNTRY_USA},
              {"BD", SWL_OP_CLASS_COUNTRY_JP},
              {"BE", SWL_OP_CLASS_COUNTRY_EU},
              {"BF", SWL_OP_CLASS_COUNTRY_USA},
              {"BG", SWL_OP_CLASS_COUNTRY_EU},
              {"BH", SWL_OP_CLASS_COUNTRY_JP},
              {"BL", SWL_OP_CLASS_COUNTRY_EU},
              {"BM", SWL_OP_CLASS_COUNTRY_USA},
              {"BN", SWL_OP_CLASS_COUNTRY_JP},
              {"BO", SWL_OP_CLASS_COUNTRY_JP},
              {"BR", SWL_OP_CLASS_COUNTRY_USA},
              {"BS", SWL_OP_CLASS_COUNTRY_USA},
              {"BT", SWL_OP_CLASS_COUNTRY_EU},
              {"BY", SWL_OP_CLASS_COUNTRY_EU},
              {"BZ", SWL_OP_CLASS_COUNTRY_JP},
              {"CA", SWL_OP_CLASS_COUNTRY_USA},
              {"CF", SWL_OP_CLASS_COUNTRY_USA},
              {"CH", SWL_OP_CLASS_COUNTRY_EU},
              {"CI", SWL_OP_CLASS_COUNTRY_USA},
              {"CL", SWL_OP_CLASS_COUNTRY_JP},
              {"CN", SWL_OP_CLASS_COUNTRY_CN},
              {"CO", SWL_OP_CLASS_COUNTRY_USA},
              {"CR", SWL_OP_CLASS_COUNTRY_USA},
              {"CU", SWL_OP_CLASS_COUNTRY_USA},
              {"CX", SWL_OP_CLASS_COUNTRY_USA},
              {"CY", SWL_OP_CLASS_COUNTRY_EU},
              {"CZ", SWL_OP_CLASS_COUNTRY_EU},
              {"DE", SWL_OP_CLASS_COUNTRY_EU},
              {"DK", SWL_OP_CLASS_COUNTRY_EU},
              {"DM", SWL_OP_CLASS_COUNTRY_USA},
              {"DO", SWL_OP_CLASS_COUNTRY_USA},
              {"DZ", SWL_OP_CLASS_COUNTRY_JP},
              {"EC", SWL_OP_CLASS_COUNTRY_USA},
              {"EE", SWL_OP_CLASS_COUNTRY_EU},
              {"EG", SWL_OP_CLASS_COUNTRY_EU},
              {"ES", SWL_OP_CLASS_COUNTRY_EU},
              {"ET", SWL_OP_CLASS_COUNTRY_EU},
              {"EU", SWL_OP_CLASS_COUNTRY_EU},
              {"FI", SWL_OP_CLASS_COUNTRY_EU},
              {"FM", SWL_OP_CLASS_COUNTRY_USA},
              {"FR", SWL_OP_CLASS_COUNTRY_EU},
              {"GB", SWL_OP_CLASS_COUNTRY_EU},
              {"GD", SWL_OP_CLASS_COUNTRY_USA},
              {"GE", SWL_OP_CLASS_COUNTRY_EU},
              {"GF", SWL_OP_CLASS_COUNTRY_EU},
              {"GH", SWL_OP_CLASS_COUNTRY_USA},
              {"GL", SWL_OP_CLASS_COUNTRY_EU},
              {"GP", SWL_OP_CLASS_COUNTRY_EU},
              {"GR", SWL_OP_CLASS_COUNTRY_EU},
              {"GT", SWL_OP_CLASS_COUNTRY_USA},
              {"GU", SWL_OP_CLASS_COUNTRY_USA},
              {"GY", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"HK", SWL_OP_CLASS_COUNTRY_EU},
              {"HN", SWL_OP_CLASS_COUNTRY_USA},
              {"HR", SWL_OP_CLASS_COUNTRY_EU},
              {"HT", SWL_OP_CLASS_COUNTRY_USA},
              {"HU", SWL_OP_CLASS_COUNTRY_EU},
              {"ID", SWL_OP_CLASS_COUNTRY_JP},
              {"IE", SWL_OP_CLASS_COUNTRY_EU},
              {"IN", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"IR", SWL_OP_CLASS_COUNTRY_JP},
              {"IS", SWL_OP_CLASS_COUNTRY_EU},
              {"IT", SWL_OP_CLASS_COUNTRY_EU},
              {"JM", SWL_OP_CLASS_COUNTRY_USA},
              {"JO", SWL_OP_CLASS_COUNTRY_JP},
              {"JP", SWL_OP_CLASS_COUNTRY_JP},
              {"KE", SWL_OP_CLASS_COUNTRY_JP},
              {"KH", SWL_OP_CLASS_COUNTRY_EU},
              {"KN", SWL_OP_CLASS_COUNTRY_EU},
              {"KP", SWL_OP_CLASS_COUNTRY_JP},
              {"KR", SWL_OP_CLASS_COUNTRY_JP},
              {"KW", SWL_OP_CLASS_COUNTRY_EU},
              {"KY", SWL_OP_CLASS_COUNTRY_USA},
              {"KZ", SWL_OP_CLASS_COUNTRY_EU},
              {"LB", SWL_OP_CLASS_COUNTRY_USA},
              {"LC", SWL_OP_CLASS_COUNTRY_EU},
              {"LI", SWL_OP_CLASS_COUNTRY_EU},
              {"LK", SWL_OP_CLASS_COUNTRY_USA},
              {"LS", SWL_OP_CLASS_COUNTRY_EU},
              {"LT", SWL_OP_CLASS_COUNTRY_EU},
              {"LU", SWL_OP_CLASS_COUNTRY_EU},
              {"LV", SWL_OP_CLASS_COUNTRY_EU},
              {"MA", SWL_OP_CLASS_COUNTRY_EU},
              {"MC", SWL_OP_CLASS_COUNTRY_EU},
              {"MD", SWL_OP_CLASS_COUNTRY_EU},
              {"ME", SWL_OP_CLASS_COUNTRY_EU},
              {"MF", SWL_OP_CLASS_COUNTRY_EU},
              {"MH", SWL_OP_CLASS_COUNTRY_USA},
              {"MK", SWL_OP_CLASS_COUNTRY_EU},
              {"MN", SWL_OP_CLASS_COUNTRY_USA},
              {"MO", SWL_OP_CLASS_COUNTRY_USA},
              {"MP", SWL_OP_CLASS_COUNTRY_USA},
              {"MQ", SWL_OP_CLASS_COUNTRY_EU},
              {"MR", SWL_OP_CLASS_COUNTRY_EU},
              {"MT", SWL_OP_CLASS_COUNTRY_EU},
              {"MU", SWL_OP_CLASS_COUNTRY_USA},
              {"MV", SWL_OP_CLASS_COUNTRY_EU},
              {"MW", SWL_OP_CLASS_COUNTRY_EU},
              {"MX", SWL_OP_CLASS_COUNTRY_USA},
              {"MY", SWL_OP_CLASS_COUNTRY_USA},
              {"NG", SWL_OP_CLASS_COUNTRY_EU},
              {"NI", SWL_OP_CLASS_COUNTRY_USA},
              {"NL", SWL_OP_CLASS_COUNTRY_EU},
              {"NO", SWL_OP_CLASS_COUNTRY_EU},
              {"NP", SWL_OP_CLASS_COUNTRY_JP},
              {"NZ", SWL_OP_CLASS_COUNTRY_EU},
              {"OM", SWL_OP_CLASS_COUNTRY_EU},
              {"PA", SWL_OP_CLASS_COUNTRY_USA},
              {"PE", SWL_OP_CLASS_COUNTRY_USA},
              {"PF", SWL_OP_CLASS_COUNTRY_EU},
              {"PG", SWL_OP_CLASS_COUNTRY_USA},
              {"PH", SWL_OP_CLASS_COUNTRY_USA},
              {"PK", SWL_OP_CLASS_COUNTRY_JP},
              {"PL", SWL_OP_CLASS_COUNTRY_EU},
              {"PM", SWL_OP_CLASS_COUNTRY_EU},
              {"PR", SWL_OP_CLASS_COUNTRY_USA},
              {"PS", SWL_OP_CLASS_COUNTRY_USA},
              {"PT", SWL_OP_CLASS_COUNTRY_EU},
              {"PW", SWL_OP_CLASS_COUNTRY_USA},
              {"PY", SWL_OP_CLASS_COUNTRY_USA},
              {"QA", SWL_OP_CLASS_COUNTRY_EU},
              {"RE", SWL_OP_CLASS_COUNTRY_EU},
              {"RO", SWL_OP_CLASS_COUNTRY_EU},
              {"RS", SWL_OP_CLASS_COUNTRY_EU},
              {"RU", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"RW", SWL_OP_CLASS_COUNTRY_USA},
              {"SA", SWL_OP_CLASS_COUNTRY_EU},
              {"SE", SWL_OP_CLASS_COUNTRY_EU},
              {"SG", SWL_OP_CLASS_COUNTRY_USA},
              {"SI", SWL_OP_CLASS_COUNTRY_EU},
              {"SK", SWL_OP_CLASS_COUNTRY_EU},
              {"SN", SWL_OP_CLASS_COUNTRY_USA},
              {"SR", SWL_OP_CLASS_COUNTRY_EU},
              {"SV", SWL_OP_CLASS_COUNTRY_USA},
              {"SY", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"TC", SWL_OP_CLASS_COUNTRY_USA},
              {"TD", SWL_OP_CLASS_COUNTRY_EU},
              {"TG", SWL_OP_CLASS_COUNTRY_EU},
              {"TH", SWL_OP_CLASS_COUNTRY_USA},
              {"TN", SWL_OP_CLASS_COUNTRY_EU},
              {"TR", SWL_OP_CLASS_COUNTRY_EU},
              {"TT", SWL_OP_CLASS_COUNTRY_USA},
              {"TW", SWL_OP_CLASS_COUNTRY_USA},
              {"TZ", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"UA", SWL_OP_CLASS_COUNTRY_EU},
              {"UG", SWL_OP_CLASS_COUNTRY_USA},
              {"US", SWL_OP_CLASS_COUNTRY_USA},
              {"UY", SWL_OP_CLASS_COUNTRY_USA},
              {"UZ", SWL_OP_CLASS_COUNTRY_EU},
              {"VC", SWL_OP_CLASS_COUNTRY_EU},
              {"VE", SWL_OP_CLASS_COUNTRY_USA},
              {"VI", SWL_OP_CLASS_COUNTRY_USA},
              {"VN", SWL_OP_CLASS_COUNTRY_USA},
              {"VU", SWL_OP_CLASS_COUNTRY_USA},
              {"WF", SWL_OP_CLASS_COUNTRY_EU},
              {"WS", SWL_OP_CLASS_COUNTRY_EU},
              {"YE", SWL_OP_CLASS_COUNTRY_UNKNOWN},
              {"YT", SWL_OP_CLASS_COUNTRY_EU},
              {"ZA", SWL_OP_CLASS_COUNTRY_EU},
              {"ZW", SWL_OP_CLASS_COUNTRY_EU})
          );

const char* swl_opClassRegion_str[] = {"USA", "EU", "JP", "CN", "UNKNOWN"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_opClassRegion_str) == SWL_OP_CLASS_COUNTRY_MAX, "swl_opClassRegion_str not correctly defined");

/**
 * @param swlLocalOpertingClass: swl_operatingClass_t local operating class for the given country
 * @param countryIndex: swl_opClassCountry_e index of the given country
 * @return global operating class in swl_operatingClass_t
 */
swl_operatingClass_t swl_chanspec_getGlobalOperClassfromLocal(swl_operatingClass_t localOpertingClass, swl_opClassCountry_e countryIndex) {
    if((countryIndex < SWL_OP_CLASS_COUNTRY_MAX) && (localOpertingClass > 0)) {
        for(uint32_t i = 0; i < SWL_ARRAY_SIZE(s_operClassTranslationTable); i++) {
            if(((countryIndex == SWL_OP_CLASS_COUNTRY_UNKNOWN) &&
                (s_operClassTranslationTable[i].global == localOpertingClass)) ||
               ((countryIndex != SWL_OP_CLASS_COUNTRY_UNKNOWN) &&
                (s_operClassTranslationTable[i].local_set[countryIndex] == localOpertingClass))) {
                return s_operClassTranslationTable[i].global;
            }
        }
    }
    SAH_TRACEZ_INFO(ME, "no global operatingclass found for local value (%d)", localOpertingClass);
    return 0;

}

/* Operating classes per region are now supported */
swl_freqBandExt_e swl_chanspec_localOperClassToFreq(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex) {
    swl_operatingClass_t GlobalOperatingClass = swl_chanspec_getGlobalOperClassfromLocal(operclass, countryIndex);
    return swl_chanspec_operClassToFreq(GlobalOperatingClass);
}

swl_phymode_ne swl_chanspec_localOperClassToPhyMode(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex) {
    swl_operatingClass_t GlobalOperatingClass = swl_chanspec_getGlobalOperClassfromLocal(operclass, countryIndex);
    return swl_chanspec_operClassToPhyMode(GlobalOperatingClass);
}

/* See 802.11ax-2021 Table E-* Global operating classes */
swl_uniiBand_m swl_chanspec_localOperClassToUniiMask(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex) {
    swl_operatingClass_t GlobalOperatingClass = swl_chanspec_getGlobalOperClassfromLocal(operclass, countryIndex);
    return swl_chanspec_operClassToUniiMask(GlobalOperatingClass);
}

/* See 802.11-2020 / 802.11ax-2021 Table E-* Global operating classes */
swl_bandwidth_e swl_chanspec_localOperClassToBandwidth(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex) {
    swl_operatingClass_t GlobalOperatingClass = swl_chanspec_getGlobalOperClassfromLocal(operclass, countryIndex);
    return swl_chanspec_operClassToBandwidth(GlobalOperatingClass);
}

/**
 * Return the country in enum format from a regulatory domain string
 * @param string: regulatory domain to be translated
 * @return enum of value of a country
 */
swl_opClassCountry_e swl_chanspec_regulatoryDomainToCountry(const char* regulatoryDomain) {
    ASSERTI_STR(regulatoryDomain, SWL_OP_CLASS_COUNTRY_UNKNOWN, ME, "Empty country name");
    swl_opClassCountry_e* country = (swl_opClassCountry_e*) swl_table_getMatchingValue(&regulatoryDomainToCountry, 1, 0, regulatoryDomain);
    ASSERTI_NOT_NULL(country, SWL_OP_CLASS_COUNTRY_UNKNOWN, ME, "No operating class region for country %s", regulatoryDomain);
    return *country;
}

/*
 * @brief find all matching operCall chanspec info based on provided :
 * - operating class (global/local)
 * - country region (when known, must fit the operating class)
 * - channel (primary/central) (optional: when not 0, it must fit the operating class)
 *
 * @param pResults (output) filled with deduced operating call region info (region id, opClass inf: band, chWidth, channels)
 * @param nMaxResults (in) max number of results
 * @param opClass (input) global/local operating class
 * @param opClassRegion (input) regulatory region of operating class,
 * used to improve deduction when opClass number exists in different regions
 * @param channel primary/central channel used to help defining the target frequency band
 * @param allowMultiMatch (input) deduce chanspec on first opClass match, even when defined in multiple regions
 * (only allowed when guessing chanspec: ie neither opClassRegion nor channel are known)
 *
 * @return number of chanspec match when deduction is successful
 *         of 0 when no results, or exceeding max number of results
 */
uint32_t swl_chanspec_findAllOperClassCountryInfo(swl_operatingClassCountryInfo_t* pResults, size_t nMaxResults,
                                                  swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion,
                                                  swl_channel_t channel) {
    uint32_t nResults = 0;
    swl_operatingClassCountryInfo_t results[SWL_OP_CLASS_COUNTRY_MAX];
    memset(results, 0, sizeof(results));

    ASSERT_TRUE(opClassRegion < SWL_OP_CLASS_COUNTRY_MAX, nResults, ME, "invalid region");
    ASSERTS_NOT_NULL(pResults, nResults, ME, "NULL");
    ASSERTS_TRUE(nMaxResults > 0, nResults, ME, "Empty");
    ASSERT_TRUE(opClass > 0, nResults, ME, "null opClass");

    swl_opClassCountry_e calcRegion = SWL_OP_CLASS_COUNTRY_UNKNOWN;
    const swl_operatingClassInfo_t* pGlobOpClassInfo = NULL;

    // 0) if country index is provided, then check first with relative region opclass table
    if(opClassRegion != SWL_OP_CLASS_COUNTRY_UNKNOWN) {
        calcRegion = opClassRegion;
        pGlobOpClassInfo = s_getGlobalOpClassInfo_byOpClass(swl_chanspec_getGlobalOperClassfromLocal(opClass, opClassRegion));
        if(pGlobOpClassInfo == NULL) {
            calcRegion = SWL_OP_CLASS_COUNTRY_UNKNOWN;
        }
    }

    // 1) if country index is unknown, then try with global region op_class table
    if(calcRegion == SWL_OP_CLASS_COUNTRY_UNKNOWN) {
        pGlobOpClassInfo = s_getGlobalOpClassInfo_byOpClass(opClass);
    }

    // 2) operating class identified with region, but must match the relative chanspec
    if(pGlobOpClassInfo != NULL) {
        if((!channel) ||
           (pGlobOpClassInfo == s_getGlobalOpClassInfo_byChSpec(channel, pGlobOpClassInfo->band, pGlobOpClassInfo->bandwidth))) {
            pResults[nResults++] = (swl_operatingClassCountryInfo_t) {calcRegion, pGlobOpClassInfo};
            return nResults;
        }
    }
    // 3) go through all regions and fetch mapped global operating class having valid and unique chanspec
    for(uint32_t i = 0; i < SWL_OP_CLASS_COUNTRY_UNKNOWN; i++) {
        swl_operatingClass_t tmpGlobOpClass = swl_chanspec_getGlobalOperClassfromLocal(opClass, i);
        if(!tmpGlobOpClass) {
            continue;
        }
        const swl_operatingClassInfo_t* pTmpGlobOpClassInfo = s_getGlobalOpClassInfo_byOpClass(tmpGlobOpClass);
        if(pTmpGlobOpClassInfo == NULL) {
            continue;
        }
        //skip entries matching freqBand and chWidth , but not the channel
        if((channel > 0) && (pTmpGlobOpClassInfo != s_getGlobalOpClassInfo_byChSpec(channel, pTmpGlobOpClassInfo->band, pTmpGlobOpClassInfo->bandwidth))) {
            continue;
        }
        results[nResults++] = (swl_operatingClassCountryInfo_t) {i, pTmpGlobOpClassInfo};
    }

    if(!nResults) {
        SAH_TRACEZ_ERROR(ME, "unmatch input(opClass:%d,region:%s,chan:%d) calc(region:%s)",
                         opClass, swl_opClassRegion_str[opClassRegion], channel, swl_opClassRegion_str[calcRegion]);
    } else if(nResults > 1) {
        char buf[128] = {0};
        for(uint32_t i = 0; i < nResults; i++) {
            swl_strlst_catFormat(buf, sizeof(buf), ",", "%s:%s",
                                 swl_typeChanspecExt_toBuf32((swl_chanspec_t) SWL_CHANSPEC_NEW(channel, results[i].operClassInfo->bandwidth, results[i].operClassInfo->band)).buf,
                                 swl_opClassRegion_str[results[i].countryRegion]);
        }
        SAH_TRACEZ_INFO(ME, "opclass(%d)/region(%s)/channel(%d) has multiple chanspec match (%d / max:%zu) (%s)",
                        opClass, swl_opClassRegion_str[opClassRegion], channel, nResults, nMaxResults, buf);
        if(nResults > nMaxResults) {
            nResults = 0;
        }
    }

    for(uint32_t i = 0; i < nMaxResults; i++) {
        pResults[i] = (swl_operatingClassCountryInfo_t) {SWL_OP_CLASS_COUNTRY_UNKNOWN, NULL};
        if(i < nResults) {
            pResults[i] = results[i];
        }
    }

    return nResults;
}

/*
 * @brief deduce all chanspec matches based on provided :
 * - operating class (global/local)
 * - country region (when known, must fit the operating class)
 * - channel (primary/central) (optional: when not 0, it must fit the operating class)
 *
 * @param pChSpecs (output) filled with all deduced chanspecs
 * @param nMaxChSpecs (in) max number of results
 * @param opClass (input) global/local operating class
 * @param opClassRegion (input) regulatory region of operating class,
 * used to improve deduction when opClass number exists in different regions
 * @param channel primary/central channel used to help defining the target frequency band
 *
 * @return number of chanspec match when deduction is successful
 *         of 0 when no results, or exceeding max number of results
 */
uint32_t swl_chanspec_findAllOperClassMatches(swl_chanspec_t* pChSpecs, size_t nMaxChSpecs, swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel) {
    ASSERTS_TRUE(nMaxChSpecs > 0, 0, ME, "empty");
    swl_operatingClassCountryInfo_t results[nMaxChSpecs];
    uint32_t nResults = swl_chanspec_findAllOperClassCountryInfo(results, nMaxChSpecs, opClass, opClassRegion, channel);
    for(uint32_t i = 0; i < nMaxChSpecs; i++) {
        pChSpecs[i] = (swl_chanspec_t) SWL_CHANSPEC_EMPTY;
        if(i < nResults) {
            swl_operatingClassCountryInfo_t* pEntry = &results[i];
            pChSpecs[i] = (swl_chanspec_t) SWL_CHANSPEC_NEW(channel, pEntry->operClassInfo->bandwidth, pEntry->operClassInfo->band);
        }
    }
    return nResults;
}

/*
 * @brief get one chanspec match based on provided :
 * - operating class (global/local)
 * - country region (when known, must fit the operating class)
 * - channel (primary/central) (optional: when not 0, it must fit the operating class)
 *
 * @param opClass (input) global/local operating class
 * @param opClassRegion (input) regulatory region of operating class,
 * used to improve deduction when opClass number exists in different regions
 * @param channel primary/central channel used to help defining the target frequency band
 * @param useFirstMatch (input) deduce chanspec on first opClass match, even when defined in multiple regions
 * (only allowed when guessing chanspec: ie neither opClassRegion nor channel are known)
 *
 * @return deduced chanspec on success
 *         or CHANSPEC_EMPTY on error
 */
swl_chanspec_t swl_chanspec_fromOperClassExt(swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel, bool useFirstMatch) {
    uint32_t nMaxChSpecs = useFirstMatch ? SWL_OP_CLASS_COUNTRY_MAX : 1;
    swl_chanspec_t chSpecs[nMaxChSpecs];
    swl_chanspec_findAllOperClassMatches(chSpecs, nMaxChSpecs, opClass, opClassRegion, channel);
    return chSpecs[0];
}

/*
 * @brief get unique chanspec match based on provided :
 * - operating class (global/local)
 * - country region (when known, must fit the operating class)
 * - channel (primary/central) (optional: when not 0, it must fit the operating class)
 *
 * @param opClass (input) global/local operating class
 * @param opClassRegion (input) regulatory region of operating class,
 * used to improve deduction when opClass number exists in different regions
 * @param channel primary/central channel used to help defining the target frequency band
 *
 * @return deduced chanspec on success
 *         or CHANSPEC_EMPTY on error
 */
swl_chanspec_t swl_chanspec_fromOperClass(swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel) {
    return swl_chanspec_fromOperClassExt(opClass, opClassRegion, channel, false);
}

/**
 * @param swlBw: swl_bandwidth_e type of bandwidth
 * @return bandwidth in Integer value (MHz)
 */
int32_t swl_chanspec_bwToInt(swl_bandwidth_e swlBw) {
    return swl_bandwidth_int[swlBw];

}

/**
 * Convert a radio bandwidth, to the bandwidth integer of its main operating channel.
 * Note that in 80+80 case, this returns 80.
 * In all other cases, it returns the obvious value
 * @param swlBw: radio bandwidth
 * @return integer showing bandwidth in MHz.
 */
int32_t swl_chanspec_radBwToInt(swl_radBw_e swlBw) {
    return swl_bandwidth_int[swl_radBw_toBw[swlBw]];

}

/**
 * @param intBw: Integer value of bandwidth in MHz
 * @return swl_bandwidth_e type
 */
swl_bandwidth_e swl_chanspec_intToBw(uint32_t intBw) {
    for(int i = 0; i < SWL_BW_MAX; i++) {
        if(swl_bandwidth_int[i] == intBw) {
            return i;
        }
    }
    return SWL_BW_AUTO;
}


/**
 * @param swlBand: swl_freqBandExt_e type of band
 * @return band in Integer value (MHz)
 */
int32_t swl_chanspec_freqBandExtToInt(swl_freqBandExt_e swlBand) {
    return swl_freqBandExt_int[swlBand];

}
/**
 * @param intBand: Integer value of band in GHz
 * @return swl_freqBandExt_e type
 *         SWL_FREQ_BAND_EXT_MAX is a value that is preferred not to be returned,
 *         as it does not have a matching value.
 */
swl_freqBandExt_e swl_chanspec_intTofreqBandExt(uint32_t intBand) {
    for(int i = 0; i < SWL_FREQ_BAND_EXT_MAX; i++) {
        if(swl_freqBandExt_int[i] == intBand) {
            return i;
        }
    }
    return SWL_FREQ_BAND_EXT_NONE;
}

bool s_chanspec_compare_cb(swl_type_t* type _UNUSED, const swl_chanspec_t* src, const swl_chanspec_t* key) {
    ASSERTS_NOT_NULL(src, false, ME, "NULL");
    ASSERTS_NOT_NULL(key, false, ME, "NULL");

    if(src->channel != key->channel) {
        return false;
    }
    if(src->bandwidth != key->bandwidth) {
        return false;
    }
    if((src->bandwidth == SWL_BW_80MHZ) && (src->secondChannel != key->secondChannel)) {
        return false;
    } else if(((src->bandwidth == SWL_BW_40MHZ) || (src->bandwidth == SWL_BW_320MHZ))
              && (src->extensionHigh != key->extensionHigh)) {
        return false;
    }
    return true;
}

static ssize_t s_chanspec_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    swl_chanspec_t chanspec = *(swl_chanspec_t*) srcData;

    char extBuffer[16] = {0};

    if((chanspec.band == SWL_FREQ_BAND_EXT_2_4GHZ) && (chanspec.bandwidth == SWL_BW_40MHZ) && (chanspec.extensionHigh != SWL_CHANSPEC_EXT_AUTO)) {
        snprintf(extBuffer, sizeof(extBuffer), "%s", chanspec.extensionHigh == SWL_CHANSPEC_EXT_HIGH ? "-A" : "-B");
    } else if((chanspec.bandwidth == SWL_BW_80MHZ) && (chanspec.secondChannel != 0)) {
        snprintf(extBuffer, sizeof(extBuffer), "-%d", chanspec.secondChannel);
    } else if(chanspec.bandwidth == SWL_BW_320MHZ) {
        if(chanspec.extensionHigh == SWL_CHANSPEC_EXT_HIGH) {
            snprintf(extBuffer, sizeof(extBuffer), "-2");
        } else if(chanspec.extensionHigh == SWL_CHANSPEC_EXT_LOW) {
            snprintf(extBuffer, sizeof(extBuffer), "-1");
        }

    }

    ssize_t ret = snprintf(tgtStr, tgtStrSize, "%d/%s%s",
                           chanspec.channel,
                           swl_bandwidth_intStr[chanspec.bandwidth],
                           extBuffer);
    return ret;
}

static bool s_parseChanspec_common(swl_chanspec_t* tgtData, char* buf) {
    char* base = &buf[0];
    char* ptr = strchr(base, '/');
    ASSERTS_NOT_NULL(ptr, false, ME, "Syntax Error");
    ASSERTS_TRUE((ptr - base) > 0, false, ME, "Syntax Error");

    *ptr = '\0';
    tgtData->channel = atoi(base);
    ASSERTS_TRUE(tgtData->channel > 0, false, ME, "Syntax Error");


    base = ptr + 1;
    ptr = strchr(base, '-');
    if(ptr != NULL) {
        *ptr = '\0';
    }
    ASSERTS_TRUE(isdigit(base[0]), false, ME, "Syntax Error");
    tgtData->bandwidth = swl_chanspec_intToBw(atoi(base));
    ASSERTS_TRUE(tgtData->bandwidth < SWL_BW_MAX, false, ME, "Syntax Error");


    if(ptr != NULL) {
        base = ptr + 1;
        ASSERTS_TRUE(base[0] != '\0', false, ME, "Syntax Error");
        ASSERTS_TRUE(base[1] == '\0', false, ME, "Syntax Error");
    } else {
        ptr = strchr(base, '\0');
        ASSERTS_TRUE(ptr != NULL, false, ME, "Syntax Error");
        ASSERTS_TRUE((ptr - base) < 4, false, ME, "Syntax Error");
        ptr = NULL;
    }


    if((tgtData->bandwidth == SWL_BW_80MHZ) && (ptr != NULL)) {
        tgtData->secondChannel = atoi(base);
        ASSERTS_TRUE(tgtData->secondChannel > 0, false, ME, "Syntax Error");
    } else if(tgtData->bandwidth == SWL_BW_40MHZ) {
        if(ptr == NULL) {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_AUTO;
        } else if(base[0] == 'A') {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_HIGH;
        } else if(base[0] == 'B') {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_LOW;
        } else {
            ASSERTS_TRUE(false, false, ME, "Syntax Error");
        }
    } else if(tgtData->bandwidth == SWL_BW_320MHZ) {
        //Special case, two values map on same thing.
        if(ptr == NULL) {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_AUTO;
        }
        if(base[0] == '1') {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_LOW;
        } else if(base[0] == '2') {
            tgtData->extensionHigh = SWL_CHANSPEC_EXT_HIGH;
        } else {
            ASSERTS_TRUE(false, false, ME, "Syntax Error");
        }
    }

    return true;
}

/**
 * converts a src string to an swl_chanspec, ignoring frequencyBand.
 * This function will NOT touch the frequency band in any way, so freqBand will remain as set.
 */
static bool s_chanspec_fromChar_cb(swl_type_t* type _UNUSED, swl_chanspec_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    char buf[32] = {0};

    swl_str_copy(buf, sizeof(buf), srcStr);
    return s_parseChanspec_common(tgtData, buf);
}

/**
 * Chanspec encodes a channel/band combination. It does NOT encode the frequencyBand, although it
 * is part of the structure that it operates on.
 * Reading and writing will ignore frequencyBand. Copy will copy entire structure including freqBand.
 */
swl_typeFun_t swl_type_chanspec_fun = {
    .name = "swl_chanspec",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_chanspec_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_chanspec_fromChar_cb,
    .equals = (swl_type_equals_cb) s_chanspec_compare_cb
};
swl_type_t gtSwl_type_chanspec = {
    .typeFun = &swl_type_chanspec_fun,
    .size = sizeof(swl_chanspec_t),
};

static ssize_t s_chanspecExt_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    swl_chanspec_t chanspec = *(swl_chanspec_t*) srcData;
    char buffer[16] = {0};
    ssize_t ret1 = s_chanspec_toChar_cb(type, buffer, sizeof(buffer), srcData);
    if(ret1 < 0) {
        return ret1;
    }

    ssize_t ret2 = snprintf(tgtStr, tgtStrSize, "%d_%s",
                            swl_chanspec_freqBandExtToInt(chanspec.band),
                            buffer);
    return ret2;
}

static bool s_chanspecExt_fromChar_cb(swl_type_t* type _UNUSED, swl_chanspec_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    char buf[32] = {0};

    swl_str_copy(buf, sizeof(buf), srcStr);

    char* base = &buf[0];
    char* ptr = strchr(base, '_');
    if((ptr == NULL) || (ptr == base)) {
        return false;
    }
    *ptr = '\0';
    tgtData->band = swl_chanspec_intTofreqBandExt(atoi(base));
    if(tgtData->band == SWL_FREQ_BAND_EXT_NONE) {
        return false;
    }
    base = ptr + 1;
    return s_parseChanspec_common(tgtData, base);
}

/**
 * Compare two swl_chanspec_t entirely.
 * Note that we can NOT do a memcmp, because channel is only 8 bit. If we do a memcmp, and allocate
 * a chanspec on the stack with the SWL_CHANSPEC_NEW function, the 3 bytes that occur after channel
 * will potentially contain garbage, meaning memcmp may fail unexpectedly.
 * Setting channel last also does not work, as compiler auto alligns. Setting to packed will work, but
 * that may reduce access time.
 */
bool s_chanspecExt_compare_cb(swl_type_t* type _UNUSED, const swl_chanspec_t* src, const swl_chanspec_t* key) {
    ASSERTS_NOT_NULL(src, false, ME, "NULL");
    ASSERTS_NOT_NULL(key, false, ME, "NULL");

    if(src->band != key->band) {
        return false;
    }
    return s_chanspec_compare_cb(type, src, key);
}

swl_typeFun_t swl_type_chanspecExt_fun = {
    .name = "swl_chanspecExt",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_chanspecExt_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_chanspecExt_fromChar_cb,
    .equals = (swl_type_equals_cb) s_chanspecExt_compare_cb
};
swl_type_t gtSwl_type_chanspecExt = {
    .typeFun = &swl_type_chanspecExt_fun,
    .size = sizeof(swl_chanspec_t)
};


/**
 * Function used to parse the comma seperated channels spec list and returns the swl_chanspec_t
 * @param srcStrList: comma separated channel spec list in the format of <channel>/<(int)bandwidth>
 * @param count: Maximum size of the List
 * @param chanSpec: Type of swl_chanspec_t to get the items and values as an integer
 */
int32_t swl_chanspec_arrayFromChar(swl_chanspec_t* chanSpec, uint8_t count, const char* srcStrList) {
    ASSERT_NOT_NULL(srcStrList, -1, ME, "NULL");
    ASSERT_NOT_NULL(chanSpec, -1, ME, "NULL");
    ASSERT_TRUE(count > 0, -1, ME, "null List");

    int len = 0;
    unsigned int i = 0;
    char* mainstr = strdup(srcStrList);
    char* strPtr = NULL;
    char substr[32] = {0};
    ASSERT_NOT_NULL(mainstr, -1, ME, "Alloc Failed");
    strPtr = mainstr;
    do {
        if(i >= count) {
            SAH_TRACEZ_ERROR(ME, "Source Str List is too large(%d,%d)", i, count);
            free(strPtr);
            return -1;
        }
        len = strcspn(mainstr, ",");
        snprintf(substr, sizeof(substr), "%.*s", (int) len, mainstr);
        if(s_parseChanspec_common(&chanSpec[i], substr) == false) {
            SAH_TRACEZ_ERROR(ME, "Syntax Error");
            free(strPtr);
            return -2;
        }
        i++;
        mainstr += len;
    } while(*mainstr++);

    free(strPtr);
    return i;

}

/**
 * Function used to get the Array of swl_chanspec_t and provide comma seperated channel spec list
 * @param srcData: swl_chanspec_t type of elements
 * @param srcDataSize: Number of elements available in srcData
 * @param tgtStrSize: buffer size of tgtStr
 * @param tgtStr: Output contains the comma seperated list
 */
bool swl_chanspec_arrayToChar(char* tgtStr, uint32_t tgtStrSize, swl_chanspec_t* srcData, size_t srcDataSize) {
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    uint32_t i = 0;
    uint32_t index = 0;
    int printed = 0;
    tgtStr[0] = '\0';

    for(i = 0; i < srcDataSize; i++) {
        if(index) {
            printed = snprintf(&tgtStr[index], tgtStrSize - index, ",%d/%d", srcData[i].channel, swl_chanspec_bwToInt(srcData[i].bandwidth));
        } else {
            printed = snprintf(&tgtStr[index], tgtStrSize - index, "%d/%d", srcData[i].channel, swl_chanspec_bwToInt(srcData[i].bandwidth));
        }
        index += printed;
        ASSERT_TRUE(index < tgtStrSize, false, ME, "Buff too small");
    }
    return true;
}

swl_uniiBand_e swl_chanspec_toUnii(const swl_chanspec_t* chanspec) {
    ASSERTS_NOT_NULL(chanspec, 0, ME, "NULL");
    if(chanspec->band == SWL_FREQ_BAND_EXT_2_4GHZ) {
        return SWL_BAND_UNII_UNKNOWN;
    } else if(chanspec->band == SWL_FREQ_BAND_EXT_5GHZ) {
        if((chanspec->channel >= 36) && (chanspec->channel <= 48)) {
            return SWL_BAND_UNII_1;
        } else if((chanspec->channel >= 52) && (chanspec->channel <= 68)) {
            return SWL_BAND_UNII_2A;
        } else if((chanspec->channel >= 70) && (chanspec->channel <= 96)) {
            return SWL_BAND_UNII_2B;
        } else if((chanspec->channel >= 100) && (chanspec->channel <= 144)) {
            return SWL_BAND_UNII_2C;
        } else if((chanspec->channel >= 149) && (chanspec->channel <= 165)) {
            return SWL_BAND_UNII_3;
        } else if((chanspec->channel >= 167) && (chanspec->channel <= 181)) {
            return SWL_BAND_UNII_4;
        }
    } else if(chanspec->band == SWL_FREQ_BAND_EXT_6GHZ) {
        if((chanspec->channel >= 1) && (chanspec->channel <= 93)) {
            return SWL_BAND_UNII_5;
        } else if((chanspec->channel >= 97) && (chanspec->channel <= 113)) {
            return SWL_BAND_UNII_6;
        } else if((chanspec->channel >= 117) && (chanspec->channel <= 185)) {
            return SWL_BAND_UNII_7;
        } else if((chanspec->channel >= 189) && (chanspec->channel <= 233)) {
            return SWL_BAND_UNII_8;
        }
    }
    SAH_TRACEZ_INFO(ME, "Unsupported channel/band %d/%d", chanspec->channel, chanspec->band);
    return SWL_BAND_UNII_UNKNOWN;
}

/* Only global operating classes are supported for now, operating classes per region are not yet listed */
swl_freqBandExt_e swl_chanspec_operClassToFreq(swl_operatingClass_t operclass) {
    const swl_operatingClassInfo_t* pOpCInf = s_getGlobalOpClassInfo_byOpClass(operclass);
    if(pOpCInf) {
        return pOpCInf->band;
    }
    SAH_TRACEZ_INFO(ME, "Unsupported operating class %d", operclass);
    return SWL_FREQ_BAND_EXT_NONE;
}

swl_phymode_ne swl_chanspec_operClassToPhyMode(swl_operatingClass_t operclass) {
    if((operclass >= 81) && (operclass <= 84)) {
        return SWL_PHYMODE_HT;
    } else if((operclass >= 115) && (operclass <= 130)) {
        return SWL_PHYMODE_VHT;
    } else if((operclass >= 131) && (operclass <= 136)) {
        return SWL_PHYMODE_HE;
    } else if((operclass == 137)) {
        return SWL_PHYMODE_EHT;
    }
    SAH_TRACEZ_INFO(ME, "Unsupported operating class %d", operclass);
    return SWL_PHYMODE_UNKNOWN;
}

/* See 802.11ax-2021 Table E-4 Global operating classes */
swl_uniiBand_m swl_chanspec_operClassToUniiMask(swl_operatingClass_t operclass) {
    if((operclass >= 115) && (operclass <= 117)) {
        return M_SWL_BAND_UNII_1;
    } else if((operclass >= 118) && (operclass <= 120)) {
        return M_SWL_BAND_UNII_2A;
    } else if((operclass >= 121) && (operclass <= 123)) {
        return M_SWL_BAND_UNII_2C;
    } else if(operclass == 124) {
        return M_SWL_BAND_UNII_3;
    } else if((operclass >= 125) && (operclass <= 127)) {
        return M_SWL_BAND_UNII_3 | M_SWL_BAND_UNII_4;
    } else if((operclass >= 128) && (operclass <= 130)) {
        return M_SWL_BAND_UNII_ALL_5G;
    } else if((operclass >= 131) && (operclass <= 135)) {
        return M_SWL_BAND_UNII_ALL_6G;
    } else if(operclass == 136) {
        return M_SWL_BAND_UNII_5;
    }
    SAH_TRACEZ_INFO(ME, "Unsupported operating class %d", operclass);
    return 0;
}

/* See 802.11-2020 / 802.11ax-2021 Table E-4 Global operating classes */
swl_bandwidth_e swl_chanspec_operClassToBandwidth(swl_operatingClass_t operclass) {
    swl_operatingClass_t sOperClassBandwidthMaps[SWL_BW_MAX][CHAN_MAX_VALUE] = {
        {0},                                                          /* BW AUTO */
        {81, 82, 115, 118, 121, 124, 125, 131, 0},                    /* BW 20MHz */
        {83, 84, 116, 117, 119, 120, 122, 123, 126, 127, 132, 0},     /* BW 40MHz */
        {128, 130, 133, 135, 0},                                      /* BW 80MHz */
        {129, 134, 0},                                                /* BW 160MHz */
        {137, 0},                                                     /* BW 320MHz */
        {0},                                                          /* BW 10MHz */
        {0},                                                          /* BW 5MHz */
        {0},                                                          /* BW 2MHz */
    };
    for(uint32_t i = 0; i < SWL_BW_MAX; i++) {
        for(uint8_t j = 0; (j < CHAN_MAX_VALUE) && (sOperClassBandwidthMaps[i][j] > 0); j++) {
            if(sOperClassBandwidthMaps[i][j] == operclass) {
                return i;
            }
        }
    }
    SAH_TRACEZ_INFO(ME, "No glob bw for operating class %d", operclass);
    return SWL_BW_AUTO;
}

swl_freqBandExt_e swl_chanspec_uniiToFreq(swl_uniiBand_m uniiBands) {
    if(uniiBands & M_SWL_BAND_UNII_1
       || uniiBands & M_SWL_BAND_UNII_2
       || uniiBands & M_SWL_BAND_UNII_3
       || uniiBands & M_SWL_BAND_UNII_4) {
        return SWL_FREQ_BAND_EXT_5GHZ;
    }
    if(uniiBands & M_SWL_BAND_UNII_5
       || uniiBands & M_SWL_BAND_UNII_6
       || uniiBands & M_SWL_BAND_UNII_7
       || uniiBands & M_SWL_BAND_UNII_8) {
        return SWL_FREQ_BAND_EXT_6GHZ;
    }
    return SWL_FREQ_BAND_EXT_NONE;
}

swl_uniiBand_m swl_chanspec_toUniiMask(const swl_chanspec_t* chanspec) {
    ASSERTS_NOT_NULL(chanspec, M_SWL_BAND_UNII_UNKNOWN, ME, "NULL");
    return (1 << swl_chanspec_toUnii(chanspec));
}

swl_uniiBand_m swl_chanspec_getUniiBandGroupMaskWithName(const char* groupName) {
    ASSERTS_NOT_NULL(groupName, M_SWL_BAND_UNII_UNKNOWN, ME, "NULL");
    uint32_t i;
    for(i = 0; i < SWL_ARRAY_SIZE(swl_chanspec_uniiBandGroups); i++) {
        if(!strcmp(groupName, swl_chanspec_uniiBandGroups[i].name)) {
            return swl_chanspec_uniiBandGroups[i].mask;
        }
    }
    return M_SWL_BAND_UNII_UNKNOWN;
}

const char* swl_chanspec_getUniiBandGroupNameWithMask(swl_uniiBand_m groupMask) {
    uint32_t i;
    for(i = 0; i < SWL_ARRAY_SIZE(swl_chanspec_uniiBandGroups); i++) {
        if(swl_chanspec_uniiBandGroups[i].mask == groupMask) {
            return swl_chanspec_uniiBandGroups[i].name;
        }
    }
    return NULL;
}

/**
 * Function checks whether freqBand has a valid frequency band value (2.4GHz, 5GHz, 6GHz)
 * @param freqBand: frequency Band
 * @return true if frequency Band is in [SWL_FREQ_BAND_2_4GHZ, SWL_FREQ_BAND_5GHZ, SWL_FREQ_BAND_6GHZ] else false
 */
bool swl_chanspec_isValidBand(swl_freqBand_e freqBand) {
    if((freqBand != SWL_FREQ_BAND_2_4GHZ) && (freqBand != SWL_FREQ_BAND_5GHZ)
       && (freqBand != SWL_FREQ_BAND_6GHZ)) {
        return false;
    }
    return true;
}

/**
 * Function checks whether freqBandExt has a valid frequency band value (2.4GHz, 5GHz, 6GHz)
 * @param freqBand: frequency Band
 * @return true if frequency Band is in [SWL_FREQ_BAND_EXT_2_4GHZ, SWL_FREQ_BAND_EXT_5GHZ, SWL_FREQ_BAND_EXT_6GHZ] else false
 */
bool swl_chanspec_isValidBandExt(swl_freqBandExt_e freqBandExt) {
    return swl_chanspec_isValidBand(swl_chanspec_freqBandExtToFreqBand(freqBandExt, SWL_FREQ_BAND_MAX, NULL));
}

/**
 * Returns a valid swl_freqBand_e extracted from the swl_chanspec_t parameter.
 * swl_chanspec_t->band's type is swl_freqBandExt_e, which is a longer enum than swl_freqBand_e
 *
 * @param chanSpec: channel specification
 * @param defaultFreqBand: the default swl_freqBand_e to return when chanSpec->band does not fit in swl_freqBand_e
 * @param hasMatch: optional, considered only if not NULL; set to true or false,depending on successful fit
 * @return: the matching swl_freqBand_e, else the default parameter value
 */
swl_freqBand_e swl_chanspec_getFreq(swl_chanspec_t* chanSpec, swl_freqBand_e defaultFreqBand, bool* hasMatch) {
    if(hasMatch != NULL) {     // when chanspec is NULL
        *hasMatch = false;
    }
    ASSERTS_NOT_NULL(chanSpec, defaultFreqBand, ME, "null chanSpec, default returned");
    return swl_chanspec_freqBandExtToFreqBand(chanSpec->band, defaultFreqBand, hasMatch);
}

/**
 * Deduces a valid frequency band when the base channel argument matches only one frequency band channel list.
 *
 * @param baseChannel: base (primary) channel
 * @return: the matching swl_freqBandExt_e,
 *          SWL_FREQ_BAND_EXT_AUTO value when channel has multiple matches,
 *          or SWL_FREQ_BAND_EXT_NONE when channel has no matches
 */
swl_freqBandExt_e swl_chanspec_freqBandExtFromBaseChannel(swl_channel_t baseChannel) {
    if(swl_channel_is6g(baseChannel)) {
        //Freq Band 6GHz has few shared channels index, with freq band 2.4GHz and 5GHz
        if(swl_channel_is2g(baseChannel) || swl_channel_is5g(baseChannel)) {
            return SWL_FREQ_BAND_EXT_AUTO;
        }
        return SWL_FREQ_BAND_EXT_6GHZ;
    }
    if(swl_channel_is5g(baseChannel)) {
        return SWL_FREQ_BAND_EXT_5GHZ;
    }
    if(swl_channel_is2g(baseChannel)) {
        return SWL_FREQ_BAND_EXT_2_4GHZ;
    }
    return SWL_FREQ_BAND_EXT_NONE;
}

/**
 * Converts a valid swl_freqBandExt_e into an swl_freqBand_e value.
 * swl_freqBandExt_e is a longer enum like swl_freqBand_e, only values fitting in swl_freqBand_e are considered.
 *
 * @param chanSpec: freqBandExt the extended freqBand enum
 * @param defaultFreqBand: returned when conversion is impossible
 * @param hasMatch: optional, considered only if not NULL; set to true or false, depending on conversion success
 * @return: the converted swl_freqBand_e, if any, else the default parameter value
 */
swl_freqBand_e swl_chanspec_freqBandExtToFreqBand(swl_freqBandExt_e freqBandExt, swl_freqBand_e defaultFreqBand, bool* hasMatch) {
    bool match = false;
    swl_freqBand_e matchedBand = defaultFreqBand;

    if(freqBandExt < (swl_freqBandExt_e) SWL_FREQ_BAND_MAX) {
        match = true;
        matchedBand = (swl_freqBand_e) freqBandExt;
    }

    if(hasMatch != NULL) {
        *hasMatch = match;
    }

    return matchedBand;
}

/**
 * Get the operating classe that encompass a given primary channel and that identify its location.
 *
 * @param channel: the channel number (primary or center)
 * @param band: the frequecy band where to look for the channel
 * @param bandwidth: the bandwidth of operating class
 * @return swl_operatingClass_t value, 0 if not found
 */
swl_operatingClass_t swl_chanspec_getOperClassDirect(swl_channel_t channel, swl_freqBandExt_e band, swl_bandwidth_e bandwidth) {
    const swl_operatingClassInfo_t* pOpClassInfo = s_getGlobalOpClassInfo_byChSpec(channel, band, bandwidth);
    ASSERTS_NOT_NULL(pOpClassInfo, 0, ME, "no match");

    return pOpClassInfo->operClass;
}

/**
 * Get the operating classe that encompass a given primary channel and that identify its location.
 *
 * @param chanSpec
 * @return the global operating class
 */
swl_operatingClass_t swl_chanspec_getOperClass(swl_chanspec_t* chanSpec) {
    return swl_chanspec_getOperClassDirect(chanSpec->channel, chanSpec->band, chanSpec->bandwidth);
}
/**
 * Get the operating classe that encompass a given primary channel and that identify its location.
 *
 * @param chanSpec
 * @param countryIndex: the country index,USA=0,EU=1,JP=2,CN=3
 * @return the local operating class for the given country index value, the global operting class if not found
 */
swl_operatingClass_t swl_chanspec_getLocalOperClass(swl_chanspec_t* chanSpec, swl_opClassCountry_e countryIndex) {
    swl_operatingClass_t globalOperClass = swl_chanspec_getOperClassDirect(chanSpec->channel, chanSpec->band, chanSpec->bandwidth);
    if((globalOperClass > 0) && (countryIndex < SWL_OP_CLASS_COUNTRY_MAX)) {
        for(uint32_t i = 0; i < SWL_ARRAY_SIZE(s_operClassTranslationTable); i++) {
            if(s_operClassTranslationTable[i].global == globalOperClass) {
                return s_operClassTranslationTable[i].local_set[countryIndex];
            }
        }
    }
    SAH_TRACEZ_ERROR(ME, "no local operatingclass found for global value (%d)", globalOperClass);
    return 0;
}

SWL_TABLE(swl_bwToNbOfChans,
          ARR(swl_bandwidth_e bandwidth; uint8_t nbChans; ),
          ARR(&gtSwl_type_bandwidth.enumType, swl_type_uint8),
          ARR({SWL_BW_AUTO, 1},
              {SWL_BW_20MHZ, 1},
              {SWL_BW_40MHZ, 2},
              {SWL_BW_80MHZ, 4},
              {SWL_BW_160MHZ, 8},
              {SWL_BW_320MHZ, 16})
          );

/**
 * Return the number of valid channels for a given bandwidth and given frequency band.
 * This is required, because 40MHz on 2.4GHz covers 5 channels, while
 * 40MHz on 5 and 6GHz covers only 2 channels.
 *
 */
uint8_t swl_chanspec_getNrChannelsPerFreqAndBw(swl_freqBandExt_e band, swl_bandwidth_e bandwidth) {
    if(band != SWL_FREQ_BAND_EXT_2_4GHZ) {
        uint8_t* val = (uint8_t*) swl_table_getMatchingValue(&swl_bwToNbOfChans, 1, 0, &bandwidth);
        return val != NULL ? *val : 0;
    } else {
        if((bandwidth == SWL_BW_20MHZ) || (bandwidth == SWL_BW_AUTO)) {
            return 1;
        } else if(bandwidth == SWL_BW_40MHZ) {
            return 5;
        }
    }
    SAH_TRACEZ_ERROR(ME, "Unknown frequency band (%d/%d)", band, bandwidth);
    return 0;
}

/**
 * Return the amount of 20MHz channels in a given band.
 */
uint8_t swl_chanspec_getNrChannelsInBand(const swl_chanspec_t* chanspec) {
    return swl_chanspec_getNrChannelsPerFreqAndBw(chanspec->band, chanspec->bandwidth);
}

/**
 * @brief return the "base channel" of a given chanspec
 * If unable to convert the chanspec, SWL_CHANNEL_INVALID is returned.
 */
swl_channel_t swl_chanspec_getBaseChannel(const swl_chanspec_t* chanspec) {
    swl_channel_t channel = chanspec->channel;
    if(chanspec->band == SWL_FREQ_BAND_EXT_6GHZ) {
        uint32_t nrChans = swl_chanspec_getNrChannelsInBand(chanspec);
        if(nrChans == 0) {
            SAH_TRACEZ_ERROR(ME, "can't get base %u %u", channel, chanspec->bandwidth);
            return SWL_CHANNEL_INVALID;
        }

        if(nrChans == 1) {
            return channel;
        } else if((chanspec->bandwidth == SWL_BW_320MHZ) && (chanspec->extensionHigh == SWL_CHANSPEC_EXT_HIGH)) {
            return channel - ((channel - 33) % (nrChans * SWL_CHANNEL_INCREMENT));
        } else {
            return channel - ((channel - 1) % (nrChans * SWL_CHANNEL_INCREMENT));
        }

    } else if(chanspec->band == SWL_FREQ_BAND_EXT_5GHZ) {
        uint32_t nrChans = swl_chanspec_getNrChannelsInBand(chanspec);
        if(nrChans == 0) {
            SAH_TRACEZ_ERROR(ME, "can't get base %u %u", chanspec->channel, chanspec->bandwidth);
            return channel;
        }
        if(channel >= 149) {
            return channel - ((channel - 149) % (nrChans * SWL_CHANNEL_INCREMENT));
        } else {
            return channel - ((channel - 36) % (nrChans * SWL_CHANNEL_INCREMENT));
        }
    } else {
        //2.4GHz
        if((chanspec->bandwidth == SWL_BW_40MHZ)) {
            if((chanspec->extensionHigh == SWL_CHANSPEC_EXT_LOW) || (( chanspec->extensionHigh == SWL_CHANSPEC_EXT_AUTO) && (chanspec->channel > 6))) {
                return channel - 4;
            } else {
                return channel;
            }
        } else {
            return channel;
        }
    }
}

/**
 * Find the correct bandwidth from the control channel and center channel and the frequency band.
 *
 * @param freqBand frequency band.
 * @param ctrlChan a control channel.
 * @param centerChan a channel center.
 *
 * @return the bandwidth number for the given control channel and center channel.
 */

swl_bandwidth_e swl_chanspec_getBwFromFreqCtrlCentre(swl_freqBandExt_e freqBand, swl_channel_t ctrlChan, swl_channel_t centerChan) {
    swl_chanspec_t tmpSpec = SWL_CHANSPEC_EMPTY;
    swl_rc_ne ret = swl_chanspec_fromFreqCtrlCentre(&tmpSpec, freqBand, ctrlChan, centerChan);
    if(ret != SWL_RC_OK) {
        return SWL_BW_AUTO;
    } else {
        return tmpSpec.bandwidth;
    }
}

swl_channel_t swl_chanspec_getCentreChannel(const swl_chanspec_t* chanspec) {
    swl_channel_t centerChannel;
    swl_channel_t baseChannel = swl_chanspec_getBaseChannel(chanspec);
    uint32_t nrChannelsInBand = swl_chanspec_getNrChannelsInBand(chanspec);
    if(chanspec->band != SWL_FREQ_BAND_EXT_2_4GHZ) {
        if(nrChannelsInBand > 1) {
            centerChannel = baseChannel + (nrChannelsInBand / 2 * 4) - 2;
        } else {
            centerChannel = baseChannel;
        }
    } else {
        if(chanspec->bandwidth == SWL_BW_40MHZ) {
            centerChannel = baseChannel + 2;
        } else {
            centerChannel = baseChannel;
        }
    }
    return centerChannel;
}

/*
 * @brief fill a list with channels which are present in a chanspec
 *
 * @param chanspec: chanspec to retrieve channels
 * @param channelsList: channels in chanspec is added in the list
 * @param listSize : size of channelsList
 *
 * @return the number of channel added in channelsList
 */
uint8_t swl_chanspec_getChannelsInChanspec(const swl_chanspec_t* chanspec, swl_channel_t* channelsList, uint8_t listSize) {
    uint32_t nbChans = swl_chanspec_getNrChannelsInBand(chanspec);
    ASSERT_TRUE(nbChans <= listSize, 0, ME, "List size too small %u < %u", listSize, nbChans);
    if(chanspec->band != SWL_FREQ_BAND_EXT_2_4GHZ) {
        swl_channel_t baseChannel = swl_chanspec_getBaseChannel(chanspec);
        for(uint32_t i = 0; i < nbChans; i++) {
            channelsList[i] = baseChannel + i * 4;
        }
    } else {
        swl_channel_t channel = chanspec->channel;
        if((chanspec->bandwidth == SWL_BW_40MHZ) && ((chanspec->extensionHigh == SWL_CHANSPEC_EXT_LOW) ||
                                                     ((chanspec->extensionHigh == SWL_CHANSPEC_EXT_AUTO) && (channel > 6)))) {
            channel -= 4;
        }
        for(uint32_t i = 0; i < nbChans; i++) {
            channelsList[i] = channel + i;
        }
    }
    return nbChans;
}

/**
 * Returns true if the testChannel is in the band determing by the input channel and bandwidth.
 * Returns false otherwise.
 */
bool swl_channel_isInChanspec(const swl_chanspec_t* chanspec, const swl_channel_t testChannel) {
    swl_channel_t chanList[SWL_BW_CHANNELS_MAX];
    uint8_t nrChannels = swl_chanspec_getChannelsInChanspec(chanspec, chanList, SWL_ARRAY_SIZE(chanList));
    for(uint8_t i = 0; i < nrChannels; i++) {
        if(testChannel == chanList[i]) {
            return true;
        }
    }
    return false;
}

/*
 * @brief return the first channel of the complementary sub-band of a band
 * For 40MHz, return the first channel of the secondary 20MHz band (eg. 44/40, ret 48, or 1/40 ret 5 (2.4G))
 * For 80MHz, return the first channel of the secondary 40MHz band (eg. 48/80, ret 36 or 36/80 ret 44)
 * For 160MHz, return the first channel of the secondary 80MHz band (eg. 100/160, ret 116)
 * For 320MHz, return the first channel of the secondary 160MHz band (eg. 1/320-1, ret 33)
 *
 * @param chanspec to get the complementary base channel
 *
 * @return the complementary base channel or -1 if does not exist
 */
swl_channel_t swl_channel_getComplementaryBaseChannel(const swl_chanspec_t* chanspec) {
    if((chanspec->band == SWL_FREQ_BAND_EXT_2_4GHZ) || (chanspec->bandwidth <= SWL_BW_20MHZ)) {
        return -1;
    }
    swl_chanspec_t baseChanspec = SWL_CHANSPEC_NEW(swl_chanspec_getBaseChannel(chanspec), chanspec->bandwidth - 1, chanspec->band);
    if(swl_channel_isInChanspec(&baseChanspec, chanspec->channel)) {
        uint32_t nrChannels = swl_chanspec_getNrChannelsInBand(&baseChanspec);
        return baseChanspec.channel + nrChannels * SWL_CHANNEL_INCREMENT;
    } else {
        SAH_TRACEZ_ERROR(ME, "Test %u %u %s %s", chanspec->channel, swl_chanspec_getBaseChannel(chanspec), swl_typeChanspecExt_toBuf32(baseChanspec).buf, swl_typeChanspecExt_toBuf32(*chanspec).buf);
        return baseChanspec.channel;
    }
}

/*
 * @brief Get the channel spec (band, channel number) from central frequency (in Mega Hertz).
 * Note that this will always return a chanspec with "auto" bandwidth, and just provides
 * The centre channel value. Starting at 5955MHz we start at 6GHz, while technically it could also be a 5GHz channel
 *
 * @param freq central frequency (in Mega Hertz)
 * @param pChanSpec pointer to chanspec struct to be filled.
 *
 * @return SWL_RC_OK in case success
 *         <= SWL_RC_ERROR otherwise
 */
swl_rc_ne swl_chanspec_channelFromMHz(swl_chanspec_t* pChanSpec, uint32_t freqMHz) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_chanspec_t chanspec = {
        .channel = 0,
        .band = SWL_FREQ_BAND_EXT_AUTO,
        .bandwidth = SWL_BW_AUTO,
    };
    uint32_t rem = 0;
    /* see 802.11 17.3.8.3.2 and Annex J */
    if(freqMHz == SWL_CHANNEL_2G_FREQ_END) {
        chanspec.band = SWL_FREQ_BAND_EXT_2_4GHZ;
        chanspec.channel = SWL_CHANNEL_2G_END;
    } else if((freqMHz >= SWL_CHANNEL_2G_FREQ_START) && (freqMHz < SWL_CHANNEL_2G_FREQ_END)) {
        chanspec.band = SWL_FREQ_BAND_EXT_2_4GHZ;
        chanspec.channel = (freqMHz - SWL_CHANNEL_2G_FREQ_START) / SWL_CHANNEL_INTER_FREQ_5MHZ;
        rem = (freqMHz - SWL_CHANNEL_2G_FREQ_START) % SWL_CHANNEL_INTER_FREQ_5MHZ;
    } else if(freqMHz == SWL_CHANNEL_6G_FREQ_CHAN2) {
        chanspec.band = SWL_FREQ_BAND_EXT_6GHZ;
        chanspec.channel = 2;
        rem = 0;
    } else if((freqMHz > SWL_CHANNEL_5G_FREQ_START) && (freqMHz < SWL_CHANNEL_6G_FREQ_START)) {
        chanspec.band = SWL_FREQ_BAND_EXT_5GHZ;
        chanspec.channel = (freqMHz - SWL_CHANNEL_5G_FREQ_START) / SWL_CHANNEL_INTER_FREQ_5MHZ;
        rem = (freqMHz - SWL_CHANNEL_5G_FREQ_START) % SWL_CHANNEL_INTER_FREQ_5MHZ;
    } else if((freqMHz >= (SWL_CHANNEL_6G_FREQ_START + SWL_CHANNEL_INTER_FREQ_5MHZ)) && (freqMHz <= SWL_CHANNEL_6G_FREQ_END)) {
        /* see 802.11ax D6.1 27.3.22.2 */
        chanspec.band = SWL_FREQ_BAND_EXT_6GHZ;
        chanspec.channel = (freqMHz - SWL_CHANNEL_6G_FREQ_START) / SWL_CHANNEL_INTER_FREQ_5MHZ;
        rem = (freqMHz - SWL_CHANNEL_6G_FREQ_START) % SWL_CHANNEL_INTER_FREQ_5MHZ;
    }
    ASSERTS_EQUALS(rem, 0, SWL_RC_ERROR, ME, "not matching any channel");
    ASSERTS_NOT_EQUALS(chanspec.channel, 0, SWL_RC_ERROR, ME, "not matching any channel");
    *pChanSpec = chanspec;
    return SWL_RC_OK;
}

/**
 * @brief convert the channel / band to freqHz. Note that the bandwidth is
 * NOT taken into account.
 *
 * Legal values taken from IEEE standard. Note that for 5GHz channels 191 and above, this
 * function will provide a frequency as stated in standard, while reverse function will return a 6GHz channel.
 *
 * @param pChanSpec: the chanspec to convert
 * @param freqHz: the output parameter to which the result will be written.
 *
 */
swl_rc_ne swl_chanspec_channelToMHz(const swl_chanspec_t* pChanSpec, uint32_t* freqMHz) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(freqMHz, SWL_RC_INVALID_PARAM, ME, "NULL");

    if(pChanSpec->band == SWL_FREQ_BAND_EXT_2_4GHZ) {
        ASSERT_TRUE(pChanSpec->channel > SWL_CHANNEL_INVALID && pChanSpec->channel <= SWL_CHANNEL_2G_END, SWL_RC_INVALID_PARAM, ME, "Invalid channel %u in band %u", pChanSpec->channel, pChanSpec->band);
        if(pChanSpec->channel == SWL_CHANNEL_2G_END) {
            *freqMHz = SWL_CHANNEL_2G_FREQ_END;
            return SWL_RC_OK;
        } else {
            *freqMHz = SWL_CHANNEL_2G_FREQ_START + (SWL_CHANNEL_INTER_FREQ_5MHZ * pChanSpec->channel);
            return SWL_RC_OK;
        }
    } else if(pChanSpec->band == SWL_FREQ_BAND_EXT_5GHZ) {
        ASSERT_TRUE(pChanSpec->channel > SWL_CHANNEL_INVALID && pChanSpec->channel <= 200, SWL_RC_INVALID_PARAM, ME, "Invalid channel %u in band %u", pChanSpec->channel, pChanSpec->band);
        *freqMHz = SWL_CHANNEL_5G_FREQ_START + (SWL_CHANNEL_INTER_FREQ_5MHZ * pChanSpec->channel);
        return SWL_RC_OK;
    } else if(pChanSpec->band == SWL_FREQ_BAND_EXT_6GHZ) {
        ASSERT_TRUE(pChanSpec->channel > SWL_CHANNEL_INVALID && pChanSpec->channel <= SWL_CHANNEL_6G_END, SWL_RC_INVALID_PARAM, ME, "Invalid channel %u in band %u", pChanSpec->channel, pChanSpec->band);
        if(pChanSpec->channel == 2) {
            *freqMHz = SWL_CHANNEL_6G_FREQ_CHAN2; // see 802.11ax D6.1 27.3.22.2
            return SWL_RC_OK;
        }
        *freqMHz = SWL_CHANNEL_6G_FREQ_START + (pChanSpec->channel * SWL_CHANNEL_INTER_FREQ_5MHZ);
        return SWL_RC_OK;
    }

    SAH_TRACEZ_ERROR(ME, "Invalid band %u", pChanSpec->band);
    return SWL_RC_INVALID_PARAM;
}

/**
 * @brief provide the center frequency of the provided channel / band. In this function the bandwidth is
 * NOT taken into account. If conversion fails, then the default frequency is returned.
 *
 * Legal values taken from IEEE standard. Note that for 5GHz channels 191 and above, this
 * function will provide a frequency as stated in standard, while reverse function will return a 6GHz channel.
 *
 * @param pChanSpec: the chanspec to convert
 * @param defFreqMHz: if conversion of chanspec fails, this value is returned.
 *
 */
uint32_t swl_chanspec_channelToMHzDef(const swl_chanspec_t* pChanSpec, uint32_t defFreqMHz) {
    uint32_t freq = 0;
    swl_rc_ne retVal = swl_chanspec_channelToMHz(pChanSpec, &freq);
    if(retVal < 0) {
        return defFreqMHz;
    } else {
        return freq;
    }
}

/**
 * @brief provide the center frequency of the provided chanspec, including bandwidth.
 *
 * Legal values taken from IEEE standard. Note that for 5GHz channels 191 and above, this
 * function will provide a frequency as stated in standard, while reverse function will return a 6GHz channel.
 *
 * @param pChanSpec: the chanspec to convert
 * @param freqHz: the output parameter to which the result will be written.
 *
 */
swl_rc_ne swl_chanspec_toMHz(const swl_chanspec_t* pChanSpec, uint32_t* freqMHz) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(freqMHz, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_chanspec_t tmpChanspec = *pChanSpec;
    tmpChanspec.channel = swl_chanspec_getCentreChannel(pChanSpec);
    return swl_chanspec_channelToMHz(&tmpChanspec, freqMHz);
}

/**
 * @brief provide the center frequency of the provided chanspec, including bandwidth.
 *
 * Legal values taken from IEEE standard. Note that for 5GHz channels 191 and above, this
 * function will provide a frequency as stated in standard, while reverse function will return a 6GHz channel.
 *
 * @param pChanSpec: the chanspec to convert
 * @param defFreqMHz: if conversion of chanspec fails, this value is returned.
 *
 */
uint32_t swl_chanspec_toMHzDef(const swl_chanspec_t* pChanSpec, uint32_t defFreqMHz) {
    ASSERT_NOT_NULL(pChanSpec, defFreqMHz, ME, "NULL");

    uint32_t freq = 0;
    swl_rc_ne retVal = swl_chanspec_toMHz(pChanSpec, &freq);
    if(retVal < 0) {
        return defFreqMHz;
    } else {
        return freq;
    }
}

/**
 * Return the radio bandwidth of the given chanspec.
 */
swl_radBw_e swl_chanspec_toRadBw(const swl_chanspec_t* pCs) {
    if(pCs->bandwidth == SWL_BW_320MHZ) {
        if(pCs->extensionHigh == SWL_CHANSPEC_EXT_HIGH) {
            return SWL_RAD_BW_320MHZ2;
        } else {
            return SWL_RAD_BW_320MHZ1;
        }
    }
    if((pCs->bandwidth == SWL_BW_80MHZ) && (pCs->secondChannel != 0)) {
        return SWL_RAD_BW_8080MHZ;
    }
    if(pCs->bandwidth <= SWL_BW_160MHZ) {
        return (swl_radBw_e) pCs->bandwidth;
    } else {
        return SWL_RAD_BW_AUTO;
    }
}

typedef struct {
    uint32_t offset;
    uint32_t modulo;
    swl_bandwidth_e bw;
} swl_chanspec_offsetBw;

const swl_chanspec_offsetBw bwTable[] = {
    {0, 2, SWL_BW_20MHZ},
    {1, 4, SWL_BW_40MHZ},
    {3, 8, SWL_BW_80MHZ},
    {7, 16, SWL_BW_160MHZ},
    {15, 32, SWL_BW_320MHZ},
};
//table should be max - 1 size, as AUTO is not included
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(bwTable) == SWL_BW_RAD_MAX - 1, "bwTable not correctly defined");


/**
 * returns the bandwidth estimate based on the offset of the center channel from the base channel
 */
swl_bandwidth_e s_getBwFromOffset(uint32_t offset) {
    offset = offset / 2;

    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(bwTable); i++) {
        if((offset % bwTable[i].modulo) == bwTable[i].offset) {
            return bwTable[i].bw;
        }
    }
    return SWL_BW_AUTO;
}

/**
 * Returns the channel offset between the center channel and base channel, based on bandwidth.
 */
uint32_t s_getOffsetFromBw(swl_bandwidth_e bw) {
    for(uint32_t i = 0; i < SWL_ARRAY_SIZE(bwTable); i++) {
        if(bw == bwTable[i].bw) {
            return bwTable[i].offset * 2;
        }
    }
    return 0;
}

/**
 * Attempts to convert a frequency of centre channel to a chanspec, including bandwidth.
 * As channel it will set the "first" primary channel.
 *
 * For 2.4GHz, this will assume 20MHz, as all channels can be 20MHz,
 * and all channels between 3 and 11 can be 40
 *
 * For 5 and 6GHz, this function will work by assuming that "middle channel frequency" actually
 * means the base channel + operatingChannelBandwidth. Note that this will then NOT provide a
 * primary channel.
 *
 * For 5GHz, this function will work for the "generally established" frequencies between:
 * * 36 and 64
 * * 100 and 144
 * * 149 and 177
 *
 */
swl_rc_ne swl_chanspec_fromMHz(swl_chanspec_t* pChanSpec, uint32_t freqMHz) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_chanspec_t tmpChanspec;
    swl_rc_ne retVal = swl_chanspec_channelFromMHz(&tmpChanspec, freqMHz);
    ASSERTS_TRUE(retVal >= 0, retVal, ME, "ERROR");
    return swl_chanspec_fromFreqAndCentreChannel(pChanSpec, tmpChanspec.band, tmpChanspec.channel);
}


/**
 * Attempts to convert a frequency to a chanspec, including bandwidth.
 * As channel it will set the "first" or base primary channel.
 *
 * For 2.4GHz, this will assume 20MHz, as all channels can be 20MHz,
 * and all channels between 3 and 11 can be 40
 *
 * For 5 and 6GHz, this function will work by assuming that "middle channel frequency" actually
 * means the base channel + operatingChannelBandwidth. Note that this will then NOT provide a
 * correct primary channel, but the base channel, which should be primary in most cases.
 *
 * For 5GHz, this function will work for the "generally established" frequencies between:
 * * 36 and 64
 * * 100 and 144
 * * 149 and 177
 *
 */
swl_rc_ne swl_chanspec_fromFreqAndCentreChannel(swl_chanspec_t* pChanSpec, swl_freqBandExt_e freq, swl_channel_t centreChannel) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_chanspec_t tmpChanspec = SWL_CHANSPEC_NEW(centreChannel, SWL_BW_AUTO, freq);

    if(tmpChanspec.band == SWL_FREQ_BAND_EXT_2_4GHZ) {
        tmpChanspec.bandwidth = SWL_BW_20MHZ;


    } else if(tmpChanspec.band == SWL_FREQ_BAND_EXT_5GHZ) {
        if((tmpChanspec.channel >= 36) && (tmpChanspec.channel <= 64)) {
            uint32_t diff = tmpChanspec.channel - 36;
            tmpChanspec.bandwidth = s_getBwFromOffset(diff);
        } else if((tmpChanspec.channel >= 100) && (tmpChanspec.channel <= 144)) {
            uint32_t diff = tmpChanspec.channel - 100;
            tmpChanspec.bandwidth = s_getBwFromOffset(diff);
        } else if((tmpChanspec.channel >= 149) && (tmpChanspec.channel <= 177)) {
            uint32_t diff = tmpChanspec.channel - 149;
            tmpChanspec.bandwidth = s_getBwFromOffset(diff);
        } else {
            return SWL_RC_INVALID_PARAM;
        }
    } else if(tmpChanspec.band == SWL_FREQ_BAND_EXT_6GHZ) {
        tmpChanspec.bandwidth = s_getBwFromOffset(tmpChanspec.channel - 1);
        if(tmpChanspec.bandwidth == SWL_BW_AUTO) {
            if(((tmpChanspec.channel - 63) % 64) == 0) {
                tmpChanspec.bandwidth = SWL_BW_320MHZ;
                tmpChanspec.extensionHigh = SWL_CHANSPEC_EXT_HIGH;
            }
        } else if(tmpChanspec.bandwidth == SWL_BW_320MHZ) {
            tmpChanspec.extensionHigh = SWL_CHANSPEC_EXT_LOW;
        }
    }
    ASSERTS_TRUE(tmpChanspec.bandwidth != SWL_BW_AUTO, SWL_RC_INVALID_PARAM, ME, "ERROR");
    tmpChanspec.channel = tmpChanspec.channel - (s_getOffsetFromBw(tmpChanspec.bandwidth));
    *pChanSpec = tmpChanspec;

    return SWL_RC_OK;
}


/**
 * Create chanspec from frequency, control channel and centre channel.
 *
 * This shall work for all cases, including 2.4GHz 40MHz.
 * The chanspec shall be properly set to the control channel
 */
swl_rc_ne swl_chanspec_fromFreqCtrlCentre(swl_chanspec_t* pChanSpec, swl_freqBandExt_e freq, swl_channel_t ctrlChannel, swl_channel_t centreChannel) {
    ASSERTS_NOT_NULL(pChanSpec, SWL_RC_INVALID_PARAM, ME, "NULL");

    if(freq != SWL_FREQ_BAND_EXT_2_4GHZ) {
        swl_rc_ne ret = swl_chanspec_fromFreqAndCentreChannel(pChanSpec, freq, centreChannel);
        if(ret != SWL_RC_OK) {
            return ret;
        }
        pChanSpec->channel = ctrlChannel;
        return SWL_RC_OK;
    }

    pChanSpec->channel = ctrlChannel;
    pChanSpec->band = freq;
    if(centreChannel != ctrlChannel) {
        pChanSpec->bandwidth = SWL_BW_40MHZ;
        if(centreChannel < ctrlChannel) {
            pChanSpec->extensionHigh = SWL_CHANSPEC_EXT_LOW;
        } else {
            pChanSpec->extensionHigh = SWL_CHANSPEC_EXT_HIGH;
        }
    } else {
        pChanSpec->bandwidth = SWL_BW_20MHZ;
    }
    return SWL_RC_OK;
}


bool swl_channel_is2g(const swl_channel_t channel) {
    return ((channel >= SWL_CHANNEL_2G_START) && (channel <= SWL_CHANNEL_2G_END));
}

bool swl_channel_is5g(const swl_channel_t channel) {
    return (((((channel >= SWL_CHANNEL_5G_UNII_1_START) && (channel <= SWL_CHANNEL_5G_UNII_2A_END)) ||
              ((channel >= SWL_CHANNEL_5G_UNII_2C_START) && (channel <= SWL_CHANNEL_5G_UNII_2C_END))) && ((channel % 4) == 0))) ||
           (((channel >= SWL_CHANNEL_5G_UNII_3_START) && (channel <= SWL_CHANNEL_5G_UNII_4_END)) && ((channel % 4) == 1));
}

bool swl_channel_is6g(const swl_channel_t channel) {
    return (((channel >= SWL_CHANNEL_6G_START) && (channel <= SWL_CHANNEL_6G_END)) &&
            (channel % 4) == 1);
}

/*
 * @brief check whether channel is 6ghz Preferred Scanning Channels
 * PSC = (5 + (16 * n))
 */
bool swl_channel_is6gPsc(const swl_channel_t channel) {
    return ((swl_channel_is6g(channel)) &&
            ((channel % SWL_CHANNEL_6G_PSC_INTER) == SWL_CHANNEL_6G_PSC_FIRST));
}

bool swl_channel_isDfs(const swl_channel_t channel) {
    ASSERTS_TRUE(swl_channel_is5g(channel), false, ME, "Not 5GHz channel");
    return (channel >= SWL_CHANNEL_5G_DFS_START) && (channel <= SWL_CHANNEL_5G_DFS_END);
}

bool swl_chanspec_isDfs(const swl_chanspec_t chanspec) {
    ASSERTS_TRUE(chanspec.band == SWL_FREQ_BAND_EXT_5GHZ, false, ME, "Not 5GHz chanspec");
    if((chanspec.bandwidth == SWL_BW_160MHZ) && (chanspec.channel <= SWL_CHANNEL_5G_DFS_END)) {
        return true;
    }
    return swl_channel_isDfs(chanspec.channel);
}

bool swl_channel_isWeather(const swl_channel_t channel) {
    ASSERTS_TRUE(swl_channel_is5g(channel), false, ME, "Not 5GHz channel");
    return (channel >= SWL_CHANNEL_5G_WEATHER_START) && (channel <= SWL_CHANNEL_5G_WEATHER_END);
}

bool swl_chanspec_isWeather(const swl_chanspec_t chanspec) {
    ASSERTS_TRUE(chanspec.band == SWL_FREQ_BAND_EXT_5GHZ, false, ME, "Not 5GHz chanspec");
    swl_channel_t channelsList[SWL_BW_CHANNELS_MAX];
    uint8_t nrChannels = swl_chanspec_getChannelsInChanspec(&chanspec, channelsList, SWL_BW_CHANNELS_MAX);
    for(uint8_t i = 0; i < nrChannels; i++) {
        if(swl_channel_isWeather(channelsList[i])) {
            return true;
        }
    }
    return false;
}

bool swl_channel_isHighPower(const swl_channel_t channel) {
    ASSERTS_TRUE(swl_channel_is5g(channel), false, ME, "Not 5GHz channel");
    return (channel >= SWL_CHANNEL_5G_UNII_2C_START) && (channel <= SWL_CHANNEL_5G_UNII_4_END);
}

bool swl_chanspec_isHighPower(const swl_chanspec_t chanspec) {
    ASSERTS_TRUE(chanspec.band == SWL_FREQ_BAND_EXT_5GHZ, false, ME, "Not 5GHz chanspec");
    return swl_channel_isHighPower(chanspec.channel);
}

/**
 * Convert a chanspec from the data model entries to swl_chanspec_t form
 */
swl_rc_ne swl_chanspec_fromDmExt(swl_chanspec_t* targetChanspec, swl_channel_t channel, swl_radBw_e bw, swl_freqBandExt_e freq,
                                 swl_channel_t extChannel, swl_chanspec_ext_e extentionHigh) {
    ASSERT_NOT_NULL(targetChanspec, SWL_RC_INVALID_PARAM, ME, "NULL");

    targetChanspec->channel = channel;
    targetChanspec->band = freq;
    targetChanspec->extensionHigh = SWL_CHANSPEC_EXT_AUTO;
    if(bw <= SWL_RAD_BW_160MHZ) {
        targetChanspec->bandwidth = (swl_bandwidth_e) bw;
    } else {
        if(bw == SWL_RAD_BW_8080MHZ) {
            ASSERT_TRUE(extChannel != 0, SWL_RC_INVALID_PARAM, ME, "INVALID");
            targetChanspec->bandwidth = SWL_BW_80MHZ;
            targetChanspec->secondChannel = extChannel;

        } else if(bw == SWL_RAD_BW_320MHZ1) {
            targetChanspec->bandwidth = SWL_BW_320MHZ;
            targetChanspec->extensionHigh = SWL_CHANSPEC_EXT_LOW;
        } else if(bw == SWL_RAD_BW_320MHZ2) {
            targetChanspec->bandwidth = SWL_BW_320MHZ;
            targetChanspec->extensionHigh = SWL_CHANSPEC_EXT_HIGH;
        } else {
            ASSERT_TRUE(false, SWL_RC_INVALID_PARAM, ME, "INVALID");
        }
    }

    if((bw == SWL_RAD_BW_40MHZ) && (freq == SWL_FREQ_BAND_EXT_2_4GHZ)) {
        //Auto is allowed, in which case default rules are applied, i.e. channel > 6 is extentionLow,  <= 6 is extentionHigh
        targetChanspec->extensionHigh = extentionHigh;
    }

    return SWL_RC_OK;
}


swl_chanspec_t swl_chanspec_fromDm(swl_channel_t channel, swl_radBw_e bw, swl_freqBandExt_e freq) {
    swl_chanspec_t tmpSpec = SWL_CHANSPEC_EMPTY;
    swl_chanspec_fromDmExt(&tmpSpec, channel, bw, freq, 0, SWL_CHANSPEC_EXT_AUTO);
    return tmpSpec;
}

/**
 * Return if the provided channel is a high power channel in the specified country.
 * @param channel: channel as swl_channel_t type
 * @param country: country as enum type
 * @return true if the channel is a high power channel
 */
bool swl_channel_isHighPowerWithCountry(const swl_channel_t channel, const swl_opClassCountry_e country) {
    ASSERTS_TRUE(swl_channel_is5g(channel), false, ME, "Not 5GHz channel");
    if(country == SWL_OP_CLASS_COUNTRY_USA) {
        return (((channel >= SWL_CHANNEL_5G_UNII_1_START) && (channel <= SWL_CHANNEL_5G_UNII_1_END))
                || ((channel >= SWL_CHANNEL_5G_UNII_3_START) && (channel <= SWL_CHANNEL_5G_UNII_4_END)));
    } else {
        return (channel >= SWL_CHANNEL_5G_UNII_2C_START) && (channel <= SWL_CHANNEL_5G_UNII_4_END);
    }
}
