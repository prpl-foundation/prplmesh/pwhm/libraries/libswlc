/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include  "swl/swl_base64.h"
#include "swl/swl_string.h"



#define ME "swlBase64"



static const swl_bit8_t sBase64Table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

ssize_t swl_base64_encode(char* tgtStr, size_t tgtStrSize, const swl_bit8_t* src, size_t srcSize) {
    ASSERT_NOT_NULL(tgtStr, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(src, SWL_RC_INVALID_PARAM, ME, "NULL");

    size_t outputLength = ((srcSize * 4) / 3 ) + 4; /* 3-byte blocks to 4-byte */
    outputLength++;                                 /* nul termination */
    if(outputLength < srcSize) {
        SAH_TRACEZ_ERROR(ME, "Integer overflow!");
        return SWL_RC_ERROR;
    }
    if(outputLength > tgtStrSize) {
        return (outputLength - 1);// when return, null terminator is not counted
    }
    memset(tgtStr, 0, tgtStrSize);
    const swl_bit8_t* end = src + srcSize;
    char* pos = tgtStr;
    while(end - src >= 3) {
        *pos = sBase64Table[src[0] >> 2];
        pos++;
        *pos = sBase64Table[((src[0] & 0x03) << 4) | (src[1] >> 4)];
        pos++;
        *pos = sBase64Table[((src[1] & 0x0f) << 2) | (src[2] >> 6)];
        pos++;
        *pos = sBase64Table[src[2] & 0x3f];
        pos++;
        src += 3;
    }

    if(end - src) {
        *pos = sBase64Table[src[0] >> 2];
        pos++;
        if(end - src == 1) {
            *pos = sBase64Table[(src[0] & 0x03) << 4];
            pos++;
            *pos = '=';
            pos++;
        } else {
            *pos = sBase64Table[((src[0] & 0x03) << 4) |
            (src[1] >> 4)];
            pos++;
            *pos = sBase64Table[(src[1] & 0x0f) << 2];
            pos++;
        }
        *pos = '=';
        pos++;
    }
    outputLength = pos - tgtStr;
    return outputLength;
}

ssize_t swl_base64_decode(swl_bit8_t* tgtStr, size_t tgtStrSize, const char* src) {
    ASSERT_NOT_NULL(tgtStr, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(src, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_bit8_t dtable[256] = {0x80};
    dtable['='] = 0;
    size_t i = 0;
    for(i = 0; i < sizeof(sBase64Table) - 1; i++) {
        dtable[sBase64Table[i]] = (swl_bit8_t) i;
    }

    size_t count = 0;
    size_t srcSize = swl_str_len(src);
    for(i = 0; i < srcSize; i++) {
        if(dtable[(uint8_t) src[i]] != 0x80) {
            count++;
        }
    }

    if((count == 0) || count % 4) {
        return SWL_RC_ERROR;
    }

    size_t outputLength = (count / 4) * 3;
    if(outputLength > tgtStrSize) {
        return outputLength - 1;
    }
    memset(tgtStr, 0, tgtStrSize);

    swl_bit8_t* pos = tgtStr;
    count = 0;
    swl_bit8_t block[4];
    size_t pad = 0;
    for(i = 0; i < srcSize; i++) {
        swl_bit8_t tmp = dtable[(uint8_t) src[i]];
        if(tmp == 0x80) {
            continue;
        }

        if(src[i] == '=') {
            pad++;
        }
        block[count] = tmp;
        count++;
        if(count == 4) {
            *pos = (block[0] << 2) | (block[1] >> 4);
            pos++;
            *pos = (block[1] << 4) | (block[2] >> 2);
            pos++;
            *pos = (block[2] << 6) | block[3];
            pos++;
            count = 0;
            if(pad) {
                if(pad == 1) {
                    pos--;
                } else if(pad == 2) {
                    pos -= 2;
                } else {
                    /* Invalid padding */
                    SAH_TRACEZ_ERROR(ME, "Invalid padding");
                    return SWL_RC_ERROR;
                }
                break;
            }
        }
    }

    outputLength = pos - tgtStr;
    return outputLength;
}

