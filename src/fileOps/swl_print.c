/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "swl/swl_common.h"
#include "swl/fileOps/swl_print.h"
#include "swl/swl_string.h"



const swl_print_args_t g_swl_print_json = {
    .delim = {"{", ":", ",", "}", "[", ",", "]"},
    .charsToEscape = "\"\\",
    .escapeChar = '\\',
    .indentation = 0,
    .valueEncap = '\"',
    .nullEncode = "null",
    .options = M_SWL_PRINT_PRETTY,
};
const swl_print_args_t g_swl_print_jsonCompact = {
    .delim = {"{", ":", ",", "}", "[", ",", "]"},
    .charsToEscape = "\"\\",
    .escapeChar = '\\',
    .indentation = 0,
    .valueEncap = '\"',
    .nullEncode = "null",
    .noNewLineIndentLevel = 1,
    .options = M_SWL_PRINT_PRETTY,
};

const swl_print_args_t g_swl_print_dm = {
    .delim = {"{", ":", ",", "}", "[", ",", "]"},
    .charsToEscape = NULL,
    .nullEncode = "\\0",
    .indentation = 0,
};

const swl_print_args_t g_swl_print_csv = {
    .delim = {"", "=", ",", "", "", ",", ""},
    .charsToEscape = "\\,=",
    .escapeChar = '\\',
    .nullEncode = "\\0",
    .indentation = 0,
};

const swl_print_args_t g_swl_print_default = {
    .delim = {"{", "=", ",", "}", "[", ",", "]"},
    .charsToEscape = "\"\\,{}[]",
    .escapeChar = '\\',
    .nullEncode = "\\0",
    .indentation = 0,
};

#define ME "swlPrnt"

/**
 * Write to stream, escaping whatever characters need to be escaped
 */
bool swl_print_toStreamBuf(FILE* stream, swl_print_args_t* printArgs, const char* buf, size_t strLen) {
    ASSERT_NOT_NULL(stream, false, ME, "NULL");
    ASSERT_NOT_NULL(buf, false, ME, "NULL");
    ASSERT_NOT_NULL(printArgs, false, ME, "NULL");
    if(printArgs->charsToEscape != NULL) {
        char escBuf[strLen * 2 + 1];
        memcpy(escBuf, buf, strLen + 1);
        swl_str_addEscapeChar(escBuf, sizeof(escBuf), printArgs->charsToEscape, printArgs->escapeChar);

        fprintf(stream, "%s", escBuf);
    } else {
        fprintf(stream, "%s", buf);
    }
    return true;
}

//Print data to stream, escaping what needs to be escaped
bool swl_print_toStreamArgs(FILE* stream, swl_print_args_t* printArgs, const char* format, ...) {
    ASSERT_NOT_NULL(stream, false, ME, "NULL");
    ASSERT_NOT_NULL(format, false, ME, "NULL");

    char* buf;
    va_list args;
    va_start(args, format);
    int ret = vasprintf(&buf, format, args);
    va_end(args);

    if(ret < 0) {
        return false;
    }

    bool result = swl_print_toStreamBuf(stream, printArgs, buf, swl_str_len(buf));

    free(buf);
    return result;
}

/**
 * Print a value to stream. Format value can be NULL, in which case the null encode can be printed.
 * It will be surrounded in value cases if applicable
 * It will be escaped if applicable.
 */
bool swl_print_valueToStreamArgs(FILE* stream, swl_print_args_t* printArgs, const char* format, ...) {
    ASSERT_NOT_NULL(stream, false, ME, "NULL");
    if(format == NULL) {
        if(printArgs->nullEncode != NULL) {
            fprintf(stream, "%s", printArgs->nullEncode);
        }
        return true;
    }

    char* buf;
    va_list args;
    va_start(args, format);
    int ret = vasprintf(&buf, format, args);
    va_end(args);

    if(ret < 0) {
        return false;
    }

    swl_print_startValue(stream, printArgs);
    bool result = swl_print_toStreamBuf(stream, printArgs, buf, swl_str_len(buf));
    swl_print_stopValue(stream, printArgs);

    free(buf);
    return result;
}

const swl_print_args_t* swl_print_getDefaultArgs() {
    return &g_swl_print_default;
}

ssize_t s_getMatchingClose(const swl_print_args_t* printArgs, const char* elementStart, bool list) {
    const char* openVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_OPEN : SWL_PRINT_DELIM_MAP_OPEN];
    const char* closeVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_CLOSE : SWL_PRINT_DELIM_MAP_CLOSE];

    ssize_t openIndex = swl_str_findNoEscape(elementStart, openVal, printArgs->escapeChar);
    ssize_t closeIndex = swl_str_findNoEscape(elementStart, closeVal, printArgs->escapeChar);

    const char* nextSearch = elementStart;

    size_t depth = 0;
    ssize_t size = 0;

    while(depth > 0 || (openIndex >= 0 && openIndex < closeIndex)) {
        if((openIndex >= 0) && (openIndex < closeIndex)) {
            depth++;
            nextSearch = &nextSearch[openIndex + 1];
            size += openIndex + 1;
        } else {
            depth--;
            nextSearch = &nextSearch[closeIndex + 1];
            size += closeIndex + 1;
        }

        openIndex = swl_str_findNoEscape(nextSearch, openVal, printArgs->escapeChar);
        closeIndex = swl_str_findNoEscape(nextSearch, closeVal, printArgs->escapeChar);
    }

    if(closeIndex < 0) {
        return closeIndex;
    }
    return size + closeIndex;
}

/**
 * Returns the element size of the next element.
 * If there is no more elements to be found, a negative number is returned.
 * Note that a zero size is possible, and indicates a zero (empty) element.
 * @param printArgs: the print arguments to parse
 * @param elementStart: pointer to the string where element should begin, so after the initial delimiter
 * @param list: whether or not we look for a list element.
 */
ssize_t swl_print_getElementSize(const swl_print_args_t* printArgs, const char* elementStart, bool list) {
    const char* nextVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_NEXT : SWL_PRINT_DELIM_MAP_NEXT];
    const char* closeVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_CLOSE : SWL_PRINT_DELIM_MAP_CLOSE];

    if(strlen(closeVal) == 0) {
        ssize_t nextIndex = swl_str_findNoEscape(elementStart, nextVal, printArgs->escapeChar);
        if(nextIndex >= 0) {
            return nextIndex;
        } else {
            return strlen(elementStart);
        }
    }


    if(swl_str_startsWith(elementStart, printArgs->delim[SWL_PRINT_DELIM_LIST_OPEN])) {
        return s_getMatchingClose(printArgs, elementStart + 1, true) + 2;
    } else if(swl_str_startsWith(elementStart, printArgs->delim[SWL_PRINT_DELIM_MAP_OPEN])) {
        return s_getMatchingClose(printArgs, elementStart + 1, false) + 2;
    } else {
        ssize_t nextIndex = swl_str_findNoEscape(elementStart, nextVal, printArgs->escapeChar);
        ssize_t closeIndex = swl_str_findNoEscape(elementStart, closeVal, printArgs->escapeChar);
        if((nextIndex != -1) && (nextIndex < closeIndex)) {
            return nextIndex;
        } else {
            return closeIndex;
        }
    }
}

/**
 * Return the size of the "key" of key value pairs.
 * @param printArgs: the print arguments to parse
 * @param elementStart: pointer to the string where assign element should begin, so after the initial delimiter
 * Returns negative number if no key element can be found. Empty can be valid
 */
ssize_t swl_print_getMapKeySize(const swl_print_args_t* printArgs, const char* elementStart) {
    ssize_t nextIndex = swl_str_findNoEscape(elementStart, printArgs->delim[SWL_PRINT_DELIM_MAP_ASSIGN], printArgs->escapeChar);
    return nextIndex;
}

/**
 * Return the size of the first next entry, which should be before the given delimiter. So valid values for delim are
 * * SWL_PRINT_DELIM_MAP_NEXT: return size of the value of a key value pair
 * * SWL_PRINT_DELIM_LIST_NEXT: return size of the element in a list
 * * SWL_PRINT_DELIM_MAP_ASSIGN: return size of the key of a key value pair
 *
 * @param printArgs: the print arguments to parse
 * @param elementStart: pointer to the string where assign element should begin, so after the delimiter
 * @param delim: the delimiter with which the next block should end
 *
 */
ssize_t swl_print_getNextSize(const swl_print_args_t* printArgs, const char* elementStart, swl_print_delim_e delim) {
    if(delim == SWL_PRINT_DELIM_MAP_NEXT) {
        return swl_print_getElementSize(printArgs, elementStart, false);
    } else if(delim == SWL_PRINT_DELIM_LIST_NEXT) {
        return swl_print_getElementSize(printArgs, elementStart, true);
    } else if(delim == SWL_PRINT_DELIM_MAP_ASSIGN) {
        return swl_print_getMapKeySize(printArgs, elementStart);
    }
    return SWL_RC_INVALID_PARAM;
}

static bool s_isElement(const swl_print_args_t* printArgs, const char* elementStart, size_t len) {
    if(printArgs->valueEncap == 0) {
        // No value encapsulation, so anything in the element counts as value
        return len > 0;
    } else {
        return !swl_str_isOnlyWhitespace(elementStart, len);
    }
}

/**
 * Return whether the string has a next element (i.e. whether we should still parse something afterwards).
 * @param printArgs: the print arguments to parse
 * @param elementStart: pointer to the string immediately after the OPEN character, or else immediately behind the
 * previous element, which will usually be either the NEXT or CLOSED character, or whitespace in front of it.
 * Note that this is different from all other functions, as this is used to determine whether there is anything
 * to parse at all.
 * valueEncap sign is set.
 */
bool swl_print_hasNext(const swl_print_args_t* printArgs, const char* elementStart, bool list) {

    const char* nextVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_NEXT : SWL_PRINT_DELIM_MAP_NEXT];
    const char* closeVal = printArgs->delim[list ? SWL_PRINT_DELIM_LIST_CLOSE : SWL_PRINT_DELIM_MAP_CLOSE];

    ssize_t nextIndex = swl_str_findNoEscape(elementStart, nextVal, printArgs->escapeChar);
    if(strlen(closeVal) == 0) {
        return nextIndex >= 0;
    }

    ssize_t closeIndex = swl_str_findNoEscape(elementStart, closeVal, printArgs->escapeChar);


    if(closeIndex < 0) {
        return false;
    }
    if(nextIndex < 0) {
        return s_isElement(printArgs, elementStart, closeIndex);
    }
    if(nextIndex < closeIndex) {
        return true;
    }
    return s_isElement(printArgs, elementStart, closeIndex);
}

void s_nextLine(FILE* file, swl_print_args_t* args, ssize_t indentIncrement) {
    if(indentIncrement < 0) {
        args->indentation += indentIncrement;
    }
    size_t checkLevel = args->indentation;
    if(indentIncrement == 0) {
        checkLevel -= 1;
    }

    if((args->noNewLineIndentLevel != 0) && (checkLevel >= args->noNewLineIndentLevel)) {
        if(indentIncrement > 0) {
            args->indentation += indentIncrement;
        }
        return;
    }
    fprintf(file, "\n");
    if(indentIncrement > 0) {
        args->indentation += indentIncrement;
    }
    for(size_t i = 0; i < args->indentation; i++) {
        fprintf(file, "    ");
    }
}

void swl_print_addOpen(FILE* file, swl_print_args_t* args, bool list) {
    fprintf(file, "%s", args->delim[list ? SWL_PRINT_DELIM_LIST_OPEN : SWL_PRINT_DELIM_MAP_OPEN]);
    if(args->options & M_SWL_PRINT_OPEN_NEW_LINE) {
        s_nextLine(file, args, +1);
    } else if(args->options & M_SWL_PRINT_ADD_SPACE) {
        fprintf(file, " ");
    }
}

void swl_print_addClose(FILE* file, swl_print_args_t* args, bool list) {

    if(args->options & M_SWL_PRINT_CLOSE_PRE_NEW_LINE) {
        s_nextLine(file, args, -1);
    } else if(args->options & M_SWL_PRINT_ADD_SPACE) {
        fprintf(file, " ");
    }

    fprintf(file, "%s", args->delim[list ? SWL_PRINT_DELIM_LIST_CLOSE : SWL_PRINT_DELIM_MAP_CLOSE]);

    if(args->options & M_SWL_PRINT_CLOSE_POST_NEW_LINE) {
        s_nextLine(file, args, 0);
    } else if(args->options & M_SWL_PRINT_ADD_SPACE) {
        fprintf(file, " ");
    }
}

void swl_print_addNext(FILE* file, swl_print_args_t* args, bool list) {
    fprintf(file, "%s", args->delim[list ? SWL_PRINT_DELIM_LIST_NEXT : SWL_PRINT_DELIM_MAP_NEXT]);
    if(args->options & M_SWL_PRINT_NEXT_NEW_LINE) {
        s_nextLine(file, args, 0);
    } else if(args->options & M_SWL_PRINT_ADD_SPACE) {
        fprintf(file, " ");
    }
}

void swl_print_addAssign(FILE* file, swl_print_args_t* args) {
    if(args->options & M_SWL_PRINT_ASSIGN_NEW_LINE) {
        fprintf(file, "%s", args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
        args->indentation += 2;
        s_nextLine(file, args, 0);
    } else if(args->options & M_SWL_PRINT_ADD_SPACE) {
        fprintf(file, " %s ", args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
    } else {
        fprintf(file, "%s", args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
    }
}

void swl_print_startValue(FILE* file, swl_print_args_t* args) {
    if(args->valueEncap != '\0') {
        fprintf(file, "%c", args->valueEncap);
    }
}

void swl_print_stopValue(FILE* file, swl_print_args_t* args) {
    if(args->valueEncap != '\0') {
        fprintf(file, "%c", args->valueEncap);
    }
}


