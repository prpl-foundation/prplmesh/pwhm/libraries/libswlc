/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>

#include <swl/swl_common.h>
#include <swl/swl_string.h>
#include "swl/fileOps/swl_fileUtils.h"
#include "debug/sahtrace.h"

#define ME "swlFile"
#define DEFAULT_FILE_SIZE (1024 * 1024)

/**
 * Compare two files and check if their content matches. If the files cannot be opened
 * they are by default considered unequal and false is returned
 */
bool swl_fileUtils_contentMatches(const char* file1, const char* file2) {
    ASSERT_STR(file1, false, ME, "INVALID");
    ASSERT_STR(file2, false, ME, "INVALID");
    if(swl_str_matches(file1, file2)) {
        return true;
    }

    FILE* fp1 = fopen(file1, "r");
    ASSERTS_NOT_NULL(fp1, false, ME, "ERROR");


    FILE* fp2 = fopen(file2, "r");
    if(fp2 == NULL) {
        fclose(fp1);
        return false;
    }

    char buffer1[4096] = {0};
    char buffer2[4096] = {0};

    bool success = true;
    bool goOn = true;

    while(success && goOn) {
        size_t nread1 = fread(buffer1, 1, sizeof(buffer1), fp1);
        size_t nread2 = fread(buffer2, 1, sizeof(buffer2), fp2);

        success = (nread1 == nread2) && memcmp(buffer1, buffer2, nread1) == 0;
        success &= (ferror(fp1) == 0) && (ferror(fp2) == 0);
        goOn = (nread1 == sizeof(buffer1));
    }

    fclose(fp1);
    fclose(fp2);
    return success;
}

/**
 * Get the line number, offset and the contents of the first line where file1 differs from file2.
 * Returns SWL_RC_OK if diff is found.
 * Returns SWL_RC_ERROR if both files are equal (i.e. equal file name or content equal).
 * Returns SWL_RC_INVALID_PARAM if for some reason comparison can not be done
 * Returns SWL_RC_NOT_AVAILABLE if files can not be opened.
 */
swl_rc_ne swl_fileUtils_getContentDiff(const char* file1, const char* file2, char* buf1, char* buf2, size_t bufSize, size_t* line, size_t* offset) {
    ASSERT_STR(file1, SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_STR(file2, SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_NOT_NULL(buf1, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(buf2, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(line, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(offset, SWL_RC_INVALID_PARAM, ME, "NULL");
    if(swl_str_matches(file1, file2)) {
        return SWL_RC_ERROR;
    }

    FILE* fp1 = fopen(file1, "r");
    ASSERTS_NOT_NULL(fp1, SWL_RC_NOT_AVAILABLE, ME, "ERROR");


    FILE* fp2 = fopen(file2, "r");
    if(fp2 == NULL) {
        fclose(fp1);
        return SWL_RC_NOT_AVAILABLE;
    }

    bool equals;
    *line = 0;

    while(true) {
        memset(buf1, 0, bufSize);
        memset(buf2, 0, bufSize);
        char* test1 = fgets(buf1, bufSize, fp1);
        char* test2 = fgets(buf2, bufSize, fp2);
        if((test1 == NULL) || (test2 == NULL)) {
            equals = (test1 == NULL && test2 == NULL);
            break;
        } else {
            size_t len1 = strlen(test1);
            size_t len2 = strlen(test2);
            ssize_t test = swl_str_ngetMismatchIndex(buf1, buf2, SWL_MIN(len1, len2));
            if(test >= 0) {
                *offset = (size_t) test;
                equals = false;
                break;
            } else if(len1 == bufSize - 1) {
                equals = false;
                *offset = bufSize;
                break;
            }
        }
        *line = *line + 1;
    }

    fclose(fp1);
    fclose(fp2);
    return equals ? SWL_RC_ERROR : SWL_RC_OK;

}

/**
 * Copy a file from one location to another location.
 */
swl_rc_ne swl_fileUtils_copy(const char* to, const char* from) {
    ASSERT_STR(to, SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_STR(from, SWL_RC_INVALID_PARAM, ME, "INVALID");

    FILE* toFile = fopen(to, "w+");
    ASSERT_NOT_NULL(toFile, SWL_RC_ERROR, ME, "ERROR");

    FILE* fromFile = fopen(from, "r");
    if(fromFile == NULL) {
        SAH_TRACEZ_ERROR(ME, "ERROR");
        fclose(toFile);
        return SWL_RC_ERROR;
    }

    char buf[4096];
    size_t nread = fread(buf, 1, sizeof(buf), fromFile);
    swl_rc_ne retCode = (ferror(fromFile) == 0 ? SWL_RC_OK : SWL_RC_ERROR);

    while(nread > 0 && retCode == SWL_RC_OK) {
        size_t nwrite = fwrite(buf, 1, nread, toFile);
        if(nwrite != nread) {
            retCode = SWL_RC_ERROR;
        } else {
            nread = fread(buf, 1, sizeof(buf), fromFile);
            retCode = (ferror(fromFile) == 0 ? SWL_RC_OK : SWL_RC_ERROR);
        }
    }

    fclose(fromFile);
    fclose(toFile);
    return retCode;
}


/**
 * try to create a directory recursively.
 * It will return true if the directory exists, so either when it is made, or already existed before.
 * It will return false if creating the directory failed.
 */
bool swl_fileUtils_mkdirRecursive(const char* dirName) {
    char tmp[256];
    size_t len = swl_str_len(dirName);
    swl_str_copy(tmp, sizeof(tmp), dirName);
    ASSERT_TRUE(len, false, ME, "EMPTY");
    ASSERTI_FALSE(swl_fileUtils_existsDir(dirName), true, ME, "EXISTS");

    if(tmp[len - 1] == '/') {
        tmp[len - 1] = 0;
    }
    for(size_t i = 1; i < len; i++) {
        if(tmp[i] == '/') {
            tmp[i] = 0;
            if(!swl_fileUtils_existsDir(tmp)) {
                int ret = mkdir(tmp, S_IRWXU);
                if(ret < 0) {
                    return false;
                }
            }
            tmp[i] = '/';
        }
    }
    return mkdir(tmp, S_IRWXU) >= 0;
}


/**
 * Check if a given file exists and is a regular file. Return true if so, false otherwise.
 */
bool swl_fileUtils_existsFile(const char* fname) {
    struct stat sb;
    return (stat(fname, &sb) == 0 && S_ISREG(sb.st_mode));
}

/**
 * Check if a given file exists and is a directory. Return true if so, false otherwise.
 */
bool swl_fileUtils_existsDir(const char* dirName) {
    struct stat sb;
    return (stat(dirName, &sb) == 0 && S_ISDIR(sb.st_mode));
}

/**
 * @brief get the file size
 *
 * @param pathname the file name
 *
 * @return the file size on success. Otherwise, -1.
 */
int64_t swl_fileUtils_getFileSize(const char* pathname) {
    SAH_TRACEZ_IN(ME);
    ASSERTS_NOT_NULL(pathname, -1, ME, "NULL");
    struct stat sb;
    int fileSize;
    if(stat(pathname, &sb) == 0) {
        fileSize = sb.st_size;
    } else {
        SAH_TRACEZ_ERROR(ME, "error %s(%d) when reading the file size of %s", strerror(errno), errno, pathname);
        fileSize = -1;
    }
    SAH_TRACEZ_OUT(ME);
    return fileSize;
}

/**
 * @brief read a file into a memory buffer.
 * This function allocates the memory buffer. It should be freed later by the user.
 * Reading buffer could be truncated if the maxFileSize < pathname size
 *
 * @param tgtBuf memory buffer where the file is read
 * @param pathname the file name
 * @param maxFileSize the limit of file size. The default value is -1 which means DEFAULT_FILE_SIZE(1Mbytes)
 *
 * @return SWL_RC_OK when the file is copied into memory buffer. Otherwise, error.
 */
swl_rc_ne swl_fileUtils_readFileAlloc(char** tgtBuf, const char* pathname, int64_t maxFileSize) {
    SAH_TRACEZ_IN(ME);
    ASSERT_NOT_NULL(tgtBuf, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(pathname, SWL_RC_INVALID_PARAM, ME, "NULL");

    // Get the file size
    int64_t fileSize = swl_fileUtils_getFileSize(pathname);
    ASSERT_FALSE(fileSize < 0, SWL_RC_ERROR, ME, "wrong file size");

    if(maxFileSize == -1) {
        maxFileSize = DEFAULT_FILE_SIZE;
    }

    if(fileSize > maxFileSize) {
        SAH_TRACEZ_WARNING(ME, "%s: reading file will be truncated because fileSize(%" PRId64 ") > maxFileSize(%" PRId64 ")",
                           pathname, fileSize, maxFileSize);
        fileSize = maxFileSize;
    }

    // Allocate the memory buffer containing the config file
    *tgtBuf = malloc(fileSize + 1);
    ASSERT_NOT_NULL(*tgtBuf, SWL_RC_ERROR, ME, "NULL");

    // Read the config file into the memory buffer
    FILE* fp = fopen(pathname, "r");
    ASSERTS_NOT_NULL(fp, SWL_RC_ERROR, ME, "NULL");
    size_t readBytes = fread(*tgtBuf, 1, fileSize, fp);
    fclose(fp);
    if(readBytes != (size_t) fileSize) {
        SAH_TRACEZ_ERROR(ME, "failed to read %s", pathname);
        free(*tgtBuf);
        *tgtBuf = NULL;
        SAH_TRACEZ_OUT(ME);
        return SWL_RC_ERROR;
    }
    (*tgtBuf)[fileSize] = '\0';
    SAH_TRACEZ_OUT(ME);
    return SWL_RC_OK;
}

/**
 * Read the leftover file size from a file buffer.
 */
size_t swl_fileUtils_readLeftoverFileSize(FILE* file) {
    ASSERT_NOT_NULL(file, 0, ME, "NULL");
    size_t curSize = ftell(file);
    fseek(file, 0, SEEK_END);
    size_t endSize = ftell(file);
    size_t diff = endSize - curSize;
    fseek(file, curSize, SEEK_SET);
    return diff;
}

/**
 * @brief read a file into a memory buffer
 *
 * @param tgtBuf memory buffer where the file is read
 * @param tgtBufSize the size of the memory buffer
 * @param pathname the file name
 *
 * @return SWL_RC_OK when the file is copied into memory buffer. Otherwise, error.
 */
swl_rc_ne swl_fileUtils_readFile(char* tgtBuf, size_t tgtBufSize, const char* pathname) {
    SAH_TRACEZ_IN(ME);
    ASSERT_NOT_NULL(tgtBuf, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(pathname, SWL_RC_INVALID_PARAM, ME, "NULL");

    // Get the file size
    int64_t fileSize = swl_fileUtils_getFileSize(pathname);
    ASSERT_FALSE(fileSize < 0, SWL_RC_ERROR, ME, "wrong file size");
    ASSERT_TRUE(fileSize < (int) (tgtBufSize - 1), SWL_RC_ERROR, ME, "wrong buffer size");

    // Read the config file into the memory buffer
    FILE* fp = fopen(pathname, "r");
    ASSERTS_NOT_NULL(fp, SWL_RC_ERROR, ME, "NULL");
    size_t readBytes = fread(tgtBuf, 1, fileSize, fp);
    fclose(fp);
    if(readBytes != (size_t) fileSize) {
        SAH_TRACEZ_ERROR(ME, "failed to read %s", pathname);
        SAH_TRACEZ_OUT(ME);
        return SWL_RC_ERROR;
    }
    tgtBuf[fileSize] = '\0';
    SAH_TRACEZ_OUT(ME);
    return SWL_RC_OK;
}

/**
 * @brief write the file from memory buffer.
 * The pathname file will be overwritten by srcBuf content.
 *
 * @param srcBuf memory buffer containing the file
 * @param pathname the file name
 *
 * @return true on success. Otherwise, false.
 */
bool swl_fileUtils_writeFile(char* srcBuf, const char* pathname) {
    SAH_TRACEZ_IN(ME);
    ASSERT_NOT_NULL(pathname, false, ME, "NULL");
    ASSERT_NOT_NULL(srcBuf, false, ME, "NULL");
    char tmpName[128];

    snprintf(tmpName, sizeof(tmpName), "%s.tmp.txt", pathname);

    FILE* fp = fopen(tmpName, "w");
    ASSERT_NOT_NULL(fp, false, ME, "NULL");
    int res = fputs(srcBuf, fp);
    bool ret = (res != EOF) ? true : false;
    fclose(fp);
    if(ret == true) {
        ret = rename(tmpName, pathname) >= 0 ? true : false;
    }
    unlink(tmpName);
    SAH_TRACEZ_OUT(ME);
    return ret;
}

static bool s_fileSmallEnough(const char* from, uint32_t threshold) {
    ASSERTS_NOT_NULL(from, false, ME, "NULL");
    struct stat st = {.st_size = 0};
    stat(from, &st);
    uint32_t fileSize = (uint32_t) st.st_size;
    if(fileSize < threshold) {
        SAH_TRACEZ_INFO(ME, "File small enough %u of %u", fileSize, threshold);
        return true;
    } else {
        SAH_TRACEZ_INFO(ME, "File over threshold: copy %u of %u", fileSize, threshold);
        return false;
    }
}

swl_rc_ne swl_fileUtils_copyFileIfTooBig(const char* from, const char* tag, uint32_t maxFileSize) {
    ASSERTS_NOT_NULL(from, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTI_FALSE(s_fileSmallEnough(from, maxFileSize), SWL_RC_OK, ME, "Enough space");

    if(tag == NULL) {
        //Default tag name
        tag = "2";
    }

    char to[128];
    snprintf(to, sizeof(to), "%s%s", from, tag);

    unlink(to);
    rename(from, to);
    return SWL_RC_OK;
}

void swl_fileUtils_writeLine(const char* fileName, const char* tagName, const char* buffer, uint32_t maxFileSize) {
    ASSERTS_NOT_NULL(buffer, , ME, "NULL");
    FILE* fp = fopen(fileName, "a");
    ASSERTI_NOT_NULL(fp, , ME, "error opening file");

    swl_fileUtils_copyFileIfTooBig(fileName, tagName, maxFileSize);

    fprintf(fp, "\n");
    fprintf(fp, "%s", buffer);
    fflush(fp);
    fclose(fp);
}

/**
 * @brief This function reads a file in binary mode and searches for
 * a specific string that begins with the provided prefix.
 * If allOccur is set to true, this function will fetch all occurrences
 * starting with the provided prefix, in a comma separated list.
 *
 * @param tgtBuf buffer where the located string is copied
 * @param tgtBufSize The maximum number of charecters to be copied from the located string to the tgtBuf
 * @param filePath The file path
 * @param prefix The beginning of the searched string within the file
 * @param allOccur indicates whether to retrieve the first occurence or all occurences
 *
 * @return Then number of occurrences of the located string.
 */
uint32_t swl_fileUtils_findStrInFile(char* tgtBuf, size_t tgtBufSize, const char* filePath, const char* prefix, bool allOccur) {
    uint32_t count = 0;
    ASSERT_NOT_NULL(tgtBuf, count, ME, "NULL");
    ASSERT_TRUE(tgtBufSize > 0, count, ME, "wrong buffer size");
    memset(tgtBuf, 0, tgtBufSize);
    ASSERT_NOT_NULL(filePath, count, ME, "NULL");
    ASSERT_STR(prefix, count, ME, "Invalid prefix");
    size_t windowSize = swl_str_len(prefix);
    ASSERT_TRUE(windowSize < tgtBufSize, count, ME, "prefix longer that target buffer");

    FILE* fp = fopen(filePath, "rb");
    ASSERT_NOT_NULL(fp, count, ME, "Failure to open %s", filePath);

    char window[windowSize];
    memset(window, 0, windowSize);
    uint32_t nBytesInWindow = 0;
    bool startCopy = false;
    bool stopRead = false;
    do {
        size_t nBytesRead = fread(&window[nBytesInWindow], sizeof(char), (windowSize - nBytesInWindow), fp);
        //No data read, we can leave
        if(!nBytesRead) {
            break;
        }
        nBytesInWindow += nBytesRead;
        //Wait having full window to start checking bytes
        //unless processing last remaining bytes of previously found prefix
        if((!startCopy) && (nBytesInWindow < windowSize)) {
            continue;
        }
        //check token to start copying bytes, or detect successive tokens
        if(!memcmp(window, prefix, windowSize)) {
            if(((count > 0) && (!allOccur)) || (!swl_strlst_cat(tgtBuf, tgtBufSize, ",", ""))) {
                break;
            }
            startCopy = true;
            count++;
        }
        uint32_t i = 0;
        uint32_t j = nBytesInWindow;
        //process bytes in window
        for(; i < nBytesInWindow; i++, j--) {
            if(isprint(window[i]) || isblank(window[i])) {
                //if printable chars:
                //check if belonging to current prefix sequence
                //or potential beginning of then next one
                if(((!startCopy) || (i > 0)) && (!memcmp(&window[i], prefix, j))) {
                    //when remaining bytes in window, of the current sequence,
                    //are matching the begging of prefix token
                    //then delay processing by moving them to window head,
                    //to be appended with new bytes on next read
                    //for a full prefix check
                    memmove(&window[0], &window[i], j);
                    break;
                }
                //concat valid printable char to current sequence
                if((startCopy) && (!swl_str_catChar(tgtBuf, tgtBufSize, window[i]))) {
                    stopRead = true;
                    break;
                }
            } else {
                if((count > 0) && (!allOccur)) {
                    stopRead = true;
                    break;
                }
                startCopy = false;
            }
        }
        nBytesInWindow = j;
    } while(!feof(fp) && !stopRead);

    fclose(fp);

    return count;
}
