/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/****
* A key value file writer. Allows writing the map to file as a fresh file, i.e. deleting the old
* file and writing the values anew, or just updating the values in the file.
****/


#include <stdlib.h>
#include "debug/sahtrace.h"

#include "swl/swl_common.h"
#include "swl/swl_string.h"
#include "swl/swl_map.h"
#include "swl/swl_maps.h"
#include "swl/fileOps/swl_print.h"
#include "swl/fileOps/swl_fileUtils.h"
#include "swl/fileOps/swl_mapWriterKVP.h"

#define CHARS_TO_ESCAPE "\\="
#define ESCAPE_CHAR '\\'

#define ME "swlMapK"

/**
 * Write contents of map to file, in key=value format.
 * If this function fails (e.g. due to full file system), the previous content may be
 * discarded or partially discarded without updated content to replace it.
 */
bool swl_mapWriterKVP_writeToFileEsc(swl_map_t* map, const char* fileLocation, const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_STR(fileLocation, false, ME, "NULL");

    char tmpName[128];
    snprintf(tmpName, sizeof(tmpName), "%s.tmp.txt", fileLocation);


    FILE* fptr = fopen(tmpName, "w");
    ASSERT_NOT_NULL(fptr, false, ME, "NULL");
    bool success = swl_mapWriterKVP_writeToFptrEsc(map, fptr, charsToEscape, escapeChar);
    fclose(fptr);

    if(!success) {
        return false;
    }

    return rename(tmpName, fileLocation) >= 0;
}

bool swl_mapWriterKVP_writeToFile(swl_map_t* map, const char* fileLocation) {
    return swl_mapWriterKVP_writeToFileEsc(map, fileLocation, CHARS_TO_ESCAPE, ESCAPE_CHAR);
}

/**
 * @brief write the content of a map into a string pointer in key=value format.
 *
 * @param map having key=value format
 * @param buffer output buffer
 * @param bufferSize output buffer size
 *
 * @return true when the map is written in a string pointer. Otherwise, false.
 */
bool swl_mapWriterKVP_writeToStr(swl_map_t* map, char* buffer, size_t bufferSize) {
    return swl_map_toCharSepEsc(buffer, bufferSize, map, '=', '\n', CHARS_TO_ESCAPE, ESCAPE_CHAR);
}

/**
 * @brief write the content of a map into a file in key=value format.
 *
 * @param map having key=value format
 * @param fptr a file pointer. It is opended and closed by the caller.
 * @param charsToEscape characters to escape
 * @param escapeChar character to put in front of charsToEscape when found
 *
 * @return true when the map is written in a file pointer. Otherwise, false.
 */
bool swl_mapWriterKVP_writeToFptrEsc(swl_map_t* map, FILE* fptr, const char* charsToEscape, char escapeChar) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_NOT_NULL(fptr, false, ME, "NULL");

    swl_print_args_t myArgs;
    memcpy(&myArgs, &g_swl_print_dm, sizeof(swl_print_args_t));
    myArgs.charsToEscape = charsToEscape;
    myArgs.escapeChar = escapeChar;


    swl_mapIt_t it;
    bool success = true;
    swl_map_for_each(it, map) {
        success &= swl_map_keyToFile(fptr, &it, &myArgs);
        success &= (fprintf(fptr, "=") > 0);
        success &= swl_map_valueToFile(fptr, &it, &myArgs);
        success &= (fprintf(fptr, "\n") > 0);
    }

    return success;
}

/**
 * Update a KVP file with the given map. It will read the file, and replace any entries at the designated
 * locations with the matching value in the map.
 * If a given key of the file does not appear in map, then value will be unchanged
 * If a given key of the map does not appear in the file, it will be append at the end of the file if append
 * parameter is true otherwise it will not be written.
 */
bool swl_mapWriterKVP_updateFileExt(swl_map_t* map, const char* fileLocation, bool append) {
    ASSERT_NOT_NULL(map, false, ME, "NULL");
    ASSERT_STR(fileLocation, false, ME, "NULL");

    swl_mapChar_t missingKeysMap;
    swl_mapChar_init(&missingKeysMap);
    swl_map_copyTo(&missingKeysMap, map);

    FILE* readPtr = fopen(fileLocation, "r");
    char tmpName[128];
    snprintf(tmpName, sizeof(tmpName), "%s.tmp.txt", fileLocation);
    FILE* writePtr = fopen(tmpName, "w");

    char buffer[128] = {0};
    char noEscBuffer[128] = {0};
    char tgtBuffer[128] = {0};
    char* result = fgets(buffer, sizeof(buffer), readPtr);
    while(result != NULL) {
        memset(tgtBuffer, 0, sizeof(tgtBuffer));
        uint32_t loc = swl_str_getNonEscCharLoc(buffer, '=', ESCAPE_CHAR);
        if(buffer[loc] != '=') {
            fprintf(writePtr, "%s", buffer);
            result = fgets(buffer, sizeof(buffer), readPtr);
            continue;
        }
        swl_str_copy(noEscBuffer, loc + 1, buffer);
        swl_str_removeEscapeChar(noEscBuffer, sizeof(noEscBuffer), CHARS_TO_ESCAPE, ESCAPE_CHAR);
        bool hasEntry = swl_map_getCharFromChar(tgtBuffer, sizeof(tgtBuffer), map, noEscBuffer);

        if(!hasEntry) {
            buffer[loc] = '=';
            fprintf(writePtr, "%s", buffer);
        } else {
            //Put unescaped file key back in noEscBuffer
            swl_str_copy(noEscBuffer, loc + 1, buffer);
            swl_str_addEscapeChar(tgtBuffer, sizeof(tgtBuffer), CHARS_TO_ESCAPE, ESCAPE_CHAR);
            fprintf(writePtr, "%s=%s\n", noEscBuffer, tgtBuffer);

            //Remove from missing keys map
            swl_mapChar_delete(&missingKeysMap, noEscBuffer);
        }
        result = fgets(buffer, sizeof(buffer), readPtr);
    }

    fclose(readPtr);
    fclose(writePtr);

    if(append) {
        FILE* appendPtr = fopen(tmpName, "a");
        bool success = swl_mapWriterKVP_writeToFptrEsc(&missingKeysMap, appendPtr, CHARS_TO_ESCAPE, ESCAPE_CHAR);
        if(!success) {
            SAH_TRACEZ_ERROR(ME, "Failed to append left entries of map");
        }
        fclose(appendPtr);
    }

    swl_map_cleanup(&missingKeysMap);
    remove(fileLocation);
    rename(tmpName, fileLocation);
    return true;
}

/**
 * Update a KVP file with the given map. It will read the file, and replace any entries at the designated
 * locations with the matching value in the map.
 * If a given key of the file does not appear in map, then value will be unchanged
 * If a given key of the map does not appear in the file, it will not be written.
 * It will only update entries that are already in the file, and not write all
 * entries that are in the map.
 */
bool swl_mapWriterKVP_updateFile(swl_map_t* map, const char* fileLocation) {
    return swl_mapWriterKVP_updateFileExt(map, fileLocation, false);
}
