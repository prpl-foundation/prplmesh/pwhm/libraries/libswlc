/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * Provide a unrolled linked list structure, that copies data in expandable storage.
 * When mentioning index, the global index is meant. When mentioning blockIndex, the index
 * of the element within the block is meant.
 *
 * Note that this means that the index and pointer value of a given element will shift
 * when adding or inserting values.
 */

#include "../include/swl/swl_unLiList.h"

#include "swl/swl_common.h"
#include "swl/swl_common_tupleType.h"
#include "malloc.h"
#define ME "swlULLi"

/**
 * Allocate and add list block to list
 */
static swl_unLiListBlock_t* s_addBlock(swl_unLiList_t* list) {
    swl_unLiListBlock_t* block = calloc(1, sizeof(swl_unLiListBlock_t) + list->blockSize * list->elementSize);
    swl_llist_append(&list->blocks, &block->it);
    return block;
}

/**
 * Remove a block from list and free it
 */
static void s_removeBlock(swl_unLiListBlock_t* block) {
    swl_llist_iterator_take(&block->it);
    free(block);
}

static swl_unLiListBlock_t* s_getBlockOfIndex(const swl_unLiList_t* list, uint32_t index, uint32_t* blockOffset) {
    uint32_t i = 0;
    swl_llist_iterator_t* it;
    swl_llist_for_each(it, &list->blocks) {
        swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        if(index < i + block->nrElements) {
            if(blockOffset != NULL) {
                *blockOffset = i;
            }
            return block;
        }
        i += block->nrElements;
    }
    return NULL;
}

/**
 * Initialize a unrolled linked list with given block size and element size.
 * @param list
 *  the list to initialize
 * @param blockSize
 *  the size of the blocks. This means the number of elements stored in a single block
 *  Is required to be greater than 1, and smaller or equal to 32.
 * @param elemSize
 *  the size of an element. Basically the size of the type that should be stored in the block.
 *  Is required to be greater than 0
 * @return SWL_RC_OK if created, SWL_RC_INVALID_PARAM if invalid parameters provided.
 */
swl_rc_ne swl_unLiList_initExt(swl_unLiList_t* list, uint32_t blockSize, uint32_t elemSize) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(blockSize > 1 && blockSize <= 32, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(elemSize > 0, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_llist_initialize(&list->blocks);
    SAH_TRACEZ_INFO(ME, "Init %p %u %u", list, blockSize, elemSize);
    list->blockSize = blockSize;
    list->elementSize = elemSize;
    list->keepLastBlock = false;
    return SWL_RC_OK;
}

bool swl_unLiList_isInitialized(const swl_unLiList_t* list) {
    if(list->blockSize == 0) {
        return false;
    }
    if(list->elementSize == 0) {
        return false;
    }
    return true;
}

/**
 * set whether the unliList needs to keep the last block allocated when list is empty.
 * This prevents alloc'ing and deallocing when few entries are present.
 * Note that setting this to true on an empty list will not allocate a block. Setting
 * this to false on an empty list with last block still allocated will deallocate it;
 * @param list
 *  the list to configure
 * @param keepsLastBlock
 *  whether or not to keep the last block.
 * @return SWL_RC_OK if created, SWL_RC_INVALID_PARAM if invalid parameters provided.
 */
swl_rc_ne swl_unLiList_setKeepsLastBlock(swl_unLiList_t* list, bool keepsLastBlock) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    list->keepLastBlock = keepsLastBlock;
    if(!keepsLastBlock && (swl_llist_size(&list->blocks) == 1)) {
        swl_llist_iterator_t* it = swl_llist_first(&list->blocks);
        swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        if(block->nrElements == 0) {
            s_removeBlock(block);
        }
    }
    return SWL_RC_OK;
}

/**
 * Initialize a list with given element size, using default block size
 * @param list
 *  the list to initialize
 * @param elemSize
 *  the size of an element. Basically the size of the type that should be stored in the block.
 */
swl_rc_ne swl_unLiList_init(swl_unLiList_t* list, uint32_t elemSize) {
    return swl_unLiList_initExt(list, SWL_UNLILIST_DEFAULT_BLOCK_SIZE, elemSize);
}

/**
 * Destroy a given list. Shall free all blocks allocated by this list and invalidate everything
 * pointing to this list.
 * @param list
 *  the list to destroy
 */
void swl_unLiList_destroy(swl_unLiList_t* list) {
    ASSERTI_NOT_NULL(list, , ME, "NULL");
    SAH_TRACEZ_INFO(ME, "Destroy %p", list);
    swl_llist_iterator_t* it = swl_llist_first(&list->blocks);
    while(it != NULL) {
        swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        s_removeBlock(block);
        it = swl_llist_first(&list->blocks);
    }
}

/**
 * Returns number of elements in list
 */
uint32_t swl_unLiList_size(const swl_unLiList_t* list) {
    swl_llist_iterator_t* it = NULL;
    uint32_t size = 0;
    swl_llist_for_each(it, &list->blocks) {
        swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        size += block->nrElements;
    }
    return size;
}

/**
 * Returns whether the `list` contains at least one occurrence of `data` (by pointer).
 */
bool swl_unLiList_contains(const swl_unLiList_t* list, const void* data) {
    ASSERT_NOT_NULL(list, false, ME, "NULL");
    ASSERT_NOT_NULL(data, false, ME, "NULL");
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, list) {
        if(memcmp(data, it.data, list->elementSize) == 0) {
            return true;
        }
    }
    return false;
}

/**
 * Debug function
 * Returns number of blocks currently in this list
 */
uint32_t swl_unLiList_getNrBlocks(const swl_unLiList_t* list) {
    return swl_llist_size(&list->blocks);
}

/**
 * get the data pointer of a given location. Will return NULL if invalid block index.
 */
static void* s_getBlockData(const swl_unLiList_t* list, swl_unLiListBlock_t* block, uint32_t blockIndex) {
    if((block == NULL) || (blockIndex > list->blockSize)) {
        return NULL;
    }
    return &block->data + blockIndex * list->elementSize;
}

/**
 * Returns a pointer to the data at a given index.
 * @param list
 *  the list containing the data
 * @param index
 *  the index of the requested data
 * @returns a pointer to the data at the given index. Will return 0 if index does not exist, or invalid list.
 *  Any index between 0 and swl_list_size() is valid.
 */
void* swl_unLiList_get(const swl_unLiList_t* list, int32_t inIndex) {
    ASSERT_NOT_NULL(list, NULL, ME, "NULL");

    uint32_t size = swl_unLiList_size(list);
    uint32_t index = 0;
    if(inIndex < 0) {
        index = inIndex + size;
    } else {
        index = inIndex;
    }
    if(index >= size) {
        SAH_TRACEZ_INFO(ME, "invalid index %d %u / %u", inIndex, index, size);
        return NULL;
    }

    uint32_t i = 0;
    swl_llist_iterator_t* it;
    swl_llist_for_each(it, &list->blocks) {
        swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        if(index < i + block->nrElements) {
            uint32_t blockIndex = index - i;
            return s_getBlockData(list, block, blockIndex);
        } else {
            i = i + block->nrElements;
        }
    }
    return NULL;
}

/**
 * Remote the data at a given block index, and move elements behind it to front.
 */
static void s_removeDataAtBlockIndex(swl_unLiList_t* list, swl_unLiListBlock_t* block, uint32_t blockIndex) {
    ASSERT_NOT_NULL(block, , ME, "NULL");
    ASSERT_TRUE(blockIndex < block->nrElements, , ME, "invalid el %u / %u", blockIndex, block->nrElements);
    uint32_t i = 0;
    for(i = blockIndex + 1; i < list->blockSize; i++) {
        memcpy(s_getBlockData(list, block, i - 1), s_getBlockData(list, block, i), list->elementSize);
    }
    memset(s_getBlockData(list, block, block->nrElements - 1), 0, list->elementSize);
    block->nrElements -= 1;
}

/**
 * Add data to the back of list. This shall copy the data in the list doing
 * a memcpy from the data, with the element size
 * @param list
 *  the list to which to add data
 * @param data
 *  the data to add
 * @return the index at which the data is added. Returns -1 in case of error.
 */
ssize_t swl_unLiList_add(swl_unLiList_t* list, const void* data) {
    ASSERT_NOT_NULL(list, -1, ME, "NULL");
    ASSERT_NOT_NULL(data, -1, ME, "NULL");

    swl_llist_iterator_t* it = swl_llist_last(&list->blocks);
    swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
    if((block != NULL) && (block->nrElements < list->blockSize)) {
        uint32_t blockIndex = block->nrElements;
        memcpy(s_getBlockData(list, block, blockIndex), data, list->elementSize);
        block->nrElements++;
        return swl_unLiList_size(list) - 1;
    }
    // Nothing free, add block
    block = s_addBlock(list);
    ASSERTS_NOT_NULL(block, -1, ME, "INVALID");
    memcpy(s_getBlockData(list, block, 0), data, list->elementSize);
    block->nrElements++;
    return swl_unLiList_size(list) - 1;
}

/**
 * Retrieve a pointer to an empty data element, and mark this element as taken.
 * This data shall be set to all zeros.
 * @param list
 *  the list to which a new element shall be added
 * @param outindex
 *  output parameter. Shall contain the index of the created entry after execution, or -1 in case of failure
 * @return
 *  pointer to a newly created data element.
 */
void* swl_unLiList_allocElement(swl_unLiList_t* list) {
    char buffer[list->elementSize];
    memset(&buffer, 0, list->elementSize);
    swl_unLiList_add(list, buffer);
    return swl_unLiList_get(list, -1);
}


/**
 * Retrieve a pointer to an empty data element, and mark this element as taken.
 * This data shall be set to all zeros.
 * @param list
 *  the list to which a new element shall be added
 * @param outindex
 *  output parameter. Shall contain the index of the created entry after execution, or -1 in case of failure
 * @return
 *  pointer to a newly created data element.
 */
swl_rc_ne swl_unLiList_set(swl_unLiList_t* list, uint32_t index, void* data) {
    uint32_t blockOffset = 0;
    swl_unLiListBlock_t* block = s_getBlockOfIndex(list, index, &blockOffset);
    ASSERT_NOT_NULL(block, SWL_RC_INVALID_PARAM, ME, "NULL");
    memcpy(s_getBlockData(list, block, index - blockOffset), data, list->elementSize);
    return SWL_RC_OK;
}

/**
 * Insert data in the block at the given blockIndex
 */
static swl_rc_ne s_insertDataAt(swl_unLiList_t* list, swl_unLiListBlock_t* block, uint32_t blockIndex, void* data) {
    if(block->nrElements == list->blockSize) {
        swl_unLiListBlock_t* newBlock = calloc(1, sizeof(swl_unLiListBlock_t) + list->blockSize * list->elementSize);
        ASSERTS_NOT_NULL(newBlock, SWL_RC_NOT_AVAILABLE, ME, "NULL");
        bool copyToNew = false;
        if(blockIndex == list->blockSize) {
            swl_llist_insertAfter(&list->blocks, &block->it, &newBlock->it);
            copyToNew = true;
        } else if(blockIndex == 0) {
            swl_llist_insertBefore(&list->blocks, &block->it, &newBlock->it);
            copyToNew = true;
        }
        if(copyToNew) {
            memcpy(s_getBlockData(list, newBlock, 0), data, list->elementSize);
            newBlock->nrElements++;
            return SWL_RC_OK;
        }
        // insert in middle => put block after and split data in half
        swl_llist_insertAfter(&list->blocks, &block->it, &newBlock->it);
        memmove(s_getBlockData(list, newBlock, 0), s_getBlockData(list, block, list->blockSize - 1), list->elementSize);
        newBlock->nrElements++;
        block->nrElements--;
    }

    uint32_t elToMove = block->nrElements - blockIndex;
    memmove(s_getBlockData(list, block, blockIndex + 1), s_getBlockData(list, block, blockIndex), elToMove * list->elementSize);
    memcpy(s_getBlockData(list, block, blockIndex), data, list->elementSize);
    block->nrElements++;
    return SWL_RC_OK;
}

/**
 * Insert element at given index
 * @param list
 *  the list to which to add the element
 * @param inIndex
 *  the index at which to add. This index can be in range [- size - 1, size]
 *  Negative indexes shall be counted from back of list. Note that inserting at -1 will add the
 *  element to the back. Inserting at -size -1, will insert at the start.
 * @param data
 *  the data to add.
 */
swl_rc_ne swl_unLiList_insert(swl_unLiList_t* list, int32_t inIndex, void* data) {
    uint32_t size = swl_unLiList_size(list);
    uint32_t index = 0;
    if(inIndex < 0) {
        index = inIndex + size + 1;
    } else {
        index = inIndex;
    }
    if(index > size) {
        SAH_TRACEZ_ERROR(ME, "invalid index %d / %u", inIndex, size);
        return SWL_RC_INVALID_PARAM;
    }
    if(index == size) {
        ssize_t index = swl_unLiList_add(list, data);
        return (index >= 0 ? SWL_RC_OK : SWL_RC_ERROR);
    }

    uint32_t blockOffset = 0;
    swl_unLiListBlock_t* block = s_getBlockOfIndex(list, index, &blockOffset);

    if(block == NULL) {
        SAH_TRACEZ_ERROR(ME, "block not found %i %u / %u", inIndex, index, size);
        return SWL_RC_NOT_AVAILABLE;
    }

    return s_insertDataAt(list, block, index - blockOffset, data);

}

/**
 * get the block from a given iterator, or NULL if iterator is NULL
 */
static swl_unLiListBlock_t* s_getBlockFromIt(swl_llist_iterator_t* it) {
    if(it == NULL) {
        return NULL;
    }
    return swl_llist_iterator_data(it, swl_unLiListBlock_t, it);
}

/**
 * transfer as much data as possible from a given block to another block.
 */
static void s_transferData(swl_unLiList_t* list, swl_unLiListBlock_t* toBlock, swl_unLiListBlock_t* fromBlock) {
    uint32_t emptySize = list->blockSize - toBlock->nrElements;
    uint32_t transferSize = SWL_MIN(fromBlock->nrElements, emptySize);

    memcpy(s_getBlockData(list, toBlock, toBlock->nrElements), s_getBlockData(list, fromBlock, 0), transferSize * list->elementSize);
    toBlock->nrElements += transferSize;
    fromBlock->nrElements -= transferSize;

    uint32_t i = 0;
    for(i = transferSize; i < list->blockSize; i++) {
        memcpy(s_getBlockData(list, fromBlock, i - transferSize), s_getBlockData(list, fromBlock, i), list->elementSize);
    }
    memset(s_getBlockData(list, fromBlock, list->blockSize - transferSize), 0, list->elementSize * transferSize);
}

/**
 * Try to compress a given block, after removing an element from the block
 * This shall try to spread the data of the current, previous and next block, in fewer blocks.
 * It only be done if the sum of elements of all 3 blocks, is smaller than the sum of blocksizes minus
 * the list compression threshold.
 */
static void s_tryCompress(swl_unLiList_t* list, swl_unLiListBlock_t* block) {
    if(block->nrElements == 0) {
        if(!list->keepLastBlock || (swl_llist_at(&list->blocks, 1) != NULL)) {
            SAH_TRACEZ_INFO(ME, "Free block");
            s_removeBlock(block);
            return;
        }

    }

    swl_unLiListBlock_t* prevBlock = s_getBlockFromIt(swl_llist_iterator_prev(&block->it));
    swl_unLiListBlock_t* nextBlock = s_getBlockFromIt(swl_llist_iterator_next(&block->it));

    uint32_t freeBlocksPrev = prevBlock != NULL ? list->blockSize - prevBlock->nrElements : 0;
    uint32_t freeBlocksNext = nextBlock != NULL ? list->blockSize - nextBlock->nrElements : 0;

    if(freeBlocksPrev + freeBlocksNext < block->nrElements + SWL_UNLILIST_COMPRESS_THRESH) {
        // not enough free room to compress. Return
        SAH_TRACEZ_INFO(ME, "No compress %d %d %d %u %u ",
                        (prevBlock != NULL ? (int32_t) prevBlock->nrElements : -1),
                        block->nrElements,
                        (nextBlock != NULL ? (int32_t) nextBlock->nrElements : -1),
                        freeBlocksPrev, freeBlocksNext);
        return;
    }

    SAH_TRACEZ_INFO(ME, "Try compress %d %d %d",
                    prevBlock != NULL ? (int32_t) prevBlock->nrElements : -1,
                    block->nrElements,
                    nextBlock != NULL ? (int32_t) nextBlock->nrElements : -1);

    if(prevBlock != NULL) {
        s_transferData(list, prevBlock, block);
    }

    if(nextBlock == NULL) {
        if(block->nrElements > 0) {
            SAH_TRACEZ_ERROR(ME, "Compression failure");
            return;
        }

        swl_llist_iterator_take(&block->it);
        free(block);
        return;
    }

    s_transferData(list, block, nextBlock);

    if(nextBlock->nrElements > 0) {
        SAH_TRACEZ_ERROR(ME, "Compression failure");
        return;
    } else {
        swl_llist_iterator_take(&nextBlock->it);
        free(nextBlock);
        return;
    }

}

/**
 * remove data at a given index, and try compressing the list.
 * Note that this is the global index.
 */
static void s_removeDataAtIndex(swl_unLiList_t* list, swl_unLiListBlock_t* block, uint32_t blockOffset, uint32_t globalIndex) {
    uint32_t blockIndex = globalIndex - blockOffset;
    s_removeDataAtBlockIndex(list, block, blockIndex);
    s_tryCompress(list, block);
}

/**
 * Remove data at a given index
 */
swl_rc_ne swl_unLiList_remove(swl_unLiList_t* list, int32_t inIndex) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    uint32_t size = swl_unLiList_size(list);
    uint32_t index;
    if(inIndex < 0) {
        index = inIndex + size;
    } else {
        index = inIndex;
    }

    ASSERT_TRUE(index < size, SWL_RC_INVALID_PARAM, ME, "invalid index %d / %u", inIndex, size);


    uint32_t blockOffset = 0;
    swl_unLiListBlock_t* block = s_getBlockOfIndex(list, index, &blockOffset);
    s_removeDataAtIndex(list, block, blockOffset, index);
    return SWL_RC_OK;
}

/**
 * Remove data by providing a pointer to the spot in the list
 */
int32_t swl_unLiList_removeByPointer(swl_unLiList_t* list, const void* data) {
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, list) {
        if(it.data == data) {
            s_removeDataAtIndex(list, it.block, it.blockOffset, it.index);
            return it.index;
        }
    }
    return -1;
}

/**
 * Remove data by providing the same data. Note that providing the spot
 * in the list will work, but be less efficient than swl_list_removeByPointer
 */
int32_t swl_unLiList_removeByData(swl_unLiList_t* list, const void* data) {
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, list) {
        if(memcmp(data, it.data, list->elementSize) == 0) {
            swl_unLiListBlock_t* block = it.block;
            s_removeDataAtIndex(list, block, it.blockOffset, it.index);
            return it.index;
        }
    }
    return -1;
}

/**
 * Initialize the item to the first element of list.
 */
void swl_unLiList_firstIt(swl_unLiListIt_t* item, const swl_unLiList_t* list) {
    memset(item, 0, sizeof(swl_unLiListIt_t));
    swl_llist_iterator_t* it = swl_llist_first(&list->blocks);
    if(it == NULL) {
        item->valid = false;
        return;
    }
    swl_unLiListBlock_t* block = swl_llist_item_data(it, swl_unLiListBlock_t, it);

    item->block = block;
    if(block->nrElements == 0) {
        item->valid = false;
        return;
    }

    item->coll = (swl_unLiList_t*) list;
    item->blockOffset = 0;
    item->index = 0;
    item->data = s_getBlockData(list, block, item->index);
    item->valid = (item->data != NULL);
}

swl_unLiListIt_t swl_unLiList_getFirstIt(const swl_unLiList_t* list) {
    swl_unLiListIt_t data;
    memset(&data, 0, sizeof(swl_unLiListIt_t));
    swl_unLiList_firstIt(&data, list);
    return data;
}

/**
 * Set item to the next element of the given list.
 */
void swl_unLiList_nextIt(swl_unLiListIt_t* item) {
    ASSERT_NOT_NULL(item, , ME, "NULL");
    swl_unLiListBlock_t* block = item->block;
    if(block == NULL) {
        item->valid = false;
        return;
    }
    item->index++;
    if(item->index >= item->blockOffset + block->nrElements) {
        swl_llist_iterator_t* it = swl_llist_iterator_next(&block->it);
        if(it == NULL) {
            item->data = NULL;
            item->valid = false;
            return;
        }
        item->blockOffset += block->nrElements;
        block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        item->block = block;
    }
    swl_unLiList_t* list = item->coll;
    item->data = s_getBlockData(list, block, item->index - item->blockOffset);
    if(item->data == NULL) {
        item->valid = false;
    } else {
        item->valid = true;
    }
}

/**
 * delete the current entry, and put the iterator in a state where nextIt would call the
 * value after the deleted value. Note that calling delit twice will only delete the
 * original entry.
 */
void swl_unLiList_delIt(swl_unLiListIt_t* item) {
    ASSERT_NOT_NULL(item, , ME, "NULL");
    ASSERT_TRUE(item->valid, , ME, "INVALID");

    swl_unLiListBlock_t* block = item->block;
    swl_unLiList_t* list = item->coll;
    if(block->nrElements == 1) {
        swl_llist_iterator_t* it = swl_llist_iterator_next(&block->it);
        block = swl_llist_item_data(it, swl_unLiListBlock_t, it);
        item->block = block;
    }
    swl_unLiList_removeByPointer(list, item->data);
    item->index--;
    if(swl_unLiList_size(list) == 0) {
        item->block = NULL;
    }
    item->data = NULL;
    item->valid = false;
}
