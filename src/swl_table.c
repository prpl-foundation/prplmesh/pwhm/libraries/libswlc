/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_table.h"
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "swl/types/swl_tupleTypeArray.h"

#define ME "swlTbl"


/**
 * This module implements a table, i.e. a list of tuples (or rows). Basically a buffer of tuples,
 * which belong to a given tupleType.
 *
 * With regards to naming.
 * The table consists of multiple tuples.
 * Each tuple consists of multiple elements.
 * An element is a single piece of data, which can either be of primitive data type or pointer data type.
 */

static swl_type_t* s_getType(const swl_table_t* table) {
    return &table->tupleType->type;
}


//Local function for fast access
static size_t s_getTupSize(const swl_table_t* table) {
    return table->tupleType->type.size;
}

//Local function for fast access, only unsigned index.
static swl_tuple_t* s_getTuple(const swl_table_t* table, size_t index) {
    size_t tupSize = s_getTupSize(table);
    ASSERT_TRUE(index < table->nrTuples, NULL, ME, "SIZE");
    return table->tuples + tupSize * index;
}

/**
 * Checks whether a table is properly initialized and contains all relevant values.
 */
bool swl_table_checkValid(const swl_table_t* table, bool dynamicOnly) {
    ASSERT_NOT_NULL(table, false, ME, "NULL");
    ASSERTI_FALSE(table->nrTuples == 0, false, ME, "SIZE");
    ASSERTI_NOT_NULL(table->tuples, false, ME, "NULL");
    if(dynamicOnly) {
        ASSERT_TRUE(table->isDynamic, false, ME, "STATIC TABLE");
    }

    return swl_tupleType_check(table->tupleType);
}

/**
 * Returns the current number of tuples stored in this table
 *
 * @param table: the table for which the size should be returned
 *
 * @return: the number of tuples in this table, for which getter functions will provide a positive result
 * If table is invalid, zero is returned.
 */
size_t swl_table_getSize(const swl_table_t* table) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), 0, ME, "INVALID");
    return table->curNrTuples;
}

/**
 * Returns the tuple of the table at the given index.
 *
 * @param table: the table from which to get the tuple
 * @param tupleIndex: the index of the tuple to get. Can be negative according to SWL_ARRAY_INDEX
 *
 * @return the tuple at SWL_ARRAY_INDEX(tupleIndex, table->curNrTuples), NULL otherwise
 */
swl_tuple_t* swl_table_getTuple(swl_table_t* table, ssize_t tupleIndex) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getTuple(table->tupleType, table->tuples, table->curNrTuples, tupleIndex);
}

/**
 * Returns a pointer to the type element in the table. Note that referenced values are not dereferenced.
 *
 * @param table: the table from which to get the tuple
 * @param tupleIndex: the index of the tuple to get. Can be negative according to SWL_ARRAY_INDEX
 * @param typeIndex: the index of the type to get.
 *
 * @return the type element at SWL_ARRAY_INDEX(tupleIndex, table->curNrTuples), NULL otherwise
 */
swl_typeEl_t* swl_table_getReference(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getElementReference(table->tupleType, table->tuples, table->curNrTuples, tupleIndex, typeIndex);
}

/**
 * Returns a pointer to the type data in the table. Note that referenced values are not dereferenced.
 *
 * @param table: the table from which to get the tuple
 * @param tupleIndex: the index of the tuple to get. Can be negative according to SWL_ARRAY_INDEX
 * @param typeIndex: the index of the type to get.
 *
 * @return the type data at SWL_ARRAY_INDEX(tupleIndex, table->curNrTuples), NULL otherwise
 */
swl_typeData_t* swl_table_getValue(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getElementValue(table->tupleType, table->tuples, table->curNrTuples, tupleIndex, typeIndex);
}

/**
 * Finds the index of a given value
 * @param list
 *  the arrayList in which to look
 * @param val
 *  the value to look for
 * @return
 *  the first index for which applies that swl_type_equals(type, val, swl_arrayBuf_getValue(buf, index))
 *  If no such index exists, -1 is returned.
 */
ssize_t swl_table_find(const swl_table_t* table, const swl_typeData_t* val) {
    ASSERT_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_type_arrayFind(s_getType(table), table->tuples, table->curNrTuples, val);
}

/**
 * Return the first tuple index at or after offset, for which the element at type index srcIndex equals the provided value.
 *
 * returns SWL_RC_INVALID_PARAM if no such parameter exists.
 */
ssize_t swl_table_findMatchingValue(swl_table_t* table, size_t offset, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERT_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_TRUE(srcIndex < table->tupleType->nrTypes, SWL_RC_INVALID_PARAM, ME, "SIZE %zu %zu", srcIndex, table->tupleType->nrTypes);

    return swl_tta_findMatchingTupleIndex(table->tupleType, table->tuples, table->curNrTuples, offset, srcIndex, srcValue);
}


/**
 * Return the first tuple index at or after offset, for which the tuple will be equal by mask with the provided tuple.
 *
 * returns SWL_RC_INVALID_PARAM if no such parameter exists.
 */
ssize_t swl_table_findMatchingTupleByMask(swl_table_t* table,
                                          size_t offset, const swl_tuple_t* srcValue, size_t mask) {
    ASSERT_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_findMatchingTupleIndexByMask(table->tupleType, table->tuples, table->curNrTuples, offset, srcValue, mask);
}


/**
 * Returns the first tuple in table, where the elements of the tuple match the given srcValue over a given mask
 *
 * @param table: the table which to check for a match
 * @param srcValue: the source value tuple
 * @param mask: the mask over which to check
 *
 * @return the first swl_tuple_t, which is equal over mask with srcValue
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_table_getMatchingTupleByTuple(swl_table_t* table, const swl_tuple_t* srcValue, size_t mask, size_t offset) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingTupleByMask(table->tupleType,
                                          table->tuples, table->curNrTuples, offset, srcValue, mask);
}

/**
 * Returns the type data of the first tuple in table, for which the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param mask: the mask over which to check
 *
 * @return the type data of type tgtTypeIndex of the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists.
 */
swl_typeData_t* swl_table_getMatchingValueByTuple(swl_table_t* table, size_t tgtTypeIndex, const swl_tuple_t* srcValue, size_t mask, size_t offset) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingElementByMask(table->tupleType, table->tuples, table->curNrTuples, offset,
                                            tgtTypeIndex, srcValue, mask);
}

/**
 * Returns the first tuple in table, where the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param srcValue: the data to check
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_table_getMatchingTuple(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingTuple(table->tupleType, table->tuples, table->curNrTuples, 0,
                                    srcIndex, srcValue);
}

/**
 * Returns the first encountered tuple in the table in the direction of search, where srcIndex element of the tuple equals srcValue.
 * {startElem, endElem, stepSize} define the (sub)set and direction of the search. if needed, wraps around the first or last element, at most once.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param srcValue: the data to check
 * @param startElem: index of the first tuple in the table to check; negative value implies 'counting from the end'
 * @param endElem: index of the last tuple in the table to check; negative value implies 'counting from the end'
 * @param stepSize: signed step size for the iteration; iteration direction is encoded in the sign
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_table_getMatchingTupleInRange(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue, ssize_t startElem,
                                               ssize_t endElem, ssize_t stepSize) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingTupleInRange(table->tupleType, table->tuples, table->curNrTuples,
                                           srcIndex, srcValue, startElem, endElem, stepSize);
}

/**
 * Returns the first tuple in table, where the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param the data to check
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_table_getMatchingTupleOffset(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue, size_t offset) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingTuple(table->tupleType, table->tuples, table->curNrTuples, offset,
                                    srcIndex, srcValue);
}

/**
 * Returns the type data of the first tuple in table, for which the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param the data to check
 *
 * @return the type data of type tgtTypeIndex of the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists.
 */
swl_typeData_t* swl_table_getMatchingValue(swl_table_t* table, size_t tgtTypeIndex, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingElement(table->tupleType, table->tuples, table->curNrTuples, 0,
                                      tgtTypeIndex, srcIndex, srcValue);
}

/**
 * Returns the type data of the first tuple after index offset in table, for which the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param the data to check
 *
 * @return the type data of type tgtTypeIndex of the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists.
 */
swl_typeData_t* swl_table_getMatchingValueOffset(swl_table_t* table, size_t tgtTypeIndex, size_t srcIndex, const swl_typeData_t* srcValue, size_t offset) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), NULL, ME, "INVALID");
    return swl_tta_getMatchingElement(table->tupleType, table->tuples, table->curNrTuples, offset,
                                      tgtTypeIndex, srcIndex, srcValue);
}


/**
 * Initialize the table tuples. Requires a non initialized table.
 * All table entries will be 0
 *
 * @param table: the talbe to initialize
 * @param tupleType: the tuple type of this table, must not be NULL.
 * @param size: the number of tuples in this table, must not be zero.
 */
void swl_table_init(swl_table_t* table, swl_tupleType_t* tupleType, size_t size) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_NOT_NULL(tupleType, , ME, "NULL");
    ASSERT_NOT_EQUALS(size, 0, , ME, "ZERO");
    ASSERT_NULL(table->tuples, , ME, "INIT DONE");
    ASSERT_NULL(table->tupleType, , ME, "INIT DONE");

    table->tupleType = tupleType;
    swl_tupleType_check(table->tupleType);

    table->tuples = calloc(size, table->tupleType->type.size);
    table->curNrTuples = size;
    table->nrTuples = size;
    table->isDynamic = true;
}

swl_rc_ne swl_table_initMax(swl_table_t* table, swl_tupleType_t* tupleType, size_t maxSize) {
    ASSERT_NOT_NULL(table, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tupleType, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NULL(table->tuples, SWL_RC_INVALID_PARAM, ME, "INIT DONE");
    ASSERT_NULL(table->tupleType, SWL_RC_INVALID_PARAM, ME, "INIT DONE");

    size_t targetSize = maxSize != 0 ? maxSize : SWL_TABLE_DEFAULT_MIN_SIZE;

    table->tupleType = tupleType;
    swl_tupleType_check(table->tupleType);
    table->tuples = calloc(targetSize, table->tupleType->type.size);
    table->curNrTuples = 0;
    table->nrTuples = targetSize;
    table->isDynamic = true;
    return SWL_RC_OK;
}

/**
 * Resize the table, allowing more or less rows to be stored in the table.
 * If copy is set, the resize will attempt to fill the new data with the old data.
 * It will copy all rows i, with i between 0 and minimum of old and new table size.
 *
 * Requires dynamic table. Elements that do not fit anymore in the table will be cleaned up.
 *
 * @param table: the table to resize, must be dynamic
 * @param newSize: the new size of the table
 * @param copy: whether or not to copy the old data from table to new table. Data will always be
 * filled up from 0 onwards, to however much fits.
 */
void swl_table_resize(swl_table_t* table, size_t newSize, bool copy) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");

    swl_tuple_t* data = calloc(newSize, table->tupleType->type.size);

    size_t newNrEl = 0;

    if(copy) {
        newNrEl = SWL_MIN(table->curNrTuples, newSize);
        memcpy(data, table->tuples, newNrEl * table->tupleType->type.size);
    }

    for(size_t i = 0; i < table->tupleType->nrTypes; i++) {
        swl_type_t* targetType = table->tupleType->types[i];
        size_t offSet = swl_tupleType_getOffset(table->tupleType, i);
        swl_typeEl_t* element = ((swl_typeEl_t*) table->tuples) + offSet + newNrEl * table->tupleType->type.size;

        for(size_t j = newNrEl; j < table->curNrTuples; j++) {
            swl_type_cleanup(targetType, element);
            element += table->tupleType->type.size;
        }
    }

    free(table->tuples);
    table->curNrTuples = newNrEl;
    table->tuples = data;
    table->nrTuples = newSize;
}

/**
 * Copy the given table to a new table, doing a deep copy of all data
 * @param srcTable : the table to copy
 *
 * @return a deep copy of srcTable if successfull, NULL otherwise.
 */
swl_table_t* swl_table_copy(swl_table_t* srcTable) {
    ASSERTI_TRUE(swl_table_checkValid(srcTable, true), NULL, ME, "INVALID");
    swl_table_t* newTable = calloc(1, sizeof(swl_table_t));
    ASSERT_NOT_NULL(newTable, NULL, ME, "NULL");

    swl_table_initMax(newTable, srcTable->tupleType, srcTable->nrTuples);

    newTable->curNrTuples = srcTable->curNrTuples;

    swl_rc_ne retCode = swl_type_arrayCopy(&srcTable->tupleType->type, newTable->tuples, srcTable->tuples, srcTable->curNrTuples);
    if(retCode != SWL_RC_OK) {
        SAH_TRACEZ_ERROR(ME, "Fail to copy table : %u : %s", retCode, swl_rc_toString(retCode));
        free(newTable);
        return NULL;
    }
    return newTable;
}

/**
 * Check whether the list should grow before adding an element.
 * It is assumed here that an element shall be added.
 * This should be called before element is added, and before size is updated.
 */
static void s_checkGrow(swl_table_t* table) {
    if(table->curNrTuples < table->nrTuples) {
        return;
    }
    size_t newSize = table->nrTuples * (1000 + SWL_TABLE_GROWTH_FACTOR) / 1000;
    size_t tupSize = s_getTupSize(table);
    void* newData = realloc(table->tuples, newSize * tupSize);
    if(newData == NULL) {
        return;
    }
    table->nrTuples = newSize;
    table->tuples = newData;
    memset(table->tuples + table->curNrTuples * tupSize, 0,
           (table->nrTuples - table->curNrTuples) * tupSize);
}

/**
 * Check whether the list should shrink.
 * This should be called after the actual element has been cleared, and the
 * size updated.
 */
static void s_checkShrink(swl_table_t* table) {
    if(table->nrTuples <= SWL_TABLE_DEFAULT_MIN_SIZE) {
        return;
    }
    size_t shrinkSize = table->nrTuples * SWL_TABLE_SHRINK_SIZE / 1000;
    if(table->curNrTuples >= shrinkSize) {
        return;
    }
    size_t tupSize = s_getTupSize(table);

    size_t postSize = table->nrTuples * SWL_TABLE_SHRINK_FACTOR / 1000;
    postSize = SWL_MAX(postSize, SWL_TABLE_DEFAULT_MIN_SIZE);
    void* newData = realloc(table->tuples, postSize * tupSize);
    if(newData == NULL) {
        return;
    }
    table->nrTuples = postSize;
    table->tuples = newData;
}

/**
 * Add a new element to the table
 * @param table
 *  the table to which to add the element
 * @param data
 *  the data which to add
 * @return the index at which the data was added, or SWL_RC_INVALID_PARAM if some problem occurred, or NULL data was tried to be added.
 */
ssize_t swl_table_add(swl_table_t* table, swl_tuple_t* data) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    s_checkGrow(table);

    swl_typeEl_t* location = s_getTuple(table, table->curNrTuples);

    swl_type_copyTo(&table->tupleType->type, location, data);
    table->curNrTuples++;
    return table->curNrTuples - 1;
}

/**
 * Allocate an empty element
 * @param table
 *  the list to which to add the element
 * @return
 *  the pointer to an empty tuple, NULL if failed
 */
swl_tuple_t* swl_table_alloc(swl_table_t* table) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), NULL, ME, "INVALID");
    s_checkGrow(table);
    ASSERT_FALSE(table->curNrTuples >= table->nrTuples, NULL, ME, "SIZE");
    table->curNrTuples++;
    return s_getTuple(table, table->curNrTuples - 1);
}

/**
 * Insert the data at the given index.
 * The remaining elements will be shifted to the right.
 * To append at the end, use index -1
 * @param list
 *  the list to which the data has to be appended
 * @param data
 *  the data to be appended
 * @param index
 *  the index at which the data must be inserted. Valid indexes go from [-size - 1, size]
 * @return
 *  SWL_RC_OK if the data was properly inserted
 *  swl_rc_ne error code otherwise.
 */
swl_rc_ne swl_table_insert(swl_table_t* table, swl_tuple_t* data, ssize_t index) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    s_checkGrow(table);
    ASSERT_FALSE(table->curNrTuples >= table->nrTuples, SWL_RC_INVALID_PARAM, ME, "SIZE");

    size_t targetIndex = SWL_ARRAY_INDEX(index, table->curNrTuples + 1);

    swl_typeEl_t* location = (swl_typeEl_t*) table->tuples + targetIndex * s_getTupSize(table);
    swl_type_arrayShift(s_getType(table), location, table->nrTuples - targetIndex, 1);
    swl_type_copyTo(s_getType(table), location, data);
    table->curNrTuples += 1;
    return SWL_RC_OK;
}

/**
 * Delete the entry at the given index
 * @param list
 *  the list from which to delete the given entry
 * @param index
 *  the index at which to delete the given index
 * @return
 *  SWL_RC_OK if delete succeeded, error if not.
 *  Remaining elements after index shall be shifted one to the left
 */
swl_rc_ne swl_table_delete(swl_table_t* table, ssize_t index) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), SWL_RC_INVALID_PARAM, ME, "INVALID");
    ASSERT_FALSE(table->curNrTuples == 0, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_rc_ne ret = swl_type_arrayDelete(s_getType(table), table->tuples, table->curNrTuples, index);
    if(ret == SWL_RC_OK) {
        table->curNrTuples--;
        s_checkShrink(table);
    }
    return ret;
}


/**
 * Deallocate the data on a given row, and set pointer to null
 * Only if type is a reference type
 *
 * @param table: the table for which to clean up the column, must be dynamic
 * @param typeIndex: the index of the type in the table tuple type, which needs to be cleaned.
 */
void swl_table_cleanupColumn(swl_table_t* table, size_t typeIndex) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");
    return swl_tta_cleanupColumn(table->tupleType, table->tuples, table->curNrTuples,
                                 typeIndex);
}

/**
 * Clean up the elements of the given types of the tuple, as indicated by the mask (i.e. cleaning up all columns of the table
 * for which the typeMask is set).
 */
void swl_table_cleanupByMask(swl_table_t* table, size_t typeMask) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");
    swl_tta_cleanupByMask(table->tupleType, table->tuples, table->curNrTuples, typeMask);
}


/**
 * Deprecated, please use swl_table_cleanElements, or swl_table_cleanupByMask
 * Clean up the table, deallocating dynamically allocated types.
 * Either all dynamically allocated types will be cleaned if cleanAllTypes is set to true,
 * or one can just give a mask with indexes of types to clean. Note that if the mask
 * has a bit enabled of a field that is not dynamically allocated, nothing will happen.
 *
 * Also, the number of elements in the table does not change, so if cleanAllTypes is set to true
 * all elements remain marked as present, but all contain zero tuples.
 *
 * @param table: the table to be cleaned up, must be dynamic.
 * @param cleanAllTypes: whether to clean up all types
 * @param typeMask: if not all types must be cleaned, user can provide mask containing offsets of which types should be cleaned.
 *
 */
void swl_table_cleanup(swl_table_t* table, bool cleanAllTypes, size_t typeMask) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");

    if(cleanAllTypes) {
        swl_table_cleanElements(table);
    } else {
        swl_table_cleanupByMask(table, typeMask);
    }
}



/**
 * Destroy the table, cleaning up all types in it.
 *
 * @param table: the table to destroy. Must be dynamic.
 */
void swl_table_destroy(swl_table_t* table) {
    ASSERT_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");
    swl_table_cleanElements(table);

    free(table->tuples);
    table->tuples = NULL;
    table->curNrTuples = 0;
    table->tupleType = NULL;
}

/**
 * Clear all data of the table, setting all values to 0 again
 * Will not reduce nr tuples, so it will contain same amount of tuples, just all empty.
 * Use swl_table_reset to actually fully reset the table to 0 elements.
 *
 * @param table: the table to clear the data. Must be dynamic.
 */
void swl_table_cleanElements(swl_table_t* table) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), , ME, "INVALID");
    swl_tta_cleanup(table->tupleType, table->tuples, table->curNrTuples);
}

/**
 * Reset the table, setting all values to 0 again. It will also reset size counter to zero.
 *
 * @param table: the table to reset. Must be dynamic.
 */
void swl_table_clear(swl_table_t* table) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), , ME, "INVALID");
    swl_tta_cleanup(table->tupleType, table->tuples, table->curNrTuples);
    table->curNrTuples = 0;
}

/**
 * Set the value at a given table to the given targetData value. Note that the data will be copied
 * according to swl_type_copyTo
 *
 * @param table: the table of which to set a value. Must be dynamic.
 * @param tupleIndex: the index of the tuple of which the value must be set
 * @param typeIndex: the index of the type.
 * @param data: the data to be set. Must be of type matching the tupleTypes[typeIndex]
 */
swl_rc_ne swl_table_setValue(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_setElementValue(table->tupleType, table->tuples, table->curNrTuples, tupleIndex, typeIndex, data);
}

/**
 * Set a row of data in the table.
 * The old values will be cleaned, and the new values will be copied into the table.
 * A deep copy is used for reference types.
 *
 * @param table: the table of which to set the tuple. Must be dynamic.
 * @param tupleIndex: the index of the tuple which must be set
 * @param data: the data tuple to be set. Must be matching tupleType.
 */
swl_rc_ne swl_table_setTuple(swl_table_t* table, ssize_t tupleIndex, swl_tuple_t* data) {
    ASSERTI_TRUE(swl_table_checkValid(table, true), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_setTuple(table->tupleType, table->tuples, table->curNrTuples, tupleIndex, data);
}

/**
 * Perform a shallow copy of data entry to array
 *
 * @param array: the array in which to put the data
 * @param arraySize: the size of the array.
 * @param table: the table of which to set the tuple. Must be dynamic.
 * @param typeIndex: the index of the type to copy
 * @param offSet: the offset from which to start. Will cycle if around to 0 if more elements can be put. Will put max table.size entries.
 */
swl_rc_ne swl_table_columnToArrayOffset(swl_typeEl_t* array, size_t arraySize, swl_table_t* table, size_t typeIndex, size_t offSet) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_columnToArrayOffset(table->tupleType, array, arraySize, table->tuples, table->curNrTuples, typeIndex, offSet);
}

/**
 * Perform a shallow copy of data entry to array
 *
 * @param array: the array in which to put the data
 * @param arraySize: the size of the array.
 * @param table: the table of which to set the tuple. Must be dynamic.
 * @param typeIndex: the index of the type to copy
 */
swl_rc_ne swl_table_columnToArray(swl_typeEl_t* array, size_t arraySize, swl_table_t* table, size_t typeIndex) {
    return swl_table_columnToArrayOffset(array, arraySize, table, typeIndex, 0);
}

/**
 * Check whether two tables are equal.
 * They match if:
 * * tuple types are identical (i.e. must point to same field)
 * * nrTuples is identical
 * * each tuple is identical (so each value in table 1 matches each value in table 2 based on swl_type_matches)
 *
 * @param table1: the table to compare.
 * @param table2: the table to compare against.
 */
bool swl_table_equals(swl_table_t* table1, swl_table_t* table2) {
    ASSERTS_EQUALS(table1->tupleType, table2->tupleType, false, ME, "TYPE MISMATCH");
    return swl_tta_equals(table1->tupleType, table1->tuples, table1->nrTuples, table2->tuples, table2->nrTuples);
}

/**
 * Print all the lists to target string buffer, with each column printed as list
 *
 * @param table: the table to print
 * @param tgtStr: the string to print to
 * @param tgtStrSize: the size of the target string
 * @param offset: the offset to apply with printing. Note that all elements will be printed.
 */
ssize_t swl_table_printLists(swl_table_t* table, char* tgtStr, size_t tgtStrSize, size_t offset) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_printLists(table->tupleType, tgtStr, tgtStrSize, table->tuples, table->curNrTuples, offset);
}

/**
 * Print all the lists to target file, with each column printed as list
 *
 * @param table: the table to print
 * @param file: the file pointer to which to print
 * @param offset: the offset to apply with printing. Note that all elements will be printed.
 */
bool swl_table_fprintLists(swl_table_t* table, FILE* file, size_t offset, swl_print_args_t* args) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_fprintLists(table->tupleType, file, table->tuples, table->curNrTuples, offset, true, args);
}

/**
 * Print all the lists to target string buffer, with each column printed as map with name the
 * typeIndex elements of names, appended with suffix
 *
 * @param table: the table to print
 * @param tgtStr: the string to print to
 * @param tgtStrSize: the size of the target string
 * @param offset: the offset to apply with printing. Note that all elements will be printed.
 * @param names: list of char pointers, the names for the different columns
 * @param suffix: suffix to add to names, do that the "names" list may just contain the name of a single element.
 */
ssize_t swl_table_printMaps(swl_table_t* table, char* tgtStr, size_t tgtStrSize,
                            size_t offset, char** names, char* suffix) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    return swl_tta_printMaps(table->tupleType, tgtStr, tgtStrSize, table->tuples, table->curNrTuples,
                             offset, names, suffix);

}


/**
 * Print all the lists to target file, with each column printed as map with name the
 * typeIndex elements of names, appended with suffix
 *
 * @param table: the table to print
 * @param file: the file to print to.
 * @param offset: the offset to apply with printing. Note that all elements will be printed.
 * @param names: list of char pointers, the names for the different columns
 * @param suffix: suffix to add to names, do that the "names" list may just contain the name of a single element.
 */
bool swl_table_fprintMaps(swl_table_t* table, FILE* file,
                          size_t offset, swl_print_args_t* args,
                          char** names, char* suffix) {
    ASSERTI_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");

    return swl_tta_fprintMaps(table->tupleType, file, table->tuples, table->curNrTuples, offset, true, args,
                              names, suffix);
}

ssize_t swl_tableUInt64Char_maskToCharSep(swl_table_t* table, char* tgtStr, size_t tgtStrSize, swl_mask64_m inputMask, char separator) {
    ASSERT_NOT_NULL(tgtStr, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_EQUALS(separator, '\0', SWL_RC_INVALID_PARAM, ME, "Zero separator");
    ASSERT_TRUE(swl_table_checkValid(table, false), SWL_RC_INVALID_PARAM, ME, "INVALID");
    size_t tableSize = swl_table_getSize(table);
    memset(tgtStr, 0, tgtStrSize);
    ssize_t index = 0;
    ssize_t printed = 0;
    for(size_t count = 0; count < tableSize; count++) {
        swl_tuple_t* current_tuple = swl_table_getTuple(table, count);
        uint64_t valueMask = *(uint64_t*) swl_tupleType_getValue(table->tupleType, current_tuple, 0);
        if(valueMask & inputMask) {// bit value is set--> let's get its string representation from the table
            char* valueInTable = (char*) swl_tupleType_getValue(table->tupleType, current_tuple, 1);
            if(index) {
                if((size_t) index > tgtStrSize) {
                    printed = snprintf(&tgtStr[index], 0, "%c%s", separator, valueInTable);
                } else {
                    printed = snprintf(&tgtStr[index], tgtStrSize - index, "%c%s", separator, valueInTable);
                }
            } else {
                printed = snprintf(&tgtStr[index], tgtStrSize - index, "%s", valueInTable);
            }
            index += printed;
        }
    }
    return index;
}
