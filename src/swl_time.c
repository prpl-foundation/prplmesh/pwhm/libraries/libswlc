/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <malloc.h>
#include <string.h>
#include <errno.h>
#include "swl/swl_common_time.h"
#include "swl/swl_common_time_spec.h"
#include "swl/swl_datetime.h"
#include "swl/swl_common.h"
#define ME "swlTime"

/****************************************************************************
 * SWL Time library
 *
 * This library attempts to provide helpful functions to handle time.
 * Generally internally time should only be stored as either
 * * monotonic time if only the relative differences between time entries are required
 * * realtime if the absolute value of time is important
 *
 * All exporting to struct tm, variant maps or datamodel objects must happen to
 * UTC timestamp. The only exception is printing, which may require print to local
 * time for debugging purposes.
 *
 * Note that all conversion functions from or to monotonic time may provide different
 * results when done at a different time. This is because all monotonic offset is
 * evaluated based on current system date. This date may shift because of clock adjustments
 * of NTP, or user settings date.
 ***************************************************************************/

// Some build tools chains do not have this in time.h:

//Use TR-181 requirerd NULL string
#define SWL_TIME_NULL_STR "0001-01-01T00:00:00Z"

//The string length of a time value.
#define SWL_TIME_STR_SIZE 20

// struct tm representation of NULL time.
const struct tm swl_time_nullTimeTm = {.tm_mday = 1, .tm_mon = 0, .tm_year = -1899, .tm_wday = 1};

/**
 * Return the real time of the zero point of real time.
 */
swl_timeReal_t swl_time_getRealOfMonoBaseTime() {
    return swl_timespec_getRealOfMonoBaseTimeVal().tv_sec;
}


/**
 * Returns the monotonic elapsed time.
 */
swl_timeMono_t swl_time_getMonoSec() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec;
}

/**
 * Returns the elapsed seconds since epoch.
 */
swl_timeReal_t swl_time_getRealSec() {
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec;
}

/**
 * Converts monotonic to realtime.
 * Zero monotonic time will return zero real time.
 */
swl_timeReal_t swl_time_monoToReal(swl_timeMono_t srcTime) {
    if(srcTime == 0) {
        return 0;
    }
    return swl_time_getRealOfMonoBaseTime() + srcTime;
}

/**
 * Converts monotonic time to realtime.
 * Zero real time will return zero monotonic time.
 * If the input real time is before or equal to the start of monotonic time, one will be returned,
 * as zero is used as the NULL time stamp. One is the earliest non zero monotonic time.
 */
swl_timeMono_t swl_time_realToMono(swl_timeReal_t srcTime) {
    if(srcTime == 0) {
        return 0;
    }
    swl_timeReal_t baseMono = swl_time_getRealOfMonoBaseTime();
    if(srcTime <= baseMono) {
        return 1;
    }
    return srcTime - baseMono;
}

/**
 * Converts a monotonic time in seconds to a struct tm timeval in UTC realtime.
 */
void swl_time_monoToTm(struct tm* tgtTm, const swl_timeMono_t srcTime) {

    if(srcTime == 0) {
        memcpy(tgtTm, &swl_time_nullTimeTm, sizeof(struct tm));
        return;
    }

    struct timespec ts = {.tv_sec = swl_time_getRealOfMonoBaseTime() + srcTime};
    gmtime_r(&(ts.tv_sec), tgtTm);
}

/**
 * Converts a monotonic time in seconds to a struct tm timeval in UTC realtime.
 */
void swl_time_realToTm(struct tm* tgtTm, const swl_timeReal_t srcTime) {
    if(srcTime == 0) {
        memcpy(tgtTm, &swl_time_nullTimeTm, sizeof(struct tm));
        return;
    }
    gmtime_r(&srcTime, tgtTm);
}

/**
 * Converts a struct tm timeval in UTC time to monotonic time
 */
swl_timeMono_t swl_time_tmToMono(struct tm* tmTime) {
    return swl_time_realToMono(swl_time_tmToReal(tmTime));
}

/**
 * Converts a struct tm timeval in UTC time to monotonic time
 */
swl_timeReal_t swl_time_tmToReal(struct tm* tmTime) {
    if(!memcmp(tmTime, &swl_time_nullTimeTm, sizeof(struct tm))) {
        return 0;
    }
    return timegm(tmTime);
}

/**
 * Provides a real UTC time in struct tm val format.
 */
void swl_time_getRealTm(struct tm* tmTime) {
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    gmtime_r(&(ts.tv_sec), tmTime);
}

static ssize_t s_printTime(char* tgtStr, size_t tgtStrSize, struct tm* tmVal) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(tmVal, -EINVAL, ME, "NULL");
    ASSERTS_NOT_EQUALS(tgtStrSize, 0, SWL_TIME_STR_SIZE, ME, "NULL");

    char buffer[SWL_TIME_STR_SIZE + 1];
    size_t retVal = strftime(buffer, sizeof(buffer), SWL_DATETIME_ISO8601_DATE_TIME_FMT, tmVal);
    if(retVal == 0) {
        return -ENOTRECOVERABLE;
    }
    size_t copyLen = SWL_MIN(tgtStrSize, sizeof(buffer));
    memcpy(tgtStr, buffer, copyLen);
    tgtStr[copyLen - 1] = '\0';
    return SWL_TIME_STR_SIZE;
}

/**
 * prints a monotonic time to UTC timestamp.
 * On success, it returns number of bytes that would have been printed if buffer were big enough, excluding final '\0'
 * If size is 0, it will just return size required for printing.
 * Function ensures string ends on '\0'
 * On failure, returns negative number according to errno.h
 */
ssize_t swl_time_monoToDate(char* tgtStr, size_t tgtStrSize, swl_timeMono_t srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    if(srcTime == 0) {
        return snprintf(tgtStr, tgtStrSize, SWL_TIME_NULL_STR);
    }
    struct tm tmVal;
    swl_time_monoToTm(&tmVal, srcTime);
    return s_printTime(tgtStr, tgtStrSize, &tmVal);
}

/**
 * prints a realtime to UTC timestamp.
 * On success, it returns number of bytes that would have been printed if buffer were big enough, excluding final '\0'
 * Function ensures string ends on '\0'
 * On failure, returns negative number according to errno.h
 */
ssize_t swl_time_realToDate(char* tgtStr, size_t tgtStrSize, swl_timeReal_t srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    if(srcTime == 0) {
        return snprintf(tgtStr, tgtStrSize, SWL_TIME_NULL_STR);
    }
    struct tm tmVal;
    swl_time_realToTm(&tmVal, srcTime);
    return s_printTime(tgtStr, tgtStrSize, &tmVal);
}

/**
 * prints a monotonic time to local date.
 * On success, it returns number of bytes that would have been printed if buffer were big enough, excluding final '\0'
 * Function ensures string ends on '\0'
 * On failure, returns negative number according to errno.h
 */
ssize_t swl_time_monoToLocalDate(char* tgtStr, size_t tgtStrSize, swl_timeMono_t srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    if(srcTime == 0) {
        return snprintf(tgtStr, tgtStrSize, SWL_TIME_NULL_STR);
    }
    struct tm tmVal;
    swl_timeReal_t realSec = swl_time_monoToReal(srcTime);
    localtime_r(&realSec, &tmVal);
    return s_printTime(tgtStr, tgtStrSize, &tmVal);
}

/**
 * prints a realtime to local date.
 * On success, it returns number of bytes that would have been printed if buffer were big enough, excluding final '\0'
 * Function ensures string ends on '\0'
 * On failure, returns negative number according to errno.h
 */
ssize_t swl_time_realToLocalDate(char* tgtStr, size_t tgtStrSize, swl_timeReal_t srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    if(srcTime == 0) {
        return snprintf(tgtStr, tgtStrSize, SWL_TIME_NULL_STR);
    }
    struct tm tmVal;
    localtime_r(&srcTime, &tmVal);
    return s_printTime(tgtStr, tgtStrSize, &tmVal);
}


static ssize_t s_timeReal_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_timeReal_t* srcData) {
    return swl_time_realToDate(tgtStr, tgtStrSize, *srcData);
}
static bool s_timeReal_fromChar_cb(swl_type_t* type _UNUSED, swl_timeReal_t* tgtData, const char* srcStr) {
    swl_datetime_t dateTime;
    swl_datetime_initialize(&dateTime, SWL_DATETIME_EPOCH_TR098);
    char* result = swl_datetime_strptime(srcStr, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &dateTime);
    if(result == NULL) {
        return false;
    }
    *tgtData = swl_time_tmToReal(&dateTime.datetime);
    return true;
}

swl_typeFun_t swl_type_timeReal_fun = {
    .name = "swl_timeReal",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_timeReal_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_timeReal_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};

swl_type_t gtSwl_type_timeReal = {
    .typeFun = &swl_type_timeReal_fun,
    .size = sizeof(swl_timeReal_t),
};

SWL_REF_TYPE_C(gtSwl_type_timeRealPtr, gtSwl_type_timeReal);

static ssize_t s_timeMono_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_timeMono_t* srcData) {
    return swl_time_monoToDate(tgtStr, tgtStrSize, *srcData);
}
static bool s_timeMono_fromChar_cb(swl_type_t* type _UNUSED, swl_timeMono_t* tgtData, const char* srcStr) {
    swl_datetime_t dateTime;
    swl_datetime_initialize(&dateTime, SWL_DATETIME_EPOCH_TR098);
    char* result = swl_datetime_strptime(srcStr, SWL_DATETIME_ISO8601_DATE_TIME_FMT, &dateTime);
    if(result == NULL) {
        return false;
    }
    *tgtData = swl_time_tmToMono(&dateTime.datetime);
    return true;
}

swl_typeFun_t swl_type_timeMono_fun = {
    .name = "swl_timeMono",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_timeMono_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_timeMono_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};
swl_type_t gtSwl_type_timeMono = {
    .typeFun = &swl_type_timeMono_fun,
    .size = sizeof(swl_timeMono_t),
};
SWL_REF_TYPE_C(gtSwl_type_timeMonoPtr, gtSwl_type_timeMono);
