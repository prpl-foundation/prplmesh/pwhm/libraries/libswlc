/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>   // gettimeofday()
#include <inttypes.h>
#include <unistd.h>     // _POSIX_TIMERS
#include <limits.h>
#include <ctype.h>
#include <stdbool.h>

#include <debug/sahtrace.h>
#include "swl/swl_datetime.h"

static bool swl_datetime_canConvert(const swl_datetime_t* dt) {
    // If we're not using 32-bit time_t values, we're in the clear
    if(sizeof(time_t) > 4) {
        return true;
    }
    // 32-bit time_t underflows at 20:45:52 Friday, 13 December 1901 UTC
    // and overflows at 03:14:07 Tuesday, 19 January 2038 UTC
    // These are close to year boundaries, so let's just say anything before 1902 or after 2038 will overflow
    // Note: tm_year is defined as the year since 1900, so substract 1900 from the year boundaries.
    if((dt->datetime.tm_year < (1902 - 1900)) || (dt->datetime.tm_year > (2038 - 1900))) {
        return false;
    }
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Access the internal tm structure of a datetime structure.

   @details
   Copies the pointer to the internal tm structure from the datetime structure, and returns that pointer\n
   Note that the pointer should normally NOT be freed!

   @param dt the datetime pointer

   @return
   - NULL if dt is NULL
   - a pointer to the internal struct tm
 */
const struct tm* swl_datetime_da_tm(const swl_datetime_t* dt) {
    if(!dt) {
        return NULL;
    }
    return &dt->datetime;
}

/**
   @ingroup swl_datetime
   @brief
   Convert the datetime to a struct tm

   @details
   Copies the internal tm structure from the datetime structure, and returns a pointer to a new tm pointer\n
   The caller of this routine becomes owner of the returned tm pointer and is responsible for freeing the memory.

   @param dt the datetime pointer

   @return
   - NULL if dt is NULL or no more memory free
   - a pointer to a correct struct tm
 */
struct tm* swl_datetime_tm(const swl_datetime_t* dt) {
    if(!dt) {
        return NULL;
    }
    struct tm* tm = calloc(1, sizeof(struct tm));
    if(!tm) {
        return NULL;
    }
    memcpy(tm, &dt->datetime, sizeof *tm);
    return tm;
}

/**
   @ingroup swl_datetime
   @brief
   Get the nanoseconds part of the datetime structure

   @details
   Returns the 'nanoseconds' member of a datetime structure.\n
   NOTE: this function does NOT convert the total structure to a certain amount of nanoseconds!
    Use swl_datetime_time(dt)*1000000000ULL+swl_datetime_nanoseconds(dt) for that

   @param dt the datetime pointer

   @return
    - 0 if the dt pointer was invalid
    - The number of nanoseconds stored in the datetime structure
 */
uint32_t swl_datetime_nanoseconds(const swl_datetime_t* dt) {
    if(!dt) {
        return 0;
    }
    return dt->nanoseconds;
}

/**
   @ingroup swl_datetime
   @brief
   Initializes a datetime structure.

   @details
   Initializes the argument with a "default" datetime.
   Two different defaults are possible, depending on the epoch value (@see swl_datetime_epoch_t):\n
 * SWL_DATETIME_EPOCH_UNIX: set to "1970-01-01T00:00:00Z" \n
 * SWL_DATETIME_EPOCH_TR098: set to "0001-01-01T00:00:00Z".\n
   This default is specified by TR-098, but can NOT be used with many other datetime functions.
   Use swl_datetime_fromChar(dt, swl_datetime_UNIXEPOCH) instead in your own code!\n

   @param[out] dt the datetime pointer to initialize
   @param[in] epoch The epoch to use, which determines the default value to initialize to.
 */
void swl_datetime_initialize(swl_datetime_t* dt, swl_datetime_epoch_t epoch) {
    if(!dt) {
        return;
    }
    memset(dt, 0, sizeof *dt);
    dt->datetime.tm_mday = 1;
    switch(epoch) {
    case SWL_DATETIME_EPOCH_UNIX: dt->datetime.tm_year = 70; break;          //1970-01-01
    case SWL_DATETIME_EPOCH_TR098: dt->datetime.tm_year = -1899; break;      //0001-01-01
    }
}

/**
   @ingroup swl_datetime
   @brief
   Get the current time as a datetime structure.

   @details
   Returns the current time in the highest precision and accuracy that is allowed by the current system, and put it in the argument.\n

   @param[out] dt the datetime pointer to store the time into

   @return
    - false if the dt pointer was invalid or getting the time failed
    - true if we successfully stored the current time in the argument
 */
bool swl_datetime_now(swl_datetime_t* dt) {
    if(dt == NULL) {
        return false;
    }
// man clock_gettime: On POSIX systems on which these functions are available,
// the symbol _POSIX_TIMERS is defined in <unistd.h> to a value  greater  than  0
#if _POSIX_TIMERS > 0
    struct timespec ts;
    if(clock_gettime(CLOCK_REALTIME, &ts) != 0) {
        return false;
    }
    return swl_datetime_fromTimespec(dt, &ts);
#else
    struct timeval tv;
    if(gettimeofday(&tv, NULL) != 0) {
        return false;
    }
    return swl_datetime_fromTimeval(dt, &tv);
#endif
}

/**
   @ingroup swl_datetime
   @brief
   Get the difference between 2 datetime structures (in nanoseconds).

   @details
   Returns the amount of nanoseconds that have passed in between the two datetime structures presented.\n
   The result will be negative if the first argument happened after the second, positive otherwise.\n
   The maximum difference that can be represented by a 64-bit signed int is about 292 years

   @param start the first datetime
   @param end the second datetime

   @return
    - LLONG_MAX if one of the arguments was NULL
    - LLONG_MAX if one of the arguments cannot be converted to a time_t (out of range 1902-2038)
    - 0 if the arguments provided are equal
    - the difference, in nanoseconds, between the two timestamps otherwise.
 */
int64_t swl_datetime_delta(const swl_datetime_t* start, const swl_datetime_t* end) {
    int64_t delta = 0LL;
    if(!start || !end) {
        return LLONG_MAX;
    }
    if(!swl_datetime_canConvert(start) || !swl_datetime_canConvert(end)) {
        return LLONG_MAX;
    }
    time_t start_t, end_t;
    start_t = swl_datetime_time(start);
    end_t = swl_datetime_time(end);

    delta = 1000000000LL * (end_t - start_t);
    return delta + ((int64_t) end->nanoseconds - (int64_t) start->nanoseconds);
}

/**
   @ingroup swl_datetime
   @brief
   Convert the datetime structure to a timespec structure.

   @details
   Stores the time represented by the datetime structure in the provided timespec structure.\n

   @param dt the datetime structure
   @param ts the timespec structure

   @return
    - false if either argument was NULL
    - false if the datetime cannot be converted to a struct timespec (out of range for time_t)
    - true if the conversion was successfully performed.
 */
bool swl_datetime_toTimespec(const swl_datetime_t* dt, struct timespec* ts) {
    if(!dt || !ts) {
        return false;
    }
    if(!swl_datetime_canConvert(dt)) {
        return false;
    }
    struct tm tm_copy = dt->datetime;
    ts->tv_sec = timegm(&tm_copy);  // timegm may modify its argument
    ts->tv_nsec = (long) dt->nanoseconds;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Convert a timespec structure to a datetime structure.

   @details
   Stores the time represented by the timespec structure in the provided datetime structure.\n

   @param dt the datetime structure
   @param ts the timespec structure

   @return
    - false if either argument was NULL
    - true if the conversion was successfully performed.
 */
bool swl_datetime_fromTimespec(swl_datetime_t* dt, const struct timespec* ts) {
    if(!dt || !ts) {
        return false;
    }
    gmtime_r(&ts->tv_sec, &dt->datetime);
    dt->nanoseconds = (uint32_t) ts->tv_nsec;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Convert the datetime structure to a timeval structure.

   @details
   Stores the time represented by the datetime structure in the provided timeval structure.\n

   @param dt the datetime structure
   @param tv the timeval structure

   @return
    - false if either argument was NULL
    - false if the datetime cannot be converted to a struct timespec (out of range for time_t)
    - true if the conversion was successfully performed.
 */
bool swl_datetime_toTimeval(const swl_datetime_t* dt, struct timeval* tv) {
    if(!dt || !tv) {
        return false;
    }
    if(!swl_datetime_canConvert(dt)) {
        return false;
    }
    struct tm tm_copy = dt->datetime;
    tv->tv_sec = timegm(&tm_copy);  // timegm may modify its argument
    tv->tv_usec = dt->nanoseconds / 1000;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Convert a timeval structure to a datetime structure.

   @details
   Stores the time represented by the timeval structure in the provided datetime structure.\n

   @param dt the datetime structure
   @param ts the timeval structure

   @return
    - false if either argument was NULL
    - true if the conversion was successfully performed.
 */
bool swl_datetime_fromTimeval(swl_datetime_t* dt, const struct timeval* tv) {
    if(!dt || !tv) {
        return false;
    }
    gmtime_r(&tv->tv_sec, &dt->datetime);
    dt->nanoseconds = (uint32_t) tv->tv_usec * 1000;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Return the time represented by the supplied datetime structure as a new timeval structure.

   @details
   Stores the time represented by the datetime structure in a new timeval structure,
    and returns the new timeval structure.\n
   The caller is responsible for freeing the memory taken up by the timeval structure

   @param dt the datetime structure

   @return
    - NULL if dt was NULL or the allocation failed or the time cannot be represented as a struct timeval
    - a new timeval structure corresponding to the supplied datetime structure
 */
struct timeval* swl_datetime_timeval(const swl_datetime_t* dt) {
    struct timeval* tv = calloc(1, sizeof(struct timeval));
    if(!tv) {
        return NULL;
    }
    if(!swl_datetime_toTimeval(dt, tv)) {
        free(tv);
        return NULL;
    }
    return tv;
}

/**
   @ingroup swl_datetime
   @brief
   Return the time represented by the supplied datetime structure as a new timespec structure.

   @details
   Stores the time represented by the datetime structure in a new timespec structure,
   and returns the new timespec structure.\n
   The caller is responsible for freeing the memory taken up by the timespec structure

   @param dt the datetime structure

   @return
    - NULL if dt was NULL or the allocation failed or the time cannot be represented as a struct timespec
    - a new timespec structure corresponding to the supplied datetime structure
 */
struct timespec* swl_datetime_timespec(const swl_datetime_t* dt) {
    struct timespec* ts = calloc(1, sizeof(struct timespec));
    if(!ts) {
        return NULL;
    }
    if(!swl_datetime_toTimespec(dt, ts)) {
        free(ts);
        return NULL;
    }
    return ts;
}

/**
   @ingroup swl_datetime
   @brief
   Convert a timestamp (time_t) to a datetime structure.

   @details
   Stores the time represented by the timestamp in the provided datetime structure.\n

   @param dt the datetime structure
   @param tim the timestamp

   @return
    - false if either argument was NULL
    - true if the conversion was successfully performed.
 */
bool swl_datetime_fromTime(swl_datetime_t* dt, time_t tim) {
    if(!dt) {
        return false;
    }
    gmtime_r(&tim, &dt->datetime);
    dt->nanoseconds = 0;
    return true;
}
/**
   @ingroup swl_datetime
   @brief
   Convert the datetime structure to a time_t.

   @details
   Stores the time represented by the datetime structure in the provided time_t argument.\n

   @param dt the datetime structure
   @param[out] tm the time_t parameter to set

   @return
    - false if either argument was NULL or the time cannot be represented as a time_t
    - true if the conversion was successfully performed.
 */
bool swl_datetime_toTime(const swl_datetime_t* dt, time_t* tm) {
    if(!dt || !tm) {
        return false;
    }
    if(!swl_datetime_canConvert(dt)) {
        return false;
    }
    struct tm tm_copy = dt->datetime;
    *tm = timegm(&tm_copy);  // timegm may modify its argument
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Convert a tm structure to a datetime structure.

   @details
   Stores the time represented by the tm structure in the provided datetime structure.\n

   @param dt the datetime structure
   @param tm the tm structure

   @return
    - false if either argument was NULL
    - true if the conversion was successfully performed.
 */
bool swl_datetime_fromTm(swl_datetime_t* dt, const struct tm* tm) {
    if(!dt || !tm) {
        return false;
    }

    dt->datetime = *tm;
    dt->nanoseconds = 0;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Convert the datetime structure to a tm structure.

   @details
   Stores the time represented by the datetime structure in the provided tm structure.\n

   @param dt the datetime structure
   @param tm the tm structure

   @return
    - false if either argument was NULL
    - true if the conversion was successfully performed.
 */
bool swl_datetime_toTm(const swl_datetime_t* dt, struct tm* tm) {
    if(!dt || !tm) {
        return false;
    }
    if(!swl_datetime_canConvert(dt)) {
        SAH_TRACEZ_NOTICE("swl_dt", "Exposing an unconvertible struct tm");
    }
    *tm = dt->datetime;
    return true;
}

/**
   @ingroup swl_datetime
   @brief
   Get the timestamp corresponding to the provided datetime structure.

   @details
   Returns the time in the datetime structure in the form of a time_t.\n

   @param dt the datetime structure

   @return
    - (time_t)-1 if the provided structure pointer was NULL or the datetime cannot be converted
    - The correct timestamp otherwise.
 */
time_t swl_datetime_time(const swl_datetime_t* dt) {
    if(!dt) {
        return (time_t) -1;
    }
    if(!swl_datetime_canConvert(dt)) {
        return (time_t) -1;
    }
    struct tm tm_copy = dt->datetime;
    return timegm(&tm_copy);  // timegm may modify its argument
}

/**
 * @brief Parse the date and time in <i>input</i> according to <i>format</i>, and store the result in <i>out</i> as a swl_datetime_t.
 *
 * @details
 * This function aims to replicate the system strptime function (@see strptime(3)),
 *  except for 2 important differences:
 *  1. We're outputting to a swl_datetime_t structure instead of a struct tm
 *  2. New format specifiers are added:
 *      2.1 %f: Number of microseconds (taken from the python implementation of strptime, hopefully a future standard)
 *      2.2 %N: Number of nanoseconds (also supported by the GNU date command)
 *      2.3 %i: Number of seconds as floating point number (instead of %S, which represents an integer number of seconds)
 *
 * @param input The input string (e.g. "2019-07-17T13:01:22.1234Z")
 * @param format The format the string is expected to conform to (@see strptime(3)), e.g. "%Y-%m-%dT%H:%M:%iZ"
 * @param out The resulting swl_datetime_t structure
 *
 * @return
 *      - if the entire input string was processed: a pointer to the '\0' character at the end of the input string)
 *      - if not all the input was processed: a pointer to the first character not processed in this function call
 *      - if none of the input string could be processed or some other error occurred, NULL is returned
 */
char* swl_datetime_strptime(const char* input, const char* format, swl_datetime_t* out) {
    const char* input_it = input;
    const char* format_it = format;
    bool matching = true;

    if(!input || !format || !out) {
        return NULL;
    }
    memset(out, 0, sizeof *out);

    while(matching && *input_it && *format_it) {
        if(isspace(*format_it)) {
            while(isspace(*input_it)) {
                ++input_it;
            }
            ++format_it;
        } else if(*format_it != '%') {
            if(*input_it == *format_it) {
                ++input_it;
                ++format_it;
            } else {
                matching = false;
            }
        } else { // *format_it == '%'
            ++format_it;
            switch(*format_it) {
            case '%':
            {
                if(*input_it == '%') {
                    ++input_it;
                } else {
                    matching = false;
                }
                break;
            }
            // Newly introduced %f and %N specifiers: number of microseconds and nanoseconds, respectively
            case 'f':
            case 'N':
            {
                char* endptr = NULL;
                unsigned long useconds = strtoul(input_it, &endptr, 10);
                if(input_it != endptr) {
                    out->nanoseconds = (*format_it == 'f') ? useconds * 1000 : useconds;
                    input_it = (const char*) endptr;
                } else {
                    matching = false;
                }
                break;
            }
            case 'i':
            {
                char* endptr = NULL;
                double sec = strtod(input_it, &endptr);
                if(input_it != endptr) {
                    out->datetime.tm_sec = (int) sec;
                    // +0.1: guard against FP rounding -> int casting errors i.e. "0.6" -> 0.59999999999999932 -> 0.600000000 instead of 0.599999999
                    out->nanoseconds = (uint32_t) ((sec - (int) sec) * (double) 1000000000LL + 0.1);
                    input_it = (const char*) endptr;
                } else {
                    matching = false;
                }
                break;
            }
            // Use system strptime function to parse other specifiers (but parse them one at a time)
            default:
            {
                char format_tmp[4];
                if((*format_it == '0') || (*format_it == 'E')) {
                    sprintf(format_tmp, "%%%.2s", format_it);
                    ++format_it;
                } else {
                    sprintf(format_tmp, "%%%c", *format_it);
                }
                char* next_str = strptime(input_it, format_tmp, &out->datetime);
                if(next_str == NULL) {
                    matching = false;
                } else {
                    input_it = (const char*) next_str;
                }
                break;
            }
            }
            ++format_it;
        }
    }
    if(input_it == input) {
        return NULL;
    }
    return (char*) input_it;
}


/**
 * @brief Format the date and time in <i>dt</i> according to <i>format</i>, and store the result in <i>s</i> as a char*.
 *
 * @details
 * This function aims to replicate the system strftime function (@see the strftime manpage),
 *  except for 2 important differences:
 *  1. We're converting from a swl_datetime_t structure instead of a struct tm
 *  2. New format specifiers are added:
 *      2.1 %f: Number of microseconds (taken from the python implementation of strftime, hopefully a future standard)
 *      2.2 %N: Number of nanoseconds (also supported by the GNU date command)
 *      2.3 %i: Number of seconds as floating point number (instead of %S, which represents an integer number of seconds)
 *
 * @param s The output string (e.g. "2019-07-17T13:01:22.123400Z" after this call)
 * @param max The maximum amount of bytes that may be written to s (including NUL byte)
 * @param format The format the string is expected to conform to (@see strptime(3)), e.g. "%Y-%m-%dT%H:%M:%iZ"
 * @param dt The input swl_datetime_t structure
 *
 * @return
 *      - 0 if an error occurred
 *      - the length of the resulting string placed in s otherwise (does not include NUL byte)
 */
size_t swl_datetime_strftime(char* s, size_t max, const char* format, const swl_datetime_t* dt) {
    if(!s || !format || !dt || (max == 0)) {
        return 0;
    }
    const char* format_it = format;
    char* out_it = s;
    size_t spaceUsed = out_it - s;
    while(*format_it && spaceUsed < max - 1) {
        if(*format_it != '%') {
            *out_it = *format_it;
            ++format_it;
            ++out_it;
        } else {
            ++format_it;
#define CHECK_RESULT_AND_ADVANCE(rv)  \
    if((int) rv >= (int) (max - spaceUsed) || rv <= 0) \
    { \
        *out_it = '\0'; \
        return spaceUsed; \
    } \
    out_it += rv
            switch(*format_it) {
            case 'Y':
            {
                int rv = snprintf(out_it, max - spaceUsed, "%04d", dt->datetime.tm_year + 1900);
                CHECK_RESULT_AND_ADVANCE(rv);
                break;
            }
            case 'N':
            {
                int rv = snprintf(out_it, max - spaceUsed, "%09" PRIu32, dt->nanoseconds);
                CHECK_RESULT_AND_ADVANCE(rv);
                break;
            }
            case 'f':
            {
                int rv = snprintf(out_it, max - spaceUsed, "%06" PRIu32, dt->nanoseconds / 1000);
                CHECK_RESULT_AND_ADVANCE(rv);
                break;
            }
            case 'i':
            {
                int rv = snprintf(out_it, max - spaceUsed, "%02d.%06" PRIu32,
                                  dt->datetime.tm_sec,
                                  dt->nanoseconds / 1000);
                CHECK_RESULT_AND_ADVANCE(rv);
                break;
            }
            // Use system strftime function to format other specifiers (but format them one at a time)
            default:
            {
                char format_tmp[4];
                if((*format_it == '0') || (*format_it == 'E')) {
                    sprintf(format_tmp, "%%%.2s", format_it);
                    ++format_it;
                } else {
                    sprintf(format_tmp, "%%%c", *format_it);
                }
                size_t sz = strftime(out_it, max - spaceUsed, format_tmp, &dt->datetime);
                CHECK_RESULT_AND_ADVANCE(sz);
            }
            }
            ++format_it;
        }
        spaceUsed = out_it - s;
    }
    *out_it = '\0';
    return spaceUsed;
}
