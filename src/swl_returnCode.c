/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <swl/swl_common.h>

#define ME "swlRc"

const char* swl_rc_strErr[] = {"INVALID", "Error", "Invalid parameter", "Invalid state", "Not implemented", "Not available", "Result out of bounds"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_rc_strErr) == -1 * SWL_RC_MIN, "swl_rc_strOk not correctly defined");
const char* swl_rc_strOk[] = {"OK", "Done", "Continuing"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_rc_strOk) == SWL_RC_MAX, "swl_rc_strOk not correctly defined");


/**
 * @brief Return the const string for a given return code.
 *
 * @param returnCode: the return code to translate to string
 * @return string associated with given return code.
 *     Shall return NULL if returnCode is invalid.
 */
const char* swl_rc_toString(swl_rc_ne returnCode) {
    int index = (int) returnCode;
    if(index < 0) {
        if(index <= SWL_RC_MIN) {
            SAH_TRACEZ_ERROR(ME, "Invalid retCode request %u", index);
            return NULL;
        }
        index = -1 * index;
        return swl_rc_strErr[index];
    } else {
        if(index >= SWL_RC_MAX) {
            SAH_TRACEZ_ERROR(ME, "Invalid retCode request %u", index);
            return NULL;
        }
        return swl_rc_strOk[index];
    }

}

/**
 * @brief Return whether a given return code indicates normal positive operation
 *
 * @param returnCode: the return code to evaluate
 * @return whether return code indicates that function ended positively.
 *     Shall return false if returnCode is invalid.
 */
bool swl_rc_isOk(swl_rc_ne returnCode) {
    if((returnCode <= SWL_RC_MIN) || (returnCode >= SWL_RC_MAX)) {
        SAH_TRACEZ_ERROR(ME, "Invalid check %u", returnCode);
        return false;
    }
    return (returnCode >= 0);
}
