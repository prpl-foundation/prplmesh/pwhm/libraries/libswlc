/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_usp_cmdStatus.h"

#define ME "swlUspC"

/*
 * usp command status string list as defined in
 * https://usp-data-models.broadband-forum.org/tr-181-2-15-1-usp.html
 */
static const char* swl_uspCmdStatus_str[] = {
    "Error_Invalid_Mac",
    "Error_Interface_Down",
    "Error_Timeout",
    "Error_Other",
    "Error_Not_Implemented",
    "Error_Not_Ready",
    "Error_Invalid_Input",
    "Error",
    "Success",
    "Complete",
    "Canceled",
    SWL_USP_CMD_STATUS_UNDEFINED_STR, //for SWL_USP_CMD_STATUS_UNDEFINED: MUST be the last entry
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_uspCmdStatus_str) == SWL_USP_CMD_STATUS_COUNT, "swl_uspCmdStatus_str not correctly defined");

/**
 * @brief Return the const string for a given command status.
 *
 * @param statusCode: the status code to translate to string
 * @return string associated with given status code.
 *     Shall return SWL_USP_CMD_STATUS_UNDEFINED_STR if returnCode is unknown.
 */
const char* swl_uspCmdStatus_toString(swl_usp_cmdStatus_ne statusCode) {
    int32_t index = statusCode - 1 - SWL_USP_CMD_STATUS_MIN;
    if((statusCode <= SWL_USP_CMD_STATUS_MIN) || (statusCode >= SWL_USP_CMD_STATUS_MAX) ||
       (index < 0) || (index >= SWL_USP_CMD_STATUS_COUNT)) {
        return swl_uspCmdStatus_str[SWL_USP_CMD_STATUS_UNDEFINED];
    }
    return swl_uspCmdStatus_str[index];
}

/**
 * @brief Return the command status code for a given status string
 *
 * @param statusStr: the status string to translate to code
 * @return cmdStatus code associated with given status string.
 *     Shall return SWL_USP_CMD_STATUS_UNDEFINED if status string does not matches.
 */
int32_t swl_uspCmdStatus_toCode(const char* statusStr) {
    ASSERTS_NOT_NULL(statusStr, SWL_USP_CMD_STATUS_UNDEFINED, ME, "NULL");
    //ckeck known status strings
    int32_t code;
    for(uint32_t i = 0; i < SWL_USP_CMD_STATUS_COUNT - 1; i++) {
        if(!strcmp(swl_uspCmdStatus_str[i], statusStr)) {
            if((code = (SWL_USP_CMD_STATUS_MIN + i + 1)) < SWL_USP_CMD_STATUS_MAX) {
                return code;
            }
        }
    }
    return SWL_USP_CMD_STATUS_UNDEFINED;
}
