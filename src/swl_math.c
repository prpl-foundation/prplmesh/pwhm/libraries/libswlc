/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include "swl/swl_bit.h"
#include "swl/swl_common.h"
#include "swl/swl_assert.h"
#include "swl/swl_math.h"
#include <math.h>

#define ME "swlMath"

/**
 * Check whether a given value is a power of a given number,
 * without actually calculating the logarithm value.
 */
bool swl_math_isPowerOf(int64_t value, int64_t powerBase) {
    if(powerBase == 0) {
        return (value == 0);
    }
    if(SWL_MATH_ABS(powerBase) == 1) {
        return value == powerBase || value == 1;
    }
    while(value != 1) {
        if((value % powerBase) != 0) {
            return false;
        }
        value = value / powerBase;
    }

    return true;
}

/**
 * Check whether the given value is a power of two.
 * More efficiently calculated that swl_math_isPowerOf
 * Note that only unsigned values are allowed as negative can never be power of 2
 */
bool swl_math_isPowerOf2(uint64_t value) {
    ASSERT_TRUE(value > 0, false, ME, "INVALID");
    uint64_t bit = value;
    bit &= -bit;
    return (value == (bit));
}

/**
 * Calculate the inverse of the sum of the inverses of the two numbers
 * Function will down shift one of the inputs, if multiplication does not fit
 * in 64 bit, so maximum accuracy is preserved. Will only happen when (likely) needed.
 * Function will only use integer logic for speed.
 */
uint64_t swl_math_invSumInv(uint64_t val1, uint64_t val2) {
    ASSERTS_NOT_EQUALS(val1, 0, 0, ME, "ZERO");
    ASSERTS_NOT_EQUALS(val2, 0, 0, ME, "ZERO");

    uint32_t nrBit1 = swl_bit64_getHighest(val1);
    uint32_t nrBit2 = swl_bit64_getHighest(val2);
    uint32_t totalBit = (nrBit1 + nrBit2) + 2;
    uint64_t shift = 1;
    uint64_t tmpVal1 = val1;
    uint64_t tmpVal2 = val2;

    if(totalBit > 64) {
        shift = SWL_BIT_SHIFT(totalBit - 64);
    }
    if(val1 > val2) {
        tmpVal1 = tmpVal1 / shift;
    } else {
        tmpVal2 = tmpVal2 / shift;
    }

    uint64_t invSumInv = tmpVal1 * tmpVal2 / (val1 + val2);
    invSumInv = invSumInv * shift;
    return invSumInv;
}

/**
 * Interpolate the y value between two given x values, based on dist / maxDist ratio
 *
 * Extrapolation is also possible
 */
uint64_t swl_math_interpolate(uint64_t point1, uint64_t point2, int64_t dist, int64_t maxDist) {
    return (point1 * (maxDist - dist) + point2 * dist) / maxDist;
}

/**
 * Returns the remainder under floored division. This guarantees modulo will always be positive
 *
 * Rule aplies that dividend = fdiv(dividend,divider) * divider + fmod(dividend, divider).
 * Only when divider == 0, can we not satisfy this rule, and as such a arithmetic error is still printed
 *
 */
uint64_t swl_math_fmod(int64_t dividend, int64_t divider) {
    int64_t remainder = dividend % divider;
    return (uint64_t) ((remainder < 0 ) ? (remainder + llabs(divider)) : remainder);
}

/*
 * Returns the "floored division" value
 *
 * Rule aplies that dividend = fdiv(dividend,divider) * divider + fmod(dividend, divider).
 * Only when divider == 0, can we not satisfy this rule, and as such a arithmetic error is still printed
 */
int64_t swl_math_fdiv(int64_t dividend, int64_t divider) {
    int64_t div = (dividend < 0 ? (dividend - llabs(divider) + 1) : dividend);
    return div / divider;
}

/**
 * Do unsigned division with given division option;
 */
uint64_t swl_math_udiv(uint64_t dividend, uint64_t divider, swl_math_roundOpt_e opt) {
    if(opt == SWL_MATH_ROUND_OPT_DOWN) {
        return dividend / divider;
    } else if(opt == SWL_MATH_ROUND_OPT_HALF) {
        return (dividend + divider / 2) / divider;
    } else if(opt == SWL_MATH_ROUND_OPT_UP) {
        return (dividend + divider - 1) / divider;
    }
    SAH_TRACEZ_ERROR(ME, "Unknown round option %u", opt);
    return 0;
}

/**
 * Returns the complement under base of the multiplication of the complements of val1 and val2
 *
 * This is useful to for example multiply failure percentages, to come to a final failure percentage
 */
uint64_t swl_math_multComplement(uint64_t base, uint64_t val1, uint64_t val2) {
    uint64_t compl1 = base - val1;
    uint64_t compl2 = base - val2;
    uint64_t complRes = compl1 * compl2 / base;
    return base - complRes;
}

/**
 * perform exponential averaging:
 * @param accum
 *  the current accumulator value
 * @param newVal
 *  the new value to be accumulated
 * @param factor
 *  the factor with which to do exponential averaging
 * @param first
 *  whether this is the first step.
 *
 * @returns the new accumulator value
 *
 */
int32_t swl_math_doExpAvgStep(int32_t accum, int32_t newVal, int32_t factor, bool first) {
    if(first) {
        return newVal * 1000;
    } else {
        int64_t accumulator = accum;
        accumulator = ((1000 - factor) * accumulator) / 1000 + factor * newVal;
        return (int32_t) accumulator;
    }
}

/**
 * get the current exponential average actual value, based on accumulator value.
 * @param accum
 *  the current accumulator value
 *
 * @return
 *  the average observed value
 */
int32_t swl_math_getExpAvgResult(int32_t accum) {
    if(accum > 0) {
        return (accum + 500) / 1000;
    } else {
        return (accum - 500) / 1000;
    }
}


uint8_t s_log10Two[65] = {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15, 15, 15, 15, 16, 16, 16, 17, 17, 17, 18, 18, 18, 18, 19};
uint64_t s_sizeVals[20] = {1, 1E1, 1E2, 1E3, 1E4, 1E5, 1E6, 1E7, 1E8, 1E9, 1E10, 1E11, 1E12, 1E13, 1E14, 1E15, 1E16, 1E17, 1E18, 1E19};

/**
 * An efficient 64 bit log 10, two orders of magnitude faster than float log10 on arm
 *
 * @param val
 *   the value of which the log10 must be taken
 *
 * @return the log10 of val, if val is not zero. Zero otherwise.
 */
uint64_t swl_math_log10(uint64_t val) {
    if(val == 0) {
        return 0;
    }
    uint64_t test = 64 - __builtin_clzll(val);
    uint64_t log = s_log10Two[test];
    if(s_sizeVals[log] > val) {
        log--;
    }
    return log;
}


uint64_t s_log10Breakpoints[45] = {
    3162277660168379331,
    2154434690031877847, 4641588833612791549,
    1778279410038922801, 3162277660168379331, 5623413251903490803,
    1584893192461110892, 2511886431509571891, 3981071705534985534, 6309573444801942817,
    1467799267622073543, 2154434690031877847, 3162277660168379331, 4641588833612791549, 6812920690579594277,
    1389495494373136013, 1930697728883245654, 2682695795279716342, 3727593720314953234, 5179474679231223240, 7196856730011528609,
    1333521432163324025, 1778279410038922801, 2371373705661655261, 3162277660168379331, 4216965034285822485, 5623413251903490803, 7498942093324558273,
    1291549665014882701, 1668100537200055721, 2154434690031877847, 2782559402207114480, 3593813663804640368, 4641588833612791549, 5994842503189421045, 7742636826811277634,
    1258925411794171329, 1584893192461110892, 1995262314968882865, 2511886431509571891, 3162277660168379331, 3981071705534985534, 5011872336272714650, 6309573444801942817, 7943282347242789028,
};


/**
 * An efficient way to calculate (mult * log10 (val)) for integers.
 * Little gain on x86 architecture, but almost 2 orders of magnitude gain on arm.

 * @param val
 *   the value of which the log10 must be taken
 * @param mult
 *   the multiplier with which the logaritm should be multiplied.
 *
 * @return mult * log10 of val, if val is not zero. Zero otherwise.
 *   if mult <= 10, the efficient algorithm is used, otherwise inefficient floating point algorithm is used.
 */
uint64_t swl_math_log10Mult(uint64_t val, uint64_t mult) {
    if((val == 0) || (mult == 0)) {
        return 0;
    } else if(mult == 1) {
        return swl_math_log10(val);
    } else if(mult <= 10) {
        uint64_t test = 64 - __builtin_clzll(val);
        uint64_t log = s_log10Two[test];
        if(s_sizeVals[log] > val) {
            log--;
        }
        uint64_t testVal = val;
        if(log < 18) {
            testVal = val * ( s_sizeVals[18 - log]);
        } else if(log > 18) {
            testVal = val / 10;
        }
        log = log * mult;

        uint32_t index = mult - 2;
        uint32_t startIndex = (index * (index + 1)) >> 1;
        for(uint32_t i = 0; i < (mult - 1); i++) {

            if(testVal >= s_log10Breakpoints[startIndex + i]) {
                log++;
            }
        }
        return log;
    } else {
        return (uint64_t) ( mult * log10(val));
    }
}

/**
 * Returns the index of "insertion" for a uint32_t val, to have an ordered array.
 * It assumes array is ordered.
 * So this function returns the smallest value i, for which val <= array[i], and val > array[i-1]
 * If val is larger than any element in array, size is returned.
 */
uint32_t swl_math_getInsertIndexU32(const uint32_t* array, uint32_t size, uint32_t val) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");
    for(uint32_t i = 0; i < size; i++) {
        if(val <= array[i]) {
            return i;
        }
    }
    return size;
}

/**
 * Interpolate the source in the source array, providing the matching interpolated target array value
 * @param srcArray : the array in which source needs to be interpolated. Needs to contain nrValues values.
 *      should be ordered small to large always.
 * @param targetArray: the array from which the results needs to be interpolated. Needs to contain nrValues values.
 *      does not need to be ordered.
 * @param nrValues: the number of values in both srcArray and targetArray
 * @param source the source value which shall be used to determine the place in srcArray which to use to map to targetArray.
 *
 * @return
 *  * if nrValues is zero, or any of the arrays are NULL, then zero
 *  * else if source is smaller or equal to srcArray[0], then targetArray[0]
 *  * else if source is larger or equal to srcArray[nrValues - 1], then targetArray[nrValues -1]
 *  * else targetArray[i-1] + (targetArray[i] - targetArray[i-1]) * (source - srcArray[i-1]) / (srcArray[i] - srcArray[i-1],
 *  *  for the first i with 0 < i < nrValues where source <= srcArray[i]
 */
double swl_math_interpolateDArray(const double* srcArray, const double* targetArray, size_t nrValues, double source) {
    if(nrValues == 0) {
        return 0.0;
    }
    ASSERT_NOT_NULL(srcArray, 0.0, ME, "NULL");
    ASSERT_NOT_NULL(targetArray, 0.0, ME, "NULL");
    if(source <= srcArray[0]) {
        return targetArray[0];
    }
    for(size_t i = 1; i < nrValues; i++) {
        if(source == srcArray[i]) {
            return targetArray[i];
        }
        if(source < srcArray[i]) {
            double base = (targetArray[i] - targetArray[i - 1]);
            double prod = (source - srcArray[i - 1]);
            double div = (srcArray[i] - srcArray[i - 1]);
            return targetArray[i - 1] + base * prod / div;
        }
    }
    return targetArray[nrValues - 1];
}
