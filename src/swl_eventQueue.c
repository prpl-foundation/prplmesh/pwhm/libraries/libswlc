/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_eventQueue.h"
#include "swl/swl_common.h"

#define ME "swl_evtQ"

#include "swl/swl_llist.h"

struct swl_eventQueue_s {
    swl_llist_t subscribers;
    char* name;
};



typedef struct {
    swl_llist_iterator_t it;
    bs_eventQueue_cbf callback;
    void* data;
    const char* file;
    uint32_t line;
} swl_eventQueue_callback_t;


swl_rc_ne swl_eventQueue_init(swl_eventQueue_t* queue, const char* name) {
    ASSERT_NOT_NULL(queue, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_STR(name, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_FALSE(queue->init, SWL_RC_INVALID_STATE, ME, "%s: already init", queue->name);

    queue->init = true;
    queue->name = strdup(name);
    swl_llist_initialize(&queue->subscribers);

    return SWL_RC_OK;
}


/**
 * Destroy the event queue. It will return:
 * * SWL_RC_OK if all handlers were properly removed before calling destroy
 * * SWL_RC_DONE if any handlers were still active. These will be cleaned up.
 * * SWL_RC_INVALID_PARAM if args are NULL
 * * SWL_RC_INVALID_STATE if queue is not initialized
 */
swl_rc_ne swl_eventQueue_destroy(swl_eventQueue_t* queue _UNUSED) {
    ASSERT_NOT_NULL(queue, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(queue->init, SWL_RC_INVALID_STATE, ME, "%s: not init", queue->name);

    bool cleaned = false;

    SWL_LLIST_FOR_EACH(swl_eventQueue_callback_t, it, subs, &queue->subscribers) {
        SAH_TRACEZ_ERROR(ME, "%s: still subscribed %s:%u", queue->name, subs->file, subs->line);
        swl_llist_iterator_take(&subs->it);
        free(subs);
        cleaned = true;
    } SWL_LLIST_DONE;

    queue->init = false;
    if((queue->name != NULL) && !queue->staticName) {
        free(queue->name);
        queue->name = NULL;
    }

    return cleaned ? SWL_RC_DONE : SWL_RC_OK;
}

swl_eventQueue_callback_t* s_findHandler(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data) {
    ASSERT_NOT_NULL(queue, NULL, ME, "NULL");

    SWL_LLIST_FOR_EACH(swl_eventQueue_callback_t, it, subs, &queue->subscribers) {
        if((subs->callback == callback) && (subs->data == data)) {
            return subs;
        }
    } SWL_LLIST_DONE;
    return NULL;
}

bool swl_eventQueue_hasSubscriber(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data) {
    return s_findHandler(queue, callback, data) != NULL;
}

/**
 * Subscribe to given queue, as given callback and data. Data may be NULL.
 * Shall retuern SWL_RC_OK if properly subscribed.
 * Shall return SWL_RC_INVALID_PARAM if queue is NULL
 * Shall return SWL_RC_INVALID_STATE if subscription already exists.
 * Shall return SWL_RC_NOT_AVAILABLE if no memory available for subscription.
 */
swl_rc_ne swl_eventQueue_subscribe(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data,
                                   const char* file _UNUSED, uint32_t line _UNUSED) {
    ASSERT_NOT_NULL(queue, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_FALSE(swl_eventQueue_hasSubscriber(queue, callback, data), SWL_RC_INVALID_STATE, ME, "EXISTS");

    SAH_TRACEZ_INFO(ME, "%s: subscribe from %s:%u", queue->name, file, line);
    swl_eventQueue_callback_t* cb = calloc(1, sizeof(swl_eventQueue_callback_t));
    ASSERT_NOT_NULL(cb, SWL_RC_NOT_AVAILABLE, ME, "NULL");
    swl_llist_append(&queue->subscribers, &cb->it);
    cb->data = data;
    cb->callback = callback;
    cb->file = file;
    cb->line = line;

    return SWL_RC_OK;
}

swl_rc_ne swl_eventQueue_cancel(swl_eventQueue_t* queue _UNUSED, bs_eventQueue_cbf callback _UNUSED, void* data _UNUSED,
                                const char* file _UNUSED, uint32_t line _UNUSED) {
    swl_eventQueue_callback_t* handler = s_findHandler(queue, callback, data);
    ASSERT_NOT_NULL(handler, SWL_RC_INVALID_STATE, ME, "NOT FOUND");
    swl_llist_iterator_take(&handler->it);
    free(handler);

    return SWL_RC_OK;
}

swl_rc_ne swl_eventQueue_publish(swl_eventQueue_t* queue _UNUSED, const swl_event_t* event _UNUSED) {
    ASSERT_NOT_NULL(queue, SWL_RC_INVALID_PARAM, ME, "NULL");

    SWL_LLIST_FOR_EACH(swl_eventQueue_callback_t, it, subs, &queue->subscribers) {
        subs->callback(event, subs->data);
    } SWL_LLIST_DONE;
    return SWL_RC_OK;
}

char* swl_eventQueue_getName(swl_eventQueue_t* queue) {
    ASSERT_NOT_NULL(queue, NULL, ME, "NULL");

    return queue->name;
}

uint32_t swl_eventQueue_getNrHandlers(swl_eventQueue_t* queue) {
    ASSERT_NOT_NULL(queue, 0, ME, "NULL");

    return swl_llist_size(&queue->subscribers);
}



