/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_hash.h"
#include "swl/swl_hex.h"

#define ME "swlHash"

bool swl_hash_hexToMD5Hex(const char* frameStr, char* newMd5Hash, size_t newMd5HashLen, bool upperCase) {
    ASSERTS_NOT_NULL(frameStr, false, ME, "NULL");
    ASSERTS_NOT_NULL(newMd5Hash, false, ME, "NULL");
    ASSERT_NOT_EQUALS(newMd5HashLen, 0, false, ME, "Invalid buffer");
    size_t frameStrLen = strlen(frameStr);
    size_t frameByteLen = frameStrLen / 2;
    swl_bit8_t frameByte[frameByteLen];
    memset(frameByte, 0, frameByteLen);
    bool ret = swl_hex_toBytes((swl_bit8_t*) frameByte, frameByteLen, frameStr, frameStrLen);
    ASSERTS_TRUE(ret, false, ME, "Error in swl_hex_toBytes");
    swl_bit8_t md5Hash[MD5_DIGEST_LENGTH] = {'\0'};
    swl_rc_ne rc = swl_hash_rawToMD5(md5Hash, MD5_DIGEST_LENGTH, frameByte, frameByteLen);
    ASSERTS_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_hash_rawToMD5 %d", rc);
    return swl_hex_fromBytes(newMd5Hash, newMd5HashLen, md5Hash, MD5_DIGEST_LENGTH, upperCase);
}

/**
 * Convert a bytes buffer to a 16 bytes MD5 hash
 * Destination buffer must equals to 16 bytes
 */
swl_rc_ne swl_hash_rawToMD5(swl_bit8_t* md5Hash, size_t md5HashLen, const swl_bit8_t* rawFrame, size_t frameLen) {
    ASSERT_NOT_NULL(md5Hash, SWL_RC_ERROR, ME, "NULL");
    ASSERT_NOT_NULL(rawFrame, SWL_RC_ERROR, ME, "NULL");
    ASSERT_TRUE(md5HashLen >= MD5_DIGEST_LENGTH, SWL_RC_ERROR, ME, "Invalid md5HashLen %d", (int) md5HashLen);
    ASSERT_TRUE(frameLen != 0, SWL_RC_ERROR, ME, "Wrong frameLen %d", (int) frameLen);
    EVP_MD_CTX* mdctx;
    swl_rc_ne rc = SWL_RC_OK;

    mdctx = EVP_MD_CTX_new();
    if(mdctx == NULL) {
        rc = SWL_RC_ERROR;
        goto end;
    }
    if(EVP_DigestInit(mdctx, EVP_md5()) != 1) {
        rc = SWL_RC_ERROR;
        goto cleanup;
    }
    if(EVP_DigestUpdate(mdctx, rawFrame, frameLen) != 1) {
        rc = SWL_RC_ERROR;
        goto cleanup;
    }
    if(EVP_DigestFinal(mdctx, md5Hash, NULL) != 1) {
        rc = SWL_RC_ERROR;
        goto cleanup;
    }
cleanup:
    EVP_MD_CTX_free(mdctx);
end:
    return rc;
}
