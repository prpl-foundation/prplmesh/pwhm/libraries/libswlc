/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_string.h"
#include "swl/swl_hex.h"
#include "swl/swl_common_conversion.h"

#define ME "swlConv"

#define SWL_CONV_BUF_SIZE 128


/**
 * Convert a char string to a bit mask.
 * @param srcStr: the source string of which to create a mask. Note that if srcStr is NULL, then
 * 0 mask is returned.
 *
 * @param allUsed: written to, expressing whether all items in `srcStr` caused a bit
 *      of the bitmask to be set (setting twice counts as setting).
 *      Can be null.
 */
swl_mask_m swl_conv_charToMaskSep(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal,
                                  char separator, bool* allUsed) {

    ASSERT_NOT_NULL(srcEnumStrList, 0, ME, "NULL");
    ASSERT_NOT_EQUALS(separator, '\0', 0, ME, "Zero separator");
    ASSERTI_NOT_NULL(srcStr, 0, ME, "NULL");
    ASSERTI_NOT_EQUALS(srcStr[0], '\0', 0, ME, "Empty string");

    swl_mask_m mask = 0;
    uint32_t bufSize = strlen(srcStr) + 1;
    char buffer[bufSize];
    memcpy(buffer, srcStr, bufSize);
    char* needle = buffer;
    bool allUsedSoFar = true;

    swl_mask_m tmpMask = 0;
    do {
        char* sep = strchr(needle, separator);
        if(sep != NULL) {
            *sep = '\0';
        }
        tmpMask = swl_conv_charToEnum(needle, srcEnumStrList, maxVal, maxVal);
        if(tmpMask < maxVal) {
            mask |= (1 << tmpMask);
        } else {
            allUsedSoFar = false;
        }
        needle = (sep != NULL) ? sep + 1 : NULL;

    } while(needle != 0);
    if(allUsed != NULL) {
        *allUsed = allUsedSoFar;
    }
    if(mask == 0) {
        SAH_TRACEZ_INFO(ME, "Str without mask %s", srcStr);
    }
    return mask;
}

swl_mask_m swl_conv_charToMask(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal) {
    return swl_conv_charToMaskSep(srcStr, srcEnumStrList, maxVal, ',', NULL);
}

swl_enum_e swl_conv_charToEnum(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal, swl_enum_e defaultVal) {
    return swl_conv_charToEnum_ext(srcStr, srcEnumStrList, maxVal, defaultVal, NULL, 0, NULL);
}

/* Returns the first `index` in `srcEnumStrList` such that `srcEnumStrList[index]` equals `srcStr`.
 *
 * @param srcStr: the string to search in `srcEnumStrList`.
 * @param srcEnumStrList: array of strings in which this function searches.
 *   Its length can be given by maxVal, or it can be zero-terminated (whichever is shortest).
 * @param maxVal: only look for `maxVal` first elements in `srcEnumStrList`.
 * @param defaultVal: value to return when `srcStr` is not found.
 * @param errorFile: filename of caller, used to trace/log error when `srcStr` is not found.
 * @param line: line number of file of caller, used to trace/log error when `srcStr` is not found.
 *
 * @note: this function should not be used directly, but only through the SWL_CONV_CHAR_TO_ENUM* macro's
 * */
swl_enum_e swl_conv_charToEnum_ext(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal, swl_enum_e defaultVal, const char* errorFile _UNUSED, uint32_t line _UNUSED, bool* success) {
    ASSERT_NOT_NULL(srcEnumStrList, defaultVal, ME, "NULL (%s:%i)", errorFile, line);

    swl_enum_e i = 0;

    for(i = 0; i < maxVal && srcEnumStrList[i]; i++) {
        if(swl_str_matches(srcStr, srcEnumStrList[i])) {
            if(success != NULL) {
                *success = true;
            }
            return i;
        }
    }

    if(errorFile != NULL) {
        SAH_TRACEZ_ERROR(ME, "strToEnum : '%s' not found (%s:%i)", srcStr, errorFile, line);
    }
    if(success != NULL) {
        *success = false;
    }
    return defaultVal;
}

/*
 * @return True on success, false if buffer is too small or invalid arguments.
 */
bool swl_conv_maskToCharSep(char* tgtStr, uint32_t tgtStrSize, swl_mask_m srcMask, const char* const* srcEnumStrList, swl_enum_e maxVal, char separator) {
    ASSERT_NOT_NULL(srcEnumStrList, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_FALSE(tgtStrSize == 0, false, ME, "NULL");
    ASSERT_NOT_EQUALS(separator, '\0', false, ME, "Zero separator");
    uint32_t i = 0;
    uint32_t index = 0;
    int printed = 0;
    tgtStr[0] = '\0';

    for(i = 0; i < maxVal; i++) {
        if(srcMask & (1 << i)) {
            if(index) {
                printed = snprintf(&tgtStr[index], tgtStrSize - index, "%c%s", separator, srcEnumStrList[i]);
            } else {
                printed = snprintf(&tgtStr[index], tgtStrSize - index, "%s", srcEnumStrList[i]);
            }
            index += printed;

            ASSERT_TRUE(index < tgtStrSize, false, ME, "Buff too small");
        }
    }
    return true;
}

bool swl_conv_maskToChar(char* tgtStr, uint32_t tgtStrSize, swl_mask_m srcMask, const char* const* srcEnumStrList, swl_enum_e maxVal) {
    return swl_conv_maskToCharSep(tgtStr, tgtStrSize, srcMask, srcEnumStrList, maxVal, ',');
}

/**
 * Function used to get the uint8 values and provide comma seperated string list
 * @param srcData: uint8 type of elements
 * @param srcDataSize: Number of elements in srcData
 * @param tgtStrSize: buffer size of tgtStr
 * @param tgtStr: Output contains the comma seperated list
 */
bool swl_conv_uint8ArrayToChar(char* tgtStr, uint32_t tgtStrSize, uint8_t* srcData, size_t srcDataSize) {
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_NOT_NULL(srcData, false, ME, "NULL");
    uint32_t i = 0;
    uint32_t index = 0;
    int printed = 0;
    tgtStr[0] = '\0';
    for(i = 0; i < srcDataSize; i++) {
        if(index) {
            printed = snprintf(&tgtStr[index], tgtStrSize - index, ",%d", srcData[i]);
        } else {
            printed = snprintf(&tgtStr[index], tgtStrSize - index, "%d", srcData[i]);
        }
        index += printed;
        ASSERTS_TRUE(index < tgtStrSize, false, ME, "Buff too small");
    }
    return true;
}

/**
 * Convert a RCPI or ANPI report from range [0-255] in dBm according to
 * IEEE 802.11-2012 20.3.21.6 Received channel power indicator (RCPI) measurement
 */
int32_t swl_conv_refToDbm(uint8_t ref) {
    return (ref / 2) - 110;
}

/**
 * @brief Encodes a string according to URL encoding standards.
 * @param str The string to be encoded.
 * @return A newly allocated percent-encoded string.
 */
char* swl_conv_encodeAsUrl(const char* str) {
    ASSERTS_NOT_NULL(str, NULL, ME, "NULL");
    const char* hex = SWL_HEX_UPPER_LIST;
    size_t len = strlen(str);
    char* result = malloc(3 * len + 1);
    ASSERT_NOT_NULL(result, NULL, ME, "out of mem");

    size_t resIndex = 0;

    for(size_t i = 0; i < len; ++i) {
        if(isalnum((unsigned char) str[i]) || (strchr("-_.~", str[i]) != NULL)) {
            result[resIndex++] = str[i];
        } else if(str[i] == ' ') {
            result[resIndex++] = '+';
        } else {
            result[resIndex++] = '%';
            result[resIndex++] = hex[(unsigned char) str[i] >> 4];
            result[resIndex++] = hex[(unsigned char) str[i] & 15];
        }
    }
    result[resIndex] = '\0';
    SAH_TRACEZ_INFO(ME, "encoded string: '%s' -> '%s'", str, result);
    return result;
}

/**
 * @brief Converts a hexadecimal digit to its integer value.
 * This function takes a single character representing a hexadecimal digit
 * and returns its integer value.
 * @param ch The hexadecimal digit character.
 * @return The integer value of the hexadecimal digit, or -1 if the character is invalid.
 */
static int s_hexToInt(char ch) {
    if((ch >= '0') && (ch <= '9')) {
        return ch - '0';
    } else if((ch >= 'a') && (ch <= 'f')) {
        return ch - 'a' + 10;
    } else if((ch >= 'A') && (ch <= 'F')) {
        return ch - 'A' + 10;
    } else {
        return -1;
    }
}

/**
 * @brief Decodes string according to URL encoding standards.
 * @param str The percent-encoded string to be decoded.
 * @return A newly allocated decoded string.
 */
char* swl_conv_decodeAsUrl(const char* str) {
    ASSERTS_NOT_NULL(str, NULL, ME, "NULL");
    size_t len = strlen(str);
    char* result = malloc(len + 1);
    ASSERT_NOT_NULL(result, NULL, ME, "out of mem");
    size_t resIndex = 0;

    for(size_t i = 0; i < len; ++i) {
        if(str[i] == '+') {
            result[resIndex++] = ' ';
        } else if((str[i] == '%') && (i + 2 < len)) {
            int high = s_hexToInt(str[i + 1]);
            int low = s_hexToInt(str[i + 2]);
            if((high != -1) && (low != -1)) {
                result[resIndex++] = (high << 4) | low;
                i += 2;
            } else {
                result[resIndex++] = str[i];
            }
        } else {
            result[resIndex++] = str[i];
        }
    }
    result[resIndex] = '\0';
    SAH_TRACEZ_INFO(ME, "decoded string: '%s' -> '%s'", str, result);
    return result;
}
