/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlTrl"
#include "swl/swl_common_trilean.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common.h"

/**
 * \file a trilean is a value in three-state logic.
 *
 * States: false/Off/Disabled, true/On/Enabled, unknown/Auto.
 */

/**
 * Maps #swl_trl_format_e to #swl_trl_e to a string.
 *
 * For example, swl_trl_trl[SWL_TRL_FORMAT_UNKNOWN][SWL_TRL_TRUE] == "True".
 */
static const char* const mapping[][SWL_TRL_MAX] = {
    {"Off", "On", "Auto"},
    {"False", "True", "Unknown"},
    {"0", "1", "-1"},
    {"false", "true", "null"},
};

SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(mapping) == SWL_TRL_FORMAT_MAX,
                  "mapping needs to be defined for all formattings");


static const int32_t s_intMapping[SWL_TRL_MAX] = {0, 1, -1};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(s_intMapping) == SWL_TRL_MAX,
                  "s_intMapping has inproper number of members");

/**
 * Whether the given value is a valid trilean value.
 *
 * @return true if and only if `trl` is one of the defined values at #swl_trilean_e
 *   except #SWL_TRILEAN_MAX.
 */
bool swl_trl_isValid(swl_trl_e trl) {
    // enum is, depending on platform, sometimes signed and sometimes unsigned,
    // so avoid "comparison is always true" compiler errors on some platforms
    // with "trl >= 0".
    // Not all platforms have "#pragma GCC diagnostic".
    bool nonnegative = trl == 0 || trl > 0;

    return nonnegative && trl < SWL_TRL_MAX;
}

bool swl_trl_format_isValid(swl_trl_format_e format) {
    bool nonnegative = format == 0 || format > 0;
    return nonnegative && format < SWL_TRL_FORMAT_MAX;
}

/**
 * Converts from null-terminated string to #swl_trilean_e.
 *
 * @return true in case of success, false on failure:
 *   - NULL arg
 *   - string not one of expected values
 *   - invalid format.
 */
bool swl_trl_fromChar(swl_trl_e* tgtTrlValue, const char* srcCharValue, swl_trl_format_e format) {
    ASSERT_NOT_NULL(tgtTrlValue, false, ME, "NULL");
    ASSERT_NOT_NULL(srcCharValue, false, ME, "NULL");
    ASSERT_TRUE(swl_trl_format_isValid(format), false, ME, "Invalid format: %d", format);
    swl_trl_e trl = swl_conv_charToEnum(srcCharValue, mapping[format], SWL_TRL_MAX, SWL_TRL_MAX);
    ASSERTI_NOT_EQUALS(trl, SWL_TRL_MAX, false, ME, "Cannot convert to trilean: '%s'", srcCharValue);
    *tgtTrlValue = trl;
    return true;
}

/**
 * Converts from #swl_trilean_e to null-terminated string.
 *
 * @return true in case of success, false on failure:
 *   - NULL arg
 *   - not a trilean
 *   - invalid format
 */
bool swl_trl_toChar(const char** tgtCharValue, swl_trl_e srcTrlValue, swl_trl_format_e format) {
    ASSERT_NOT_NULL(tgtCharValue, false, ME, "NULL");
    ASSERTI_TRUE(swl_trl_isValid(srcTrlValue), false, ME, "Invalid trilean: %d", srcTrlValue);
    ASSERTI_TRUE(swl_trl_format_isValid(format), false, ME, "Invalid format: %d", format);
    *tgtCharValue = mapping[format][srcTrlValue];
    return true;
}

/**
 * Converts from boolean to #swl_trilean_e
 */
swl_trl_e swl_trl_fromBool(bool boolean) {
    return boolean ? SWL_TRL_TRUE : SWL_TRL_FALSE;
}

/**
 * Converts from #swl_trilean_e to boolean
 *
 * @param valueOnUnknown: value to return if given trilean is #SWL_TRL_UNKNOWN
 *
 * @return converted value or false if given trilean is not a trilean.
 */
bool swl_trl_toBool(swl_trl_e trl, bool valueOnUnknown) {
    ASSERTI_TRUE(swl_trl_isValid(trl), false, ME, "Invalid trilean: %d", trl);

    if(trl == SWL_TRL_TRUE) {
        return true;
    } else if(trl == SWL_TRL_FALSE) {
        return false;
    } else {
        return valueOnUnknown;
    }
}


/**
 * Converts from integer to #swl_trilean_e
 */
swl_trl_e swl_trl_fromInt(int32_t val) {
    if(val > 0) {
        return SWL_TRL_TRUE;
    }
    if(val == 0) {
        return SWL_TRL_FALSE;
    }
    return SWL_TRL_UNKNOWN;
}

/**
 * Converts from #swl_trilean_e to integer

 */
int32_t swl_trl_toInt(swl_trl_e trl) {
    ASSERTI_TRUE(swl_trl_isValid(trl), false, ME, "Invalid trilean: %d", trl);
    return s_intMapping[trl];
}

