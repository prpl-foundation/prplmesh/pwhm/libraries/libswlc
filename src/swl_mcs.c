/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <ctype.h>
#include "swl/swl_common_mcs.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_common.h"
#include "swl/swl_string.h"

#define ME "swlMcs"
#define MAX_LEGACY_ELEM 12

#define MCS_TOKEN_SEPARATORS 4
#define SEPCHAR '_'
#define MCS_TOKEN_LEN 32

const char* swl_mcs_consistencyIssue_str[SWL_MCS_CONSISTENCY_ISSUE_MCS_MAX] = {"MCS", "HT_MCS", "VHT_MCS", "HE_80_NSS", "HE_80_Ant", "HE_160_NSS", "HE_160_Ant", "HE_80x80_NSS", "HE_80x80_Ant"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_consistencyIssue_str) == SWL_MCS_CONSISTENCY_ISSUE_MCS_MAX, "swl_mcs_consistencyIssue_str not correctly defined");

const uint32_t swl_mcs_legacyList[SWL_MCS_LEGACY_LIST_SIZE] = {1, 2, 5, 6, 9, 11, 12, 18, 24, 36, 48, 54};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_legacyList) == SWL_MCS_LEGACY_LIST_SIZE, "swl_mcs_legacyList not correctly defined");
const char* swl_mcs_legacyStrList[SWL_MCS_LEGACY_LIST_SIZE] = {"1", "2", "5.5", "6", "9", "11", "12", "18", "24", "36", "48", "54"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_legacyStrList) == SWL_MCS_LEGACY_LIST_SIZE, "swl_mcs_legacyStrList not correctly defined");

// mcs holds the index into this array
const uint32_t swl_guardinterval_int_list[SWL_SGI_MAX] = { 0, 400, 800, 1600, 3200 };

const char* swl_guardint_str_list[SWL_SGI_MAX] = { "0", "400", "800", "1600", "3200" };
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_guardinterval_int_list) == SWL_SGI_MAX, "swl_guardinterval_int_list not correctly defined");

const char* swl_mcs_standard_str_list[SWL_MCS_STANDARD_MAX] = { "unk", "leg", "ht", "vht", "he", "eht" };
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_standard_str_list) == SWL_MCS_STANDARD_MAX, "swl_rate_standard_strings not correctly defined");

SWL_TYPE_ENUM_C(gtSwl_type_mcsStandard, swl_mcsStandard_e, SWL_MCS_STANDARD_UNKNOWN, SWL_MCS_STANDARD_MAX, swl_mcs_standard_str_list);



const swl_guardinterval_m swl_mcsStandard_supported_Sgi[] = {
    0,              // unknown
    M_SWL_SGI_AUTO, // legacy, etc
    M_SWL_SGI_400 | M_SWL_SGI_800,
    M_SWL_SGI_400 | M_SWL_SGI_800,
    M_SWL_SGI_800 | M_SWL_SGI_1600 | M_SWL_SGI_3200,
    M_SWL_SGI_800 | M_SWL_SGI_1600 | M_SWL_SGI_3200
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcsStandard_supported_Sgi) == (SWL_MCS_STANDARD_MAX), "swl_bandwidth_int_list not correctly defined");

#define M_SWL_COMMON_BW  M_SWL_BW_AUTO | M_SWL_BW_20MHZ | M_SWL_BW_40MHZ | M_SWL_BW_80MHZ | M_SWL_BW_160MHZ
const swl_bandwidth_m swl_mcsStandard_supported_Bw[] = {
    0,
    M_SWL_COMMON_BW,
    M_SWL_COMMON_BW,
    M_SWL_COMMON_BW,
    M_SWL_COMMON_BW | M_SWL_BW_10MHZ | M_SWL_BW_5MHZ | M_SWL_BW_2MHZ,
    M_SWL_COMMON_BW | M_SWL_BW_320MHZ | M_SWL_BW_10MHZ | M_SWL_BW_5MHZ | M_SWL_BW_2MHZ,
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcsStandard_supported_Bw) == (SWL_MCS_STANDARD_MAX), "swl_bandwidth_int_list not correctly defined");

const swl_radStd_e swl_mcsStandard_toRadStandard_map[] = {
    SWL_RADSTD_AUTO, // Unknown
    SWL_RADSTD_AUTO, // Legacy: must be refined using current frequency band
    SWL_RADSTD_N,    // HT
    SWL_RADSTD_AC,   // VHT
    SWL_RADSTD_AX,   // HE
    SWL_RADSTD_BE
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcsStandard_toRadStandard_map) == (SWL_MCS_STANDARD_MAX), "swl_mcsStandard_radStandard_maps not correctly defined");


const swl_mcsStandard_e swl_mcsStandard_fromRadStandard_map[] = {
    SWL_MCS_STANDARD_UNKNOWN, //      SWL_RADSTD_AUTO,
    SWL_MCS_STANDARD_LEGACY,  //      SWL_RADSTD_A,
    SWL_MCS_STANDARD_LEGACY,  //      SWL_RADSTD_B,
    SWL_MCS_STANDARD_LEGACY,  //      SWL_RADSTD_G,
    SWL_MCS_STANDARD_HT,      //      SWL_RADSTD_N,
    SWL_MCS_STANDARD_VHT,     //      SWL_RADSTD_AC,
    SWL_MCS_STANDARD_HE,      //      SWL_RADSTD_AX,
    SWL_MCS_STANDARD_EHT      //      SWL_RADSTD_BE
};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcsStandard_fromRadStandard_map) == (SWL_RADSTD_MAX), "swl_mcsStandard_radStandard_maps not correctly defined");


/**
 * @param intLegacy: Integer value of legacy rate in MHz
 * @return index of legacy rate
 */
int32_t swl_mcs_intToLegacy(uint32_t intLegacy) {
    for(int i = 0; i < SWL_MCS_LEGACY_LIST_SIZE; i++) {
        if(swl_mcs_legacyList[i] == intLegacy) {
            return i;
        }
    }
    return -1;
}

/**
 * Cleans mcs info
 * Mcs info are reseted to respective default value (SWL_XXX_AUTO enum)
 * @param mcs: pointer on the swl_mcs_t to reset
 */
void swl_mcs_clean(swl_mcs_t* mcs) {
    mcs->mcsIndex = 0;
    mcs->bandwidth = SWL_BW_AUTO;
    mcs->guardInterval = SWL_SGI_AUTO;
    mcs->standard = SWL_MCS_STANDARD_UNKNOWN;
    mcs->numberOfSpatialStream = 0;
}

/**
 * Checks the validity of the passed mcs structure's bandwidth and interval tags.
 * Invalid values are reset to the respective SWL_XXX_AUTO enum.
 * @param mcs: pointer on the swl_mcs_t to check
 * @return: false if corrected structure, otherwise true
 */
bool swl_mcs_checkMcsIndexes(swl_mcs_t* mcs) {

    bool result = true;

    if(mcs->standard >= SWL_MCS_STANDARD_MAX) {
        SAH_TRACEZ_ERROR(ME, "resetting bad standard");
        mcs->standard = SWL_MCS_STANDARD_UNKNOWN;
    }

    if(mcs->standard == SWL_MCS_STANDARD_UNKNOWN) {
        return mcs->bandwidth == SWL_BW_AUTO && mcs->guardInterval == SWL_SGI_AUTO
               && mcs->numberOfSpatialStream == 0 && mcs->mcsIndex == 0;
    }


    // this should never happen...
    if(mcs->bandwidth >= SWL_BW_MAX) {
        SAH_TRACEZ_ERROR(ME, "resetting bad bandwidth");
        mcs->bandwidth = SWL_BW_AUTO;
        result = false;
    }

    // neither this...
    if(mcs->guardInterval >= SWL_SGI_MAX) {
        SAH_TRACEZ_ERROR(ME, "resetting bad sgi");
        mcs->guardInterval = SWL_SGI_AUTO;
        result = false;
    }

    swl_bandwidth_m bwMask = swl_mcsStandard_supported_Bw[mcs->standard];
    swl_guardinterval_m sgiMask = swl_mcsStandard_supported_Sgi[mcs->standard];

    if((mcs->bandwidth != SWL_BW_AUTO) && (SWL_BIT_IS_SET(bwMask, mcs->bandwidth) == false)) {
        SAH_TRACEZ_ERROR(ME, "bad bw: %u/%u, reset", mcs->bandwidth, mcs->standard);
        mcs->bandwidth = SWL_BW_AUTO;
        result = false;
    }

    if((mcs->guardInterval != SWL_SGI_AUTO) && (SWL_BIT_IS_SET(sgiMask, mcs->guardInterval) == false)) {
        SAH_TRACEZ_ERROR(ME, "bad sgi: %u / %u, reset", mcs->guardInterval, mcs->standard);
        mcs->guardInterval = SWL_SGI_AUTO;
        result = false;
    }

    return result;
}

/**
 * Checks the passed serialization string's format.
 * The string is a unique '_' charcter separated list of tokens.
 * The first token is alphanumeric, the following ones hold digits only.
 * @param str: pointer on the string to check
 * @return: true or false
 */
bool swl_mcs_checkCharSerialized(const char* str) {

    ASSERTS_NOT_NULL(str, false, ME, "NULL");

    bool success = true;
    char* ptr = (char*) str;
    size_t len = strlen(ptr);
    char endchar = str[len - 1];

    ASSERT_TRUE((len < MCS_TOKEN_LEN ), false, ME, " too long string ");  // Should be enough
    ASSERT_TRUE(( swl_str_countChar(str, SEPCHAR) == MCS_TOKEN_SEPARATORS), false, ME, " too many separators ");
    ASSERT_FALSE((((*str) == SEPCHAR) || (endchar == SEPCHAR)), false, ME, " leading/ending separator ");

    ptr = strchr(ptr, SEPCHAR);
    ASSERT_NOT_NULL(ptr, false, ME, " no separator ");
    ptr++;
    while((*ptr) != 0) {
        if(((ptr[0] == SEPCHAR) && (ptr[1] == SEPCHAR)) || ((isdigit(ptr[0]) == false) && (ptr[0] != SEPCHAR))) {
            success = false;
        }
        ptr++;
    }
    ASSERT_TRUE((success), false, ME, " bad characters ");
    return success;
}

/**
 * Extracts the '_' seperated tokens from the serialized string, and writes the values in the swl_mcs_t structure.
 * The string format is "standard_mcs_bw_antenna_legacy"; standard is composed of alpha, the others of numeric characters.
 * The token's values are checked against the possible ones.
 * @param targetMcs: address of the swl_mcs_t structure to populate
 * @param mcsString: the serialized input string
 * @return: true or false upon success
 */
static bool s_mcs_fromChar_cb(swl_type_t* type _UNUSED, swl_mcs_t* tgtData, const char* srcStr) {
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERT_TRUE((strlen(srcStr) < MCS_TOKEN_LEN), false, ME, " small MCS_TOKEN_LEN ");
    ASSERT_TRUE((swl_mcs_checkCharSerialized(srcStr) == true), false, ME, " bad input string ");

    char* from = (char*) srcStr;
    bool result = false;

    uint32_t usCount = swl_str_nrCharOccurances('_', from, strlen(from), false);
    ASSERTS_NOT_EQUALS(usCount, 0, false, ME, "No underscore found");

    size_t bw = 0;
    swl_mcsStandard_e standard = SWL_MCS_STANDARD_UNKNOWN;
    size_t mindex = 0;
    size_t sgi = 0;

    char buffer[32] = {'\0'};
    size_t bufferLen = sizeof(buffer);

    swl_rc_ne rc = swl_strlst_copyStringAtIndexToBuffer(buffer, &bufferLen, from, "_", 0, true);
    ASSERT_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_strlst_copyStringAtIndexFromBuffer");
    standard = SWL_CONV_CHAR_TO_ENUM_RET(buffer, swl_mcs_standard_str_list,
                                         SWL_ARRAY_SIZE(swl_mcs_standard_str_list), SWL_MCS_STANDARD_UNKNOWN, &result);
    ASSERT_TRUE(result, false, ME, " unknown standard");

    rc = swl_strlst_copyStringAtIndexToBuffer(buffer, &bufferLen, from, "_", 1, true);
    ASSERT_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_strlst_copyStringAtIndexFromBuffer");
    mindex = atoi(buffer);

    rc = swl_strlst_copyStringAtIndexToBuffer(buffer, &bufferLen, from, "_", 2, true);
    ASSERT_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_strlst_copyStringAtIndexFromBuffer");
    bw = SWL_CONV_CHAR_TO_ENUM_RET(buffer, swl_bandwidth_intStr, SWL_BW_MAX, SWL_BW_AUTO, &result);
    ASSERT_TRUE(result, false, ME, " unknown bandwidth");

    rc = swl_strlst_copyStringAtIndexToBuffer(buffer, &bufferLen, from, "_", 4, true);
    ASSERT_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_strlst_copyStringAtIndexFromBuffer");
    sgi = SWL_CONV_CHAR_TO_ENUM_RET(buffer, swl_guardint_str_list,
                                    SWL_ARRAY_SIZE(swl_guardint_str_list), SWL_SGI_AUTO, &result);
    ASSERT_TRUE(result, false, ME, " unknown guardinterval");

    tgtData->standard = standard;
    tgtData->mcsIndex = mindex;
    tgtData->bandwidth = bw;
    rc = swl_strlst_copyStringAtIndexToBuffer(buffer, &bufferLen, from, "_", 3, true);
    ASSERT_EQUALS(rc, SWL_RC_OK, false, ME, "Error in swl_strlst_copyStringAtIndexFromBuffer");
    tgtData->numberOfSpatialStream = atoi(buffer);
    tgtData->guardInterval = sgi;

    return swl_mcs_checkMcsIndexes(tgtData);  // tgtData's bwindex and guardinterval may be overwritten
}

/**
 * Public interface to s_mcs_fromChar_cb
 * @param targetMcs: address of the swl_mcs_t structure
 * @param mcsString: the serial string
 * @return: true or false upon success
 */
bool swl_mcs_fromChar(swl_mcs_t* targetMcs, char* mcsString) {
    return s_mcs_fromChar_cb(swl_type_mcs, targetMcs, mcsString);
}

/**
 * Serializes the passed swl_mcs_t structure into the tgtStr buffer.
 * @param mcs: swl_mcs_t structure to serialize.
 * @param targetSize: size of the destination string
 * @param tgtStr: destination string
 * String format: "standard_mcsindex_bandwidth_antenna_sgi".
 * If standard is unknown, "unk_0_0_0_0" is written in tgtStr.
 * Bandwith and guardinterval values read from the string are checked/reset to 0.
 * @return: the length of the written string (snprintf).
 */
static ssize_t s_mcs_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t targetSize, swl_mcs_t* mcs) {
    ASSERT_NOT_NULL(mcs, -EINVAL, ME, "swl_mcs_t is NULL");
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "target string is NULL");
    ASSERT_FALSE((mcs->standard >= SWL_MCS_STANDARD_MAX), false, ME, " bad standard");

    if(mcs->standard == SWL_MCS_STANDARD_UNKNOWN) {
        char buffer[128];
        memset(buffer, 0, sizeof(buffer));
        if(swl_strlst_catFormat(buffer, sizeof(buffer), "", "%s_%u_%u_%u_%u", swl_mcs_standard_str_list[SWL_MCS_STANDARD_UNKNOWN], 0, 0, 0, 0) == false) {
            SAH_TRACEZ_ERROR(ME, "insufficient destsize");
            return -ENOTRECOVERABLE;
        }
        return snprintf(tgtStr, targetSize, "%s", buffer);
    }

    memset(tgtStr, 0, targetSize);
    swl_mcs_checkMcsIndexes(mcs); // MCS MAY be overwritten (bw and sgi)!
    uint32_t bw = swl_bandwidth_int[mcs->bandwidth];
    uint32_t interval = swl_guardinterval_int_list[mcs->guardInterval];

    return snprintf(tgtStr, targetSize, "%s_%u_%u_%u_%u",
                    swl_mcs_standard_str_list[mcs->standard], mcs->mcsIndex, bw, mcs->numberOfSpatialStream, interval);

}

size_t swl_mcs_toChar(char* tgtStr, size_t targetSize, swl_mcs_t* mcs) {
    return s_mcs_toChar_cb(swl_type_mcs, tgtStr, targetSize, mcs);
}

swl_typeFun_t swl_type_mcs_fun = {
    .name = "swl_mcs",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_VALUE,
    .toChar = (swl_type_toChar_cb) s_mcs_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_mcs_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};

swl_type_t gtSwl_type_mcs = {
    .typeFun = &swl_type_mcs_fun,
    .size = sizeof(swl_mcs_t)
};

/* Nsd number is the number of data subcarrier related to the bandwidth */
const uint32_t swl_mcs_nsd[SWL_BW_MAX] = {0, 52, 108, 234, 468};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_nsd) == SWL_BW_MAX, "swl_mcs_nsd not correctly defined");
const uint32_t swl_mcs_he_nsd[SWL_BW_MAX] = {0, 234, 468, 980, 1960};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_he_nsd) == SWL_BW_MAX, "swl_mcs_he_nsd not correctly defined");

const double swl_mcs_symbol = 3.2e-6;
const double swl_mcs_he_symbol = 12.8e-6;
const double swl_mcs_sgi[SWL_SGI_MAX] = {0, 400e-9, 800e-9, 1600e-9, 3200e-9};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_mcs_sgi) == SWL_SGI_MAX, "swl_mcs_nsd not correctly defined");

const swl_mcs_rateInfo_t swl_mcs_info[SWL_MCS_MAX] = {
    { 1, 1, 2 },  /* MCS  0: MOD: BPSK,   CR 1/2 */
    { 2, 1, 2 },  /* MCS  1: MOD: QPSK,   CR 1/2 */
    { 2, 3, 4 },  /* MCS  2: MOD: QPSK,   CR 3/4 */
    { 4, 1, 2 },  /* MCS  3: MOD: 16QAM,  CR 1/2 */
    { 4, 3, 4 },  /* MCS  4: MOD: 16QAM,  CR 3/4 */
    { 6, 2, 3 },  /* MCS  5: MOD: 64QAM,  CR 2/3 */
    { 6, 3, 4 },  /* MCS  6: MOD: 64QAM,  CR 3/4 */
    { 6, 5, 6 },  /* MCS  7: MOD: 64QAM,  CR 5/6 */
    { 8, 3, 4 },  /* MCS  8: MOD: 256QAM, CR 3/4 */
    { 8, 5, 6 },  /* MCS  9: MOD: 256QAM, CR 5/6 */
    { 10, 3, 4 }, /* MCS 10: MOD: 1024QAM, CR 3/4 */
    { 10, 5, 6 }, /* MCS 11: MOD: 1024QAM, CR 5/6 */
    { 12, 3, 4 }, /* MCS 12: MOD: 4096QAM, CR 3/4 */
    { 12, 5, 6 }, /* MCS 13: MOD: 4096QAM, CR 5/6 */
    { 1, 1, 2 },  /* MCS 14: MOD: BPSK-DCM-DUP, CR 5/6 */
    { 1, 1, 2 }  /* MCS 15: MOD: BPSK-DCM, CR 5/6 */
};

/**
 * Calculate the rate from mcs informations
 * The rate depends on: mcs value, bandwidth,
 * number of spatial streams, guard interval
 * and also a coding rate (done in the swl_mcs_info table)
 */
uint32_t swl_mcs_toRate(swl_mcs_t* mcs) {
    ASSERT_NOT_NULL(mcs, 0, ME, "NULL");
    ASSERT_TRUE(mcs->mcsIndex < SWL_MCS_MAX, 0, ME, "MCS Index is not correct");
    /* If mcs->bandwidth = BW_AUTO, rate is 0 */
    uint32_t rate = 0;
    double timePerSymbol = 0.0;

    if((mcs->standard == SWL_MCS_STANDARD_HE) || (mcs->standard == SWL_MCS_STANDARD_EHT)) {
        timePerSymbol = swl_mcs_he_symbol;
        rate = swl_mcs_he_nsd[mcs->bandwidth];
    } else {
        timePerSymbol = swl_mcs_symbol;
        rate = swl_mcs_nsd[mcs->bandwidth];
    }

    rate = rate * swl_mcs_info[mcs->mcsIndex].modulation;
    rate = rate * mcs->numberOfSpatialStream;
    rate = (rate * swl_mcs_info[mcs->mcsIndex].quotient) / swl_mcs_info[mcs->mcsIndex].divider;
    rate = (uint32_t) ((double) rate / (swl_mcs_sgi[mcs->guardInterval] + timePerSymbol) / 1000.0);

    if(mcs->mcsIndex >= 14) {
        // rate uses double modulation for redundancy. Throughput halved
        rate = rate / 2;
        if(mcs->mcsIndex == 14) {
            // Extra duplicates, rate halved again
            rate = rate / 2;
        }
    }

    return rate;
}

/*
 * @brief get guard interval duration (in us) from enum value
 *
 * @param gi guard internal enumeration
 *
 * @return guard interval duration in ms if successful
 *         otherwise 0 (stands for gi SWL_SGI_AUTO)
 */
uint32_t swl_mcs_guardIntervalToInt(swl_guardinterval_e gi) {
    ASSERTS_TRUE(gi < SWL_SGI_MAX, 0, ME, "unknown");
    return swl_guardinterval_int_list[gi];
}

/*
 * @brief return radio standard value based on mcs standard, when possible
 * otherwise use frequency band to get legacy value
 * This function also checks validity of mcs standard against frequency band
 *
 * @param mcsStd MCS standard (ht/vht/he)
 * @param freqBandExt frequency band used for legacy case
 *
 * @return radio standard when conversion is successful
 *         SWL_RADSTD_AUTO otherwise
 */
swl_radStd_e swl_mcs_radStdFromMcsStd(swl_mcsStandard_e mcsStd, swl_freqBandExt_e freqBandExt) {
    swl_radStd_e radStd = SWL_RADSTD_AUTO;
    swl_freqBand_e freqBand = swl_chanspec_freqBandExtToFreqBand(freqBandExt, SWL_FREQ_BAND_MAX, NULL);
    if(mcsStd < SWL_MCS_STANDARD_MAX) {
        radStd = swl_mcsStandard_toRadStandard_map[mcsStd];
    }
    if(freqBand < SWL_FREQ_BAND_MAX) {
        swl_radStd_e legacyRadStd = swl_freqBand_legacyRadStd[freqBand];
        ASSERTS_TRUE(SWL_BIT_IS_SET(swl_freqBand_radStd[freqBand], radStd), legacyRadStd, ME, "fall back to legacy radStd");
    }
    return radStd;
}

/**
 * @brief return the mcs standard that matches the provded radio standard
 */
swl_mcsStandard_e swl_mcs_mcsStdFromRadStd(swl_radStd_e radStd) {
    if(radStd >= SWL_RADSTD_MAX) {
        return SWL_MCS_STANDARD_UNKNOWN;
    }
    return swl_mcsStandard_fromRadStandard_map[radStd];

}

/**
 * Return whether the provided mcs standard and radio standard match.
 *
 * @return true if mcsStd matches the result of swl_mcs_mcsStdFromRadStd of the provided radStd, false otherwise.
 */
bool swl_mcs_matchesRadStandard(swl_mcsStandard_e mcsStd, swl_radStd_e radStd) {
    return swl_mcs_mcsStdFromRadStd(radStd) == mcsStd;
}

