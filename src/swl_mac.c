/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "swl/swl_common.h"
#include "swl/swl_hex.h"
#include "swl/swl_common_mac.h"
#include "swl/types/swl_refType.h"

#include "swl/swl_string.h"

#define ME "swlMac"
#define MAC_ADDR_STR_DEFAULT_SEPARATOR ':'


static bool s_binMac_fromChar_cb(swl_type_t* type _UNUSED, swl_macBin_t* tgtData, const char* srcStr) {
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    swl_macBin_t* tgtBMAC = (swl_macBin_t*) tgtData;
    memcpy(tgtBMAC, &g_swl_macBin_null, sizeof(swl_macBin_t));
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERTS_NOT_EQUALS(srcStr[0], 0, false, ME, "Empty");
    size_t srcStrSize = strlen(srcStr);
    ASSERT_TRUE(srcStrSize < SWL_MAC_CHAR_LEN, false, ME, "mac str too long");
    size_t lenByteWritten = 0;
    char separator;
    //get first available separator in mac str
    swl_hex_findValidSepChar(&separator, srcStr, SWL_MIN((size_t) 3, srcStrSize));
    bool ret = swl_hex_toBytesSep(tgtBMAC->bMac, SWL_MAC_BIN_LEN, srcStr, srcStrSize, separator, &lenByteWritten);
    ASSERTS_TRUE(ret, ret, ME, "fail to parse mac str [%s]", srcStr);
    return (lenByteWritten == SWL_MAC_BIN_LEN);
}

static ssize_t s_binMac_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_macBin_t* srcData) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    tgtStr[0] = 0;
    swl_macBin_t* srcBMac = (swl_macBin_t*) srcData;
    size_t lenWritten = 0;
    swl_hex_fromBytesSep(tgtStr, tgtStrSize, srcBMac->bMac, SWL_MAC_BIN_LEN, true, MAC_ADDR_STR_DEFAULT_SEPARATOR, &lenWritten);
    return SWL_MAC_CHAR_LEN - 1;
}

/**
 * Convert a char string mac, to a binary mac.
 * Returns false if either one NULL, or the character mac does not contain a hex format, true otherwise.
 */
bool swl_mac_charToBin(swl_macBin_t* tgtBMac, const swl_macChar_t* srcCMac) {
    ASSERT_NOT_NULL(tgtBMac, false, ME, "NULL");
    ASSERT_NOT_NULL(srcCMac, false, ME, "NULL");

    return s_binMac_fromChar_cb(swl_type_macBin, tgtBMac, (char*) srcCMac);
}

/**
 * Convert a binary mac to a char string mac, with user options for uppercase hex digits and separator char.
 * Returns false if either element is NULL, true otherwise.
 */
bool swl_mac_binToCharSep(swl_macChar_t* tgtCMac, const swl_macBin_t* srcBMac, bool upperCase, char separator) {
    ASSERT_NOT_NULL(srcBMac, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtCMac, false, ME, "NULL");
    return swl_hex_fromBytesSep(tgtCMac->cMac, SWL_MAC_CHAR_LEN, srcBMac->bMac, SWL_MAC_BIN_LEN, upperCase, separator, NULL);
}

/**
 * Convert a binary mac to a char string mac.
 * Returns false if either element is NULL, true otherwise.
 */
bool swl_mac_binToChar(swl_macChar_t* tgtCMac, const swl_macBin_t* srcBMac) {
    ASSERT_NOT_NULL(srcBMac, false, ME, "NULL");
    ASSERT_NOT_NULL(tgtCMac, false, ME, "NULL");
    return swl_mac_binToCharSep(tgtCMac, srcBMac, true, MAC_ADDR_STR_DEFAULT_SEPARATOR);
}

/**
 * Check whether two binary macs are equal, so  when their memory is equal
 */
bool swl_mac_binMatches(const swl_macBin_t* bMac1, const swl_macBin_t* bMac2) {
    return (memcmp(bMac1->bMac, bMac2->bMac, SWL_MAC_BIN_LEN) == 0);
}

/**
 * Check whether two char string macs are equal, while ignoring case and byte separator chars
 */
bool swl_mac_charMatches(const swl_macChar_t* cMac1, const swl_macChar_t* cMac2) {
    swl_macBin_t bMac1;
    swl_macBin_t bMac2;
    return (swl_mac_charToBin(&bMac1, cMac1) && swl_mac_charToBin(&bMac2, cMac2) && swl_mac_binMatches(&bMac1, &bMac2));
}


/**
 * Convert any valid string mac into standard format "XX:XX:XX:XX:XX:XX"
 */
bool swl_mac_charToStandard(swl_macChar_t* cMac, const char* srcStr) {
    ASSERT_NOT_NULL(cMac, false, ME, "NULL");
    swl_macBin_t bMac;
    ASSERT_TRUE(s_binMac_fromChar_cb(swl_type_macBin, &bMac, srcStr), false, ME, "INVALID");
    s_binMac_toChar_cb(swl_type_macBin, cMac->cMac, SWL_MAC_CHAR_LEN, &bMac);
    return true;
}

/**
 * Check whether the provided char string mac matches format "XX:XX:XX:XX:XX:XX"
 * - colon char separator
 * - two hex digits per byte
 * - capital hex digits
 */
bool swl_mac_charIsStandard(const swl_macChar_t* cMac) {
    ASSERT_NOT_NULL(cMac, false, ME, "NULL");
    swl_macChar_t cMacStd;
    cMacStd.cMac[0] = 0;
    ASSERTS_TRUE(swl_mac_charToStandard(&cMacStd, cMac->cMac), false, ME, "INVALID");
    return (strncmp(cMacStd.cMac, cMac->cMac, SWL_MAC_CHAR_LEN) == 0);
}

/**
 * Add a value to a binary mac, if it were considered a number. The addition
 * shall only touch the last nrMaskBits bits, starting to count from lowest bit of byte SWL_MAC_BIN_LEN - 1
 **/
bool swl_mac_binAddVal(swl_macBin_t* bMac, int64_t value, uint32_t nrMaskBits) {
    ASSERT_NOT_NULL(bMac, false, ME, "NULL");
    for(int32_t i = SWL_MAC_BIN_LEN - 1; i >= 0 && value != 0 && nrMaskBits != 0; i--) {
        int64_t tmpVal = value + bMac->bMac[i];
        uint32_t nrBits = SWL_MIN(nrMaskBits, (uint8_t) SWL_BIT_IN_BYTE);
        uint32_t setMask = (1 << nrBits) - 1;
        uint32_t oldMask = ~setMask;

        uint8_t setVal = (tmpVal & setMask);
        value = tmpVal >> nrBits;
        nrMaskBits -= nrBits;
        bMac->bMac[i] = setVal + (bMac->bMac[i] & oldMask);
    }
    return true;
}

/**
 * Add a value to a string mac, if it were considered a number. The addition
 * shall only touch the last nrMaskBits bits, starting to count from lowest bit of byte SWL_MAC_BIN_LEN - 1
 */
bool swl_mac_charAddVal(swl_macChar_t* cMac, int64_t value, uint32_t nrMaskBits) {
    ASSERT_NOT_NULL(cMac, false, ME, "NULL");
    swl_macBin_t binMac;
    memset(&binMac, 0, sizeof(swl_macBin_t));
    ASSERT_TRUE(swl_mac_charToBin(&binMac, cMac), false, ME, "INVALID");
    bool ret = swl_mac_binAddVal(&binMac, value, nrMaskBits);
    ASSERT_TRUE(swl_mac_binToChar(cMac, &binMac), false, ME, "INVALID");
    return ret;
}

bool s_doBinCheckOfCharMac(bool (* bmacCheck)(const swl_macBin_t* bMac), const swl_macChar_t* cmac) {
    ASSERT_NOT_NULL(cmac, false, ME, "NULL");
    swl_macBin_t binMac = SWL_MAC_BIN_NEW();

    ASSERT_TRUE(swl_mac_charToBin(&binMac, cmac), false, ME, "INVALID");
    return bmacCheck(&binMac);
}

/**
 * Check whether the provided bin mac is a local mac address
 */
bool swl_mac_binIsLocal(const swl_macBin_t* bMac) {
    ASSERT_NOT_NULL(bMac, false, ME, "NULL");
    return (bMac->bMac[0] & 0x02) != 0;
}

/**
 * Check whether the provided char string mac is a local mac address
 */
bool swl_mac_charIsLocal(const swl_macChar_t* cMac) {
    return s_doBinCheckOfCharMac(swl_mac_binIsLocal, cMac);
}


/**
 * Check whether the provided bin mac is the broadcast mac address.
 */
bool swl_mac_binIsBroadcast(const swl_macBin_t* bMac) {
    ASSERT_NOT_NULL(bMac, false, ME, "NULL");
    return swl_mac_binMatches(bMac, &g_swl_macBin_bCast);
}

/**
 * Check whether the provided char string mac is the broadcast mac address.
 */
bool swl_mac_charIsBroadcast(const swl_macChar_t* cMac) {
    return s_doBinCheckOfCharMac(swl_mac_binIsBroadcast, cMac);
}

/**
 * Check whether the provided char string mac is a valid mac address.
 * Not empty, not null and not a broadcast address.
 */
bool swl_mac_charIsValidStaMac(const swl_macChar_t* cMac) {
    ASSERT_NOT_NULL(cMac, false, ME, "NULL");
    return (!swl_str_isEmpty(cMac->cMac) && !swl_mac_charIsNull(cMac) && !swl_mac_charIsBroadcast(cMac));
}

/**
 * Check whether the provided bin mac is the null (i.e. all zero) address.
 * Note that bMac == NULL will return false.
 */
bool swl_mac_binIsNull(const swl_macBin_t* bMac) {
    ASSERT_NOT_NULL(bMac, false, ME, "NULL");
    return swl_mac_binMatches(bMac, &g_swl_macBin_null);
}

/**
 * Clear the binary mac, setting it to 00:00:00:00:00:00
 */
void swl_mac_binClear(swl_macBin_t* bMac) {
    memcpy(bMac, &g_swl_macBin_null, sizeof(swl_macBin_t));
}

/**
 * Check whether the provided char string mac is the null (i.e. all zero) address.
 * Note that cMac == NULL will return false.
 */
bool swl_mac_charIsNull(const swl_macChar_t* cMac) {
    return s_doBinCheckOfCharMac(swl_mac_binIsNull, cMac);
}

/**
 * Clear the char mac, setting it to 00:00:00:00:00:00
 */
void swl_mac_charClear(swl_macChar_t* cMac) {
    memcpy(cMac, &g_swl_macChar_null, sizeof(swl_macChar_t));
}

#define NR_MULTICAST_MAC 6

swl_macMask_t s_multicastMaskList[NR_MULTICAST_MAC] =
{
    {.mac = {{0x01, 0x00, 0x5e, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0xff, 0x00, 0x00, 0x00}} },
    {.mac = {{0x33, 0x33, 0x00, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0x00, 0x00, 0x00, 0x00}} },
    {.mac = {{0x01, 0x80, 0xc2, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0xff, 0x00, 0x00, 0x00}} },
    {.mac = {{0x01, 0x1b, 0x19, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0xff, 0x00, 0x00, 0x00}} },
    {.mac = {{0x01, 0x0c, 0xcd, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0xff, 0x00, 0x00, 0x00}} },
    {.mac = {{0x01, 0x00, 0x0c, 0x00, 0x00, 0x00}}, .mask = {{0xff, 0xff, 0xff, 0x00, 0x00, 0x00}} },
};

/**
 * Check whether the provided bin mac is a multicast mac address.
 */
bool swl_mac_binIsMulticast(const swl_macBin_t* bMac) {
    ASSERT_NOT_NULL(bMac, false, ME, "NULL");
    for(uint8_t i = 0; i < NR_MULTICAST_MAC; i++) {
        if(swl_macMask_binMatches(&s_multicastMaskList[i], bMac)) {
            return true;
        }
    }
    return false;
}

/**
 * Check whether the provided char string mac is a multicast mac address.
 */
bool swl_mac_charIsMulticast(const swl_macChar_t* cMac) {
    return s_doBinCheckOfCharMac(swl_mac_binIsMulticast, cMac);
}

bool swl_mac_binMatchesMask(const swl_macBin_t* mac1, const swl_macBin_t* mac2, const swl_macBin_t* mask) {
    ASSERT_NOT_NULL(mac1, false, ME, "NULL");
    ASSERT_NOT_NULL(mac2, false, ME, "NULL");
    ASSERT_NOT_NULL(mask, false, ME, "NULL");

    for(uint8_t i = 0; i < SWL_MAC_BIN_LEN; i++) {
        if((mac1->bMac[i] & mask->bMac[i]) != (mac2->bMac[i] & mask->bMac[i])) {
            return false;
        }
    }
    return true;
}

bool swl_mac_charMatchesMask(const swl_macChar_t* mac1, const swl_macChar_t* mac2, const swl_macChar_t* mask) {
    ASSERT_NOT_NULL(mac1, false, ME, "NULL");
    ASSERT_NOT_NULL(mac2, false, ME, "NULL");
    ASSERT_NOT_NULL(mask, false, ME, "NULL");

    swl_macBin_t binMask;
    swl_macBin_t binMac1;
    swl_macBin_t binMac2;
    ASSERT_TRUE(swl_mac_charToBin(&binMask, mask), false, ME, "INVALID");
    ASSERT_TRUE(swl_mac_charToBin(&binMac1, mac1), false, ME, "INVALID");
    ASSERT_TRUE(swl_mac_charToBin(&binMac2, mac2), false, ME, "INVALID");

    return swl_mac_binMatchesMask(&binMac1, &binMac2, &binMask);
}

/**
 * Check if a given binary mac matches the mac mask.
 */
bool swl_macMask_binMatches(const swl_macMask_t* mask, const swl_macBin_t* bMac) {
    return swl_mac_binMatchesMask(bMac, &mask->mac, &mask->mask);
}

/**
 * Check if a given char mac matches the mac mask.
 */
bool swl_macMask_charMatches(const swl_macMask_t* mask, const swl_macChar_t* cMac) {
    ASSERT_NOT_NULL(cMac, false, ME, "NULL");
    swl_macBin_t binMac = SWL_MAC_BIN_NEW();

    ASSERT_TRUE(swl_mac_charToBin(&binMac, cMac), false, ME, "INVALID");
    return swl_macMask_binMatches(mask, &binMac);
}

swl_typeFun_t swl_type_macBin_fun = {
    .name = "swl_macBin",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_binMac_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_binMac_fromChar_cb,
    .equals = NULL,
    .copy = NULL
};
swl_type_t gtSwl_type_macBin = {
    .typeFun = &swl_type_macBin_fun,
    .size = SWL_MAC_BIN_LEN,
};
SWL_REF_TYPE_C(gtSwl_type_macBinPtr, gtSwl_type_macBin);

static bool s_charMac_fromChar_cb(swl_type_t* type _UNUSED, swl_macChar_t* tgtData, const char* srcStr) {
    ASSERT_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERT_NOT_NULL(srcStr, false, ME, "NULL");
    ASSERTS_NOT_EQUALS(srcStr[0], 0, false, ME, "Empty");
    size_t srcStrSize = strlen(srcStr);
    ASSERT_TRUE(srcStrSize < SWL_MAC_CHAR_LEN, false, ME, "mac str too long");
    return swl_mac_charToStandard(tgtData, srcStr);
}

static ssize_t s_charMac_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_macChar_t* srcData) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%s", srcData->cMac);
}

swl_typeFun_t swl_type_macChar_fun = {
    .name = "swl_macChar",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_charMac_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_charMac_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};
swl_type_t gtSwl_type_macChar = {
    .typeFun = &swl_type_macChar_fun,
    .size = SWL_MAC_CHAR_LEN,
};
SWL_REF_TYPE_C(gtSwl_type_macCharPtr, gtSwl_type_macChar);

