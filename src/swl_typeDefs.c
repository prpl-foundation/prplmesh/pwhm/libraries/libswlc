/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "swl/fileOps/swl_print.h"
#include "swl/types/swl_refType.h"
#include "swl/subtypes/swl_numberSType.h"
#include <inttypes.h> // for PRIi64 and PRIu64

#define ME "swlTDef"


static bool s_charToUInt64(const char* srcStr, uint64_t* data) {
    ASSERTS_NOT_NULL(data, false, ME, "NULL");
    ASSERT_FALSE(swl_str_isEmpty(srcStr), false, ME, "EMPTY");

    *data = strtoull(srcStr, NULL, 10);
    return true;
}

static bool s_charToInt64(const char* srcStr, int64_t* data) {
    ASSERTS_NOT_NULL(data, false, ME, "NULL");
    ASSERT_FALSE(swl_str_isEmpty(srcStr), false, ME, "EMPTY");
    *data = strtoll(srcStr, NULL, 10);
    return true;
}


static ssize_t s_bool_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    bool tgtBool = *(bool*) srcData;
    if(args->options & M_SWL_PRINT_BOOL_INT) {
        return snprintf(tgtStr, tgtStrSize, "%u", tgtBool);
    } else if(args->options & M_SWL_PRINT_BOOL_STR_LOWER) {
        return snprintf(tgtStr, tgtStrSize, "%s", tgtBool ? "true" : "false");
    } else {
        return snprintf(tgtStr, tgtStrSize, "%s", tgtBool ? "True" : "False");
    }


}
static bool s_bool_fromChar_cb(swl_type_t* type _UNUSED, bool* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    const swl_print_args_t* args = swl_print_getDefaultArgs();

    if(args->options & M_SWL_PRINT_BOOL_INT) {
        int64_t data;
        ASSERTS_TRUE(s_charToInt64(srcStr, &data), false, ME, "ERR");
        *tgtData = (int8_t) data;
        return true;
    } else {
        if(swl_str_matchesIgnoreCase(srcStr, "True")) {
            *tgtData = true;
        } else if(swl_str_matchesIgnoreCase(srcStr, "False")) {
            *tgtData = false;
        } else {
            return false;
        }
        return true;
    }
}

swl_typeFun_t swl_type_bool_fun = {
    .name = "swl_bool",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_BASIC,
    .toChar = (swl_type_toChar_cb) s_bool_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_bool_fromChar_cb,
    .equals = NULL,
    .copy = NULL,

};

swl_type_t gtSwl_type_bool = {
    .size = sizeof(bool),
    .typeFun = &swl_type_bool_fun,
};

SWL_REF_TYPE_C(gtSwl_type_boolPtr, gtSwl_type_bool);

/**
 * int8_t handler
 */
static ssize_t s_int8_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%d", *(int8_t*) srcData);
}
static bool s_int8_fromChar_cb(swl_type_t* type _UNUSED, int8_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    int64_t data;
    ASSERTS_TRUE(s_charToInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (int8_t) data;
    return true;
}

static int64_t s_int8_toInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    int8_t* myValue = (int8_t*) el;
    return (int64_t) *myValue;
}

static swl_rc_ne s_int8_fromInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, int64_t val) {
    int8_t* myValue = (int8_t*) el;
    *myValue = val;
    bool fits = (val <= INT8_MAX) && (val >= INT8_MIN);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_numberSTypeFun_t s_int8NumFun = {
    .isSigned = true,
    .nrBits = 8,
    .toInt64 = s_int8_toInt64,
    .fromInt64 = s_int8_fromInt64
};

swl_typeFun_t swl_type_int8_fun = {
    .name = "swl_int8",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_int8_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_int8_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_int8NumFun,
};

swl_type_t gtSwl_type_int8 = {
    .size = sizeof(int8_t),
    .typeFun = &swl_type_int8_fun,
};
SWL_REF_TYPE_C(gtSwl_type_int8Ptr, gtSwl_type_int8);

/**
 * int16_t handlers
 */
static ssize_t s_int16_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%d", *(int16_t*) srcData);
}
static bool s_int16_fromChar_cb(swl_type_t* type _UNUSED, int16_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    int64_t data;
    ASSERTS_TRUE(s_charToInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (int16_t) data;
    return true;
}

static int64_t s_int16_toInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    int16_t* myValue = (int16_t*) el;
    return (int64_t) *myValue;
}

static swl_rc_ne s_int16_fromInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, int64_t val) {
    int16_t* myValue = (int16_t*) el;
    *myValue = val;
    bool fits = (val <= INT16_MAX) && (val >= INT16_MIN);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_numberSTypeFun_t s_int16NumFun = {
    .isSigned = true,
    .nrBits = 16,
    .toInt64 = s_int16_toInt64,
    .fromInt64 = s_int16_fromInt64
};

swl_typeFun_t swl_type_int16_fun = {
    .name = "swl_int16",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_int16_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_int16_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_int16NumFun,
};
swl_type_t gtSwl_type_int16 = {
    .size = sizeof(int16_t),
    .typeFun = &swl_type_int16_fun,
};
SWL_REF_TYPE_C(gtSwl_type_int16Ptr, gtSwl_type_int16);

/**
 * int32_t handlers
 */
static ssize_t s_int32_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%d", *(int32_t*) srcData);
}
static bool s_int32_fromChar_cb(swl_type_t* type _UNUSED, int32_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    int64_t data;
    ASSERTS_TRUE(s_charToInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (int32_t) data;
    return true;
}
static int64_t s_int32_toInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    int32_t* myValue = (int32_t*) el;
    return (int64_t) *myValue;
}

static swl_rc_ne s_int32_fromInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, int64_t val) {
    int32_t* myValue = (int32_t*) el;
    *myValue = val;
    bool fits = (val <= INT32_MAX) && (val >= INT32_MIN);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_numberSTypeFun_t s_int32NumFun = {
    .isSigned = true,
    .nrBits = 32,
    .toInt64 = s_int32_toInt64,
    .fromInt64 = s_int32_fromInt64
};

swl_typeFun_t swl_type_int32_fun = {
    .name = "swl_int32",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_int32_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_int32_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_int32NumFun,
};
swl_type_t gtSwl_type_int32 = {
    .size = sizeof(int32_t),
    .typeFun = &swl_type_int32_fun,
};
SWL_REF_TYPE_C(gtSwl_type_int32Ptr, gtSwl_type_int32);
/**
 * int64_t handlers
 */
static ssize_t s_int64_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%" PRIi64, *(int64_t*) srcData);
}
static bool s_int64_fromChar_cb(swl_type_t* type _UNUSED, int64_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    int64_t data;
    ASSERTS_TRUE(s_charToInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (int64_t) data;
    return true;
}

static int64_t s_int64_toInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    int64_t* myValue = (int64_t*) el;
    return (int64_t) *myValue;
}

static swl_rc_ne s_int64_fromInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, int64_t val) {
    int64_t* myValue = (int64_t*) el;
    *myValue = val;
    return SWL_RC_OK;
}

static swl_numberSTypeFun_t s_int64NumFun = {
    .isSigned = true,
    .nrBits = 64,
    .toInt64 = s_int64_toInt64,
    .fromInt64 = s_int64_fromInt64
};

swl_typeFun_t swl_type_int64_fun = {
    .name = "swl_int64",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_int64_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_int64_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_int64NumFun,
};
swl_type_t gtSwl_type_int64 = {
    .typeFun = &swl_type_int64_fun,
    .size = sizeof(int64_t),
};
SWL_REF_TYPE_C(gtSwl_type_int64Ptr, gtSwl_type_int64);


/**
 * uint8_t handler
 */
static ssize_t s_uint8_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%u", *(uint8_t*) srcData);
}
static bool s_uint8_fromChar_cb(swl_type_t* type _UNUSED, uint8_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    uint64_t data;
    ASSERTS_TRUE(s_charToUInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (uint8_t) data;
    return true;
}


static uint64_t s_uint8_toUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    uint8_t* myValue = (uint8_t*) el;
    return (uint64_t) *myValue;
}

static swl_rc_ne s_uint8_fromUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, uint64_t val) {
    int8_t* myValue = (int8_t*) el;
    *myValue = val;
    bool fits = (val <= UINT8_MAX);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_numberSTypeFun_t s_uint8NumFun = {
    .isSigned = false,
    .nrBits = 8,
    .toUInt64 = s_uint8_toUInt64,
    .fromUInt64 = s_uint8_fromUInt64
};

swl_typeFun_t swl_type_uint8_fun = {
    .name = "swl_uint8",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_uint8_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_uint8_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_uint8NumFun,
};

swl_type_t gtSwl_type_uint8 = {
    .typeFun = &swl_type_uint8_fun,
    .size = sizeof(uint8_t),
};

SWL_REF_TYPE_C(gtSwl_type_uint8Ptr, gtSwl_type_uint8);


/**
 * int16_t handlers
 */
static ssize_t s_uint16_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%u", *(uint16_t*) srcData);
}
static bool s_uint16_fromChar_cb(swl_type_t* type _UNUSED, uint16_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    uint64_t data;
    ASSERTS_TRUE(s_charToUInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (uint16_t) data;
    return true;
}

static uint64_t s_uint16_toUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    uint16_t* myValue = (uint16_t*) el;
    return (uint64_t) *myValue;
}

static swl_rc_ne s_uint16_fromUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, uint64_t val) {
    uint16_t* myValue = (uint16_t*) el;
    *myValue = val;
    bool fits = (val <= UINT16_MAX);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_rc_ne s_switchHtoN16(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, void* out) {
    uint16_t* myValue = (uint16_t*) el;
    uint16_t value = SWL_BIT16_SWAP(*myValue);
    memcpy(out, &value, sizeof(uint16_t));
    return SWL_RC_OK;
}

static swl_numberSTypeFun_t s_uint16NumFun = {
    .isSigned = false,
    .nrBits = 16,
    .toUInt64 = s_uint16_toUInt64,
    .fromUInt64 = s_uint16_fromUInt64,
    .switchHtoN = s_switchHtoN16
};

swl_typeFun_t swl_type_uint16_fun = {
    .name = "swl_uint16",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_uint16_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_uint16_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_uint16NumFun,
};

swl_type_t gtSwl_type_uint16 = {
    .typeFun = &swl_type_uint16_fun,
    .size = sizeof(uint16_t),
};

SWL_REF_TYPE_C(gtSwl_type_uint16Ptr, gtSwl_type_uint16);

/**
 * int32_t handlers
 */
static ssize_t s_uint32_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%u", *(uint32_t*) srcData);
}
static bool s_uint32_fromChar_cb(swl_type_t* type _UNUSED, uint32_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    uint64_t data;
    ASSERTS_TRUE(s_charToUInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (uint32_t) data;
    return true;
}


static uint64_t s_uint32_toUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    uint32_t* myValue = (uint32_t*) el;
    return (uint64_t) *myValue;
}

static swl_rc_ne s_uint32_fromUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, uint64_t val) {
    uint32_t* myValue = (uint32_t*) el;
    *myValue = val;
    bool fits = (val <= UINT32_MAX);
    return fits ? SWL_RC_OK : SWL_RC_RESULT_OUT_OF_BOUNDS;
}

static swl_rc_ne s_switchHtoN32(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, void* out) {
    uint32_t* myValue = (uint32_t*) el;
    uint32_t value = SWL_BIT32_SWAP(*myValue);
    memcpy(out, &value, sizeof(uint32_t));
    return SWL_RC_OK;
}

static swl_numberSTypeFun_t s_uint32NumFun = {
    .isSigned = false,
    .nrBits = 32,
    .toUInt64 = s_uint32_toUInt64,
    .fromUInt64 = s_uint32_fromUInt64,
    .switchHtoN = s_switchHtoN32
};


swl_typeFun_t swl_type_uint32_fun = {
    .name = "swl_uint32",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_uint32_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_uint32_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_uint32NumFun,
};
swl_type_t gtSwl_type_uint32 = {
    .typeFun = &swl_type_uint32_fun,
    .size = sizeof(uint32_t),
};
SWL_REF_TYPE_C(gtSwl_type_uint32Ptr, gtSwl_type_uint32);

/**
 * int64_t handlers
 */
static ssize_t s_uint64_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%" PRIu64, *(uint64_t*) srcData);
}
static bool s_uint64_fromChar_cb(swl_type_t* type _UNUSED, uint64_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");
    uint64_t data;
    ASSERTS_TRUE(s_charToUInt64(srcStr, &data), false, ME, "ERR");
    *tgtData = (uint64_t) data;
    return true;
}


static uint64_t s_uint64_toUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el) {
    uint64_t* myValue = (uint64_t*) el;
    return (uint64_t) *myValue;
}

static swl_rc_ne s_uint64_fromUInt64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, uint64_t val) {
    uint64_t* myValue = (uint64_t*) el;
    *myValue = val;
    return SWL_RC_OK;
}

static swl_rc_ne s_switchHtoN64(swl_numberType_t* numberType _UNUSED, swl_typeEl_t* el, void* out) {
    uint64_t* myValue = (uint64_t*) el;
    uint64_t value = SWL_BIT64_SWAP(*myValue);
    memcpy(out, &value, sizeof(uint64_t));
    return SWL_RC_OK;
}

static swl_numberSTypeFun_t s_uint64NumFun = {
    .isSigned = false,
    .nrBits = 64,
    .toUInt64 = s_uint64_toUInt64,
    .fromUInt64 = s_uint64_fromUInt64,
    .switchHtoN = s_switchHtoN64
};


swl_typeFun_t swl_type_uint64_fun = {
    .name = "swl_uint64",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_uint64_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_uint64_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
    .subFun = &s_uint64NumFun,
};
swl_type_t gtSwl_type_uint64 = {
    .typeFun = &swl_type_uint64_fun,
    .size = sizeof(uint64_t),
};
SWL_REF_TYPE_C(gtSwl_type_uint64Ptr, gtSwl_type_uint64);


/**
 * double handlers
 */
static ssize_t s_double_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    char buffer[128];
    ssize_t len = snprintf(buffer, sizeof(buffer), "%f", *(double*) srcData);
    if(len < 0) {
        return len;
    }
    //find index of last significant digit, to remove non significant 0's, and the final "." if not relevant
    size_t lastSignificantIndex = SWL_MIN(sizeof(buffer) - 1, (size_t) len) - 1;
    while(lastSignificantIndex >= 1 && (buffer[lastSignificantIndex] == '0' )) {
        lastSignificantIndex -= 1;
    }
    if((lastSignificantIndex >= 1) && (buffer[lastSignificantIndex] == '.')) {
        lastSignificantIndex -= 1;
    }
    buffer[lastSignificantIndex + 1] = '\0';
    swl_str_copy(tgtStr, tgtStrSize, buffer);
    return lastSignificantIndex + 1;
}

static bool s_double_fromChar_cb(swl_type_t* type _UNUSED, double* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");

    *tgtData = strtod(srcStr, NULL);
    return true;
}
swl_typeFun_t swl_type_double_fun = {
    .name = "swl_double",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_double_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_double_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};

swl_type_t gtSwl_type_double = {
    .size = sizeof(double),
    .typeFun = &swl_type_double_fun,
};
SWL_REF_TYPE_C(gtSwl_type_doublePtr, gtSwl_type_double);


/**
 * Char ptr handlers
 */

ssize_t swl_typeDef_charPtr_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const char* srcData) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    return snprintf(tgtStr, tgtStrSize, "%s", srcData);
}
bool swl_typeDef_charPtr_fromChar_cb(swl_type_t* type _UNUSED, char** tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");

    if(*tgtData != NULL) {
        free(*tgtData);
        *tgtData = NULL;
    }
    *tgtData = strdup(srcStr);
    return true;
}

bool swl_typeDef_charPtr_toFile_cb(swl_type_t* type _UNUSED, FILE* file, const char* srcStr, swl_print_args_t* printArgs) {
    ASSERTS_NOT_NULL(file, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");

    return swl_print_toStreamBuf(file, printArgs, srcStr, swl_str_len(srcStr));
}



char* swl_typeDef_charPtr_copy_cb(swl_type_t* type _UNUSED, const char* srcStr) {
    ASSERT_NOT_NULL(srcStr, NULL, ME, "NULL");
    return strdup(srcStr);
}


bool swl_typeDef_charPtr_equals_cb (swl_type_t* type _UNUSED, const void* key0, const void* key1) {
    return swl_str_matches(key0, key1);
}

swl_typeFun_t swl_type_charPtr_fun = {
    .name = "swl_charPtr",
    .isValue = false,
    .toChar = (swl_type_toChar_cb) swl_typeDef_charPtr_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) swl_typeDef_charPtr_fromChar_cb,
    .toFile = (swl_type_toFile_cb) swl_typeDef_charPtr_toFile_cb,
    .equals = (swl_type_equals_cb) swl_typeDef_charPtr_equals_cb,
    .copy = (swl_type_copy_cb) swl_typeDef_charPtr_copy_cb,
};

swl_type_t gtSwl_type_charPtr = {
    .size = sizeof(char*),
    .typeFun = &swl_type_charPtr_fun,
};

bool swl_typeDef_charBuf_fromChar_cb(swl_type_t* type, char* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERTS_NOT_NULL(srcStr, false, ME, "NULL");

    swl_str_copy(tgtData, type->size, srcStr);
    return true;
}



swl_typeFun_t swl_type_charbuf_fun = {
    .name = "swl_charBuf",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) swl_typeDef_charPtr_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) swl_typeDef_charBuf_fromChar_cb,
    .equals = (swl_type_equals_cb) swl_typeDef_charPtr_equals_cb,
    //copy should just do memcpy
};

SWL_TYPE_CHARBUF_IMPL(4);
SWL_TYPE_CHARBUF_IMPL(8);
SWL_TYPE_CHARBUF_IMPL(16);
SWL_TYPE_CHARBUF_IMPL(18);
SWL_TYPE_CHARBUF_IMPL(32);
SWL_TYPE_CHARBUF_IMPL(64);
SWL_TYPE_CHARBUF_IMPL(128);

/**
 * voidPtr handlers
 */
static ssize_t s_voidPtrToCharCb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ssize_t ret = 0;
    void** ppData = (void**) srcData;
    /*
     * NULL ptr are printed as <nil> string
     * which can not be converted back to numerical value.
     * So, consider here NULL ptr address as zero long value.
     */
    if(*ppData == NULL) {
        ret = snprintf(tgtStr, tgtStrSize, "%lx", 0L);
    } else {
        ret = snprintf(tgtStr, tgtStrSize, "%p", *ppData);
    }
    return ret;
}

static bool s_voidPtrFromCharCb(swl_type_t* type _UNUSED, void** tgtData _UNUSED, const char* srcStr _UNUSED) {
    // forbid deserialization for security reasons
    return false;
}

swl_typeFun_t swl_type_voidPtr_fun = {
    .name = "swl_voidPtr",
    //Note, void pointers are compared on their own values, not the memory pointing to.
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_voidPtrToCharCb,
    .fromChar = (swl_type_fromChar_cb) s_voidPtrFromCharCb,
};

swl_type_t gtSwl_type_voidPtr = {
    .size = sizeof(void*),
    .typeFun = &swl_type_voidPtr_fun,
};

