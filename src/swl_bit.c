/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_bit.h"
#include "swl/swl_common.h"
#include "swl/swl_assert.h"

#define ME "swlBit"

/**
 * Count the number of bits set in the 32 bit value.
 */
size_t swl_bit32_getNrSet(swl_bit32_t val) {
    size_t i = 0;
    size_t count = 0;
    for(i = 0; i < 32; i++) {
        if(SWL_BIT_IS_SET(val, i)) {
            count++;
        }
    }
    return count;
}

/**
 * Count the number of bits set in the 64 bit value.
 */
size_t swl_bit64_getNrSet(swl_bit64_t val) {
    swl_bit64_t i = 0;
    size_t count = 0;
    for(i = 0; i < 64; i++) {
        if(SWL_BIT_IS_SET(val, i)) {
            count++;
        }
    }
    return count;
}

/**
 * Get the index of the lowest bit set in val.
 * If no bit is set, returns -1;
 */
int32_t swl_bit32_getLowest(swl_bit32_t val) {
    ASSERTS_NOT_EQUALS(val, 0, -1, ME, "ZERO");

    swl_bit32_t i = 0;
    swl_bit32_t ret = 0;
    for(i = 16; i != 0; i = i / 2) {
        swl_bit32_t tmpVal = val << i;
        if(tmpVal) {
            val = tmpVal;
        } else {
            ret += i;
        }
    }
    return ret;
}

/**
 * Get the index of the highest bit set in val.
 * If no bit is set, returns -1
 */
int32_t swl_bit32_getHighest(swl_bit32_t val) {
    ASSERTS_NOT_EQUALS(val, 0, -1, ME, "ZERO");

    swl_bit32_t i = 0;
    swl_bit32_t ret = 0;
    for(i = 16; i != 0; i = i / 2) {
        swl_bit32_t tmpVal = val >> i;
        if(tmpVal) {
            val = tmpVal;
            ret += i;
        }
    }
    return ret;
}

/**
 * Get the index of the lowest bit set in val.
 * If no bit is set, returns -1;
 */
int32_t swl_bit64_getLowest(swl_bit64_t val) {
    ASSERTS_NOT_EQUALS(val, 0, -1, ME, "ZERO");

    swl_bit64_t i = 0;
    swl_bit64_t ret = 0;
    for(i = 32; i != 0; i = i / 2) {
        swl_bit64_t tmpVal = val << i;
        if(tmpVal) {
            val = tmpVal;
        } else {
            ret += i;
        }
    }
    return ret;
}

int32_t swl_bit64_getHighest(swl_bit64_t val) {
    ASSERTS_NOT_EQUALS(val, 0, -1, ME, "ZERO");

    swl_bit64_t i = 0;
    swl_bit64_t ret = 0;
    for(i = 32; i != 0; i = i / 2) {
        swl_bit32_t tmpVal = val >> i;
        if(tmpVal) {
            val = tmpVal;
            ret += i;
        }
    }
    return ret;
}

// Setters

/**
 * Set the bit with index bitNr.
 */
void swl_bitArr32_set(swl_bit32_t* array, size_t size, size_t bitNr) {
    swl_bitArr32_write(array, size, bitNr, true);
}

/**
 * clear the bit with index bitNr.
 */
void swl_bitArr32_clear(swl_bit32_t* array, size_t size, size_t bitNr) {
    swl_bitArr32_write(array, size, bitNr, false);
}

/**
 * Write the given bool value to the bit with index bitNr
 */
void swl_bitArr32_write(swl_bit32_t* array, size_t size, size_t bitNr, bool val) {
    ASSERT_NOT_NULL(array, , ME, "NULL");
    size_t arrayIndex = bitNr / 32;
    swl_bit32_t bitIndex = bitNr % 32;

    ASSERT_TRUE(arrayIndex < size, , ME, "INVALID");

    W_SWL_BIT_WRITE(array[arrayIndex], bitIndex, val);
}

/**
 * Set all bits to true
 */
void swl_bitArr32_setAll(swl_bit32_t* array, size_t size) {
    ASSERT_NOT_NULL(array, , ME, "NULL");

    size_t i;
    for(i = 0; i < size; i++) {
        array[i] = UINT32_MAX;
    }
}

/**
 * Set all bits before the bit with index bitNr. The bits with index greater or equal to bitNr will NOT
 * be changed. Only the bits before bitNr will be set to true.
 */
void swl_bitArr32_setUntil(swl_bit32_t* array, size_t size, size_t bitNr) {
    ASSERT_NOT_NULL(array, , ME, "NULL");
    size_t arrayIndex = bitNr / 32;
    swl_bit32_t bitIndex = bitNr % 32;

    size_t i = 0;
    for(i = 0; i < arrayIndex && i < size; i++) {
        array[i] = UINT32_MAX;
    }
    if(arrayIndex >= size) {
        return;
    }
    W_SWL_BIT_SET_UNTIL(array[arrayIndex], bitIndex);
}

/**
 * Set all bits to false
 */
void swl_bitArr32_clearAll(swl_bit32_t* array, size_t size) {
    ASSERT_NOT_NULL(array, , ME, "NULL");

    size_t i;
    for(i = 0; i < size; i++) {
        array[i] = 0;
    }
}

/**
 * Set all bits before the bit with index bitNr. The bits with index greater or equal to bitNr will NOT
 * be changed. Only the bits before bitNr will be set to false.
 */
void swl_bitArr32_clearUntil(swl_bit32_t* array, size_t size, size_t bitNr) {
    ASSERT_NOT_NULL(array, , ME, "NULL");
    size_t arrayIndex = (bitNr / 32);
    swl_bit32_t bitIndex = (bitNr % 32);

    size_t i = 0;
    for(i = 0; i < arrayIndex && i < size; i++) {
        array[i] = 0;
    }
    if(arrayIndex >= size) {
        return;
    }

    W_SWL_BIT_CLEAR_UNTIL(array[arrayIndex], bitIndex);
}

// getters

/**
 * returns whether bit with index bitNr is set.
 */
bool swl_bitArr32_isSet(swl_bit32_t* array, size_t size, size_t bitNr) {
    size_t arrayIndex = bitNr / 32;
    swl_bit32_t bitIndex = bitNr % 32;

    if(arrayIndex >= size) {
        return false;
    }

    return SWL_BIT_IS_SET(array[arrayIndex], bitIndex);
}

/**
 * Returns whether any bit is set.
 */
bool swl_bitArr32_isAnySet(swl_bit32_t* array, size_t size) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");

    size_t i;
    for(i = 0; i < size; i++) {
        if(array[i] != 0) {
            return true;
        }
    }

    return false;
}

/**
 * Returns 1 if the given bit number, and only the given bitnr is set in the long array.
 * Returns 0 otherwise.
 */
bool swl_bitArr32_isOnlySet(swl_bit32_t* array, size_t size, size_t bitNr) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");
    size_t arrayIndex = bitNr / 32;
    swl_bit32_t bitIndex = bitNr % 32;

    size_t i = 0;
    for(i = 0; i < size && i != arrayIndex; i++) {
        if(array[i] != 0) {
            return false;
        }
    }

    return SWL_BIT_IS_ONLY_SET(array[arrayIndex], bitIndex);
}

/**
 * Returns whether the two bit arrays are equal.
 */
bool swl_bitArr32_equals(swl_bit32_t* array1, swl_bit32_t* array2, size_t size) {
    ASSERT_NOT_NULL(array1, false, ME, "NULL");
    ASSERT_NOT_NULL(array2, false, ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < size; i++) {
        if(array1[i] != array2[i]) {
            return false;
        }
    }
    return true;
}

/**
 * get the number of bits set in the bit array.
 */
size_t swl_bitArr32_getNrSet(swl_bit32_t* array, size_t size) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");
    size_t i = 0;
    size_t acc = 0;
    for(i = 0; array && i < size; i++) {
        acc += swl_bit32_getNrSet(array[i]);
    }
    return acc;
}

/**
 * returns the lowest bit in an array of swl_bit32_t. In case no bit is set
 * it returns -1.
 */
int32_t swl_bitArr32_getLowest(swl_bit32_t* array, size_t size) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");
    size_t i = 0;
    swl_bit32_t testVal = 0;

    for(i = 0; i < size; i++) {
        testVal = array[i];
        if(testVal) {
            return swl_bit32_getLowest(array[i]) + i * 32;
        }
    }
    return -1;
}

/**
 * returns the highest bit in an array of swl_bit32_t. In case no bit is set
 * it returns -1.
 */
int32_t swl_bitArr32_getHighest(swl_bit32_t* array, size_t size) {
    int32_t i = 0;
    swl_bit32_t testVal = 0;

    for(i = (size - 1); i >= 0; i--) {
        testVal = array[i];
        if(testVal) {
            return swl_bit32_getHighest(testVal) + i * 32;
        }
    }
    return -1;
}

/**
 * set all the bits with index in the bitNrArray list to true. Other bits will not be changed
 */
void swl_bitArr32_setList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize) {
    swl_bitArr32_writeList(array, size, bitNrArray, bitNrArraySize, true);
}

/**
 * set all the bits with index in the bitNrArray list to false. Other bits will not be changed
 */
void swl_bitArr32_clearList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize) {
    swl_bitArr32_writeList(array, size, bitNrArray, bitNrArraySize, false);
}

/**
 * set all the bits with index in the bitNrArray list to val. Other bits will not be changed
 */
void swl_bitArr32_writeList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize, bool val) {
    ASSERT_NOT_NULL(array, , ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < bitNrArraySize; i++) {
        swl_bitArr32_write(array, size, bitNrArray[i], val);
    }
}

/**
 * returns true if all the bits in bitNrArray are set. Returns false otherwise
 */
bool swl_bitArr32_isSetList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize) {
    ASSERT_NOT_NULL(array, false, ME, "NULL");
    ASSERT_NOT_NULL(bitNrArray, false, ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < bitNrArraySize; i++) {
        if(!swl_bitArr32_isSet(array, size, bitNrArray[i])) {
            return false;
        }
    }
    return true;
}

/**
 * returns true if all the bits in bitNrArray are set, and only those. Returns false otherwise
 */
bool swl_bitArr32_isOnlySetList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize) {
    ASSERT_NOT_NULL(array, 0, ME, "NULL");
    swl_bit32_t testArray[size];
    swl_bitArr32_clearAll(testArray, size);
    swl_bitArr32_setList(testArray, size, bitNrArray, bitNrArraySize);
    return swl_bitArr32_equals(array, testArray, size);
}

/**
 * Perform bitwise and between array1 and array2 and write result in tgtArray.
 * Note that tgtArray can be either array1 or array2.
 */
void swl_bitArr32_and(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size) {
    ASSERT_NOT_NULL(tgtArray, , ME, "NULL");
    ASSERT_NOT_NULL(array1, , ME, "NULL");
    ASSERT_NOT_NULL(array2, , ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < size; i++) {
        tgtArray[i] = array1[i] & array2[i];
    }
}

/**
 * Perform bitwise or between array1 and array2 and write result in tgtArray.
 * Note that tgtArray can be either array1 or array2.
 */
void swl_bitArr32_or(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size) {
    ASSERT_NOT_NULL(tgtArray, , ME, "NULL");
    ASSERT_NOT_NULL(array1, , ME, "NULL");
    ASSERT_NOT_NULL(array2, , ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < size; i++) {
        tgtArray[i] = array1[i] | array2[i];
    }
}

/**
 * Perform bitwise xor between array1 and array2 and write result in tgtArray.
 * Note that tgtArray can be either array1 or array2.
 */
void swl_bitArr32_xor(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size) {
    ASSERT_NOT_NULL(tgtArray, , ME, "NULL");
    ASSERT_NOT_NULL(array1, , ME, "NULL");
    ASSERT_NOT_NULL(array2, , ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < size; i++) {
        tgtArray[i] = array1[i] ^ array2[i];
    }
}

/**
 * Perform bitwise not on srcArray and write result in tgtArray.
 * Note that tgtArray can be srcArray.
 */
void swl_bitArr32_not(swl_bit32_t* tgtArray, swl_bit32_t* srcArray, size_t size) {
    ASSERT_NOT_NULL(tgtArray, , ME, "NULL");
    ASSERT_NOT_NULL(srcArray, , ME, "NULL");
    swl_bit32_t i = 0;
    for(i = 0; i < size; i++) {
        tgtArray[i] = ~srcArray[i];
    }
}
