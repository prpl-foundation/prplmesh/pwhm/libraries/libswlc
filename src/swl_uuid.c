/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlUuid"
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "swl/swl_uuid.h"
#include "swl/swl_hex.h"
#include "swl/swl_assert.h"

/****************************************************************************
* SWL UUID library
*
* Deals with UUID (Universally Unique IDentifier) as defined in RFC 4122.
****************************************************************************/

/** Length of a mac address in text format, excluding terminating '\0'. */
#define MAC_ADDR_LEN 17

/**
 * Create a new binary UUID based on a MAC address
 *
 * This function has been borrowed and adapted from hostapd.
 *
 * @param tgtUuid: target to be written to.
 * @param macAddr: MAC address in format "E8:F1:B0:CE:07:D4" (null-terminated)
 * @param offset: allows to create two different UUIDs for the same MAC by passing two
 *   different offsets to this function. You can use it for making a different UUID on Radio base.
 * @return true on success, ok on error:
 *   - MAC address invalid (not necessarily a strict check, so do not use for validating MAC)
 */
bool swl_uuid_fromMacAddress(swl_uuid_t* tgtUuid, const char* macAddr, uint8_t offset) {
    ASSERT_NOT_NULL(tgtUuid, false, ME, "NULL");
    ASSERT_TRUE(macAddr != NULL && strlen(macAddr) == MAC_ADDR_LEN, false, ME, "Invalid MAC address");

    int uuidIdx;
    int macIdx;

    // copy the 12 chars of the mac address, in loop, until the UUID is filled.
    for(uuidIdx = 0, macIdx = 0; uuidIdx < SWL_UUID_BIN_SIZE; uuidIdx++) {
        if((macAddr[macIdx] == ':') || (macAddr[macIdx] == '-')) {
            macIdx = (macIdx + 1) % MAC_ADDR_LEN; // skip semicolons and dashes.
        }
        tgtUuid->uuid[uuidIdx] = (macAddr[macIdx] + offset);

        macIdx = (macIdx + 1) % MAC_ADDR_LEN;
    }

    /* See BUG53938: At least on Celeno Windows7 is unhappy with version
     * 4/variant 8 UUIDs.  It does accept version f/variant f. */
    tgtUuid->uuid[6] &= 0x0f; tgtUuid->uuid[6] |= (0xf << 4);
    tgtUuid->uuid[8] &= 0x3f; tgtUuid->uuid[8] |= 0xf0;

    return true;
}

/**
 * Whether the two given UUIDs are equal.
 */
bool swl_uuid_eq(const swl_uuid_t* uuid1, const swl_uuid_t* uuid2) {
    ASSERT_NOT_NULL(uuid1, false, ME, "NULL");
    ASSERT_NOT_NULL(uuid2, false, ME, "NULL");
    return 0 == bcmp(uuid1->uuid, uuid2->uuid, SWL_UUID_BIN_SIZE);
}

/**
 * Converts binary representation of a UUID to a textual representation.
 *
 * Writes as lowercase.
 *
 * @param tgtStr: target buffer written to.
 * @param tgtStrSize: size of `tgtStr` buffer. Need at least #SWL_UUID_CHAR_SIZE bytes.
 * @param srcUuid: binary UUID
 * @return true on success, false on error.
 */
bool swl_uuid_toChar(char* tgtStr, size_t tgtStrSize, const swl_uuid_t* srcUuid) {
    ASSERT_NOT_NULL(tgtStr, false, ME, "NULL");
    ASSERT_NOT_NULL(srcUuid, false, ME, "NULL");
    int len;
    const uint8_t* bin = srcUuid->uuid;
    len = snprintf(tgtStr, tgtStrSize,
                   "%02x%02x%02x%02x-"                                    // group 1
                   "%02x%02x-"                                            // group 2
                   "%02x%02x-"                                            // group 3
                   "%02x%02x-"                                            // group 4
                   "%02x%02x%02x%02x%02x%02x",                            // group 5
                   bin[0], bin[1], bin[2], bin[3],                        // group 1
                   bin[4], bin[5],                                        // group 2
                   bin[6], bin[7],                                        // group 3
                   bin[8], bin[9],                                        // group 4
                   bin[10], bin[11], bin[12], bin[13], bin[14], bin[15]); // group 5
    ASSERT_FALSE((len < 0) || ((size_t) len >= tgtStrSize), false, ME, "Insufficient space");
    return true;
}

/**
 * Converts textual representation of a UUID to a binary representation.
 *
 * @param tgtUuid: target UUID to overwrite.
 * @param str: text-representation of a UUID.
 * @return true if and only if `uuid` is NOT null and furthermore `swl_uuid_isValidChar(str)`
 *   is true.
 */
bool swl_uuid_fromChar(swl_uuid_t* tgtUuid, const char* str) {
    ASSERT_NOT_NULL(tgtUuid, false, ME, "NULL");
    ASSERT_NOT_NULL(str, false, ME, "NULL");
    //          byte:  0         1         2         3
    //                 012345678901234567890123456789012345
    //          text: "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx"
    //                 |       /    /   _/  __/
    //                 |       |   /   /   /
    //        binary: "x x x x|X X|x x|X X|x x x x x x"
    //          byte:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
    //                 0                   1

    ASSERTS_TRUE(strlen(str) == SWL_UUID_CHAR_SIZE - 1, false, ME, "Wrong length");
    bool ok = false;
    ok = swl_hex_toBytes(&tgtUuid->uuid[0], 4, &str[0], 8);
    ASSERTS_TRUE(ok && str[8] == '-', false, ME, "Invalid");
    ok = swl_hex_toBytes(&tgtUuid->uuid[4], 2, &str[9], 4);
    ASSERTS_TRUE(ok && str[13] == '-', false, ME, "Invalid");
    ok = swl_hex_toBytes(&tgtUuid->uuid[6], 2, &str[14], 4);
    ASSERTS_TRUE(ok && str[18] == '-', false, ME, "Invalid");
    ok = swl_hex_toBytes(&tgtUuid->uuid[8], 2, &str[19], 4);
    ASSERTS_TRUE(ok && str[23] == '-', false, ME, "Invalid");
    ok = swl_hex_toBytes(&tgtUuid->uuid[10], 6, &str[24], 12);
    return ok;
}

/**
 * Whether the given string is a valid UUID.
 */
bool swl_uuid_isValidChar(const char* str) {
    //      byte:  0         1         2         3
    //             012345678901234567890123456789012345
    //    format: "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx"
    //             \______/ \__/ \__/ \__/ \__________/
    //   group start:  0     9    14   19       24
    //   group length: 8     4     4    4       12
    return
        str != NULL
        && strlen(str) == SWL_UUID_CHAR_SIZE - 1
        && swl_hex_isHexChar(str + 0, 8)     // start and length from diagram above
        && swl_hex_isHexChar(str + 9, 4)
        && swl_hex_isHexChar(str + 14, 4)
        && swl_hex_isHexChar(str + 19, 4)
        && swl_hex_isHexChar(str + 24, 12)
        && str[8] == '-'
        && str[13] == '-'
        && str[18] == '-'
        && str[23] == '-';
}

