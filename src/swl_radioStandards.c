/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/** @file This .c file is for representation of and basic operations on radio standards such as 802.11a.
 *
 * A **Radio Standard** is a standard of low-level Wi-Fi communication defined by IEEE.
 * A Radio Standard exists regardless of whether the chip supports it or whether it is enabled
 * or used.
 *
 * *Operating Standards* is the set of radio standards that are enabled for a radio.
 * Operating Standards can be written in different formats:
 * - *Standard Format*: comma-separated of standards, e.g. "b,g,n". NOT: "bgn".
 * - *Legacy Format*: value without comma, e.g. "bgn", "ac" or "b". NOT: "b,g,n" or "g,ac".
 *    "ac" and "ax" mean including lower supported standards. This is the most backwards-compatible.
 *    If it cannot be expressed like this, fallback to writing it in Standard Format.
 *
 * *Supported Standards* is the set of radio standards that can be enabled (but not necessarily are)
 * for a radio.
 * Supported Standards can be written in different formats:
 * - *Standard Format*: comma-separated of standards, e.g. "b,g,n". NOT: "bgn".
 * - *Legacy Format*: comma-separated of standards and of sets of standards, e.g. "b,g,n,bg,bgn".
 *
 * Note that it is preferred to not use strings to represent data (use #swl_radioStandard_m
 * instead), it is needed only when parsing/formatting from/to users and for externally defined
 * APIs that require strings.
 */

#define ME "swlRStd"


#include "swl/swl_common.h"
#include "swl/swl_common_radioStandards.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_string.h"
#include "swl/swl_assert.h"
#include "swl/swl_typeEnum.h"

const char* const swl_radStd_str[] = {"auto", "a", "b", "g", "n", "ac", "ax", "be"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_radStd_str) == SWL_RADSTD_MAX, "swl_radStd_str not correctly defined");
const char* const swl_radStd_unknown_str[] = {"Unknown", "a", "b", "g", "n", "ac", "ax", "be"};
SWL_ASSERT_STATIC(SWL_ARRAY_SIZE(swl_radStd_unknown_str) == SWL_RADSTD_MAX, "swl_radStd_unknown_str not correctly defined");

SWL_TYPE_ENUM_C(gtSwl_type_radStd, swl_radStd_e, SWL_RADSTD_AUTO, SWL_RADSTD_MAX, swl_radStd_str);
SWL_TYPE_ENUM_C(gtSwl_type_radStdUnknown, swl_radStd_e, SWL_RADSTD_AUTO, SWL_RADSTD_MAX, swl_radStd_unknown_str);

/**
 * Superset of #swl_radStd_str that includes legacy set notations ("bg","bgn", etc).
 */
static const char* radStdIncludingLegacy[] = {
    SWL_RADSTD_LEGACY_AUTO,
    SWL_RADSTD_LEGACY_A,
    SWL_RADSTD_LEGACY_B,
    SWL_RADSTD_LEGACY_G,
    SWL_RADSTD_LEGACY_N,
    SWL_RADSTD_LEGACY_B_G,
    SWL_RADSTD_LEGACY_G_N,
    SWL_RADSTD_LEGACY_B_G_N,
    SWL_RADSTD_LEGACY_A_N,
    SWL_RADSTD_LEGACY_A_B_G_N,
    SWL_RADSTD_LEGACY_AC_AND_LOWER,
    SWL_RADSTD_LEGACY_AX_AND_LOWER,
    SWL_RADSTD_LEGACY_BE_AND_LOWER
};

/**
 * Bitmasks for #radStdIncludingLegacy.
 */
static const swl_radioStandard_m radStdMasksIncludingLegacy[] = {
    M_SWL_RADSTD_AUTO,                                                 // "auto"
    M_SWL_RADSTD_A,                                                    // "a"
    M_SWL_RADSTD_B,                                                    // "b"
    M_SWL_RADSTD_G,                                                    // "g"
    M_SWL_RADSTD_N,                                                    // "n"
    M_SWL_RADSTD_B | M_SWL_RADSTD_G,                                   // "bg"
    M_SWL_RADSTD_G | M_SWL_RADSTD_N,                                   // "gn"
    M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N,                  // "bgn"
    M_SWL_RADSTD_A | M_SWL_RADSTD_N,                                   // "an"
    M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N, // "abgn"
    M_SWL_RADSTD_AC,                                                   // "ac"
    M_SWL_RADSTD_AX,                                                   // "ax"
    M_SWL_RADSTD_BE                                                    // "be"
};

/** Conversion between string and enum representation of #swl_radStd_format_e. */
const char* formatNotation[SWL_RADSTD_FORMAT_MAX] = {"Legacy", "Standard"};

/**
 * Take care of special-case of "ac" and "ax" for Legacy format.
 *
 * i.e. convert "a,n,ac" to "ac" and "a,b,g,n,ac,ax" to "ax".
 * This takes into account which standards are supported, so "n,ac" can be converted
 * to "ac" if only "n" is a lower supported standard, but "n,ac" is not if both "a" and "n"
 * are supported.
 *
 * Note that this function is only useful for Legacy format.
 *
 * @param supportedStandards: used when including supported lower standards, as explained above.
 *   Ignored if `format` is standard.
 */
static swl_radioStandard_m s_removeLowerStandards(
    swl_radioStandard_m standards,
    swl_radioStandard_m supportedStandards) {
    swl_radioStandard_m expressAC = M_SWL_RADSTD_AC
        | ((M_SWL_RADSTD_A | M_SWL_RADSTD_N) & supportedStandards);
    swl_radioStandard_m expressAX = M_SWL_RADSTD_AX
        | ((M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC)
           & supportedStandards);
    swl_radioStandard_m expressBE = M_SWL_RADSTD_BE
        | ((M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX)
           & supportedStandards);
    if(standards == expressAC) {
        return M_SWL_RADSTD_AC;
    } else if(standards == expressAX) {
        return M_SWL_RADSTD_AX;
    } else if(standards == expressBE) {
        return M_SWL_RADSTD_BE;
    } else {
        return standards;
    }
}

/* Formats a radio standard bitmask into a string following Standard Format. */
static bool s_toCharInStandardFormat(char* targetBuffer,
                                     uint32_t targetBufferSize,
                                     swl_radioStandard_m radioStandards) {

    return swl_conv_maskToChar(targetBuffer, targetBufferSize, radioStandards,
                               swl_radStd_str, SWL_ARRAY_SIZE(swl_radStd_str));
}

/** Formats a radio standard bitmask into a string, but without fallback to standard format */
static bool s_toCharInStrictlyLegacyFormat(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m radioStandards,
    swl_radioStandard_m supportedStandards) {

    // Many bitmasks can represent "ac" or "ax", so we convert that
    // to just `M_SWL_RADSTD_AC` or `M_SWL_RADSTD_AX` so we can look
    // it up in `legacySpecialCaseValues`.
    radioStandards = s_removeLowerStandards(radioStandards, supportedStandards);

    uint32_t i;
    for(i = 0; i < SWL_ARRAY_SIZE(radStdMasksIncludingLegacy); i++) {
        if(radStdMasksIncludingLegacy[i] == radioStandards) {
            if(strlen(radStdIncludingLegacy[i]) + 1 > targetBufferSize) {
                return false;
            } else {
                swl_str_copy(targetBuffer, targetBufferSize, radStdIncludingLegacy[i]);
                return true;
            }
        }
    }
    // No special case found.
    return false;
}

/** Formats a radio standard bitmask into a string, but only for Legacy Format. */
static bool s_toCharLegacyFormat(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m radioStandards,
    swl_radioStandard_m supportedStandards) {
    // Legacy Format is defined as having some special cases, and using
    // Standard Format for the rest. This avoids having values that can
    // be represented in one format and not in the other.
    return s_toCharInStrictlyLegacyFormat(targetBuffer, targetBufferSize,
                                          radioStandards, supportedStandards)
           || s_toCharInStandardFormat(targetBuffer, targetBufferSize, radioStandards);
}

/**
 * Like #swl_radStd_toStr but with `char*` instead of `string_t`.
 *
 * @param targetBuffer: buffer written to. Must not be NULL.
 *		In case of error (i.e. when returning false), nonsense can be written to it.
 * @param targetBufferSize: length of targetBuffer
 * @param radioStandards: set of radio standards to format into a string buffer. Must be a valid set.
 * @param format: See #swl_radStd_toStr.
 * @param supportedStandards: See #swl_radStd_toStr.
 * @return true on success, false if buffer too small or invalid arguments as explained above.
 */
bool swl_radStd_toChar(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m radioStandards,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards) {
    if(!swl_radStd_isValid(radioStandards, "swl_radStd_toChar")) {
        return false;
    }

    if(format == SWL_RADSTD_FORMAT_LEGACY) {
        return s_toCharLegacyFormat(targetBuffer, targetBufferSize, radioStandards,
                                    supportedStandards);
    } else {
        return s_toCharInStandardFormat(targetBuffer, targetBufferSize, radioStandards);
    }
}

/**
 * Low-level parse set of radio standards in Legacy Format without fallback to Standard Format
 *
 * For example "bg".
 *
 * @param string: the to be parsed string. Not null. Does not have to be parseable.
 * @output: result of parsing is written here, only written to on success (i.e. only if return true).
 * @result: whether the string was parseable (according to legacy format)
 *	and furthermore results in a valid set (so no "" or "auto,b").
 */
static bool s_fromCharInStrictlyLegacyFormat(swl_radioStandard_m* output, const char* string) {
    uint32_t index = swl_conv_charToEnum(string, radStdIncludingLegacy, SWL_ARRAY_SIZE(radStdIncludingLegacy), UINT32_MAX);
    if(index == UINT32_MAX) {
        return false;
    }
    swl_radioStandard_m mask = radStdMasksIncludingLegacy[index];
    if(!swl_radStd_isValid(mask, NULL)) {
        return false;
    }

    *output = mask;
    return true;
}

/**
 * Low-level parse comma-separated notation of set of radio standards, e.g. "b,g"
 *
 * @param string: input to be parsed. Must not be null. Allowed to be not-parseable or empty.
 * @param output: result of parsing is written to here, only written to on success (i.e. only if return true).
 * @return: whether parsing succeeded and furthermore results in a valid set (so no "" or "auto,b").
 *    Parsing succeeds if all items separated
 *    by comma are a known WiFi standard, such as "a","b","g","ac",etc.
 */
static bool s_fromCharInStrictlyStandardFormat(swl_radioStandard_m* output, const char* string) {
    if(string[0] == '\0') {
        return false;
    }
    bool entireStringUsed;
    swl_radioStandard_m mask = swl_conv_charToMaskSep(string, swl_radStd_str, SWL_ARRAY_SIZE(swl_radStd_str), ',', &entireStringUsed);
    if(!entireStringUsed) {
        return false;
    }
    if(!swl_radStd_isValid(mask, NULL)) {
        return false;
    }

    *output = mask;
    return true;
}

/**
 * Converts from legacy to standard notation of expressing radiostandards if necessary
 *
 * i.e. convert "ac"-only to "a,n,ac" and "ax"-only to "a,b,g,n,ac,ax" (excluding unsupported ones),
 *
 * @param supportedStandards: used when including supported lower standards, as explained above.
 *   Ignored if format is standard.
 */
static swl_radioStandard_m s_addLowerStandardsIfLegacy(
    swl_radioStandard_m standards,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards) {
    if(format != SWL_RADSTD_FORMAT_LEGACY) {
        return standards;
    }

    swl_radioStandard_m tryToAdd;
    if(standards == M_SWL_RADSTD_AC) {
        tryToAdd = M_SWL_RADSTD_A | M_SWL_RADSTD_N;
    } else if(standards == M_SWL_RADSTD_AX) {
        tryToAdd = M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC;
    } else if(standards == M_SWL_RADSTD_BE) {
        tryToAdd = M_SWL_RADSTD_A | M_SWL_RADSTD_B | M_SWL_RADSTD_G | M_SWL_RADSTD_N | M_SWL_RADSTD_AC | M_SWL_RADSTD_AX;
    } else {
        tryToAdd = 0;
    }

    return standards | (tryToAdd & supportedStandards);
}

/**
 * Parses set of radio standards from string to bitmask.
 *
 * Takes care of specialcases in legacy notation (e.g. "bgn" vs "b,g,n").
 *
 * Also takes care of legacy vs standard format.
 * If `format` is legacy, "ac" (as entire string, not as substring) is interpreted as "a,n,ac" (excluding
 * standards not in `supportedStandards`), and "ax" is interpreted as "a,b,g,n,ac,ax" (excluding
 * unsupported standards). If `format` is standard, this is not applicable.
 *
 * @param output: result of parsing. If returning false, not written to.
 * @param input_rdStString: contains the radio standards as a string
 * @param format : format of the input_rdSTstring
 * @param supportedStandards: used when including supported lower standards, as explained above. Ignored if format is standard.
 * @param caller: name of function calling this function. Included in the log/trace messages.
 * @result: true if parsing succeeds, false if parsing fails or input_rdStString is "\0" OR "Unknown"
 */
bool swl_radStd_fromChar(
    swl_radioStandard_m* output,
    const char* input_rdStString,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards,
    const char* caller) {
    // During the first cycles, radioStandards may not have been retreived yet from the station, thus values "\0"  and "Unknown" are special and NOT WRONG
    ASSERTI_STR(input_rdStString, false, ME, "NULL");
    ASSERTI_TRUE(strcmp(input_rdStString, "Unknown"), false, ME, "Unknown");
    bool parseSuccess =
        s_fromCharInStrictlyStandardFormat(output, input_rdStString)
        || s_fromCharInStrictlyLegacyFormat(output, input_rdStString);
    if(!parseSuccess) {
        SAH_TRACEZ_ERROR(ME, "Cannot parse radio standards list '%s' (caller: %s)", input_rdStString, caller ? caller : "?");
        return false;
    }
    *output = s_addLowerStandardsIfLegacy(*output, format, supportedStandards);
    return true;
}

/**
 * Returns whether given bitmask is a valid representation of a non-empty set of 802.11 radio standards
 *
 * "auto" (i.e. `M_SWL_RADSTD_AUTO`) is considered valid if not combined with any actual radio standard.
 *
 * @param radioStandards: the potentially incorrect representation of a set of 802.11 radio standards
 * @param callerIfTrace: NULL when no error log trace should be made if `radioStandards` is not valid,
 *      otherwise the name of the caller (usually function name) to be written in such error log trace.
 */
bool swl_radStd_isValid(swl_radioStandard_m radioStandards, const char* callerIfTrace) {
    if((radioStandards & M_SWL_RADSTD_AUTO) && (radioStandards != M_SWL_RADSTD_AUTO)) {
        if(callerIfTrace != NULL) {
            SAH_TRACEZ_ERROR(ME, "swl_radStd_isValid : %#x combines \"auto\" with a standard (caller: %s)",
                             radioStandards,
                             callerIfTrace);
        }
        return false;
    }

    if((radioStandards >> SWL_RADSTD_MAX) > 0) {
        if(callerIfTrace != NULL) {
            SAH_TRACEZ_ERROR(ME, "swl_radStd_isValid : %#x has bit with unknown meaning (caller: %s)",
                             radioStandards,
                             callerIfTrace);
        }
        return false;
    }

    if(radioStandards == 0) {
        if(callerIfTrace != NULL) {
            SAH_TRACEZ_INFO(ME, "swl_radStd_isValid : empty set (caller: %s)", callerIfTrace);
        }
        return false;
    }

    return true;
}

/**
 * Like `swl_radStd_fromChar` but performs additional checks
 *
 * The additional checks are:
 * - radio standards must be supported according to given `supportedStandards`
 * - radio standards must be either "auto" with nothing else, or not contain "auto". So "auto,a" is disallowed.
 * If these checks fail, returns false and does not write to `output`.
 *
 * @param caller: name of function calling this function. Included in the log/trace messages.
 */
bool swl_radStd_fromCharAndValidate(
    swl_radioStandard_m* output,
    const char* string,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards,
    const char* caller) {
    swl_radioStandard_m radioStandards;
    bool ok = swl_radStd_fromChar(&radioStandards, string, format, supportedStandards, caller);
    if(!ok) {
        return false;
    }

    ok = swl_radStd_isValid(radioStandards, caller ? caller : "swl_radStd_fromCharAndValidate");
    if(!ok) {
        return false;
    }

    if((radioStandards != M_SWL_RADSTD_AUTO) && (radioStandards & ~supportedStandards)) {
        SAH_TRACEZ_ERROR(ME, "radio standards '%s' (%#x) contains unsupported standard (supported: %#x) (caller: %s)",
                         string,
                         radioStandards,
                         supportedStandards,
                         caller ? caller : "?");
        return false;
    }

    *output = radioStandards;
    return true;
}

/** Formats a radio standards format into a string. */
const char* swl_radStd_formatToChar(swl_radStd_format_e format) {
    return formatNotation[format];
}

/** Parses a radio standards format from a string. */
swl_radStd_format_e swl_radStd_charToFormat(const char* format) {
    return SWL_CONV_CHAR_TO_ENUM(
        format,
        formatNotation,
        SWL_RADSTD_FORMAT_MAX,
        SWL_RADSTD_FORMAT_STANDARD);
}

/** Formats a SupportedStandards set in Legacy Format. */
static bool s_supportedStandardsToCharLegacy(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m supportedStandards) {
    if(targetBufferSize == 0) {
        return false;
    }
    targetBuffer[0] = '\0';
    uint32_t i;
    for(i = 0; i < SWL_ARRAY_SIZE(radStdMasksIncludingLegacy); i++) {
        swl_radioStandard_m candidate = radStdMasksIncludingLegacy[i];
        const char* candidateStr = radStdIncludingLegacy[i];
        bool candidateIsSubMask = (candidate & supportedStandards) == candidate;
        if(candidateIsSubMask) {
            bool ok = swl_strlst_cat(targetBuffer, targetBufferSize, ",", candidateStr);
            if(!ok) {
                return false;
            }
        }
    }
    return true;
}

/**
 * Formats SupportedStandards, depending on the given `format`.
 *
 * Order is from lower standards to higher.
 */
bool swl_radStd_supportedStandardsToChar(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m supportedStandards,
    swl_radStd_format_e format) {
    if(format == SWL_RADSTD_FORMAT_LEGACY) {
        return s_supportedStandardsToCharLegacy(targetBuffer, targetBufferSize, supportedStandards);
    } else {
        return s_toCharInStandardFormat(targetBuffer, targetBufferSize, supportedStandards);
    }
}

/**
 * Parses a SupportedStandards from a string.
 *
 * This parses both Legacy and Standard Format, so both "b,g,n" and "b,g,n,bg,gn,bgn" are accepted.
 *
 * Note that "ax" does NOT mean "ax-and-lower-supported-standards", and "ac" does NOT mean
 * "ac-and-lower-supported-standards": that's only in OperatingStandards, not in in
 * SupportedStandards.
 *
 * @param tgtOutput: Result of parsing is written to here if parsing succeeds
 * @param srcString: The to-be-parsed value
 * @return Whether parsing succeeded.
 */
bool swl_radStd_supportedStandardsFromChar(
    swl_radioStandard_m* tgtOutput,
    const char* srcString) {
    ASSERT_NOT_NULL(tgtOutput, false, ME, "NULL");
    ASSERT_NOT_NULL(srcString, false, ME, "NULL");

    bool allItemsParseable = false;
    uint32_t itemsUsedBits = swl_conv_charToMaskSep(srcString, radStdIncludingLegacy,
                                                    SWL_ARRAY_SIZE(radStdIncludingLegacy), ',', &allItemsParseable);
    ASSERTS_TRUE(allItemsParseable, false, ME, "Not parseable");

    swl_radioStandard_m tmpOutput = 0;
    for(size_t i = 0; i < SWL_ARRAY_SIZE(radStdIncludingLegacy); i++) {
        if(SWL_BIT_IS_SET(itemsUsedBits, i)) {
            tmpOutput |= radStdMasksIncludingLegacy[i];
        }
    }

    ASSERTS_TRUE(swl_radStd_isValid(tmpOutput, NULL), false, ME, "Not valid %x", tmpOutput); // e.g. "auto,b"
    *tgtOutput = tmpOutput;
    return true;
}
