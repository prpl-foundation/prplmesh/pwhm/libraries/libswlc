/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include "swl/subtypes/swl_numberSType.h"

#define ME "swl_numS"

/**
 * Number function to provide mathematical operations on different data types.
 * Note that the output pointer must always be different to NULL.
 * The input pointers may be NULL, in which case the element is counted as 0. This is to allow easy
 * input of arrays with more limited input checking. Only in case of division and module is the second data arguement
 * forbidden from being NULL, as divide by 0 is not possible.
 */

/**
 * Add data1 to data2, and store it in target. Target can be one of original elements
 * Returns whether operation succeeded, potential overflows not taken into account.
 */
swl_rc_ne swl_numberSType_add(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(target, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        int64_t tmp1 = data1 != NULL ? nFun->toInt64(type, data1) : 0;
        int64_t tmp2 = data2 != NULL ? nFun->toInt64(type, data2) : 0;
        int64_t tmp3 = tmp1 + tmp2;
        return nFun->fromInt64(type, target, tmp3);
    } else {
        uint64_t tmp1 = data1 != NULL ? nFun->toUInt64(type, data1) : 0;
        uint64_t tmp2 = data2 != NULL ? nFun->toUInt64(type, data2) : 0;
        uint64_t tmp3 = tmp1 + tmp2;
        return nFun->fromUInt64(type, target, tmp3);
    }
}

/**
 * Subtract data2 from data1, and store it in target. Target can be one of original elements
 * Returns whether operation succeeded, potential overflows not taken into account.
 */
swl_rc_ne swl_numberSType_sub(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(target, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        int64_t tmp1 = data1 != NULL ? nFun->toInt64(type, data1) : 0;
        int64_t tmp2 = data2 != NULL ? nFun->toInt64(type, data2) : 0;
        int64_t tmp3 = tmp1 - tmp2;
        return nFun->fromInt64(type, target, tmp3);
    } else {
        uint64_t tmp1 = data1 != NULL ? nFun->toUInt64(type, data1) : 0;
        uint64_t tmp2 = data2 != NULL ? nFun->toUInt64(type, data2) : 0;
        uint64_t tmp3 = tmp1 - tmp2;
        return nFun->fromUInt64(type, target, tmp3);
    }
}

/**
 * Multiply data1 and data2, and store it in target. Target can be one of original elements
 * Returns whether operation succeeded, potential overflows not taken into account.
 */
swl_rc_ne swl_numberSType_mul(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(target, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data1, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data2, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        int64_t tmp1 = data1 != NULL ? nFun->toInt64(type, data1) : 0;
        int64_t tmp2 = data2 != NULL ? nFun->toInt64(type, data2) : 0;
        int64_t tmp3 = tmp1 * tmp2;
        return nFun->fromInt64(type, target, tmp3);
    } else {
        uint64_t tmp1 = data1 != NULL ? nFun->toUInt64(type, data1) : 0;
        uint64_t tmp2 = data2 != NULL ? nFun->toUInt64(type, data2) : 0;
        uint64_t tmp3 = tmp1 * tmp2;
        return nFun->fromUInt64(type, target, tmp3);
    }
}

/**
 * Divide data1 by data2, and store it in target. Target can be one of original elements
 * Returns whether operation succeeded, potential overflows not taken into account.
 * Returns SWL_RC_ERROR if having to divide by zero,
 */
swl_rc_ne swl_numberSType_div(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(target, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data2, SWL_RC_ERROR, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        int64_t tmp1 = data1 != NULL ? nFun->toInt64(type, data1) : 0;
        int64_t tmp2 = nFun->toInt64(type, data2);
        if(tmp2 == 0) {
            return SWL_RC_ERROR;
        }

        int64_t tmp3 = tmp1 / tmp2;
        return nFun->fromInt64(type, target, tmp3);
    } else {
        uint64_t tmp1 = data1 != NULL ? nFun->toUInt64(type, data1) : 0;
        uint64_t tmp2 = nFun->toUInt64(type, data2);
        if(tmp2 == 0) {
            return SWL_RC_ERROR;
        }
        uint64_t tmp3 = tmp1 / tmp2;
        return nFun->fromUInt64(type, target, tmp3);
    }
}

/**
 * Calculate data1 modulo data2, and store it in target. Target can be one of original elements
 * Returns whether operation succeeded, potential overflows not taken into account.
 * Returns SWL_RC_ERROR if having to take modulo by zero
 */
swl_rc_ne swl_numberSType_mod(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2) {
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(target, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data2, SWL_RC_ERROR, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        int64_t tmp1 = data1 != NULL ? nFun->toInt64(type, data1) : 0;
        int64_t tmp2 = nFun->toInt64(type, data2);
        if(tmp2 == 0) {
            return SWL_RC_ERROR;
        }
        int64_t tmp3 = tmp1 % tmp2;
        return nFun->fromInt64(type, target, tmp3);
    } else {
        uint64_t tmp1 = data1 != NULL ? nFun->toUInt64(type, data1) : 0;
        uint64_t tmp2 = nFun->toUInt64(type, data2);
        if(tmp2 == 0) {
            return SWL_RC_ERROR;
        }
        uint64_t tmp3 = tmp1 % tmp2;
        return nFun->fromUInt64(type, target, tmp3);
    }
}

/**
 * Return whether the provided signed integer value can be stored in an element of number type "type", without causing
 * overflow or underflow. Trying to store a negative number in an unsigned value will return false.
 */
bool swl_numberSType_isIntInRange(swl_numberType_t* type, int64_t val) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    if(nFun->isSigned) {
        if(val < 0) {
            return val >= nFun->minVal;
        } else {
            return ((uint64_t) val) <= nFun->maxVal;
        }
    } else {
        return val >= 0 && ((uint64_t) val) <= nFun->maxVal;
    }
    return true;
}

/**
 * Return whether the provided unsigned integer value can be stored in an element of number type "type", without causing overflow
 */
bool swl_numberSType_isUIntInRange(swl_numberType_t* type, uint64_t val) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");

    swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) type->typeFun->subFun;
    return val <= nFun->maxVal;
}


