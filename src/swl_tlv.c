/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swlTlv"
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include "swl/swl_common.h"
#include "swl/subtypes/swl_numberSType.h"
#include "swl/swl_tlv.h"

static swl_rc_ne s_getDataTyped(swl_tlvList_t* tlvList, void* buffer, size_t bufLen, swl_type_t* dataType, uint16_t id) {
    ASSERTS_NOT_NULL(tlvList, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(buffer, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &tlvList->tlvList) {
        swl_tlvItem_t* tlvItem = (swl_tlvItem_t*) it.data;
        if(tlvItem->tlv.id != id) {
            continue;
        }

        if(tlvItem->tlv.data == NULL) {
            SAH_TRACEZ_INFO(ME, "No data to return");
            return SWL_RC_ERROR;
        }

        ASSERTS_TRUE(tlvItem->tlv.len <= bufLen, SWL_RC_ERROR, ME, "Not enough buffer");

        if((dataType != NULL) && (dataType->typeFun != NULL) && (swl_type_getFlag(dataType) == SWL_TYPE_FLAG_NUMBER)) {
            swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) dataType->typeFun->subFun;
            if((nFun != NULL) && (nFun->switchHtoN != NULL)) {
                nFun->switchHtoN(dataType, tlvItem->tlv.data, buffer);
            }
        } else {
            memcpy(buffer, tlvItem->tlv.data, tlvItem->tlv.len);
        }
        return SWL_RC_OK;
    }
    return SWL_RC_ERROR;
}

/**
 * Return the data for a specific TLV id.
 * The dataType provided will be used to convert data when needed.
 */
swl_rc_ne swl_tlvList_getDataTyped(swl_tlvList_t* tlvList, void* buffer, swl_type_t* dataType, uint16_t id) {
    return s_getDataTyped(tlvList, buffer, dataType->size, dataType, id);
}

/**
 * Return the data for a specific TLV id.
 */
swl_rc_ne swl_tlvList_getData(swl_tlvList_t* tlvList, void* buffer, size_t bufLen, uint16_t id) {
    return s_getDataTyped(tlvList, buffer, bufLen, NULL, id);
}

/**
 * Parse a buffer in order to find one or multiple TLV.
 * Each TLV found is added into the linked list.
 */
swl_rc_ne swl_tlvList_parse(swl_tlvList_t* tlvList, char* in, size_t inLen) {
    ASSERTS_NOT_NULL(in, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(tlvList, SWL_RC_INVALID_PARAM, ME, "NULL");

    SAH_TRACEZ_INFO(ME, "Total in len %d\n", (int) inLen);
    swl_tlvElement_t* tlv = (swl_tlvElement_t*) in;
    size_t remainingLen = inLen;
    while(tlv != NULL) {
        uint16_t tlvId = SWL_BIT16_SWAP(tlv->id);
        uint16_t tlvLen = SWL_BIT16_SWAP(tlv->len);

        SAH_TRACEZ_INFO(ME, "Parsing TLV %d len %d remaining %d in %d\n", tlvId, tlvLen, (int) remainingLen, (int) inLen);
        if(tlvLen == 0) {
            SAH_TRACEZ_ERROR(ME, "Invalid len 0\n");
            return SWL_RC_ERROR;
        }

        swl_tlvList_append(tlvList, tlvId, tlvLen, ((void*) tlv + SWL_TLV_HDR_LEN));

        remainingLen -= SWL_TLV_HDR_LEN;

        if(tlvLen > remainingLen) {
            SAH_TRACEZ_ERROR(ME, "Invalid len %d\n", (int) tlvLen);
            return SWL_RC_ERROR;
        } else if(tlvLen == remainingLen) {
            tlv = (swl_tlvElement_t*) &tlv->data;
        } else {
            remainingLen -= tlvLen;
            tlv = (swl_tlvElement_t*) ((void*) &tlv->data + tlvLen);
        }
        if(remainingLen <= SWL_TLV_HDR_LEN) {
            break;
        }
    }
    return SWL_RC_OK;
}

/**
 * Create a new TLV with an id a len and a pointer to a data.
 * The dataType provided will be used to convert data when needed.
 * This new TLV is added at the beginning of the linked list.
 */
void swl_tlvList_prependType(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data, swl_type_t* dataType) {
    ASSERTS_NOT_NULL(tlvList, , ME, "NULL");
    swl_tlvItem_t tlvItem;
    tlvItem.tlv.id = id;
    tlvItem.tlv.len = len;
    tlvItem.tlv.data = data;
    tlvItem.dataType = dataType;
    swl_unLiList_insert(&tlvList->tlvList, 0, &tlvItem);
}

/**
 * Create a new TLV with an id a len and a pointer to a data.
 * This new TLV is added at the beginning of the linked list.
 */
void swl_tlvList_prepend(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data) {
    ASSERTS_NOT_NULL(tlvList, , ME, "NULL");
    swl_tlvList_prependType(tlvList, id, len, data, NULL);
}

/**
 * Create a new TLV with an id a len and a pointer to a data.
 * The dataType provided will be used to convert data when needed.
 * This new TLV is added to the end of the linked list.
 */
void swl_tlvList_appendType(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data, swl_type_t* dataType) {
    ASSERTS_NOT_NULL(tlvList, , ME, "NULL");
    swl_tlvItem_t tlvItem;
    tlvItem.tlv.id = id;
    tlvItem.tlv.len = len;
    tlvItem.tlv.data = data;
    tlvItem.dataType = dataType;
    swl_unLiList_add(&tlvList->tlvList, &tlvItem);
}

/**
 * Create a new TLV with an id a len and a pointer to a data.
 * This new TLV is added to the end of the linked list.
 */
void swl_tlvList_append(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data) {
    ASSERTS_NOT_NULL(tlvList, , ME, "NULL");
    swl_tlvList_appendType(tlvList, id, len, data, NULL);
}

size_t swl_tlvList_getTotalSize(swl_tlvList_t* tlvList) {
    ASSERTS_NOT_NULL(tlvList, 0, ME, "NULL");

    size_t totalLen = 0;
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &tlvList->tlvList) {
        swl_tlvItem_t* tlvItem = (swl_tlvItem_t*) it.data;
        totalLen += SWL_TLV_HDR_LEN;
        if(tlvItem->tlv.data != NULL) {
            totalLen += tlvItem->tlv.len;
        }
    }

    return totalLen;
}

/**
 * Serialize a linked list of TLVs into a buffer
 */
swl_rc_ne swl_tlvList_toBuffer(swl_tlvList_t* tlvList, char* out, size_t outLen, size_t* remainingLen) {
    ASSERTS_NOT_NULL(out, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(remainingLen, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERTS_NOT_NULL(tlvList, SWL_RC_INVALID_PARAM, ME, "NULL");

    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &tlvList->tlvList) {
        swl_tlvItem_t* tlvItem = (swl_tlvItem_t*) it.data;

        size_t dataLen = (tlvItem->tlv.data != NULL) ? tlvItem->tlv.len : 0;
        size_t idLen = ((2 * sizeof(uint16_t) + dataLen));
        if((idLen > *remainingLen) || (idLen > outLen)) {
            return SWL_RC_ERROR;
        }

        uint16_t id = SWL_BIT16_SWAP(tlvItem->tlv.id);
        memcpy(out, &id, sizeof(uint16_t));
        out += sizeof(uint16_t);
        *remainingLen -= sizeof(uint16_t);

        uint16_t len = SWL_BIT16_SWAP(tlvItem->tlv.len);
        memcpy(out, &len, sizeof(uint16_t));
        out += sizeof(uint16_t);
        *remainingLen -= sizeof(uint16_t);

        if(tlvItem->tlv.data == NULL) {
            continue;
        }

        swl_type_t* dataType = tlvItem->dataType;
        if((dataType != NULL) && (dataType->typeFun != NULL) && (swl_type_getFlag(tlvItem->dataType) == SWL_TYPE_FLAG_NUMBER)) {
            swl_numberSTypeFun_t* nFun = (swl_numberSTypeFun_t*) dataType->typeFun->subFun;
            if((nFun != NULL) && (nFun->switchHtoN != NULL)) {
                nFun->switchHtoN(dataType, tlvItem->tlv.data, out);
            }
        } else {
            memcpy(out, tlvItem->tlv.data, tlvItem->tlv.len);
        }
        out += tlvItem->tlv.len;
        *remainingLen -= tlvItem->tlv.len;
    }

    return SWL_RC_OK;
}

void swl_tlvList_init(swl_tlvList_t* tlvList) {
    swl_unLiList_init(&tlvList->tlvList, sizeof(swl_tlvItem_t));
}

void swl_tlvList_cleanup(swl_tlvList_t* tlvList) {
    ASSERTS_NOT_NULL(tlvList, , ME, "NULL");
    swl_unLiList_destroy(&tlvList->tlvList);
}
