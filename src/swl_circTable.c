/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_circTable.h"

#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "swl/swl_math.h"


#define ME "swlCTbl"

/**
 * This module implements a circular table, i.e. a circular buffer of tuples,
 * which belong to a given tupleType.
 */


/**
 * Initialize a circular table
 * @param table: the circular table to initialize
 * @param tupleType: the tuple type for this table
 * @param size: the size of the table, must be greater than zero
 * @return whether init was successful
 */
swl_rc_ne swl_circTable_init(swl_circTable_t* table, swl_tupleType_t* tupleType, size_t size) {
    ASSERT_NOT_NULL(table, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(tupleType, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_TRUE(size > 0, SWL_RC_INVALID_PARAM, ME, "SIZE");
    memset(table, 0, sizeof(swl_circTable_t));
    swl_table_initMax(&table->table, tupleType, size);

    return SWL_RC_OK;
}

/**
 * Set the names to be used in printing
 * @param table: the table for which the names must be set
 * @param names: the names to be used. Size must be at least equal to table size. Individual values may be NULL,
 * in which case the index of the tuple will be used.
 * names may also be NULL to remove assignment of previous names. Pointer shall be stored, data will NOT be copied.
 */
void swl_circTable_setNames(swl_circTable_t* table, char** names) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    table->names = names;
}

/**
 * Adds values to the circular table
 * @param table: the table to which to add values
 * @param data: a data tuple. Must match the circTable tupleType
 */
void swl_circTable_addValues(swl_circTable_t* table, swl_tuple_t* data) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    size_t nrTuples = table->table.nrTuples;
    table->table.curNrTuples = SWL_MIN(table->table.curNrTuples + 1, nrTuples);
    swl_table_setTuple(&table->table, table->nextFill, data);
    table->nextFill = (table->nextFill + 1 ) % nrTuples;
}

/**
 * Resets the circular table. Resetting clears all values, providing an empty circular table.
 * @param table: the table to be reset
 */
void swl_circTable_reset(swl_circTable_t* table) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    swl_table_clear(&table->table);

    table->nextFill = 0;
    table->table.curNrTuples = 0;
}

/**
 * Resizes the circular table. This will also reset all data.
 * @param table: the table to reset
 * @param size: the new size of the table. Must be greater than 0
 */
void swl_circTable_resize(swl_circTable_t* table, size_t size) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_TRUE(size > 0, , ME, "SIZE");
    swl_table_resize(&table->table, size, false);

    table->nextFill = 0;
    table->table.curNrTuples = 0;
}

/**
 * Destroys the circular table. This will clean up all tuples it contains, and free all relevant memory.
 * @param table: the table to destroy.
 * @return whether destroy was successful
 */
swl_rc_ne swl_circTable_destroy(swl_circTable_t* table) {
    ASSERT_NOT_NULL(table, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_table_destroy(&table->table);

    table->nextFill = 0;
    table->table.curNrTuples = 0;
    return SWL_RC_OK;
}

/**
 * Returns whether the table is completely filled,
 * @param table: the table to check if it's filled.
 * @return whether values were added equal or more times than the size. If table is NULL, false is returned
 */
bool swl_circTable_isFilled(swl_circTable_t* table) {
    ASSERT_NOT_NULL(table, false, ME, "NULL");
    return table->table.curNrTuples == table->table.nrTuples;
}

/**
 * Returns the number of values currently added to this circular table
 * @param table: the table for which to get the number of filled entries
 * @return the number of values added to this circular table
 */
size_t swl_circTable_getNrFilled(swl_circTable_t* table) {
    ASSERT_NOT_NULL(table, 0, ME, "NULL");
    return table->table.curNrTuples;
}

/**
 * Writes a given column to an array. Oldest values will always go first.
 * If array is not large enough, latest values will be added.
 *
 * @param array: array of type elements, of the type that matches tupleType[typeIndex]
 * @param arraySize: the size of array, in number of elements
 * @param table: the table to dump
 * @param typeIndex: the index of the type in the tupleType
 */
void swl_circTable_columnToArray(swl_typeEl_t* array, size_t arraySize, swl_circTable_t* table, size_t typeIndex) {
    ASSERT_NOT_NULL(table, , ME, "NULL");
    ASSERT_NOT_NULL(array, , ME, "NULL");
    size_t nrToFill = SWL_MIN(arraySize, table->table.curNrTuples);
    if(!swl_circTable_isFilled(table)) {
        swl_table_columnToArrayOffset(array, nrToFill, &table->table, typeIndex, table->table.curNrTuples - nrToFill);
    } else {
        swl_table_columnToArrayOffset(array, arraySize, &table->table, typeIndex, SWL_ARRAY_INDEX_SUB(table->nextFill, nrToFill, table->table.curNrTuples));
    }
}

/**
 * returns the unsigned index of a given index.
 * If index is invalid, table->table.curNrTuples is returned, as excess element
 */
static size_t s_indexToTableIndex(swl_circTable_t* table, ssize_t index) {
    size_t nrVals = table->table.nrTuples;
    size_t fillVals = table->table.curNrTuples;
    size_t baseIndex = SWL_ARRAY_INDEX(index, fillVals);

    if(baseIndex >= fillVals) {
        return fillVals;
    } else if(fillVals < nrVals) {
        return baseIndex;
    } else {
        size_t targetIndex = baseIndex + table->nextFill;
        if(targetIndex >= nrVals) {
            targetIndex -= nrVals;
        }
        return targetIndex;
    }
}

/**
 * Returns the tuple of the table at the given index.
 * @param table: the table from which to get the tuple
 * @param tupleIndex: the index of the tuple, as seen in circBuffer (i.e. 0 is always oldest, nrFilled -1 newest);
 *      TupleIndex may also be negative, but SWL_ARRAY_INDEX should resolve to a proper index.
 * @returns the tuple on that cycle index, with 0 being oldest, and nrFilled -1 being the newes
 */
swl_tuple_t* swl_circTable_getTuple(swl_circTable_t* table, ssize_t tupleIndex) {
    ASSERT_NOT_NULL(table, NULL, ME, "NULL");
    ASSERTI_TRUE(table->table.curNrTuples > 0, NULL, ME, "NULL");
    size_t index = s_indexToTableIndex(table, tupleIndex);
    ASSERTI_TRUE(index < table->table.curNrTuples, NULL, ME, "OVERFLOW");

    return swl_table_getTuple(&table->table, index);
}

/**
 * Returns a pointer to the element in the table. Note that referenced values are not dereferenced.
 *
 * @param table: the table from which to get the reference
 * @param tupleIndex: the index of the tuple, as seen in circBuffer (i.e. 0 is always oldest, nrFilled -1 newest)
 * @param typeIndex : the index of the type in the tupleType struct
 * @returns the type element pointer that is present at the given location, NULL if not available
 */
swl_typeEl_t* swl_circTable_getReference(swl_circTable_t* table, ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(table, NULL, ME, "NULL");
    ASSERTI_TRUE(table->table.curNrTuples > 0, NULL, ME, "NULL");
    size_t index = s_indexToTableIndex(table, tupleIndex);
    ASSERTI_TRUE(index < table->table.curNrTuples, NULL, ME, "OVERFLOW");
    return swl_table_getReference(&table->table, index, typeIndex);
}

/**
 * Returns a pointer to the data in the table. Note that referenced values are not dereferenced.
 *
 * @param table: the table from which to get the reference
 * @param tupleIndex: the index of the tuple, as seen in circBuffer (i.e. 0 is always oldest, nrFilled -1 newest)
 * @param typeIndex : the index of the type in the tupleType struct
 * @returns the type data pointer that is present at the given location, NULL if not available
 */
swl_typeData_t* swl_circTable_getValue(swl_circTable_t* table, ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(table, NULL, ME, "NULL");
    ASSERT_TRUE(table->table.curNrTuples > 0, NULL, ME, "NULL");
    size_t index = s_indexToTableIndex(table, tupleIndex);
    ASSERT_TRUE(index < table->table.curNrTuples, NULL, ME, "OVERFLOW");
    return swl_table_getValue(&table->table, index, typeIndex);
}

/**
 * Returns the first tuple in table, where the srcIndex element of the tuple equals srcValue.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param srcValue: the data to check
 *
 * @return the first swl_tuple_t, which is equal according to type with srcValue, for the given srcIndex,
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_circTable_getMatchingTuple(swl_circTable_t* table, size_t srcIndex, const swl_typeData_t* srcValue) {
    ASSERT_NOT_NULL(table, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, NULL, ME, "NULL");
    return swl_table_getMatchingTuple(&table->table, srcIndex, srcValue);
}

/**
 * Returns the first encountered tuple in the table in the direction of search, where srcIndex element of the tuple equals srcValue.
 * {startElem, endElem, stepSize} define the (sub)set and direction of the search. if needed, wraps around the first or last element, at most once.
 *
 * @param table: the table which to check for a match
 * @param srcIndex: the type index of the tuple, which matches the type of srcValue
 * @param srcValue: the data to check
 * @param startElem: first element to check in the table
 * @param endElem: last element to check in the table
 * @param stepSize: signed int, delta between two consecutive indexes to be checked
 *
 * @return the last swl_tuple_t, which is equal according to type with srcValue, for the given type[srcIndex],
 * NULL if no such tuple exists
 */
swl_tuple_t* swl_circTable_getMatchingTupleInRange(swl_circTable_t* table, size_t srcIndex, const swl_typeData_t* srcValue,
                                                   ssize_t startElem, ssize_t endElem, ssize_t stepSize) {
    ASSERT_NOT_NULL(table, NULL, ME, "NULL");
    ASSERT_NOT_NULL(srcValue, NULL, ME, "NULL");

    // [0  1  2  3  4  5  6  7  8 10 11 12 13]    // count of written elements
    //          [0  1  2  3  4  5  6  7  8  9]    // for a buffer of MAX_SIZE 10, direct index of elements in circular buffer
    // [0  1  2  3  4  5  6  7  8  9][0  1  2 ..] // index of elements in the underlying linear table; linear table copied to
    //           A                          B     // indicate the wraparound

    size_t indexA = s_indexToTableIndex(table, startElem);
    size_t indexB = s_indexToTableIndex(table, endElem);
    size_t arraySize = table->table.curNrTuples;

    ASSERT_TRUE((stepSize != 0), NULL, ME, "stepSize should not be 0 (curr %zd)", stepSize);
    ASSERT_TRUE((indexA < arraySize), NULL, ME, "startElem %zd out of range", startElem);
    ASSERT_TRUE((indexB < arraySize), NULL, ME, "endElem %zd out of range", endElem);


    return swl_table_getMatchingTupleInRange(&table->table, srcIndex, srcValue, (ssize_t) indexA, (ssize_t) indexB, stepSize);
}
