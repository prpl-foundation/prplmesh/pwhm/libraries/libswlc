/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_tupleType.h"
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "swl/types/swl_collType.h"

#define ME "swlTpl"

/**
 * Swl library implementing a tupleType. A tupleType defines an ordered set of values
 * that each are of a certain swl_type.
 * Basically it provides an abstraction for a struct
 */

/**
 * Return the offset of the element at a given index, within the type.
 */
size_t swl_tupleType_getOffset(swl_tupleType_t* type, size_t index) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    if(type->offsets != NULL) {
        return type->offsets[index];
    } else {
        size_t offset = 0;
        for(size_t i = 0; i < index; i++) {
            offset += type->types[i]->size;
        }
        return offset;
    }
}

/**
 * Check whether the type is valid, and update the element size if NULL.
 * This function should be called before all tupleType functions except getOffset
 */
bool swl_tupleType_check(swl_tupleType_t* type) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    if(type->type.size != 0) {
        return true;
    }
    //element size still zero, so need to calculate it;
    type->type.size = swl_tupleType_getOffset(type, type->nrTypes);
    ASSERT_TRUE(type->type.size != 0, false, ME, "ZERO");
    return true;
}


swl_typeEl_t* swl_tupleType_getReference(swl_tupleType_t* type, const swl_tuple_t* tuple, size_t index) {
    ASSERT_NOT_NULL(tuple, NULL, ME, "NULL");
    ASSERTI_TRUE(swl_tupleType_check(type), NULL, ME, "INVALID");
    ASSERT_TRUE(index < type->nrTypes, NULL, ME, "SIZE %zu %zu", index, type->nrTypes);
    size_t srcOffset = swl_tupleType_getOffset(type, index);
    return (swl_typeEl_t*) tuple + srcOffset;
}

/**
 * Retrieve the element value at the given index of the given tuple
 * For value types, a pointer to the data is provided, for reference types, the data itself is provided.
 * The relevant tuple list is required, to have the tuple definition.
 */
swl_typeData_t* swl_tupleType_getValue(swl_tupleType_t* type, const swl_tuple_t* tuple, size_t index) {
    swl_typeEl_t* data = swl_tupleType_getReference(type, tuple, index);
    ASSERTS_NOT_NULL(data, NULL, ME, "NULL");
    return swl_type_toPtr(type->types[index], data);
}


/**
 * Returns the size of this tuple time, i.e. the amount of bytes a tuple of this type requires.
 */
size_t swl_tupleType_size(swl_tupleType_t* type) {
    ASSERTI_TRUE(swl_tupleType_check(type), 0, ME, "NULL");
    return type->type.size;
}

/**
 * Check whether two tuples are equal.
 * They are considered equal if each element of tuple 1 equals each element of tuple 2, by type equals comparison.
 */
bool swl_tupleType_equals(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* val1 = swl_tupleType_getValue(tplType, tuple1, i);
        swl_typeData_t* val2 = swl_tupleType_getValue(tplType, tuple2, i);
        ASSERTS_TRUE(swl_type_equals(type, val1, val2), false, ME, "MISMATCH");
    }
    return true;
}

/**
 * Clean up the element at given index, resetting the value to zero, and making it count as empty.
 */
void swl_tupleType_cleanupIndex(swl_tupleType_t* tplType, swl_tuple_t* tuple, size_t index) {
    swl_typeEl_t* data = swl_tupleType_getReference(tplType, tuple, index);
    swl_type_cleanup(tplType->types[index], data);
}

/**
 * Cleanup the tuple
 * If type mask is 0, no references are cleaned.
 * Note that this does NOT free the tuple itself, only all references.
 * It will then set all matched references to NULL. It will not set values to zero.
 */
void swl_tupleType_cleanupByMask(swl_tupleType_t* tplType, swl_tuple_t* tuple, size_t typeMask) {
    for(size_t i = 0; i < tplType->nrTypes; i++) {
        if(SWL_BIT_IS_SET(typeMask, i)) {
            swl_tupleType_cleanupIndex(tplType, tuple, i);
        }
    }
}




/**
 * Cleanup the tuple
 * Note that this does NOT free the tuple itself, only all references.
 * It will then set all references to NULL. It will not set values to zero.
 */
void swl_tupleType_cleanup(swl_tupleType_t* tplType, swl_tuple_t* tuple) {
    swl_tupleType_cleanupByMask(tplType, tuple, -1);
}

/**
 * Copy a tuple to a target tuple by mask.
 * If type mask is 0, then no masks are cleaned.
 *
 */
void swl_tupleType_copyByMask(swl_tuple_t* targetTuple, swl_tupleType_t* tplType, const swl_tuple_t* tuple, size_t typeMask) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), , ME, "NULL");
    ASSERTI_NOT_NULL(tuple, , ME, "NULL");
    ASSERTI_NOT_NULL(targetTuple, , ME, "NULL");

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        if(SWL_BIT_IS_SET(typeMask, i)) {
            swl_typeData_t* srcData = swl_tupleType_getValue(tplType, tuple, i);
            swl_typeEl_t* tgtEl = swl_tupleType_getReference(tplType, targetTuple, i);
            swl_type_copyTo(tplType->types[i], tgtEl, srcData);
        }
    }
}

/**
 * Check whether two tuples are equal over a given mask.
 * They are considered equal if each element of which the index is set in the typemask
 * of tuple 1 equals each element of tuple 2, by type equals comparison.
 * So if typeMask is 0, all things match.
 * If one tuple is NULL, then this function will return true only if both tuples are NULL
 */
bool swl_tupleType_equalsByMask(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2, size_t typeMask) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), false, ME, "NULL");
    if((tuple1 == NULL) || (tuple2 == NULL)) {
        return tuple1 == tuple2;
    }

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        if(SWL_BIT_IS_SET(typeMask, i)) {
            swl_type_t* type = tplType->types[i];
            swl_typeData_t* val1 = swl_tupleType_getValue(tplType, tuple1, i);
            swl_typeData_t* val2 = swl_tupleType_getValue(tplType, tuple2, i);
            ASSERTS_TRUE(swl_type_equals(type, val1, val2), false, ME, "MISMATCH");
        }
    }
    return true;
}

/**
 * Return the difference mask between two tuples.
 * It will return 0 if all types in the tuple type are equal.
 * So for each bit index that is set swl_type_equals(type, val1, val2) is false, while
 * for each bit index that is not set, swl_type_equals(type, val1, val2) is true.
 */
size_t swl_tupleType_getDiffMask(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2) {
    ASSERTI_TRUE(swl_tupleType_check(tplType), 0, ME, "NULL");
    if((tuple1 == NULL) || (tuple2 == NULL)) {
        return tuple1 == tuple2;
    }

    size_t retMask = 0;

    for(size_t i = 0; i < tplType->nrTypes; i++) {
        swl_type_t* type = tplType->types[i];
        swl_typeData_t* val1 = swl_tupleType_getValue(tplType, tuple1, i);
        swl_typeData_t* val2 = swl_tupleType_getValue(tplType, tuple2, i);
        if(!swl_type_equals(type, val1, val2)) {
            W_SWL_BIT_SET(retMask, i);
        }
    }
    return retMask;
}

static ssize_t s_tupleType_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    swl_tupleType_t* tt = (swl_tupleType_t*) type;
    ssize_t curSize = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_LIST_OPEN]);
    ssize_t totalSize = curSize;
    ssize_t maxSize = tgtStrSize;
    for(size_t i = 0; i < tt->nrTypes && curSize > 0; i++) {
        swl_typeData_t* data = swl_tupleType_getValue(tt, srcData, i);
        if(data != NULL) {
            ssize_t elSize = swl_type_toCharEsc(tt->types[i], &tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize),
                                                data, args);
            ASSERT_TRUE(elSize >= 0, elSize, ME, "ERR");
            totalSize += elSize;
        }

        if(i < tt->nrTypes - 1) {
            ssize_t nextSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_NEXT]);
            ASSERT_TRUE(nextSize > 0, nextSize, ME, "ERR");
            totalSize += nextSize;
        }
    }

    curSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_CLOSE]);
    if(curSize < 0) {
        return curSize;
    }
    totalSize += curSize;
    return totalSize;
}

static bool s_tupleType_fromCharExt_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {
    swl_tupleType_t* tt = (swl_tupleType_t*) type;

    size_t index = strlen(args->delim[SWL_PRINT_DELIM_LIST_OPEN]);

    for(size_t i = 0; i < tt->nrTypes; i++) {
        swl_typeEl_t* el = swl_tupleType_getReference(tt, tgtData, i);
        ssize_t elSize = swl_type_fromCharPrint(tt->types[i], el, &srcStr[index], args, SWL_PRINT_DELIM_LIST_NEXT);
        ASSERT_TRUE(elSize >= 0, false, ME, "FAIL");

        index = index + elSize + strlen(args->delim[SWL_PRINT_DELIM_LIST_NEXT]);
    }

    return true;
}

static bool s_tupleType_fromChar_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    return s_tupleType_fromCharExt_cb(type, tgtData, srcStr, args);
}


static bool s_tupleType_equals_cb(swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1) {
    swl_tupleType_t* tt = (swl_tupleType_t*) type;
    for(size_t i = 0; i < tt->nrTypes; i++) {
        if(!swl_type_equals(tt->types[i], swl_tupleType_getValue(tt, key0, i), swl_tupleType_getValue(tt, key1, i))) {
            return false;
        }
    }
    return true;
}


static swl_typeData_t* s_tupleType_copy_cb(swl_type_t* type, const swl_typeData_t* src) {
    swl_tupleType_t* tt = (swl_tupleType_t*) type;
    swl_tuple_t* data = calloc(1, tt->type.size);

    ASSERT_NOT_NULL(data, NULL, ME, "ALLOC FAIL");

    for(size_t i = 0; i < tt->nrTypes; i++) {
        swl_type_copyTo(tt->types[i], swl_tupleType_getReference(tt, data, i), swl_tupleType_getValue(tt, src, i));
    }
    return (swl_typeData_t*) data;
}

static bool s_tupleType_toFile_cb(swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    swl_tupleType_t* tt = (swl_tupleType_t*) type;
    swl_print_addOpen(file, args, true);

    for(size_t i = 0; i < tt->nrTypes; i++) {
        ASSERT_TRUE(swl_type_toFilePrivPrint(tt->types[i], file, swl_tupleType_getValue(tt, srcData, i), args),
                    false, ME, "FAIL");

        if(i < tt->nrTypes - 1) {
            swl_print_addNext(file, args, true);
        }
    }
    swl_print_addClose(file, args, true);

    return true;
}


static void s_tupleType_cleanup_cb(swl_type_t* type, swl_typeEl_t* tgtData) {
    swl_tupleType_t* tt = (swl_tupleType_t*) type;
    for(size_t i = 0; i < tt->nrTypes; i++) {
        swl_typeEl_t* ref = swl_tupleType_getReference(tt, tgtData, i);
        swl_type_cleanup(tt->types[i], ref);
    }
}


static swl_rc_ne s_col_init_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    memset(inCollection, 0, tt->type.size);
    return SWL_RC_OK;
}

static swl_rc_ne s_col_initFromArray_cb(swl_listSType_t* colType, swl_coll_t* inCollection, swl_typeEl_t* src, size_t arraySize) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    memset(inCollection, 0, tt->type.size);

    for(size_t i = 0; i < tt->nrTypes && i < arraySize; i++) {
        swl_type_copyTo(tt->types[i], swl_tupleType_getReference(tt, inCollection, i), swl_tupleType_getValue(tt, src, i));
    }
    return SWL_RC_OK;
}

static size_t s_col_maxSize_cb(swl_listSType_t* colType, swl_coll_t* inCollection _UNUSED) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    return tt->nrTypes;
}

static void s_col_clear_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    s_tupleType_cleanup_cb(&tt->type, inCollection);
}

static void s_col_cleanup_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    s_tupleType_cleanup_cb(&tt->type, inCollection);
}

/**
 * If empty is allowed, size will return the number of non empty elements.
 */
static size_t s_col_size_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    size_t nrNotEmpty = 0;
    for(size_t i = 0; i < tt->nrTypes; i++) {
        if(!swl_type_isEmpty(tt->types[i], swl_tupleType_getValue(tt, inCollection, i))) {
            nrNotEmpty++;
        }
    }

    return nrNotEmpty;
}
static ssize_t s_col_add_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED, swl_typeData_t* data _UNUSED) {
    return SWL_RC_NOT_IMPLEMENTED; // Can not add values to a tuple type.
}
static swl_typeEl_t* s_col_alloc_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED) {
    return NULL; // Can not alloc values to a tuple type.
}

static swl_rc_ne s_col_insert_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED, swl_typeData_t* data _UNUSED, ssize_t index _UNUSED) {
    return SWL_RC_NOT_IMPLEMENTED; // Can not insert data in a tuple type
}

static swl_rc_ne s_col_set_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    size_t targetIndex = SWL_ARRAY_INDEX(index, tt->nrTypes);
    ASSERT_TRUE(targetIndex < tt->nrTypes, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_typeEl_t* ref = swl_tupleType_getReference(tt, inCollection, targetIndex);

    swl_type_copyTo(tt->types[targetIndex], ref, data);
    return SWL_RC_OK;
}

/**
 * For array type, delete shall always try to remove a non-empty element. Other elements do not count
 * for an array that has a fixed maxSize.
 */
static swl_rc_ne s_col_delete_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;

    size_t targetIndex = SWL_ARRAY_INDEX(index, tt->nrTypes);
    ASSERT_TRUE(targetIndex < tt->nrTypes, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_typeEl_t* ref = swl_tupleType_getReference(tt, inCollection, targetIndex);
    swl_type_cleanup(tt->types[targetIndex], ref);
    return SWL_RC_OK;
}

static ssize_t s_col_find_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED, swl_typeData_t* data _UNUSED) {
    return SWL_RC_NOT_IMPLEMENTED; // Finding data of "any" type is not allowed for tuple type.
}

static bool s_col_equals_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection1, swl_coll_t* inCollection2) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    return swl_tupleType_equals(tt, inCollection1, inCollection2);
}

static swl_typeData_t* s_col_getValue_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    size_t targetIndex = SWL_ARRAY_INDEX(index, tt->nrTypes);
    ASSERT_TRUE(targetIndex < tt->nrTypes, NULL, ME, "SIZE");
    return swl_tupleType_getValue(tt, inCollection, targetIndex);
}

static swl_typeEl_t* s_col_getReference_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    size_t targetIndex = SWL_ARRAY_INDEX(index, tt->nrTypes);
    ASSERT_TRUE(targetIndex < tt->nrTypes, NULL, ME, "SIZE");
    return swl_tupleType_getReference(tt, inCollection, targetIndex);
}

static inline void s_col_setItData(swl_tupleType_t* tt, swl_listSTypeIt_t* it) {
    if(it->index < tt->nrTypes) {
        it->data = swl_tupleType_getReference(tt, it->coll, it->index);
        it->valid = true;
        it->dataType = tt->types[it->index];
    } else {
        it->data = NULL;
        it->valid = false;
        it->dataType = NULL;
    }
}

static swl_listSTypeIt_t s_col_getFirstIt_cb(swl_listSType_t* colType _UNUSED, const swl_coll_t* inCollection) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    swl_listSTypeIt_t data;
    memset(&data, 0, sizeof(swl_listSTypeIt_t));
    data.coll = (swl_coll_t*) inCollection;
    data.index = 0;
    data.block = (swl_coll_t*) inCollection;
    s_col_setItData(tt, &data);
    return data;
}
static void s_col_nextIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    swl_tupleType_t* tt = (swl_tupleType_t*) colType;
    it->index++;
    s_col_setItData(tt, it);
}

static void s_col_delIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    if(it->valid) {
        swl_type_cleanup(it->dataType, it->data);
        it->valid = false;
    }
}

swl_listSTypeFun_t swl_tupleTypeCollType_fun = {
    .name = "swl_tupleType",
    .init = s_col_init_cb,
    .initFromArray = s_col_initFromArray_cb,
    .clear = s_col_clear_cb,
    .cleanup = s_col_cleanup_cb,
    .maxSize = s_col_maxSize_cb,
    .size = s_col_size_cb,
    .add = s_col_add_cb,
    .alloc = s_col_alloc_cb,
    .insert = s_col_insert_cb,
    .set = s_col_set_cb,
    .delete = s_col_delete_cb,
    .find = s_col_find_cb,
    .equals = s_col_equals_cb,
    .getValue = s_col_getValue_cb,
    .getReference = s_col_getReference_cb,
    .firstIt = s_col_getFirstIt_cb,
    .nextIt = s_col_nextIt_cb,
    .delIt = s_col_delIt_cb,
};


swl_typeFun_t swl_tupleType_fun = {
    .name = "swl_tupleType",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_LIST,
    .toChar = (swl_type_toChar_cb) s_tupleType_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_tupleType_fromChar_cb,
    .fromCharExt = (swl_type_fromCharExt_cb) s_tupleType_fromCharExt_cb,
    .equals = (swl_type_equals_cb) s_tupleType_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_tupleType_cleanup_cb,
    .copy = (swl_type_copy_cb) s_tupleType_copy_cb,
    .toFile = (swl_type_toFile_cb) s_tupleType_toFile_cb,
    .subFun = &swl_tupleTypeCollType_fun,
};
