/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "swl/swl_common_type.h"
#include "swl/swl_common_trilean.h"
#include "swl/types/swl_trileanType.h"

#define ME "swlTrlT"

/**
 * enum handlers
 */
static ssize_t s_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    ASSERTS_NOT_NULL(type, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");
    swl_trlType_t* trlType = (swl_trlType_t*) type;
    swl_trl_e trl = *(swl_trl_e*) srcData;

    ASSERT_TRUE(trl < SWL_TRL_MAX, 0, ME, "Excess value %u / %u", trl, SWL_TRL_MAX);

    const char* tgtCharValue = NULL;
    bool success = swl_trl_toChar(&tgtCharValue, trl, trlType->fmt);
    if(!success) {
        return -EINVAL;
    }

    return snprintf(tgtStr, tgtStrSize, "%s", tgtCharValue);
}

static bool s_fromChar_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");

    swl_trlType_t* trlType = (swl_trlType_t*) type;
    swl_trl_e* trl = (swl_trl_e*) tgtData;

    bool ret = swl_trl_fromChar(trl, srcStr, trlType->fmt);
    return ret;
}

swl_typeFun_t swl_trlTypeChar_fun = {
    .isValue = true,
    .flag = SWL_TYPE_FLAG_VALUE,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_fromChar_cb,
};

swl_typeFun_t swl_trlTypeNum_fun = {
    .isValue = true,
    .flag = SWL_TYPE_FLAG_NUMBER,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_fromChar_cb,
};

swl_typeFun_t swl_trlTypeJson_fun = {
    .isValue = true,
    .flag = SWL_TYPE_FLAG_BASIC,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_fromChar_cb,
};

#define SWL_TRL_TYPE_DEF_STRUCT(name, format, myFun) \
    swl_trlType_t name = { \
        .type = { \
            .size = sizeof(swl_trl_e), \
            .typeFun = &myFun, \
        }, \
        .fmt = format, \
    };

SWL_TRL_TYPE_DEF_STRUCT(gtSwl_trl, SWL_TRL_FORMAT_UNKNOWN, swl_trlTypeChar_fun);

SWL_TRL_TYPE_DEF_STRUCT(gtSwl_trlAuto, SWL_TRL_FORMAT_AUTO, swl_trlTypeChar_fun);
SWL_TRL_TYPE_DEF_STRUCT(gtSwl_trlUnknown, SWL_TRL_FORMAT_UNKNOWN, swl_trlTypeChar_fun);
SWL_TRL_TYPE_DEF_STRUCT(gtSwl_trlNum, SWL_TRL_FORMAT_NUMERIC, swl_trlTypeNum_fun);
SWL_TRL_TYPE_DEF_STRUCT(gtSwl_trlJson, SWL_TRL_FORMAT_JSON, swl_trlTypeJson_fun);



