/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "swl/types/swl_vectorType.h"

#define ME "swlV_T"

/**
 * to char function, to convert a given vector to string, for type purposes.
 */
static ssize_t s_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_vector_t* srcData) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    swl_vectorType_t* vectorType = (swl_vectorType_t*) type;

    ssize_t curSize = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_LIST_OPEN]);
    ssize_t maxSize = tgtStrSize;
    ssize_t totalSize = curSize;

    swl_type_t* elementType = vectorType->type.elementType;

    size_t vectorSize = swl_vector_size(srcData);

    swl_vector_forEachRef(it, swl_typeEl_t, elData, srcData) {
        swl_typeData_t* data = SWL_TYPE_EL_TO_DATA(elementType, elData);

        bool dataCanBePrinted = true;
        swl_typeFlag_e elementFlag = swl_type_getFlag(elementType);

        if(data == NULL) {
            dataCanBePrinted = false;
            if(args->nullEncode != NULL) {
                ssize_t nullSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->nullEncode);
                ASSERT_TRUE(nullSize > 0, nullSize, ME, "ERR");
                totalSize += nullSize;
            }
        } else if((elementFlag == SWL_TYPE_FLAG_LIST) || (elementFlag == SWL_TYPE_FLAG_MAP)) {
            dataCanBePrinted = !swl_type_isEmpty(elementType, data);
        }
        if(dataCanBePrinted) {
            ssize_t valueSizeSize = swl_type_toCharEsc(elementType, &tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), data, args);
            ASSERTS_TRUE(valueSizeSize >= 0, valueSizeSize, ME, "ERR");
            totalSize += valueSizeSize;
        }

        if(swl_vectorIt_index(&it) < vectorSize - 1) {
            ssize_t nextSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_NEXT]);
            ASSERTS_TRUE(nextSize > 0, nextSize, ME, "ERR");
            totalSize += nextSize;
        }
    }

    ssize_t closeSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_LIST_CLOSE]);
    ASSERTS_TRUE(closeSize > 0, closeSize, ME, "ERR");

    totalSize += curSize;
    return totalSize;
}

/**
 * From char function to convert a string to a vector
 */
static bool s_fromCharExt_cb(swl_type_t* type, swl_vector_t* vector, const char* srcStr, const swl_print_args_t* args) {

    swl_vectorType_t* vectorType = (swl_vectorType_t*) type;

    if(!swl_vector_isInitialized(vector)) {
        swl_vector_init(vector, vectorType->type.elementType);
    }

    swl_type_t* elType = vectorType->type.elementType;

    size_t index = strlen(args->delim[SWL_PRINT_DELIM_LIST_OPEN]);
    size_t nextSize = strlen(args->delim[SWL_PRINT_DELIM_LIST_NEXT]);

    size_t i = 0;
    while(swl_print_hasNext(args, &srcStr[index], true)) {
        if(i > 0) {
            index += nextSize;
        }

        swl_typeEl_t* tmpEl = swl_vector_alloc(vector);
        ssize_t elemSize = swl_type_fromCharPrint(elType, tmpEl, &srcStr[index], args, SWL_PRINT_DELIM_LIST_NEXT);
        ASSERTS_TRUE(elemSize >= 0, false, ME, "ERR");
        index += elemSize;
        i++;
    }

    return true;
}

/**
 * type function to check whether two vectors are equal
 */
static bool s_equals_cb(swl_type_t* type _UNUSED, const swl_vector_t* key0, const swl_vector_t* key1) {
    return swl_vector_equals(key0, key1);
}

/**
 * Vector copy function, allocating new data.
 */
static swl_typeData_t* s_copy_cb(swl_type_t* type _UNUSED, const swl_vector_t* src) {
    swl_vector_t* vector = calloc(1, sizeof(swl_vector_t));
    ASSERT_NOT_NULL(vector, NULL, ME, "NULL");

    swl_vector_initExt(vector, src->type, src->dataVector.blockSize);

    swl_vector_forEachRef(it, swl_typeEl_t, data, src) {
        swl_vector_add(vector, data);
    }

    return vector;
}

/**
 * Vector to file function, writing data to file given print args.
 */
static bool s_toFile_cb(swl_type_t* type, FILE* file, const swl_vector_t* srcData, swl_print_args_t* args) {
    swl_vectorType_t* vectorType = (swl_vectorType_t*) type;

    swl_type_t* elType = vectorType->type.elementType;

    swl_print_addOpen(file, args, true);

    size_t vectorSize = swl_vector_size(srcData);

    swl_vector_forEachRef(it, swl_typeEl_t, data, srcData) {
        ASSERT_TRUE(swl_type_toFilePrivPrint(elType, file, SWL_TYPE_EL_TO_DATA(elType, data), args),
                    false, ME, "FAIL");

        if(swl_vectorIt_index(&it) < vectorSize - 1) {
            swl_print_addNext(file, args, false);
        }
    }
    swl_print_addClose(file, args, true);

    return true;
}

static void s_cleanup_cb(swl_type_t* type _UNUSED, swl_typeEl_t* tgtData) {
    swl_vector_cleanup((swl_vector_t*) tgtData);
}

swl_typeFun_t swl_vectorType_fun = {
    .name = "swl_vector",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_LIST,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromCharExt = (swl_type_fromCharExt_cb) s_fromCharExt_cb,
    .equals = (swl_type_equals_cb) s_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_cleanup_cb,
    .copy = (swl_type_copy_cb) s_copy_cb,
    .toFile = (swl_type_toFile_cb) s_toFile_cb,
    .subFun = &swl_vectorCollectionType_fun,
};

static swl_rc_ne s_col_init_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_vectorType_t* vectorType = (swl_vectorType_t*) colType;
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_init(collection, vectorType->type.elementType);
}

static swl_rc_ne s_col_initFromArray_cb(swl_listSType_t* colType, swl_coll_t* inCollection, swl_typeEl_t* array, size_t arraySize) {
    swl_vectorType_t* vectorType = (swl_vectorType_t*) colType;
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_initFromArray(collection, vectorType->type.elementType, array, arraySize);
}

static void s_col_clear_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    swl_vector_clear(collection);
}

static void s_col_cleanup_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    swl_vector_cleanup(collection);
}
static size_t s_col_maxSize_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED) {
    return 0;
}
static size_t s_col_size_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_size(collection);
}
static ssize_t s_col_add_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_add(collection, data);
}
static swl_typeEl_t* s_col_alloc_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_alloc(collection);
}
static swl_rc_ne s_col_insert_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_insert(collection, data, index);
}
static swl_rc_ne s_col_set_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_set(collection, data, index);
}
static swl_rc_ne s_col_delete_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_delete(collection, index);
}
static ssize_t s_col_find_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_find(collection, data);
}
static bool s_col_equals_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection1, swl_coll_t* inCollection2) {
    swl_vector_t* collection1 = (swl_vector_t*) inCollection1;
    swl_vector_t* collection2 = (swl_vector_t*) inCollection2;
    return swl_vector_equals(collection1, collection2);
}

static swl_typeData_t* s_col_getValue_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_getValue(collection, index);
}
static swl_typeEl_t* s_col_getReference_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_vector_t* collection = (swl_vector_t*) inCollection;
    return swl_vector_getReference(collection, index);
}

static swl_listSTypeIt_t s_col_getFirstIt_cb(swl_listSType_t* colType _UNUSED, const swl_coll_t* inCollection) {
    swl_vectorType_t* vectorType = (swl_vectorType_t*) colType;

    swl_vector_t* collection = (swl_vector_t*) inCollection;
    swl_listSTypeIt_t it;
    memset(&it, 0, sizeof(swl_listSTypeIt_t));
    swl_unLiList_firstIt(&it, &collection->dataVector);
    it.dataType = vectorType->type.elementType;
    return it;
}
static void s_col_nextIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    swl_unLiList_nextIt(it);
}

static void s_col_delIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    if(!it->valid) {
        return;
    }
    swl_vector_t* vector = it->coll - offsetof(swl_vector_t, dataVector);
    swl_type_cleanup(vector->type, it->data);

    swl_unLiList_delIt(it);
}

swl_listSTypeFun_t swl_vectorCollectionType_fun = {
    .name = "swl_vector",
    .init = s_col_init_cb,
    .initFromArray = s_col_initFromArray_cb,
    .clear = s_col_clear_cb,
    .cleanup = s_col_cleanup_cb,
    .maxSize = s_col_maxSize_cb,
    .size = s_col_size_cb,
    .add = s_col_add_cb,
    .alloc = s_col_alloc_cb,
    .insert = s_col_insert_cb,
    .set = s_col_set_cb,
    .delete = s_col_delete_cb,
    .find = s_col_find_cb,
    .equals = s_col_equals_cb,
    .getValue = s_col_getValue_cb,
    .getReference = s_col_getReference_cb,
    .firstIt = s_col_getFirstIt_cb,
    .nextIt = s_col_nextIt_cb,
    .delIt = s_col_delIt_cb,
};

static swl_typeExtIt_t extIt = {
    .id = SWL_TYPE_COL_FUN_UID,
    .extFun = &swl_vectorCollectionType_fun
};

SWL_CONSTRUCTOR static void s_initListExt(void) {
    swl_llist_append(&swl_vectorType_fun.extensions, &extIt.it);
}
