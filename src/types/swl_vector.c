/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/types/swl_vector.h"

#define ME "swlVect"

/**
 * Initialize a given vector
 * @param vector
 *  the vector to initialize
 * @param type
 *  the type of elements
 * @return
 *  SWL_RC_OK if all went well, an swl_rc error code otherwise.
 */
swl_rc_ne swl_vector_init(swl_vector_t* vector, swl_type_t* elType) {
    return swl_vector_initExt(vector, elType, SWL_VECTOR_DEFAULT_BLOCK_SIZE);
}

/**
 * Initialize a given vector with given blocksize
 * @param vector
 *  the vector to initialize
 * @param type
 *  the type of the elements
 * @param blockSize
 *  the blocksize of the underlying unLiList.
 * @return
 *  SWL_RC_OK if all went well, an swl_rc error code otherwise.
 */
swl_rc_ne swl_vector_initExt(swl_vector_t* vector, swl_type_t* type, uint32_t blockSize) {
    vector->type = type;

    return swl_unLiList_initExt(&vector->dataVector, blockSize, vector->type->size);
}

/**
 * Initialize a vector from a given source array.
 */
swl_rc_ne swl_vector_initFromArray(swl_vector_t* vector, swl_type_t* type, const swl_typeEl_t* srcData, size_t len) {
    ASSERT_NOT_NULL(vector, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_vector_init(vector, type);

    for(size_t i = 0; i < len; i++) {
        ssize_t val = swl_vector_add(vector, swl_type_arrayGetValue(type, srcData, len, i));
        if(val < 0) {
            return (swl_rc_ne) val;
        }
    }

    return SWL_RC_OK;
}

/**
 * Check whether the given vector is initialized.
 * @param vector
 *  the vector to check whether it's initialized.
 * @return
 *  true if an init function was called on the given vector, false otherwise.
 */
bool swl_vector_isInitialized(swl_vector_t* vector) {
    if(vector->type == NULL) {
        return false;
    }
    return swl_unLiList_isInitialized(&vector->dataVector);
}

/**
 * Return the size of the vector
 * @param vector
 *  the vector of which the size is requested
 * @return
 *  the current number of elements stored in the vector if the vector is valid, 0 otherwise.
 */
size_t swl_vector_size(const swl_vector_t* vector) {
    ASSERT_NOT_NULL(vector, 0, ME, "NULL");
    return swl_unLiList_size(&vector->dataVector);
}

/**
 * Return the type data pointer of the element at the given index.
 * @param vector
 *  the vector from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type data at the requested index if it exists, NULL otherwise
 */
swl_typeData_t* swl_vector_getValue(const swl_vector_t* vector, ssize_t index) {
    return SWL_TYPE_EL_TO_DATA(vector->type, swl_unLiList_get(&vector->dataVector, index));
}

/**
 * Return the type element pointer of the element at the given index.
 * @param vector
 *  the vector from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @return
 *  the pointer to the data at the requested index if it exists, NULL otherwise.
 */
swl_typeEl_t* swl_vector_getReference(const swl_vector_t* vector, ssize_t index) {
    return swl_unLiList_get(&vector->dataVector, index);
}

/**
 * Check if two vectors are equal
 * @param vector1
 *  the first vector to compare
 * @param vector2
 *  the second vector to compare
 * @return
 *  true if all elements in vector1 are equal as in vector2, in same order, as checked with type equals
 */
bool swl_vector_equals(const swl_vector_t* vector1, const swl_vector_t* vector2) {
    ASSERT_NOT_NULL(vector2, false, ME, "NULL");
    ASSERT_NOT_NULL(vector1, false, ME, "NULL");
    ASSERT_EQUALS(vector1->type, vector2->type, false, ME, "DIFF");

    swl_type_t* elementType = vector1->type;

    if(swl_vector_size(vector2) != swl_vector_size(vector1)) {
        return false;
    }

    swl_vector_forEachRef(it, swl_typeEl_t, elData1, vector1) {
        swl_typeData_t* data1 = SWL_TYPE_EL_TO_DATA(elementType, elData1);
        swl_typeData_t* data2 = swl_vector_getValue(vector2, swl_vectorIt_index(&it));
        if(!swl_type_equals(elementType, data1, data2)) {
            return false;
        }
    }
    return true;
}

/**
 * Clear the target vector, removing all entries
 * @param vector
 *  the vector to clear
 * @return
 *  ensures that all elements in the vector are cleared, having called the type cleanup function of it.
 */
void swl_vector_clear(swl_vector_t* vector) {
    ASSERTI_NOT_NULL(vector, , ME, "NULL");
    swl_typeEl_t* entry = swl_unLiList_get(&vector->dataVector, -1);

    while(entry != NULL) {
        swl_type_cleanup(vector->type, entry);
        swl_unLiList_remove(&vector->dataVector, -1);
        entry = swl_unLiList_get(&vector->dataVector, -1);
    }
}

/**
 * Clean up the vector, freeing all allocated memory. This includes memory allocated for referenced data.
 * @param vector
 *  the vector to clean up
 * @return
 *  ensures that all elements are cleared, and destroys all allocated memory.
 *  Note that to reuse the vector, it needs to be reinitialized.
 */
void swl_vector_cleanup(swl_vector_t* vector) {
    swl_vector_clear(vector);
    vector->type = NULL;
    swl_unLiList_destroy(&vector->dataVector);
}

/**
 * Insert element at given index
 * @param vector
 *  the vector to which to add the element
 * @param val
 *  the data to add.
 * @param index
 *  the index at which to add. This index can be in range [- size - 1, size]
 *  Negative indexes shall be counted from back of list. Note that inserting at -1 will add the
 *  element to the back. Inserting at -size -1, will insert at the start.
 */
swl_rc_ne swl_vector_insert(swl_vector_t* vector, swl_typeData_t* val, ssize_t index) {
    ASSERT_NOT_NULL(vector, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(val, SWL_RC_INVALID_PARAM, ME, "NULL");

    char buf[vector->type->size];
    memset(buf, 0, vector->type->size);
    swl_type_copyTo(vector->type, buf, val);

    swl_unLiList_insert(&vector->dataVector, index, buf);
    return SWL_RC_OK;
}

/**
 * Add element to the vector
 * @param vector
 *  the vector to which to add the element
 * @param val
 *  the data to add.
 * @return
 *  the index at which the element was added, swl_rc_ne error code if failed.
 */
ssize_t swl_vector_add(swl_vector_t* vector, swl_typeData_t* val) {
    ASSERT_NOT_NULL(vector, SWL_RC_INVALID_PARAM, ME, "NULL");
    if(vector->type->typeFun->isValue) {
        ASSERT_NOT_NULL(val, SWL_RC_INVALID_PARAM, ME, "NULL");
    }

    swl_typeEl_t* entry = swl_unLiList_allocElement(&vector->dataVector);
    ASSERT_NOT_NULL(entry, SWL_RC_INVALID_PARAM, ME, "NULL");

    if(val != NULL) {
        swl_type_copyTo(vector->type, entry, val);
    }

    return swl_vector_size(vector) - 1;
}

/**
 * Allocate an empty element
 * @param vector
 *  the vector to which to add the element
 * @return
 *  the pointer to an empty element, NULL if failed
 */
swl_typeEl_t* swl_vector_alloc(swl_vector_t* vector) {
    return swl_unLiList_allocElement(&vector->dataVector);
}

/**
 * Set the value of the item at the given index.
 * @param vector
 *  the vector to which to set the element
 * @param val
 *  the data to set.
 * @param index
 *  the index at which to set. This index can be in range [- size, size - 1]
 *  Negative indexes shall be counted from back of list. Note that setting at -1 will add the
 *  element to the back. Inserting at -size, will set at the start.
 */
swl_rc_ne swl_vector_set(swl_vector_t* vector, swl_typeData_t* val, ssize_t index) {
    ASSERT_NOT_NULL(vector, false, ME, "NULL");
    ASSERT_NOT_NULL(val, false, ME, "NULL");

    swl_typeEl_t* entry = swl_vector_getReference(vector, index);
    ASSERT_NOT_NULL(entry, SWL_RC_INVALID_STATE, ME, "NULL");

    swl_type_copyTo(vector->type, entry, val);
    return SWL_RC_OK;
}

/**
 * Delete the entry at the given index
 * @param vector
 *  the vector from which to delete the given entry
 * @param index
 *  the index at which to delete the given index
 * @return
 *  SWL_RC_OK if delete succeeded, error if not.
 */
swl_rc_ne swl_vector_delete(swl_vector_t* vector, ssize_t index) {
    swl_typeEl_t* entry = swl_vector_getReference(vector, index);
    ASSERT_NOT_NULL(entry, SWL_RC_INVALID_PARAM, ME, "NULL");
    swl_type_cleanup(vector->type, entry);
    return swl_unLiList_remove(&vector->dataVector, index);
}

/**
 * Finds the index of a given value
 * @param vector
 *  the vector in which to look
 * @param val
 *  the value to look for
 * @return
 *  the first index for which applies that swl_type_equals(type, val, swl_vector_getValue(vector, index))
 *  If no such index exists, -1 is returned.
 */
ssize_t swl_vector_find(swl_vector_t* vector, swl_typeData_t* val) {
    swl_unLiListIt_t it;
    swl_unLiList_for_each(it, &vector->dataVector) {
        if(swl_type_equals(vector->type, val, it.data)) {
            return it.index;
        }
    }
    return -1;
}

/**
 * Initialize the given memory to a vector iterator
 * @param vectorIt
 *  the vector iterator to initialize
 * @param vector
 *  the vector to iterate over
 */
void swl_vectorIt_init(swl_vectorIt_t* vectorIt, const swl_vector_t* vector) {
    swl_unLiList_firstIt(&vectorIt->it, &vector->dataVector);
    vectorIt->vector = (swl_vector_t*) vector;
}

/**
 * Puts the first vector iterator of vector in vectorIt.
 * @param vector
 *  the vector to iterate over
 * @return
 *  a vector iterator struct ready to iterate over given vector.
 */
swl_vectorIt_t swl_vector_getFirstIt(const swl_vector_t* vector) {
    swl_vectorIt_t vectorIt;
    memset(&vectorIt, 0, sizeof(swl_vectorIt_t));
    swl_vectorIt_init(&vectorIt, vector);
    return vectorIt;
}

/**
 * Move the vector iterator to the next element
 * @param vectorIt
 *  the vector iterator to move
 */
void swl_vectorIt_next(swl_vectorIt_t* vectorIt) {
    swl_unLiList_nextIt(&vectorIt->it);
}

/**
 * Delete the element currently pointed to by the vector iterator. This shall
 * also cleanup all type data at the location.
 * It shall also remain valid, so that
 * @param vectorIt
 *  the vector iterator pointing to the data to remove.
 * @note
 *  Calling del on the same iterator twice in a row will have no additional effect the second time.
 *  One should not use the data of the vector iterator, as the iterator is made invalid.
 */
void swl_vectorIt_del(swl_vectorIt_t* vectorIt) {
    swl_type_cleanup(vectorIt->vector->type, vectorIt->it.data);
    swl_unLiList_delIt(&vectorIt->it);
}

/**
 * Returns the index of the element to which the vector iterator is pointing
 * @param it
 *  the iterator pointing at the data
 * @return
 *  the index of the data to which the vector is pointing to
 */
size_t swl_vectorIt_index(swl_vectorIt_t* it) {
    return it->it.index;
}

/**
 * Returns the data this iterator is currently pointing to
 * @param it
 *  the iterator pointing at the data
 * @return
 *  the swl type data at this iterator points to.
 */
swl_typeEl_t* swl_vectorIt_data(swl_vectorIt_t* it) {
    return it->it.data;
}

/**
 * Assign the value of the iterator to tgt data, doing shallow copy
 * @param it
 *  the iterator from which to extract the data
 * @param tgtData
 *  the target pointer to which assign the data
 *
 * @return
 *  true if assignment is successful, meaning iterator was valid and function could copy data, false otherwise.
 */
bool swl_vectorIt_assignVal(swl_vectorIt_t* it, swl_typeEl_t* tgtData) {
    if(!it->it.valid) {
        return false;
    }
    if(tgtData == NULL) {
        return false;
    }
    swl_type_t* myType = it->vector->type;
    memcpy(tgtData, it->it.data, myType->size);
    return true;
}
