/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ME "swl_col"

#include "swl/types/swl_collType.h"

/**
 * Returns the initial iterator for a given collection type, and collection
 */
swl_listSTypeIt_t swl_listSType_getFirstIt(swl_listSType_t* colType, const swl_coll_t* collection) {
    swl_listSTypeIt_t it;
    memset(&it, 0, sizeof(swl_listSTypeIt_t));
    it.valid = false;

    swl_listSTypeFun_t* typeFun = colType->typeFun->subFun;

    ASSERT_NOT_NULL(typeFun, it, ME, "NO FUN");
    return typeFun->firstIt(colType, collection);
}

/**
 * Moves the iterator one spot forward, for a given collection iterator
 */
void swl_listSType_nextIt(swl_listSType_t* colType, swl_listSTypeIt_t* it) {
    ASSERT_NOT_NULL(colType, , ME, "NO FUN");
    ASSERT_NOT_NULL(it, , ME, "NO FUN");
    swl_listSTypeFun_t* typeFun = colType->typeFun->subFun;
    typeFun->nextIt(colType, it);
}

/**
 * Delete the current element for the collection iterator
 */
void swl_listSType_delIt(swl_listSType_t* colType, swl_listSTypeIt_t* it) {
    ASSERT_NOT_NULL(colType, , ME, "NO FUN");
    ASSERT_NOT_NULL(it, , ME, "NO FUN");
    swl_listSTypeFun_t* typeFun = colType->typeFun->subFun;
    typeFun->delIt(colType, it);
}

/**
 * Do a shallow copy to tgtData of the element data where the iterator is currently pointing.
 */
bool swl_listSTypeIt_assignVal(swl_listSTypeIt_t* it, swl_listSType_t* colType _UNUSED, swl_typeEl_t* tgtData) {
    if(!it->valid) {
        return false;
    }
    if(tgtData == NULL) {
        return false;
    }
    swl_type_t* myType = it->dataType;
    memcpy(tgtData, it->data, myType->size);
    return true;
}
