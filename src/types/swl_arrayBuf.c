/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/types/swl_arrayBuf.h"

#define ME "swl_AB"

/**
 * This module models an array buffer, basically an array, which has a size, and where you can add / remove elements
 * up to a certain maxSize.
 *
 * This arrayBuf will NOT grow dynamically, but can contain empty elements in its size.
 */

/**
 * Initialize an array buffer using extended parameters
 * @param buf
 *  the array buffer to initialize
 * @param type
 *  the type of the array buffer
 * @param maxSize
 *  the maximum size of the array buffer
 * @param size
 *  the size of potential pre-allocated elements.
 * @note
 *  the initialize function will set all non initialized elements to 0
 */
swl_rc_ne swl_arrayBuf_initExt(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize, size_t size) {
    buf->type = type;

    buf->maxSize = maxSize;
    buf->size = size;

    memset(buf->data + size * type->size, 0, (maxSize - size) * type->size);

    return SWL_RC_OK;
}

/**
 * Initialize an array buffer
 * @param buf
 *  the array buffer to initialize
 * @param type
 *  the type of the array buffer
 * @param maxSize
 *  the maximum size of the array buffer
 * @note
 *  the initialize function will set the entire buffer to 0
 */
swl_rc_ne swl_arrayBuf_init(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize) {
    return swl_arrayBuf_initExt(buf, type, maxSize, 0);
}

/**
 * Initialize an array buffer from data.
 * @param buf
 *  the array buffer to initialize
 * @param type
 *  the type of the array buffer
 * @param maxSize
 *  the maximum size of the array buffer
 * @param array
 *  the array containing init elements
 * @param arraySize
 *  the size of the array
 * @note
 *  the initialize function will copy all elements from array into the buffer,
 *  to the length of minimum of maxSize or arraySize. So it is allowed to provide a larger array
 *  then the arrayBuf contains. If so, the function copies as much as possible, and returns SWL_RC_NOT_AVAILABLE
 */
swl_rc_ne swl_arrayBuf_initFromArray(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize, swl_typeEl_t* array, size_t arraySize) {
    swl_arrayBuf_initExt(buf, type, maxSize, arraySize);
    swl_rc_ne ret = swl_type_arrayCopy(type, &buf->data[0], array, SWL_MIN(maxSize, arraySize));
    if(swl_rc_isOk(ret)) {
        return arraySize <= maxSize ? SWL_RC_OK : SWL_RC_NOT_AVAILABLE;
    } else {
        return ret;
    }
}

/**
 * Clear the array buf, cleaning up all elements insize.
 * The buffer will remain usable
 * @param buf
 *  the buffer that needs to be cleared.
 */
void swl_arrayBuf_clear(swl_arrayBuf_t* buf) {
    swl_type_arrayCleanup(buf->type, buf->data, buf->size);
    buf->size = 0;
}

/**
 * clean up the array buffer, removing any allocated memory
 * @param buf
 *  the buffer that needs to be cleaned. Note that to
 *  use the buf again, it needs to be reinitialized.
 */
void swl_arrayBuf_cleanup(swl_arrayBuf_t* buf) {
    swl_arrayBuf_clear(buf);
    buf->type = NULL;
}

/**
 * Return the type element pointer of the element at the given index.
 * @param buf
 *  the buf from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type element at the requested index if it exists, NULL otherwise
 */
swl_typeEl_t* swl_arrayBuf_getReference(const swl_arrayBuf_t* buf, ssize_t index) {
    return swl_type_arrayGetReference(buf->type, &buf->data[0], buf->size, index);
}

/**
 * Return the type data pointer of the element at the given index.
 * @param buf
 *  the buf from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type data at the requested index if it exists, NULL otherwise
 */
swl_typeData_t* swl_arrayBuf_getValue(const swl_arrayBuf_t* buf, ssize_t index) {
    return swl_type_arrayGetValue(buf->type, &buf->data[0], buf->size, index);
}

/**
 * Set the value of the item at the given index.
 * @param buffer
 *  the buffer to which to set the element
 * @param val
 *  the data to set.
 * @param index
 *  the index at which to set. This index can be in range [- size, size - 1]
 *  Negative indexes shall be counted from back of list. Note that setting at -1 will add the
 *  element to the back. Inserting at -size, will set at the start.
 */
swl_rc_ne swl_arrayBuf_set(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index) {
    return swl_type_arraySet(buf->type, &buf->data[0], buf->size, data, index);
}

/**
 * Append the data at the given index. If the array is not full, then
 * the remaining elements will be shifted to the right.
 * If the array is full, then the remaining elements will be shifted to the left, causing the first one to drop.
 * To append at the end, use index -1
 * @param buf
 *  the buffer to which the data has to be appended
 * @param data
 *  the data to be appended
 * @param index
 *  the index at which the data must be appended
 * @return
 *  SWL_RC_OK if the data was properly appended
 *  swl_rc_ne error code otherwise.
 */
swl_rc_ne swl_arrayBuf_append(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index) {
    if(buf->size == buf->maxSize) {
        return swl_type_arrayAppend(buf->type, &buf->data[0], buf->maxSize, data, index);
    }

    size_t targetIndex = SWL_ARRAY_INDEX(index, buf->size + 1);
    ASSERT_TRUE(targetIndex <= buf->size && targetIndex < buf->maxSize,
                SWL_RC_INVALID_PARAM, ME, "SIZE %zu %zu => %zu / %zu", buf->size, buf->maxSize, targetIndex, index);

    swl_rc_ne ret = SWL_RC_ERROR;
    if(targetIndex == buf->size) {
        ret = swl_type_arraySet(buf->type, &buf->data[0], buf->size + 1, data, targetIndex);
    } else {
        ret = swl_type_arrayPrepend(buf->type, &buf->data[0], buf->size + 1, data, targetIndex);
    }

    if(ret == SWL_RC_OK) {
        buf->size++;
    }
    return ret;
}

/**
 * Prepend the data at the given index.
 * The remaining elements will be shifted to the right.
 * If the array is full,this will cause the last one to drop.
 * To append at the end, use index -1
 * @param buf
 *  the buffer to which the data has to be prepended
 * @param data
 *  the data to be prepended
 * @param index
 *  the index at which the data must be prepended
 * @return
 *  SWL_RC_OK if the data was properly prepended
 *  swl_rc_ne error code otherwise.
 */
swl_rc_ne swl_arrayBuf_prepend(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index) {
    if(buf->size == buf->maxSize) {
        return swl_type_arrayPrepend(buf->type, &buf->data[0], buf->maxSize, data, index);
    }

    size_t targetIndex = SWL_ARRAY_INDEX(index, buf->size + 1);
    ASSERT_TRUE(targetIndex <= buf->size && targetIndex < buf->maxSize,
                SWL_RC_INVALID_PARAM, ME, "SIZE %zu %zu => %zu / %zu", buf->size, buf->maxSize, targetIndex, index);
    swl_rc_ne ret = swl_type_arrayPrepend(buf->type, &buf->data[0], buf->maxSize, data, targetIndex);
    if((ret == SWL_RC_OK) && (buf->size < buf->maxSize)) {
        buf->size += 1;
    }
    return ret;
}

/**
 * Add element to the buffer
 * @param buf
 *  the buf to which to add the element
 * @param data
 *  the data to add.
 * @return
 *  the index at which the element was added, swl_rc_ne error code if failed.
 */
ssize_t swl_arrayBuf_add(swl_arrayBuf_t* buf, swl_typeData_t* data) {
    if(buf->size >= buf->maxSize) {
        return SWL_RC_NOT_AVAILABLE;
    }
    swl_rc_ne ret = swl_arrayBuf_prepend(buf, data, -1);
    ASSERTS_TRUE(swl_rc_isOk(ret), ret, ME, "FAIL");

    return buf->size - 1;
}

/**
 * Allocate element to the buffer
 * @param buf
 *  the buf to which to add the element
 * @param data
 *  the data to add.
 * @return
 *  the index at which the element was added, swl_rc_ne error code if failed.
 */
swl_typeEl_t* swl_arrayBuf_alloc(swl_arrayBuf_t* buf) {
    if(buf->size >= buf->maxSize) {
        return NULL;
    }
    buf->size++;
    return &buf->data[(buf->size - 1) * buf->type->size];
}

/**
 * Delete the entry at the given index
 * @param buf
 *  the buffer from which to delete the given entry
 * @param index
 *  the index at which to delete the given index
 * @return
 *  SWL_RC_OK if delete succeeded, error if not.
 *  Remaining elements after index shall be shifted one to the left
 */
swl_rc_ne swl_arrayBuf_delete(swl_arrayBuf_t* buf, ssize_t index) {
    swl_rc_ne ret = swl_type_arrayDelete(buf->type, &buf->data[0], buf->size, index);
    ASSERTS_TRUE(swl_rc_isOk(ret), ret, ME, "FAIL");

    buf->size--;
    return SWL_RC_OK;
}

/**
 * Finds the index of a given value
 * @param buf
 *  the buffer in which to look
 * @param val
 *  the value to look for
 * @return
 *  the first index for which applies that swl_type_equals(type, val, swl_arrayBuf_getValue(buf, index))
 *  If no such index exists, -1 is returned.
 */
ssize_t swl_arrayBuf_find(const swl_arrayBuf_t* buf, swl_typeData_t* val) {
    return swl_type_arrayFind(buf->type, &buf->data[0], buf->size, val);
}

/**
 * Return the size of the buffer
 * @param buf
 *  the buffer of which the size is requested
 * @return
 *  the current number of elements stored in the buffer if buf is valid, 0 otherwise.
 */
size_t swl_arrayBuf_size(const swl_arrayBuf_t* buf) {
    return buf->size;
}

/**
 * Check if two arrayBufs are equal
 * @param arrayBuf1
 *  the first arrayBuf to compare
 * @param arrayBuf2
 *  the second arrayBuf to compare
 * @return
 *  true if all elements in arrayBuf1 are equal as in arrayBuf2, in same order, as checked with type equals
 */
bool swl_arrayBuf_equals(const swl_arrayBuf_t* buf1, const swl_arrayBuf_t* buf2) {
    ASSERT_NOT_NULL(buf2, false, ME, "NULL");
    ASSERT_NOT_NULL(buf1, false, ME, "NULL");
    ASSERT_EQUALS(buf1->type, buf2->type, false, ME, "DIFF");

    return swl_type_arrayEquals(buf1->type, buf1->data, buf1->size, buf2->data, buf2->size);
}

/**
 * Create an iterator with which to iterate over the given buf
 *
 * @param buf
 *  the buffer for which to create an iterator
 */
swl_arrayBufIt_t swl_arrayBuf_getFirstIt(const swl_arrayBuf_t* buf) {
    swl_arrayBufIt_t it;
    swl_arrayBufIt_init(&it, buf);
    return it;
}

/**
 * Initialize the given arrayBuf iterator on the given buffer
 * @param it
 *  the iterator to initialize
 * @param buf
 *  the buffer with which to initialize it
 */
void swl_arrayBufIt_init(swl_arrayBufIt_t* it, const swl_arrayBuf_t* buf) {
    it->buf = buf;
    it->index = 0;
    if(buf->size == 0) {
        it->valid = false;
        return;
    }
    it->data = swl_arrayBuf_getValue(buf, it->index);
    it->valid = (it->data != NULL);
}

/**
 * Delete the element currently pointed to by the iterator. This shall
 * also cleanup all type data at the location.
 * It shall also remain valid, so that
 * @param it
 *  the arrayBuf iterator pointing to the data to remove.
 * @note
 *  Calling del on the same iterator twice in a row will have no additional effect the second time.
 *  One should not use the data of the arrayBuf iterator, as the iterator is made invalid.
 */
void swl_arrayBufIt_del(swl_arrayBufIt_t* it) {
    if(!it->valid) {
        return;
    }
    swl_arrayBuf_delete((swl_arrayBuf_t*) it->buf, it->index);
    it->valid = false;
}

/**
 * Move the iterator to the next element.
 * @param it
 *  the iterator to move to the next element.
 * @note
 *  if the element does not exist, it->valid shall be put to false. The index
 *  shall then point to a nonexistant index.
 */
void swl_arrayBufIt_next(swl_arrayBufIt_t* it) {
    if(it->valid) {
        it->index++;
    }
    if(it->index >= it->buf->size) {
        it->valid = false;
        return;
    }
    it->data = swl_arrayBuf_getValue(it->buf, it->index);
    it->valid = (it->data != NULL);
}

/**
 * Return the index where this iterator currently points
 * @param it
 *  the iterator for which the index is requested.
 * @return
 *  the index of the element to which the iterator currently points.
 */
size_t swl_arrayBufIt_index(swl_arrayBufIt_t* it) {
    return it->index;
}

/**
 * Returns the data this iterator is currently pointing to
 * @param it
 *  the iterator pointing at the data
 * @return
 *  the swl type data at this iterator points to.
 */
swl_typeEl_t* swl_arrayBufIt_data(swl_arrayBufIt_t* it) {
    return it->data;
}

/**
 * Assign the value of the iterator to tgt data, doing shallow copy
 * @param it
 *  the iterator from which to extract the data
 * @param tgtData
 *  the target pointer to which assign the data
 *
 * @return
 *  true if assignment is successful, meaning iterator was valid and function could copy data, false otherwise.
 */
bool swl_arrayBufIt_assignVal(swl_arrayBufIt_t* it, swl_typeEl_t* tgtData) {
    if(!it->valid) {
        return false;
    }
    if(tgtData == NULL) {
        return false;
    }
    swl_type_t* myType = it->buf->type;
    memcpy(tgtData, it->data, myType->size);
    return true;
}

