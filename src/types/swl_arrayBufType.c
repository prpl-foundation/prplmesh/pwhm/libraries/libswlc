/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_arrayBufType.h"
#include "swl/swl_string.h"

#define ME "swlAB_T"

/**
 * handle typeData to char
 */
static ssize_t s_toChar_cb(swl_arrayBufType_t* type, char* tgtStr, size_t tgtStrSize, const swl_arrayBuf_t* srcData) {
    return swl_type_arrayToCharPrint(type->type.elementType, tgtStr, tgtStrSize,
                                     srcData->data, srcData->size, true);
}

/**
 * handle typeData from char
 */
static bool s_fromCharExt_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {
    swl_arrayBufType_t* arrayType = (swl_arrayBufType_t*) type;
    swl_type_t* elementType = arrayType->type.elementType;

    swl_arrayBuf_t* typedArr = (swl_arrayBuf_t*) tgtData;
    typedArr->type = arrayType->type.elementType;
    typedArr->maxSize = arrayType->maxNrElements;

    return swl_type_arrayFromCharExt(elementType, &typedArr->data, typedArr->maxSize, srcStr, args, &typedArr->size);
}

/**
 * handle typeData equals
 */
static bool s_equals_cb(swl_arrayBufType_t* type, const swl_arrayBuf_t* key0, const swl_arrayBuf_t* key1) {
    return swl_type_arrayEquals(type->type.elementType, key0->data, key0->size, key1->data, key1->size);
}

static void s_cleanup_cb(swl_type_t* type _UNUSED, swl_typeEl_t* tgtData) {
    swl_arrayBuf_cleanup(tgtData);
}

static swl_typeData_t* s_copy_cb(swl_arrayBufType_t* arrayType, const swl_arrayBuf_t* srcArr) {
    swl_typeData_t* data = calloc(1, arrayType->type.type.size);
    ASSERT_NOT_NULL(data, NULL, ME, "NULL");

    swl_arrayBuf_t* tgtArr = (swl_arrayBuf_t*) data;
    tgtArr->size = srcArr->size;
    tgtArr->type = srcArr->type;

    for(size_t i = 0; i < srcArr->size; i++) {
        swl_type_copyTo(srcArr->type, swl_arrayBuf_getReference(tgtArr, i), swl_arrayBuf_getValue(srcArr, i));
    }

    return data;
}

static bool s_toFile_cb(swl_arrayBufType_t* type, FILE* file, const swl_arrayBuf_t* srcData, swl_print_args_t* args) {
    return swl_type_arrayToFilePrint(type->type.elementType, file,
                                     srcData->data, srcData->size, true, args);
}

swl_typeFun_t swl_arrayBufType_fun = {
    .name = "swl_arrayBuf",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_LIST,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromCharExt = (swl_type_fromCharExt_cb) s_fromCharExt_cb,
    .equals = (swl_type_equals_cb) s_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_cleanup_cb,
    .copy = (swl_type_copy_cb) s_copy_cb,
    .toFile = (swl_type_toFile_cb) s_toFile_cb,
    .subFun = &swl_arrayBufCollectionType_fun,
};

static swl_rc_ne s_col_init_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_arrayBufType_t* abType = (swl_arrayBufType_t*) colType;
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_init(collection, abType->type.elementType, abType->maxNrElements);
}

static swl_rc_ne s_col_initFromArray_cb(swl_listSType_t* colType, swl_coll_t* inCollection, swl_typeEl_t* array, size_t arraySize) {
    swl_arrayBufType_t* abType = (swl_arrayBufType_t*) colType;
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_initFromArray(collection, abType->type.elementType, abType->maxNrElements, array, arraySize);
}

static void s_col_clear_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    swl_arrayBuf_clear(collection);
}

static void s_col_cleanup_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    swl_arrayBuf_cleanup(collection);
}

static size_t s_col_maxSize_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return collection->maxSize;
}
static size_t s_col_size_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_size(collection);
}
static ssize_t s_col_add_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_add(collection, data);
}
static swl_typeEl_t* s_col_alloc_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_alloc(collection);
}
static swl_rc_ne s_col_insert_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_prepend(collection, data, index);
}
static swl_rc_ne s_col_set_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_set(collection, data, index);
}
static swl_rc_ne s_col_delete_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_delete(collection, index);
}
static ssize_t s_col_find_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_find(collection, data);
}
static bool s_col_equals_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection1, swl_coll_t* inCollection2) {
    swl_arrayBuf_t* collection1 = (swl_arrayBuf_t*) inCollection1;
    swl_arrayBuf_t* collection2 = (swl_arrayBuf_t*) inCollection2;
    return swl_arrayBuf_equals(collection1, collection2);
}

static swl_typeData_t* s_col_getValue_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_getValue(collection, index);
}
static swl_typeEl_t* s_col_getReference_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    return swl_arrayBuf_getReference(collection, index);
}

static inline void s_col_setItData(swl_arrayBufType_t* colType, swl_listSTypeIt_t* data, swl_coll_t* inCollection) {
    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    if(data->index < collection->size) {
        data->valid = true;
        data->data = &collection->data[data->index * colType->type.elementType->size];
    } else {
        data->valid = false;
    }
}

static swl_listSTypeIt_t s_col_getFirstIt_cb(swl_listSType_t* colType, const swl_coll_t* inCollection) {
    swl_arrayBufType_t* abType = (swl_arrayBufType_t*) colType;

    swl_arrayBuf_t* collection = (swl_arrayBuf_t*) inCollection;
    swl_listSTypeIt_t data;
    memset(&data, 0, sizeof(swl_listSTypeIt_t));
    data.coll = collection;
    data.index = 0;
    data.dataType = abType->type.elementType;
    s_col_setItData((swl_arrayBufType_t*) colType, &data, collection);
    return data;
}
static void s_col_nextIt_cb(swl_listSType_t* colType, swl_listSTypeIt_t* it) {
    if(it->valid) {
        it->index++;
    }
    s_col_setItData((swl_arrayBufType_t*) colType, it, (swl_arrayBuf_t*) it->coll);
}

static void s_col_delIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    if(it->valid) {
        swl_arrayBuf_delete((swl_arrayBuf_t*) it->coll, it->index);
        it->valid = false;
    }
}

swl_listSTypeFun_t swl_arrayBufCollectionType_fun = {
    .name = "swl_arrayBuf",
    .init = s_col_init_cb,
    .initFromArray = s_col_initFromArray_cb,
    .clear = s_col_clear_cb,
    .cleanup = s_col_cleanup_cb,
    .maxSize = s_col_maxSize_cb,
    .size = s_col_size_cb,
    .add = s_col_add_cb,
    .alloc = s_col_alloc_cb,
    .insert = s_col_insert_cb,
    .set = s_col_set_cb,
    .delete = s_col_delete_cb,
    .find = s_col_find_cb,
    .equals = s_col_equals_cb,
    .getValue = s_col_getValue_cb,
    .getReference = s_col_getReference_cb,
    .firstIt = s_col_getFirstIt_cb,
    .nextIt = s_col_nextIt_cb,
    .delIt = s_col_delIt_cb,
};

static swl_typeExtIt_t extIt = {
    .id = SWL_TYPE_COL_FUN_UID,
    .extFun = &swl_arrayBufCollectionType_fun
};

SWL_CONSTRUCTOR static void s_initListExt(void) {
    swl_llist_append(&swl_arrayBufType_fun.extensions, &extIt.it);
}
