/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_type.h"
#include "swl/types/swl_arrayList.h"

#define ME "swl_AL"

/**
 * Default growth factor
 */
#define SWL_ARRAY_LIST_GROWTH_FACTOR 500
#define SWL_ARRAY_LIST_SHRINK_SIZE 500
#define SWL_ARRAY_LIST_SHRINK_FACTOR 666
#define SWL_ARRAY_LIST_DEFAULT_MIN_SIZE (size_t) 10

/**
 * Initialize an arraylist using extended parameters
 * @param list
 *  the array list to initialize
 * @param type
 *  the type of the array list
 * @param defaultSize
 *  the default size of the array buffer
 * @note
 *  the initialize function will set all non initialized elements to 0.
 *  If the defaultSize is smaller than the SWL_ARRAY_LIST_DEFAULT_MIN_SIZE of 10, then
 *  the default min size shall be used.
 */
swl_rc_ne swl_arrayList_initExt(swl_arrayList_t* list, swl_type_t* type, size_t defaultSize) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    list->type = type;
    list->maxSize = SWL_MAX(SWL_ARRAY_LIST_DEFAULT_MIN_SIZE, defaultSize);
    list->data = calloc(list->maxSize, list->type->size);
    list->size = 0;
    return SWL_RC_OK;
}

/**
 * Initialize an arraylist
 * @param list
 *  the array list to initialize
 * @param type
 *  the type of the array list
 * @note
 *  the initialize function will set all non initialized elements to 0.
 */
swl_rc_ne swl_arrayList_init(swl_arrayList_t* al, swl_type_t* type) {
    return swl_arrayList_initExt(al, type, SWL_ARRAY_LIST_DEFAULT_MIN_SIZE);
}

/**
 * Initialize an arraylist from a given source array.
 */
swl_rc_ne swl_arrayList_initFromArray(swl_arrayList_t* list, swl_type_t* type, const swl_typeEl_t* srcData, size_t len) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(type, SWL_RC_INVALID_PARAM, ME, "NULL");
    list->type = type;
    list->maxSize = SWL_MAX(SWL_ARRAY_LIST_DEFAULT_MIN_SIZE, len * (1000 + SWL_ARRAY_LIST_GROWTH_FACTOR) / 1000);
    list->data = calloc(list->maxSize, type->size);
    list->size = len;
    return swl_type_arrayCopy(type, list->data, srcData, len);
}

/**
 * Check whether the given arrayList is initialized.
 * @param vector
 *  the vector to check whether it's initialized.
 * @return
 *  true if an init function was called on the given vector, false otherwise.
 */
bool swl_arrayList_isInitialized(swl_arrayList_t* list) {
    if(list->type == NULL) {
        return false;
    }
    if(list->maxSize < SWL_ARRAY_LIST_DEFAULT_MIN_SIZE) {
        return false;
    }
    if(list->data == NULL) {
        return false;
    }
    return true;
}

/**
 * Clear the array list, cleaning up all elements insize.
 * The arraylist will remain usable
 * @param buf
 *  the arraylist that needs to be cleared.
 */
void swl_arrayList_clear(swl_arrayList_t* list) {
    swl_type_arrayCleanup(list->type, list->data, list->size);
    if(list->maxSize != SWL_ARRAY_LIST_DEFAULT_MIN_SIZE) {
        free(list->data);
        list->data = calloc(SWL_ARRAY_LIST_DEFAULT_MIN_SIZE, list->type->size);
        list->maxSize = SWL_ARRAY_LIST_DEFAULT_MIN_SIZE;
    } else {
        memset(list->data, 0, list->maxSize * list->type->size);
    }
    list->size = 0;
}

/**
 * clean up the array list, removing any allocated memory
 * @param list
 *  the list that needs to be cleaned. Note that to
 *  use the list again, it needs to be reinitialized.
 */
void swl_arrayList_cleanup(swl_arrayList_t* list) {
    if(list->size > 0) {
        swl_type_arrayCleanup(list->type, list->data, list->size);
    }
    free(list->data);
    list->data = NULL;
    list->type = NULL;
    list->maxSize = 0;
    list->size = 0;
}

/**
 * Return the type element pointer of the element at the given index.
 * @param list
 *  the list from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type element at the requested index if it exists, NULL otherwise
 */
swl_typeEl_t* swl_arrayList_getReference(const swl_arrayList_t* list, ssize_t index) {
    return swl_type_arrayGetReference(list->type, list->data, list->size, index);
}

/**
 * Return the type data pointer of the element at the given index.
 * @param list
 *  the list from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type data at the requested index if it exists, NULL otherwise
 */
swl_typeData_t* swl_arrayList_getValue(const swl_arrayList_t* list, ssize_t index) {
    return swl_type_arrayGetValue(list->type, list->data, list->size, index);
}

/**
 * Check whether the list should grow before adding an element.
 * It is assumed here that an element shall be added.
 * This should be called before element is added, and before size is updated.
 */
static void s_checkGrow(swl_arrayList_t* list) {
    if(list->size < list->maxSize) {
        return;
    }
    size_t newSize = list->maxSize * (1000 + SWL_ARRAY_LIST_GROWTH_FACTOR) / 1000;
    void* newData = realloc(list->data, newSize * list->type->size);
    if(newData == NULL) {
        return;
    }
    list->maxSize = newSize;
    list->data = newData;
    memset(list->data + list->size * list->type->size, 0, (list->maxSize - list->size) * list->type->size);
}

/**
 * Check whether the list should shrink.
 * This should be called after the actual element has been cleared, and the
 * size updated.
 */
static void s_checkShrink(swl_arrayList_t* list) {
    if(list->maxSize <= SWL_ARRAY_LIST_DEFAULT_MIN_SIZE) {
        return;
    }
    size_t shrinkSize = list->maxSize * SWL_ARRAY_LIST_SHRINK_SIZE / 1000;
    if(list->size >= shrinkSize) {
        return;
    }
    size_t postSize = list->maxSize * SWL_ARRAY_LIST_SHRINK_FACTOR / 1000;
    postSize = SWL_MAX(postSize, SWL_ARRAY_LIST_DEFAULT_MIN_SIZE);
    void* newData = realloc(list->data, postSize * list->type->size);
    if(newData == NULL) {
        return;
    }
    list->maxSize = postSize;
    list->data = newData;
}

/**
 * Insert the data at the given index.
 * The remaining elements will be shifted to the right.
 * To append at the end, use index -1
 * @param list
 *  the list to which the data has to be appended
 * @param data
 *  the data to be appended
 * @param index
 *  the index at which the data must be inserted. Valid indexes go from [-size - 1, size]
 * @return
 *  SWL_RC_OK if the data was properly inserted
 *  swl_rc_ne error code otherwise.
 */
swl_rc_ne swl_arrayList_insert(swl_arrayList_t* list, swl_typeData_t* data, ssize_t index) {
    ASSERT_NOT_NULL(list, SWL_RC_INVALID_PARAM, ME, "NULL");
    ASSERT_NOT_NULL(data, SWL_RC_INVALID_PARAM, ME, "NULL");
    s_checkGrow(list);
    ASSERT_FALSE(list->size == list->maxSize, SWL_RC_NOT_AVAILABLE, ME, "SIZE");

    size_t targetIndex = SWL_ARRAY_INDEX(index, list->size + 1);

    swl_typeEl_t* location = (swl_typeEl_t*) list->data + targetIndex * list->type->size;
    swl_type_arrayShift(list->type, location, list->maxSize - targetIndex, 1);
    swl_type_copyTo(list->type, location, data);
    list->size += 1;
    return SWL_RC_OK;
}

/**
 * Add element to the list
 * @param list
 *  the list to which to add the element
 * @param data
 *  the data to add.
 * @return
 *  the index at which the element was added, swl_rc_ne error code if failed.
 */
ssize_t swl_arrayList_add(swl_arrayList_t* list, swl_typeData_t* data) {
    s_checkGrow(list);
    ASSERT_FALSE(list->size >= list->maxSize, SWL_RC_NOT_AVAILABLE, ME, "SIZE");

    swl_typeEl_t* location = (swl_typeEl_t*) list->data + list->size * list->type->size;

    swl_type_copyTo(list->type, location, data);
    list->size++;
    return list->size - 1;
}

/**
 * Allocate an empty element
 * @param list
 *  the list to which to add the element
 * @return
 *  the pointer to an empty element, NULL if failed
 */
swl_typeEl_t* swl_arrayList_alloc(swl_arrayList_t* list) {
    s_checkGrow(list);
    ASSERT_FALSE(list->size >= list->maxSize, NULL, ME, "SIZE");
    list->size++;
    return (swl_typeEl_t*) list->data + (list->size - 1) * list->type->size;
}

/**
 * Set the value of the item at the given index.
 * @param list
 *  the list to which to set the element
 * @param val
 *  the data to set.
 * @param index
 *  the index at which to set. This index can be in range [- size, size - 1]
 *  Negative indexes shall be counted from back of list. Note that setting at -1 will add the
 *  element to the back. Inserting at -size, will set at the start.
 */
swl_rc_ne swl_arrayList_set(swl_arrayList_t* list, swl_typeData_t* data, ssize_t index) {
    return swl_type_arraySet(list->type, list->data, list->size, data, index);
}

/**
 * Delete the entry at the given index
 * @param list
 *  the list from which to delete the given entry
 * @param index
 *  the index at which to delete the given index
 * @return
 *  SWL_RC_OK if delete succeeded, error if not.
 *  Remaining elements after index shall be shifted one to the left
 */
swl_rc_ne swl_arrayList_delete(swl_arrayList_t* list, ssize_t index) {
    ASSERT_FALSE(list->size == 0, SWL_RC_INVALID_PARAM, ME, "SIZE");

    swl_rc_ne ret = swl_type_arrayDelete(list->type, list->data, list->size, index);
    if(ret == SWL_RC_OK) {
        list->size--;
        s_checkShrink(list);
    }
    return ret;
}

/**
 * Finds the index of a given value
 * @param list
 *  the arrayList in which to look
 * @param val
 *  the value to look for
 * @return
 *  the first index for which applies that swl_type_equals(type, val, swl_arrayBuf_getValue(buf, index))
 *  If no such index exists, -1 is returned.
 */
ssize_t swl_arrayList_find(const swl_arrayList_t* list, const swl_typeData_t* val) {
    return swl_type_arrayFind(list->type, list->data, list->size, val);
}

/**
 * Return the size of the arraylist
 * @param list
 *  the arraylist of which the size is requested
 * @return
 *  the current number of elements stored in the buffer if buf is valid, 0 otherwise.
 */
size_t swl_arrayList_size(const swl_arrayList_t* list) {
    return list->size;
}

/**
 * Check if two arrayLists are equal
 * @param arrayList1
 *  the first arrayList to compare
 * @param arrayList2
 *  the second arrayList to compare
 * @return
 *  true if all elements in arrayList1 are equal as in arrayList2, in same order, as checked with type equals
 */
bool swl_arrayList_equals(const swl_arrayList_t* al1, const swl_arrayList_t* al2) {
    ASSERT_NOT_NULL(al1, false, ME, "NULL");
    ASSERT_NOT_NULL(al2, false, ME, "NULL");
    ASSERT_EQUALS(al1->type, al2->type, false, ME, "DIFF");

    return swl_type_arrayEquals(al1->type, al1->data, al1->size, al2->data, al2->size);
}

/**
 * Create an iterator with which to iterate over the given buf
 *
 * @param buf
 *  the buffer for which to create an iterator
 */
swl_arrayListIt_t swl_arrayList_getFirstIt(const swl_arrayList_t* al) {
    swl_arrayListIt_t it;
    swl_arrayListIt_init(&it, al);
    return it;
}

/**
 * Initialize the given arrayList iterator on the given buffer
 * @param it
 *  the iterator to initialize
 * @param buf
 *  the list with which to initialize it
 */
void swl_arrayListIt_init(swl_arrayListIt_t* it, const swl_arrayList_t* al) {
    it->buf = al;
    it->index = 0;
    if(al->size == 0) {
        it->valid = false;
        return;
    }
    it->data = swl_arrayList_getValue(al, it->index);
    it->valid = (it->data != NULL);
}

/**
 * Delete the element currently pointed to by the iterator. This shall
 * also cleanup all type data at the location.
 * It shall also remain valid, so that
 * @param it
 *  the arrayList iterator pointing to the data to remove.
 * @note
 *  Calling del on the same iterator twice in a row will have no additional effect the second time.
 *  One should not use the data of the arrayList iterator, as the iterator is made invalid.
 */
void swl_arrayListIt_del(swl_arrayListIt_t* it) {
    if(!it->valid) {
        return;
    }
    swl_arrayList_delete((swl_arrayList_t*) it->buf, it->index);
    it->valid = false;
}

/**
 * Move the iterator to the next element.
 * @param it
 *  the iterator to move to the next element.
 * @note
 *  if the element does not exist, it->valid shall be put to false. The index
 *  shall then point to a nonexistant index.
 */
void swl_arrayListIt_next(swl_arrayListIt_t* it) {
    if(it->valid) {
        it->index++;
    }
    if(it->index >= it->buf->size) {
        it->valid = false;
        return;
    }
    it->data = swl_arrayList_getValue(it->buf, it->index);
    it->valid = (it->data != NULL);
}

/**
 * Return the index where this iterator currently points
 * @param it
 *  the iterator for which the index is requested.
 * @return
 *  the index of the element to which the iterator currently points.
 */
size_t swl_arrayListIt_index(swl_arrayListIt_t* it) {
    return it->index;
}

/**
 * Returns the data this iterator is currently pointing to
 * @param it
 *  the iterator pointing at the data
 * @return
 *  the swl type data at this iterator points to.
 */
swl_typeEl_t* swl_arrayListIt_data(swl_arrayListIt_t* it) {
    return it->data;
}

/**
 * Assign the value of the iterator to tgt data, doing shallow copy
 * @param it
 *  the iterator from which to extract the data
 * @param tgtData
 *  the target pointer to which assign the data
 *
 * @return
 *  true if assignment is successful, meaning iterator was valid and function could copy data, false otherwise.
 */
bool swl_arrayListIt_assignVal(swl_arrayListIt_t* it, swl_typeEl_t* tgtData) {
    if(!it->valid) {
        return false;
    }
    if(tgtData == NULL) {
        return false;
    }
    swl_type_t* myType = it->buf->type;
    memcpy(tgtData, it->data, myType->size);
    return true;
}

