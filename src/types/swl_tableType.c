/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_collType.h"
#include "swl/types/swl_tableType.h"
#include "swl/types/swl_tupleTypeArray.h"
#include "swl/swl_string.h"

#define ME "swlTblT"

/**
 * array handlers
 */
static ssize_t s_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    swl_table_t* table = (swl_table_t*) srcData;
    return swl_table_printLists(table, tgtStr, tgtStrSize, 0);
}


static bool s_equals_cb(swl_type_t* type _UNUSED, const swl_typeData_t* key0, const swl_typeData_t* key1) {
    swl_table_t* table0 = (swl_table_t*) key0;
    swl_table_t* table1 = (swl_table_t*) key1;
    return swl_table_equals(table0, table1);
}

static void s_cleanup_cb (swl_type_t* type _UNUSED, swl_typeEl_t* tgtData) {
    swl_table_t* table = (swl_table_t*) tgtData;
    swl_table_clear(table);
}

static swl_typeData_t* s_copy_cb (swl_type_t* type _UNUSED, const swl_typeData_t* src) {
    swl_table_t* table = (swl_table_t*) src;
    return swl_table_copy(table);
}

static bool s_toFile_cb (swl_type_t* type _UNUSED, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    swl_table_t* table = (swl_table_t*) srcData;

    return swl_table_fprintLists(table, file, 0, args);
}

swl_typeFun_t swl_tableType_fun = {
    .name = "swl_table",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_LIST,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .equals = (swl_type_equals_cb) s_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_cleanup_cb,
    .copy = (swl_type_copy_cb) s_copy_cb,
    .toFile = (swl_type_toFile_cb) s_toFile_cb,
    .subFun = &swl_tableCollType_fun,
};

static swl_rc_ne s_col_init_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_tableType_t* tType = (swl_tableType_t*) colType;
    swl_table_initMax(inCollection, (swl_tupleType_t*) tType->type.elementType, SWL_TABLE_DEFAULT_MIN_SIZE);
    return SWL_RC_OK;
}

static swl_rc_ne s_col_initFromArray_cb(swl_listSType_t* colType, swl_coll_t* inCollection, swl_typeEl_t* array, size_t arraySize) {
    swl_tableType_t* tType = (swl_tableType_t*) colType;
    swl_tupleType_t* elType = (swl_tupleType_t*) tType->type.elementType;
    swl_table_t* table = (swl_table_t*) inCollection;

    swl_rc_ne ret = swl_table_initMax(table, elType, arraySize);
    if(!swl_rc_isOk(ret)) {
        return ret;
    }
    table->curNrTuples = arraySize;

    return swl_type_arrayCopy(&elType->type, table->tuples, array, table->curNrTuples);
}

static size_t s_col_maxSize_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection _UNUSED) {
    return 0;
}

static void s_col_clear_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_table_t* table = (swl_table_t*) inCollection;
    swl_table_clear(table);

}

static void s_col_cleanup_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_table_t* table = (swl_table_t*) inCollection;
    swl_table_destroy(table);
}

/**
 * If empty is allowed, size will return the number of non empty elements.
 */
static size_t s_col_size_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_getSize(table);
}

/**
 * Adding to a full tta will result in last element being dropped and overwritten.
 */
static ssize_t s_col_add_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_add(table, data);
}

static swl_typeEl_t* s_col_alloc_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_alloc(table);
}

static swl_rc_ne s_col_insert_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_insert(table, data, index);
}

static swl_rc_ne s_col_set_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_setTuple(table, index, data);
}

/**
 * For array type, delete shall always try to remove a non-empty element. Other elements do not count
 * for an array that has a fixed maxSize.
 */
static swl_rc_ne s_col_delete_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_delete(table, index);
}

static ssize_t s_col_find_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_find(table, data);
}

static bool s_col_equals_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection1, swl_coll_t* inCollection2) {
    swl_table_t* table1 = (swl_table_t*) inCollection1;
    swl_table_t* table2 = (swl_table_t*) inCollection2;

    return swl_table_equals(table1, table2);
}

static swl_typeData_t* s_col_getValue_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_getTuple(table, index);
}

//Since we are working with tuples, getValue == getRef
static swl_typeEl_t* s_col_getReference_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_table_t* table = (swl_table_t*) inCollection;
    return swl_table_getTuple(table, index);
}

static inline void s_col_setItData(swl_tableType_t* tType, swl_listSTypeIt_t* it) {
    swl_table_t* table = (swl_table_t*) it->coll;
    if(it->index < table->curNrTuples) {
        it->data = table->tuples + it->index * tType->type.elementType->size;

        swl_typeData_t* td = SWL_TYPE_EL_TO_DATA(tType->type.elementType, it->data);
        it->valid = !swl_type_isEmpty(tType->type.elementType, td);
    } else {
        it->valid = false;
    }
}

static swl_listSTypeIt_t s_col_getFirstIt_cb(swl_listSType_t* colType _UNUSED, const swl_coll_t* inCollection) {
    swl_tableType_t* ttaType = (swl_tableType_t*) colType;
    swl_listSTypeIt_t data;
    memset(&data, 0, sizeof(swl_listSTypeIt_t));
    data.coll = (swl_coll_t*) inCollection;
    data.index = 0;
    data.dataType = ttaType->type.elementType;
    s_col_setItData(ttaType, &data);
    return data;
}
static void s_col_nextIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    swl_tableType_t* ttaType = (swl_tableType_t*) colType;
    if(it->valid) {
        it->index++;
    }
    s_col_setItData(ttaType, it);
}

static void s_col_delIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    if(it->valid) {
        s_col_delete_cb(colType, it->coll, it->index);
        it->valid = false;
    }
}

swl_listSTypeFun_t swl_tableCollType_fun = {
    .name = "swl_tupleTypeArray",
    .init = s_col_init_cb,
    .initFromArray = s_col_initFromArray_cb,
    .clear = s_col_clear_cb,
    .cleanup = s_col_cleanup_cb,
    .maxSize = s_col_maxSize_cb,
    .size = s_col_size_cb,
    .add = s_col_add_cb,
    .alloc = s_col_alloc_cb,
    .insert = s_col_insert_cb,
    .set = s_col_set_cb,
    .delete = s_col_delete_cb,
    .find = s_col_find_cb,
    .equals = s_col_equals_cb,
    .getValue = s_col_getValue_cb,
    .getReference = s_col_getReference_cb,
    .firstIt = s_col_getFirstIt_cb,
    .nextIt = s_col_nextIt_cb,
    .delIt = s_col_delIt_cb,
};

static swl_typeData_t* s_getElementValue_cb(swl_ttCollType_t* type, swl_tuple_t* inCollection,
                                            ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    swl_table_t* table = (swl_table_t*) inCollection;

    return swl_table_getValue(table, tupleIndex, typeIndex);
}

static swl_typeEl_t* s_getElementRef_cb(swl_ttCollType_t* type _UNUSED, swl_tuple_t* inCollection,
                                        ssize_t tupleIndex, size_t typeIndex) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    swl_table_t* table = (swl_table_t*) inCollection;


    return swl_table_getReference(table, tupleIndex, typeIndex);
}

static swl_rc_ne s_setElementValue_cb(swl_ttCollType_t* type _UNUSED, swl_tuple_t* inCollection,
                                      ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data) {
    swl_table_t* table = (swl_table_t*) inCollection;

    return swl_table_setValue(table, tupleIndex, typeIndex, data);
}


static swl_rc_ne s_columnToArrayOffset(swl_ttCollType_t* type _UNUSED, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                       const swl_typeEl_t* inCollection, size_t typeIndex, size_t offSet) {
    swl_table_t* table = (swl_table_t*) inCollection;

    return swl_table_columnToArrayOffset(tgtArray, tgtArraySize, table, typeIndex, offSet);
}

static swl_rc_ne s_columnToArray(swl_ttCollType_t* type _UNUSED, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                 const swl_ttColl_t* srcArray, size_t typeIndex) {
    swl_table_t* table = (swl_table_t*) srcArray;

    return swl_table_columnToArray(tgtArray, tgtArraySize, table, typeIndex);
}

static ssize_t s_findMatchingTupleIndex(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                        ssize_t offset, size_t srcTypeIndex, const swl_typeData_t* srcData) {
    swl_table_t* table = (swl_table_t*) array;

    return swl_table_findMatchingValue(table, offset, srcTypeIndex, srcData);
}

static swl_tuple_t* s_getMatchingTuple(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                       ssize_t offset, size_t srcTypeIndex, const swl_typeData_t* srcData) {
    swl_table_t* table = (swl_table_t*) array;
    return swl_table_getMatchingTupleOffset(table, srcTypeIndex, srcData, offset);
}

static swl_typeData_t* s_getMatchingElement(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                            ssize_t offset, size_t tgtTypeIndex, size_t srcTypeIndex, const swl_typeData_t* srcData) {
    swl_table_t* table = (swl_table_t*) array;
    return swl_table_getMatchingValueOffset(table, tgtTypeIndex, srcTypeIndex, srcData, offset);
}


static ssize_t s_findMatchingTupleIndexByMask(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                              ssize_t offset, const swl_tuple_t* srcData, swl_mask_m mask) {

    swl_table_t* table = (swl_table_t*) array;
    return swl_table_findMatchingTupleByMask(table, offset,
                                             srcData, mask);
}

static swl_tuple_t* s_getMatchingTupleByMask(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                             ssize_t offset, const swl_tuple_t* srcData, swl_mask_m mask) {

    swl_table_t* table = (swl_table_t*) array;

    return swl_table_getMatchingTupleByTuple(table,
                                             srcData, mask, offset);
}

static swl_typeData_t* s_getMatchingElementByMask(swl_ttCollType_t* type _UNUSED, swl_ttColl_t* array,
                                                  ssize_t offset, size_t tgtTypeIndex, const swl_tuple_t* srcData, swl_mask_m mask) {

    swl_table_t* table = (swl_table_t*) array;

    return swl_table_getMatchingValueByTuple(table,
                                             tgtTypeIndex, srcData, mask, offset);
}

static bool s_fPrintLists(swl_ttCollType_t* type _UNUSED, FILE* stream, swl_ttColl_t* array, ssize_t offset, swl_print_args_t* args) {
    swl_table_t* table = (swl_table_t*) array;

    return swl_table_fprintLists(table, stream, offset, args);

    return false;
}

static bool s_fPrintMaps(swl_ttCollType_t* type _UNUSED, FILE* stream, swl_ttColl_t* array, ssize_t offset,
                         swl_print_args_t* args, char** names, char* suffix) {
    swl_table_t* table = (swl_table_t*) array;


    return swl_table_fprintMaps(table, stream, offset, args, names, suffix);
}


swl_ttCollTypeFun_t swl_tableTtCollType_fun = {
    .collTypeFun = &swl_tableCollType_fun,
    .getElementValue = s_getElementValue_cb,
    .getElementRef = s_getElementRef_cb,
    .setElementValue = s_setElementValue_cb,
    .columnToArrayOffset = s_columnToArrayOffset,
    .columnToArray = s_columnToArray,
    .findMatchingTupleIndex = s_findMatchingTupleIndex,
    .getMatchingTuple = s_getMatchingTuple,
    .getMatchingElement = s_getMatchingElement,
    .findMatchingTupleIndexByMask = s_findMatchingTupleIndexByMask,
    .getMatchingTupleByMask = s_getMatchingTupleByMask,
    .getMatchingElementByMask = s_getMatchingElementByMask,
    .fPrintLists = s_fPrintLists,
    .fPrintMaps = s_fPrintMaps,
};



static swl_typeExtIt_t ttaExtIt = {
    .id = SWL_TYPE_COL_FUN_UID,
    .extFun = &swl_tableCollType_fun,
};

SWL_CONSTRUCTOR static void s_initListExt(void) {
    swl_llist_append(&swl_tableType_fun.extensions, &ttaExtIt.it);
}
