/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include "swl/swl_common.h"
#include "swl/subtypes/swl_numberSType.h"
#include "swl/types/swl_typeMath.h"

#define ME "swl_tpM"

void s_doAdd(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, void* userData _UNUSED, swl_typeCallInfo_t* info _UNUSED) {
    ASSERT_EQUALS(arraySize, 3, , ME, "SIZE");
    if(type->typeFun->flag != SWL_TYPE_FLAG_NUMBER) {
        return;
    }
    swl_numberSType_add(type, array[0], array[1], array[2]);
}

void s_doSub(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, void* userData _UNUSED, swl_typeCallInfo_t* info _UNUSED) {
    ASSERT_EQUALS(arraySize, 3, , ME, "SIZE");
    if(type->typeFun->flag != SWL_TYPE_FLAG_NUMBER) {
        return;
    }
    swl_numberSType_sub(type, array[0], array[1], array[2]);
}

void s_doMul(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, void* userData _UNUSED, swl_typeCallInfo_t* info _UNUSED) {
    ASSERT_EQUALS(arraySize, 3, , ME, "SIZE");
    if(type->typeFun->flag != SWL_TYPE_FLAG_NUMBER) {
        return;
    }
    swl_numberSType_mul(type, array[0], array[1], array[2]);
}

void s_doDiv(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, void* userData _UNUSED, swl_typeCallInfo_t* info _UNUSED) {
    ASSERT_EQUALS(arraySize, 3, , ME, "SIZE");
    if(type->typeFun->flag != SWL_TYPE_FLAG_NUMBER) {
        return;
    }
    swl_numberSType_div(type, array[0], array[1], array[2]);
}
void s_doMod(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, void* userData _UNUSED, swl_typeCallInfo_t* info _UNUSED) {
    ASSERT_EQUALS(arraySize, 3, , ME, "SIZE");
    if(type->typeFun->flag != SWL_TYPE_FLAG_NUMBER) {
        return;
    }
    swl_numberSType_mod(type, array[0], array[1], array[2]);
}


void swl_typeMath_add(swl_type_t* type, swl_typeEl_t* out, swl_typeEl_t* in1, swl_typeEl_t* in2) {
    swl_typeEl_t* dataArray[3] = {out, in1, in2};
    swl_type_callOnBaseTypes(type, dataArray, 3, s_doAdd, NULL);
}


void swl_typeMath_sub(swl_type_t* type, swl_typeEl_t* out, swl_typeEl_t* in1, swl_typeEl_t* in2) {
    swl_typeEl_t* dataArray[3] = {out, in1, in2};
    swl_type_callOnBaseTypes(type, dataArray, 3, s_doSub, NULL);
}

void swl_typeMath_mul(swl_type_t* type, swl_typeEl_t* out, swl_typeEl_t* in1, swl_typeEl_t* in2) {
    swl_typeEl_t* dataArray[3] = {out, in1, in2};
    swl_type_callOnBaseTypes(type, dataArray, 3, s_doMul, NULL);
}

void swl_typeMath_div(swl_type_t* type, swl_typeEl_t* out, swl_typeEl_t* in1, swl_typeEl_t* in2) {
    swl_typeEl_t* dataArray[3] = {out, in1, in2};
    swl_type_callOnBaseTypes(type, dataArray, 3, s_doDiv, NULL);
}

void swl_typeMath_mod(swl_type_t* type, swl_typeEl_t* out, swl_typeEl_t* in1, swl_typeEl_t* in2) {
    swl_typeEl_t* dataArray[3] = {out, in1, in2};
    swl_type_callOnBaseTypes(type, dataArray, 3, s_doMod, NULL);
}
