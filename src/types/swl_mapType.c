/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_map.h"
#include "swl/types/swl_mapType.h"
#include "swl/subtypes/swl_mapSType.h"

#define ME "swlMapT"

/**
 * Provide a pointer to the key data
 */
swl_typeEl_t* swl_mapType_getEntryKeyRef(const swl_mapType_t* mapT _UNUSED, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return (swl_typeEl_t*) entry;
}

/**
 * Provide a pointer to the value data
 */
swl_typeEl_t* swl_mapType_getEntryValueRef(const swl_mapType_t* mapT, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return (swl_typeEl_t*) entry + mapT->keyType->size;
}

/**
 * Provide a pointer to the key data
 */
swl_typeData_t* swl_mapType_getEntryKeyValue(const swl_mapType_t* mapT, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return SWL_TYPE_EL_TO_DATA(mapT->keyType, entry);
}

/**
 * Provide a pointer to the value data
 */
swl_typeData_t* swl_mapType_getEntryValueValue(const swl_mapType_t* mapT, const swl_mapEntry_t* entry) {
    if(entry == NULL) {
        return NULL;
    }
    return SWL_TYPE_EL_TO_DATA(mapT->valueType, entry + mapT->keyType->size);
}

static ssize_t s_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_map_t* srcData) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();
    swl_mapType_t* mapType = (swl_mapType_t*) type;

    ssize_t curSize = snprintf(tgtStr, tgtStrSize, "%s", args->delim[SWL_PRINT_DELIM_MAP_OPEN]);
    ssize_t maxSize = tgtStrSize;
    ssize_t totalSize = curSize;

    swl_type_t* keyType = mapType->keyType;
    swl_type_t* valueType = mapType->valueType;

    size_t mapSize = swl_map_size(srcData);

    swl_mapIt_t it;
    swl_map_for_each(it, srcData) {
        ssize_t keySize = swl_type_toCharEsc(keyType, &tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), swl_map_itKey(&it), args);
        ASSERTS_TRUE(keySize > 0, keySize, ME, "ERR");
        totalSize += keySize;

        ssize_t assignSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
        ASSERTS_TRUE(assignSize > 0, assignSize, ME, "ERR");
        totalSize += assignSize;
        ssize_t valueSizeSize = swl_type_toCharEsc(valueType, &tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), swl_map_itValue(&it), args);
        ASSERTS_TRUE(valueSizeSize > 0, valueSizeSize, ME, "ERR");
        totalSize += valueSizeSize;

        if(swl_map_itIndex(&it) < mapSize - 1) {
            ssize_t nextSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_MAP_NEXT]);
            ASSERTS_TRUE(nextSize > 0, nextSize, ME, "ERR");
            totalSize += nextSize;
        }
    }

    ssize_t closeSize = snprintf(&tgtStr[totalSize], SWL_MAX(0, maxSize - totalSize), "%s", args->delim[SWL_PRINT_DELIM_MAP_CLOSE]);
    ASSERTS_TRUE(closeSize > 0, closeSize, ME, "ERR");

    totalSize += curSize;
    return totalSize;
}

static bool s_fromChar_cb(swl_type_t* type, swl_map_t* map, const char* srcStr) {
    const swl_print_args_t* args = swl_print_getDefaultArgs();

    swl_mapType_t* mapType = (swl_mapType_t*) type;

    if(!swl_map_isInitialized(map)) {
        swl_map_init(map, mapType->keyType, mapType->valueType);
    }

    swl_type_t* keyType = mapType->keyType;
    swl_type_t* valueType = mapType->valueType;

    size_t index = strlen(args->delim[SWL_PRINT_DELIM_MAP_OPEN]);
    size_t assignSize = strlen(args->delim[SWL_PRINT_DELIM_MAP_ASSIGN]);
    size_t nextSize = strlen(args->delim[SWL_PRINT_DELIM_MAP_NEXT]);

    size_t i = 0;
    while(true) {
        if(!swl_print_hasNext(args, &srcStr[index], false)) {
            break;
        }
        if(i > 0) {
            index += nextSize;
        }
        swl_mapEntry_t* entry = swl_map_alloc(map);

        ssize_t keySize = swl_type_fromCharPrint(keyType, swl_map_getEntryKeyRef(map, entry), &srcStr[index], args, SWL_PRINT_DELIM_MAP_ASSIGN);
        ASSERTS_TRUE(keySize >= 0, false, ME, "ERR");
        index += keySize + assignSize;

        ssize_t valueSize = swl_type_fromCharPrint(valueType, swl_map_getEntryValueRef(map, entry), &srcStr[index], args, SWL_PRINT_DELIM_MAP_NEXT);
        ASSERTS_TRUE(valueSize >= 0, false, ME, "ERR");

        index += valueSize;
        i++;
    }

    return true;
}

static bool s_equals_cb(swl_type_t* type _UNUSED, const swl_map_t* key0, const swl_map_t* key1) {
    return swl_map_equals(key0, key1);
}

static swl_typeData_t* s_copy_cb(swl_type_t* type _UNUSED, const swl_map_t* src) {
    swl_map_t* map = calloc(1, sizeof(swl_map_t));
    ASSERT_NOT_NULL(map, NULL, ME, "NULL");

    swl_map_initExt(map, src->keyType, src->valueType, src->dataVector.blockSize);

    swl_mapIt_t it;
    swl_map_for_each(it, src) {
        swl_map_add(map, swl_map_itKey(&it), swl_map_itValue(&it));
    }
    return map;
}

static bool s_toFile_cb(swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;

    swl_type_t* keyType = mapType->keyType;
    swl_type_t* valueType = mapType->valueType;

    swl_print_addOpen(file, args, false);

    size_t mapSize = swl_map_size(srcData);

    swl_mapIt_t it;
    swl_map_for_each(it, srcData) {
        ASSERT_TRUE(swl_type_toFilePrivPrint(keyType, file, swl_map_itKey(&it), args),
                    false, ME, "FAIL");
        swl_print_addAssign(file, args);
        ASSERT_TRUE(swl_type_toFilePrivPrint(valueType, file, swl_map_itValue(&it), args),
                    false, ME, "FAIL");

        if(swl_map_itIndex(&it) < mapSize - 1) {
            swl_print_addNext(file, args, false);
        }
    }
    swl_print_addClose(file, args, false);

    return true;
}

static void s_cleanup_cb(swl_type_t* type _UNUSED, swl_typeEl_t* tgtData) {
    swl_map_cleanup((swl_map_t*) tgtData);
}

swl_typeFun_t swl_map_fun = {
    .name = "swl_map",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_MAP,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_fromChar_cb,
    .equals = (swl_type_equals_cb) s_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_cleanup_cb,
    .copy = (swl_type_copy_cb) s_copy_cb,
    .toFile = (swl_type_toFile_cb) s_toFile_cb,
    .subFun = &swl_mapType_mapSTypeFun,
};



swl_rc_ne s_init_cb (swl_mapSType_t* type, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    swl_map_init(myMap, mapType->keyType, mapType->valueType);
    return SWL_RC_OK;
}

/**
 * Clear the map, removing the contents but keeping the map initialized
 */
void s_clear_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    swl_map_clear(myMap);
}

/**
 * Cleanup the map, deallocating whatever needs to be deallocated. The map should
 * be reinitialized before further use.
 */
void s_mapCleanup_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    swl_map_cleanup(myMap);
}

/**
 * Return the maximum size of this map type.
 * If there is no maximum size, other than "dynamic memory allocation allowance", 0 shall be returned.
 */
size_t s_maxSize_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll _UNUSED) {
    return 0;
}

/**
 * Return the type of the keys of this collection.
 * A map currently only supports having a single key type.
 */
swl_type_t* s_getKeyType_cb (swl_mapSType_t* colType _UNUSED, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return myMap->keyType;
}

/**
 * Return the type of the value of the given key. If isSingleValueMap is set to true, then
 * this must return said type regardsless of key input (i.e. NULL or non-existant key).
 * If isSingleValueMap is set to false, it must return NULL for keys that are not present, and the relevant type if the
 * value exists that matches the given key.
 */
swl_type_t* s_getValueType_cb (swl_mapSType_t* colType _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key _UNUSED) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return myMap->valueType;
}


/**
 * The current size of this map.
 *
 * For fixed size maps, this shall return the number of non-zero elements.
 * These maps may have an allowEmpty flag, which basically means whether "empty" is a valid value,
 * and size counting should continue after.
 */
size_t s_size_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_size(myMap);
}

/**
 * Add data to given map
 *
 * For fixed size maps, data may be written to first empty space, if possible.
 */
bool s_add_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key, swl_typeData_t* value) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_add(myMap, key, value);
}

/**
 * Provide reference to an "empty" element, if possible.
 * For dynamic maps, it shall create a new empty element.
 */
swl_typeEl_t* s_alloc_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_alloc(myMap);
}

/**
 * Write data at given index, removing whatever data may have been there before.
 * Provided data shall be deep copied into target space.
 */
swl_rc_ne s_set_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key, swl_typeData_t* data) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_set(myMap, key, data) ? SWL_RC_OK : SWL_RC_ERROR;
}

/**
 * Delete the data at given index. All other data with index higher then provided index shall be shifted one down, if
 * it's a "same type" map.
 */
swl_rc_ne s_delete_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key) {
    swl_map_t* myMap = (swl_map_t*) coll;
    swl_map_delete(myMap, key);
    return SWL_RC_OK;
}

/**
 * Find the element of the given data value
 */
swl_typeEl_t* s_find_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* data) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_find(myMap, data);
}

/**
 * Check whether two maps are equal
 */
bool s_mapEquals_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll1, swl_mapEl_t* coll2) {
    swl_map_t* myMap1 = (swl_map_t*) coll1;
    swl_map_t* myMap2 = (swl_map_t*) coll2;
    return swl_map_equals(myMap1, myMap2);
}

/**
 * Get the value data pointer of the value at the given index.
 */
swl_typeData_t* s_getValue_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_get(myMap, key);
}

/**
 * Provide the element pointer of the value at the given index.
 */
swl_typeEl_t* s_getReference_cb(swl_mapSType_t* type _UNUSED, swl_mapEl_t* coll, swl_typeData_t* key) {
    swl_map_t* myMap = (swl_map_t*) coll;
    return swl_map_getRef(myMap, key);
}

/**
 * Get an iterator, initialized at the first element
 */
swl_listSTypeIt_t s_getFirstIt_cb(swl_mapSType_t* type _UNUSED, const swl_mapEl_t* coll) {
    swl_map_t* myMap = (swl_map_t*) coll;
    swl_listSTypeIt_t it = swl_unLiList_getFirstIt(&myMap->dataVector);
    it.dataType = myMap->valueType;
    return it;
}

/**
 * Move iterator to next element
 */
void s_map_nextIt_cb(swl_mapSType_t* type _UNUSED, swl_listSTypeIt_t* it) {
    swl_unLiList_nextIt(it);
}

/**
 * Delete current element. Note that this will NOT move the iterator to next element, so
 * you MUST still call nextIt.
 * Calling  delIt a second time shall result in no change.
 */
void s_map_delIt_cb(swl_mapSType_t* type, swl_listSTypeIt_t* it) {
    swl_mapEntry_t* entry = it->data;
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    swl_typeEl_t* keyEl = swl_mapType_getEntryKeyRef(mapType, entry);
    swl_type_cleanup(mapType->keyType, keyEl);
    swl_typeEl_t* valueEl = swl_mapType_getEntryValueRef(mapType, entry);
    swl_type_cleanup(mapType->valueType, valueEl);
    swl_unLiList_delIt(it);
}



swl_typeData_t* s_key_cb(swl_mapSType_t* type, const swl_mapEl_t* coll _UNUSED, swl_mapEl_t* el) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    return swl_mapType_getEntryKeyValue(mapType, el);
}

swl_typeData_t* s_value_cb(swl_mapSType_t* type, const swl_mapEl_t* coll _UNUSED, swl_mapEl_t* el) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    return swl_mapType_getEntryValueValue(mapType, el);
}

swl_typeEl_t* s_keyRef_cb(swl_mapSType_t* type, const swl_mapEl_t* coll _UNUSED, swl_mapEl_t* el) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    return swl_mapType_getEntryKeyRef(mapType, el);
}

swl_typeEl_t* s_valueRef_cb(swl_mapSType_t* type, const swl_mapEl_t* coll _UNUSED, swl_mapEl_t* el) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    return swl_mapType_getEntryValueRef(mapType, el);
}

swl_type_t* s_valueType_cb(swl_mapSType_t* type, const swl_mapEl_t* coll _UNUSED, swl_mapEl_t* el _UNUSED) {
    swl_mapType_t* mapType = (swl_mapType_t*) type;
    return mapType->valueType;
}



swl_mapSTypeFun_t swl_mapType_mapSTypeFun = {
    .init = s_init_cb,
    .clear = s_clear_cb,
    .cleanup = s_mapCleanup_cb,
    .maxSize = s_maxSize_cb,
    .getKeyType = s_getKeyType_cb,
    .getValueType = s_getValueType_cb,
    .size = s_size_cb,
    .add = s_add_cb,
    .alloc = s_alloc_cb,
    .set = s_set_cb,
    .delete = s_delete_cb,
    .find = s_find_cb,
    .equals = s_mapEquals_cb,
    .getValue = s_getValue_cb,
    .getReference = s_getReference_cb,
    .firstIt = s_getFirstIt_cb,
    .nextIt = s_map_nextIt_cb,
    .delIt = s_map_delIt_cb,
    .getEntryKey = s_key_cb,
    .getEntryVal = s_value_cb,
    .getEntryKeyRef = s_keyRef_cb,
    .getEntryValRef = s_valueRef_cb,
    .getEntryValType = s_valueType_cb,
};




