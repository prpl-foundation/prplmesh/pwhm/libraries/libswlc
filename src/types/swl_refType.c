/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_refType.h"

#define ME "tRef"

static ssize_t s_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    swl_refType_t* refType = (swl_refType_t*) type;
    return swl_type_toChar(refType->refType, tgtStr, tgtStrSize, srcData);
}

static bool s_fromChar_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr) {
    swl_refType_t* refType = (swl_refType_t*) type;
    swl_typeEl_t** elRef = (swl_typeEl_t**) tgtData;
    swl_typeEl_t* data = *elRef;
    if(data == NULL) {
        data = calloc(1, refType->refType->size);
        *elRef = data;
    }
    bool success = swl_type_fromChar(refType->refType, data, srcStr);
    if(!success) {
        free(data);
        *elRef = NULL;
    }
    return success;
}

static bool s_fromCharExt_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {
    swl_refType_t* refType = (swl_refType_t*) type;

    swl_typeEl_t** elRef = (swl_typeEl_t**) tgtData;
    swl_typeEl_t* data = *elRef;
    if(data == NULL) {
        data = calloc(1, refType->refType->size);
        *elRef = data;
    }

    bool success = swl_type_fromCharExt(refType->refType, data, srcStr, args);
    if(!success) {
        SAH_TRACEZ_ERROR(ME, "Exit %s", srcStr);
        free(data);
        *elRef = NULL;
    }
    return success;
}

static bool s_equals_cb (swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1) {
    swl_refType_t* refType = (swl_refType_t*) type;
    return swl_type_equals(refType->refType, key0, key1);

}
static swl_typeData_t* s_copy_cb (swl_type_t* type, const swl_typeData_t* srcData) {
    swl_refType_t* refType = (swl_refType_t*) type;
    return swl_type_copy(refType->refType, srcData);
}

static bool s_toFile_cb (swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    swl_refType_t* refType = (swl_refType_t*) type;
    return swl_type_toFile(refType->refType, file, srcData, args);
}

static void s_cleanup_cb (swl_type_t* type, swl_typeEl_t* tgtData) {
    swl_refType_t* refType = (swl_refType_t*) type;
    swl_typeEl_t** refEl = (swl_typeEl_t**) tgtData;
    if(*refEl == NULL) {
        return;
    }
    swl_type_cleanup(refType->refType, *refEl);
    free(*refEl);
}

swl_typeFun_t swl_refType_fun = {
    .name = "swl_ref",
    .isValue = false,
    .flag = SWL_TYPE_FLAG_REF,
    .toChar = s_toChar_cb,
    .fromChar = s_fromChar_cb,
    .fromCharExt = s_fromCharExt_cb,
    .equals = s_equals_cb,
    .copy = s_copy_cb,
    .cleanup = s_cleanup_cb,
    .toFile = s_toFile_cb,
};
