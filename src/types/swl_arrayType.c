/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_arrayType.h"
#include "swl/swl_string.h"

#define ME "swlA_T"

/**
 * Return the type data pointer of the element at the given index.
 * @param arrayType
 *  the arrayType that matches the given array
 * @param srcArr
 *  the buf from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type data at the requested index if it exists, NULL otherwise
 */
swl_typeData_t* swl_arrayType_getArrayValue(swl_arrayType_t* arrayType, const swl_array_t* srcArr, size_t index) {
    return swl_type_arrayGetValue(arrayType->type.elementType, srcArr, arrayType->maxNrElements, index);
}

/**
 * Return the type element pointer of the element at the given index.
 * @param arrayType
 *  the arrayType that matches the given array
 * @param srcArr
 *  the buf from which the value should be retrieved
 * @param index
 *  the index of the requested element. This index can be of range [-size, size -1],
 *  where negative values start counting at the back
 * @param return
 *  the pointer to type element at the requested index if it exists, NULL otherwise
 */
swl_typeEl_t* swl_arrayType_getArrayReference(swl_arrayType_t* arrayType, const swl_array_t* srcArr, size_t index) {
    return swl_type_arrayGetReference(arrayType->type.elementType, srcArr, arrayType->maxNrElements, index);
}

/**
 * array handlers
 */
static ssize_t s_toChar_cb(swl_arrayType_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData) {
    return swl_type_arrayToCharPrint(type->type.elementType, tgtStr, tgtStrSize,
                                     srcData, type->maxNrElements, type->allowEmpty);
}

static bool s_fromCharExt_cb(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args) {

    swl_arrayType_t* arrayType = (swl_arrayType_t*) type;
    swl_type_t* elementType = arrayType->type.elementType;

    swl_array_t* typedArr = (swl_array_t*) tgtData;

    return swl_type_arrayFromCharExt(elementType, typedArr, arrayType->maxNrElements, srcStr, args, NULL);
}

static bool s_equals_cb(swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1) {
    swl_arrayType_t* arrayType = (swl_arrayType_t*) type;
    swl_type_t* elementType = arrayType->type.elementType;

    swl_array_t* typedArr0 = (swl_array_t*) key0;
    swl_array_t* typedArr1 = (swl_array_t*) key1;

    for(size_t i = 0; i < arrayType->maxNrElements; i++) {
        if(!swl_type_equals(elementType,
                            swl_arrayType_getArrayValue(arrayType, typedArr0, i),
                            swl_arrayType_getArrayValue(arrayType, typedArr1, i))) {
            return false;
        }
    }
    return true;
}

static void s_cleanup_cb (swl_type_t* type, swl_typeEl_t* tgtData) {
    swl_arrayType_t* arrayType = (swl_arrayType_t*) type;
    swl_type_t* elementType = arrayType->type.elementType;

    swl_array_t* typedArr = (swl_array_t*) tgtData;

    for(size_t i = 0; i < arrayType->maxNrElements; i++) {
        swl_type_cleanup(elementType, swl_arrayType_getArrayReference(arrayType, typedArr, i));
    }
}

static swl_typeData_t* s_copy_cb (swl_type_t* type, const swl_typeData_t* src) {
    swl_arrayType_t* arrayType = (swl_arrayType_t*) type;
    swl_typeData_t* data = calloc(1, arrayType->type.type.size);

    const swl_array_t* srcArr = (const swl_array_t*) src;
    swl_array_t* tgtArr = (swl_array_t*) data;

    for(size_t i = 0; i < arrayType->maxNrElements; i++) {
        swl_type_copyTo(arrayType->type.elementType, swl_arrayType_getArrayReference(arrayType, tgtArr, i), swl_arrayType_getArrayValue(arrayType, srcArr, i));
    }

    return data;
}

static bool s_toFile_cb (swl_arrayType_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args) {
    return swl_type_arrayToFilePrint(type->type.elementType, file,
                                     srcData, type->maxNrElements, type->allowEmpty, args);
}

swl_typeFun_t swl_arrayType_fun = {
    .name = "swl_array",
    .isValue = true,
    .flag = SWL_TYPE_FLAG_LIST,
    .toChar = (swl_type_toChar_cb) s_toChar_cb,
    .fromCharExt = (swl_type_fromCharExt_cb) s_fromCharExt_cb,
    .equals = (swl_type_equals_cb) s_equals_cb,
    .cleanup = (swl_type_cleanup_cb) s_cleanup_cb,
    .copy = (swl_type_copy_cb) s_copy_cb,
    .toFile = (swl_type_toFile_cb) s_toFile_cb,
    .subFun = &swl_arrayCollectionType_fun,
};

static swl_rc_ne s_col_init_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_arrayType_t* abType = (swl_arrayType_t*) colType;
    memset(inCollection, 0, abType->type.elementType->size * abType->maxNrElements);
    return SWL_RC_OK;
}

static swl_rc_ne s_col_initFromArray_cb(swl_listSType_t* colType, swl_coll_t* inCollection, swl_typeEl_t* array, size_t arraySize) {
    swl_arrayType_t* abType = (swl_arrayType_t*) colType;
    memset(inCollection, 0, abType->maxNrElements * abType->type.elementType->size);
    swl_rc_ne ret = swl_type_arrayCopy(abType->type.elementType, inCollection, array, SWL_MIN(arraySize, abType->maxNrElements));

    if(swl_rc_isOk(ret)) {
        return arraySize <= abType->maxNrElements ? SWL_RC_OK : SWL_RC_NOT_AVAILABLE;
    } else {
        return ret;
    }
}

static size_t s_col_maxSize_cb(swl_listSType_t* colType, swl_coll_t* inCollection _UNUSED) {
    swl_arrayType_t* abType = (swl_arrayType_t*) colType;
    return abType->maxNrElements;
}

static void s_col_clear_cb(swl_listSType_t* colType, swl_coll_t* inCollection) {
    swl_arrayType_t* abType = (swl_arrayType_t*) colType;
    swl_type_arrayCleanup(abType->type.elementType, inCollection, abType->maxNrElements);
}

static void s_col_cleanup_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    s_col_clear_cb(colType, inCollection);
}

/**
 * If empty is allowed, size will return the number of non empty elements.
 */
static size_t s_col_size_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(at->allowEmpty) {
        return swl_type_arrayNotEmpty(at->type.elementType, inCollection, at->maxNrElements);
    } else {
        return swl_type_arraySize(at->type.elementType, inCollection, at->maxNrElements);
    }
}
static ssize_t s_col_add_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    ssize_t addIndex = -1;
    if(at->allowEmpty) {
        addIndex = swl_type_getFirstEmpty(at->type.elementType, inCollection, at->maxNrElements);
    } else {
        addIndex = swl_type_arraySize(at->type.elementType, inCollection, at->maxNrElements);
    }
    if((addIndex < 0) || ((size_t) addIndex >= at->maxNrElements)) {
        return -1;
    }

    swl_type_arrayPrepend(at->type.elementType, inCollection, at->maxNrElements, data, addIndex);
    return addIndex;
}
static swl_typeEl_t* s_col_alloc_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    ssize_t addIndex = -1;
    if(at->allowEmpty) {
        addIndex = swl_type_getFirstEmpty(at->type.elementType, inCollection, at->maxNrElements);
    } else {
        addIndex = swl_type_arraySize(at->type.elementType, inCollection, at->maxNrElements);
    }
    if((addIndex < 0) || ((size_t) addIndex >= at->maxNrElements)) {
        return NULL;
    }
    return inCollection + addIndex * at->type.elementType->size;
}

static swl_rc_ne s_col_insert_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(at->allowEmpty) {
        return swl_type_arrayPrepend(at->type.elementType, inCollection, at->maxNrElements, data, index);
    } else {
        size_t curSize = s_col_size_cb(colType, inCollection);
        size_t targetSize = SWL_MIN(curSize + 1, at->maxNrElements);
        size_t targetIndex = SWL_ARRAY_INDEX(index, targetSize);
        return swl_type_arrayPrepend(at->type.elementType, inCollection, targetSize, data, targetIndex);
    }
}

static swl_rc_ne s_col_set_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data, ssize_t index) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(at->allowEmpty) {
        return swl_type_arraySet(at->type.elementType, inCollection, at->maxNrElements, data, index);
    } else {
        size_t curSize = s_col_size_cb(colType, inCollection);
        return swl_type_arraySet(at->type.elementType, inCollection, curSize, data, index);
    }
}

/**
 * For array type, delete shall always try to remove a non-empty element. Other elements do not count
 * for an array that has a fixed maxSize.
 */
static swl_rc_ne s_col_delete_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    size_t size = s_col_size_cb(colType, inCollection);
    if(size == 0) {
        return SWL_RC_NOT_AVAILABLE;
    }
    size_t targetIndex = SWL_ARRAY_INDEX(index, size);

    swl_typeEl_t* target = inCollection + targetIndex * at->type.elementType->size;
    swl_type_cleanup(at->type.elementType, target);
    swl_type_arrayShift(at->type.elementType, target, at->maxNrElements - targetIndex, -1);
    return SWL_RC_OK;
}

static ssize_t s_col_find_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, swl_typeData_t* data) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    return swl_type_arrayFind(at->type.elementType, inCollection, at->maxNrElements, data);
}

static bool s_col_equals_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection1, swl_coll_t* inCollection2) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    return swl_type_arrayEquals(at->type.elementType, inCollection1, at->maxNrElements, inCollection2, at->maxNrElements);
}

static swl_typeData_t* s_col_getValue_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(at->allowEmpty) {
        return swl_type_arrayGetValue(at->type.elementType, inCollection, at->maxNrElements, index);
    } else {
        size_t curSize = s_col_size_cb(colType, inCollection);
        return swl_type_arrayGetValue(at->type.elementType, inCollection, curSize, index);
    }
}

static swl_typeEl_t* s_col_getReference_cb(swl_listSType_t* colType _UNUSED, swl_coll_t* inCollection, ssize_t index) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(at->allowEmpty) {
        return swl_type_arrayGetReference(at->type.elementType, inCollection, at->maxNrElements, index);
    } else {
        size_t curSize = s_col_size_cb(colType, inCollection);
        return swl_type_arrayGetReference(at->type.elementType, inCollection, curSize, index);
    }
}

static inline void s_col_setItData(swl_arrayType_t* at, swl_listSTypeIt_t* it) {
    if(it->index < at->maxNrElements) {
        it->data = it->coll + it->index * at->type.elementType->size;
        if(at->allowEmpty) {
            it->valid = true;
        } else {
            swl_typeData_t* td = SWL_TYPE_EL_TO_DATA(at->type.elementType, it->data);
            it->valid = !swl_type_isEmpty(at->type.elementType, td);
        }
    } else {
        it->valid = false;
    }
}

static swl_listSTypeIt_t s_col_getFirstIt_cb(swl_listSType_t* colType _UNUSED, const swl_coll_t* inCollection) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    swl_listSTypeIt_t data;
    memset(&data, 0, sizeof(swl_listSTypeIt_t));
    data.coll = (swl_coll_t*) inCollection;
    data.index = 0;
    data.dataType = at->type.elementType;
    s_col_setItData(at, &data);
    return data;
}
static void s_col_nextIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(it->valid || at->allowEmpty) {
        it->index++;
    }
    s_col_setItData(at, it);
}

static void s_col_delIt_cb(swl_listSType_t* colType _UNUSED, swl_listSTypeIt_t* it) {
    swl_arrayType_t* at = (swl_arrayType_t*) colType;
    if(it->valid) {
        if(at->allowEmpty) {
            swl_typeEl_t* target = it->coll + it->index * at->type.elementType->size;
            swl_type_cleanup(at->type.elementType, target);
        } else {
            s_col_delete_cb(colType, it->coll, it->index);
        }
        it->valid = false;
    }
}

swl_listSTypeFun_t swl_arrayCollectionType_fun = {
    .name = "swl_array",
    .init = s_col_init_cb,
    .initFromArray = s_col_initFromArray_cb,
    .clear = s_col_clear_cb,
    .cleanup = s_col_cleanup_cb,
    .maxSize = s_col_maxSize_cb,
    .size = s_col_size_cb,
    .add = s_col_add_cb,
    .alloc = s_col_alloc_cb,
    .insert = s_col_insert_cb,
    .set = s_col_set_cb,
    .delete = s_col_delete_cb,
    .find = s_col_find_cb,
    .equals = s_col_equals_cb,
    .getValue = s_col_getValue_cb,
    .getReference = s_col_getReference_cb,
    .firstIt = s_col_getFirstIt_cb,
    .nextIt = s_col_nextIt_cb,
    .delIt = s_col_delIt_cb,
};

static swl_typeExtIt_t extIt = {
    .id = SWL_TYPE_COL_FUN_UID,
    .extFun = &swl_arrayCollectionType_fun
};

SWL_CONSTRUCTOR static void s_initListExt(void) {
    swl_llist_append(&swl_arrayType_fun.extensions, &extIt.it);
}
