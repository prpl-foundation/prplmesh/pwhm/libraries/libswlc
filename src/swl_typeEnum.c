/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include "swl/swl_common_type.h"
#include "swl/swl_common_conversion.h"
#include "swl/swl_string.h"
#include "swl/swl_common.h"
#include "swl/swl_typeEnum.h"

#define ME "swlTEnm"

/**
 * Return the enum string matching the enum val. This is basically a safer version than just indexing the array.
 * If enumVal is outside the range of the enum, the string matching the default enumval
 * will be returned if it is within range of the enum, otherwise NULL will be returned.
 */
const char* swl_typeEnum_toChar(swl_typeEnum_t* type, uint32_t enumVal) {
    ASSERT_NOT_NULL(type, NULL, ME, "NULL");
    if(enumVal < type->maxEnumVal) {
        return type->enumStr[enumVal];
    } else if(type->defaultEnumVal < type->maxEnumVal) {
        return type->enumStr[type->defaultEnumVal];
    } else {
        return NULL;
    }
}

/**
 * Returns the enum value that matches the given srcStr. This is case sensitive
 * If there is no match, then the default enum value will be returned.
 */
uint32_t swl_typeEnum_fromChar(swl_typeEnum_t* type, const char* srcStr) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    return swl_conv_charToEnum(srcStr, type->enumStr, type->maxEnumVal, type->defaultEnumVal);
}

/**
 * Convert a char string to mask, based on given type.
 */
uint32_t swl_typeEnum_charToMask(swl_typeEnum_t* type, const char* srcStr) {
    ASSERT_NOT_NULL(type, 0, ME, "NULL");
    return swl_conv_charToMask(srcStr, type->enumStr, type->maxEnumVal);
}

/**
 * Convert a mask to a char string, based on a given type.
 */
bool swl_typeEnum_maskToChar(swl_typeEnum_t* type, char* tgtStr, uint32_t tgtStrSize, uint32_t srcMask) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    return swl_conv_maskToChar(tgtStr, tgtStrSize, srcMask, type->enumStr, type->maxEnumVal);
}

/**
 * Convert a mask to a char string, based on a given type, using given separator
 */
bool swl_typeEnum_maskToCharSep(swl_typeEnum_t* type, char* tgtStr, uint32_t tgtStrSize, uint32_t srcMask, char sep) {
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    return swl_conv_maskToCharSep(tgtStr, tgtStrSize, srcMask, type->enumStr, type->maxEnumVal, sep);
}


/**
 * enum handlers
 */
static ssize_t s_enum_toChar_cb(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const void* srcData) {
    ASSERTS_NOT_NULL(type, -EINVAL, ME, "NULL");
    ASSERTS_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcData, -EINVAL, ME, "NULL");

    uint32_t val = *(uint32_t*) srcData;
    swl_type_enum_t* enumType = (swl_type_enum_t*) type;

    ASSERT_TRUE(val < enumType->maxEnumVal, 0, ME, "Excess value %u / %zu", val, enumType->maxEnumVal);

    return snprintf(tgtStr, tgtStrSize, "%s", enumType->enumStr[val]);
}

static bool s_enum_fromChar_cb(swl_type_t* type, uint32_t* tgtData, const char* srcStr) {
    ASSERTS_NOT_NULL(tgtData, false, ME, "NULL");
    ASSERT_NOT_NULL(type, false, ME, "NULL");
    swl_type_enum_t* enumType = (swl_type_enum_t*) type;
    bool result = false;
    *tgtData = swl_conv_charToEnum_ext(srcStr, enumType->enumStr, enumType->maxEnumVal, enumType->defaultEnumVal, NULL, 0, &result);

    return result;
}

swl_typeFun_t swl_type_enum_fun = {
    .name = "swl_enum",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_enum_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_enum_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};
