/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common.h"
#include "swl/swl_crypto.h"

#define ME "swlCryp"

X509* swl_crypto_getCertFromFile(const char* certPath) {
    FILE* cert_file = fopen(certPath, "r");
    if(cert_file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open certificate file");
        return NULL;
    }
    X509* cert = PEM_read_X509_AUX(cert_file, NULL, NULL, NULL);
    if(cert == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to read certificate");
        fclose(cert_file);
        return NULL;
    }
    fclose(cert_file);
    return cert;
}

EVP_PKEY* swl_crypto_getPrivateKeyFromFile(const char* privKeyPath) {
    FILE* key_file = fopen(privKeyPath, "r");
    if(key_file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open private key file");
        return NULL;
    }
    EVP_PKEY* pkey = PEM_read_PrivateKey(key_file, NULL, NULL, NULL);
    if(pkey == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to read private key");
        fclose(key_file);
        return NULL;
    }
    fclose(key_file);
    return pkey;
}

swl_rc_ne s_publicEncodeWithCert(const unsigned char* input, unsigned char* output, size_t length,
                                 X509* cert, size_t* dataSize) {
    size_t rsaResult = SWL_RC_ERROR;

    EVP_PKEY* pubKey = swl_crypto_getRsaPubKeyFromCert(cert);
    if(pubKey == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not get RSA key from public key\n");
        return SWL_RC_ERROR;
    }

    EVP_PKEY_CTX* ctx = EVP_PKEY_CTX_new(pubKey, NULL);
    if(ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not create EVP_PKEY_CTX\n");
        return SWL_RC_ERROR;
    }

    if(EVP_PKEY_encrypt_init(ctx) <= 0) {
        SAH_TRACEZ_ERROR(ME, "EVP_PKEY_encrypt_init failed\n");
        goto err;
    }

    if(EVP_PKEY_CTX_set_rsa_padding(ctx, PADDING) <= 0) {
        SAH_TRACEZ_ERROR(ME, "EVP_PKEY_CTX_set_rsa_padding failed\n");
        goto err;
    }

    if(EVP_PKEY_encrypt(ctx, output, dataSize, input, length) <= 0) {
        SAH_TRACEZ_ERROR(ME, "Openssl error during private decoding: %s", ERR_error_string(ERR_get_error(), NULL));
        goto err;
    }

    rsaResult = SWL_RC_OK;
err:
    EVP_PKEY_CTX_free(ctx);
    EVP_PKEY_free(pubKey);

    return rsaResult;
}

swl_rc_ne s_privateDecodeWithCert(const unsigned char* input, unsigned char* output, size_t length, EVP_PKEY* pkey) {
    int rsaResult = SWL_RC_ERROR;
    EVP_PKEY_CTX* ctx = EVP_PKEY_CTX_new(pkey, NULL);
    if(ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not create EVP_PKEY_CTX\n");
        return rsaResult;
    }
    if(EVP_PKEY_decrypt_init(ctx) <= 0) {
        SAH_TRACEZ_ERROR(ME, "EVP_PKEY_decrypt_init failed\n");
        goto err;
    }

    if(EVP_PKEY_CTX_set_rsa_padding(ctx, PADDING) <= 0) {
        SAH_TRACEZ_ERROR(ME, "EVP_PKEY_CTX_set_rsa_padding failed\n");
        goto err;
    }

    if(EVP_PKEY_decrypt(ctx, output, &length, input, length) <= 0) {
        SAH_TRACEZ_ERROR(ME, "Openssl error during private decoding: %s", ERR_error_string(ERR_get_error(), NULL));
        goto err;
    }

    rsaResult = SWL_RC_OK;
err:
    EVP_PKEY_CTX_free(ctx);
    return rsaResult;
}

/**
 * Decode data using a private key path.
 */
swl_rc_ne swl_crypto_decodeDataWithPrivateKeyFile(const char* privKeyPath, const unsigned char* srcData, size_t dataSize, unsigned char* dataUnciphered) {
    EVP_PKEY* privKey = swl_crypto_getPrivateKeyFromFile(privKeyPath);
    ASSERTS_NOT_NULL(privKey, SWL_RC_ERROR, ME, "NULL\n");
    swl_rc_ne ret = swl_crypto_decodeDataWithPrivateKey(privKey, srcData, dataSize, dataUnciphered);
    EVP_PKEY_free(privKey);
    return ret;
}

/**
 * Decode data using a private key.
 */
swl_rc_ne swl_crypto_decodeDataWithPrivateKey(EVP_PKEY* privKey, const unsigned char* srcData, size_t dataSize, unsigned char* dataUnciphered) {
    ASSERTS_NOT_NULL(privKey, SWL_RC_ERROR, ME, "NULL\n");
    return s_privateDecodeWithCert(srcData, dataUnciphered, dataSize, privKey);
}

/**
 * Encode data using a certificate path.
 */
swl_rc_ne swl_crypto_encodeDataWithCertFile(const char* certPath, const unsigned char* srcData, size_t* dataSize, unsigned char* dataCiphered) {
    X509* cert = swl_crypto_getCertFromFile(certPath);
    ASSERTS_NOT_NULL(cert, SWL_RC_ERROR, ME, "NULL\n");
    swl_rc_ne ret = swl_crypto_encodeDataWithCert(cert, srcData, dataCiphered, dataSize);
    X509_free(cert);
    return ret;
}

/**
 * Encode data using a certificate.
 */
swl_rc_ne swl_crypto_encodeDataWithCert(X509* cert, const unsigned char* srcData, unsigned char* dataCiphered, size_t* dataSize) {
    ASSERTS_NOT_NULL(cert, SWL_RC_ERROR, ME, "NULL\n");
    return s_publicEncodeWithCert(srcData, dataCiphered, strlen((char*) srcData) + 1, cert, dataSize);
}

/**
 * Get public RSA key algorithm from certificate.
 */
EVP_PKEY* swl_crypto_getRsaPubKeyFromCert(X509* cert) {
    ASSERTS_NOT_NULL(cert, NULL, ME, "NULL\n");
    EVP_PKEY* pubKey = SWL_CRYPTO_X509_GET_PUBKEY(cert);
    if(SWL_CRYPTO_EVP_PKEY_BASE_ID(pubKey) != EVP_PKEY_RSA) {
        EVP_PKEY_free(pubKey);
        pubKey = NULL;
    }
    ASSERTS_NOT_NULL(pubKey, NULL, ME, "NULL\n");
    return pubKey;
}

/**
 * Return if cert is valid based on the CA certificate object provided.
 */
int swl_crypto_verifyCertWithRootCA(X509* cert, X509* rootCA) {
    ASSERTS_NOT_NULL(cert, false, ME, "NULL\n");
    ASSERTS_NOT_NULL(rootCA, false, ME, "NULL\n");
    X509_STORE* store = X509_STORE_new();
    X509_STORE_add_cert(store, rootCA);
    X509_STORE_CTX* ctx = X509_STORE_CTX_new();
    X509_STORE_CTX_init(ctx, store, cert, NULL);
    swl_rc_ne result = X509_verify_cert(ctx);
    X509_STORE_CTX_free(ctx);
    X509_STORE_free(store);
    return result;
}

/**
 * Return if cert is valid based on the CA certificate file provided.
 */
int swl_crypto_verifyCertWithRootCAFile(const char* certPath, const char* rootCAPath) {
    FILE* root_cert_file = fopen(rootCAPath, "r");
    if(root_cert_file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open cert rootCA file");
        return SWL_RC_ERROR;
    }

    FILE* server_cert_file = fopen(certPath, "r");
    if(server_cert_file == NULL) {
        fclose(root_cert_file);
        SAH_TRACEZ_ERROR(ME, "Failed to open cert file");
        return SWL_RC_ERROR;
    }

    X509* root_cert = PEM_read_X509(root_cert_file, NULL, 0, NULL);
    X509* server_cert = PEM_read_X509(server_cert_file, NULL, 0, NULL);
    int result = swl_crypto_verifyCertWithRootCA(server_cert, root_cert);

    X509_free(root_cert);
    X509_free(server_cert);
    fclose(root_cert_file);
    fclose(server_cert_file);
    return result;
}
