/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include "swl/swl_common_time_spec.h"
#include "swl/swl_common_time.h"
#include "swl/swl_common.h"
#include "swl/swl_datetime.h"
#include "swl/swl_math.h"
#define ME "swlTimeSpec"

/**
 * The real time of the 0 monotonic base tmie.
 */
static swl_timeSpecReal_t s_realOfMonoBaseTime = {.tv_sec = 0, .tv_nsec = 0};

/**
 * Add seconds and nanoseconds. Negative values are allowed and will properly overflow.
 */
void swl_timespec_addTime(struct timespec* ts, int64_t seconds, int64_t nanoseconds) {
    ts->tv_sec += seconds;
    while(nanoseconds < (-1 * ts->tv_nsec)) {
        ts->tv_sec--;
        nanoseconds += SWL_TIMESPEC_NANO_PER_SEC;
    }
    ts->tv_nsec += nanoseconds;
    if(ts->tv_nsec >= SWL_TIMESPEC_NANO_PER_SEC) {
        ts->tv_sec += ts->tv_nsec / SWL_TIMESPEC_NANO_PER_SEC;
        ts->tv_nsec = ts->tv_nsec % SWL_TIMESPEC_NANO_PER_SEC;
    }
}

/**
 * Adds two timespecs together and put result in result.
 */
void swl_timespec_add(struct timespec* result, const struct timespec* spec1, const struct timespec* spec2) {
    memcpy(result, spec1, sizeof(struct timespec));
    swl_timespec_addTime(result, spec2->tv_sec, spec2->tv_nsec);
}

static void s_checkBaseTime() {
    struct timespec tsReal;
    struct timespec tsMono;

    clock_gettime(CLOCK_REALTIME, &tsReal);
    clock_gettime(CLOCK_MONOTONIC, &tsMono);

    swl_timeSpecReal_t tmpBase = {.tv_sec = 0, .tv_nsec = 0};
    swl_timespec_diff(&tmpBase, &tsMono, &tsReal);

    if(s_realOfMonoBaseTime.tv_sec != 0) {
        // Check that real time base has not significantly shifted, i.e. more than half a second
        int64_t diff = SWL_MATH_ABS(swl_timespec_diffToMillisec(&s_realOfMonoBaseTime, &tmpBase));
        if(diff < 500) {
            return;
        }
    }

    memcpy(&s_realOfMonoBaseTime, &tmpBase, sizeof(swl_timeSpecReal_t));
}


/**
 * Return the real time of the zero point of monotonic time.
 */
void swl_timespec_getRealOfMonoBaseTime(swl_timeSpecReal_t* base) {
    s_checkBaseTime();

    memcpy(base, &s_realOfMonoBaseTime, sizeof(swl_timeSpecReal_t));
    return;
}

swl_timeSpecReal_t swl_timespec_getRealOfMonoBaseTimeVal() {
    s_checkBaseTime();
    return s_realOfMonoBaseTime;
}

/**
 * Returns the current monotonic spec time.
 */
void swl_timespec_getMono(swl_timeSpecMono_t* ts) {
    ASSERT_NOT_NULL(ts, , ME, "NULL");
    clock_gettime(CLOCK_MONOTONIC, ts);
}

/**
 * Returns as value the current monotonic time
 */
swl_timeSpecMono_t swl_timespec_getMonoVal() {
    swl_timeSpecMono_t ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts;
}

/**
 * Returns the elapsed time spec since epoch.
 */
void swl_timespec_getReal(swl_timeSpecReal_t* ts) {
    ASSERT_NOT_NULL(ts, , ME, "NULL");
    clock_gettime(CLOCK_REALTIME, ts);
}

/**
 * Returns as value the current elapsed time spec since epoch
 */
swl_timeSpecReal_t swl_timespec_getRealVal() {
    swl_timeSpecReal_t ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts;
}

/**
 * Converts monotonic to realtime.
 * Zero monotonic time will return zero real time.
 */
void swl_timespec_monoToReal(swl_timeSpecReal_t* tgtTime, const swl_timeSpecMono_t* srcTime) {
    if(swl_timespec_isZero(srcTime)) {
        memset(tgtTime, 0, sizeof(swl_timeSpecReal_t));
        return;
    }
    swl_timeSpecReal_t baseTime;
    swl_timespec_getRealOfMonoBaseTime(&baseTime);
    swl_timespec_add(tgtTime, srcTime, &baseTime);
}

/**
 * Converts monotonic time to realtime.
 * Zero real time will return zero monotonic time.
 * If the input real time is before or equal to the start of monotonic time,
 * monotonic time of 1 nanosec will be returned.
 */
void swl_timespec_realToMono(swl_timeSpecMono_t* tgtTime, swl_timeSpecReal_t* srcTime) {
    if(swl_timespec_isZero(srcTime)) {
        memset(tgtTime, 0, sizeof(swl_timeSpecMono_t));
        return;
    }

    swl_timeSpecReal_t baseMono;
    swl_timespec_getRealOfMonoBaseTime(&baseMono);
    int diff = swl_timespec_diff(tgtTime, &baseMono, srcTime);
    if(diff != 1) {
        tgtTime->tv_sec = 0;
        tgtTime->tv_nsec = 1;
    }
    return;
}


/**
 * Reset time spec
 */
void swl_timespec_reset(struct timespec* ts) {
    ASSERT_NOT_NULL(ts, , ME, "NULL");
    ts->tv_sec = 0;
    ts->tv_nsec = 0;
}

/**
 * Get total time spec in nano seconds
 */
int64_t swl_timespec_getTimeInNanosec(const struct timespec* ts) {
    ASSERT_NOT_NULL(ts, 0, ME, "NULL");
    if(ts->tv_sec >= LONG_MAX / SWL_TIMESPEC_NANO_PER_SEC) {
        return LONG_MAX;
    }
    if(ts->tv_sec < LONG_MIN / SWL_TIMESPEC_NANO_PER_SEC) {
        return LONG_MIN;
    }

    return ((int64_t) ts->tv_sec * SWL_TIMESPEC_NANO_PER_SEC + ts->tv_nsec);
}

/**
 * Get total time in micro seconds
 */
int64_t swl_timespec_getTimeInMicrosec(const struct timespec* ts) {
    ASSERT_NOT_NULL(ts, 0, ME, "NULL");
    if(ts->tv_sec >= LONG_MAX / SWL_TIMESPEC_MICRO_PER_SEC) {
        return LONG_MAX;
    }
    if(ts->tv_sec < LONG_MIN / SWL_TIMESPEC_MICRO_PER_SEC) {
        return LONG_MIN;
    }

    return ((int64_t) ts->tv_sec * SWL_TIMESPEC_MICRO_PER_SEC + ts->tv_nsec / SWL_TIMESPEC_NANO_PER_MICRO);
}

/**
 * Get total time in micro seconds
 */
int64_t swl_timespec_getTimeInMillisec(const struct timespec* ts) {
    ASSERT_NOT_NULL(ts, 0, ME, "NULL");
    if(ts->tv_sec >= LONG_MAX / SWL_TIMESPEC_MILLI_PER_SEC) {
        return LONG_MAX;
    }
    if(ts->tv_sec < LONG_MIN / SWL_TIMESPEC_MILLI_PER_SEC) {
        return LONG_MIN;
    }

    return ((int64_t) ts->tv_sec * SWL_TIMESPEC_MILLI_PER_SEC + ts->tv_nsec / SWL_TIMESPEC_NANO_PER_MILLI);
}

/**
 * Return difference between time specs (absolute value)
 * @return int   0 : if time equals
 *                   if start time is NULL, and stop time is NULL
 *               1 : if start time < stop time
 *                   if start time is NULL, and stop time is != NULL
 *              -1 : if start time > stop time
 *                   if start time is != NULL, and stop time is NULL
 */
int swl_timespec_diff(struct timespec* result, const struct timespec* start, const struct timespec* stop) {
    if((start == NULL) && (stop == NULL)) {
        if(result != NULL) {
            result->tv_sec = 0;
            result->tv_nsec = 0;
        }
        return 0;
    }
    if((start == NULL) && (stop != NULL)) {
        if(result != NULL) {
            result->tv_sec = LONG_MAX;
            result->tv_nsec = ULONG_MAX;
        }
        return 1;
    }
    if((start != NULL) && (stop == NULL)) {

        if(result != NULL) {
            result->tv_sec = LONG_MIN;
            result->tv_nsec = 0;
        }
        return -1;
    }

    int64_t tvSec;
    int64_t tvNSec;
    int retval = 0;

    tvSec = stop->tv_sec - start->tv_sec;
    tvNSec = stop->tv_nsec - start->tv_nsec;

    if(tvSec > 0) {
        retval = 1;
        if(tvNSec < 0) {
            tvSec -= 1;
            tvNSec += 1000000000;
        }
    } else if(tvSec < 0) {
        retval = -1;
        if(tvNSec > 0) {
            tvSec += 1;
            tvNSec -= 1000000000;
        }
    } else {
        //(tvSec == 0)
        if(tvNSec > 0) {
            retval = 1;
        } else if(tvNSec < 0) {
            retval = -1;
        }
    }
    if(result != NULL) {
        result->tv_sec = tvSec;
        result->tv_nsec = llabs(tvNSec);
    }

    return retval;
}

/**
 * Return seconds difference between time specs
 */
int64_t swl_timespec_diffToSec(struct timespec* start, struct timespec* stop) {
    struct timespec result;
    swl_timespec_diff(&result, start, stop);
    return result.tv_sec;
}

/**
 * Return milli seconds difference between time specs
 */
int64_t swl_timespec_diffToMillisec(struct timespec* start, struct timespec* stop) {
    struct timespec result;
    swl_timespec_diff(&result, start, stop);
    return swl_timespec_getTimeInMillisec(&result);
}

/**
 * Return milli seconds difference between time specs
 */
int64_t swl_timespec_diffToMicrosec(struct timespec* start, struct timespec* stop) {
    struct timespec result;
    swl_timespec_diff(&result, start, stop);
    return swl_timespec_getTimeInMicrosec(&result);
}

/**
 * Return nanoseconds difference between time specs
 */
int64_t swl_timespec_diffToNanosec(struct timespec* start, struct timespec* stop) {
    struct timespec result;
    swl_timespec_diff(&result, start, stop);
    return swl_timespec_getTimeInNanosec(&result);
}

/**
 * Check if time specs are equals
 */
bool swl_timespec_equals(struct timespec* firstTs, struct timespec* secondTs) {
    if((firstTs == NULL) || (secondTs == NULL)) {
        return firstTs == NULL && secondTs == NULL;
    }
    return !memcmp(firstTs, secondTs, sizeof(struct timespec));
}

/**
 * Check if time spec is equal to 0
 */
bool swl_timespec_isZero(const struct timespec* ts) {
    ASSERT_NOT_NULL(ts, false, ME, "NULL");
    return ts->tv_nsec == 0 && ts->tv_sec == 0;
}

/**
 * prints a realtime to UTC timestamp according to SWL_DATETIME_ISO8601_DATE_TIME_FMT_US format
 * e.g. "2021-06-09T09:15:18.342513Z"
 */
ssize_t swl_timespec_realToDate(char* tgtStr, size_t tgtStrSize, const swl_timeSpecReal_t* srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcTime, -EINVAL, ME, "NULL");
    ASSERTS_NOT_EQUALS(tgtStrSize, 0, SWL_TIMESPEC_STR_SIZE - 1, ME, "NULL");

    if(swl_timespec_isZero(srcTime)) {
        return snprintf(tgtStr, tgtStrSize, SWL_NULLTIME_NANO_STR);
    }

    swl_datetime_t dt;
    swl_timespec_realToDatetime(&dt, srcTime);

    swl_datetime_strftime(tgtStr, tgtStrSize, SWL_DATETIME_ISO8601_FMT_US, &dt);
    return SWL_TIMESPEC_STR_SIZE - 1;
}

/**
 * prints a monotime to UTC timestamp according to SWL_DATETIME_ISO8601_DATE_TIME_FMT_US format
 * e.g. "2021-06-09T09:15:18.342513Z"
 */
ssize_t swl_timespec_monoToDate(char* tgtStr, size_t tgtStrSize, const swl_timeSpecMono_t* srcTime) {
    ASSERT_NOT_NULL(tgtStr, -EINVAL, ME, "NULL");
    ASSERT_NOT_NULL(srcTime, -EINVAL, ME, "NULL");
    ASSERTS_NOT_EQUALS(tgtStrSize, 0, SWL_TIMESPEC_STR_SIZE - 1, ME, "NULL");

    if(swl_timespec_isZero(srcTime)) {
        return snprintf(tgtStr, tgtStrSize, SWL_NULLTIME_NANO_STR);
    }
    swl_timeSpecReal_t realTime;
    swl_timespec_monoToReal(&realTime, srcTime);

    return swl_timespec_realToDate(tgtStr, tgtStrSize, &realTime);
}

/**
 * Converts a swl dateTime to monotonic timespec
 */
void swl_timespec_datetimeToReal(swl_timeSpecReal_t* mySpec, swl_datetime_t* dateTime) {
    if(!memcmp(&dateTime->datetime, &swl_time_nullTimeTm, sizeof(struct tm))
       && ( dateTime->nanoseconds == 0)) {
        swl_timespec_reset(mySpec);
        return;
    }
    mySpec->tv_sec = swl_datetime_time(dateTime);
    mySpec->tv_nsec = swl_datetime_nanoseconds(dateTime);
}

/**
 * Converts a swl dateTime to realtimespec
 */
void swl_timespec_realToDatetime(swl_datetime_t* dt, const swl_timeSpecReal_t* srcTime) {
    if(swl_timespec_isZero(srcTime)) {
        swl_datetime_initialize(dt, SWL_DATETIME_EPOCH_TR098);
        swl_datetime_fromTm(dt, &swl_time_nullTimeTm);
        return;
    }

    swl_datetime_initialize(dt, SWL_DATETIME_EPOCH_TR098);
    swl_datetime_fromTimespec(dt, srcTime);
}

/**
 * Converts a monotonic timespec to swl datetime
 */
void swl_timespec_monoToDatetime(swl_datetime_t* dt, const swl_timeSpecMono_t* srcTime) {
    if(swl_timespec_isZero(srcTime)) {
        swl_datetime_initialize(dt, SWL_DATETIME_EPOCH_TR098);
        swl_datetime_fromTm(dt, &swl_time_nullTimeTm);
        return;
    }

    swl_timeSpecReal_t realTime;
    swl_timespec_monoToReal(&realTime, srcTime);
    swl_timespec_realToDatetime(dt, &realTime);
}

/**
 * Converts a swl datetime to monotonic time
 */
void swl_timespec_datetimeToMono(swl_timeSpecMono_t* tgtTime, swl_datetime_t* dateTime) {
    swl_timeSpecReal_t curTime;
    swl_timespec_datetimeToReal(&curTime, dateTime);
    return swl_timespec_realToMono(tgtTime, &curTime);
}

/**
 * Type implementation realtimespec
 */
static ssize_t s_timespecReal_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_timeSpecReal_t* srcData) {
    return swl_timespec_realToDate(tgtStr, tgtStrSize, srcData);
}

static bool s_timespecReal_fromChar_cb(swl_type_t* type _UNUSED, swl_timeSpecReal_t* tgtData, const char* srcStr) {
    swl_datetime_t dateTime;
    swl_datetime_initialize(&dateTime, SWL_DATETIME_EPOCH_TR098);
    char* result = swl_datetime_strptime(srcStr, SWL_DATETIME_ISO8601_FMT_US, &dateTime);
    if(result == NULL) {
        return false;
    }
    swl_timespec_datetimeToReal(tgtData, &dateTime);
    return true;
}


swl_typeFun_t swl_type_timeSpecReal_fun = {
    .name = "swl_timeSpecReal",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_timespecReal_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_timespecReal_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};

swl_type_t gtSwl_type_timeSpecReal = {
    .typeFun = &swl_type_timeSpecReal_fun,
    .size = sizeof(swl_timeSpecReal_t),
};

SWL_REF_TYPE_C(gtSwl_type_timeSpecRealPtr, gtSwl_type_timeSpecReal);


/**
 * Type implementation monotonic timespec
 */

static ssize_t s_timespecMono_toChar_cb(swl_type_t* type _UNUSED, char* tgtStr, size_t tgtStrSize, const swl_timeSpecMono_t* srcData) {
    return swl_timespec_monoToDate(tgtStr, tgtStrSize, srcData);
}

static bool s_timespecMono_fromChar_cb(swl_type_t* type _UNUSED, swl_timeSpecMono_t* tgtData, const char* srcStr) {
    swl_datetime_t dateTime;
    swl_datetime_initialize(&dateTime, SWL_DATETIME_EPOCH_TR098);
    char* result = swl_datetime_strptime(srcStr, SWL_DATETIME_ISO8601_FMT_US, &dateTime);
    if(result == NULL) {
        return false;
    }
    swl_timespec_datetimeToMono(tgtData, &dateTime);
    return true;
}

swl_typeFun_t swl_type_timeSpecMono_fun = {
    .name = "swl_timeSpecMono",
    .isValue = true,
    .toChar = (swl_type_toChar_cb) s_timespecMono_toChar_cb,
    .fromChar = (swl_type_fromChar_cb) s_timespecMono_fromChar_cb,
    .equals = NULL,
    .copy = NULL,
};

swl_type_t gtSwl_type_timeSpecMono = {
    .typeFun = &swl_type_timeSpecMono_fun,
    .size = sizeof(swl_timeSpecMono_t),
};

SWL_REF_TYPE_C(gtSwl_type_timeSpecMonoPtr, gtSwl_type_timeSpecMono);

