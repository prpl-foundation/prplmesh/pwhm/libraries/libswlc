/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "swl/swl_common_mem.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define ME "swlMem"

#define SWL_MEM_MAGIC 0x20230925

#define SWL_MEM_PATH_LEN 16

/** Defines a monitored memory zone */
static struct {
    swl_llist_t llMems;         /*!< List of memory */
    size_t maxSize;             /*!< Maximum size of allocated memories */
    size_t curSize;             /*!< Current size allocated */
    swl_mem_threshold_h th;     /*!< Threshold reached handler */
} s_self;

/** Defines a reference-counting memory object */
typedef struct {
    uint32_t magic;             /*!< Magic number */
    swl_llist_iterator_t entry; /*!< Entry in the list */
    swl_memZone_t* zone;        /*!< Zone where memory belong (can be null) */
    uint32_t nRefs;             /*!< Number of references */
    size_t size;                /*!< Size of memory pointed by @data */
    swl_mem_destroy_h dh;       /*!< Destroy handler */
    uint8_t data[];             /*!< Data zone */
} swl_mem_t;

/** get swl_mem_t header from pointer */
static swl_mem_t* s_getMem(void* ptr) {
    swl_mem_t* mem = (swl_mem_t*) (((uint8_t*) ptr) - offsetof(swl_mem_t, data));
    ASSERT_EQUALS(mem->magic, SWL_MEM_MAGIC, NULL, ME, "not valid mem");
    return mem;
}

/** check threshold and print warning */
static void s_checkThreshold() {
    if((s_self.maxSize != 0) && (s_self.curSize > s_self.maxSize)) {
        SAH_TRACEZ_WARNING(ME, "max memory exceeded %zu/%zu",
                           s_self.curSize,
                           s_self.maxSize);
        /* increment maxSize by 50% */
        s_self.maxSize += (s_self.maxSize >> 1);
        /* trigger handler */
        SWL_CALL(s_self.th, s_self.curSize);
    }
}

/** add a new swl_mem into a memZone */
static void s_addZone(swl_memZone_t* zone, swl_mem_t* mem) {
    mem->zone = zone;
    ASSERTS_NOT_NULL(zone, , ME, "no zone");
    zone->nrAllocs++;
    zone->size += mem->size;
}

/** remove the memory object from associated zone */
static void s_removeZone(swl_mem_t* mem) {
    swl_memZone_t* zone = mem->zone;
    ASSERTS_NOT_NULL(zone, , ME, "no zone");
    ASSERT_FALSE(zone->size < mem->size, , ME, "small zone(%s) size %zu/%zu",
                 zone->name, zone->size, mem->size);
    zone->nrAllocs--;
    zone->size -= mem->size;
    mem->zone = NULL;
}

swl_rc_ne swl_mem_setThreshold(size_t max) {
    s_self.maxSize = max;
    return SWL_RC_OK;
}

swl_rc_ne swl_mem_setThresholdHandler(swl_mem_threshold_h th) {
    s_self.th = th;
    return SWL_RC_OK;
}

void* swl_mem_alloc(swl_memZone_t* zone, size_t size, swl_mem_destroy_h dh) {
    swl_mem_t* mem = malloc(size + sizeof(swl_mem_t));
    ASSERT_NOT_NULL(mem, NULL, ME, "allocation failed");
    memset(mem, 0, sizeof(swl_mem_t));
    mem->dh = dh;
    mem->size = size;
    mem->nRefs++;
    mem->magic = SWL_MEM_MAGIC;
    swl_llist_append(&s_self.llMems, &mem->entry);
    s_self.curSize += mem->size;
    s_addZone(zone, mem);
    s_checkThreshold();
    return ((void*) mem->data);
}

void* swl_mem_zalloc(swl_memZone_t* zone, size_t size, swl_mem_destroy_h dh) {
    void* p = swl_mem_alloc(zone, size, dh);
    ASSERTS_NOT_NULL(p, NULL, ME, "NULL");
    memset(p, 0, size);
    return p;
}

char* swl_mem_dupStr(swl_memZone_t* zone, const char* str) {
    ASSERTS_NOT_NULL(str, NULL, ME, "NULL");
    size_t size = strlen(str) + 1;
    void* p = swl_mem_zalloc(zone, size, NULL);
    ASSERTS_NOT_NULL(p, NULL, ME, "NULL");
    memcpy(p, str, size);
    return p;
}

void* swl_mem_dup(void* ptr) {
    ASSERTS_NOT_NULL(ptr, NULL, ME, "NULL");
    swl_mem_t* mem = s_getMem(ptr);
    ASSERTS_NOT_NULL(mem, NULL, ME, "NULL");
    void* p = swl_mem_alloc(mem->zone, mem->size, mem->dh);
    ASSERTS_NOT_NULL(p, NULL, ME, "NULL");
    memcpy(p, ptr, mem->size);
    return p;
}

void* swl_mem_ref(void* ptr) {
    ASSERTS_NOT_NULL(ptr, NULL, ME, "NULL");
    swl_mem_t* mem = s_getMem(ptr);
    ASSERTS_NOT_NULL(mem, NULL, ME, "NULL");
    mem->nRefs++;
    return ptr;
}

void swl_mem_unref(void** pptr) {
    ASSERTS_NOT_NULL(pptr, , ME, "NULL");
    void* ptr = *pptr;
    swl_mem_t* mem = s_getMem(ptr);
    ASSERTS_NOT_NULL(mem, , ME, "NULL");
    *pptr = NULL;
    ASSERTS_TRUE(mem->nRefs > 0, , ME, "no reference");
    mem->nRefs--;
    ASSERTS_FALSE(mem->nRefs > 0, , ME, "still referenced");
    SWL_CALL(mem->dh, ptr);
    ASSERTS_FALSE(mem->nRefs > 0, , ME, "referenced again");
    swl_llist_iterator_take(&mem->entry);
    s_self.curSize -= mem->size;
    s_removeZone(mem);
    memset(mem, 0, mem->size + sizeof(swl_mem_t));
    free(mem);
}

uint32_t swl_mem_nref(void* ptr) {
    ASSERTS_NOT_NULL(ptr, 0, ME, "NULL");
    swl_mem_t* mem = s_getMem(ptr);
    ASSERTS_NOT_NULL(mem, 0, ME, "NULL");
    return mem->nRefs;
}

size_t swl_mem_size(void* ptr) {
    ASSERTS_NOT_NULL(ptr, 0, ME, "NULL");
    swl_mem_t* mem = s_getMem(ptr);
    ASSERTS_NOT_NULL(mem, 0, ME, "NULL");
    return mem->size;
}

size_t swl_mem_total() {
    return s_self.curSize;
}

void swl_mem_log() {
    swl_llist_iterator_t* it = NULL;
    SAH_TRACEZ_WARNING(ME, "mem / zone / size / refs");
    swl_llist_for_each(it, &s_self.llMems) {
        swl_mem_t* mem = swl_llist_iterator_data(it, swl_mem_t, entry);
        SAH_TRACEZ_WARNING(ME, "%p / %s / %zu / %u",
                           mem->data,
                           (mem->zone != NULL) ? mem->zone->name : "none",
                           mem->size,
                           mem->nRefs);
    }
    SAH_TRACEZ_WARNING(ME, "total: %zu", s_self.curSize);
}

static SWL_CONSTRUCTOR void s_mem_init() {
    memset(&s_self, 0, sizeof(s_self));
    swl_llist_initialize(&s_self.llMems);
}

static SWL_DESTRUCTOR void s_mem_deinit() {
    ASSERTS_FALSE(swl_llist_isEmpty(&s_self.llMems), , ME, "no left memory");
    swl_mem_log();
}
