/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_TTB_SWL_TTB_H_
#define SRC_INCLUDE_SWL_TTB_SWL_TTB_H_


/**
 * @file
 * @brief
 * The SWL test toolbox extensions.
 * Only to be used if test toolbox is present.
 */


#include "test-toolbox/ttb.h"
#include "swl/swl_common_type.h"
#include "swl/swl_string.h"
#include "swl/swl_returnCode.h"
#include "swl/fileOps/swl_fileUtils.h"

/**
 * @brief Checks if the output of command is SWL_RC_OK, fail otherwise
 */
#define swl_ttb_assertRcOk(cmd) \
    { \
        swl_rc_ne retCode = cmd; \
        ttb_assert_msg(retCode == SWL_RC_OK, "Success expected, but got %i: '%s'", retCode, swl_rc_toString(retCode)); \
    }

#define swl_ttb_assertRcEq(rc, cmd) \
    { \
        swl_rc_ne retCode0 = rc; \
        swl_rc_ne retCode1 = cmd; \
        ttb_assert_msg(retCode0 == retCode1, "Expected result %i: '%s', but got %i: '%s'", \
                       retCode0, swl_rc_toString(retCode0), \
                       retCode1, swl_rc_toString(retCode1)); \
    }

#define swl_ttb_assertTypeEquals(type, val1, val2) \
    ttb_assert_msg(swl_type_equals(type, val1, val2), "Type Equality expected, but got: '%s' != '%s'", \
                   swl_type_toBuf32(type, val1).buf, swl_type_toBuf32(type, val2).buf)

#define swl_ttb_assertTypeNotEquals(type, val1, val2) \
    ttb_assert_msg(!swl_type_equals(type, val1, val2), "Type Inequality expected, but got: '%s' = '%s'", \
                   swl_type_toBuf32(type, val1).buf, swl_type_toBuf32(type, val2).buf)

#define swl_ttb_assertTypeElEquals(type, val1, val2) \
    { \
        swl_typeData_t* typeVal1 = SWL_TYPE_EL_TO_DATA(type, val1); \
        swl_typeData_t* typeVal2 = SWL_TYPE_EL_TO_DATA(type, val2); \
        ttb_assert_msg(swl_type_equals(type, typeVal1, typeVal2), "Type Equality expected, but got: '%s' != '%s'", \
                       swl_type_toBuf32(type, typeVal1).buf, swl_type_toBuf32(type, typeVal2).buf); \
    }

#define swl_ttb_assertTypeElNotEquals(type, val1, val2) \
    { \
        swl_typeData_t* typeVal1 = SWL_TYPE_EL_TO_DATA(type, val1); \
        swl_typeData_t* typeVal2 = SWL_TYPE_EL_TO_DATA(type, val2); \
        ttb_assert_msg(!swl_type_equals(type, typeVal1, typeVal2), "Type Inequality expected, but got: '%s' = '%s'", \
                       swl_type_toBuf32(type, typeVal1).buf, swl_type_toBuf32(type, typeVal2).buf); \
    }

#define swl_ttb_assertTypeValEquals(type, val1, val2) \
    ttb_assert_msg(swl_type_equals(type, &val1, &val2), "Type Equality expected, but got: '%s' != '%s'", \
                   swl_type_toBuf32(type, &val1).buf, swl_type_toBuf32(type, &val2).buf)

#define swl_ttb_assertTypeValNotEquals(type, val1, val2) \
    ttb_assert_msg(!swl_type_equals(type, &val1, &val2), "Type Inequality expected, but got: '%s' = '%s'", \
                   swl_type_toBuf32(type, &val1).buf, swl_type_toBuf32(type, &val2).buf)

#define swl_ttb_assertBigTypeEquals(type, val1, val2) \
    {   char* str1 = swl_type_toCString(type, val1); \
        char* str2 = swl_type_toCString(type, val2); \
        ttb_assert_msg(swl_type_equals(type, val1, val2), "Type Equality expected, but got: \n * '%s' \n * '%s'\n", \
                       str1, str2); \
        free(str1); \
        free(str2); \
    }

#define swl_ttb_assertBigTypeNotEquals(type, val1, val2) \
    {   char* str1 = swl_type_toCString(type, val1); \
        ttb_assert_msg(!swl_type_equals(type, val1, val2), "Type Inequality expected, but got: \n * '%s' \n", \
                       str1); \
        free(str1); \
    }

#define swl_ttb_assertBigTypeValEquals(type, val1, val2) \
    {   char* str1 = swl_type_toCString(type, &val1); \
        char* str2 = swl_type_toCString(type, &val2); \
        ttb_assert_msg(swl_type_equals(type, &val1, &val2), "Type Equality expected, but got: \n * '%s' \n * '%s'\n", \
                       str1, str2); \
        free(str1); \
        free(str2); \
    }

#define swl_ttb_assertBigTypeValNotEquals(type, val1, val2) \
    {   char* str1 = swl_type_toCString(type, &val1); \
        ttb_assert_msg(!swl_type_equals(type, &val1, &val2), "Type Inequality expected, but got: \n * '%s' \n", \
                       str1); \
        free(str1); \
    }

#define swl_ttb_assertTypeArrayEquals(type, val1, val1Size, val2, val2Size) \
    ttb_assert_msg(swl_type_arrayEquals(type, val1, val1Size, val2, val2Size), \
                   "Type array equality expected, but got: \n ** '%s' \n ** '%s'", \
                   swl_type_arrayToBuf128(type, val1, val1Size).buf, swl_type_arrayToBuf128(type, val2, val2Size).buf)

#define swl_ttb_assertTypeArrayNotEquals(type, val1, val1Size, val2, val2Size) \
    ttb_assert_msg(!swl_type_arrayEquals(type, val1, val1Size, val2, val2Size), \
                   "Type array inequality expected, but got: \n ** '%s' \n ** '%s'", \
                   swl_type_arrayToBuf128(type, val1, val1Size).buf, swl_type_arrayToBuf128(type, val2, val2Size).buf)

#define swl_ttb_assertBigTypeArrayEquals(type, val1, val1Size, val2, val2Size) \
    {char* str1 = swl_type_arrayToCString(type, val1, val1Size); \
        char* str2 = swl_type_arrayToCString(type, val2, val2Size); \
        ttb_assert_msg(swl_type_arrayEquals(type, val1, val1Size, val2, val2Size), \
                       "Type array equality expected, but got: \n ** '%s' \n ** '%s'", \
                       str1, str2); \
        free(str1); \
        free(str2); \
    }

#define swl_ttb_assertBigTypeArrayNotEquals(type, val1, val1Size, val2, val2Size) \
    {char* str1 = swl_type_arrayToCString(type, val1, val1Size); \
        ttb_assert_msg(!swl_type_arrayEquals(type, val1, val1Size, val2, val2Size), \
                       "Type array inequality expected, but got: \n ** '%s' ", \
                       str1); \
        free(str1); \
    }


#define swl_ttb_assertTypeEmpty(type, val1) \
    ttb_assert_msg(swl_type_isEmpty(type, val1), "Type Empty expected, but got: '%s'", \
                   swl_type_toBuf32(type, val1).buf)

#define swl_ttb_assertTypeNotEmpty(type, val1) \
    ttb_assert_msg(!swl_type_isEmpty(type, val1), "Type Not Empty expected, but got: '%s'", \
                   swl_type_toBuf32(type, val1).buf)

#define swl_ttb_assertTypeValEmpty(type, val1) \
    ttb_assert_msg(swl_type_isEmpty(type, &val1), "Type Empty expected, but got: '%s'", \
                   swl_type_toBuf32(type, &val1).buf)

#define swl_ttb_assertTypeValNotEmpty(type, val1) \
    ttb_assert_msg(!swl_type_isEmpty(type, &val1), "Type Not Empty expected, but got: '%s'", \
                   swl_type_toBuf32(type, &val1).buf)

#define swl_ttb_assertTypeBuf32StrMatches(type, val, str) \
    ttb_assert_msg(swl_str_matches(swl_type_toBuf32(type, val).buf, str), "Type Buf32 Match expected, but got: '%s' != '%s'", \
                   swl_type_toBuf32(type, val).buf, str)

#define swl_ttb_assertTypeBuf32StrNotMatches(type, val, str) \
    ttb_assert_msg(!swl_str_matches(swl_type_toBuf32(type, val).buf, str), "Type Buf32 Not Match expected, but got: '%s' = '%s'", \
                   swl_type_toBuf32(type, val).buf, str)

#define swl_ttb_assertTypeValBuf32StrMatches(type, val, str) \
    ttb_assert_msg(swl_str_matches(swl_type_toBuf32(type, &val).buf, str), "Type Buf32 Match expected, but got: '%s' != '%s'", \
                   swl_type_toBuf32(type, &val).buf, str)

#define swl_ttb_assertTypeValBuf32StrNotMatches(type, val, str) \
    ttb_assert_msg(!swl_str_matches(swl_type_toBuf32(type, &val).buf, str), "Type Buf32 Not Match expected, but got: '%s' = '%s'", \
                   swl_type_toBuf32(type, &val).buf, str)

#define swl_ttb_assertEnumEquals(val1, val2, strList) \
    ttb_assert_msg(val1 == val2, "Enum equality expected, but got '%s' != '%s'", strList[val1], strList[val2]);

#define swl_ttb_assertEnumNotEquals(val1, val2, strList) \
    ttb_assert_msg(val1 != val2, "Enum inequality expected, but got '%s'", strList[val1]);

#define swl_ttb_assertFileEquals(fileName1, fileName2) \
    { \
        char buf1[256] = {0}; \
        char buf2[256] = {0}; \
        size_t offset = 0; \
        size_t line = 0; \
        swl_rc_ne ret = swl_fileUtils_getContentDiff(fileName1, fileName2, buf1, buf2, 256, &line, &offset); \
        ttb_assert_msg(ret >= SWL_RC_ERROR, "Unable to diff: err %s", swl_rc_toString(ret)); \
        ttb_assert_msg(ret == SWL_RC_ERROR, "Expected files equal '%s' - '%s', but diff on line %zu, char %zu :\n * '%s'\n * '%s'", \
                       fileName1, fileName2, line, offset, buf1, buf2); \
    }

#define swl_ttb_assertTypeToFileEquals(type, fileName, typeData, printArgs, append) \
    { \
        char tmpNameBuffer[L_tmpnam] = {0}; \
        tmpnam(tmpNameBuffer); \
        ttb_assert(swl_type_toFileName((swl_type_t*) type, !gTtb_assert_printMode ? tmpNameBuffer : fileName, typeData, printArgs, append)); \
        if(!gTtb_assert_printMode) { \
            swl_ttb_assertFileEquals(tmpNameBuffer, fileName); \
            remove(tmpNameBuffer); \
        } \
    }

#endif /* SRC_INCLUDE_SWL_TTB_SWL_TTB_H_ */
