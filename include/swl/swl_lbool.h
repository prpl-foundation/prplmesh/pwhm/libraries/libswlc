/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_LBOOL_H_
#define SRC_INCLUDE_SWL_LBOOL_H_

#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

/**
 * "fat" boolean that also tracks which line in which file the decision was made.
 */
typedef struct {
    /** PRIVATE */
    int32_t line;
    /** PRIVATE */
    const char* file;
    /** PRIVATE. Use #SWL_LBOOL_BOOL */
    bool value;
} swl_lbool_t;

/**
 * Create a "yes" value, tracking where in the code this "yes" was decided.
 */
#define SWL_LBOOL_TRUE ((swl_lbool_t) {.line = __LINE__, .file = __FILE__, .value = true})

/**
 * Create a "no" value, tracking where in the code this "no" was decided.
 */
#define SWL_LBOOL_FALSE ((swl_lbool_t) {.line = __LINE__, .file = __FILE__, .value = false})

/**
 * Converts a boolean into an lbool.
 */
#define SWL_LBOOL(x) ((x) ? SWL_LBOOL_TRUE : SWL_LBOOL_FALSE)

/**
 * Converts an lbool into a boolean
 */
#define SWL_LBOOL_BOOL(x) ((x).value)


/**
 * Format string for printing the lbool.
 *
 * Example:
 * ```
 * swl_lbool_t shouldBeOn = shouldLaserBeTurnedOn(targetName);
 * SAH_TRACEZ_INFO(ME, "Should laser be on: " SWL_LBOOL_FMT " for target %s", SWL_LBOOL_ARG(shouldBeOn), targetName);
 * ```
 */
#define SWL_LBOOL_FMT "%s(%s:%" PRId32 ")"

/**
 * See #SWL_LBOOL_FMT
 */
#define SWL_LBOOL_ARG(x) (x).value ? "true" : "false", (x).file, (x).line

#endif
