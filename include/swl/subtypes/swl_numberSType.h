/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERSTYPE_H_
#define SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERTYPE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"

/**
 * Typedef for a type that has typeFlag number.
 */
typedef swl_type_t swl_numberType_t;


/**
 * Convert the number data to an int64_t value;
 */
typedef int64_t (* swl_numberSType_toInt64) (swl_numberType_t* numberType, swl_typeEl_t* el);

/**
 * Convert the number data to a uint64_t value;
 */
typedef uint64_t (* swl_numberSType_toUInt64) (swl_numberType_t* numberType, swl_typeEl_t* el);

/**
 * write an int64_t value to the data value.
 * If input value is out of bounds, SWL_RC_RESULT_OUT_OF_BOUNDS is returned, and overflow rules apply, so el is changed.
 * If input value is invalid, SWL_RC_INVALID_PARAM is returned, and nothing is changed.
 * If value can be accurately written, SWL_RC_OK is returned.
 */
typedef swl_rc_ne (* swl_numberSType_fromInt64) (swl_numberType_t* numberType, swl_typeEl_t* el, int64_t val);

/**
 * write an int64_t value to the data value.
 * If input value is out of bounds, SWL_RC_RESULT_OUT_OF_BOUNDS is returned, and overflow rules apply, so el is changed.
 * If input value is invalid, SWL_RC_INVALID_PARAM is returned, and nothing is changed.
 * If value can be accurately written, SWL_RC_OK is returned.
 */
typedef swl_rc_ne (* swl_numberSType_fromUInt64) (swl_numberType_t* numberType, swl_typeEl_t* el, uint64_t val);

/**
 * Get the integer bytes pointed by el, swap the bytes and write the result in out.
 * If value can be accurately written, SWL_RC_OK is returned.
 */
typedef swl_rc_ne (* swl_numberSType_switchHtoN) (swl_numberType_t* numberType, swl_typeEl_t* el, void* out);

typedef struct  {
    bool isSigned;
    uint8_t nrBits;
    int64_t minVal;
    uint64_t maxVal;
    swl_numberSType_toInt64 toInt64;
    swl_numberSType_fromInt64 fromInt64;
    swl_numberSType_toUInt64 toUInt64;
    swl_numberSType_fromUInt64 fromUInt64;
    swl_numberSType_switchHtoN switchHtoN;
} swl_numberSTypeFun_t;


swl_rc_ne swl_numberSType_add(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2);
swl_rc_ne swl_numberSType_sub(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2);
swl_rc_ne swl_numberSType_mul(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2);
swl_rc_ne swl_numberSType_div(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2);
swl_rc_ne swl_numberSType_mod(swl_numberType_t* type, swl_typeEl_t* target, swl_typeEl_t* data1, swl_typeEl_t* data2);

bool swl_numberSType_isIntInRange(swl_numberType_t* type, int64_t val);
bool swl_numberSType_isUIntInRange(swl_numberType_t* type, uint64_t val);


#endif /* SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERSTYPE_H_ */
