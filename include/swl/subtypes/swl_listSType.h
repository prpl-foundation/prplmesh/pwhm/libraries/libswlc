/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_LISTSTYPE_H_
#define SRC_INCLUDE_SWL_LISTSTYPE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"


/**
 * A collection is basically a type element of a collectionType
 */
typedef swl_typeEl_t swl_listEl_t;

typedef swl_type_t swl_listSType_t;

typedef struct swl_listSTypeFun_s swl_listSTypeFun_t;

typedef struct {
    bool valid;
    swl_listEl_t* coll;   // pointer to the collection where this lists points.
    void* block;          // optional internal pointer for list iteration
    size_t blockOffset;   // optional internal offset for list iterator
    size_t index;         // index of value in list, must be set
    swl_type_t* dataType; // data type of data at index
    swl_typeEl_t* data;   // data at index
} swl_listSTypeIt_t;

/**
 * Initialize the collection from given collection type
 */
typedef swl_rc_ne (* swl_listSType_init_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * Initialize the collection from given collection type, and initialize its content to provided array.
 *
 */
typedef swl_rc_ne (* swl_listSType_initFromArray_cb) (swl_listSType_t* colType, swl_listEl_t* coll, swl_typeEl_t* array, size_t arraySize);

/**
 * Clear the collection, removing the contents but keeping the collection initialized
 */
typedef void (* swl_listSType_clear_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * Cleanup the collection, deallocating whatever needs to be deallocated. The collection should
 * be reinitialized before further use.
 */
typedef void (* swl_listSType_cleanup_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * Return the maximum size of this collection type.
 * If there is no maximum size, other than "dynamic memory allocation allowance", 0 shall be returned.
 */
typedef size_t (* swl_listSType_maxSize_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * The current size of this collection.
 *
 * For fixed size collections, this shall return the number of non-zero elements.
 * These collections may have an allowEmpty flag, which basically means whether "empty" is a valid value,
 * and size counting should continue after.
 */
typedef size_t (* swl_listSType_size_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * Add data to given collection
 *
 * For fixed size collections, data may be written to first empty space, if possible.
 * If collection does not support add, return SWL_RC_NOT_IMPLEMENTED
 */
typedef ssize_t (* swl_listSType_add_cb) (swl_listSType_t* colType, swl_listEl_t* coll, swl_typeData_t* data);

/**
 * Provide reference to an "empty" element, if possible.
 * For dynamic collections, it shall create a new empty element.
 */
typedef swl_typeEl_t* (* swl_listSType_alloc_cb) (swl_listSType_t* colType, swl_listEl_t* coll);

/**
 * Insert data at given index.
 */
typedef swl_rc_ne (* swl_listSType_insert_cb) (swl_listSType_t* colType, swl_listEl_t* coll, swl_typeData_t* data, ssize_t index);

/**
 * Write data at given index, removing whatever data may have been there before.
 * Provided data shall be deep copied into target space.
 */
typedef swl_rc_ne (* swl_listSType_set_cb) (swl_listSType_t* colType, swl_listEl_t* coll, swl_typeData_t* data, ssize_t index);

/**
 * Delete the data at given index. All other data with index higher then provided index shall be shifted one down, if
 * it's a "same type" collection.
 */
typedef swl_rc_ne (* swl_listSType_delete_cb) (swl_listSType_t* colType, swl_listEl_t* coll, ssize_t index);

/**
 * Find the index of the given data.
 * If collection does not support find, return SWL_RC_NOT_IMPLEMENTED
 */
typedef ssize_t (* swl_listSType_find_cb) (swl_listSType_t* colType, swl_listEl_t* coll, swl_typeData_t* data);

/**
 * Check whether two collections are equal
 */
typedef bool (* swl_listSType_equals_cb) (swl_listSType_t* colType, swl_listEl_t* coll1, swl_listEl_t* coll2);

/**
 * Get the value data pointer of the value at the given index.
 */
typedef swl_typeData_t* (* swl_listSType_getValue_cb) (swl_listSType_t* colType, swl_listEl_t* coll, ssize_t index);

/**
 * Provide the element pointer of the value at the given index.
 */
typedef swl_typeEl_t* (* swl_listSType_getReference_cb) (swl_listSType_t* colType, swl_listEl_t* coll, ssize_t index);

/**
 * Get an iterator, initialized at the first element
 */
typedef swl_listSTypeIt_t (* swl_listSType_getFirstIt_cb) (swl_listSType_t* colType, const swl_listEl_t* coll);

/**
 * Move iterator to next element
 */
typedef void (* swl_listSTypeIt_nextIt_cb) (swl_listSType_t* colType, swl_listSTypeIt_t* it);

/**
 * Delete current element. Note that this will NOT move the iterator to next element, so
 * you MUST still call nextIt.
 * Calling  delIt a second time shall result in no change.
 */
typedef void (* swl_listSTypeIt_delIt_cb) (swl_listSType_t* colType, swl_listSTypeIt_t* it);

struct swl_listSTypeFun_s {
    const char* name;
    swl_listSType_init_cb init;
    swl_listSType_initFromArray_cb initFromArray;
    swl_listSType_clear_cb clear;
    swl_listSType_cleanup_cb cleanup;
    swl_listSType_maxSize_cb maxSize;
    swl_listSType_size_cb size;
    swl_listSType_add_cb add;
    swl_listSType_alloc_cb alloc;
    swl_listSType_insert_cb insert;
    swl_listSType_set_cb set;
    swl_listSType_delete_cb delete;
    swl_listSType_find_cb find;
    swl_listSType_equals_cb equals;
    swl_listSType_getValue_cb getValue;
    swl_listSType_getReference_cb getReference;
    swl_listSType_getFirstIt_cb firstIt;
    swl_listSTypeIt_nextIt_cb nextIt;
    swl_listSTypeIt_delIt_cb delIt;
};

swl_listSTypeIt_t swl_listSType_getFirstIt(swl_listSType_t* colType, const swl_listEl_t* coll);
void swl_listSType_nextIt(swl_listSType_t* colType, swl_listSTypeIt_t* it);
void swl_listSType_delIt(swl_listSType_t* colType, swl_listSTypeIt_t* it);
bool swl_listSTypeIt_assignVal(swl_listSTypeIt_t* it, swl_listSType_t* colType, swl_typeEl_t* tgtData);

#define swl_listSType_forEachRef(type, myIt, dataType, dataName, col) \
    dataType* dataName = NULL; \
    for(swl_listSTypeIt_t myIt = swl_listSType_getFirstIt(type, col); \
        myIt.valid && ((dataName = (dataType*) myIt.data) || true ); \
        swl_listSType_nextIt(type, &myIt) \
        )

#define swl_listSType_forEachVal(type, myIt, dataType, dataName, col) \
    dataType dataName; \
    memset(&dataName, 0, sizeof(dataType)); \
    for(swl_listSTypeIt_t myIt = swl_listSType_getFirstIt(type, col); \
        swl_listSTypeIt_assignVal(&myIt, type, &dataName); \
        swl_listSType_nextIt(type, &myIt) \
        )

#endif /* SRC_INCLUDE_SWL_LISTSTYPE_H_ */

