/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERTYPE_H_
#define SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERTYPE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"

/**
 * Typedef for a type that has map typeFlag.
 */
typedef swl_type_t swl_mapSType_t;

/**
 * Typedef for structures that are of the swl_mapSType_t type.
 */
typedef swl_typeEl_t swl_mapEl_t;

typedef struct swl_mapSTypeFun_s swl_mapSTypeFun_t;


/**
 * Type that encodes the data for a single entry. Format can be type specific so do not mix and match !
 */
typedef void swl_mapEntry_t;

typedef struct {
    bool valid;
    swl_mapEl_t* map;     // pointer to the map where this iterator is used for
    void* block;          // optional internal pointer for list iteration
    size_t blockOffset;   // optional internal offset for list iterator
    size_t index;         // index of value in map. Optional only for indexed maps.
    swl_mapEntry_t* data; // entry info belonging to the given map. This is map type specific
} swl_mapSTypeIt_t;



/**
 * Initialize the map from given map type
 */
typedef swl_rc_ne (* swl_mapSType_init_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * Clear the map, removing the contents but keeping the map initialized
 */
typedef void (* swl_mapSType_clear_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);


/**
 * Return the type of the keys of this collection.
 * A map currently only supports having a single key type.
 */
typedef swl_type_t* (* swl_mapSType_getKeyType_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * Return the type of the value of the given key. If isSingleValueMap is set to true, then
 * this must return said type regardsless of key input (i.e. NULL or non-existant key).
 * If isSingleValueMap is set to false, it must return NULL for keys that are not present, and the relevant type if the
 * value exists that matches the given key.
 */
typedef swl_type_t* (* swl_mapSType_getValueType_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* key);

/**
 * Cleanup the map, deallocating whatever needs to be deallocated. The map should
 * be reinitialized before further use.
 */
typedef void (* swl_mapSType_cleanup_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * Return the maximum size of this map type.
 * If there is no maximum size, other than "dynamic memory allocation allowance", 0 shall be returned.
 */
typedef size_t (* swl_mapSType_maxSize_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * The current size of this map.
 *
 * For fixed size maps, this shall return the number of non-zero elements.
 */
typedef size_t (* swl_mapSType_size_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * Add data to given map, if possible.
 * Will not be possible for fixedName maps.
 */
typedef bool (* swl_mapSType_add_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* key, swl_typeData_t* value);

/**
 * Provide reference to an "empty" element, if possible.
 * For dynamic maps, it shall create a new empty element.
 * To write data into the entry, the getEntryKeyRef and getEntryValRef can be used.
 * Note that this function is not possible to use for dynamic type maps, or isFixedName maps
 */
typedef swl_mapEntry_t* (* swl_mapSType_alloc_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll);

/**
 * Write data at given key, removing whatever data may have been there before.
 * Provided data shall be deep copied into target space.
 */
typedef swl_rc_ne (* swl_mapSType_set_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* key, swl_typeData_t* data);

/**
 * Delete the data at given key.
 */
typedef swl_rc_ne (* swl_mapSType_delete_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* key);

/**
 * Find a key for which the value equals data. If multiple such keys exist, any may be returned.
 * Will only be allowed in single value type maps.
 */
typedef swl_typeData_t* (* swl_mapSType_find_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* data);

/**
 * Check whether two maps are equal
 */
typedef bool (* swl_mapSType_equals_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll1, swl_mapEl_t* coll2);

/**
 * Get the value data pointer of the value at the given key.
 */
typedef swl_typeData_t* (* swl_mapSType_getValue_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* data);

/**
 * Provide the element pointer of the value at the given key.
 */
typedef swl_typeEl_t* (* swl_mapSType_getReference_cb) (swl_mapSType_t* colType, swl_mapEl_t* coll, swl_typeData_t* data);

/**
 * Get an iterator, initialized at the starting element. The data field shall be an swl_mapEntry_t
 * to be used for this specific swl_mapSType_t
 */
typedef swl_listSTypeIt_t (* swl_mapSType_getFirstIt_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll);

/**
 * Move iterator to next element
 */
typedef void (* swl_mapSTypeIt_nextIt_cb) (swl_mapSType_t* colType, swl_listSTypeIt_t* it);

/**
 * Delete current element. Note that this will NOT move the iterator to next element, so
 * you MUST still call nextIt.
 * Calling  delIt a second time shall result in no change.
 */
typedef void (* swl_mapSTypeIt_delIt_cb) (swl_mapSType_t* colType, swl_listSTypeIt_t* it);

/**
 * Get the key data for a given map entry. Should not be changed
 */
typedef swl_typeData_t* (* swl_mapSType_key_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll, swl_mapEntry_t* mapEntry);

typedef swl_typeData_t* (* swl_mapSType_value_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll, swl_mapEntry_t* mapEntry);

/**
 * Get the key element for a given entry. Should only be provided if dynamic key changes are supported
 * Should return null if areKeysFixed is true.
 */
typedef swl_typeEl_t* (* swl_mapSType_keyRef_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll, swl_mapEntry_t* mapEntry);

/**
 * Return a reference to the value of the given entry.
 */
typedef swl_typeEl_t* (* swl_mapSType_valueRef_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll, swl_mapEntry_t* mapEntry);

/**
 * Return the type of the given entry.
 */
typedef swl_type_t* (* swl_mapSType_valueType_cb) (swl_mapSType_t* colType, const swl_mapEl_t* coll, swl_mapEntry_t* mapEntry);

struct swl_mapSTypeFun_s {

    /**
     * Whether the keys are fixed. This means that only preset keys are allowed, and no new entries can be added.
     */
    bool areKeysFixed;

    /**
     * Whether all values are of the same type, or may differ in type.
     */
    bool hasVarValType;

    swl_mapSType_init_cb init;
    swl_mapSType_clear_cb clear;
    swl_mapSType_cleanup_cb cleanup;
    swl_mapSType_maxSize_cb maxSize;
    swl_mapSType_size_cb size;
    swl_mapSType_getKeyType_cb getKeyType;
    swl_mapSType_getValueType_cb getValueType;
    swl_mapSType_add_cb add;
    swl_mapSType_alloc_cb alloc;
    swl_mapSType_set_cb set;
    swl_mapSType_delete_cb delete;
    swl_mapSType_find_cb find;
    swl_mapSType_equals_cb equals;
    swl_mapSType_getValue_cb getValue;
    swl_mapSType_getReference_cb getReference;
    swl_mapSType_getFirstIt_cb firstIt;
    swl_mapSTypeIt_nextIt_cb nextIt;
    swl_mapSTypeIt_delIt_cb delIt;
    swl_mapSType_key_cb getEntryKey;
    swl_mapSType_value_cb getEntryVal;
    swl_mapSType_keyRef_cb getEntryKeyRef;
    swl_mapSType_valueRef_cb getEntryValRef;
    swl_mapSType_valueType_cb getEntryValType;
};



#endif /* SRC_INCLUDE_SWL_SUBTYPES_SWL_NUMBERTYPE_H_ */
