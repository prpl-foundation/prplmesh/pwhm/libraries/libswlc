/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_COMMON_TABLE_H_
#define SRC_INCLUDE_SWL_COMMON_TABLE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_tupleType.h"


#define SWL_TABLE_GROWTH_FACTOR 500
#define SWL_TABLE_SHRINK_SIZE 500
#define SWL_TABLE_SHRINK_FACTOR 666
#define SWL_TABLE_DEFAULT_MIN_SIZE (size_t) 10

typedef struct {
    swl_tupleType_t* tupleType;
    swl_tuple_t* tuples;
    size_t nrTuples;
    size_t curNrTuples;
    bool isDynamic;
} swl_table_t;

/**
 * Macro to simplify definition of a table.
 *
 * To use this, note that array definitions need to be done with ARR(...) or KEEPCOMMA(...)
 * as otherwise precompiler will misinterpret braces and commas.
 * No init needs to be done.
 *
 * @param tableName: name of the tuple
 * @param tupleStruct: struct definition of entry element
 * @param tupleSwlTypes: swl types matching the elements in the struct.
 * @param tupleSwlValues: Data
 *
 * Example usage:
 * SWL_TABLE(myTuple,
          ARR(int32_t index; int64_t key; char* val; ),
          ARR(swl_type_int32, swl_type_int64, swl_type_charPtr),
          ARR({1, 100, "test1"},
              {2, -200, "test2"},
              {-8852, 289870, "foobar"})
          );
 **
 * the struct shall be named myTupleStruct_t, and the values shall be named myTupleValues.
 */
#define SWL_TABLE(tableName, tupleStruct, tupleSwlTypes, tupleSwlValues) \
    typedef struct SWL_PACKED tupleStruct tableName ## Struct_t; \
    tableName ## Struct_t tableName ## Values[] = tupleSwlValues; \
    SWL_TUPLE_TYPE(tableName, KEEPCOMMA(tupleSwlTypes)) \
    swl_table_t tableName = { \
        .tupleType = &tableName ## TupleType, \
        .tuples = tableName ## Values, \
        .nrTuples = SWL_ARRAY_SIZE(tableName ## Values), \
        .curNrTuples = SWL_ARRAY_SIZE(tableName ## Values), \
        .isDynamic = false, \
    }


/**
 * Create a table that can be dynamically allocated and reallocated.
 */
#define SWL_TABLE_ALLOC(tableName, tupleSwlTypes) \
    SWL_TUPLE_TYPE(tableName, KEEPCOMMA(tupleSwlTypes)) \
    swl_table_t tableName = { \
        .tupleType = &tableName ## TupleType, \
        .tuples = NULL, \
        .nrTuples = 0, \
        .curNrTuples = 0, \
        .isDynamic = true, \
    }



/**
 * Define a table that is statically allocated and assigned.
 * Size of this table cannot change.
 */
#define SWL_TABLE_FIXED(tableName, typeName, tupleSwlValues) \
    typeName ## _type tableName ## Values[] = tupleSwlValues; \
    swl_table_t tableName = { \
        .tupleType = &typeName, \
        .tuples = tableName ## Values, \
        .nrTuples = SWL_ARRAY_SIZE(tableName ## Values), \
        .curNrTuples = SWL_ARRAY_SIZE(tableName ## Values), \
        .isDynamic = false, \
    }


#define SWL_TABLE_NEW(tableName, typeName, size) \
    swl_table_t tableName = { \
        .tupleType = &typeName, \
        .tuples = NULL, \
        .nrTuples = 0, \
        .curNrTuples = 0, \
        .isDynamic = true, \
    }; \
    swl_table_init(&tableName, &typeName, size);


bool swl_table_checkValid(const swl_table_t* table, bool dynamicOnly);
size_t swl_table_getSize(const swl_table_t* table);
swl_tuple_t* swl_table_getTuple(swl_table_t* table, ssize_t index);
swl_typeEl_t* swl_table_getReference(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex);
swl_typeData_t* swl_table_getValue(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex);
ssize_t swl_table_find(const swl_table_t* table, const swl_typeData_t* val);
ssize_t swl_table_findMatchingValue(swl_table_t* table, size_t offset, size_t srcIndex, const swl_typeData_t* srcValue);
ssize_t swl_table_findMatchingTupleByMask(swl_table_t* table,
                                          size_t offset, const swl_tuple_t* srcValue, size_t mask);

swl_typeData_t* swl_table_getMatchingValue(swl_table_t* table, size_t tgtIndex, size_t srcIndex, const swl_typeData_t* srcValue);
swl_typeData_t* swl_table_getMatchingValueOffset(swl_table_t* table, size_t tgtTypeIndex, size_t srcIndex, const swl_typeData_t* srcValue, size_t offset);
swl_tuple_t* swl_table_getMatchingTuple(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue);
swl_tuple_t* swl_table_getMatchingTupleInRange(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue,
                                               ssize_t startElem, ssize_t endElem, ssize_t stepSize);

swl_tuple_t* swl_table_getMatchingTupleOffset(swl_table_t* table, size_t srcIndex, const swl_typeData_t* srcValue, size_t offset);

swl_tuple_t* swl_table_getMatchingTupleByTuple(swl_table_t* table, const swl_tuple_t* srcValue, size_t mask, size_t offset);
swl_typeData_t* swl_table_getMatchingValueByTuple(swl_table_t* table, size_t tgtTypeIndex, const swl_tuple_t* srcValue, size_t mask, size_t offset);

void swl_table_init(swl_table_t* table, swl_tupleType_t* tupleType, size_t size);
swl_rc_ne swl_table_initMax(swl_table_t* table, swl_tupleType_t* tupleType, size_t maxSize);
void swl_table_clear(swl_table_t* table);
void swl_table_cleanElements(swl_table_t* table);

void swl_table_cleanupColumn(swl_table_t* table, size_t typeIndex);
void swl_table_cleanupByMask(swl_table_t* table, size_t typeMask);
//DEPRECATED
void swl_table_cleanup(swl_table_t* table, bool cleanAllRow, size_t rowMask);

void swl_table_destroy(swl_table_t* table);
void swl_table_resize(swl_table_t* table, size_t newSize, bool copy);
swl_table_t* swl_table_copy(swl_table_t* srcTable);
ssize_t swl_table_add(swl_table_t* table, swl_tuple_t* data);
swl_tuple_t* swl_table_alloc(swl_table_t* table);
swl_rc_ne swl_table_insert(swl_table_t* table, swl_tuple_t* data, ssize_t index);
swl_rc_ne swl_table_delete(swl_table_t* table, ssize_t index);

swl_rc_ne swl_table_setValue(swl_table_t* table, ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data);
swl_rc_ne swl_table_setTuple(swl_table_t* table, ssize_t tupleIndex, swl_tuple_t* data);

swl_rc_ne swl_table_columnToArray(swl_typeEl_t* array, size_t arraySize, swl_table_t* table, size_t typeIndex);
swl_rc_ne swl_table_columnToArrayOffset(swl_typeEl_t* array, size_t arraySize, swl_table_t* table, size_t typeIndex, size_t offSet);
bool swl_table_equals(swl_table_t* table1, swl_table_t* table2);


ssize_t swl_table_printLists(swl_table_t* table, char* tgtStr, size_t tgtStrSize, size_t offset);
bool swl_table_fprintLists(swl_table_t* table, FILE* file, size_t offset, swl_print_args_t* args);
ssize_t swl_table_printMaps(swl_table_t* table, char* tgtStr, size_t tgtStrSize,
                            size_t offset, char** names, char* suffix);
bool swl_table_fprintMaps(swl_table_t* table, FILE* file,
                          size_t offset, swl_print_args_t* args,
                          char** names, char* suffix);
/**
 * Macro to simplify definition of a table with two columns of strings.
 *
 * Example:
 * ```c
 *     SWL_TABLE_STR2STR(alternatives, ARR(
 *        {"zsh",   "bash"},
 *        {"linux", "bsd"},
 *        {"vi", "emacs"}
 *     );
 * ```
 *
 * @param tableName: identifier. A variable will be declared and defined with this name.
 * @param values: the contents of the table.
 */
#define SWL_TABLE_STR2STR(tableName, values) \
    SWL_TABLE(tableName, \
              KEEPCOMMA({char* val1; char* val2; }), \
              KEEPCOMMA({swl_type_charPtr, swl_type_charPtr}), \
              KEEPCOMMA(values));

/**
 * Macro to simplify definition of a table of uint32 to char* string.
 */
#define SWL_TABLE_UINT32STR(tableName, values) \
    SWL_TABLE(tableName, \
              KEEPCOMMA({uint32_t val1; char* val2; }), \
              KEEPCOMMA({swl_type_uint32, swl_type_charPtr}), \
              KEEPCOMMA(values));


/**
 * Macro to simplify definition of a table of uint64_t to char* string.
 */
#define SWL_TABLE_UINT64STR(tableName, values) \
    SWL_TABLE(tableName, \
              KEEPCOMMA({uint64_t val1; char* val2; }), \
              KEEPCOMMA({swl_type_uint64, swl_type_charPtr}), \
              KEEPCOMMA(values));

static inline char* swl_tableUInt32Char_toValue(swl_table_t* table, uint32_t value) {
    return (char*) swl_table_getMatchingValue(table, 1, 0, &value);
}
static inline uint32_t* swl_tableUInt32Char_toKey(swl_table_t* table, char* value) {
    return (uint32_t*) swl_table_getMatchingValue(table, 0, 1, value);
}

/**
 * Macro to simplify definition of a table of uint32 to uint32 conversion.
 */
#define SWL_TABLE_UINT32(tableName, values) \
    SWL_TABLE(tableName, \
              KEEPCOMMA({uint32_t val1; uint32_t val2; }), \
              KEEPCOMMA({swl_type_uint32, swl_type_uint32}), \
              KEEPCOMMA(values));

static inline uint32_t* swl_tableUInt32_toValue(swl_table_t* table, uint32_t value) {
    return (uint32_t*) swl_table_getMatchingValue(table, 1, 0, &value);
}

static inline uint32_t* swl_tableUInt32_toKey(swl_table_t* table, uint32_t value) {
    return (uint32_t*) swl_table_getMatchingValue(table, 0, 1, &value);
}


/**
 * Convert inputMask into a list of strings
 * @param tgtStr: buffer where to save the list of strings representing all set bits, elements are separated with the 'separator'
 * @param tgtStrSize: size of output string buffer
 * @param inputMask: bitmask value
 * @param table: string mapping bitMask-->string value
 * @param separator: output list separator
 *
 * return size of output buffer, If the size of the buffer is small, output string will be limited to tgtStrSize, but returned valye will be the size of the
 * whole needed buffer to write all the output string.
 * NOTE: the returned value does not count the terminating null character!
 * return SWL_RC_INVALID_PARAM for any param issue.
 *
 */
ssize_t swl_tableUInt64Char_maskToCharSep(swl_table_t* table, char* tgtStr, size_t tgtStrSize, swl_mask64_m inputMask, char separator);


#endif /* SRC_INCLUDE_SWL_TABLE_H_ */
