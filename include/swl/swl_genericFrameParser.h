/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * Defines for ieee802.11 frame information
 */

#ifndef SRC_INCLUDE_SWL_SWL_GENERIC_FRAME_PARSER_H_
#define SRC_INCLUDE_SWL_SWL_GENERIC_FRAME_PARSER_H_

#include "swl/swl_80211.h"
#include "swl/swl_common_oui.h"
#include "swl/swl_common_chanspec.h"
#include "swl/swl_common_mcs.h"
#include "swl/swl_security.h"
#include "swl/swl_staCap.h"
#include "swl/swl_wps.h"

typedef struct {
    swl_chanspec_t operChanInfo;
    // For AssocDevice
    swl_uniiBand_m uniiBandsCapabilities;

    // For AssocDevice Caps
    swl_oui_list_t vendorOUI;
    swl_staCap_m capabilities;
    swl_staCapVendor_m vendorCapabilities;
    swl_staCapHt_m htCapabilities;
    swl_staCapVht_m vhtCapabilities;
    swl_staCapHe_m heCapabilities;
    swl_staCapRrm_m rrmCapabilities;
    uint32_t rrmOnChannelMaxDuration;
    uint32_t rrmOffChannelMaxDuration;
    swl_staCapEht_m ehtCapabilities;
    swl_staCapEhtEml_m ehtEmlCapabilities;
    swl_macBin_t ehtMldMacAddress;
    uint8_t ehtLinksMask;
    swl_macBin_t ehtLinksMacAddress[14];
    swl_freqBandExt_m freqCapabilities;

    uint16_t maxRxSpatialStreamsSupported; /* number of Rx Spatial streams*/
    uint16_t maxTxSpatialStreamsSupported; /* number of Tx Spatial streams*/
    uint32_t maxDownlinkRateSupported;
    uint32_t maxUplinkRateSupported;

    swl_mcs_supMCS_t supportedMCS;
    swl_mcs_supMCS_t supportedHtMCS;
    swl_mcs_supMCS_adv_t supportedVhtMCS[SWL_COM_DIR_MAX];
    swl_mcs_supMCS_adv_t supportedHeMCS[SWL_COM_DIR_MAX];
    swl_mcs_supMCS_adv_t supportedHe160MCS[SWL_COM_DIR_MAX];
    swl_mcs_supMCS_adv_t supportedHe80x80MCS[SWL_COM_DIR_MAX];

    // For SSID Scan
    uint8_t ssidLen;
    char ssid[SWL_80211_SSID_STR_LEN];
    swl_security_apMode_e secModeEnabled;
    swl_radioStandard_m operatingStandards;
    swl_wps_cfgMethod_m WPS_ConfigMethodsEnabled;
    uint16_t wpsPasswordID;
    bool wpsRequestToEnroll;
    char modelName[64];
    char modelNumber[64];
    char manufacturer[64];

    char country[3];
    swl_opClassCountry_e operClassRegion;

    //BSS load elements
    uint16_t stationCount;
    uint8_t channelUtilization;
    uint16_t availAdmissionCapacity;

} swl_wirelessDevice_infoElements_t;

/*
 * @brief optional argument that may improve frame IEs parsing
 */
typedef struct {
    swl_chanspec_t seenOnChanspec; //channel on which the frame has been seen.
} swl_parsingArgs_t;


swl_rc_ne swl_80211_processCustomFrame(void* pUserData, swl_table_t* pCustomParseTable, swl_parsingArgs_t* pParsingArgs, uint8_t elementId, int8_t len, uint8_t* data);

swl_rc_ne swl_80211_processInfoElementFrame(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs, uint8_t elementId, int8_t len, uint8_t* data);

/*
 * @brief Parse 80211 Info Elements buffer (including TLVs) into user data structure with custom parsing table
 *
 * @param pUserData: (out) pointer to device info to be filled
 * @param pCustomParseTable (in) A custom defined parsing table for custom handling of different IEs
 * @param pParsingArgs: (in) pointer to optional parsing argument, that may be used to initialize some result values
 * @param iesLen: (in) Information Elements buffer length
 * @param iesData: (in) Information Elements buffer data
 *
 * @return number of processed bytes of the input buffer (shall be equal to iesLen when all data has been processed)
 *         or negative SWL_RC error code when major error happens.
 *         It is up to application to consider error when parsing is partial (i.e returning < iesLen)
 */
ssize_t swl_80211_parseCustomInfoElementsBuffer(void* pUserData, swl_table_t* pCustomParseTable, swl_parsingArgs_t* pParsingArgs, size_t iesLen, uint8_t* iesData);


/*
 * @brief Parse 80211 Info Elements buffer (including TLVs) into user data structure
 *
 * @param pWirelessDevIE: (out) pointer to device info to be filled
 * @param pParsingArgs: (in) pointer to optional parsing argument, that may be used to initialize some result values
 * @param iesLen: (in) Information Elements buffer length
 * @param iesData: (in) Information Elements buffer data
 *
 * @return number of processed bytes of the input buffer (shall be equal to iesLen when all data has been processed)
 *         or negative SWL_RC error code when major error happens.
 *         It is up to application to consider error when parsing is partial (i.e returning < iesLen)
 */
ssize_t swl_80211_parseInfoElementsBuffer(swl_wirelessDevice_infoElements_t* pWirelessDevIE, swl_parsingArgs_t* pParsingArgs, size_t iesLen, uint8_t* iesData);

/*
 * @brief check frame raw data as management frame
 *
 * @param frameData: (in) Management frame binary data
 * @param frameLen: (in) Management frame binary data length
 *
 * @return pointer to management frame struct when raw data checking is successful
 *         or NULL in case of error
 */
swl_80211_mgmtFrame_t* swl_80211_getMgmtFrame(swl_bit8_t* frameData, size_t frameLen);

/*
 * @brief locate 80211 Info Elements (with TLVs) into management frame raw data
 *
 * @param pIesLen: (out) length of located IEs Information Elements Tags (TLVs)
 * @param frameData: (in) Management frame binary data
 * @param frameLen: (in) Management frame binary data length
 *
 * @return pointer to located Information Elements Tags (TLVs)
 *         or NULL in case of error:
 *         invalid frame length or unsupported management frame subType
 */
swl_bit8_t* swl_80211_getInfoElementsOfMgmtFrame(size_t* pIesLen, swl_bit8_t* frameData, size_t frameLen);

typedef void (* swl_80211_mgmtFrameCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen);
typedef void (* swl_80211_actionFrameCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_actionFrame_t* actionFrame, size_t actionFrameDataLen);
typedef void (* swl_80211_assocReqCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_assocReqFrameBody_t* assocReq, size_t assocReqDataLen);
typedef void (* swl_80211_reassocReqCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_reassocReqFrameBody_t* reassocReq, size_t reassocReqDataLen);
typedef void (* swl_80211_probeReqCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_probeReqFrameBody_t* probeReq, size_t probeReqDataLen);
typedef void (* swl_80211_probeRespCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_probeRespFrameBody_t* probeResp, size_t probeRespDataLen);
typedef void (* swl_80211_beaconCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_beaconFrameBody_t* beacon, size_t beaconDataLen);
typedef void (* swl_80211_btmQueryCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_wnmActionBTMQueryFrameBody_t* btmQuery, size_t btmQueryDataLen);
typedef void (* swl_80211_btmRespCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_wnmActionBTMResponseFrameBody_t* btmResp, size_t btmRespDataLen);
typedef void (* swl_80211_actionVsCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, size_t frameLen, swl_80211_vendorIdEl_t* vendor, size_t vendorLen);
typedef void (* swl_80211_disassocCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, swl_80211_disassocFrameBody_t* disassocFrame);
typedef void (* swl_80211_deauthCb_f)(void* userData, swl_80211_mgmtFrame_t* frame, swl_80211_deauthFrameBody_t* deauthFrame);

/*
 * @brief structure of handlers for sub types of 80211 management frame
 */
typedef struct {
    swl_80211_mgmtFrameCb_f fProcMgmtFrame;          //basic handler of management frame
    swl_80211_actionFrameCb_f fProcActionFrame;      //basic handler of management action frame
    swl_80211_assocReqCb_f fProcAssocReq;            //handler of association request
    swl_80211_reassocReqCb_f fProcReAssocReq;        //handler of re-association request
    swl_80211_probeReqCb_f fProcProbeReq;            //handler of Probe request
    swl_80211_probeRespCb_f fProcProbeResp;          //handler of Probe response
    swl_80211_beaconCb_f fProcBeacon;                //handler of beacon frame
    swl_80211_btmQueryCb_f fProcBtmQuery;            //handler of BSS Transition management query
    swl_80211_btmRespCb_f fProcBtmResp;              //handler of BSS Transition management response
    swl_80211_actionVsCb_f fProcActionVs;            //handler of Vendor specific action frame
    swl_80211_disassocCb_f fProcDisassoc;            //handler of disassociation frame
    swl_80211_deauthCb_f fProcDeauth;                //handler of deauthentication frame
} swl_80211_mgmtFrameHandlers_cb;

/*
 * @brief dig in 80211 management frame, fetch included sub elements
 * and call relative handlers
 *
 * @param userData: (in) user data passed to handlers
 * @param frame: (in) management frame
 * @param frameLen: (in) Management frame binary data length
 * @param handlers: (in) pointer to handlers struct
 *
 * @return SWL_RC_OK in case of success, error code otherwise
 */
swl_rc_ne swl_80211_handleMgmtFrame(void* userData, swl_bit8_t* frameBin, size_t frameLen, swl_80211_mgmtFrameHandlers_cb* handlers);

swl_rc_ne swl_80211_getDataAKMsuite(uint8_t type, swl_80211_akm_m* akmSuiteParams);
swl_rc_ne swl_80211_getDataCipherSuite(uint8_t type, swl_80211_cipher_m* cipherSuiteParams);



#endif /* SRC_INCLUDE_SWL_SWL_GENERIC_FRAME_PARSER_H_ */

