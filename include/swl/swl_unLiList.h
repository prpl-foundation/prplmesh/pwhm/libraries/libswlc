/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_UNLILIST_H_
#define SRC_INCLUDE_SWL_UNLILIST_H_

#include <stdbool.h>
#include <unistd.h>
#include <swl/swl_bit.h>
#include <swl/swl_llist.h>
#include "swl/swl_returnCode.h"
#include "swl/types/swl_collType.h"

/**
 * Implementation of a list data calls.
 * Note that list is unordered, and indexes may shift when both adding and
 * removing data. No guarantee on index return is present.
 */

#define SWL_UNLILIST_DEFAULT_BLOCK_SIZE 8

//additional free space required in the leftover blocks after removal of block
#define SWL_UNLILIST_COMPRESS_THRESH 1

typedef struct {
    swl_llist_iterator_t it;
    uint32_t nrElements;
    swl_bit8_t data[1]; // flexible data entry
} swl_unLiListBlock_t;

typedef struct {
    swl_llist_t blocks;
    uint32_t blockSize;
    uint32_t elementSize;
    bool keepLastBlock;
} swl_unLiList_t;

typedef swl_listSTypeIt_t swl_unLiListIt_t;

swl_rc_ne swl_unLiList_initExt(swl_unLiList_t* list, uint32_t blockSize, uint32_t elemSize);
swl_rc_ne swl_unLiList_init(swl_unLiList_t* list, uint32_t elemSize);
bool swl_unLiList_isInitialized(const swl_unLiList_t* list);
swl_rc_ne swl_unLiList_setKeepsLastBlock(swl_unLiList_t* list, bool keepsLastBlock);
void swl_unLiList_destroy(swl_unLiList_t* list);

uint32_t swl_unLiList_size(const swl_unLiList_t* list);
bool swl_unLiList_contains(const swl_unLiList_t* list, const void* data);

ssize_t swl_unLiList_add(swl_unLiList_t* list, const void* data);
void* swl_unLiList_allocElement(swl_unLiList_t* list);
void* swl_unLiList_get(const swl_unLiList_t* list, int32_t index);
swl_rc_ne swl_unLiList_remove(swl_unLiList_t* list, int32_t index);
swl_rc_ne swl_unLiList_set(swl_unLiList_t* list, uint32_t index, void* data);
swl_rc_ne swl_unLiList_insert(swl_unLiList_t* list, int32_t inIndex, void* data);
int32_t swl_unLiList_removeByPointer(swl_unLiList_t* list, const void* data);
int32_t swl_unLiList_removeByData(swl_unLiList_t* list, const void* data);

void swl_unLiList_firstIt(swl_listSTypeIt_t* listIt, const swl_unLiList_t* list);
swl_listSTypeIt_t swl_unLiList_getFirstIt(const swl_unLiList_t* list);
void swl_unLiList_nextIt(swl_listSTypeIt_t* listIt);
void swl_unLiList_delIt(swl_listSTypeIt_t* item);

#define swl_unLiList_for_each(item, list) \
    for(swl_unLiList_firstIt(&item, list); item.valid; swl_unLiList_nextIt(&item))

#define swl_unLiList_data(item, type) \
    ((type) (item)->data)

uint32_t swl_unLiList_getNrBlocks(const swl_unLiList_t* list);

#endif /* SRC_INCLUDE_SWL_UNLILIST_H_ */
