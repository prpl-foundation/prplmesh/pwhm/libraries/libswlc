/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_COMMON_H_
#define SRC_INCLUDE_SWL_COMMON_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "swl_assert.h"
#include "swl_bit.h"
#include "swl_returnCode.h"
#include "swl_enum.h"

// Fix compiler warnings
#ifndef _UNUSED_
#define _UNUSED_(x) (void) x
#endif

#ifndef _UNUSED
#define _UNUSED __attribute__((unused))
#endif

#define SWL_PACKED __attribute__((__packed__))

#ifndef SWL_NOTIFICATION_BASE
#define SWL_NOTIFICATION_BASE 100
#endif

/**
 * Use this to pass an argument to a macro that contains commas.
 *
 * It workarounds that the macro preprocessor interprets commas as preprocessor-macro-argument
 * separator when intended to be c-language-commas.
 *
 * Note: uncrustify can deal better with #ARR than with #KEEPCOMMA.
 */
#define KEEPCOMMA(...) __VA_ARGS__
#define KEEP_COMMA(...) __VA_ARGS__

/**
 * `ARR(x)` is same as `KEEPCOMMA({x})`.
 *
 * Note that ARR cannot be used to pass arguments from one macro to another, as it adds
 * curly braces. To do that, use #KEEPCOMMA instead.
 */
#define ARR(...) {__VA_ARGS__}

/**
 * Return the size of the array in number of elements.
 */
#define SWL_ARRAY_SIZE(X) (((X) == NULL) ? 0 : (sizeof((X)) / sizeof((X)[0])))

/**
 * Convert from signed index to non signed index. Currently only supports [-arraySize, arraySize - 1], but
 * if replaced with swl_math_fmod, would support any integer
 */
#define SWL_ARRAY_INDEX(index, arraySize) (size_t) (index < 0 ? index + (ssize_t) arraySize : index)

/**
 * Add a certain offset to an index, over a given array size.
 * Will automatically cycle once if offset + index exceed array size, but will only cycle once
 */
#define SWL_ARRAY_INDEX_ADD(index, offset, arraySize) (index + offset > arraySize ? index + offset - arraySize : index + offset)

/**
 * Subtract a certain offset from index over a given array size.
 * Will automatically cycle if offset is larger than index, but will only cycle once
 */
#define SWL_ARRAY_INDEX_SUB(index, offset, arraySize) (offset > index ? index + arraySize - offset : index - offset)

#define SWL_HEXSTR_SIZE(X) (SWL_BYTE_SIZE(X) * 2)

#define SWL_MIN(a, b) \
    ({ __typeof__ (a) _a = (a); \
         __typeof__ (b) _b = (b); \
         _a < _b ? _a : _b; })

#define SWL_MAX(a, b) \
    ({ __typeof__ (a) _a = (a); \
         __typeof__ (b) _b = (b); \
         _a > _b ? _a : _b; })

#define SWL_CALL(fun, arg, ...) \
    if(fun != NULL) { \
        fun(arg, ## __VA_ARGS__); \
    }

//Set a pointer parameter to value if not null
#define W_SWL_SETPTR(param, val) \
    if(param != NULL) { \
        *param = val; \
    }

//If myVal is not null, then myVal will be freed, and NULL will be written to myVal
#define W_SWL_FREE(_myVal) \
    if(_myVal != NULL) { \
        free(_myVal); \
        _myVal = NULL; \
    }

#define SWL_CONSTRUCTOR __attribute__((constructor))

#define SWL_DESTRUCTOR __attribute__((destructor))

#define SWL_PRIVATE __attribute__((visibility("hidden")))

#endif /* SRC_INCLUDE_SWL_COMMON_H_ */

