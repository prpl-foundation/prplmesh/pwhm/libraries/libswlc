/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_DATETIME_H_
#define SRC_INCLUDE_SWL_DATETIME_H_

#include <time.h>
#include <sys/time.h>
#include <stdbool.h>
#include <inttypes.h>


/**


   @brief
   Date-time manipulation functions (TARDIS not included).

   @details
   This date-time implementation came about because of shortcomings of libc time handing,
    especially when it comes to sub-second precision time handling and conversion.

   Included are various conversion functions to convert from or to a @ref swl_datetime_t structure,
    which is internally used for keeping track of the date/time.

   <h1>Date time</h1>
   <h2>Definitions</h2>
   <p>
   A datetime in swl is defined to be an absolute time in UTC with no DST (daylight saving time)
    but WITH leap second adjustments.
   Implementations that are interested in the <i>local</i> time should do the conversion manually
    (i.e. by setting the TZ env variable for the correct time zone, converting the swl_datetime_t to a time_t,
    and calling localtime_r).
    To avoid confusion, please don't store a datetime in local time in a swl_datetime_t structure.
   </p>
   <h2>The swl_datetime_t type</h2>

   <p>The swl datetime type is composed of a struct tm and a nanoseconds field.
   You can think of it as an extension of the struct tm structure with an extra field to store the subsecond bit.</p>
   <p>This representation was chosen in order to be compatible with older interfaces that demand direct access to a struct tm
    (e.g. variant_da_dateTime). Otherwise, struct timespec would have been a better candidate.</p>

   <h2>String formatting and parsing</h2>
   <h3>ISO8601</h3>
   <p>
   The canonical string representation of date and time is ISO8601. It looks like this:
   <table>
   <th><td>index</td><td>representation</td><td>comment</td></th>
   <tr><td>1</td>2019-07-25</td><td>date only</td></tr>
   <tr><td>2</td>2019-07-25T16:24:33</td><td>unknown time zone</td></tr>
   <tr><td>3</td>2019-07-25T16:24:33Z</td><td>time zone is UTC</td></tr>
   <tr><td>4</td>2019-07-25T16:24:33.123456Z</td><td>microseconds/nanoseconds is represented by a fractional seconds field</td></tr>
   <tr><td>5</td>2019-07-25T16:24:33+02:00 </td><td>Time zones other than UTC are indicated by offset from UTC in +HH:MM or -HH:MM format</td></tr>
   </table>

   As in swl, all datetimes are assumed to be in UTC, the default formats used are #3 and #4.
   The predefined macros @ref swl_datetime_toIso, @ref swl_datetime_toIsoUs and @ref swl_datetime_toChar will convert
    a datetime to format #3, format #4, and #3 or #4 depending on if the sub-second part is zero or not, respectively.
   The predefined macro @ref swl_datetime_fromChar will parse a string into a datetime by assuming it is formatted as #3 or #4 above.
   <h3>Custom formats</h3>
   swl allows you to use custom formats for formatting and parsing your datetime. To do this, a printf-style format string is used.
    This is modeled on the C functions strftime() and strptime(). All format sequences supported by these C library functions
    should also be supported by swl.
    swl also allows 3 "non-standard" formats:
 * %f will expand to the number of microseconds (zero padded)
 * %N will expand to the number of nanoseconds  (zero padded)
 * %i will expand to a number of seconds and microseconds, expressed as a fractional number i.e. 12.345678

    For instance, a format of "%f microseconds past %H:%M:%S" might yield the string "123456 microseconds past 16:24:33"
    The same format specifiers can also be used for <i>parsing</i> a string back into a date time structure.
    Care should be taken that the input string does indeed match the expected format
    (swl will not check, and will continue parsing until an error occurs, meaning that parsing "2019-07-25T16:24:33.123456Z" using the format
    "%f microseconds past %H:%M:%S" will yield "002019 microseconds past 00:00:00" (the year was parsed as a number of microseconds, then parsing stopped))

    The function @ref swl_strftime is responsible for formatting a datetime into a string using a custom format. It is based on strftime(3)
    The function @ref swl_strptime is responsible for parsing a string into a datetime. It is based on strptime(3)
   <h2>Timing functions</h2>
   This library includes two functions that may be useful for timing: @ref swl_datetime_now and @ref swl_datetime_delta.

   <b>swl_datetime_now</b> will get the current time as precisely as possible and store it in the provided swl_datetime_t.<br>
   <b>swl_datetime_delta</b> will return the difference (in nanoseconds) between two datetime structures.<br>
   @warning swl_datetime_now returns the current date according to the system, which can make big jumps due to NTP synchronization or manual setting of the date/time.
   @warning This means that using it for timing carries some risk that a time jump will scew the measurement. i.e.:
   <code>
    swl_datetime_t dt,dt2;
    swl_datetime_now(&dt);
    // expensive operation we want to time
    //                                  <--- NTP sync occurs here
    swl_datetime_now(&dt2);
    uint64_t diff = swl_datetime_delta(&dt1, &dt2);
    printf("The operation took %"PRUInt64 " nanoseconds", diff);    // WRONG!
   </code>
   This will print the wrong time if a NTP sync happened in between the two measurements.
   Applications should sanity-check the result or use clock_gettime(2) instead with the MONOTONIC timer.
   </p>

 */


/**
 * @brief
 * A type to store date and time to nanosecond precision
 *
 * @details
 * This type contains a struct tm for compatibility with old code, which requires a `struct tm` pointer.
 * The nanoseconds part is stored separately.
 */
typedef struct swl_datetime_s {
    struct tm datetime;
    uint32_t nanoseconds;
} swl_datetime_t;

///< The ISO8601 standard format for dates, when the time is not known or irrelevant.
#define SWL_DATETIME_ISO8601_DATE_FMT "%Y-%m-%d"

///< The ISO8601 standard format for date+time
#define SWL_DATETIME_ISO8601_DATE_TIME_FMT "%Y-%m-%dT%H:%M:%SZ"

///< The ISO8601 standard format for date+time, with microseconds precision for the seconds. Note that %i is a non-standard extension.
#define SWL_DATETIME_ISO8601_FMT_US "%Y-%m-%dT%H:%M:%iZ"

#define SWL_DATETIME_STATIC_INITIALIZER {.datetime = {.tm_year = 70, .tm_mday = 1}, .nanoseconds = 0};

#define SWL_DATETIME_UNIX_EPOCH_STR "1970-01-01T00:00:00Z"
#define SWL_DATETIME_TR098_EPOCH_STR "0001-01-01T00:00:00Z"

/**
 * @brief enumeration of supported epochs
 */
typedef enum {
    SWL_DATETIME_EPOCH_UNIX,     /**< Unix time, based on 1970-01-01T00:00:00Z. Converts to (time_t)0. */
    SWL_DATETIME_EPOCH_TR098,    /**< TR-098 time, based on 0001-01-01T00:00:00Z. Cannot be safely converted to time_t. Required by TR-069 */
} swl_datetime_epoch_t;

/**
   @}
 */

// Replacement for strptime. Converts a suitably formatted string to swl_datetime_t.
char* swl_datetime_strptime(const char* input, const char* format, swl_datetime_t* dt);
// Replacement for strftime. Converts a swl_datetime_t to a suitably formatted string.
size_t swl_datetime_strftime(char* s, size_t max, const char* format, const swl_datetime_t* dt);

#define swl_datetime_toIso(dt, buf, max) swl_strftime(buf, max, ISO8601_DATE_TIME_FMT, dt)
#define swl_datetime_toIsoUs(dt, buf, max) swl_strftime(buf, max, ISO8601_DATE_TIME_FMT_US, dt)
#define swl_datetime_toChar(dt, buf, max) swl_strftime(buf, max, (dt)->nanoseconds != 0 ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT, dt)

#define swl_datetime_fromChar(dt, in) swl_strptime(in, ISO8601_DATE_TIME_FMT_US, dt)

void swl_datetime_initialize(swl_datetime_t* dt, swl_datetime_epoch_t epoch);
bool swl_datetime_now(swl_datetime_t* dt);
int64_t swl_datetime_delta(const swl_datetime_t* start, const swl_datetime_t* end);

bool swl_datetime_fromTimespec(swl_datetime_t* dt, const struct timespec* ts);
bool swl_datetime_fromTimeval(swl_datetime_t* dt, const struct timeval* tv);
bool swl_datetime_fromTime(swl_datetime_t* dt, time_t tim);
bool swl_datetime_fromTm(swl_datetime_t* dt, const struct tm* tm);

bool swl_datetime_toTimespec(const swl_datetime_t* dt, struct timespec* ts);
bool swl_datetime_toTimeval(const swl_datetime_t* dt, struct timeval* tv);
bool swl_datetime_toTime(const swl_datetime_t* dt, time_t* tim);
bool swl_datetime_toTm(const swl_datetime_t* dt, struct tm* tm);

struct timespec* swl_datetime_timespec(const swl_datetime_t* dt);
struct timeval* swl_datetime_timeval(const swl_datetime_t* dt);
struct tm* swl_datetime_tm(const swl_datetime_t* dt);
time_t swl_datetime_time(const swl_datetime_t* dt);
uint32_t swl_datetime_nanoseconds(const swl_datetime_t* dt);

const struct tm* swl_datetime_da_tm(const swl_datetime_t* dt);

static inline bool swl_datetime_equals(const swl_datetime_t* first, const swl_datetime_t* second) {
    return swl_datetime_delta(first, second) == 0;
}

#endif
