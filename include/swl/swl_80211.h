/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * Defines for ieee802.11 frame information
 */

#ifndef SRC_INCLUDE_SWL_SWL_80211_H_
#define SRC_INCLUDE_SWL_SWL_80211_H_

#include "swl/swl_common.h"
#include "swl/swl_common_mac.h"
#include <swl/swl_common_table.h>

#define SWL_80211_MGMT_FRAME_HEADER_LEN (offsetof(swl_80211_mgmtFrame_t, data))
#define SWL_80211_MGMT_FRAME_FIXED_IES_LEN (offsetof(swl_80211_assocReqFrameBody_t, data))
#define SWL_80211_MGMT_FRAME_TAGGED_IES_OFFSET (SWL_80211_MGMT_FRAME_HEADER_LEN + SWL_80211_MGMT_FRAME_FIXED_IES_LEN)

#define SWL_80211_EL_ID_SSID 0
#define SWL_80211_EL_ID_SUP_RATES 1
#define SWL_80211_EL_ID_REPORTING_DETAIL 2
#define SWL_80211_EL_ID_DSSS_PARAM_SET 3
#define SWL_80211_EL_ID_CF_PARAM_SET 4
#define SWL_80211_EL_ID_TIM 5
#define SWL_80211_EL_ID_IBSS_PARAM_SET 6
#define SWL_80211_EL_ID_COUNTRY 7
#define SWL_80211_EL_ID_REQUEST 10
#define SWL_80211_EL_ID_BSS_LOAD 11
#define SWL_80211_EL_ID_EDCA_PARAM_SET 12
#define SWL_80211_EL_ID_TSPEC 13
#define SWL_80211_EL_ID_TCLAS 14
#define SWL_80211_EL_ID_SCHEDULE 15
#define SWL_80211_EL_ID_CHALLENGE_TEXT 16
#define SWL_80211_EL_ID_POW_CONSTR 32
#define SWL_80211_EL_ID_POW_CAP 33
#define SWL_80211_EL_ID_TPC_REQ 34
#define SWL_80211_EL_ID_TPC_REP 35
#define SWL_80211_EL_ID_SUP_CHAN 36
#define SWL_80211_EL_ID_CSA 37
#define SWL_80211_EL_ID_MEAS_REQ 38
#define SWL_80211_EL_ID_MEAS_REP 39
#define SWL_80211_EL_ID_QUIET 40
#define SWL_80211_EL_ID_IBSS_DFS 41
#define SWL_80211_EL_ID_ERP 42
#define SWL_80211_EL_ID_TS_DELAY 43
#define SWL_80211_EL_ID_TCLAS_PROCESS 44
#define SWL_80211_EL_ID_HT_CAP 45
#define SWL_80211_EL_ID_QOS_CAPAB 46
#define SWL_80211_EL_ID_RSN 48
#define SWL_80211_EL_ID_EXT_CAP_RATE_BSS_MEMB 50
#define SWL_80211_EL_ID_AP_CHAN_RPRT 51
#define SWL_80211_EL_ID_NEIGH_RPRT 52
#define SWL_80211_EL_ID_RCPI 53
#define SWL_80211_EL_ID_MOB_DOM 54
#define SWL_80211_EL_ID_FBT 55
#define SWL_80211_EL_ID_TIMEOUT_INT 56
#define SWL_80211_EL_ID_RIC_DATA 57
#define SWL_80211_EL_ID_DSE_REG_LOC 58
#define SWL_80211_EL_ID_SUP_OP_CLASS 59
#define SWL_80211_EL_ID_EXT_CAPCSA 60
#define SWL_80211_EL_ID_HT_OP 61
#define SWL_80211_EL_ID_SEC_CHAN_OFFSET 62
#define SWL_80211_EL_ID_BSS_AVG_DELAY 63
#define SWL_80211_EL_ID_ANTENNA 64
#define SWL_80211_EL_ID_RSNI 65
#define SWL_80211_EL_ID_MEAS_PILOT_TRANS 66
#define SWL_80211_EL_ID_BSS_AVAIL_ADM_CAP 67
#define SWL_80211_EL_ID_BSS_AC_ACCES_DELAY 68
#define SWL_80211_EL_ID_TIME_ADV 69
#define SWL_80211_EL_ID_RM_ENAB_CAP 70
#define SWL_80211_EL_ID_MBSSID 71
#define SWL_80211_EL_ID_20_40_BSS_COEX 72
#define SWL_80211_EL_ID_20_40_BSS_INTO_CHAN_RPRT 73
#define SWL_80211_EL_ID_OVERLAP_BSS_SCAN_PARAM 74
#define SWL_80211_EL_ID_RIC_DESC 75
#define SWL_80211_EL_ID_MGMT_MIC 76
#define SWL_80211_EL_ID_EV_REQ 78
#define SWL_80211_EL_ID_EV_REP 79
#define SWL_80211_EL_ID_DIAG_REQ 80
#define SWL_80211_EL_ID_DIAG_REP 81
#define SWL_80211_EL_ID_LOC_PARAM 82
#define SWL_80211_EL_ID_NON_TRANS_BSS_CAP 83
#define SWL_80211_EL_ID_SSID_LIST 84
#define SWL_80211_EL_ID_MBSSID_IDX 85
#define SWL_80211_EL_ID_FMS_DESC 86
#define SWL_80211_EL_ID_FMS_REQ 87
#define SWL_80211_EL_ID_FMS_REP 88
#define SWL_80211_EL_ID_QOS_CAP 89
#define SWL_80211_EL_ID_BSS_MAX_IDLE_PERIOD 90
#define SWL_80211_EL_ID_TFS_REQ 91
#define SWL_80211_EL_ID_TFS_REP 92
#define SWL_80211_EL_ID_WNM_SLEEP_MODE 93
#define SWL_80211_EL_ID_TIM_BC_REQ 94
#define SWL_80211_EL_ID_TIM_BC_REP 95
#define SWL_80211_EL_ID_COLL_INTF_REP 96
#define SWL_80211_EL_ID_CHAN_USAGE 97
#define SWL_80211_EL_ID_TIME_ZONE 98
#define SWL_80211_EL_ID_DMS_REQ 99
#define SWL_80211_EL_ID_DMS_REP 100
#define SWL_80211_EL_ID_LINK_ID 101
#define SWL_80211_EL_ID_WAKEUP_SCHED 102
#define SWL_80211_EL_ID_CS_TIMING 104
#define SWL_80211_EL_ID_PTI_CONTROL 105
#define SWL_80211_EL_ID_TPU_BUF_STATUS 106
#define SWL_80211_EL_ID_INTERWORKING 107
#define SWL_80211_EL_ID_ADV_PROT 108
#define SWL_80211_EL_ID_EXP_BW_REQ 109
#define SWL_80211_EL_ID_QOS_MAP 110
#define SWL_80211_EL_ID_ROAM_CONS 111
#define SWL_80211_EL_ID_EMERGENCY_ALERT_ID 112
#define SWL_80211_EL_ID_MESH_CONFIG 113
#define SWL_80211_EL_ID_MESH_ID 114
#define SWL_80211_EL_ID_MESH_LINK_METRIC_REP 115
#define SWL_80211_EL_ID_CONGESTION_NOT 116
#define SWL_80211_EL_ID_MESH_PEERING_MGMT 117
#define SWL_80211_EL_ID_MESH_CS_PARAM 118
#define SWL_80211_EL_ID_MESH_AWAKE_WINDOW 119
#define SWL_80211_EL_ID_BEACON_TIMING 120
#define SWL_80211_EL_ID_MCCAOP_SETUP_REQ 121
#define SWL_80211_EL_ID_MCCAOP_SETUP_REP 122
#define SWL_80211_EL_ID_MCCAOP_ADV 123
#define SWL_80211_EL_ID_MCCAOP_TEARDOM 124
#define SWL_80211_EL_ID_GANN 125
#define SWL_80211_EL_ID_RANN 126
#define SWL_80211_EL_ID_EXT_CAP 127
#define SWL_80211_EL_ID_PREQ 130
#define SWL_80211_EL_ID_PREP 131
#define SWL_80211_EL_ID_PERR 132
#define SWL_80211_EL_ID_PXU 137
#define SWL_80211_EL_ID_PXUC 138
#define SWL_80211_EL_ID_AUTH_MESH_PEERING_EXC 139
#define SWL_80211_EL_ID_MIC 140
#define SWL_80211_EL_ID_DEST_URI 141
#define SWL_80211_EL_ID_UAPSD_COEX 142
#define SWL_80211_EL_ID_DMG_WAKEUP_SCHED 143
#define SWL_80211_EL_ID_EXT_CAPSCHED 144
#define SWL_80211_EL_ID_STA_AVAIL 145
#define SWL_80211_EL_ID_DMG_TSPEC 146
#define SWL_80211_EL_ID_NEXT_DMG_ATI 147
#define SWL_80211_EL_ID_DMG_CAP 148
#define SWL_80211_EL_ID_DMG_OP 151
#define SWL_80211_EL_ID_BSS_PARAM_CHANGE 152
#define SWL_80211_EL_ID_BEAM_REFINE 153
#define SWL_80211_EL_ID_CHAN_MEAS_FB 154
#define SWL_80211_EL_ID_AWAKE_WINDOW 157
#define SWL_80211_EL_ID_MULTI_BAND 158
#define SWL_80211_EL_ID_ADDBA_EXT 159
#define SWL_80211_EL_ID_NEXT_PCP_LIST 160
#define SWL_80211_EL_ID_PCP_HANDOVER 161
#define SWL_80211_EL_ID_DMG_LINK_MARGIN 162
#define SWL_80211_EL_ID_SWITCHING_STREAM 163
#define SWL_80211_EL_ID_SESSION_TRANSITION 164
#define SWL_80211_EL_ID_DYN_TOME_PAIRING_REP 165
#define SWL_80211_EL_ID_CLUSTER_REP 166
#define SWL_80211_EL_ID_RElAY_CAP 167
#define SWL_80211_EL_ID_RELAY_TRANSFER_PARAM_SET 168
#define SWL_80211_EL_ID_BEAMLINK_MAINTENANCE 169
#define SWL_80211_EL_ID_MULTI_MAC_SUBLAYERS 170
#define SWL_80211_EL_ID_U_PID 171
#define SWL_80211_EL_ID_DMG_LINK_ADP_ACK 172
#define SWL_80211_EL_ID_MCCAOP_ADV_OVERVIEW 174
#define SWL_80211_EL_ID_QUIET_PERIOD_REQ 175
#define SWL_80211_EL_ID_QUIET_PERIOD_REP 177
#define SWL_80211_EL_ID_QMF_POL 181
#define SWL_80211_EL_ID_ECAPC POL 182
#define SWL_80211_EL_ID_CLUSTER_TIME_OFFSET 183
#define SWL_80211_EL_ID_INTRA_ACCESS_CAP_PRIO 184
#define SWL_80211_EL_ID_SCS_DESC 185
#define SWL_80211_EL_ID_QLOAD_REP 186
#define SWL_80211_EL_ID_HCCA_TXOP_UPDATE_COUNT 187
#define SWL_80211_EL_ID_HIGHER_LAYER_STREAM_ID 188
#define SWL_80211_EL_ID_GCR_GROUP_ADDR 189
#define SWL_80211_EL_ID_ANT_SECTOR_ID_PATTERN 190
#define SWL_80211_EL_ID_VHT_CAP 191
#define SWL_80211_EL_ID_VHT_OP 192
#define SWL_80211_EL_ID_EXT_CAPBSS_LOAD 193
#define SWL_80211_EL_ID_WIDE_BW_CS 194
#define SWL_80211_EL_ID_TRANSMIT_POW_ENVELOPE 195
#define SWL_80211_EL_ID_CS_WRAPPER 196
#define SWL_80211_EL_ID_AID 197
#define SWL_80211_EL_ID_QUIET_CHANNEL 198
#define SWL_80211_EL_ID_OP_MODE_NOT 199
#define SWL_80211_EL_ID_UPSIM 200
#define SWL_80211_EL_ID_TVHT_OP 202
#define SWL_80211_EL_ID_DEV_LOC 204
#define SWL_80211_EL_ID_WHITE_SPACE_MAP 205
#define SWL_80211_EL_ID_FIME_TIMEING_MEAS_PARAM 206
#define SWL_80211_EL_ID_VENDOR_SPECIFIC 221
#define SWL_80211_EL_ID_EXT 255
typedef uint8_t swl_80211_elId_ne;

#define SWL_80211_EL_IDEXT_FTM_SYNCRO_INFO 9
#define SWL_80211_EL_IDEXT_EXT_REQ 10
#define SWL_80211_EL_IDEXT_EST_SERVICE_PARAM 11
#define SWL_80211_EL_IDEXT_FUTURE_CHANNEL_GUIDANCE 14
#define SWL_80211_EL_IDEXT_HE_CAP 35
#define SWL_80211_EL_IDEXT_HE_OP 36
#define SWL_80211_EL_IDEXT_UORA_PARAM_SET_EL 37
#define SWL_80211_EL_IDEXT_SPATIAL_REUSE_SET_EL 39
#define SWL_80211_EL_IDEXT_NDP_FEEDBACK_REP_PARAM 41
#define SWL_80211_EL_IDEXT_BSS_COLOR_CHANGE_ANNOUNCE 42
#define SWL_80211_EL_IDEXT_QUIET_TIME_PERIOD 43
#define SWL_80211_EL_IDEXT_ESS_REPORT 45
#define SWL_80211_EL_IDEXT_OPS 46
#define SWL_80211_EL_IDEXT_HE_BSS_LOAD 47
#define SWL_80211_EL_IDEXT_MBSSID_CONFIG 55
#define SWL_80211_EL_IDEXT_KNOWN_BSSID 57
#define SWL_80211_EL_IDEXT_SHORT_SSID_LIST 58
#define SWL_80211_EL_IDEXT_HE_6GHZ_BAND_CAP 59
#define SWL_80211_EL_IDEXT_UL_MU_POW_CAP 60
#define SWL_80211_EL_IDEXT_EHT_OPER 106
#define SWL_80211_EL_IDEXT_EHT_ML 107
#define SWL_80211_EL_IDEXT_EHT_CAP 108
#define SWL_80211_EL_IDEXT_EHT_MLO_INFO 133
typedef uint8_t swl_80211_elIdExt_ne;

#define SWL_80211_MCS_HT_NSS_SET_LEN 4
#define SWL_80211_MCS_HT_NSS_SET_NUM_BITS SWL_80211_MCS_HT_NSS_SET_LEN*8
#define SWL_80211_MCS_VHT_NSS_SET_LEN 8
#define SWL_80211_MCS_SET_LEN 16
#define SWL_80211_MCS_MAX_NUM 128

#define M_SWL_80211_HTCAPINFO_LDPC 0x0001
#define M_SWL_80211_HTCAPINFO_CAP_40 0x0002
#define M_SWL_80211_HTCAPINFO_POW_SAVE 0x000c
#define M_SWL_80211_HTCAPINFO_GREENFEELD 0x0010
#define M_SWL_80211_HTCAPINFO_SGI_20 0x0020
#define M_SWL_80211_HTCAPINFO_SGI_40 0x0040
#define M_SWL_80211_HTCAPINFO_TX_STBC 0x0080
#define M_SWL_80211_HTCAPINFO_RX_STBC 0x0300
#define M_SWL_80211_HTCAPINFO_HT_DELAY_BA 0x0400
#define M_SWL_80211_HTCAPINFO_MAX_AMSDU 0x0800
#define M_SWL_80211_HTCAPINFO_MODE_40 0x1000
#define M_SWL_80211_HTCAPINFO_INTOL_40 0x4000
#define M_SWL_80211_HTCAPINFO_LSIG_PROT 0x8000
typedef uint16_t swl_80211_htCapInfo_m;
extern swl_table_t swl_80211_htCapInfoStrTable;

extern const char* swl_multiApType_str[];

typedef enum {
    SWL_MULTIAP_TYPE_DEFAULT,
    SWL_MULTIAP_TYPE_FRONTHAUL_BSS,
    SWL_MULTIAP_TYPE_BACKHAUL_BSS,
    SWL_MULTIAP_TYPE_BACKHAUL_STA,
    SWL_MULTIAP_TYPE_MAX
} swl_multiApType_e;

typedef uint32_t swl_multiApType_m;

#define M_SWL_MULTIAP_TYPE_DEFAULT       (1 << SWL_MULTIAP_TYPE_DEFAULT)
#define M_SWL_MULTIAP_TYPE_FRONTHAUL_BSS (1 << SWL_MULTIAP_TYPE_FRONTHAUL_BSS)
#define M_SWL_MULTIAP_TYPE_BACKHAUL_BSS  (1 << SWL_MULTIAP_TYPE_BACKHAUL_BSS)
#define M_SWL_MULTIAP_TYPE_BACKHAUL_STA  (1 << SWL_MULTIAP_TYPE_BACKHAUL_STA)
#define M_SWL_MULTIAP_TYPE_ALL           ((1 << SWL_MULTIAP_TYPE_MAX) - 1)

/* WFA Vendor Extension subelements */
typedef enum {
    SWL_80211_WFA_ELEMENT_VERSION2 = 0x00,
    SWL_80211_WFA_ELEMENT_AUTHORIZEDMACS = 0x01,
    SWL_80211_WFA_ELEMENT_NETWORK_KEY_SHAREABLE = 0x02,
    SWL_80211_WFA_ELEMENT_REQUEST_TO_ENROLL = 0x03,
    SWL_80211_WFA_ELEMENT_SETTINGS_DELAY_TIME = 0x04,
    SWL_80211_WFA_ELEMENT_REGISTRAR_CONFIGURATION_METHODS = 0x05,
    SWL_80211_WFA_ELEMENT_MULTI_AP = 0x06,
    SWL_80211_WFA_ELEMENT_MAX = 0x07
} swl_80211_wfaElement_e;

/* WFA MBO v0.0_r19, 4.2.6: Transition Reason Code Attribute */
/* Table 4-19: Transition Reason Code Field Values */
typedef enum SWL_PACKED {
    SWL_80211_WFA_MBO_TRANSITION_REASON_UNSPECIFIED = 0,
    SWL_80211_WFA_MBO_TRANSITION_REASON_FRAME_LOSS = 1,
    SWL_80211_WFA_MBO_TRANSITION_REASON_DELAY = 2,
    SWL_80211_WFA_MBO_TRANSITION_REASON_BANDWIDTH = 3,
    SWL_80211_WFA_MBO_TRANSITION_REASON_LOAD_BALANCE = 4,
    SWL_80211_WFA_MBO_TRANSITION_REASON_RSSI = 5,
    SWL_80211_WFA_MBO_TRANSITION_REASON_RETRANSMISSIONS = 6,
    SWL_80211_WFA_MBO_TRANSITION_REASON_INTERFERENCE = 7,
    SWL_80211_WFA_MBO_TRANSITION_REASON_GRAY_ZONE = 8,
    SWL_80211_WFA_MBO_TRANSITION_REASON_PREMIUM_AP = 9,
    SWL_80211_WFA_MBO_TRANSITION_REASON_MAX
} swl_wfaMbo_transitionReason_e;

/*
 * 802.11 management frame control sub header
 */
typedef struct SWL_PACKED {
    uint16_t version : 2;
    uint16_t type : 2;
    uint16_t subType : 4;
    uint16_t flags : 8;
} swl_80211_mgmtFrameControl_t;

/*
 * 802.11 management frame
 */
typedef struct SWL_PACKED {
    swl_80211_mgmtFrameControl_t fc;
    uint16_t duration;
    swl_macBin_t destination;
    swl_macBin_t transmitter;
    swl_macBin_t bssid;
    uint16_t seqCtrl;
    swl_bit8_t data[1];
} swl_80211_mgmtFrame_t;

/*
 * 802.11 association request frame body
 */
typedef struct SWL_PACKED {
    uint16_t capInfo;
    uint16_t listenInterval;
    swl_bit8_t data[1];
} swl_80211_assocReqFrameBody_t;

/*
 * 802.11 re-association request frame body
 */
typedef struct SWL_PACKED {
    uint16_t capInfo;
    uint16_t listenInterval;
    swl_macBin_t currentAp;
    swl_bit8_t data[1];
} swl_80211_reassocReqFrameBody_t;

/*
 * 802.11 probe request frame body
 */
typedef struct SWL_PACKED {
    swl_bit8_t data[1];
} swl_80211_probeReqFrameBody_t;

/*
 * 802.11 probe response frame body
 */
typedef struct SWL_PACKED {
    uint64_t timestamp;
    uint16_t beaconInterval;
    uint16_t capInfo;
    swl_bit8_t data[1];
} swl_80211_probeRespFrameBody_t;

/*
 * 802.11 beacon frame body
 */
typedef struct SWL_PACKED {
    uint64_t timestamp;
    uint16_t beaconInterval;
    uint16_t capInfo;
    swl_bit8_t data[1];
} swl_80211_beaconFrameBody_t;

/*
 * 802.11 BSS Load Information Element
 */
typedef struct SWL_PACKED {
    uint16_t stationCount;
    uint8_t channelUtilization;
    uint16_t availAdmissionCapacity;
} swl_80211_bssLoadIE_t;

typedef struct SWL_PACKED {
    uint16_t reason;
    swl_bit8_t data[1];
} swl_80211_deauthFrameBody_t, swl_80211_disassocFrameBody_t;

/*
 * 802.11ac HT Capabilities Elements
 */
typedef struct SWL_PACKED {
    swl_80211_htCapInfo_m htCapInfo;
    uint8_t ampduParam;
    uint8_t supMCSSet[SWL_80211_MCS_SET_LEN];
    uint16_t htExtCap;
    uint32_t txBfCap;
    uint8_t ASELCap;
} swl_80211_htCapIE_t;

#define SWL_80211_HT_OP_IE_BASIC_HT_MCS_SET_SIZE 16
typedef uint8_t swl_80211_htOpIE_basicHtMcsSet_t[SWL_80211_HT_OP_IE_BASIC_HT_MCS_SET_SIZE];

/**
 * Channel offset and width values as defined in Table 9-190 - HT Operation element fields and subfields
 */
typedef enum {
    SWL_80211_HT_OPER_SEC_CHAN_NONE = 0,
    SWL_80211_HT_OPER_SEC_CHAN_ABOVE = 1,
    SWL_80211_HT_OPER_SEC_CHAN_RESERVED = 2,
    SWL_80211_HT_OPER_SEC_CHAN_BELOW = 3,
} swl_80211_htOperInfo_secChanOffset_ne;

typedef enum {
    SWL_80211_HT_OPER_STA_CHAN_WIDTH_20_ONLY = 0,
    SWL_80211_HT_OPER_STA_CHAN_WIDTH_ANY = 1,
} swl_80211_htOperInfo_staChanWidth_ne;

/**
 * HT Operation IE (sec 9.4.2.56)
 */
typedef struct SWL_PACKED {
    uint8_t primChan;
    uint8_t secChanOffset : 2;
    uint8_t staChanWidth : 1;
    uint8_t rifsMode : 1;
    uint8_t res_b4_b7 : 4;
    uint16_t htInfoSubset2;
    uint16_t htInfoSubset3;
    swl_80211_htOpIE_basicHtMcsSet_t basicHtMcsSet;
} swl_80211_htOpIE_t;

/**
 * Channel Width values as defined in Table 9-274-VHT Operation Information subfields
 */
typedef enum {
    SWL_80211_VHT_OPER_CHAN_WIDTH_20_40 = 0,
    SWL_80211_VHT_OPER_CHAN_WIDTH_80_160_8080 = 1,
    SWL_80211_VHT_OPER_CHAN_WIDTH_160 = 2,
    SWL_80211_VHT_OPER_CHAN_WIDTH_8080 = 3,
    SWL_80211_VHT_OPER_CHAN_WIDTH_MAX = 4, //4-255 are reserved
} swl_80211_vhtOperInfo_chanWidth_ne;

/**
 * VHT Operation IE (sec 8.4.2.161)
 */
typedef struct SWL_PACKED {
    uint8_t chanWidth;       //together with the HT Operation element STA Channel Width field, defines the BSS bandwidth
    uint8_t chanCenterSeg0;  //channel center frequency index of the segment containing the primary channel
                             //for bandwidths 20, 40, 80, 160, or 80+80 MHz
    uint8_t chanCenterSeg1;  //channel center frequency index for a 160 or 80+80 MHz VHT BSS
                             //(primary 160 or secondary 80 in 80+80)
    uint16_t suppMcs;        // Basic VHT-MCS And NSS Set
} swl_80211_vhtOpIE_t;


typedef enum {
    SWL_80211_HT_MCSSET_20_40_NSS1,             /* CBW = 20/40 MHz, Nss = 1, Nes = 1, EQM/ No EQM */
    SWL_80211_HT_MCSSET_20_40_NSS2,             /* CBW = 20/40 MHz, Nss = 2, Nes = 1, EQM */
    SWL_80211_HT_MCSSET_20_40_NSS3,             /* CBW = 20/40 MHz, Nss = 3, Nes = 1, EQM */
    SWL_80211_HT_MCSSET_20_40_NSS4,             /* CBW = 20/40 MHz, Nss = 4, Nes = 1, EQM */
    SWL_80211_HT_MCSSET_20_40_UEQM1,            /* MCS 32 and UEQM MCSs 33 - 39 */
    SWL_80211_HT_MCSSET_20_40_UEQM2,            /* UEQM MCSs 40 - 47 */
    SWL_80211_HT_MCSSET_20_40_UEQM3,            /* UEQM MCSs 48 - 55 */
    SWL_80211_HT_MCSSET_20_40_UEQM4,            /* UEQM MCSs 56 - 63 */
    SWL_80211_HT_MCSSET_20_40_UEQM5,            /* UEQM MCSs 64 - 71 */
    SWL_80211_HT_MCSSET_20_40_UEQM6,            /* UEQM MCSs 72 - 76 plus 3 reserved bits */
    SWL_80211_HT_MCSSET_20_40_MAX
} swl_80211_htMcsSet_e;

#define M_SWL_80211_VHTCAPINFO_MAX_MPDU 0x00000003
#define M_SWL_80211_VHTCAPINFO_SUP_WIDTH 0x0000000c
#define M_SWL_80211_VHTCAPINFO_RX_LDPC 0x00000010
#define M_SWL_80211_VHTCAPINFO_SGI_80 0x00000020
#define M_SWL_80211_VHTCAPINFO_SGI_160 0x00000040
#define M_SWL_80211_VHTCAPINFO_TX_STBC 0x00000080
#define M_SWL_80211_VHTCAPINFO_RX_STBC 0x00000700
#define M_SWL_80211_VHTCAPINFO_SU_BFR 0x00000800
#define M_SWL_80211_VHTCAPINFO_SU_BFE 0x00001000
#define M_SWL_80211_VHTCAPINFO_BFE_STS_CAP 0x0000e000
#define M_SWL_80211_VHTCAPINFO_NR_SDIM 0x00070000
#define M_SWL_80211_VHTCAPINFO_MU_BFR 0x00080000
#define M_SWL_80211_VHTCAPINFO_MU_BFE 0x00100000
#define M_SWL_80211_VHTCAPINFO_TXOP_PS 0x00200000
#define M_SWL_80211_VHTCAPINFO_HTC_CAP 0x00400000
#define M_SWL_80211_VHTCAPINFO_MAX_AMPDU_EXP 0x03800000
#define M_SWL_80211_VHTCAPINFO_LINK_ADAPT_CAP 0x0b000000
#define M_SWL_80211_VHTCAPINFO_RX_ANT_PAT_CONS 0x10000000
#define M_SWL_80211_VHTCAPINFO_TX_ANT_PAT_CONS 0x20000000
#define M_SWL_80211_VHTCAPINFO_EXT_NSS_SUP 0xb0000000
typedef uint32_t swl_80211_vhtCapInfo_m;
extern swl_table_t swl_80211_vhtCapInfoStrTable;


/*
 * 802.11 VHT Capabilities Elements
 */
typedef struct SWL_PACKED {
    swl_80211_vhtCapInfo_m vhtCapInfo;
    /* supported MCS set - 64 bit field */
    uint16_t rxMcsMap;
    uint16_t rxRateMap;
    uint16_t txMcsMap;
    uint16_t txRateMap;
} swl_80211_vhtCapIE_t;

#define SWL_80211_VHT_CAP_INFO_SIZE       4
#define SWL_80211_VHT_MCS_NSS_SIZE        8
#define SWL_80211_VHT_MCS_OFFSET          2 + SWL_80211_VHT_CAP_INFO_SIZE
#define SWL_80211_HE_OP_IE_PARAMS_SIZE       3
typedef uint8_t swl_80211_heOpIE_params_t[SWL_80211_HE_OP_IE_PARAMS_SIZE];

typedef struct SWL_PACKED {
    swl_80211_heOpIE_params_t params;
    uint8_t color;
    uint16_t basicMcsNssSet;
} swl_80211_heOpIE_t;

#define SWL_80211_HEOP_MINLEN (sizeof(swl_80211_heOpIE_t))
#define SWL_80211_HEOP_MAXLEN (sizeof(swl_80211_heOpIE_t) + 3 + 1)
#define SWL_80211_HEOP_ELEMID_EXT 36

#define SWL_80211_HECAP_MAC_CAP_INFO_SIZE 6
typedef struct SWL_PACKED {
    uint8_t cap[SWL_80211_HECAP_MAC_CAP_INFO_SIZE];
} swl_80211_hecap_macCapInfo_t;

#define SWL_80211_HECAP_PHY_CAP_INFO_SIZE 11
typedef struct SWL_PACKED {
    uint8_t cap[SWL_80211_HECAP_PHY_CAP_INFO_SIZE];
} swl_80211_hecap_phyCapInfo_t;

#define M_SWL_80211_HE_PHY_B0 0x00000001
#define M_SWL_80211_HE_PHY_40MHZ_2_4GHZ 0x00000002
#define M_SWL_80211_HE_PHY_40_80MHZ_5GHZ 0x00000004
#define M_SWL_80211_HE_PHY_160MHZ_5GHZ 0x00000008
#define M_SWL_80211_HE_PHY_160_80_80_MHZ_5GHZ 0x0000010
#define M_SWL_80211_HE_PHY_SU_BEAMFORMER 0x40000000
#define M_SWL_80211_HE_PHY_SU_BEAMFORMEE 0x80000000
#define M_SWL_80211_HE_PHY_MU_BEAMFORMEE 0x100000000
#define M_SWL_80211_HE_PHY_BEAMFORMEE_STS_LE_80MHZ 0xE00000000
#define M_SWL_80211_HE_PHY_BEAMFORMEE_STD_GT_80MHZ 0x7000000000
typedef uint64_t swl_80211_hePhyCapInfo_m;
extern swl_table_t swl_80211_heCapPhysStrTable;

#define SWL_80211_HECAP_PHY_CAP_INFO_INDEX 7

typedef struct SWL_PACKED {
    uint16_t rxMcsMap;
    uint16_t txMcsMap;
} swl_80211_hecap_MCSCap_t;

#define SWL_80211_HECAP_MCS_CAP_ARRAY_SIZE 3   // 80 Mhz, 160 and 80+80
/*
 * 802.11 HE Capabilities Elements
 */
typedef uint8_t swl_80211_heCapInfo_m;

typedef struct SWL_PACKED {
    swl_80211_heCapInfo_m heCaps;
    swl_80211_hecap_macCapInfo_t macCap;       /* MAC Capabilities Information */
    swl_80211_hecap_phyCapInfo_t phyCap;       /* PHY Capabilities Information */
    /* Supported HE-MCS And NSS Set (4, 8 or 12 bytes) */
    swl_80211_hecap_MCSCap_t mcsCaps[SWL_80211_HECAP_MCS_CAP_ARRAY_SIZE];
    /* PPE Thresholds (optional) (variable length) */
} swl_80211_heCapIE_t;

#define SWL_80211_HECAP_MINLEN (sizeof(swl_80211_heCapIE_t) + 4)
//for size description, please see IEEE802.11 - 2021: 9.4.2.248 HE Capabilities element
// Max supported MCS + NSS Set => 12 bytes
// PPE Threshold info max size = 8 * 4 * 6 bits => divide by 8
// 1 extra byte for NSTS + RU Index bitmask (technically 7 bits)
#define SWL_80211_HECAP_MAXLEN (sizeof(swl_80211_heCapIE_t) + 12 + 1 + (8 * 4 * 6 ) / 8 )
#define SWL_80211_HECAP_ELEMID_EXT 35

#define SWL_80211_HECAP_MCS_LT80_ELM 0     // indexof the first (LT80) MCS set in the frame's list
#define SWL_80211_HECAP_MCS_160_ELM 1      // index of the second (160) MCS set in the frame's list
#define SWL_80211_HECAP_MCS_80P80_ELM 2

typedef struct SWL_PACKED {
    swl_bit16_t epcs_priority_access_support : 1,
        om_control_support : 1,
        triggered_txop_sharing_mode1_support : 1,
        triggered_txop_sharing_mode2_support : 1,
        restricted_twt_support : 1,
        scs_traffic_descr_support : 1,
        max_mpdu_len : 2,
        max_ampdu_len_exp_ext : 1,
        trs_support : 1,
        txop_return_sharing_mode2_support : 1,
        bqr_x2_support : 1,
        link_adaptation_support : 2;
} swl_80211_ehtcap_macCapInfo_t;

typedef struct SWL_PACKED {
    swl_bit32_t reserved : 1, /* reserved */
        support_320mhz : 1,
        support_242tone_ru_bw_wider_20mhz : 1,
        ndp_4xeht_ltf_3_2ms_gi : 1,
        partial_bw_ulmumimo : 1,
        su_beamformer : 1,
        su_beamformee : 1,
        beamformee_ss_80mhz : 3,
        beamformee_ss_160mhz : 3,
        beamformee_ss_320mhz : 3,
        nb_sounding_80mhz : 3,
        nb_sounding_160mhz : 3,
        nb_sounding_320mhz : 3,
        ng16_su_feedback : 1,
        ng16_mu_feedback : 1,
        codebook_su_feedback : 1,
        codebook_mu_feedback : 1,
        triggered_su_beamforming_feedback : 1,
        triggered_mu_beamforming_partial_bw_feedback : 1,
        triggered_cqi_feedback : 1;
    swl_bit32_t partial_bw_dlmumimo : 1,
        psr_based_sr_support : 1,
        power_boost_factor_support : 1,
        muppdu_4xeht_ltf_0_8ms_gi : 1,
        max_nc : 4,
        non_triggered_cqi_feedback : 1,
        support_tx_1024_4096_qam_242_tone_ru : 1,
        support_rx_1024_4096_qam_242_tone_ru : 1,
        ppe_thresholds_present : 1,
        common_nominal_packet_padding : 2,
        maximum_nb_supported_eht_ltfs : 5,
        support_eht_mcs15_mru : 4,
        support_ehtdup_mcs14_6ghz : 1,
        support_20mhz_oper_sta_rcv_ndp_wider_bw : 1,
        non_ofdma_ulmimo_80mhz : 1,
        non_ofdma_ulmimo_160mhz : 1,
        non_ofdma_ulmimo_320mhz : 1,
        mu_beamformer_80mhz : 1,
        mu_beamformer_160mhz : 1,
        mu_beamformer_320mhz : 1,
        tb_sounding_feedback_rate_limit : 1;
    swl_bit8_t rx_1024qam_wider_bw_dl_ofdma_support : 1,
        rx_4096qam_wider_bw_dl_ofdma_support : 1,
        support_20mhz_only_limited_cap : 1,
        support_20mhz_only_triggered_mu_beamforming_full_bw_feedback_dl_mumimo : 1,
        support_20mhz_only_mru : 1;
} swl_80211_ehtcap_phyCapInfo_t;

typedef struct SWL_PACKED {
    swl_bit8_t tag;                          /* Tag of the IE (108) */
    swl_80211_ehtcap_macCapInfo_t macCap;    /* MAC Capabilities Information */
    swl_80211_ehtcap_phyCapInfo_t phyCap;    /* PHY Capabilities Information */
} swl_80211_ehtCapIE_t;

typedef struct SWL_PACKED {
    swl_bit16_t type : 3,
        reserved : 1,
        linkIdInfo : 1,
        bssParametersChangeCount : 1,
        mediumSynchronizationDelayInformation : 1,
        emlCapabilities : 1,
        mldCapabilitiesAndOperations : 1,
        apMldId : 1,
        extendedMLDCapabilitiesAndOperations : 1;
} swl_80211_ehtBasicMlCtrlIE_t;

typedef struct SWL_PACKED {
    swl_bit16_t type : 3,
        reserved : 1,
        apMldId : 1;
} swl_80211_ehtProbeMlCtrlIE_t;

typedef struct SWL_PACKED {
    swl_bit16_t type : 3,
        reserved : 1,
        mldMacAddress : 1,
        emlCapabilities : 1,
        mldCapabilitiesAndOperations : 1;
} swl_80211_ehtReconfMlCtrlIE_t;

typedef union SWL_PACKED {
    swl_bit16_t type : 3;                   /*!< used to differentiate the variants of the Multi-Link element */
    swl_80211_ehtBasicMlCtrlIE_t basic;
    swl_80211_ehtProbeMlCtrlIE_t probe;
    swl_80211_ehtReconfMlCtrlIE_t reconf;
} swl_80211_ehtMlCtrlIE_t;

#define SWL_80211_EHT_MLCTRL_BASIC_TYPE 0x0
#define SWL_80211_EHT_MLCTRL_PROBE_TYPE 0x1
#define SWL_80211_EHT_MLCTRL_RECONF_TYPE 0x2
#define SWL_80211_EHT_MLCTRL_TDLS_TYPE 0x3
#define SWL_80211_EHT_MLCTRL_EPCS_TYPE 0x4

typedef struct SWL_PACKED {
    swl_bit32_t emlsrSupport : 1,
        emlsrEmlmrPaddingDelay : 3,
        emlsrEmlmrTransitionDelay : 3,
        emlmrSupport : 1,
        reserved : 3,
        transitionTimeout : 4;
} swl_80211_ehtEmlCapabilitiesIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t subElemId;
    swl_bit8_t length;
    swl_bit8_t data[0];
} swl_80211_ehtMlLinkInfoIE_t;

typedef struct SWL_PACKED {
    swl_bit16_t linkId : 4,
        completeProfile : 1,
        staMacAddress : 1,
        beaconInterval : 1,
        tsfOffset : 1,
        dtimInfo : 1,
        nstrLinkPair : 1,
        nstrBitmap : 1,
        bssParameterChangeCount : 1;
} swl_80211_ehtMlStaControlIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    swl_bit8_t data[0];
} swl_80211_ehtMlStaInfoIE_t;

typedef struct SWL_PACKED {
    swl_80211_ehtMlStaControlIE_t staCtrl;
    swl_80211_ehtMlStaInfoIE_t staInfo;
} swl_80211_ehtMlPerStaProfileIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    swl_macBin_t mldMacAddress;
    /* other fields depends on MlCtrl type */
    swl_bit8_t data[0];
} swl_80211_ehtBasicMlInfoIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    /* other fields depends on MlCtrl type */
    swl_bit8_t data[0];
} swl_80211_ehtProbeMlInfoIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    /* other fields depends on MlCtrl type */
    swl_bit8_t data[0];
} swl_80211_ehtReconfMlInfoIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    swl_macBin_t mldMacAddress;
    swl_80211_ehtMlLinkInfoIE_t linkInfo[0];
} swl_80211_ehtTdlsMlInfoIE_t;

typedef struct SWL_PACKED {
    swl_bit8_t length;
    swl_macBin_t mldMacAddress;
    swl_80211_ehtMlLinkInfoIE_t linkInfo[0];
} swl_80211_ehtEpcsMlInfoIE_t;

typedef union SWL_PACKED {
    swl_bit8_t length;
    swl_80211_ehtBasicMlInfoIE_t basic;
    swl_80211_ehtProbeMlInfoIE_t probe;
    swl_80211_ehtReconfMlInfoIE_t reconf;
    swl_80211_ehtTdlsMlInfoIE_t tdls;
    swl_80211_ehtEpcsMlInfoIE_t epcs;
} swl_80211_ehtMlCommonInfoIE_t;

/* IEEE P802.11be/D5.01, March 2024
 * 9.4.2.321 Multi-Link element
 */
typedef struct SWL_PACKED {
    swl_bit8_t tag;                               /*!< Tag of the IE (107) */
    swl_80211_ehtMlCtrlIE_t mlCtrl;               /*!< Multi-Link Control */
    swl_80211_ehtMlCommonInfoIE_t mlCommonInfo;   /*!< Common Info */
    /* Link Info will follow if any (Per-STA Profile) */
} swl_80211_ehtMlIE_t;

#define SWL_80211_MOB_DOM_CAP_FBSS_TRANS_DS 0x01
#define SWL_80211_MOB_DOM_CAP_RESOURCE_REQ_PROT_CAP 0x02

typedef struct SWL_PACKED {
    uint16_t MDID;
    uint8_t cap;
} swl_80211_mobDomEl_t;

typedef struct SWL_PACKED {
    uint8_t oui[3];
    uint8_t data[1]; // More data possible, depends on len.
} swl_80211_vendorIdEl_t;

#define SWL_80211_TBTT_LEN_OFFSET         (1)
#define SWL_80211_TBTT_LEN_SHORT_SSID     (4)
#define SWL_80211_TBTT_LEN_BSSID          (6)
#define SWL_80211_TBTT_LEN_BSS_PARAMS     (1)
#define SWL_80211_TBTT_LEN_PSD            (1)

#define SWL_80211_BSS_PARAMS_OCT          (1)
#define SWL_80211_BSS_PARAMS_SAME_SSID    (1 << 1)
#define SWL_80211_BSS_PARAMS_MBSSID       (1 << 2)
#define SWL_80211_BSS_PARAMS_TBSSID       (1 << 3)
#define SWL_80211_BSS_PARAMS_ESS_24_5_COLOC_AP (1 << 4)
#define SWL_80211_BSS_PARAMS_UPR_ACTIVE (1 << 5)
#define SWL_80211_BSS_PARAMS_COLOC_AP (1 << 6)

#define SWL_80211_SSID_STR_LEN 33

#define SWL_80211_EL_RM_ENAB_CAP_LEN        5

#define SWL_80211_HE_CAP_MAX_MCS_NONE       3   /* not supported bit value */
#define SWL_80211_HE_CAP_MAX_MCS_SIZE       2   /* num bits for 1-stream */
#define SWL_80211_HE_CAP_MAX_MCS_MASK       0x3 /* mask for 1-stream */

// Decodes the nss stream's -mcs value from mcs_nss_map
#define SWL_80211_HE_CAP_MCS_NSS_GET_MCS(nss, mcs_nss_map) (( mcs_nss_map >> (nss * SWL_80211_HE_CAP_MAX_MCS_SIZE)) & SWL_80211_HE_CAP_MAX_MCS_MASK )

void swl_80211_parseHtCap(swl_80211_htCapIE_t* htCapIE, uint8_t* frm);
void swl_80211_parseVhtCap(swl_80211_vhtCapIE_t* vhtCapIE, uint8_t* frm);
void swl_80211_parseHeCap(swl_80211_heCapIE_t* heCapIE, uint8_t* frm);

/* This enum can be completed with more parameters
 * ==> See IEEE_std_80211 specification, Table 9-149
 */
typedef enum {
    SWL_80211_USE_GRP_CIPHER_SUITE = 0,
    SWL_80211_CIPHER_SUITE_WEP_40 = 1,
    SWL_80211_CIPHER_SUITE_TKIP = 2,
    SWL_80211_CIPHER_SUITE_CCMP_128 = 4,
    SWL_80211_CIPHER_SUITE_WEP_104 = 5,
    SWL_80211_CIPHER_SUITE_BIP_CMAC_128 = 6,
    SWL_80211_CIPHER_SUITE_GCMP_128 = 8,
    SWL_80211_CIPHER_SUITE_GCMP_256 = 9,
    SWL_80211_CIPHER_SUITE_CCMP_256 = 10,
    SWL_80211_CIPHER_SUITE_BIP_GMAC_128 = 11,
    SWL_80211_CIPHER_SUITE_BIP_GMAC_256 = 12,
    SWL_80211_CIPHER_SUITE_BIP_CMAC_256 = 13

} swl_80211_cipherSuite_ne;

/* This enum can be completed with more parameters
 * ==> See IEEE_std_80211 specification, Table 9-151
 */
typedef enum {
    SWL_80211_AKM_SUITES_RESERVED = 0,
    SWL_80211_AKM_SUITES_IEEE_802_1X = 1,
    SWL_80211_AKM_SUITES_PSK = 2,
    SWL_80211_AKM_SUITES_FT_IEEE_802_1X = 3,
    SWL_80211_AKM_SUITES_FT_PSK = 4,
    SWL_80211_AKM_SUITES_IEEE_802_1X_SHA_256 = 5,
    SWL_80211_AKM_SUITES_PSK_SHA_256 = 6,
    SWL_80211_AKM_SUITES_TDLS_TPK = 7,
    SWL_80211_AKM_SUITES_SAE = 8,
    SWL_80211_AKM_SUITES_FT_SAE = 9,
    SWL_80211_AKM_SUITES_AES_256 = 14

} swl_80211_AKMSuite_ne;

/* This enum can be completed with more parameters
 * ==> See IEEE_std_80211 specification
 */
typedef enum
{
    /* management types: */
    SWL_80211_MGT_FRAME_TYPE_ASSOC_REQUEST = 0x00,
    SWL_80211_MGT_FRAME_TYPE_ASSOC_RESPONSE = 0x10,
    SWL_80211_MGT_FRAME_TYPE_REASSOC_REQUEST = 0x20,
    SWL_80211_MGT_FRAME_TYPE_REASSOC_RESPONSE = 0x30,
    SWL_80211_MGT_FRAME_TYPE_PROBE_REQUEST = 0x40,
    SWL_80211_MGT_FRAME_TYPE_PROBE_RESPONSE = 0x50,
    SWL_80211_MGT_FRAME_TYPE_BEACON = 0x80,
    SWL_80211_MGT_FRAME_TYPE_ATIM = 0x90,
    SWL_80211_MGT_FRAME_TYPE_DISASSOCIATION = 0xa0,
    SWL_80211_MGT_FRAME_TYPE_AUTHENTICATION = 0xb0,
    SWL_80211_MGT_FRAME_TYPE_DEAUTHENTICATION = 0xc0,
    SWL_80211_MGT_FRAME_TYPE_ACTION = 0xd0,
    SWL_80211_MGT_FRAME_TYPE_NOACKACTION = 0xe0,
    SWL_80211_MGT_FRAME_TYPE_RESERVED = 0xf0,
    SWL_80211_MGT_FRAME_TYPE_INVALID = 0xff,
} swl_80211_mgtFrameType_ne;

const char* swl_80211_mgtFrameTypeToChar(swl_80211_mgtFrameType_ne type);

/*
 * @brief return management frame type/subtype from frame control struct
 *
 * @param pFrameControl pointer to management frame control struct
 *
 * @return type as one of  swl_80211_mgtFrameType_ne values
 *         or byte value when not included
 *         SWL_80211_MGT_FRAME_TYPE_INVALID when frame is not a valid mgmt frame
 */
uint8_t swl_80211_mgtFrameType(swl_80211_mgmtFrameControl_t* pFrameControl);

#define M_SWL_80211_CIPHER_WEP40       (1 << SWL_80211_CIPHER_SUITE_WEP_40)
#define M_SWL_80211_CIPHER_TKIP        (1 << SWL_80211_CIPHER_SUITE_TKIP)
#define M_SWL_80211_CIPHER_CCMP        (1 << SWL_80211_CIPHER_SUITE_CCMP_128)
#define M_SWL_80211_CIPHER_WEP104      (1 << SWL_80211_CIPHER_SUITE_WEP_104)
#define M_SWL_80211_CIPHER_BIP_CMAC    (1 << SWL_80211_CIPHER_SUITE_BIP_CMAC_128)
#define M_SWL_80211_CIPHER_GCMP        (1 << SWL_80211_CIPHER_SUITE_GCMP_128)
#define M_SWL_80211_CIPHER_GCMP256     (1 << SWL_80211_CIPHER_SUITE_GCMP_256)
#define M_SWL_80211_CIPHER_CCMP256     (1 << SWL_80211_CIPHER_SUITE_CCMP_256)
#define M_SWL_80211_CIPHER_BIP_GMAC    (1 << SWL_80211_CIPHER_SUITE_BIP_GMAC_128)
#define M_SWL_80211_CIPHER_BIP_GMAC256 (1 << SWL_80211_CIPHER_SUITE_BIP_GMAC_256)
#define M_SWL_80211_CIPHER_BIP_CMAC256 (1 << SWL_80211_CIPHER_SUITE_BIP_CMAC_256)
typedef uint32_t swl_80211_cipher_m;

#define M_SWL_80211_AKM_IEEE_802_1X         (1 << SWL_80211_AKM_SUITES_IEEE_802_1X)
#define M_SWL_80211_AKM_PSK                 (1 << SWL_80211_AKM_SUITES_PSK)
#define M_SWL_80211_AKM_FT_IEEE_802_1X      (1 << SWL_80211_AKM_SUITES_FT_IEEE_802_1X)
#define M_SWL_80211_AKM_FT_PSK              (1 << SWL_80211_AKM_SUITES_FT_PSK)
#define M_SWL_80211_AKM_IEEE_802_1X_SHA_256 (1 << SWL_80211_AKM_SUITES_IEEE_802_1X_SHA_256)
#define M_SWL_80211_AKM_PSK_SHA_256         (1 << SWL_80211_AKM_SUITES_PSK_SHA_256)
#define M_SWL_80211_AKM_TDLS_TPK            (1 << SWL_80211_AKM_SUITES_TDLS_TPK)
#define M_SWL_80211_AKM_SAE                 (1 << SWL_80211_AKM_SUITES_SAE)
#define M_SWL_80211_AKM_FT_SAE              (1 << SWL_80211_AKM_SUITES_FT_SAE)
#define M_SWL_80211_AKM_AES_256             (1 << SWL_80211_AKM_SUITES_AES_256)
typedef uint32_t swl_80211_akm_m;

/*
 * management frame action categories
 * This enum can be completed with more values from
 * IEEE Std 802.11-2020, Table 9-51 Category values
 */
typedef enum
{
    SWL_80211_MGT_FRAME_ACTION_CATEGORY_PUBLIC = 4,            //Public
    SWL_80211_MGT_FRAME_ACTION_CATEGORY_RADIO_MEAS = 5,
    SWL_80211_MGT_FRAME_ACTION_CATEGORY_FAST_BSS_TRANS = 6,
    SWL_80211_MGT_FRAME_ACTION_CATEGORY_WNM = 10,              //Wireless Network Management
    SWL_80211_MGT_FRAME_ACTION_CATEGORY_VENDOR_SPECIFIC = 127, //Vendor specific
} swl_80211_mgtFrameActionCategory_ne;

/*
 * management frame public action categories
 * This enum can be completed with more values from
 * IEEE Std 802.11-2020, Table 9-364—Public Action field values
 */
typedef enum
{
    SWL_80211_MGT_FRAME_PUBLIC_ACTION_CATEGORY_VENDOR_SPECIFIC = 9,     //Public Vendor specific
} swl_80211_mgtFramePublicActionCategory_ne;

/*
 * wireless network management action purposes
 * This enum can be completed with more values from
 * IEEE Std 802.11-2020, Table 9-425 WNM Action field values
 */
typedef enum {
    SWL_80211_WNM_ACTION_PURPOSE_BTM_QUERY = 6,
    SWL_80211_WNM_ACTION_PURPOSE_BTM_REQUEST = 7,
    SWL_80211_WNM_ACTION_PURPOSE_BTM_RESPONSE = 8,
} swl_80211_wnmActionPurpose_ne;

/*
 * 802.11 Action Frame format
 */
typedef struct SWL_PACKED {
    uint8_t category;
    uint8_t data[1]; // More data possible, depends on len.
} swl_80211_actionFrame_t;

/*
 * 802.11 WNM Action Frame body format
 */
typedef struct SWL_PACKED {
    uint8_t purpose;
    uint8_t data[1]; // More data possible, depends on len.
} swl_80211_wnmActionFrameBody_t;

/*
 * 802.11 WNM Action BSS Transition Management Query Frame body format
 */
typedef struct SWL_PACKED {
    uint8_t dialogToken;
    uint8_t reason;
    uint8_t data[1]; // More data possible, including optional candidate list
} swl_80211_wnmActionBTMQueryFrameBody_t;

/*
 * 802.11 WNM Action BSS Transition Management Response Frame body format
 */
typedef struct SWL_PACKED {
    uint8_t dialogToken;
    uint8_t statusCode;
    uint8_t terminationDelay;
    uint8_t data[1]; // More data possible, including optional candidate list
} swl_80211_wnmActionBTMResponseFrameBody_t;



/*
 * @brief converts HTCapabilities mask into a list of strings
 *
 * @param htCapStr buffer where to write the string list of capabilities.
 * @param htCapStrSize size of the buffer htCapStr
 * @param  mask for the HTCapabilities, as defined in the standard section "9.4.2.55.2 HT Capability Information field"
 *
 * return size of output buffer, If the size of the buffer is small, output string will be limited to tgtStrSize, but returned valye will be the size of the
 * whole needed buffer to write all the output string.
 * NOTE: the returned value does not count the terminating null character!
 * return SWL_RC_INVALID_PARAM for any param issue.
 */
ssize_t swl_80211_htCapMaskToChar (char* htCapStr, size_t htCapStrSize, swl_80211_htCapInfo_m mask);

/*
 * @brief converts VHTCapabilities mask into a list of strings
 *
 * @param vhtCapStr buffer where to write the string list of capabilities.
 * @param vhtCapStrSize size of the buffer htCapStr
 * @param  mask for the VHTCapabilities, as defined in the standard section "9.4.2.157.2 VHT Capabilities Information field"
 *
 * return size of output buffer, If the size of the buffer is small, output string will be limited to tgtStrSize, but returned valye will be the size of the
 * whole needed buffer to write all the output string.
 * NOTE: the returned value does not count the terminating null character!
 * return SWL_RC_INVALID_PARAM for any param issue.
 */
ssize_t swl_80211_vhtCapMaskToChar(char* vhtCapStr, size_t vhtCapStrSize,
                                   swl_80211_vhtCapInfo_m mask);


/*
 * @brief converts He Physical Capabilities mask into a list of strings
 *
 * @param hePhyCapStr buffer where to write the string list of capabilities.
 * @param hePhyCapStrSize size of the buffer htCapStr
 * @param  mask for the HePhysCapabilities, as defined in the standard section "9.4.2.248.3 HE PHY Capabilities Information field"
 *
 * return size of output buffer, If the size of the buffer is small, output string will be limited to tgtStrSize, but returned valye will be the size of the
 * whole needed buffer to write all the output string.
 * NOTE: the returned value does not count the terminating null character!
 * return SWL_RC_INVALID_PARAM for any param issue.
 */
ssize_t swl_80211_hePhyCapMaskToChar(char* hePhysCapStr, size_t hePhysCapStrSize,
                                     swl_80211_hePhyCapInfo_m mask);

/**
 * @brief Legacy data rates in the 802.11 Supported Rates and Extended Supported Rates IEs
 */

#define M_SWL_80211_RATE_1MBPS   0x02
#define M_SWL_80211_RATE_2MBPS   0x04
#define M_SWL_80211_RATE_5_5MBPS 0x0b
#define M_SWL_80211_RATE_11MBPS  0x16
#define M_SWL_80211_RATE_6MBPS   0x0c
#define M_SWL_80211_RATE_9MBPS   0x12
#define M_SWL_80211_RATE_12MBPS  0x18
#define M_SWL_80211_RATE_18MBPS  0x24
#define M_SWL_80211_RATE_24MBPS  0x30
#define M_SWL_80211_RATE_36MBPS  0x48
#define M_SWL_80211_RATE_48MBPS  0x60
#define M_SWL_80211_RATE_54MBPS  0x6c

#endif /* SRC_INCLUDE_SWL_SWL_80211_H_ */
