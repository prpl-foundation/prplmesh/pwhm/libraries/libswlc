/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_TYPES_SWL_ARRAYBUF_H_
#define SRC_INCLUDE_SWL_TYPES_SWL_ARRAYBUF_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"

typedef struct swl_arrayBuf_s {
    size_t size;
    size_t maxSize;
    swl_type_t* type;
    swl_bit8_t data[];
} swl_arrayBuf_t;

typedef struct {
    const swl_arrayBuf_t* buf;
    swl_typeData_t* data;
    size_t index;
    bool valid;
} swl_arrayBufIt_t;

swl_rc_ne swl_arrayBuf_init(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize);
swl_rc_ne swl_arrayBuf_initFromArray(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize, swl_typeEl_t* array, size_t arraySize);
swl_rc_ne swl_arrayBuf_initExt(swl_arrayBuf_t* buf, swl_type_t* type, size_t maxSize, size_t size);
void swl_arrayBuf_clear(swl_arrayBuf_t* buf);
void swl_arrayBuf_cleanup(swl_arrayBuf_t* buf);

swl_typeEl_t* swl_arrayBuf_getReference(const swl_arrayBuf_t* buf, ssize_t index);
swl_typeData_t* swl_arrayBuf_getValue(const swl_arrayBuf_t* buf, ssize_t index);

swl_rc_ne swl_arrayBuf_append(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_arrayBuf_prepend(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index);
ssize_t swl_arrayBuf_add(swl_arrayBuf_t* buf, swl_typeData_t* data);
swl_typeEl_t* swl_arrayBuf_alloc(swl_arrayBuf_t* buf);

swl_rc_ne swl_arrayBuf_set(swl_arrayBuf_t* buf, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_arrayBuf_delete(swl_arrayBuf_t* buf, ssize_t index);
ssize_t swl_arrayBuf_find(const swl_arrayBuf_t* buf, swl_typeData_t* val);
size_t swl_arrayBuf_size(const swl_arrayBuf_t* buf);
bool swl_arrayBuf_equals(const swl_arrayBuf_t* buf1, const swl_arrayBuf_t* buf2);

swl_arrayBufIt_t swl_arrayBuf_getFirstIt(const swl_arrayBuf_t* arrayBuf);

void swl_arrayBufIt_init(swl_arrayBufIt_t* arrayBufIt, const swl_arrayBuf_t* arrayBuf);
void swl_arrayBufIt_next(swl_arrayBufIt_t* arrayBufIt);
void swl_arrayBufIt_del(swl_arrayBufIt_t* it);
size_t swl_arrayBufIt_index(swl_arrayBufIt_t* arrayBufIt);
swl_typeEl_t* swl_arrayBufIt_data(swl_arrayBufIt_t* it);
bool swl_arrayBufIt_assignVal(swl_arrayBufIt_t* it, swl_typeEl_t* tgtData);

#define swl_arrayBuf_forEachRef(item, dataType, dataName, arrayBuf) \
    dataType* dataName = NULL; \
    for(swl_arrayBufIt_t item = swl_arrayBuf_getFirstIt((swl_arrayBuf_t*) arrayBuf); \
        item.valid && ((dataName = (dataType*) swl_arrayBufIt_data(&it)) || true ); \
        swl_arrayBufIt_next(&item))

#define swl_arrayBuf_forEachVal(item, dataType, dataName, arrayBuf) \
    dataType data; \
    memset(&data, 0, sizeof(dataType)); \
    for(swl_arrayBufIt_t item = swl_arrayBuf_getFirstIt((swl_arrayBuf_t*) arrayBuf); \
        swl_arrayBufIt_assignVal(&item, &data); \
        swl_arrayBufIt_next(&item))

#endif /* SRC_INCLUDE_SWL_TYPES_SWL_ARRAYBUF_H_ */
