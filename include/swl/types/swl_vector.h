/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_TYPES_SWL_VECTOR_H_
#define SRC_INCLUDE_SWL_TYPES_SWL_VECTOR_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_unLiList.h"

#define SWL_VECTOR_DEFAULT_BLOCK_SIZE 5

typedef struct  {
    swl_type_t* type;
    swl_unLiList_t dataVector;
} swl_vector_t;

typedef struct {
    swl_vector_t* vector;
    swl_unLiListIt_t it;
} swl_vectorIt_t;

swl_rc_ne swl_vector_init(swl_vector_t* vector, swl_type_t* type);
swl_rc_ne swl_vector_initExt(swl_vector_t* vector, swl_type_t* type, uint32_t blockSize);
swl_rc_ne swl_vector_initFromArray(swl_vector_t* vector, swl_type_t* type, const swl_typeEl_t* srcData, size_t len);
bool swl_vector_isInitialized(swl_vector_t* vector);

void swl_vector_clear(swl_vector_t* vector);
void swl_vector_cleanup(swl_vector_t* vector);
swl_rc_ne swl_vector_insert(swl_vector_t* vector, swl_typeData_t* val, ssize_t index);

swl_typeData_t* swl_vector_getValue(const swl_vector_t* vector, ssize_t index);
swl_typeEl_t* swl_vector_getReference(const swl_vector_t* vector, ssize_t index);
bool swl_vector_equals(const swl_vector_t* vector1, const swl_vector_t* vector2);

ssize_t swl_vector_add(swl_vector_t* vector, swl_typeData_t* val);
swl_typeEl_t* swl_vector_alloc(swl_vector_t* vector);
swl_rc_ne swl_vector_set(swl_vector_t* ta, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_vector_delete(swl_vector_t* ta, ssize_t index);

ssize_t swl_vector_find(swl_vector_t* ta, swl_typeData_t* val);
size_t swl_vector_size(const swl_vector_t* vector);
swl_vectorIt_t swl_vector_getFirstIt(const swl_vector_t* vector);

void swl_vectorIt_init(swl_vectorIt_t* vectorIt, const swl_vector_t* vector);
void swl_vectorIt_next(swl_vectorIt_t* vectorIt);
void swl_vectorIt_del(swl_vectorIt_t* vectorIt);
size_t swl_vectorIt_index(swl_vectorIt_t* vectorIt);
swl_typeEl_t* swl_vectorIt_data(swl_vectorIt_t* vectorIt);
bool swl_vectorIt_assignVal(swl_vectorIt_t* it, swl_typeEl_t* tgtData);

swl_vectorIt_t swl_vector_getFirstIt(const swl_vector_t* vector);

#define swl_vector_forEachRef(item, dataType, dataName, vector) \
    dataType* dataName = NULL; \
    for(swl_vectorIt_t item = swl_vector_getFirstIt(vector); \
        item.it.valid && ((dataName = (dataType*) swl_vectorIt_data(&item)) || true ); \
        swl_vectorIt_next(&item))

#define swl_vector_forEachVal(item, dataType, dataName, vector) \
    dataType data; \
    memset(&data, 0, sizeof(dataType)); \
    for(swl_vectorIt_t item = swl_vector_getFirstIt(vector); \
        swl_vectorIt_assignVal(&item, &data); \
        swl_vectorIt_next(&item))

#endif /* SRC_INCLUDE_SWL_TYPECOLLECTION_SWL_VECTOR_H_ */
