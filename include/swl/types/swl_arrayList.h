/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_TYPES_SWL_ARRAYLIST_H_
#define SRC_INCLUDE_SWL_TYPES_SWL_ARRAYLIST_H_

typedef struct swl_arrayList_s {
    size_t size;
    size_t maxSize;
    swl_type_t* type;
    swl_typeData_t* data;
} swl_arrayList_t;

typedef struct {
    const swl_arrayList_t* buf;
    swl_typeData_t* data;
    size_t index;
    bool valid;
} swl_arrayListIt_t;

swl_rc_ne swl_arrayList_initExt(swl_arrayList_t* al, swl_type_t* type, size_t defaultSize);
swl_rc_ne swl_arrayList_init(swl_arrayList_t* al, swl_type_t* type);
swl_rc_ne swl_arrayList_initFromArray(swl_arrayList_t* al, swl_type_t* type, const swl_typeEl_t* srcData, size_t len);
bool swl_arrayList_isInitialized(swl_arrayList_t* al);
void swl_arrayList_clear(swl_arrayList_t* al);
void swl_arrayList_cleanup(swl_arrayList_t* al);

swl_typeEl_t* swl_arrayList_getReference(const swl_arrayList_t* al, ssize_t index);
swl_typeData_t* swl_arrayList_getValue(const swl_arrayList_t* al, ssize_t index);

ssize_t swl_arrayList_add(swl_arrayList_t* al, swl_typeData_t* data);
swl_typeEl_t* swl_arrayList_alloc(swl_arrayList_t* al);
swl_rc_ne swl_arrayList_insert(swl_arrayList_t* al, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_arrayList_set(swl_arrayList_t* al, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_arrayList_delete(swl_arrayList_t* al, ssize_t index);

ssize_t swl_arrayList_find(const swl_arrayList_t* al, const swl_typeData_t* val);
size_t swl_arrayList_size(const swl_arrayList_t* al);
bool swl_arrayList_equals(const swl_arrayList_t* al1, const swl_arrayList_t* al2);

swl_arrayListIt_t swl_arrayList_getFirstIt(const swl_arrayList_t* al);
void swl_arrayListIt_init(swl_arrayListIt_t* arrayListIt, const swl_arrayList_t* arrayList);
void swl_arrayListIt_next(swl_arrayListIt_t* arrayListIt);
void swl_arrayListIt_del(swl_arrayListIt_t* it);
size_t swl_arrayListIt_index(swl_arrayListIt_t* arrayListIt);
swl_typeEl_t* swl_arrayListIt_data(swl_arrayListIt_t* it);
bool swl_arrayListIt_assignVal(swl_arrayListIt_t* it, swl_typeEl_t* tgtData);

#define swl_arrayList_forEachRef(item, dataType, dataName, arrayList) \
    dataType* dataName = NULL; \
    for(swl_arrayListIt_t item = swl_arrayList_getFirstIt(arrayList); \
        item.valid && ((dataName = (dataType*) swl_arrayListIt_data(&it)) || true ); \
        swl_arrayListIt_next(&item))

#define swl_arrayList_forEachVal(item, dataType, dataName, arrayList) \
    dataType data; \
    memset(&data, 0, sizeof(dataType)); \
    for(swl_arrayListIt_t item = swl_arrayList_getFirstIt(arrayList); \
        swl_arrayListIt_assignVal(&item, &data); \
        swl_arrayListIt_next(&item))

#endif /* SRC_INCLUDE_SWL_TYPES_SWL_ARRAYLIST_H_ */
