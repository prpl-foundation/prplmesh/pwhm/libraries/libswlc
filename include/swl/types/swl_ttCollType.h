/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_TT_COLL_TYPE_H_
#define SRC_INCLUDE_SWL_TT_COLL_TYPE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/types/swl_collType.h"

#define SWL_TYPE_TT_COL_FUN_UID 3

typedef swl_collType_t swl_ttCollType_t;

// A tuple type collection (swl_ttColl_t) is a collection, where the collection contains tuple values
// This allows much more functions to be defined (i.e. key value lookup related)
typedef swl_coll_t swl_ttColl_t;


/**
 * Return the typeData value in the collection of the element at row tupleIndex, and column typeIndex.
 * Returns NULL if no such value exists.
 */
typedef swl_typeData_t* (* swl_ttCollType_getElementValue_cb)(swl_ttCollType_t* type, swl_ttColl_t* ttColl,
                                                              ssize_t tupleIndex, size_t typeIndex);

/**
 * Return the type Element reference in the collection of the element at row tupleIndex, and column typeIndex.
 * Returns NULL if no such value exists.
 */
typedef swl_typeEl_t* (* swl_ttCollType_getElementRef_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                          ssize_t tupleIndex, size_t typeIndex);

/**
 * Set the element at row tupleIndex and column typeINdex, with given data value.
 * Returns SWL_RC_OK if successfull. Returns SWL_RC error code upon failure.
 */
typedef swl_rc_ne (* swl_ttCollType_setElementValue_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                        ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data);

/**
 * Perform a shallow copy of all data in a given column, to target array, starting at offset. It will copy as much
 * elements as fit in tgtArray.
 * Returns SWL_RC_OK if successfull and all elements copied. Returns SWL_RC_CONTINUE if more elements could be printed,
 * return SWL_RC error code if for some reason no copying could be done.
 *
 * Since this is a shallow copy, the resulting array MUST NOT clean its element references.
 */
typedef swl_rc_ne (* swl_ttCollType_columnToArrayOffset_cb)(swl_ttCollType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                                            const swl_ttColl_t* ttColl, size_t typeIndex, size_t offSet);
/**
 * Perform a shallow copy of all data in a given column, to target array. It will copy as much
 * elements as fit in tgtArray.
 * Returns SWL_RC_OK if successfull and all elements copied. Returns SWL_RC_CONTINUE if more elements could be printed,
 * return SWL_RC error code if for some reason no copying could be done.
 *
 * Since this is a shallow copy, the resulting array MUST NOT clean its element references.
 */
typedef swl_rc_ne (* swl_ttCollType_columnToArray_cb)(swl_ttCollType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                                      const swl_ttColl_t* ttColl, size_t typeIndex);

/**
 * Find the tuple index, so that the element at the resulting index, in column srcTypeIndex, matches by type with srcData.
 *
 * Will return SWL_RC_INVALID_PARAM if no such element can be found
 */
typedef ssize_t (* swl_ttCollType_findMatchingTuple_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                        ssize_t offset, size_t srcTypeIndex, const swl_typeData_t* srcData);

/**
 * Find the tuple, so that the element at the resulting index, in colum srcTypeIndex, matches by type with srcData.
 *
 * Will return SWL_RC_INVALID_PARAM if no such element can be found
 */
typedef swl_tuple_t* (* swl_ttCollType_getMatchingTuple_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                            ssize_t offset, size_t srcTypeIndex, const swl_typeData_t* srcData);


/**
 * Return the data in column tgtTypeIndex, of the first tuple where the column srcTypeIndex matches srcData.
 *
 * Will return NULL if no such element can be found
 */
typedef swl_typeData_t* (* swl_ttCollType_getMatchingElement_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                                 ssize_t offset, size_t tgtTypeIndex, size_t srcTypeIndex, const swl_typeData_t* srcData);

/**
 * Find the tuple index, so that the tuple row matches by mask with the provided src data.
 *
 * Will return SWL_RC_INVALID_PARAM if no such element can be found
 */
typedef ssize_t (* swl_ttCollType_findMatchingTupleIndexByMask_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                                   ssize_t offset, const swl_tuple_t* srcData, swl_mask_m mask);

/**
 * Find the tuple, so that the tuple row matches by mask with the provided src data.
 *
 * Will return NULL if no such element can be found
 */
typedef swl_tuple_t* (* swl_ttCollType_getMatchingTupleByMask_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                                  ssize_t offset, const swl_tuple_t* srcData, swl_mask_m mask);

/**
 * Return the data in column tgtTypeIndex, of the first tuple which matches by mask with the provided srcData tuple, over provided mask.
 *
 * Will return NULL if no such element can be found
 */
typedef swl_typeData_t* (* swl_ttCollType_getMatchingElementByMask_cb)(swl_ttCollType_t* type, swl_ttColl_t* array,
                                                                       ssize_t offset, size_t tgtTypeIndex, const swl_tuple_t* srcData, swl_mask_m mask);

/**
 * Print the provided array as a set of lists to the file stream, starting at providing offset, printing all elements, and with provided print args.
 * Each list should contain the elements in a single column.
 * Note that "print lists per row" can be achieved by printing as a standard collection.
 */
typedef bool (* swl_ttCollType_fPrintLists)(swl_ttCollType_t* type, FILE* stream, swl_ttColl_t* array,
                                            ssize_t offset, swl_print_args_t* args);

/**
 * Print the provided array as a set of maps to the file stream, starting at providing offset, printing all elements, and with provided print args.
 * Each map entry should contain the elements in a single column, and as key it shall have the type index'th element of names, appended with suffix.
 */
typedef bool (* swl_ttCollType_fPrintMaps)(swl_ttCollType_t* type, FILE* stream, swl_ttColl_t* array,
                                           ssize_t offset, swl_print_args_t* args, char** names, char* suffix);


typedef struct swl_ttCollTypeFun_s {
    swl_listSTypeFun_t* collTypeFun;
    swl_ttCollType_getElementValue_cb getElementValue;
    swl_ttCollType_getElementRef_cb getElementRef;
    swl_ttCollType_setElementValue_cb setElementValue;
    swl_ttCollType_columnToArrayOffset_cb columnToArrayOffset;
    swl_ttCollType_columnToArray_cb columnToArray;
    swl_ttCollType_findMatchingTuple_cb findMatchingTupleIndex;
    swl_ttCollType_getMatchingTuple_cb getMatchingTuple;
    swl_ttCollType_getMatchingElement_cb getMatchingElement;
    swl_ttCollType_findMatchingTupleIndexByMask_cb findMatchingTupleIndexByMask;
    swl_ttCollType_getMatchingTupleByMask_cb getMatchingTupleByMask;
    swl_ttCollType_getMatchingElementByMask_cb getMatchingElementByMask;
    swl_ttCollType_fPrintLists fPrintLists;
    swl_ttCollType_fPrintMaps fPrintMaps;
} swl_ttCollTypeFun_t;

#endif /* SRC_INCLUDE_SWL_TT_COLL_TYPE_H_ */

