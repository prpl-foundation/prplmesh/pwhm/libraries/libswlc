/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_TYPES_SWL_TTATYPE_H_
#define SRC_INCLUDE_SWL_TYPES_SWL_TTATYPE_H_

#include "swl/swl_common.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_tupleType.h"
#include "swl/types/swl_collType.h"
#include "swl/types/swl_ttCollType.h"

// Defines a tuple type array type. Note that the element type of the collType is a swl_tupleType_t*
typedef struct swl_ttaType_s {
    swl_collType_t type;
    size_t maxNrElements;
    bool allowEmpty; // If true, then insertion will be allowed everywhere, and size will return number of non empty entries.
} swl_ttaType_t;

extern swl_typeFun_t swl_ttaType_fun;
extern swl_listSTypeFun_t swl_ttaCollType_fun;
extern swl_ttCollTypeFun_t swl_ttaTtCollType_fun;


/**
 * Internal macro for common define functionality
 * DO NOT USE elsewhere
 */
#define SWL_TTA_TYPE_COMMON(inType, inElementType, inMaxNrElements) \
    typedef struct {inElementType ## _type data[inMaxNrElements]; } inType ## _type;

/**
 * Macro to declare an array type in header file, to be used outside of the defining file
 */
#define SWL_TTA_TYPE_H(inType, inElementType, inMaxNrElements) \
    SWL_TTA_TYPE_COMMON(inType, inElementType, inMaxNrElements); \
    extern swl_ttaType_t inType;

/**
 * Macro to initialize an array type in c file, with declaration to be done in header file
 */
#define SWL_TTA_TYPE_C(inType, inElementType, inMaxNrElements, inAllowEmpty, inToStrVar) \
    swl_ttaType_t inType = { \
        .maxNrElements = inMaxNrElements, \
        .allowEmpty = inAllowEmpty, \
        .type = { \
            .type = { \
                .typeFun = &swl_ttaType_fun, \
                .size = sizeof(inType ## _type), \
            }, \
            .elementType = &inElementType.type, \
            .toStrVar = inToStrVar, \
        } \
    };

/**
 * Macro to be used to declare and initialize a tuple type array type in c file. Usefull if
 * the type only has to be used inside the C file.
 *
 * inType : the name of the TTA_TYPE
 * inElementType: an swl_tupleType_t name
 * inMaxNrElements: the max nr of elements
 * allowEmpty : whether to allow empty elements
 * whether the "toVariant" should return a list of variants, or a list of strings
 */
#define SWL_TTA_TYPE(inType, inElementType, inMaxNrElements, allowEmpty, inToStrVar) \
    SWL_TTA_TYPE_COMMON(inType, inElementType, inMaxNrElements) \
    SWL_TTA_TYPE_C(inType, inElementType, inMaxNrElements, allowEmpty, inToStrVar)


#endif /* SRC_INCLUDE_SWL_TYPES_SWL_TTATYPE_H_ */
