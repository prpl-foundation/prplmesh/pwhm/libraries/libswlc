/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_TYPES_SWL_TUPLETYPEARRAY_H_
#define SRC_INCLUDE_SWL_TYPES_SWL_TUPLETYPEARRAY_H_

#include "swl/swl_common_tupleType.h"

swl_tuple_t* swl_tta_getTuple(swl_tupleType_t* type, const swl_tuple_t* array, size_t arraySize, ssize_t tupleIndex);
swl_rc_ne swl_tta_setTuple(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                           ssize_t tupleIndex, swl_tuple_t* data);

swl_typeEl_t* swl_tta_getElementReference(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                          ssize_t tupleIndex, size_t typeIndex);
swl_typeData_t* swl_tta_getElementValue(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                        ssize_t tupleIndex, size_t typeIndex);
swl_rc_ne swl_tta_setElementValue(swl_tupleType_t* tupleType, swl_tuple_t* array, size_t arraySize,
                                  ssize_t tupleIndex, size_t typeIndex, const swl_typeData_t* data);

void swl_tta_cleanupColumn(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t typeIndex);
void swl_tta_cleanupByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t typeMask);
void swl_tta_cleanup(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize);

swl_rc_ne swl_tta_columnToArrayOffset(swl_tupleType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                      const swl_typeEl_t* srcArray, size_t srcArraySize, size_t typeIndex, size_t offSet);
swl_rc_ne swl_tta_columnToArray(swl_tupleType_t* targetType, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                const swl_typeEl_t* srcArray, size_t srcArraySize, size_t typeIndex);
bool swl_tta_equals(swl_tupleType_t* targetType, const swl_typeEl_t* array0, size_t arraySize0, const swl_typeEl_t* array1, size_t arraySize1);

swl_typeData_t* swl_tta_getMatchingElement(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset,
                                           size_t tgtTypeIndex, size_t srcIndex, const swl_typeData_t* srcValue);
swl_typeData_t* swl_tta_getMatchingElementByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset, size_t tgtTypeIndex,
                                                 const swl_tuple_t* srcValue, size_t mask);

ssize_t swl_tta_findMatchingTupleIndex(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                       size_t offset, size_t srcIndex, const swl_typeData_t* srcValue);

ssize_t swl_tta_findMatchingTupleIndexByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize,
                                             size_t offset, const swl_tuple_t* srcValue, size_t mask);

swl_tuple_t* swl_tta_getMatchingTuple(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset,
                                      size_t srcIndex, const swl_typeData_t* srcValue);
swl_tuple_t* swl_tta_getMatchingTupleInRange(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t srcIndex,
                                             const swl_typeData_t* srcValue, ssize_t startElem, ssize_t endElem, ssize_t stepSize);

swl_tuple_t* swl_tta_getMatchingTupleByMask(swl_tupleType_t* type, swl_tuple_t* array, size_t arraySize, size_t offset,
                                            const swl_tuple_t* srcValue, size_t mask);

ssize_t swl_tta_printLists(swl_tupleType_t* targetType, char* tgtStr, size_t tgtStrSize,
                           const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset);
bool swl_tta_fprintLists(swl_tupleType_t* targetType, FILE* file,
                         const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset,
                         bool printEmpty, swl_print_args_t* args);

ssize_t swl_tta_printMaps(swl_tupleType_t* targetType, char* tgtStr, size_t tgtStrSize,
                          const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset,
                          char** names, char* suffix);
bool swl_tta_fprintMaps(swl_tupleType_t* targetType, FILE* file,
                        const swl_typeEl_t* tgtArray, size_t tgtArraySize, size_t offset, bool printEmpty, swl_print_args_t* args,
                        char** names, char* suffix);

#endif /* SRC_INCLUDE_SWL_TYPES_SWL_TUPLETYPEARRAY_H_ */
