/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __SWL_COMMON_MCS_H__
#define __SWL_COMMON_MCS_H__

#include "swl/swl_common_chanspec.h"

typedef enum {
    SWL_MCS_CONSISTENCY_ISSUE_MCS,          // indicates legacy MCS list is not continous
    SWL_MCS_CONSISTENCY_ISSUE_HT,           // indicates HT MCS list is not continous
    SWL_MCS_CONSISTENCY_ISSUE_VHT,          // indicates that values of the mcs list are not the same
    SWL_MCS_CONSISTENCY_ISSUE_HE_80_NSS,    // indicates that values of the mcs list are not the same
    SWL_MCS_CONSISTENCY_ISSUE_HE_80_ANT,    // indicates that the max NSS of the mcs list is not equal to NrRxSpatialStream/NrTxSpatialStream
    SWL_MCS_CONSISTENCY_ISSUE_HE_160_NSS,   // indicates that values of the mcs list are not the same
    SWL_MCS_CONSISTENCY_ISSUE_HE_160_ANT,   // indicates that the max NSS of the mcs list is not equal to NrRxSpatialStream/NrTxSpatialStream
    SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_NSS, // indicates that values of the mcs list are not the same
    SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_ANT, // indicates that the max NSS of the mcs list is not equal to NrRxSpatialStream/NrTxSpatialStream
    SWL_MCS_CONSISTENCY_ISSUE_MCS_MAX,
} swl_mcs_consistencyIssue_e;

#define M_SWL_MCS_CONSISTENCY_ISSUE_MCS (1 << SWL_MCS_CONSISTENCY_ISSUE_MCS )
#define M_SWL_MCS_CONSISTENCY_ISSUE_HT (1 << SWL_MCS_CONSISTENCY_ISSUE_HT)
#define M_SWL_MCS_CONSISTENCY_ISSUE_VHT (1 << SWL_MCS_CONSISTENCY_ISSUE_VHT)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_80_NSS (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_80_NSS)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_80_ANT (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_80_ANT)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_160_NSS (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_160_NSS)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_160_ANT (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_160_ANT)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_NSS (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_NSS)
#define M_SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_ANT (1 << SWL_MCS_CONSISTENCY_ISSUE_HE_80X80_ANT)

typedef uint32_t swl_mcs_consistencyIssue_m;

extern const char* swl_mcs_consistencyIssue_str[SWL_MCS_CONSISTENCY_ISSUE_MCS_MAX];

#define SWL_MCS_LEGACY_LIST_SIZE 12
extern const uint32_t swl_mcs_legacyList[SWL_MCS_LEGACY_LIST_SIZE];

typedef uint32_t swl_mcs_legacyIndex_m;
extern const char* swl_mcs_legacyStrList[SWL_MCS_LEGACY_LIST_SIZE];

typedef struct {
    uint8_t modulation;
    uint8_t quotient;
    uint8_t divider;
} swl_mcs_rateInfo_t;

#define SWL_MCS_MAX_HE_MCS 11
#define SWL_MCS_MAX_VHT_MCS 9
#define SWL_MCS_MAX_HT_MCS 7
#define SWL_MCS_MAX 16
#define SWL_MCS_SUP_MAX 128
#define SWL_MCS_SUP_MAX_NSS 8

extern const swl_mcs_rateInfo_t swl_mcs_info[SWL_MCS_MAX];
extern const char* swl_mcs_standard_str_list[];

typedef enum {
    SWL_SGI_AUTO,
    SWL_SGI_400,
    SWL_SGI_800,
    SWL_SGI_1600,
    SWL_SGI_3200,
    SWL_SGI_MAX,
} swl_guardinterval_e;


typedef enum {
    SWL_MCS_STANDARD_UNKNOWN,
    SWL_MCS_STANDARD_LEGACY,
    SWL_MCS_STANDARD_HT,
    SWL_MCS_STANDARD_VHT,
    SWL_MCS_STANDARD_HE,
    SWL_MCS_STANDARD_EHT,
    SWL_MCS_STANDARD_MAX
} swl_mcsStandard_e;

typedef uint32_t swl_guardinterval_m;  // swl_bandwidth_m is defined in swl_chanspec.h

#define M_SWL_SGI_AUTO (1 << SWL_SGI_AUTO )
#define M_SWL_SGI_400 (1 << SWL_SGI_400)
#define M_SWL_SGI_800 (1 << SWL_SGI_800)
#define M_SWL_SGI_1600 (1 << SWL_SGI_1600)
#define M_SWL_SGI_3200 (1 << SWL_SGI_3200)


typedef struct {
    swl_mcsStandard_e standard;
    uint32_t mcsIndex;
    swl_bandwidth_e bandwidth;
    uint32_t numberOfSpatialStream;
    swl_guardinterval_e guardInterval;
} swl_mcs_t;

typedef enum {
    SWL_COM_DIR_TRANSMIT,
    SWL_COM_DIR_RECEIVE,
    SWL_COM_DIR_MAX
} swl_com_dir_e;

typedef struct {
    uint8_t mcsNbr;
    uint8_t mcs[SWL_MCS_SUP_MAX];
} swl_mcs_supMCS_t;

typedef struct {
    uint8_t nssNbr;
    uint8_t nssMcsNbr[SWL_MCS_SUP_MAX_NSS];
} swl_mcs_supMCS_adv_t;

bool swl_mcs_checkMcsIndexes(swl_mcs_t* mcs);
bool swl_mcs_checkCharSerialized(const char* str);
int32_t swl_mcs_intToLegacy(uint32_t intLegacy);
void swl_mcs_clean(swl_mcs_t* mcs);

size_t swl_mcs_toChar(char* tgtStr, size_t targetSize, swl_mcs_t* mcs);
bool swl_mcs_fromChar(swl_mcs_t* targetMcs, char* mcsString);

uint32_t swl_mcs_toRate(swl_mcs_t* mcs);
uint32_t swl_mcs_guardIntervalToInt(swl_guardinterval_e gi);
swl_radStd_e swl_mcs_radStdFromMcsStd(swl_mcsStandard_e mcsStd, swl_freqBandExt_e freqBandExt);
swl_mcsStandard_e swl_mcs_mcsStdFromRadStd(swl_radStd_e radStd);
bool swl_mcs_matchesRadStandard(swl_mcsStandard_e mcsStd, swl_radStd_e radStd);

#define swl_type_mcs &gtSwl_type_mcs
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Mcs, gtSwl_type_mcs, swl_mcs_t)

SWL_TYPE_ENUM_H(gtSwl_type_mcsStandard, swl_mcsStandard_e);
#define swl_type_mcsStandard &gtSwl_type_mcsStandard

#endif /* __SWL_MCS_H__ */

