/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_TUPLETYPE_H_
#define SRC_INCLUDE_SWL_COMMON_TUPLETYPE_H_

#include <stdio.h>
#include <stdbool.h>
#include "swl/swl_common_type.h"
#include "swl/types/swl_refType.h"
#include "swl/types/swl_collType.h"

typedef struct {
    swl_type_t type;
    swl_type_t** types;
    size_t nrTypes;
    size_t* offsets;
} swl_tupleType_t;

/**
 * Model of a tuple. A tuple is a structure that matches the types as listed in a specific tuple type.
 * A tuple thus basically consist of a collection of swl_typeEl_t fields, with each swl_typeEl_t
 * having its own type.
 */
typedef void swl_tuple_t;

extern swl_typeFun_t swl_tupleType_fun;
extern swl_listSTypeFun_t swl_tupleTypeCollType_fun;

/**
 * @Deprecated: potentially confusing, please use SWL_TUPLE_TYPE_NEW
 * Define a tuple type, old way.
 * Given a name tupleTypeName this macro produces a swl_type_t*[] of the typeArray named "tupleTypeNameTypes"
 * and a swl_tupleType_t named "tupleTypeNameTupleType"
 *
 * Example usage:
 * SWL_TUPLE_TYPE(myTestTuple, ARR(swl_type_int32, swl_type_int64, swl_type_charPtr));
 */
#define SWL_TUPLE_TYPE(tupleTypeName, typeArray) \
    swl_type_t* tupleTypeName ## Types[] = typeArray; \
    swl_tupleType_t tupleTypeName ## TupleType = { \
        .type = { \
            .typeFun = &swl_tupleType_fun, \
        }, \
        .types = tupleTypeName ## Types, \
        .nrTypes = SWL_ARRAY_SIZE(tupleTypeName ## Types), \
        .offsets = NULL, \
    };

/**
 * Define a tuple type, new way.
 * Given a name "ttMyTestData" this macro produces a swl_type_t*[] of the typeArray named "ttMyTestDataTypes"
 * and a swl_tupleType_t named "ttMyTestData"
 *
 * Example usage:
 * SWL_TUPLETYPE_NEW(ttMyTestData, ARR(swl_type_int32, swl_type_int64, swl_type_charPtr));
 */
#define SWL_TUPLE_TYPE_NEW(tupleTypeName, typeArray) \
    swl_type_t* tupleTypeName ## Types[] = typeArray; \
    swl_tupleType_t tupleTypeName = { \
        .type = { \
            .typeFun = &swl_tupleType_fun, \
        }, \
        .types = tupleTypeName ## Types, \
        .nrTypes = SWL_ARRAY_SIZE(tupleTypeName ## Types) \
    };



/**
 * Create a tuple type, and associated type struct, using an X-Macro
 * Example:
 *
 * #define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index) \
    X(Y, gtSwl_type_int64, key) \
    X(Y, gtSwl_type_charPtr, val)
   SWL_TT(myTestNTTType, swl_myTestTT_t, MY_TEST_TABLE_VAR, structMod)

 * The X-Macro must have arguments X, Y.
 * Each variable of the named tuple type must be define with:
 * X(Y, typeVariable, variableStructName, variableStringName) \
 * The typeVariable is the type of the variable, i.e. the name of the struct of the type
 * The variableName is the name as the variable should appear in its struct.
 * The structMod is to add potential modifiers to the struct such as SWL_PACKED or force alignment
 */

/*****************************************
* Internal macro's, DO NOT USE EXTERNALLY
*****************************************/
#define SWL_TT_EXPAND_TYPE(name, swl_type, varName) \
    swl_type ## _type varName;

#define SWL_TT_EXPAND_TUP_TYPE(name, swl_type, varName) (swl_type_t*) &swl_type,
#define SWL_TT_EXPAND_VAR_NAME(name, swl_type, varName) name ## _ ## varName,
#define SWL_TT_EXPAND_VAR_NAME_MASK(name, swl_type, varName) m_ ## name ## _ ## varName = (1 << name ## _ ## varName),
#define SWL_TT_EXPAND_OFFSET(name, swl_type, varName) offsetof(name, varName),

#define SWL_TT_H_COMMON_ENUM(typeName, structName, xMacro) \
    typedef structName typeName ## _type; \
    typedef enum { \
        xMacro(SWL_TT_EXPAND_VAR_NAME, typeName) \
        typeName ## __max \
    } typeName ## _e;

#define SWL_TT_H_COMMON(typeName, structName, xMacro, structMod) \
    typedef struct structMod { \
        xMacro(SWL_TT_EXPAND_TYPE, typeName) \
    } structName; \
    SWL_TT_H_COMMON_ENUM(typeName, structName, xMacro)

#define SWL_TT_H_EXT(typeName, structName, xMacro) \
    extern swl_type_t* typeName ## Types[]; \
    extern size_t typeName ## OffSets[]; \
    extern swl_tupleType_t typeName;
/*****************************************
* End of internal macro's, DO NOT USE EXTERNALLY
*****************************************/



// Add tuple type declaration to header, constains struct and enum definitions, and references to arrays
#define SWL_TT_H(typeName, structName, xMacro, structMod) \
    SWL_TT_H_COMMON(typeName, structName, xMacro, structMod) \
    SWL_TT_H_EXT(typeName, structName, xMacro) \
    SWL_REF_TYPE_H(typeName ## Ptr, typeName)

// Add the tuple type implementation to c file. To be used together with SWL_TT_H for declaration
#define SWL_TT_C(typeName, structName, xMacro) \
    swl_type_t* typeName ## Types[] = ARR(xMacro(SWL_TT_EXPAND_TUP_TYPE, typeName)); \
    size_t typeName ## OffSets[] = ARR(xMacro(SWL_TT_EXPAND_OFFSET, structName) \
                                       sizeof(structName)); \
    swl_tupleType_t typeName = { \
        .type = { \
            .size = sizeof(structName), \
            .typeFun = &swl_tupleType_fun, \
        }, \
        .types = typeName ## Types, \
        .nrTypes = SWL_ARRAY_SIZE(typeName ## Types), \
        .offsets = typeName ## OffSets, \
    }; \
    SWL_REF_TYPE_C(typeName ## Ptr, typeName)

// Add both type declaration and implemenation to C file. Can not be used together with SWL_TT_H and SWL_TT_C
#define SWL_TT(typeName, structName, xMacro, structMod) \
    SWL_TT_H_COMMON(typeName, structName, xMacro, structMod) \
    SWL_REF_TYPE_COMMON(typeName ## Ptr, typeName) \
    SWL_TT_C(typeName, structName, xMacro)

// Add type annotation declaration to an existing file in header. For implementation part, SWL_TT_C can be used
// This is used to "annotate" an existing type. Not all fields need to be annotated.
#define SWL_TT_H_ANNOTATE(typeName, structName, xMacro) \
    SWL_TT_H_COMMON_ENUM(typeName, structName, xMacro) \
    SWL_TT_H_EXT(typeName, structName, xMacro) \
    SWL_REF_TYPE_H(typeName ## Ptr, typeName)

// Add type annotation declaration and implementation to an existing source file. Can not be used together with SWL_TT_H_ANNOTATE
// This is used to "annotate" an existing type. Not all fields need to be annotated.
#define SWL_TT_ANNOTATE(typeName, structName, xMacro) \
    SWL_TT_H_COMMON_ENUM(typeName, structName, xMacro) \
    SWL_REF_TYPE_COMMON(typeName ## Ptr, typeName) \
    SWL_TT_C(typeName, structName, xMacro)

/*
 * Define mask names for all fields in the struct. Note that this will limit possible number of values to
 * 32, as that's the default max size of an enum. As such, this is not standard included.
 */
#define SWL_TT_MASK(typeName, structName, xMacro) \
    typedef enum { \
        xMacro(SWL_TT_EXPAND_VAR_NAME_MASK, typeName) \
        m_ ## typeName ## __all = (1 << typeName ## __max) - 1 \
    } typeName ## _m;

swl_typeEl_t* swl_tupleType_getReference(swl_tupleType_t* type, const swl_tuple_t* tuple, size_t index);
swl_typeData_t* swl_tupleType_getValue(swl_tupleType_t* type, const swl_tuple_t* tuple, size_t index);
size_t swl_tupleType_getOffset(swl_tupleType_t* type, size_t index);
bool swl_tupleType_check(swl_tupleType_t* type);
size_t swl_tupleType_size(swl_tupleType_t* type);

bool swl_tupleType_equals(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2);
void swl_tupleType_cleanup(swl_tupleType_t* tplType, swl_tuple_t* tuple);
void swl_tupleType_cleanupIndex(swl_tupleType_t* tplType, swl_tuple_t* tuple, size_t index);
void swl_tupleType_cleanupByMask(swl_tupleType_t* tplType, swl_tuple_t* tuple, size_t typeMask);

void swl_tupleType_copyByMask(swl_tuple_t* targetTuple, swl_tupleType_t* tplType, const swl_tuple_t* tuple, size_t typeMask);
bool swl_tupleType_equalsByMask(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2, size_t typeMask);
size_t swl_tupleType_getDiffMask(swl_tupleType_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2);

#endif /* SRC_INCLUDE_SWL_TUPLETYPE_H_ */
