/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_BIDIRCOLLECTION__
#define SRC_INCLUDE_SWL_BIDIRCOLLECTION__

#include "swl/swl_unLiList.h"

typedef struct {
    /**
     *
     * Never NULL. Must never return NULL (except if `child` is NULL).
     *
     * Must not change the biDirCollection.
     */
    const char* (*getName)(const void* child);

    /**
     *
     * Never NULL.
     * Can return NULL if having a parent is optional for the child.
     *
     * Must not change the biDirCollection.
     */
    void* (*getParent)(void* child);

    /**
     * Called when `child` is being added to `parent`.
     *
     * Must not change the biDirCollection of `parent`.
     *
     * This function pointer can be NULL.
     *
     * If it is NULL, then when a child is added to a parent, the child must already
     *   have its pointer to the parent, i.e. the bidirectional association must be initiated
     *   by the child.
     *
     * The child must not check if adding would cause duplicates (by name or by pointer);
     * this check is taken care of already.
     *
     * @return
     *   true: the child allows `parent`, and after the function call returns has `parent`
     *     as its parent. The caller guarantees to call `onRemove(child, parent)` later.
     *   false: the child does not allow `parent` as its parent, and after the function call
     *     returns, #getParent() returns the same as before.
     */
    bool (* onAdd)(void* child, void* parent);

    /**
     * Called when `child` is removed from `parent`.
     *
     * Must not change the biDirCollection.
     *
     * Technically it's called when `child` is removed from a biDirCollection of `parent`.
     * This normally also happens when `parent` is destroyed.
     *
     * Must remove the link from child to parent or destroy the child.
     *
     * Does not need to remove the link from parent to child, but it's safe to do so.
     */
    void (* onRemove)(void* child, void* parent);
} swl_bdc_child_type_t;

/**
 *
 * Treat this as encapsulated (so users of biDirCollection must not touch its fields).
 *
 * The struct definition is only publicly visible to allow to use it as a struct field
 * ```
 * typedef struct {
 *   swl_biDirCollection_t children; // <-- like this
 *   // ...
 * } myParent_t;
 * ```
 */
typedef struct {
    /**
     * Parent of the children in `children`.
     *
     * Null if and only if during destruction (responsibility of biDirCollection, not of user
     * of biDirCollection).
     */
    void* parent;

    /**
     * Children of `parent`.
     *
     * Must not change during destruction (i.e. when `parent`==NULL).
     */
    swl_unLiList_t children;

    /**
     * Methods that can be called on items of `children`.
     * Never NULL.
     */
    const swl_bdc_child_type_t* childType;
} swl_bdc_t;

bool swl_bdc_init(swl_bdc_t* self, void* parent, const swl_bdc_child_type_t* childType);

void swl_bdc_cleanup(swl_bdc_t* self);

bool swl_bdc_add(swl_bdc_t* self, void* child);

bool swl_bdc_remove(swl_bdc_t* self, void* child);

void* swl_bdc_byName(swl_bdc_t* self, const char* name);

typedef swl_unLiListIt_t swl_bdc_it_t;
void* swl_bdc_firstIt(swl_bdc_it_t* tgtIterator, swl_bdc_t* self);
bool swl_bdc_it_isValid(swl_bdc_it_t* it);
void* swl_bdc_it_next(swl_bdc_it_t* it);
#define swl_bdc_for_each(item, list) \
    for(swl_bdc_firstIt(&item, list); swl_bdc_it_isValid(&item); swl_bdc_it_next(&item))
#define swl_bdc_for_each_ext(iterator, elemPtr, listPtr) \
    for(elemPtr = swl_bdc_firstIt(&iterator, listPtr); elemPtr; elemPtr = swl_bdc_it_next(&iterator))
#define swl_bdc_it_data(item, type) \
    swl_unLiList_data(item, type)

#endif
