/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_MAC_H_
#define SRC_INCLUDE_SWL_COMMON_MAC_H_

#include <swl/swl_common.h>
#include "swl/swl_common_type.h"

#define SWL_MAC_BIN_LEN 6
#define SWL_MAC_CHAR_LEN 18

#define SWL_MAC_FMT   "%02X:%02X:%02X:%02X:%02X:%02X"
#define SWL_MAC_ARG(mac) (mac)[0], (mac)[1], (mac)[2], (mac)[3], (mac)[4], (mac)[5]

#define SWL_MAC_CHAR_TO_BIN(tgtBin, srcChar) swl_mac_charToBin((swl_macBin_t*) tgtBin, (swl_macChar_t*) srcChar)
#define SWL_MAC_BIN_TO_CHAR(tgtChar, srcBin) swl_mac_binToChar((swl_macChar_t*) tgtChar, (swl_macBin_t*) srcBin)

#define SWL_MAC_BIN_MATCHES(srcBin1, srcBin2) swl_mac_binMatches((swl_macBin_t*) srcBin1, (swl_macBin_t*) srcBin2)
#define SWL_MAC_CHAR_MATCHES(srcChar1, srcChar2) swl_mac_charMatches((swl_macChar_t*) srcChar1, (swl_macChar_t*) srcChar2)

typedef struct {
    swl_bit8_t bMac[SWL_MAC_BIN_LEN];
} swl_macBin_t;

typedef struct {
    swl_macBin_t mac;
    swl_macBin_t mask;
} swl_macMask_t;

typedef struct {
    char cMac[SWL_MAC_CHAR_LEN];
} swl_macChar_t;

static const swl_macBin_t g_swl_macBin_bCast = {{255, 255, 255, 255, 255, 255}};
static const swl_macBin_t g_swl_macBin_null = {{0, 0, 0, 0, 0, 0}};


#define SWL_MAC_CHAR_BCAST "FF:FF:FF:FF:FF:FF"
#define SWL_MAC_CHAR_NULL "00:00:00:00:00:00"

#define SWL_MAC_BIN_NEW() {.bMac = {0, 0, 0, 0, 0, 0}}
#define SWL_MAC_CHAR_NEW() {.cMac = SWL_MAC_CHAR_NULL}

static const swl_macChar_t g_swl_macChar_bCast = {.cMac = SWL_MAC_CHAR_BCAST};
static const swl_macChar_t g_swl_macChar_null = {.cMac = SWL_MAC_CHAR_NULL};

bool swl_mac_charToBin(swl_macBin_t* tgtChar, const swl_macChar_t* srcBin);
bool swl_mac_binToChar(swl_macChar_t* tgtChar, const swl_macBin_t* srcBin);
bool swl_mac_binToCharSep(swl_macChar_t* tgtCMac, const swl_macBin_t* srcBMac, bool upperCase, char separator);

bool swl_mac_binMatches(const swl_macBin_t* bMac1, const swl_macBin_t* bMac2);
bool swl_mac_charMatches(const swl_macChar_t* cMac1, const swl_macChar_t* cMac2);

bool swl_mac_charIsStandard(const swl_macChar_t* cMac);
bool swl_mac_charToStandard(swl_macChar_t* cMac, const char* strMac);

bool swl_mac_binAddVal(swl_macBin_t* bMac, int64_t value, uint32_t nrMaskBits);
bool swl_mac_charAddVal(swl_macChar_t* cMac, int64_t value, uint32_t nrMaskBits);

bool swl_mac_binIsLocal(const swl_macBin_t* bMac);
bool swl_mac_charIsLocal(const swl_macChar_t* cMac);
bool swl_mac_binIsBroadcast(const swl_macBin_t* bMac);
bool swl_mac_charIsBroadcast(const swl_macChar_t* cMac);
bool swl_mac_charIsValidStaMac(const swl_macChar_t* cMac);
bool swl_mac_binIsNull(const swl_macBin_t* bMac);
void swl_mac_binClear(swl_macBin_t* bMac);
bool swl_mac_charIsNull(const swl_macChar_t* cMac);
void swl_mac_charClear(swl_macChar_t* cMac);
bool swl_mac_binIsMulticast(const swl_macBin_t* bMac);
bool swl_mac_charIsMulticast(const swl_macChar_t* cMac);

bool swl_macMask_binMatches(const swl_macMask_t* mask, const swl_macBin_t* mac);
bool swl_macMask_charMatches(const swl_macMask_t* mask, const swl_macChar_t* mac);

bool swl_mac_binMatchesMask(const swl_macBin_t* mac1, const swl_macBin_t* mac2, const swl_macBin_t* mask);
bool swl_mac_charMatchesMask(const swl_macChar_t* mac1, const swl_macChar_t* mac2, const swl_macChar_t* mask);

#define swl_type_macBin &gtSwl_type_macBin
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(MacBin, gtSwl_type_macBin, swl_macBin_t)
SWL_REF_TYPE_H(gtSwl_type_macBinPtr, gtSwl_type_macBin);

#define swl_type_macChar &gtSwl_type_macChar
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(MacChar, gtSwl_type_macChar, swl_macChar_t)
SWL_REF_TYPE_H(gtSwl_type_macCharPtr, gtSwl_type_macChar);

#endif /* SRC_INCLUDE_SWL_MAC_H_ */
