/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_TYPE_H_
#define SRC_INCLUDE_SWL_COMMON_TYPE_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

#include <errno.h>


#include "swl/swl_returnCode.h"
#include "swl/swl_common.h"
#include "swl/swl_buf.h"
#include "swl/swl_llist.h"
#include "swl/swl_type_types.h"
#include "swl/types/swl_refType.h"


#define SWL_TYPE_BUF_SIZE 512

#define SWL_TYPE_TO_PTR(type, ptr) (type->typeFun->isValue ? (swl_typeData_t*) (ptr) :*(swl_typeData_t**) (ptr))
#define SWL_TYPE_EL_TO_DATA(type, swlTypeEl) (type->typeFun->isValue ? (swl_typeData_t*) (swlTypeEl) :*(swl_typeData_t**) (swlTypeEl))
#define SWL_TYPE_GET_PTR(type, val) (swl_typeData_t*) (type->typeFun->isValue ? &(ptr) : (ptr))
swl_typeData_t* swl_type_toPtr(swl_type_t* type, const swl_typeEl_t* ptr);
swl_typeExtFun_t* swl_type_getExtFun(swl_type_t* type, swl_typeExtUID_t id);

bool swl_type_equals(swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1);
swl_typeData_t* swl_type_copy(swl_type_t* type, const swl_typeData_t* src);
void swl_type_copyTo(swl_type_t* type, swl_typeEl_t* tgt, const swl_typeData_t* src);
void swl_type_cleanup(swl_type_t* type, swl_typeEl_t* value);
void swl_type_cleanupPtr(swl_type_t* type, swl_typeData_t** value);

ssize_t swl_type_toChar(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData);
ssize_t swl_type_toCharEsc(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData, const swl_print_args_t* args);
bool swl_type_toFile(swl_type_t* type, FILE* stream, const swl_typeData_t* srcData, const swl_print_args_t* args);
bool swl_type_toFileName(swl_type_t* type, const char* fileName, const swl_typeData_t* srcData, const swl_print_args_t* inArgs, bool append);
bool swl_type_toFilePriv(swl_type_t* type, FILE* stream, const swl_typeData_t* srcData, swl_print_args_t* args);
bool swl_type_toFilePrivPrint(swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args);
bool swl_type_fromFile(swl_type_t* type, swl_typeEl_t* tgtData, FILE* stream, uint32_t size, const swl_print_args_t* inArgs);
bool swl_type_fromChar(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr);
bool swl_type_fromCharExt(swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args);
ssize_t swl_type_fromCharPrint(swl_type_t* type, swl_typeEl_t* data, const char* srcStr, const swl_print_args_t* args, swl_print_delim_e delim);
bool swl_type_isEmpty(swl_type_t* type, swl_typeData_t* data);
void swl_type_toBuf(swl_type_t* type, char* buf, size_t bufSize, const swl_typeData_t* srcData);
swl_buf32_t swl_type_toBuf32(swl_type_t* type, const swl_typeData_t* srcData);
swl_buf128_t swl_type_toBuf128(swl_type_t* type, const swl_typeData_t* srcData);
swl_typeFlag_e swl_type_getFlag(swl_type_t* type);
bool swl_type_fromFileName(swl_type_t* type, swl_typeEl_t* tgtData, const char* fileName, const swl_print_args_t* inArgs);
char* swl_type_toCString(swl_type_t* type, const swl_typeData_t* srcData);
char* swl_type_toCStringExt(swl_type_t* type, const swl_typeData_t* srcData, const swl_print_args_t* args);

swl_rc_ne swl_type_arrayAppend(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_type_arrayPrepend(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index);
swl_rc_ne swl_type_arrayCleanAt(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t index);
swl_rc_ne swl_type_arrayDelete(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t index);
swl_rc_ne swl_type_arraySet(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, swl_typeData_t* data, ssize_t index);
void swl_type_arrayCircShift(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t shiftSize);
void swl_type_arrayShift(swl_type_t* type, swl_typeEl_t* array, size_t arraySize, ssize_t shiftSize);
swl_rc_ne swl_type_arrayCopy(swl_type_t* type, swl_typeEl_t* tgtArray, const swl_typeEl_t* srcArray, size_t size);
swl_typeData_t* swl_type_arrayGetValue(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, ssize_t index);
swl_typeEl_t* swl_type_arrayGetReference(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, ssize_t index);
swl_rc_ne swl_type_arrayGet(swl_type_t* type, swl_typeEl_t* tgt, const swl_typeEl_t* array, size_t arraySize, ssize_t index);
swl_rc_ne swl_type_arrayCleanup(swl_type_t* type, swl_typeEl_t* array, size_t arraySize);
size_t swl_type_arraySize(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize);
size_t swl_type_arrayNotEmpty(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize);
ssize_t swl_type_getFirstEmpty(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize);
ssize_t swl_type_getNonEmptyIndex(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, size_t index);

#define SWL_TYPE_MAX_DEPTH 8

typedef struct {
    uint8_t depth;
    int8_t index[SWL_TYPE_MAX_DEPTH];
    swl_typeData_t* key[SWL_TYPE_MAX_DEPTH];
} swl_typeCallInfo_t;

typedef void (* swl_type_callOnBaseTypes_f) (swl_type_t* numberType, swl_typeEl_t** array, size_t arraySize, void* userData, swl_typeCallInfo_t* info);
void swl_type_callOnBaseTypes(swl_type_t* type, swl_typeEl_t** array, size_t arraySize, swl_type_callOnBaseTypes_f callFun, void* userData);

size_t swl_type_arrayFromChar(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const char* srcStr);
size_t swl_type_arrayFromCharSep(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const char* srcStr, const char* sep);
size_t swl_type_arrayToChar(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeEl_t* srcArray, size_t srcArraySize);
void swl_type_arrayToBuf(swl_type_t* type, char* buf, size_t bufSize, const swl_typeEl_t* srcArray, size_t srcArraySize);
swl_buf32_t swl_type_arrayToBuf32(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize);
swl_buf128_t swl_type_arrayToBuf128(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize);
ssize_t swl_type_arrayToCharPrint(swl_type_t* type, char* tgtStr, size_t tgtStrSize,
                                  const swl_typeEl_t* srcArray, size_t srcArraySize, bool printEmpty);
bool swl_type_arrayToFilePrint(swl_type_t* type, FILE* file,
                               const swl_typeEl_t* srcArray, size_t srcArraySize, bool printEmpty, swl_print_args_t* args);
char* swl_type_arrayToCStringExt(swl_type_t* type,
                                 const swl_typeEl_t* srcArray, size_t srcArraySize,
                                 bool printEmpty, swl_print_args_t* args);
char* swl_type_arrayToCString(swl_type_t* type, const swl_typeEl_t* srcArray, size_t srcArraySize);
bool swl_type_arrayFromCharExt(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize,
                               const char* srcStr, const swl_print_args_t* args, size_t* nrParsed);
//Private function, size must not be 0, args must not be NULL
bool swl_type_arrayFromFilePriv(swl_type_t* type,
                                swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                FILE* stream, size_t size,
                                const swl_print_args_t* inArgs, size_t* nrParsed);
bool swl_type_arrayFromFile(swl_type_t* type,
                            swl_typeEl_t* tgtArray, size_t tgtArraySize,
                            FILE* stream, size_t fileSize,
                            const swl_print_args_t* inArgs, size_t* nrParsed);
bool swl_type_arrayFromFileName(swl_type_t* type,
                                swl_typeEl_t* tgtArray, size_t tgtArraySize,
                                const char* fileName,
                                const swl_print_args_t* inArgs, size_t* nrParsed);
size_t swl_type_arrayToCharSep(swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeEl_t* srcArray, size_t srcArraySize, const char* sep);
bool swl_type_arrayEquals(swl_type_t* type, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size);
size_t swl_type_arrayCount(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeEl_t* val);
ssize_t swl_type_arrayFindOffset(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val, ssize_t offset);
ssize_t swl_type_arrayFind(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val);
swl_typeEl_t* swl_type_arrayGetByData(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val);
bool swl_type_arrayContains(swl_type_t* type, const swl_typeEl_t* array, size_t arraySize, const swl_typeData_t* val);
size_t swl_type_arrayDiff(swl_type_t* type, swl_typeEl_t* tgtArray, size_t tgtArraySize, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size);
bool swl_type_arrayMatches(swl_type_t* type, const swl_typeEl_t* array1, size_t array1Size, const swl_typeEl_t* array2, size_t array2Size);
bool swl_type_arrayIsSuperset(swl_type_t* type, const swl_typeEl_t* superArray, size_t superArraySize, const swl_typeEl_t* subArray, size_t subArraySize);

size_t swl_type_extractValueArrayFromStructArray(swl_type_t* type, swl_typeEl_t* tgtArray, const swl_typeEl_t* srcStructArray, size_t offset, size_t structSize, size_t curIndex, size_t nrValues);


#define SWL_TYPE_ARRAYCOUNTVAL(type, array, arraySize, value) \
    ({ __typeof__ (value) _tmpValue = (value); \
         swl_type_arrayCount(type, array, arraySize, &_tmpValue);})

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_BASE(name, swlType, cType) \
    static inline size_t swl_type ## name ## _arrayCleanup(cType * array, size_t arraySize) { \
        return swl_type_arrayCleanup(swlType, array, arraySize);} \
    static inline bool swl_type ## name ## _arrayEquals(cType * array1, size_t array1Size, cType * array2, size_t array2Size) { \
        return swl_type_arrayEquals(swlType, array1, array1Size, array2, array2Size);} \
    static inline size_t swl_type ## name ## _arrayDiff(cType * tgtArray, size_t tgtArraySize, cType * array1, size_t array1Size, cType * array2, size_t array2Size) { \
        return swl_type_arrayDiff(swlType, tgtArray, tgtArraySize, array1, array1Size, array2, array2Size);} \
    static inline size_t swl_type ## name ## _arrayMatches(cType * array1, size_t array1Size, cType * array2, size_t array2Size) { \
        return swl_type_arrayMatches(swlType, array1, array1Size, array2, array2Size);} \
    static inline size_t swl_type ## name ## _arrayIsSuperset(cType * superArray, size_t superArraySize, cType * subArray, size_t subArraySize) { \
        return swl_type_arrayIsSuperset(swlType, superArray, superArraySize, subArray, subArraySize);} \
    static inline size_t swl_type ## name ## _extractValueArrayFromStructArray(cType * tgtArray, void* srcStructArray, size_t offset, size_t structSize, size_t curIndex, size_t nrValues) { \
        return swl_type_extractValueArrayFromStructArray(swlType, tgtArray, srcStructArray, offset, structSize, curIndex, nrValues);}

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_STR(name, swlType, cType) \
    static inline size_t swl_type ## name ## _arrayFromChar(cType * tgtArray, size_t tgtArraySize, const char* srcStr) { \
        return swl_type_arrayFromChar(swlType, tgtArray, tgtArraySize, srcStr);} \
    static inline size_t swl_type ## name ## _arrayFromCharSep(cType * tgtArray, size_t tgtArraySize, const char* srcStr, const char* sep) { \
        return swl_type_arrayFromCharSep(swlType, tgtArray, tgtArraySize, srcStr, sep);} \
    static inline size_t swl_type ## name ## _arrayToChar(char* tgtStr, size_t tgtStrSize, cType * srcArray, size_t srcArraySize) { \
        return swl_type_arrayToChar(swlType, tgtStr, tgtStrSize, srcArray, srcArraySize);} \
    static inline size_t swl_type ## name ## _arrayToCharSep(char* tgtStr, size_t tgtStrSize, cType * srcArray, size_t srcArraySize, const char* sep) { \
        return swl_type_arrayToCharSep(swlType, tgtStr, tgtStrSize, srcArray, srcArraySize, sep);}

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_BASE(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_STR(name, swlType, cType)

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_BASE(name, swlType, cType) \
    static inline bool swl_type ## name ## _equals(cType key0, cType key1) { \
        return swl_type_equals(swlType, &key0, &key1);} \
    static inline bool swl_type ## name ## _equalsRef(cType * key0, cType * key1) { \
        return swl_type_equals(swlType, key0, key1);} \
    static inline cType swl_type ## name ## _copy(cType src) { \
        cType* tmpVal = (cType*) swl_type_copy(swlType, &src); \
        cType retVal = *tmpVal; \
        free(tmpVal); \
        return retVal;} \
    static inline cType* swl_type ## name ## _copyRef(cType * src) { \
        return swl_type_copy(swlType, src);} \
    static inline size_t swl_type ## name ## _arrayCount(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayCount(swlType, array, arraySize, &value);} \
    static inline bool swl_type ## name ## _arrayContains(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayContains(swlType, array, arraySize, &value);} \
    static inline size_t swl_type ## name ## _arrayCountRef(cType * array, size_t arraySize, const cType * value) { \
        return swl_type_arrayCount(swlType, array, arraySize, value);} \
    static inline bool swl_type ## name ## _arrayContainsRef(cType * array, size_t arraySize, const cType * value) { \
        return swl_type_arrayContains(swlType, array, arraySize, value);} \
    static inline ssize_t swl_type ## name ## _arrayFind(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayFind(swlType, array, arraySize, &value); } \
    static inline ssize_t swl_type ## name ## _arrayFindRef(cType * array, size_t arraySize, const cType * value) { \
        return swl_type_arrayFind(swlType, array, arraySize, value); } \
    static inline ssize_t swl_type ## name ## _arrayFindOffset(cType * array, size_t arraySize, cType value, ssize_t offset) { \
        return swl_type_arrayFindOffset(swlType, array, arraySize, &value, offset); } \
    static inline ssize_t swl_type ## name ## _arrayFindOffsetRef(cType * array, size_t arraySize, const cType * value, ssize_t offset) { \
        return swl_type_arrayFindOffset(swlType, array, arraySize, value, offset); } \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_BASE(name, swlType, cType)

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_STR(name, swlType, cType) \
    static inline bool swl_type ## name ## _toChar(char* tgtStr, size_t tgtStrSize, cType srcData) { \
        return swl_type_toChar(swlType, tgtStr, tgtStrSize, &srcData);} \
    static inline bool swl_type ## name ## _toCharRef(char* tgtStr, size_t tgtStrSize, cType * srcData) { \
        return swl_type_toChar(swlType, tgtStr, tgtStrSize, srcData);} \
    static inline bool swl_type ## name ## _fromChar(cType * tgtData, const char* srcStr) { \
        return swl_type_fromChar(swlType, tgtData, srcStr);} \
    static inline cType swl_type ## name ## _fromCharDef(const char* srcStr, cType defaultVal) { \
        cType tmpVal; \
        memset(&tmpVal, 0, (swlType)->size); \
        if(swl_type_fromChar(swlType, &tmpVal, srcStr)) { \
            return tmpVal; \
        } \
        return defaultVal;} \
    static inline swl_buf32_t swl_type ## name ## _toBuf32(cType tgtData) { \
        return swl_type_toBuf32(swlType, &tgtData);} \
    static inline swl_buf32_t swl_type ## name ## _toBuf32Ref(cType * tgtData) { \
        return swl_type_toBuf32(swlType, tgtData);} \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC_STR(name, swlType, cType)

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_BASE(name, swlType, cType) \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_STR(name, swlType, cType)

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_REF(name, swlType, cType) \
    static inline bool swl_type ## name ## _equals(cType key0, cType key1) { \
        return swl_type_equals(swlType, key0, key1);} \
    static inline cType swl_type ## name ## _copy(cType src) { \
        return swl_type_copy(swlType, src);} \
    static inline bool swl_type ## name ## _toChar(char* tgtStr, size_t tgtStrSize, cType srcData) { \
        return swl_type_toChar(swlType, tgtStr, tgtStrSize, srcData);} \
    static inline bool swl_type ## name ## _fromChar(cType * tgtData, const char* srcStr) { \
        return swl_type_fromChar(swlType, tgtData, srcStr);} \
    static inline cType swl_type ## name ## _fromCharDef(const char* srcStr, cType defaultVal) { \
        cType tmpVal; \
        memset(&tmpVal, 0, (swlType)->size); \
        if(swl_type_fromChar(swlType, &tmpVal, srcStr)) { \
            return tmpVal; \
        } \
        if(defaultVal) { \
            return swl_type_copy(swlType, defaultVal); \
        } \
        return NULL;} \
    static inline swl_buf32_t swl_type ## name ## _toBuf32(cType tgtData) { \
        return swl_type_toBuf32(swlType, tgtData);} \
    static inline size_t swl_type ## name ## _arrayCount(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayCount(swlType, array, arraySize, value);} \
    static inline size_t swl_type ## name ## _arrayContains(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayCount(swlType, array, arraySize, value);} \
    static inline ssize_t swl_type ## name ## _arrayFind(cType * array, size_t arraySize, cType value) { \
        return swl_type_arrayFind(swlType, array, arraySize, value); } \
    static inline ssize_t swl_type ## name ## _arrayFindOffset(cType * array, size_t arraySize, cType value, ssize_t offset) { \
        return swl_type_arrayFindOffset(swlType, array, arraySize, value, offset); } \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_GENERIC(name, swlType, cType)

extern swl_typeFun_t swl_type_charbuf_fun;

#define SWL_TYPE_CHARBUF_IMPL(bufSize) \
    swl_type_t gtSwl_type_charBuf ## bufSize = { \
        .typeFun = &swl_type_charbuf_fun, \
        .size = bufSize, \
    };

#define SWL_TYPE_CHARBUF_HEADER(bufSize) \
    extern swl_type_t gtSwl_type_charBuf ## bufSize; \
    typedef char gtSwl_type_charBuf ## bufSize ## _type [bufSize]; \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL(CharBuf ## bufSize, &gtSwl_type_charBuf ## bufSize, char*)



#define SWL_TYPE_CAST2VOIDPTR(x) (void*) (uintptr_t) (x)



//Need to define types as references to static memory, to allow use in static structures.
#define swl_type_bool &gtSwl_type_bool
#define swl_type_int8 &gtSwl_type_int8
#define swl_type_int16 &gtSwl_type_int16
#define swl_type_int32 &gtSwl_type_int32
#define swl_type_int64 &gtSwl_type_int64
#define swl_type_uint8 &gtSwl_type_uint8
#define swl_type_uint16 &gtSwl_type_uint16
#define swl_type_uint32 &gtSwl_type_uint32
#define swl_type_uint64 &gtSwl_type_uint64
//Note, no float support,as float will already overflow on surprisingly low values. If you need floating point, use double instead.
#define swl_type_double &gtSwl_type_double
#define swl_type_charPtr &gtSwl_type_charPtr
#define swl_type_voidPtr &gtSwl_type_voidPtr

/**
 * Define a type and add associated list of functions for a value type
 * @param funcName: the function name string, to be added for names such swl_type<FunctionName>_equals()
 * @param typeVariable: the swl_type_t variable name (not pointer name)
 */
#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(funcName, typeVar, cType) \
    extern swl_type_t typeVar; \
    typedef cType typeVar ## _type; \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL(funcName, &typeVar, cType)

#define SWL_COMMON_TYPE_DEFINE_FUNCTIONS_REF_EXT(funcName, typeVar, cType) \
    extern swl_type_t typeVar; \
    typedef cType typeVar ## _type; \
    SWL_COMMON_TYPE_DEFINE_FUNCTIONS_REF(funcName, &typeVar, cType)


SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Bool, gtSwl_type_bool, bool)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Int8, gtSwl_type_int8, int8_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Int16, gtSwl_type_int16, int16_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Int32, gtSwl_type_int32, int32_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Int64, gtSwl_type_int64, int64_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(UInt8, gtSwl_type_uint8, uint8_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(UInt16, gtSwl_type_uint16, uint16_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(UInt32, gtSwl_type_uint32, uint32_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(UInt64, gtSwl_type_uint64, uint64_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Double, gtSwl_type_double, double)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_REF_EXT(CharPtr, gtSwl_type_charPtr, char*)
//Note, void pointers are compared on their own values, not the memory pointing to.
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(VoidPtr, gtSwl_type_voidPtr, void*)

SWL_REF_TYPE_H(gtSwl_type_boolPtr, gtSwl_type_bool);
SWL_REF_TYPE_H(gtSwl_type_int8Ptr, gtSwl_type_int8);
SWL_REF_TYPE_H(gtSwl_type_int16Ptr, gtSwl_type_int16);
SWL_REF_TYPE_H(gtSwl_type_int32Ptr, gtSwl_type_int32);
SWL_REF_TYPE_H(gtSwl_type_int64Ptr, gtSwl_type_int64);
SWL_REF_TYPE_H(gtSwl_type_uint8Ptr, gtSwl_type_uint8);
SWL_REF_TYPE_H(gtSwl_type_uint16Ptr, gtSwl_type_uint16);
SWL_REF_TYPE_H(gtSwl_type_uint32Ptr, gtSwl_type_uint32);
SWL_REF_TYPE_H(gtSwl_type_uint64Ptr, gtSwl_type_uint64);
SWL_REF_TYPE_H(gtSwl_type_doublePtr, gtSwl_type_double);


#define swl_type_charBuf4 &swl_type_charBuf4_impl
#define swl_type_charBuf8 &swl_type_charBuf8_impl
#define swl_type_charBuf16 &swl_type_charBuf16_impl
#define swl_type_charBuf18 &swl_type_charBuf18_impl
#define swl_type_charBuf32 &swl_type_charBuf32_impl
#define swl_type_charBuf64 &swl_type_charBuf64_impl
#define swl_type_charBuf128 &swl_type_charBuf128_impl


SWL_TYPE_CHARBUF_HEADER(4);
SWL_TYPE_CHARBUF_HEADER(8);
SWL_TYPE_CHARBUF_HEADER(16);
SWL_TYPE_CHARBUF_HEADER(18);
SWL_TYPE_CHARBUF_HEADER(32);
SWL_TYPE_CHARBUF_HEADER(64);
SWL_TYPE_CHARBUF_HEADER(128);

//Defines for backward compatiblity: deprecated
#define swl_type_int8_impl gtSwl_type_int8
#define swl_type_int16_impl gtSwl_type_int16
#define swl_type_int32_impl gtSwl_type_int32
#define swl_type_int64_impl gtSwl_type_int64
#define swl_type_uint8_impl gtSwl_type_uint8
#define swl_type_uint16_impl gtSwl_type_uint16
#define swl_type_uint32_impl gtSwl_type_uint32
#define swl_type_uint64_impl gtSwl_type_uint64

#define swl_type_charBuf4_impl gtSwl_type_charBuf4
#define swl_type_charBuf8_impl gtSwl_type_charBuf8
#define swl_type_charBuf16_impl gtSwl_type_charBuf16
#define swl_type_charBuf18_impl gtSwl_type_charBuf18
#define swl_type_charBuf32_impl gtSwl_type_charBuf32
#define swl_type_charBuf64_impl  gtSwl_type_charBuf64
#define swl_type_charBuf128_impl gtSwl_type_charBuf128

#define swl_type_double_impl gtSwl_type_double
#define swl_type_voidPtr_impl gtSwl_type_voidPtr
#define swl_type_charPtr_impl gtSwl_type_charPtr

#endif /* SRC_INCLUDE_SWL_TYPE_H_ */
