/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_SECURITY_H_
#define SRC_INCLUDE_SWL_SECURITY_H_

#include "swl/swl_80211.h"


/**
 * Definition of possible modes the accesspoint is configured in
 */
typedef enum {
    SWL_SECURITY_APMODE_UNKNOWN,
    SWL_SECURITY_APMODE_NONE,
    SWL_SECURITY_APMODE_WEP64,
    SWL_SECURITY_APMODE_WEP128,
    SWL_SECURITY_APMODE_WEP128IV,
    SWL_SECURITY_APMODE_WPA_P,
    SWL_SECURITY_APMODE_WPA2_P,
    SWL_SECURITY_APMODE_WPA_WPA2_P,
    SWL_SECURITY_APMODE_WPA3_P,
    SWL_SECURITY_APMODE_WPA2_WPA3_P,
    SWL_SECURITY_APMODE_WPA_E,
    SWL_SECURITY_APMODE_WPA2_E,
    SWL_SECURITY_APMODE_WPA_WPA2_E,
    SWL_SECURITY_APMODE_WPA3_E,
    SWL_SECURITY_APMODE_WPA2_WPA3_E,
    SWL_SECURITY_APMODE_OWE,
    SWL_SECURITY_APMODE_AUTO,
    SWL_SECURITY_APMODE_UNSUPPORTED,
    SWL_SECURITY_APMODE_MAX
} swl_security_apMode_e;

#define M_SWL_SECURITY_APMODE_UNKNOWN (1 << SWL_SECURITY_APMODE_UNKNOWN)
#define M_SWL_SECURITY_APMODE_NONE (1 << SWL_SECURITY_APMODE_NONE)
#define M_SWL_SECURITY_APMODE_WEP64 (1 << SWL_SECURITY_APMODE_WEP64)
#define M_SWL_SECURITY_APMODE_WEP128 (1 << SWL_SECURITY_APMODE_WEP128)
#define M_SWL_SECURITY_APMODE_WEP128IV (1 << SWL_SECURITY_APMODE_WEP128IV)
#define M_SWL_SECURITY_APMODE_WPA_P (1 << SWL_SECURITY_APMODE_WPA_P)
#define M_SWL_SECURITY_APMODE_WPA2_P (1 << SWL_SECURITY_APMODE_WPA2_P)
#define M_SWL_SECURITY_APMODE_WPA_WPA2_P (1 << SWL_SECURITY_APMODE_WPA_WPA2_P)
#define M_SWL_SECURITY_APMODE_WPA3_P (1 << SWL_SECURITY_APMODE_WPA3_P)
#define M_SWL_SECURITY_APMODE_WPA2_WPA3_P (1 << SWL_SECURITY_APMODE_WPA2_WPA3_P)
#define M_SWL_SECURITY_APMODE_WPA_E (1 << SWL_SECURITY_APMODE_WPA_E)
#define M_SWL_SECURITY_APMODE_WPA2_E (1 << SWL_SECURITY_APMODE_WPA2_E)
#define M_SWL_SECURITY_APMODE_WPA_WPA2_E (1 << SWL_SECURITY_APMODE_WPA_WPA2_E)
#define M_SWL_SECURITY_APMODE_WPA3_E (1 << SWL_SECURITY_APMODE_WPA3_E)
#define M_SWL_SECURITY_APMODE_WPA2_WPA3_E (1 << SWL_SECURITY_APMODE_WPA2_WPA3_E)
#define M_SWL_SECURITY_APMODE_OWE (1 << SWL_SECURITY_APMODE_OWE)
#define M_SWL_SECURITY_APMODE_AUTO (1 << SWL_SECURITY_APMODE_AUTO)
#define M_SWL_SECURITY_APMODE_UNSUPPORTED (1 << SWL_SECURITY_APMODE_UNSUPPORTED)

#define M_SWL_SECURITY_APMODE_WEP_ALL_TYPES (M_SWL_SECURITY_APMODE_WEP64 | M_SWL_SECURITY_APMODE_WEP128 | M_SWL_SECURITY_APMODE_WEP128IV)
#define M_SWL_SECURITY_APMODE_WPA2_PERSONAL_TYPES (M_SWL_SECURITY_APMODE_WPA2_P | M_SWL_SECURITY_APMODE_WPA_WPA2_P)
#define M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES (M_SWL_SECURITY_APMODE_WPA3_P | M_SWL_SECURITY_APMODE_WPA2_WPA3_P)
#define M_SWL_SECURITY_APMODE_WPA_ALL_PERSONAL_TYPES (M_SWL_SECURITY_APMODE_WPA_P | M_SWL_SECURITY_APMODE_WPA2_PERSONAL_TYPES | M_SWL_SECURITY_APMODE_WPA3_PERSONAL_TYPES)

typedef enum {
    SWL_SECURITY_CONMODE_UNKNOWN,
    SWL_SECURITY_CONMODE_NONE,
    SWL_SECURITY_CONMODE_WEP64,
    SWL_SECURITY_CONMODE_WEP128,
    SWL_SECURITY_CONMODE_WEP128IV,
    SWL_SECURITY_CONMODE_WPA_P,
    SWL_SECURITY_CONMODE_WPA2_P,
    SWL_SECURITY_CONMODE_WPA3_P,
    SWL_SECURITY_CONMODE_WPA_E,
    SWL_SECURITY_CONMODE_WPA2_E,
    SWL_SECURITY_CONMODE_WPA3_E,
    SWL_SECURITY_CONMODE_OWE,
    SWL_SECURITY_CONMODE_MAX
} swl_security_conMode_e;

#define M_SWL_SECURITY_CONMODE_UNKNOWN (1 << SWL_SECURITY_CONMODE_UNKNOWN)
#define M_SWL_SECURITY_CONMODE_NONE (1 << SWL_SECURITY_CONMODE_NONE)
#define M_SWL_SECURITY_CONMODE_WEP64 (1 << SWL_SECURITY_CONMODE_WEP64)
#define M_SWL_SECURITY_CONMODE_WEP128 (1 << SWL_SECURITY_CONMODE_WEP128)
#define M_SWL_SECURITY_CONMODE_WEP128IV (1 << SWL_SECURITY_CONMODE_WEP128IV)
#define M_SWL_SECURITY_CONMODE_WPA_P (1 << SWL_SECURITY_CONMODE_WPA_P)
#define M_SWL_SECURITY_CONMODE_WPA2_P (1 << SWL_SECURITY_CONMODE_WPA2_P)
#define M_SWL_SECURITY_CONMODE_WPA3_P (1 << SWL_SECURITY_CONMODE_WPA3_P)
#define M_SWL_SECURITY_CONMODE_WPA_E (1 << SWL_SECURITY_CONMODE_WPA_E)
#define M_SWL_SECURITY_CONMODE_WPA2_E (1 << SWL_SECURITY_CONMODE_WPA2_E)
#define M_SWL_SECURITY_CONMODE_WPA3_E (1 << SWL_SECURITY_CONMODE_WPA3_E)
#define M_SWL_SECURITY_CONMODE_OWE (1 << SWL_SECURITY_CONMODE_OWE)

//Tr 181 standard way of defining security modes, as per Device.WiFi.AccessPoint.{i}.Security.
extern const char* swl_security_apMode_str[];
//Alternate TR181 way as stated in Device.WiFi.AccessPoint.{i}.Security.
extern const char* swl_security_apModeAlt_str[];
//Alternate TR181 way as stated in Device.WiFi.NeighboringWiFiDiagnostic.Result.{i}.
//Note that all WEP entries are encoded as a single string
extern const char* swl_security_diagMode_str[];
//Legacy way of writing security modes of Device.WiFi.AccessPoint.{i}.Security
extern const char* swl_security_apModeLeg_str[];
typedef uint32_t swl_security_apMode_m;

extern const char* swl_security_conMode_str[];
typedef uint32_t swl_security_conMode_m;


extern const swl_security_conMode_m swl_security_apModeToConModeMask[];

typedef enum {
    SWL_SECURITY_ENCMODE_DEFAULT,
    SWL_SECURITY_ENCMODE_AES,
    SWL_SECURITY_ENCMODE_TKIP,
    SWL_SECURITY_ENCMODE_TKIPAES,
    SWL_SECURITY_ENCMODE_MAX
} swl_security_encMode_e;
typedef uint32_t swl_security_encMode_m;
extern const char* swl_security_encMode_str[SWL_SECURITY_ENCMODE_MAX];

typedef enum {
    SWL_SECURITY_MFPMODE_DISABLED,
    SWL_SECURITY_MFPMODE_OPTIONAL,
    SWL_SECURITY_MFPMODE_REQUIRED,
    SWL_SECURITY_MFPMODE_MAX
} swl_security_mfpMode_e;
typedef uint32_t swl_security_mfpMode_m;
extern const char* swl_security_mfpMode_str[SWL_SECURITY_MFPMODE_MAX];



typedef enum {
    SWL_SECURITY_APMODEFMT_DEFAULT,
    SWL_SECURITY_APMODEFMT_ALTERNATE,
    SWL_SECURITY_APMODEFMT_LEGACY,
    SWL_SECURITY_APMODEFMT_MAX,
} swl_security_apModeFmt_e;


swl_security_apMode_e swl_security_apModeFromString(char* string);
const char* swl_security_apModeToString(swl_security_apMode_e srcMode, swl_security_apModeFmt_e fmt);
bool swl_security_isApModeValid(swl_security_apMode_e mode);
bool swl_security_apModeMaskToString(char* buffer, size_t bufferSize, swl_security_apModeFmt_e fmt, swl_security_apMode_m mask);
bool swl_security_apModeMaskToStringExt(char* buffer, size_t bufferSize, swl_security_apModeFmt_e fmt, swl_security_apMode_m mask, bool onlyValidModes);
swl_security_apMode_m swl_security_apModeMaskFromString(const char* buffer);
swl_security_apMode_m swl_security_apModeMaskFromStringExt(const char* buffer, bool onlyValidModes);

swl_security_mfpMode_e swl_security_mfpModeFromString(const char* buffer);
const char* swl_security_mfpModeToString(swl_security_mfpMode_e srcMode);
swl_security_mfpMode_e swl_security_getTargetMfpMode(swl_security_apMode_e securityMode, swl_security_mfpMode_e mfpConfig);

bool swl_security_staMatchesAp(swl_security_conMode_m supportedModes, swl_security_apMode_e apMode);

swl_rc_ne swl_security_getMode(swl_security_apMode_e* pSecModeEnabled, swl_80211_cipher_m* cipherSuiteParams, swl_80211_akm_m* akmSuiteParams);
bool swl_security_isApModeWEP(swl_security_apMode_e mode);
bool swl_security_isApModeWPAPersonal(swl_security_apMode_e mode);
bool swl_security_isApModeWPA3Personal(swl_security_apMode_e mode);

#endif /* SRC_INCLUDE_SWL_SECURITY_H_ */
