/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __SWL_STACAP_H__
#define __SWL_STACAP_H__

#include <stdint.h>

// sta capabilities in 802.11x notation
extern const char* const swl_staCapStdNot_str[];
// sta capabilities in abbreviated notation
extern const char* const swl_staCap_str[];

typedef enum {
    SWL_STACAP_RRM,     //Radio Resource Management capability
    SWL_STACAP_BTM,     //Bss Transfer Management capability
    SWL_STACAP_FBT,     //Fast Bss Transfer capability
    SWL_STACAP_QOS_MAP, //Qos Map capability
    SWL_STACAP_PMF,     //PMF capability
    SWL_STACAP_MAX,
} swl_staCap_e;

typedef uint32_t swl_staCap_m;

#define M_SWL_STACAP_RRM (1 << SWL_STACAP_RRM)
#define M_SWL_STACAP_BTM (1 << SWL_STACAP_BTM)
#define M_SWL_STACAP_FBT (1 << SWL_STACAP_FBT)
#define M_SWL_STACAP_QOS_MAP (1 << SWL_STACAP_QOS_MAP)
#define M_SWL_STACAP_PMF (1 << SWL_STACAP_PMF)

typedef enum {
    SWL_STACAP_RRM_LINK_ME,
    SWL_STACAP_RRM_NEIGHBOR_RE,
    SWL_STACAP_RRM_PRALLAL_ME,
    SWL_STACAP_RRM_REPEATED_ME,
    SWL_STACAP_RRM_BEACON_ACTIVE_ME,
    SWL_STACAP_RRM_BEACON_PASSIVE_ME,
    SWL_STACAP_RRM_BEACON_TABLE_ME,
    SWL_STACAP_RRM_BEACON_ME_RE_COND,
    SWL_STACAP_RRM_FRAME_ME,
    SWL_STACAP_RRM_CHAN_LOAD_ME,
    SWL_STACAP_RRM_NOISE_HISTOGRAM_ME,
    SWL_STACAP_RRM_STATS_ME,
    SWL_STACAP_RRM_LCI_ME,
    SWL_STACAP_RRM_LCI_AZIMUTH_ME,
    SWL_STACAP_RRM_TRANS_SC_ME,
    SWL_STACAP_RRM_TRIG_SC_ME,
    SWL_STACAP_RRM_AP_CHAN_RE,
    SWL_STACAP_RRM_MIB,
    SWL_STACAP_RRM_PILOT_TRANS_INFO_ME,
    SWL_STACAP_RRM_NEIG_RE_TSF_OFFSET,
    SWL_STACAP_RRM_RCPI_ME,
    SWL_STACAP_RRM_RSNI_ME,
    SWL_STACAP_RRM_BSS_AVG_ACCESS_DELAY,
    SWL_STACAP_RRM_BSS_AVAIL_ADMISS_CAPACITY,
    SWL_STACAP_RRM_ANTENNA,
    SWL_STACAP_RRM_FTM_RANGE_RE,
    SWL_STACAP_RRM_CIVIC_LOCATION_ME,
    SWL_STACAP_RRM_IDENT_LOCATION_ME,
    SWL_STACAP_RRM_MAX,
} swl_staCapRrmCap_e;

#define M_SWL_STACAP_RRM_LINK_ME (1 << SWL_STACAP_RRM_LINK_ME)
#define M_SWL_STACAP_RRM_NEIGHBOR_RE (1 << SWL_STACAP_RRM_NEIGHBOR_RE)
#define M_SWL_STACAP_RRM_PRALLAL_ME (1 << SWL_STACAP_RRM_PRALLAL_ME)
#define M_SWL_STACAP_RRM_REPEATED_ME (1 << SWL_STACAP_RRM_REPEATED_ME)
#define M_SWL_STACAP_RRM_BEACON_ACTIVE_ME (1 << SWL_STACAP_RRM_BEACON_ACTIVE_ME)
#define M_SWL_STACAP_RRM_BEACON_PASSIVE_ME (1 << SWL_STACAP_RRM_BEACON_PASSIVE_ME)
#define M_SWL_STACAP_RRM_BEACON_TABLE_ME (1 << SWL_STACAP_RRM_BEACON_TABLE_ME)
#define M_SWL_STACAP_RRM_BEACON_ME_RE_COND (1 << SWL_STACAP_RRM_BEACON_ME_RE_COND)
#define M_SWL_STACAP_RRM_FRAME_ME (1 << SWL_STACAP_RRM_FRAME_ME)
#define M_SWL_STACAP_RRM_CHAN_LOAD_ME (1 << SWL_STACAP_RRM_CHAN_LOAD_ME)
#define M_SWL_STACAP_RRM_NOISE_HISTOGRAM_ME (1 << SWL_STACAP_RRM_NOISE_HISTOGRAM_ME)
#define M_SWL_STACAP_RRM_STATS_ME (1 << SWL_STACAP_RRM_STATS_ME)
#define M_SWL_STACAP_RRM_LCI_ME (1 << SWL_STACAP_RRM_LCI_ME)
#define M_SWL_STACAP_RRM_LCI_AZIMUTH_ME (1 << SWL_STACAP_RRM_LCI_AZIMUTH_ME)
#define M_SWL_STACAP_RRM_TRANS_SC_ME (1 << SWL_STACAP_RRM_TRANS_SC_ME)
#define M_SWL_STACAP_RRM_TRIG_SC_ME (1 << SWL_STACAP_RRM_TRIG_SC_ME)
#define M_SWL_STACAP_RRM_AP_CHAN_RE (1 << SWL_STACAP_RRM_AP_CHAN_RE)
#define M_SWL_STACAP_RRM_NEIG_RE_TSF_OFFSET (1 << SWL_STACAP_RRM_NEIG_RE_TSF_OFFSET)
#define M_SWL_STACAP_RRM_RCPI_ME (1 << SWL_STACAP_RRM_RCPI_ME)
#define M_SWL_STACAP_RRM_RSNI_ME (1 << SWL_STACAP_RRM_RSNI_ME)
#define M_SWL_STACAP_RRM_BSS_AVG_ACCESS_DELAY (1 << SWL_STACAP_RRM_BSS_AVG_ACCESS_DELAY)
#define M_SWL_STACAP_RRM_BSS_AVAIL_ADMISS_CAPACITY (1 << SWL_STACAP_RRM_BSS_AVAIL_ADMISS_CAPACITY)
#define M_SWL_STACAP_RRM_ANTENNA (1 << SWL_STACAP_RRM_ANTENNA)
#define M_SWL_STACAP_RRM_FTM_RANGE_RE (1 << SWL_STACAP_RRM_FTM_RANGE_RE)
#define M_SWL_STACAP_RRM_CIVIC_LOCATION_ME (1 << SWL_STACAP_RRM_CIVIC_LOCATION_ME)
#define M_SWL_STACAP_RRM_IDENT_LOCATION_ME (1 << SWL_STACAP_RRM_IDENT_LOCATION_ME)

extern const char* swl_staCapRrm_str[SWL_STACAP_RRM_MAX];
typedef uint32_t swl_staCapRrm_m;

/* HT Capabilities */
typedef enum {
    SWL_STACAP_HT_40MHZ,
    SWL_STACAP_HT_SGI20,
    SWL_STACAP_HT_SGI40,
    SWL_STACAP_HT_40MHZ_INTOL,
    SWL_STACAP_HT_MAX,
} swl_staCapHt_e;

#define M_SWL_STACAP_HT_40MHZ          (1 << SWL_STACAP_HT_40MHZ)
#define M_SWL_STACAP_HT_SGI20          (1 << SWL_STACAP_HT_SGI20)
#define M_SWL_STACAP_HT_SGI40          (1 << SWL_STACAP_HT_SGI40)
#define M_SWL_STACAP_HT_40MHZ_INTOL    (1 << SWL_STACAP_HT_40MHZ_INTOL)
// Defines with incorrect spelling due to legacy.
#define SWL_STACAP_HT_40MHz SWL_STACAP_HT_40MHZ
#define M_SWL_STACAP_HT_40MHz M_SWL_STACAP_HT_40MHZ
extern const char* swl_staCapHt_str[SWL_STACAP_HT_MAX];
typedef uint16_t swl_staCapHt_m;

/* VHT Capabilities */
typedef enum {
    SWL_STACAP_VHT_SGI80,
    SWL_STACAP_VHT_SGI160,
    SWL_STACAP_VHT_SU_BFR,
    SWL_STACAP_VHT_SU_BFE,
    SWL_STACAP_VHT_MU_BFR,
    SWL_STACAP_VHT_MU_BFE,
    SWL_STACAP_VHT_MAX,
} swl_staCapVht_e;
#define M_SWL_STACAP_VHT_SGI80         (1 << SWL_STACAP_VHT_SGI80)
#define M_SWL_STACAP_VHT_SGI160        (1 << SWL_STACAP_VHT_SGI160)
#define M_SWL_STACAP_VHT_SU_BFR        (1 << SWL_STACAP_VHT_SU_BFR)
#define M_SWL_STACAP_VHT_SU_BFE        (1 << SWL_STACAP_VHT_SU_BFE)
#define M_SWL_STACAP_VHT_MU_BFR        (1 << SWL_STACAP_VHT_MU_BFR)
#define M_SWL_STACAP_VHT_MU_BFE        (1 << SWL_STACAP_VHT_MU_BFE)

extern const char* swl_staCapVht_str[SWL_STACAP_VHT_MAX];
typedef uint16_t swl_staCapVht_m;

/* HE Capabilities*/
typedef enum {
    SWL_STACAP_HE_SU_BFR,
    SWL_STACAP_HE_SU_AND_MU_BFE,
    SWL_STACAP_HE_MU_BFR,
    SWL_STACAP_HE_TWT_REQ,
    SWL_STACAP_HE_TWT_RESP,
    SWL_STACAP_HE_TWT_BCAST,
    SWL_STACAP_HE_TWT_SCHED,
    SWL_STACAP_HE_MAX,
} swl_staCapHe_e;
#define M_SWL_STACAP_HE_SU_BFR         (1 << SWL_STACAP_HE_SU_BFR)
#define M_SWL_STACAP_HE_SU_AND_MU_BFE  (1 << SWL_STACAP_HE_SU_AND_MU_BFE)
#define M_SWL_STACAP_HE_MU_BFR         (1 << SWL_STACAP_HE_MU_BFR)
#define M_SWL_STACAP_HE_TWT_REQ        (1 << SWL_STACAP_HE_TWT_REQ)
#define M_SWL_STACAP_HE_TWT_RESP       (1 << SWL_STACAP_HE_TWT_RESP)
#define M_SWL_STACAP_HE_TWT_BCAST      (1 << SWL_STACAP_HE_TWT_BCAST)
#define M_SWL_STACAP_HE_TWT_SCHED      (1 << SWL_STACAP_HE_TWT_SCHED)

extern const char* swl_staCapHe_str[SWL_STACAP_HE_MAX];
typedef uint16_t swl_staCapHe_m;

/* Station Vendor capabilities */
typedef enum {
    SWL_STACAP_VENDOR_MS_WPS,
    SWL_STACAP_VENDOR_WFA_MBO,
    SWL_STACAP_VENDOR_MAX,
} swl_staCapVendor_e;
#define M_SWL_STACAP_VENDOR_MS_WPS       (1 << SWL_STACAP_VENDOR_MS_WPS)
#define M_SWL_STACAP_VENDOR_WFA_MBO      (1 << SWL_STACAP_VENDOR_WFA_MBO)

extern const char* const swl_staCapVendor_str[SWL_STACAP_VENDOR_MAX];
typedef uint16_t swl_staCapVendor_m;

/* EHT Capabilities*/
typedef enum {
    SWL_STACAP_EHT_320MHZ_6GHZ,
    SWL_STACAP_EHT_SU_BEAMFORMER,
    SWL_STACAP_EHT_SU_BEAMFORMEE,
    SWL_STACAP_EHT_MU_BEAMFORMER_80MHZ,
    SWL_STACAP_EHT_MU_BEAMFORMER_160MHZ,
    SWL_STACAP_EHT_MU_BEAMFORMER_320MHZ,
    SWL_STACAP_EHT_MAX,
} swl_staCapEht_e;
#define M_SWL_STACAP_EHT_320MHZ_6GHZ            (1 << SWL_STACAP_EHT_320MHZ_6GHZ)
#define M_SWL_STACAP_EHT_SU_BEAMFORMER          (1 << SWL_STACAP_EHT_SU_BEAMFORMER)
#define M_SWL_STACAP_EHT_SU_BEAMFORMEE          (1 << SWL_STACAP_EHT_SU_BEAMFORMEE)
#define M_SWL_STACAP_EHT_MU_BEAMFORMER_80MHZ    (1 << SWL_STACAP_EHT_MU_BEAMFORMER_80MHZ)
#define M_SWL_STACAP_EHT_MU_BEAMFORMER_160MHZ   (1 << SWL_STACAP_EHT_MU_BEAMFORMER_160MHZ)
#define M_SWL_STACAP_EHT_MU_BEAMFORMER_320MHZ   (1 << SWL_STACAP_EHT_MU_BEAMFORMER_320MHZ)

extern const char* swl_staCapEht_str[SWL_STACAP_EHT_MAX];
typedef uint16_t swl_staCapEht_m;

/* EHT EML Capabilities*/
typedef enum {
    SWL_STACAP_EHT_EML_EMLSR,
    SWL_STACAP_EHT_EML_EMLMR,
    SWL_STACAP_EHT_EML_MAX,
} swl_staCapEhtEml_e;
#define M_SWL_STACAP_EHT_EML_EMLSR              (1 << SWL_STACAP_EHT_EML_EMLSR)
#define M_SWL_STACAP_EHT_EML_EMLMR              (1 << SWL_STACAP_EHT_EML_EMLMR)

extern const char* swl_staCapEhtEml_str[SWL_STACAP_EHT_EML_MAX];
typedef uint8_t swl_staCapEhtEml_m;

#endif /* __SWL_STACAP_H__ */
