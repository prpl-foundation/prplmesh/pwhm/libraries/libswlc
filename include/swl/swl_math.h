/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_MATH_H_
#define SRC_INCLUDE_SWL_MATH_H_

#include <stdbool.h>
#include <stdint.h>

#define SWL_MATH_ABS(x) ((x) > 0 ? (x) : (x) * (-1))

#define SWL_MATH_SIGN(x) ((x) < 0 ? (-1) : (1))


typedef enum {
    SWL_MATH_ROUND_OPT_DOWN,
    SWL_MATH_ROUND_OPT_HALF,
    SWL_MATH_ROUND_OPT_UP,
    SWL_MATH_ROUND_OPT_MAX,
} swl_math_roundOpt_e;

#define SWL_MATH_E  2.718281828459045
#define SWL_MATH_PI 3.141592653589793

bool swl_math_isPowerOf(int64_t value, int64_t powerBase);
bool swl_math_isPowerOf2(uint64_t value);
uint64_t swl_math_invSumInv(uint64_t val1, uint64_t val2);
uint64_t swl_math_interpolate(uint64_t point1, uint64_t point2, int64_t dist, int64_t maxDist);
uint64_t swl_math_fmod(int64_t dividend, int64_t divider);
int64_t swl_math_fdiv(int64_t dividend, int64_t divider);

uint64_t swl_math_udiv(uint64_t dividend, uint64_t divider, swl_math_roundOpt_e opt);
uint64_t swl_math_multComplement(uint64_t base, uint64_t val1, uint64_t val2);

int32_t swl_math_doExpAvgStep(int32_t accum, int32_t newVal, int32_t factor, bool first);
int32_t swl_math_getExpAvgResult(int32_t accum);
uint64_t swl_math_log10(uint64_t val);
uint64_t swl_math_log10Mult(uint64_t val, uint64_t mult);
uint32_t swl_math_getInsertIndexU32(const uint32_t* array, uint32_t size, uint32_t val);
double swl_math_interpolateDArray(const double* srcArray, const double* targetArray, size_t nrValues, double source);
#endif /* SRC_INCLUDE_SWL_MATH_H_ */
