/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_HISTOGRAM_H_
#define SRC_INCLUDE_SWL_COMMON_HISTOGRAM_H_

#include "swl/swl_common.h"
#include "swl/swl_common_time.h"

typedef struct swl_histogram_s swl_histogram_t;

typedef struct {
    swl_timeMono_t startTime;
    swl_timeMono_t endTime;
    uint16_t nrValues;
    uint16_t* buckets; // buckets equal to sum of bucketSizes;
} swl_histogramWindow_t;

typedef enum {
    SWL_HISTOGRAM_EVENT_OFF,          // Off histogram event. Can be used for configuring deamons when certain things need to happen.
    SWL_HISTOGRAM_EVENT_WINDOW_DONE,  //Event called when window gets cycled. When called, current window will contain no measurements.
    SWL_HISTOGRAM_EVENT_HISTORY_DONE, // Event called when the full history (all windows) is filled, and we start filling first window again.
    SWL_HISTOGRAM_EVENT_MAX
} swl_histogram_event_e;

typedef enum {
    SWL_HISTOGRAM_WINDOW_ID_CURRENT, // index for current window
    SWL_HISTOGRAM_WINDOW_ID_TOTAL,   // index for total window
    SWL_HISTOGRAM_WINDOW_ID_LAST,    // index for the last window in the historyList
    SWL_HISTOGRAM_WINDOW_ID_MAX
} swl_histogram_windowId_e;

extern const char* swl_histogram_tableNames_str[SWL_HISTOGRAM_WINDOW_ID_MAX];

extern const char* swl_histogram_event_str[SWL_HISTOGRAM_EVENT_MAX];

typedef void (* swl_histogramHandler_f) (swl_histogram_t* map, swl_histogram_event_e event, void* data);

typedef struct {
    swl_histogramWindow_t currentWindow;
    uint32_t nextWindowIndex;    // index of the next window to be filled
    uint32_t filledWindows;      // number of last windows actually filled
    uint32_t totalWindowsFilled; // number of windows filled since last reset;
    swl_histogramWindow_t* lastWindows;
    swl_histogramWindow_t totalWindow;
} swl_histogram_windowData_t;

typedef struct {
    uint32_t nextHistoryIndex;      // the index of the next history value
    uint32_t filledHistorySize;     // current number of history values
    uint8_t* history;               // one uint8_t* of size historyLen * nrValues.
    swl_timeMono_t lastEntryTime;   // timestamp of the last entry
} swl_histogram_historyData_t;

/**
 * Structure containing config and data for histogram taking functionality
 */
struct swl_histogram_s {
    uint32_t nrValues;          // the number of values this histogram needs to keep track of
    uint32_t nrObsPerWindow;    // the number of observations in a single window
    uint32_t nrWindows;         // the number of windows in the histogram
    uint8_t* nrBucketsPerValue; // the number of buckets for each variable
    uint16_t* offsetList;       // local calculated, the offset for each list of buckets
    uint16_t totalNrBuckets;    // the total number of buckets needed to allocate = sum of all buckets in nrBucketsPerValue list
    uint32_t historyLen;        // the configured length of the history
    char** names;

    swl_histogramHandler_f handler; // Handler to be called when window is filled, or full histogram is filled
    void* handlerData;

    swl_histogram_windowData_t histogramData;
    swl_histogram_historyData_t historyData;
};


typedef struct {
    uint32_t nrValues;
    uint8_t* bucketSizes;
    char** names;
} swl_histogramInitData_t;

/**
 * Internal macro's : Do not use
 */

#define SWL_HISTOGRAM_EXPAND_TYPE(Y, name, strName, size) uint8_t name;
#define SWL_HISTOGRAM_EXPAND_STRING(Y, name, strName, size) strName,
#define SWL_HISTOGRAM_EXPAND_SIZE(Y, name, strName, size) size,
#define SWL_HISTOGRAM_EXPAND_NUM(Y, name, strName, size) 1 +
#define SWL_HISTOGRAM_EXPAND_ENUM(structName, name, strName, size) structName ## _ ## name,


/**
 * End internal macro's : Do not use
 */

/**
 * Add new way to create and update a histogram, by defining a histogram data type, and doing init with init data
 *
 * To define the "init data", please use following xMacro approach, where you put both in the c file, in case
 * the type is local to the c file only.
 * The structure of the xMacro is:
 * X(Y, nameOfVariableInStruct, nameOfTableInPrint, nrBuckets)
 *
 * #define MY_TEST_HISTOGRAM_VAR(X, Y) \
    X(Y, test0Index, "Test0History", 5) \
    X(Y, test1Index, "Test1History", 10) \
    X(Y, test2Index, "Test2History", 20) \
    X(Y, test3Index, "Test3History", 4) \
   SWL_HISTOGRAM(s_myHistInit, swl_myTestHistData_t, MY_TEST_HISTOGRAM_VAR)

   Or if type should be in header, then put in header, the xMacro and:
   SWL_HISTOGRAM_H(s_myHistInit, swl_myTestHistData_t, MY_TEST_HISTOGRAM_VAR)
   and in c file:
   SWL_HISTOGRAM_C(s_myHistInit, swl_myTestHistData_t, MY_TEST_HISTOGRAM_VAR)

 * Then, to initialize the histogram do:
 * swl_histogram_initExt(&myHistogram, &s_myHistInit, nrObsPerWindow, nrWindows, historyLen);
 *
 * Finally, to add a value, you can do
 * swl_myTestHistData_t data;
 * data.test0Index = 0;
 * data.test1Index = 1;
 * data.test2Index = 2;
 * data.test3Index = 3;
 * swl_histogram_addDatavalues(&myHistogram, &data);
 *
 * Note there is also the swl_math_getInsertIndexU32(array, arraySize, val), which returns the index of val in the array,
 * which may be usefull for determining index.
 *
 * If index is -1, then for this data add, no data will be added, but it will still count as a value added
 * for the purposes of notifications etc.
 *
 */


#define SWL_HISTOGRAM_H(initDataName, valStructName, xMacro) \
    typedef enum { \
        xMacro(SWL_HISTOGRAM_EXPAND_ENUM, initDataName) \
        initDataName ## __max} initDataName ## _e; \
    typedef struct { \
        xMacro(SWL_HISTOGRAM_EXPAND_TYPE, 0) \
    } valStructName;


#define SWL_HISTOGRAM_C(initDataName, valStructName, xMacro) \
    char* initDataName ## Names[] = { \
        xMacro(SWL_HISTOGRAM_EXPAND_STRING, 0) \
    }; \
    static uint8_t initDataName ## BucketSizes[] = { \
        xMacro(SWL_HISTOGRAM_EXPAND_SIZE, 0) \
    }; \
    swl_histogramInitData_t initDataName = { \
        .names = initDataName ## Names, \
        .nrValues = xMacro(SWL_HISTOGRAM_EXPAND_NUM, 0) 0, \
        .bucketSizes = initDataName ## BucketSizes, \
    };

#define SWL_HISTOGRAM(initDataName, valStructName, xMacro) \
    SWL_HISTOGRAM_H(initDataName, valStructName, xMacro) \
    SWL_HISTOGRAM_C(initDataName, valStructName, xMacro) \


swl_rc_ne swl_histogram_init(swl_histogram_t* map, uint32_t nrValues, uint32_t nrObsPerWindow,
                             uint32_t nrWindows, uint8_t* bucketSizes, uint32_t historyLen);

swl_rc_ne swl_histogram_initExt(swl_histogram_t* map, swl_histogramInitData_t* initData,
                                uint32_t nrObsPerWindow, uint32_t nrWindows, uint32_t historyLen);

swl_rc_ne swl_histogram_setNames(swl_histogram_t* map, char** names);

swl_rc_ne swl_histogram_destroy(swl_histogram_t* map);

swl_rc_ne swl_histogram_addValues(swl_histogram_t* map, uint8_t* indexes);
swl_rc_ne swl_histogram_addDataValues(swl_histogram_t* hist, void* data);

swl_rc_ne swl_histogram_cycleWindow(swl_histogram_t* hist);
swl_rc_ne swl_histogram_reset(swl_histogram_t* map);

swl_rc_ne swl_histogram_setHandler(swl_histogram_t* map, swl_histogramHandler_f handler, void* handlerData);

swl_rc_ne swl_histogram_setNrObsPerWindow(swl_histogram_t* hist, uint32_t nrObsPerWindow);

swl_histogramWindow_t* swl_histogram_getWindow(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex);

swl_rc_ne swl_histogram_setNrWindows(swl_histogram_t* hist, uint32_t nrWindows);

swl_rc_ne swl_histogram_setHistoryLen(swl_histogram_t* hist, uint32_t historyLen);

swl_rc_ne swl_histogram_getHistoryFromValue(swl_histogram_t* hist, uint8_t* tgtArray, size_t tgtArraySize, uint16_t valueIndex, size_t* elementsCopies);
swl_rc_ne swl_histogram_getHistogramFromValue(swl_histogram_t* hist, uint16_t* tgtArray, uint32_t tgtArraySize,
                                              swl_histogram_windowId_e histogramIndex, uint16_t valueIndex, size_t* elementsCopied);

size_t swl_histogram_getNrFilledWindows(swl_histogram_t* hist);

size_t swl_histogram_getNrValuesInWindow(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex);

swl_timeMono_t swl_histogram_getStartTime(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex);

swl_timeMono_t swl_histogram_getEndTime(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex);

void swl_histogram_getBucketOffsets(swl_histogram_t* hist, swl_histogram_windowId_e histogramIndex, size_t typeIndex,
                                    uint16_t* bucketIndexes, const uint16_t* tgtBucketPct, uint16_t nrValues);

swl_rc_ne swl_histogram_printBucketHistogram(swl_histogram_t* hist, char* array, size_t arraySize, swl_histogram_windowId_e histogramIndex,
                                             uint16_t bucketIndex);


swl_rc_ne swl_histogram_printWindowToFile(swl_histogram_t* hist, FILE* file, swl_histogram_windowId_e histogramIndex, swl_print_args_t* args);

swl_rc_ne swl_histogram_printWindowToFileName(swl_histogram_t* hist, const char* fileName,
                                              swl_histogram_windowId_e histogramIndex, swl_print_args_t* args, bool append);

swl_rc_ne swl_histogram_printHistoryToFile(swl_histogram_t* hist, FILE* file, swl_print_args_t* args);
swl_rc_ne swl_histogram_printHistoryToFileName(swl_histogram_t* hist, const char* fileName, swl_print_args_t* args, bool append);

#endif /* SRC_INCLUDE_SWL_HISTOGRAM_H_ */
