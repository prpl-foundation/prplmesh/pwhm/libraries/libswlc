/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#ifndef SRC_INCLUDE_SWL_COMMON_TRILEAN_H_
#define SRC_INCLUDE_SWL_COMMON_TRILEAN_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

typedef enum {
    SWL_TRL_FALSE,
    SWL_TRL_TRUE,
    /** also used as "auto" */
    SWL_TRL_UNKNOWN,
    SWL_TRL_MAX
} swl_trl_e;

#define SWL_TRL_AUTO SWL_TRL_UNKNOWN

typedef enum {
    /** [ "Off", "On", "Auto" ] */
    SWL_TRL_FORMAT_AUTO,
    /** [ "False", "True", "Unknown" ] */
    SWL_TRL_FORMAT_UNKNOWN,
    /** [ "0", "1", "-1" ] */
    SWL_TRL_FORMAT_NUMERIC,
    /** [ "false", "true", "null" ] */
    SWL_TRL_FORMAT_JSON,
    SWL_TRL_FORMAT_MAX
} swl_trl_format_e;

bool swl_trl_isValid(swl_trl_e trl);
bool swl_trl_fromChar(swl_trl_e* tgtTrlValue, const char* srcCharValue, swl_trl_format_e);
bool swl_trl_toChar(const char** tgtCharValue, swl_trl_e srcTrlValue, swl_trl_format_e);
swl_trl_e swl_trl_fromBool(bool boolean);
bool swl_trl_toBool(swl_trl_e trl, bool valueOnUnknown);
swl_trl_e swl_trl_fromInt(int32_t val);
int32_t swl_trl_toInt(swl_trl_e trl);
#endif /* SRC_INCLUDE_SWL_TRILEAN_H_ */
