/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


// Documentation is in the .c file (except for what is only in this .h).

#ifndef __SWL_COMMON_RADIOSTANDARDS_H__
#define __SWL_COMMON_RADIOSTANDARDS_H__

#include <stdbool.h>
#include <stdint.h>
#include "swl/swl_typeEnum.h"

// An enum value represents one single radio standard such as IEEE802.11a.
typedef enum {
    SWL_RADSTD_AUTO,
    SWL_RADSTD_A,
    SWL_RADSTD_B,
    SWL_RADSTD_G,
    SWL_RADSTD_N,
    SWL_RADSTD_AC,
    SWL_RADSTD_AX,
    SWL_RADSTD_BE,
    SWL_RADSTD_MAX
} swl_radStd_e;
// Define a custom "LAST" radio standard to be used
#define SWL_RADSTD_LAST (SWL_RADSTD_MAX - 1)

// radio standards strings, with SWL_RADSTD_AUTO mapped to "Auto" string
extern const char* const swl_radStd_str[];
// radio standards strings, with SWL_RADSTD_AUTO mapped to "Unknown" string.
extern const char* const swl_radStd_unknown_str[];

SWL_TYPE_ENUM_H(gtSwl_type_radStd, swl_radStd_e);
#define swl_type_radStd &gtSwl_type_radStd

// A bitmask value represents a set of radio standards such as IEEE802.11a.
#define M_SWL_RADSTD_AUTO  (1 << SWL_RADSTD_AUTO)
#define M_SWL_RADSTD_A     (1 << SWL_RADSTD_A)
#define M_SWL_RADSTD_B     (1 << SWL_RADSTD_B)
#define M_SWL_RADSTD_G     (1 << SWL_RADSTD_G)
#define M_SWL_RADSTD_N     (1 << SWL_RADSTD_N)
#define M_SWL_RADSTD_AC    (1 << SWL_RADSTD_AC)
#define M_SWL_RADSTD_AX    (1 << SWL_RADSTD_AX)
#define M_SWL_RADSTD_BE    (1 << SWL_RADSTD_BE)
#define M_SWL_RADSTD_LAST  (1 << SWL_RADSTD_LAST)
typedef uint32_t swl_radioStandard_m;

// Legacy representation of sets of radio standards. Avoid using.
// Prefer using `swl_radioStandard_m`.
#define SWL_RADSTD_LEGACY_AUTO "auto"
#define SWL_RADSTD_LEGACY_A    "a"
#define SWL_RADSTD_LEGACY_B    "b"
#define SWL_RADSTD_LEGACY_G    "g"
#define SWL_RADSTD_LEGACY_N    "n"
#define SWL_RADSTD_LEGACY_B_G  "bg"
#define SWL_RADSTD_LEGACY_G_N  "gn"
#define SWL_RADSTD_LEGACY_B_G_N        "bgn"
#define SWL_RADSTD_LEGACY_A_N          "an"
#define SWL_RADSTD_LEGACY_A_B_G_N      "abgn"
#define SWL_RADSTD_LEGACY_AC_AND_LOWER "ac"
#define SWL_RADSTD_LEGACY_AX_AND_LOWER "ax"
#define SWL_RADSTD_LEGACY_BE_AND_LOWER "be"

typedef enum {
    // Interpret "ac" as '802.11ac plus 802.11a
    // if supported plus 802.11n if supported' and "ax" as '802.11ax plus all
    // lower supported radio standards'.
    SWL_RADSTD_FORMAT_LEGACY,
    // Interpret "ac" as 802.11ac-only and "ax" as 802.11ax-only.
    SWL_RADSTD_FORMAT_STANDARD,

    SWL_RADSTD_FORMAT_MAX
} swl_radStd_format_e;

bool swl_radStd_toChar(
    char* targetBuffer,
    uint32_t targetBufferSize,
    swl_radioStandard_m radioStandards,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards);

bool swl_radStd_fromChar(
    swl_radioStandard_m* output,
    const char* string,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards,
    const char* caller);

bool swl_radStd_fromCharAndValidate(
    swl_radioStandard_m* output,
    const char* string,
    swl_radStd_format_e format,
    swl_radioStandard_m supportedStandards,
    const char* caller);

bool swl_radStd_isValid(swl_radioStandard_m radioStandards, const char* callerIfTrace);

const char* swl_radStd_formatToChar(swl_radStd_format_e format);
swl_radStd_format_e swl_radStd_charToFormat(const char* format);

bool swl_radStd_supportedStandardsToChar(char* targetBuffer,
                                         uint32_t targetBufferSize,
                                         swl_radioStandard_m supportedStandards,
                                         swl_radStd_format_e format);

bool swl_radStd_supportedStandardsFromChar(
    swl_radioStandard_m* output,
    const char* string);


#endif /* __SWL_RADIOSTANDARDS_H__ */
