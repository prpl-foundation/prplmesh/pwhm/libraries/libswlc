/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_COMMON_SCAN_H_
#define SRC_INCLUDE_SWL_COMMON_SCAN_H_

#include "swl/swl_common_radioStandards.h"
#include "swl/swl_common_type.h"
#include "swl/swl_common_chanspec.h"
#include "swl/swl_common_mac.h"
#include "swl/swl_typeEnum.h"
#include "swl/swl_80211.h"
#include "swl/swl_security.h"
#include "swl/swl_wps.h"
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct swl_scan_resultItem {
    swl_macBin_t bssid;
    char ssid[SWL_80211_SSID_STR_LEN];
    swl_channel_t channel;
    swl_channel_t centreChannel;
    swl_bandwidth_e bandwidth;
    int32_t snr;
    int32_t noise;
    int32_t signalStrength;
    swl_radioStandard_m operatingStandards;
    swl_security_apMode_e securityMode;
    swl_security_encMode_e encMode;
    swl_security_mfpMode_e mfpMode;
    swl_wps_cfgMethod_m wpsConfigMethods;
    bool adHoc;
    swl_80211_htCapInfo_m htCap;
    swl_80211_vhtCapInfo_m vhtCap;
    swl_80211_heCapInfo_m heCap;
    swl_llist_t vendorIEs;  // list of swl_vendorInfoElt_t

    swl_llist_iterator_t it;
} swl_scan_resultItem_t;

typedef struct swl_scan_results {
    swl_llist_t results;
} swl_scan_results_t;

typedef struct swl_vendorIeElts {
    swl_80211_vendorIdEl_t elt;
    swl_llist_iterator_t it;
} swl_vendorInfoElt_t;

#endif /* SRC_INCLUDE_SWL_SCAN_C_ */
