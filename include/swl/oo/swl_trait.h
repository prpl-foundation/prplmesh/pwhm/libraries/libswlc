/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_OO_SWL_TRAIT_H_
#define SRC_INCLUDE_SWL_OO_SWL_TRAIT_H_

#include "swl/oo/swl_oo_common.h"


typedef struct {
    // Struct containing the vft should have _trt tag
} swl_trait_trt;

typedef struct {
    char* name;
} swl_traitId_t;

typedef struct {
    swl_traitId_t* traitId;
    swl_trait_trt* vft;
} swl_trait_trt_t;

swl_trait_trt* swl_trait_findVft(swl_oo_class_t* obj, swl_traitId_t* traitId);
swl_trait_trt* swl_trait_findCommonVft(swl_oo_class_t* obj1, swl_oo_class_t* obj2, swl_traitId_t* traitId);

#define W_SWL_OO_TRT_CALL(retVal, trait, funName, var, ...) { \
        swl_oo_class_t* _tmpVar = (swl_oo_class_t*) var; \
        trait* _tmpVft = (trait*) swl_trait_findVft(_tmpVar, &trait ## _uid); \
        if(_tmpVft != NULL) { \
            retVal = _tmpVft->funName(_tmpVar, ## __VA_ARGS__); \
        } else { \
            SAH_TRACEZ_ERROR(ME, "Obj %s does not support vft %s", var != NULL ? var->vft->name : "NULL", trait ## _uid.name); \
        }}


#define SWL_OO_TRT_CALL(type, funName, var, ...) { \
        swl_oo_class_t* _tmpVar = (swl_oo_class_t*) var; \
        trait* _tmpVft = (trait*) swl_trait_findVft(_tmpVar, &trait ## _uid); \
        if(_tmpVft != NULL) { \
            _tmpVft->funName(_tmpVar, ## __VA_ARGS__); \
        } else { \
            SAH_TRACEZ_ERROR(ME, "Obj %s does not support trait %s", var != NULL ? var->vft->name : "NULL", trait ## _uid.name); \
        }}

#define SWL_OO_TRT(type, functionList) \
    extern swl_traitId_t type ## _uid; \
    typedef struct { \
        functionList \
    } type;

#define SWL_OO_TRT_ID(type) swl_traitId_t type ## _uid = {.name = #type};

#define SWL_OO_TRT_MAP(type, list) \
    static type s_trt_ ## type ## map = {list};


#define SWL_OO_TRT_REF(type) {&(type ## _uid), (swl_trait_trt*) &s_trt_ ## type ## map}

#endif /* SRC_INCLUDE_SWL_OO_SWL_TRAIT_H_ */
