/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_OO_SWL_OO_H_
#define SRC_INCLUDE_SWL_OO_SWL_OO_H_

#include "swl/oo/swl_oo_common.h"
#include "swl/oo/swl_trait.h"

/**
 * Implements a framework to use object oriented code in c.
 *
 * To use, you must first define a class of a given type in the header file. In this class the function calls and data fields of the class are listed:
 *
 * Example:
 * SWL_OO_CLASS(swl_oo_class1_t,
            KEEP_COMMA(
                bool (* doSomething1)(swl_oo_class1_t* self, uint32_t testValue);
                bool (* doSomething1a) (swl_oo_class1_t* self, uint32_t testValue);
                ),
            KEEP_COMMA(
                uint32_t var1;
                uint32_t var2;
                ));
 *
 * Then you must provide an implementation for this class by putting following macro in source file:
 *
 * SWL_OO_CLASS_INST(swl_oo_class1_t, KEEP_COMMA(
                     .doSomething1 = swl_oo_class1_doSomething1,
                     .doSomething1a = swl_oo_class1_doSomething1a,
                     ), KEEP_COMMA(
                     SWL_OO_TRT_REF(swl_trait1_trt),
                     SWL_OO_TRT_REF(swl_trait2_trt)
                     ));
 *
 * the implementation must also implement a new function:
 * * void swl_oo_class1_t_new(swl_oo_class1_t* self, uint32_t val);
 *
 * method calls should be done using the relevant macro's.
 * * One macro to return result
 *  W_SWL_OO_CALL(type,functionName,object,parameters)
 * * One macro to not return a result
 *  SWL_OO_CALL(type,functionName,object,parameters)
 *
 * Destruction must be done with the SWL_OO_DESTROY(obj) macro. A class may overwrite the destroy method
 * of the root class, to provide some behaviour that must be done before destruction.
 *
 * Inheritance works that instead of SWL_OO_CLASS, one must call SWL_OO_SUBCLASS, and instead of SWL_OO_CLASS_IMPL, one must call
 * SWL_OO_SUBCLASS_IMPL, and provide the super class as the second argument of both calls.
 *
 * Any created class with no superclass will by default have the swl_ooRoot_t class as its super class.
 *
 * Traits work similarly to class methods, except that multiple separate object trees may implement same trait.
 * To define a trait, you must first declare the trait in a header file:
 * SWL_OO_TRT(swl_compare_trt, KEEP_COMMA(
               swl_comparison_e (* compare)(swl_oo_pclass_t* self, swl_oo_pclass_t* target);
               ));
 *
 * and then add a trait id implement in a source file:
 * SWL_OO_TRT_ID(swl_compare_trt);
 *
 * To have a class implement a trait, you must just define the instantiation in the class c file:
 *
 * SWL_OO_TRT_MAP(swl_compare_trt, KEEP_COMMA(
                   .compare = s_myCompare,
                   ));
 *
 * and then you must add the trait reference in the SWL_OO_CLASS_IMPL block.
 *
 * To call a trait, you must use following macro's:
 * * W_SWL_OO_TRT_CALL(retVal, trait, funName, var, ...) to have a return value
 * * SWL_OO_TRT_CALL(type, funName, var, ...) to just execute function
 *
 * similarly to how the object traits get called.
 *
 * Traits get inherited, so if a superclass implements a trait, all subclasses automatically offer that trait, but they can
 * provide an updated implementation.
 *
 * The current implementation will NOT crash if one tries to call a trait or function on an object that does not inherit from
 * the provided class, or does not implement the provided trait, but will just print an error.
 *
 *
 * It is also possible to define interfaces, which are then implemented by classes. To define an interface, please use
 * * SWL_OO_INTF(intfName, functions) for a "top level interface",
 * * SWL_OO_SUBINTF(intfName,superIntfName,functions) for a sub interface
 * a sub interface.
 *
 * To declare the classes of an interface, you use:
 * * SWL_OO_CLASS_IMPL(className,intfName,variables) for a "top level class"
 * * SWL_OO_SUBCLASS_IMPL(className, superClassName, intfName, variables)
 *
 * To instantiate these classes you use:
 * * SWL_OO_CLASS_IMPL_INST(className,intfName,vftDef,traitList)
 * * SWL_OO_SUBCLASS_IMPL_INST(className, superClassName, intfName, vftDef, traitList)
 *
 * Note that to work properly, the superClass must implement the super interface of the interface that intfName extends.
 * It is not possible to implement multiple interfaces at this time, nor is it possible to add functions to the implementation
 * itself. It is however possible to define a subclass of the implementation with extra functions.
 *
 * Calling functions on interface implementation is the same as calling them on normal objects, except that the interface
 * name must be used as the "calling type". Simply put, one must always provide the name of the interface, but classes that
 * do not explicitly implement an interface, implement the interface that is named after the class.
 */

typedef struct swl_oo_class_t_fun_s {
    //Put functions here
} swl_oo_class_t_fun;

typedef struct swl_oo_class_t_data_s {
    //Put data here
} swl_oo_class_t_data;

struct swl_oo_class_t_vft_s {
    char* name;
    uint32_t depth;
    size_t size;
    size_t vftFunSize;
    swl_oo_class_t_vft* superPtr;
    size_t nrTraits;
    swl_trait_trt_t* traits;
    swl_oo_class_t_fun vft;
};

struct swl_oo_class_s {
    swl_oo_class_t_vft* vft;
    swl_oo_class_t_data data;
};


bool swl_oo_isInstanceOf(swl_oo_class_t_vft* vft, swl_oo_class_t* obj);
swl_oo_class_t_vft* swl_oo_findCommonSuperClass(swl_oo_class_t_vft* vft1, swl_oo_class_t_vft* vft2);

#define SWL_OO_IS_INSTANCE_OF(type, obj) \
    swl_oo_isInstanceOf((swl_oo_class_t_vft*) &type ## _vfti, (swl_oo_class_t*) obj)


swl_oo_class_t_fun* swl_oo_getFun(swl_oo_class_t* data, size_t offset);

#define W_SWL_OO_CALL(retVal, type, funName, var, ...) { \
        type* _tmpVar = (type*) var; \
        type ## _fun* _tmpFun = (type ## _fun*) swl_oo_getFun((swl_oo_class_t*) var, offsetof(type ## _fun, funName)); \
        if(_tmpFun != NULL) { \
            retVal = _tmpFun->funName(_tmpVar, ## __VA_ARGS__); \
        } else { \
            SAH_TRACEZ_ERROR(ME, "Obj %s does not support vft %s", var != NULL ? var->vft->name : "NULL", #type); \
        }}

#define SWL_OO_CALL(type, funName, var, ...) { \
        type* _tmpVar = (type*) var; \
        type ## _fun* _tmpFun = (type ## _fun*) swl_oo_getFun((swl_oo_class_t*) var, offsetof(type ## _fun, funName)); \
        if(_tmpFun != NULL) { \
            _tmpFun->funName(_tmpVar, ## __VA_ARGS__); \
        } else { \
            SAH_TRACEZ_ERROR(ME, "Obj %s does not support vft %s", var != NULL ? var->vft->name : "NULL", #type); \
        }}

#define SWL_OO_NEW(type, name, ...) \
    type* name = calloc(1, type ## _vfti.size); \
    name->vft = &type ## _vfti; \
    type ## _new(name, __VA_ARGS__);

#define SWL_OO_DESTROY(name) \
    SWL_OO_CALL(swl_ooRoot_t, destroy, name) \
    free(name); \
    name = NULL;

typedef struct swl_ooRoot_t_s swl_ooRoot_t;
typedef struct {} swl_ooRoot_t_data;
typedef struct swl_ooRoot_t_fun_s {
    void (* destroy)(swl_ooRoot_t* self);
} swl_ooRoot_t_fun;

typedef struct {
    char* name;
    uint32_t depth;
    size_t size;
    size_t vftSize;
    void* superPtr;
    size_t nrTraits;
    swl_trait_trt_t* traits;
    swl_ooRoot_t_fun vft;
} swl_ooRoot_t_vft;

struct swl_ooRoot_t_s {
    swl_ooRoot_t_vft* vft;
    swl_ooRoot_t_data data;
};

extern swl_ooRoot_t_vft swl_ooRoot_t_vfti;

/**
 * Define a "sub vft", which extends the super vft with some functions
 */
#define SWL_OO_SUBINTF_COMMON(intfName, superIntfName, functions) \
    typedef struct intfName ## _fun_s { \
        superIntfName ## _fun super; \
        functions \
    } intfName ## _fun;

#define SWL_OO_SUBINTF(intfName, superIntfName, functions) \
    typedef void intfName; \
    SWL_OO_SUBINTF_COMMON(intfName, superIntfName, KEEP_COMMA(functions))

/**
 * Define a "top vft", which extends the root vft
 */
#define SWL_OO_INTF(intfName, functions) SWL_OO_SUBINTF(intfName, swl_ooRoot_t, KEEP_COMMA(functions))

/**
 * Define a sub class that implements a vft. Such a class inherit from a super class, but implements the vft with the given name
 * For this macro, the super VFT name must be given, which is the name of interface of the superclass.
 */
#define SWL_OO_SUBCLASS_IMPL(className, superClassName, intfName, variables) \
    typedef struct className ## _s className; \
    typedef struct { \
        superClassName ## _data super; \
        variables \
    } className ## _data; \
    typedef struct { \
        char* name; \
        uint32_t depth; \
        size_t size; \
        size_t vftSize; \
        superClassName ## _vft* superPtr; \
        size_t nrTraits; \
        swl_trait_trt_t* traits; \
        intfName ## _fun vft; \
    } className ## _vft; \
    struct className ## _s { \
        className ## _vft* vft; \
        className ## _data data; \
    }; \
    extern className ## _vft className ## _vfti;

#define SWL_OO_CLASS_IMPL(className, intfName, variables) SWL_OO_SUBCLASS_IMPL(className, swl_ooRoot_t, intfName, KEEP_COMMA(variables))


#define SWL_OO_SUBCLASS(className, superClassName, functions, variables) \
    typedef struct className ## _s className; \
    SWL_OO_SUBINTF_COMMON(className, superClassName, KEEP_COMMA(functions)) \
    SWL_OO_SUBCLASS_IMPL(className, superClassName, className, KEEP_COMMA(variables))

#define SWL_OO_CLASS(className, functions, variables) SWL_OO_SUBCLASS(className, swl_ooRoot_t, KEEP_COMMA(functions), KEEP_COMMA(variables))


#define SWL_OO_SUBCLASS_IMPL_INST(className, superClassName, intfName, vftDef, traitList) \
    swl_trait_trt_t className ## _traitArr[] = {traitList}; \
    className ## _vft className ## _vfti = { \
        .name = #className, \
        .size = sizeof(className), \
        .vftSize = sizeof(intfName ## _fun), \
        .superPtr = &superClassName ## _vfti, \
        .vft = { \
            vftDef \
        }, \
        .nrTraits = SWL_ARRAY_SIZE(className ## _traitArr), \
        .traits = className ## _traitArr, \
    }

#define SWL_OO_CLASS_IMPL_INST(className, intfName, vftDef, traitList) \
    SWL_OO_SUBCLASS_IMPL_INST(className, swl_ooRoot_t, intfName, KEEP_COMMA(vftDef), KEEP_COMMA(traitList))

#define SWL_OO_SUBCLASS_INST(className, superClassName, vftDef, traitList) \
    SWL_OO_SUBCLASS_IMPL_INST(className, superClassName, className, KEEP_COMMA(vftDef), KEEP_COMMA(traitList))
/**
 * Instatiate a top class, to be done in source file
 */
#define SWL_OO_CLASS_INST(className, vftDef, traitList) SWL_OO_SUBCLASS_INST(className, swl_ooRoot_t, KEEP_COMMA(vftDef), KEEP_COMMA(traitList))


#endif /* SRC_INCLUDE_SWL_OO_SWL_OO_H_ */
