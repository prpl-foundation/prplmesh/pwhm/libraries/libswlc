/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_SWL_WPS_H_
#define INCLUDE_SWL_WPS_H_

#include <stddef.h>
#include "swl/swl_common.h"

#define SWL_WPS_NOTIF_TYPE_BASE (SWL_NOTIFICATION_BASE + 10)

typedef enum {
    /**
     * Pairing has started successfully.
     *
     * Must be followed by #SWL_WPS_NOTIF_TYPE_PAIRING_ENDED
     */
    SWL_WPS_NOTIF_TYPE_PAIRING_STARTED = SWL_WPS_NOTIF_TYPE_BASE,

    /**
     * Pairing has stopped (after it succeeded starting).
     *
     * The pairing could be successful or not.
     */
    SWL_WPS_NOTIF_TYPE_PAIRING_ENDED,

    /**
     * Pairing has not started due to an error.
     *
     * Cannot follow after #SWL_WPS_NOTIF_TYPE_PAIRING_STARTED since it's a contradiction to say
     * pairing has started successfully and pairing has not started due to an error.
     *
     * For WPS sessions that are an aggregation of multiple sub-WPS-sessions,
     * a PAIRING_STARTERROR of a sub-WPS-session can be converted into a PAIRING_ENDED of the
     * aggregating WPS session, if the aggregating WPS session has already sent a PAIRING_STARTED.
     */
    SWL_WPS_NOTIF_TYPE_PAIRING_STARTERROR,

    SWL_WPS_NOTIF_TYPE_PAIRING_MAX
} swl_wps_notif_type_ne;

/** See #SWL_WPS_NOTIF_TYPE_PAIRING_STARTED */
#define SWL_WPS_NOTIF_PAIRING_STARTED    "pairingReady"
/** See #SWL_WPS_NOTIF_TYPE_PAIRING_ENDED */
#define SWL_WPS_NOTIF_PAIRING_ENDED     "pairingDone"
/** See #SWL_WPS_NOTIF_TYPE_PAIRING_STARTERROR */
#define SWL_WPS_NOTIF_PAIRING_STARTERROR    "pairingError"

swl_wps_notif_type_ne swl_wps_notifNameToType(const char* name);
const char* swl_wps_notifTypeToName(swl_wps_notif_type_ne notifType);

/** Only to be used with #SWL_WPS_NOTIF_TYPE_PAIRING_STARTED */
#define SWL_WPS_NOTIF_STARTED_REASON_START_WPS_PBC "Start WPS push button"
#define SWL_WPS_NOTIF_STARTED_REASON_START_WPS_PIN "Start WPS sta pin"


/** Only to be used with #SWL_WPS_NOTIF_TYPE_PAIRING_ENDED */
#define SWL_WPS_NOTIF_ENDED_REASON_TIMEOUT    "Timeout"
#define SWL_WPS_NOTIF_ENDED_REASON_SUCCESS    "Success"
#define SWL_WPS_NOTIF_ENDED_REASON_FAILURE    "Failure"
#define SWL_WPS_NOTIF_ENDED_REASON_CANCELLED  "Canceled"
#define SWL_WPS_NOTIF_ENDED_REASON_LOCKED     "Locked"
#define SWL_WPS_NOTIF_ENDED_REASON_UNLOCKED   "Unlocked"
#define SWL_WPS_NOTIF_ENDED_REASON_CONFIGURING   "Configuring"
#define SWL_WPS_NOTIF_ENDED_REASON_BAD_CONFIG    "Bad config"
#define SWL_WPS_NOTIF_ENDED_REASON_OVERLAP       "Overlap"


/** Only to be used with #SWL_WPS_NOTIF_TYPE_PAIRING_STARTERROR */
#define SWL_WPS_NOTIF_STARTERROR_REASON_START_PBC   "Failed to start push button"
#define SWL_WPS_NOTIF_STARTERROR_REASON_START_PIN   "Failed to start sta pin"
#define SWL_WPS_NOTIF_STARTERROR_REASON_NO_SELF_PIN "No self pin support"
#define SWL_WPS_NOTIF_STARTERROR_REASON_INVALID_PIN "Sta pin invalid"
#define SWL_WPS_NOTIF_STARTERROR_REASON_CREDENTIALS "Failed to retrieve credentials"


#define SWL_WPS_NOTIF_PARAM_REASON        "reason"
#define SWL_WPS_NOTIF_PARAM_MACADDRESS    "macAddress"
#define SWL_WPS_NOTIF_PARAM_ACCESSPOINT   "accesspoint"
#define SWL_WPS_NOTIF_PARAM_DEVICE        "device"
#define SWL_WPS_NOTIF_PARAM_SSID          "SSID"
#define SWL_WPS_NOTIF_PARAM_SECURITYMODE  "securitymode"
#define SWL_WPS_NOTIF_PARAM_KEYPASSPHRASE "KeyPassPhrase"


/** Return codes for starting WPS */
typedef enum {
    SWL_WPS_STARTRC_OK,
    SWL_WPS_STARTRC_BUG,
    SWL_WPS_STARTRC_DISABLED,
    SWL_WPS_STARTRC_ALREADY_STARTED,
    SWL_WPS_STARTRC_NO_APS,
    SWL_WPS_STARTRC_ALL_FAIL,
    SWL_WPS_STARTRC_NO_EPS,
    SWL_WPS_STARTRC_MAX,
} swl_wps_startRc_e;

extern const char* const swl_wps_startRcChar[];

typedef enum {
    SWL_WPS_CFG_MTHD_USBFD = 0,       // USBA (Flash Drive): Deprecated
    SWL_WPS_CFG_MTHD_ETH = 1,         // Ethernet: Deprecated
    SWL_WPS_CFG_MTHD_LABEL = 2,       // Label: 8 digit static PIN, typically available on device.
    SWL_WPS_CFG_MTHD_DISPLAY = 3,     // Display: A dynamic 4 or 8 digit PIN is available from a display.
    SWL_WPS_CFG_MTHD_EXTNFCTOKEN = 4, // External NFC Token: An NFC Tag is used to transfer the configuration or device password.
    SWL_WPS_CFG_MTHD_INTNFCTOKEN = 5, // Integrated NFC Token: The NFC Tag is integrated in the device.
    SWL_WPS_CFG_MTHD_NFCINTF = 6,     // NFC Interface: The device contains an NFC interface.
    SWL_WPS_CFG_MTHD_PBC = 7,         // Pushbutton: The device contains either a physical or virtual Pushbutton.
    SWL_WPS_CFG_MTHD_PIN = 8,         // same as legacy "Keypad": Device is capable of entering a PIN

    /* Below enums are only defined in WPS 2.x */
    SWL_WPS_CFG_MTHD_PBC_V = 9,       // Virtual Pushbutton: Pushbutton functionality is available through a software user interface.
    SWL_WPS_CFG_MTHD_PBC_P = 10,      // Physical Pushbutton: A physical Pushbutton is available on the device.
    SWL_WPS_CFG_MTHD_DISPLAY_V = 13,  // Virtual Display PIN: The dynamic 4 or 8 digit PIN is displayed through a remote user interface.
    SWL_WPS_CFG_MTHD_DISPLAY_P = 14,  // Physical Display PIN: The dynamic 4 or 8 digit PIN is shown on a display/screen that is part of the device.

    SWL_WPS_CFG_MTHD_MAX
} swl_wps_cfgMethod_ne;

typedef enum {
    SWL_WPS_PASSWORD_ID_DEFAULT = 0,
    SWL_WPS_PASSWORD_ID_USER_SPECIFIED = 1,
    SWL_WPS_PASSWORD_ID_MACHINE_SPECIFIED = 2,
    SWL_WPS_PASSWORD_ID_REKEY = 3,
    SWL_WPS_PASSWORD_ID_PUSH_BUTTON = 4,
    SWL_WPS_PASSWORD_ID_REGISTRAR_SPECIFIED = 5,
    SWL_WPS_PASSWORD_ID_NFC_CONNECTION_HANDOVER = 7,
    SWL_WPS_PASSWORD_ID_MAX
} swl_wps_passwordId_ne;

#define M_SWL_WPS_CFG_MTHD_NONE 0
#define M_SWL_WPS_CFG_MTHD_USBFD (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_USBFD))
#define M_SWL_WPS_CFG_MTHD_ETH (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_ETH))
#define M_SWL_WPS_CFG_MTHD_LABEL (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_LABEL))
#define M_SWL_WPS_CFG_MTHD_DISPLAY (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_DISPLAY))
#define M_SWL_WPS_CFG_MTHD_EXTNFCTOKEN (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_EXTNFCTOKEN))
#define M_SWL_WPS_CFG_MTHD_INTNFCTOKEN (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_INTNFCTOKEN))
#define M_SWL_WPS_CFG_MTHD_NFCINTF (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_NFCINTF))
#define M_SWL_WPS_CFG_MTHD_PBC (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_PBC))
#define M_SWL_WPS_CFG_MTHD_PIN (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_PIN))
#define M_SWL_WPS_CFG_MTHD_PBC_V (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_PBC_V))
#define M_SWL_WPS_CFG_MTHD_PBC_P (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_PBC_P))
#define M_SWL_WPS_CFG_MTHD_DISPLAY_V (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_DISPLAY_V))
#define M_SWL_WPS_CFG_MTHD_DISPLAY_P (SWL_BIT_SHIFT(SWL_WPS_CFG_MTHD_DISPLAY_P))

#define M_SWL_WPS_CFG_MTHD_PBC_ALL (M_SWL_WPS_CFG_MTHD_PBC | M_SWL_WPS_CFG_MTHD_PBC_P | M_SWL_WPS_CFG_MTHD_PBC_V)
#define M_SWL_WPS_CFG_MTHD_DISPLAY_ALL (M_SWL_WPS_CFG_MTHD_DISPLAY | M_SWL_WPS_CFG_MTHD_DISPLAY_P | M_SWL_WPS_CFG_MTHD_DISPLAY_V)
// All WPS 1.x config method flags
#define M_SWL_WPS_CFG_MTHD_WPS10_ALL (M_SWL_WPS_CFG_MTHD_PBC_V - 1)
// Only WPS 2.x config method flags
#define M_SWL_WPS_CFG_MTHD_WPS2X_ONLY (M_SWL_WPS_CFG_MTHD_PBC_P | M_SWL_WPS_CFG_MTHD_PBC_V | M_SWL_WPS_CFG_MTHD_DISPLAY_P | M_SWL_WPS_CFG_MTHD_DISPLAY_V)
#define M_SWL_WPS_CFG_MTHD_ALL (M_SWL_WPS_CFG_MTHD_WPS10_ALL | M_SWL_WPS_CFG_MTHD_WPS2X_ONLY)

typedef swl_mask_m swl_wps_cfgMethod_m;

const char* swl_wps_configMethodTypeToName(swl_wps_cfgMethod_ne type);
swl_wps_cfgMethod_ne swl_wps_configMethodNameToType(const char* name);
swl_wps_cfgMethod_m swl_wps_configMethodNamesToMask(const char* names);
bool swl_wps_configMethodMaskToNames(char* buffer, size_t bufferSize, swl_wps_cfgMethod_m mask);

#endif
