/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_CRYPTO_H_
#define SRC_INCLUDE_SWL_CRYPTO_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>

#define PADDING RSA_PKCS1_PADDING

#if OPENSSL_VERSION_NUMBER >= 0x30000000L
#define SWL_CRYPTO_X509_GET_PUBKEY(cert) X509_get_pubkey(cert)
#define SWL_CRYPTO_EVP_PKEY_BASE_ID(pubkey) EVP_PKEY_base_id(pubkey)
#define SWL_CRYPTO_INIT_OPENSSL() OPENSSL_init_crypto(OPENSSL_INIT_ADD_ALL_CIPHERS | OPENSSL_INIT_ADD_ALL_DIGESTS, NULL)
#elif OPENSSL_VERSION_NUMBER >= 0x10100000L
#define SWL_CRYPTO_X509_GET_PUBKEY(cert) X509_get_pubkey(cert)
#define SWL_CRYPTO_EVP_PKEY_BASE_ID(pubkey) EVP_PKEY_base_id(pubkey)
#define SWL_CRYPTO_INIT_OPENSSL() OPENSSL_init_crypto(OPENSSL_INIT_ADD_ALL_CIPHERS | OPENSSL_INIT_ADD_ALL_DIGESTS, NULL)
#else
#define SWL_CRYPTO_X509_GET_PUBKEY(cert) X509_get_pubkey(cert)
#define SWL_CRYPTO_EVP_PKEY_BASE_ID(pubkey) (pubkey->type)
#define SWL_CRYPTO_INIT_OPENSSL() OpenSSL_add_all_algorithms()
#endif

X509* swl_crypto_getCertFromFile(const char* certPath);
EVP_PKEY* swl_crypto_getPrivateKeyFromFile(const char* privKeyPath);
swl_rc_ne swl_crypto_decodeDataWithPrivateKeyFile(const char* privKeyPath, const unsigned char* srcData, size_t dataSize, unsigned char* dataUnciphered);
swl_rc_ne swl_crypto_decodeDataWithPrivateKey(EVP_PKEY* privKey, const unsigned char* srcData, size_t dataSize, unsigned char* dataUnciphered);
swl_rc_ne swl_crypto_encodeDataWithCertFile(const char* certPath, const unsigned char* srcData, size_t* dataSize, unsigned char* dataCiphered);
swl_rc_ne swl_crypto_encodeDataWithCert(X509* cert, const unsigned char* srcData, unsigned char* dataCiphered, size_t* dataSize);
EVP_PKEY* swl_crypto_getRsaPubKeyFromCert(X509* cert);
int swl_crypto_verifyCertWithRootCA(X509* cert, X509* rootCA);
int swl_crypto_verifyCertWithRootCAFile(const char* cert, const char* rootCAPath);

#endif /* SRC_INCLUDE_SWL_CRYPTO_H_ */

