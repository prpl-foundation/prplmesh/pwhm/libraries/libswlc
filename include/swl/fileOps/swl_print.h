/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_SWL_PRINT_H_
#define SRC_INCLUDE_SWL_SWL_PRINT_H_

#include <stdbool.h>

typedef enum {
    SWL_PRINT_DELIM_MAP_OPEN,
    SWL_PRINT_DELIM_MAP_ASSIGN,
    SWL_PRINT_DELIM_MAP_NEXT,
    SWL_PRINT_DELIM_MAP_CLOSE,
    SWL_PRINT_DELIM_LIST_OPEN,
    SWL_PRINT_DELIM_LIST_NEXT,
    SWL_PRINT_DELIM_LIST_CLOSE,
    SWL_PRINT_DELIM_MAX
} swl_print_delim_e;

typedef enum {
    SWL_PRINT_OPEN_NEW_LINE,
    SWL_PRINT_NEXT_NEW_LINE,
    SWL_PRINT_CLOSE_PRE_NEW_LINE,
    SWL_PRINT_CLOSE_POST_NEW_LINE,
    SWL_PRINT_ASSIGN_NEW_LINE,
    SWL_PRINT_ADD_SPACE,      // add a space after delim, if no newline is required
    SWL_PRINT_INDENT,
    SWL_PRINT_BOOL_INT,       // If true, bool will be printed as int, else as string
    SWL_PRINT_BOOL_STR_LOWER, // If true, and BOOL_STR is true, bool will be printed as "true", else as "True". Parsing shall allow both.
    SWL_PRINT_OPTION_MAX
} swl_print_option_e;


typedef uint32_t swl_print_option_m;
#define M_SWL_PRINT_OPEN_NEW_LINE (1 << SWL_PRINT_OPEN_NEW_LINE)
#define M_SWL_PRINT_NEXT_NEW_LINE (1 << SWL_PRINT_NEXT_NEW_LINE)
#define M_SWL_PRINT_CLOSE_PRE_NEW_LINE (1 << SWL_PRINT_CLOSE_PRE_NEW_LINE)
#define M_SWL_PRINT_CLOSE_POST_NEW_LINE (1 << SWL_PRINT_CLOSE_POST_NEW_LINE)
#define M_SWL_PRINT_ASSIGN_NEW_LINE (1 << SWL_PRINT_ASSIGN_NEW_LINE)
#define M_SWL_PRINT_ADD_SPACE (1 << SWL_PRINT_ADD_SPACE)
#define M_SWL_PRINT_INDENT (1 << SWL_PRINT_INDENT)
#define M_SWL_PRINT_BOOL_INT (1 << SWL_PRINT_BOOL_INT)
#define M_SWL_PRINT_BOOL_STR_LOWER (1 << SWL_PRINT_BOOL_STR_LOWER)


#define M_SWL_PRINT_PRETTY M_SWL_PRINT_OPEN_NEW_LINE | M_SWL_PRINT_NEXT_NEW_LINE | M_SWL_PRINT_CLOSE_PRE_NEW_LINE | M_SWL_PRINT_ADD_SPACE | M_SWL_PRINT_INDENT


typedef enum {
    SWL_PRINT_PARSE_OPTION_CLEAN_LIST,
    SWL_PRINT_PARSE_OPTION_FAIL_ON_SMALL_TGT,
    SWL_PRINT_PARSE_OPTION_MAX,
} swl_print_parseOption_e;

/**
 * Argument struct for printing
 */
typedef struct {
    const char* delim[SWL_PRINT_DELIM_MAX];
    const char* charsToEscape;
    char escapeChar;
    char valueEncap;
    const char* nullEncode;
    size_t indentation;
    swl_print_option_m options;
    swl_print_parseOption_e parseOptions;
    size_t noNewLineIndentLevel;
} swl_print_args_t;

extern const swl_print_args_t g_swl_print_json;
extern const swl_print_args_t g_swl_print_jsonCompact;
extern const swl_print_args_t g_swl_print_dm;
extern const swl_print_args_t g_swl_print_csv;


bool swl_print_toStreamBuf(FILE* stream, swl_print_args_t* printArgs, const char* buf, size_t bufSize);
bool swl_print_toStreamArgs(FILE* stream, swl_print_args_t* printArgs, const char* format, ...);
bool swl_print_valueToStreamArgs(FILE* stream, swl_print_args_t* printArgs, const char* format, ...);

const swl_print_args_t* swl_print_getDefaultArgs();
ssize_t swl_print_getElementSize(const swl_print_args_t* printArgs, const char* elementStart, bool list);
ssize_t swl_print_getMapKeySize(const swl_print_args_t* printArgs, const char* elementStart);
bool swl_print_hasNext(const swl_print_args_t* printArgs, const char* elementStart, bool list);
ssize_t swl_print_getNextSize(const swl_print_args_t* printArgs, const char* elementStart, swl_print_delim_e delim);

void swl_print_addOpen(FILE* file, swl_print_args_t* args, bool list);
void swl_print_addClose(FILE* file, swl_print_args_t* args, bool list);
void swl_print_addNext(FILE* file, swl_print_args_t* args, bool list);
void swl_print_addAssign(FILE* file, swl_print_args_t* args);
void swl_print_startValue(FILE* file, swl_print_args_t* args);
void swl_print_stopValue(FILE* file, swl_print_args_t* args);

#endif /* SRC_INCLUDE_SWL_SWL_PRINT_H_ */

