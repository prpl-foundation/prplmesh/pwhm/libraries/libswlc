/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SWL_LLIST_H
#define SWL_LLIST_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>


/**

   @brief
   Doubly linked list implementation

   @details
   The list has two pointers, one to the first node (called head) and one to the last node (called tail)
   Each element in the list has two pointers, one to the previous element and one to the next.
   @image html 489px-Doubly-linked-list.svg.png "doubly linked list"
   If the linked list is empty, the head and tail pointers will be NULL. The previous pointer of the first node in the
   linked list will always be NULL, the next pointer of the last node in the linked list will always be NULL.

   @section swl_llist_create_items Adding data to a list
   The elements in the list do not contain a pointer to data, they are iterators. To create a list with elements
   containing data, a structure has to be defined that contains a swl_llist_iterator_t\n
   @verbatim
   typedef struct _int32_list_item {
    uint32_t data;
    swl_llist_iterator_t it;
   } int32_list_item_t;
   @endverbatim
   In the above example a list item is defined that contains a 32 bit signed integer. Before this can be used you
   have to initialize the iterator part of the structure using @ref swl_llist_iterator_initialize function.
   After doing that you can add an instance of this structure to any linked list.
   @verbatim
   swl_llist_t mylist;
   swl_llist_initialize(&mylist);

   int32_list_item_t *element = (int32_list_item_t *)calloc(1,sizeof(int32_list_item_t));
   swl_llist_iterator_initialize(&element->it);
   element->data = 105;
   swl_llist_append(&element->it);
   @endverbatim

   @section swl_llist_get_items Retreiving data from a list
   To retrieve data from a list, it is needed to get the correct iterator first. This can be done by using any
   of the following functions @ref swl_llist_first, @ref swl_llist_last, @ref swl_llist_iterator_next, @ref swl_llist_iterator_prev
   or even on a position in the list @ref swl_llist_at.\n
   To retrieve the data from the iterator, use the macro @ref swl_llist_item_data.
   @verbatim
   swl_llist_iterator_t *some_it = swl_llist_first(&mylist);
   int32_list_item_t *data_element = swl_llist_item_data(some_it,int32_list_item_t,it);
   @endverbatim

   @section swl_llist_traversing Traversing a list
   Some convieniance macros are provide to iterate a list:@ref swl_llist_for_each and @ref swl_llist_for_each_reverse.
   @warning
   While using these macros do not add elements to the list or delete elements from the list.

   @section swl_llist_considerations Some warnings and limitations
   @li Always initialize a linked list and iterators using @ref swl_llist_initialize and @ref swl_llist_iterator_initialize
   functions before using them.
   @li Never free an iterator directly, always free the memory allocated for the structure containing the iterator.
   @li When an iterator is added to a linked list, ownership of the pointer is assumed by the linked list. Freeing
   the iterator (or the iterator's container), will corrupt the linked list. Always make sure the iterator is not
   in a linked list anymore before freeing up the memory taken by the iterator.
   @li Never pass a swl_llist_t or swl_llist_iterator_t by value.
   @li Do not create lists containing different types of data elements, although this is possible, it will hard (or even
   impossible) to find out the iterator's container type. The type is needed to convert the iterator back to its container.
 */

/**
   @brief
   Definition of a helper macro for forward iteration through the linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forwards through a <b>linked list</b> starting from the head

   @warning
   don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param item a pointer variable to the list items
   @param list the list
 */
#define swl_llist_for_each(item, list) \
    for(item = swl_llist_first(list); item; item = swl_llist_iterator_next(item))


/**
   @brief
   Definition of a helper macro for forward iteration through the linked list (head->tail)

   @details
   This <b>helper macro</b> iterates forwards through a <b>linked list</b> starting from the head

   @warning
   don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param item a pointer variable to the list items
   @param list the list
 */

#define swl_llist_forEachit(item, list) \
    for(swl_llist_iterator_t* item = swl_llist_first(list), \
        * item ## _next = swl_llist_iterator_next(item); \
        item; \
        item = item ## _next, \
        item ## _next = swl_llist_iterator_next(item))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for backward iteration through the linked list (tail->head)

   @details
   This <b>helper macro</b> iterates backwards through a <b>linked list</b> starting from the tail

   @warning
   don&rsquo;t <b>modify</b> the linked list during the iteration!

   @param item a pointer variable to the list items
   @param list the list
 */
#define swl_llist_for_each_reverse(item, list) \
    for(item = swl_llist_last(list); item; item = swl_llist_iterator_prev(item))

// data macro
//---------------------------------------------------------------------------------------------
/**
   @brief
   Definition of a helper macro for getting a pointer to the real data structure.

   @details
   This <b>macro</b> calculates the address of the containing data structure.

   @warning
   If the wrong type is specified, the resulting pointer could be invalid.

   @param item a pointer to the linked list item for which we want to calculate the pointer to the containing structure
   @param type the type to which the pointer has to be casted
   @param member the name of the data member containing the list pointer
 */
#define swl_llist_item_data(item, type, member) \
    (type*) (((char*) item) - offsetof(type, member))

/**
   @brief
   Definition of a helper macro for getting a pointer to the real data structure.

   @details
   This <b>macro</b> calculates the address of the containing data structure.

   @warning
   If the wrong type is specified, the resulting pointer could be invalid.

   @param item a pointer to the linked list item for which we want to calculate the pointer to the containing structure
   @param type the type to which the pointer has to be casted
   @param member the name of the data member containing the list pointer
 */
#define swl_llist_iterator_data(item, type, member) \
    (type*) (((char*) item) - offsetof(type, member))

//---------------------------------------------------------------------------------------------
/**
   @brief
   Linked list iterator

   @details
   A linked list iterator
 */
typedef struct _swl_llist_iterator {
    struct _swl_llist_iterator* prev; /**< Pointer to the previous element. */
    struct _swl_llist_iterator* next; /**< Pointer to the next element. */
    struct _swl_llist* list;          /**< Pointer to the linked list to which this element belongs (NULL if not in any list) */
} swl_llist_iterator_t;

//---------------------------------------------------------------------------------------------
/**
   @brief
   Linked list

   @details
   A linked list container
 */
typedef struct _swl_llist {
    struct _swl_llist_iterator* head; /**< Pointer to the head element. */
    struct _swl_llist_iterator* tail; /**< Pointer to the tail element. */
} swl_llist_t;

/**
   @}
 */

// initializer functions
bool swl_llist_initialize(swl_llist_t* list);
bool swl_llist_iterator_initialize(swl_llist_iterator_t* it);

// cleanup functions
void swl_llist_cleanup(swl_llist_t* list);


swl_llist_iterator_t* swl_llist_first(const swl_llist_t* list);



swl_llist_iterator_t* swl_llist_last(const swl_llist_t* list);


swl_llist_iterator_t* swl_llist_at(const swl_llist_t* list, unsigned int index);


swl_llist_iterator_t* swl_llist_iterator_next(const swl_llist_iterator_t* it);



swl_llist_iterator_t* swl_llist_iterator_prev(const swl_llist_iterator_t* it);


// insertion functions
bool swl_llist_insertBefore(swl_llist_t* list, swl_llist_iterator_t* reference, swl_llist_iterator_t* insert);
bool swl_llist_insertAfter(swl_llist_t* list, swl_llist_iterator_t* reference, swl_llist_iterator_t* insert);
bool swl_llist_append(swl_llist_t* list, swl_llist_iterator_t* insert);
bool swl_llist_prepend(swl_llist_t* list, swl_llist_iterator_t* insert);
bool swl_llist_insertAt(swl_llist_t* list, unsigned int index, swl_llist_iterator_t* insert);

// removal functions
swl_llist_iterator_t* swl_llist_iterator_take(swl_llist_iterator_t* it);

swl_llist_iterator_t* swl_llist_takeFirst(swl_llist_t* list);
swl_llist_iterator_t* swl_llist_takeLast(swl_llist_t* list);


// property functions
unsigned int swl_llist_size(const swl_llist_t* list);


bool swl_llist_isEmpty(const swl_llist_t* list);

// sort functions
typedef int (* swl_llist_qsort_compar)(const swl_llist_iterator_t*, const swl_llist_iterator_t*, void* userdata);
bool swl_llist_qsort(swl_llist_t* list, swl_llist_qsort_compar compar, bool reverse, void* userdata);


/**
 * Loop over all entries of the list.
 * name will be declared as a pointer to the type.
 * member must be the iterating member of the list
 *
 * SWL_LLIST_FOR_EACH(myType, it, name, myList) {
 *      print("%s\n",name->charValue);
 * } SWL_LLIST_DONE;
 */
#define SWL_LLIST_FOR_EACH(type, member, name, list) { \
        swl_llist_iterator_t* _it = swl_llist_first(list); \
        while(_it != NULL) { \
            swl_llist_iterator_t* _nextIt = swl_llist_iterator_next(_it); \
            type* name = swl_llist_item_data(_it, type, member); \


#define SWL_LLIST_DONE \
    _it = _nextIt; \
}}

#endif
