/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * Defines for ieee802 information
 */
#ifndef SRC_INCLUDE_SWL_IEEE802_H_
#define SRC_INCLUDE_SWL_IEEE802_H_

#include <stdbool.h>
#include <stdint.h>
#include "swl/swl_common.h"

typedef enum {
    SWL_IEEE802_TRAFFIC_NORMAL,
    SWL_IEEE802_TRAFFIC_PRIO,
    SWL_IEEE802_TRAFFIC_AC_BE,
    SWL_IEEE802_TRAFFIC_AC_BK,
    SWL_IEEE802_TRAFFIC_AC_VI,
    SWL_IEEE802_TRAFFIC_AC_VO,
    SWL_IEEE802_TRAFFIC_AC_MAX
} swl_ieee802_trafficTypes_e;

typedef enum SWL_PACKED {
    SWL_IEEE802_RRM_REQ_MODE_PARALLEL,
    SWL_IEEE802_RRM_REQ_MODE_ENABLE,
    SWL_IEEE802_RRM_REQ_MODE_REQUEST,
    SWL_IEEE802_RRM_REQ_MODE_REPORT,
    SWL_IEEE802_RRM_REQ_MODE_MANDATORY_DURATION,
    SWL_IEEE802_RRM_REQ_MODE_MAX,
} swl_ieee802_rrmReqMode_e;

typedef uint8_t swl_ieee802_rrmReqMode_m;

#define M_SWL_IEEE802_RRM_REQ_MODE_PARALLEL (1 << SWL_IEEE802_RRM_REQ_MODE_PARALLEL)
#define M_SWL_IEEE802_RRM_REQ_MODE_ENABLE (1 << SWL_IEEE802_RRM_REQ_MODE_ENABLE)
#define M_SWL_IEEE802_RRM_REQ_MODE_REQUEST (1 << SWL_IEEE802_RRM_REQ_MODE_REQUEST)
#define M_SWL_IEEE802_RRM_REQ_MODE_REPORT (1 << SWL_IEEE802_RRM_REQ_MODE_REPORT)
#define M_SWL_IEEE802_RRM_REQ_MODE_MANDATORY_DURATION (1 << SWL_IEEE802_RRM_REQ_MODE_MANDATORY_DURATION)

typedef enum SWL_PACKED {
    SWL_IEEE802_RRM_REQ_TYPE_BASIC,
    SWL_IEEE802_RRM_REQ_TYPE_CCA,
    SWL_IEEE802_RRM_REQ_TYPE_RPI,
    SWL_IEEE802_RRM_REQ_TYPE_CL,
    SWL_IEEE802_RRM_REQ_TYPE_Noise,
    SWL_IEEE802_RRM_REQ_TYPE_BEACON,
    SWL_IEEE802_RRM_REQ_TYPE_FRAME,
    SWL_IEEE802_RRM_REQ_TYPE_SS,
    SWL_IEEE802_RRM_REQ_TYPE_LCI,
    SWL_IEEE802_RRM_REQ_TYPE_TSCM,
    SWL_IEEE802_RRM_REQ_TYPE_MD,
    SWL_IEEE802_RRM_REQ_TYPE_CIVIC,
    SWL_IEEE802_RRM_REQ_TYPE_IDENTIFIER,
    SWL_IEEE802_RRM_REQ_TYPE_DCQ,
    SWL_IEEE802_RRM_REQ_TYPE_DM,
    SWL_IEEE802_RRM_REQ_TYPE_DS,
    SWL_IEEE802_RRM_REQ_TYPE_FTM,
    SWL_IEEE802_RRM_REQ_TYPE_MAX,
} swl_ieee802_rrmReqType_e;

typedef enum SWL_PACKED {
    SWL_IEEE802_RRM_BEACON_REQ_MODE_PASSIVE,
    SWL_IEEE802_RRM_BEACON_REQ_MODE_ACTIVE,
    SWL_IEEE802_RRM_BEACON_REQ_MODE_TABLE,
    SWL_IEEE802_RRM_BEACON_REQ_MODE_MAX,
} swl_ieee802_rrmBeaconReqMode_e;

typedef enum SWL_PACKED {
    SWL_IEEE802_BTM_REQ_MODE_PREF_LIST_INCL,
    SWL_IEEE802_BTM_REQ_MODE_ABRIDGED,
    SWL_IEEE802_BTM_REQ_MODE_DISASSOC_IMMINENT,
    SWL_IEEE802_BTM_REQ_MODE_BSS_TERM_INCL,
    SWL_IEEE802_BTM_REQ_MODE_ESS_DISASSOC_IMMINENT,
    SWL_IEEE802_BTM_REQ_MODE_MAX,
} swl_ieee802_btmReqMode_e;

typedef uint8_t swl_ieee802_btmReqMode_m;

#define M_SWL_IEEE802_BTM_REQ_MODE_PREF_LIST_INCL (1 << SWL_IEEE802_BTM_REQ_MODE_PREF_LIST_INCL)
#define M_SWL_IEEE802_BTM_REQ_MODE_ABRIDGED (1 << SWL_IEEE802_BTM_REQ_MODE_ABRIDGED)
#define M_SWL_IEEE802_BTM_REQ_MODE_DISASSOC_IMMINENT (1 << SWL_IEEE802_BTM_REQ_MODE_DISASSOC_IMMINENT)
#define M_SWL_IEEE802_BTM_REQ_MODE_BSS_TERM_INCL (1 << SWL_IEEE802_BTM_REQ_MODE_BSS_TERM_INCL)
#define M_SWL_IEEE802_BTM_REQ_MODE_ESS_DISASSOC_IMMINENT (1 << SWL_IEEE802_BTM_REQ_MODE_ESS_DISASSOC_IMMINENT)


/* MBO v0.0_r19, 4.2.4: Association Disallowed Attribute */
/* Table 4-15: Reason Code Field Values */
typedef enum SWL_PACKED {
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_OFF = 0,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_UNSPECIFIED = 1,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_MAX_STA = 2,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_AIR_INTERFERENCE = 3,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_AUTH_SERVER_OVERLOAD = 4,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_LOW_RSSI = 5,
    SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_MAX
} swl_ieee802_mbo_assocDisallowReason_e;

extern const char* swl_ieee802_trafficTypes_str[SWL_IEEE802_TRAFFIC_AC_MAX];

extern const char* swl_ieee802_rrmReqMode_str[SWL_IEEE802_RRM_REQ_MODE_MAX];

extern const char* swl_ieee802_rrmReqType_str[SWL_IEEE802_RRM_REQ_TYPE_MAX];

extern const char* swl_ieee802_rrmBeaconReqMode_str[SWL_IEEE802_RRM_BEACON_REQ_MODE_MAX];

extern const char* swl_ieee802_btmReqMode_str[SWL_IEEE802_BTM_REQ_MODE_MAX];

extern const char* swl_ieee802_mboAssocDisallowReason_str[SWL_IEEE802_MBO_ASSOC_DISALLOW_REASON_MAX];

bool swl_ieee802_isValidRrmReport(uint8_t ref);
int32_t swl_ieee802_getRssiFromRcpi(uint8_t rcpi);

#endif /* SRC_INCLUDE_SWL_IEEE802_H_ */
