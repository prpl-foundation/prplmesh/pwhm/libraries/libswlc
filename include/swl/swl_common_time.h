/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_TIME_H_
#define SRC_INCLUDE_SWL_COMMON_TIME_H_

#include "time.h"
#include "swl/swl_common_type.h"

#define SWL_TIME_SEC_PER_MIN 60
#define SWL_TIME_MIN_PER_HOUR 60
#define SWL_TIME_SEC_PER_HOUR 3600
#define SWL_TIME_MIN_PER_DAY 1440
#define SWL_TIME_SEC_PER_DAY 86400
#define SWL_TIME_DAYS_PER_WEEK 7

/**
 * Represents a timestamp in monotonic format, as seconds from a non
 * descript point in past. This timestamp shall never shift and guarantee
 * that a 10 second difference will only happen if 10 seconds passed.
 *
 * Note that this timestamp should only be used to track and compare events
 * as they happen. For old events that happen before system boot, realtime should be used.
 */
typedef time_t swl_timeMono_t;
/**
 * Represents a timestamp in seconds from epoch. Note that real time may change
 * if users set a different data, or clock adjustments are made.
 */
typedef time_t swl_timeReal_t;

extern const struct tm swl_time_nullTimeTm;

swl_timeReal_t swl_time_getRealOfMonoBaseTime();
swl_timeMono_t swl_time_getMonoSec();
swl_timeReal_t swl_time_getRealSec();

void swl_time_getRealTm(struct tm* tmTime);
//no getMonoTm, as tm only makes sense as realtime value

swl_timeReal_t swl_time_monoToReal(swl_timeMono_t time);
swl_timeMono_t swl_time_realToMono(swl_timeReal_t time);

void swl_time_monoToTm(struct tm* tgtTm, const swl_timeMono_t srcTime);
void swl_time_realToTm(struct tm* tgtTm, const swl_timeReal_t srcTime);

swl_timeMono_t swl_time_tmToMono(struct tm* tmTime);
swl_timeReal_t swl_time_tmToReal(struct tm* tmTime);

ssize_t swl_time_monoToDate(char* tgtStr, size_t tgtStrSize, swl_timeMono_t srcTime);
ssize_t swl_time_realToDate(char* tgtStr, size_t tgtStrSize, swl_timeReal_t srcTime);

ssize_t swl_time_monoToLocalDate(char* tgtStr, size_t tgtStrSize, swl_timeMono_t srcTime);
ssize_t swl_time_realToLocalDate(char* tgtStr, size_t tgtStrSize, swl_timeReal_t srcTime);



#define swl_type_timeMono &gtSwl_type_timeMono
#define swl_type_timeReal &gtSwl_type_timeReal
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(TimeMono, gtSwl_type_timeMono, swl_timeMono_t)
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(TimeReal, gtSwl_type_timeReal, swl_timeReal_t)

#define swl_type_timeMono_impl gtSwl_type_timeMono
#define swl_type_timeReal_impl gtSwl_type_timeReal

SWL_REF_TYPE_H(gtSwl_type_timeRealPtr, gtSwl_type_timeReal);
SWL_REF_TYPE_H(gtSwl_type_timeMonoPtr, gtSwl_type_timeMono);


#endif /* SRC_INCLUDE_SWL_TIME_H_ */
