/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_MAPS_H_
#define SRC_INCLUDE_SWL_MAPS_H_

#include "swl/swl_map.h"
#include "swl/swl_common_chanspec.h"
#include "swl/swl_common.h"
#include "swl/swl_string.h"
#include <stdbool.h>
#include <stdint.h>

#define swl_map_createMap(mapName, mapTypeKey, mapTypeValue) \
    typedef swl_map_t swl_map ## mapName ## _t; \
    void swl_map ## mapName ## _init(swl_map ## mapName ## _t * map); \
    void swl_map ## mapName ## _cleanup(swl_map ## mapName ## _t * map); \
    swl_mapEntry_t* swl_map ## mapName ## _alloc(swl_map ## mapName ## _t * map); \
    bool swl_map ## mapName ## _add(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue value); \
    bool swl_map ## mapName ## _addOrSet(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue value); \
    void swl_map ## mapName ## _delete(swl_map ## mapName ## _t * map, mapTypeKey key); \
    bool swl_map ## mapName ## _has(swl_map ## mapName ## _t * map, mapTypeKey key); \
    bool swl_map ## mapName ## _set(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue val); \
    swl_mapEntry_t* swl_map ## mapName ## _getEntry(swl_map ## mapName ## _t * map, mapTypeKey key); \
    void swl_map ## mapName ## _deleteEntry(swl_map ## mapName ## _t * map, swl_mapEntry_t * key); \
    mapTypeValue swl_map ## mapName ## _get(swl_map ## mapName ## _t * map, mapTypeKey key); \
    mapTypeKey swl_map ## mapName ## _itKey(swl_mapIt_t * mapIt); \
    mapTypeValue swl_map ## mapName ## _itValue(swl_mapIt_t * mapIt);

#define swl_map_implement_common(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    void swl_map ## mapName ## _init(swl_map ## mapName ## _t * map) { \
        swl_map_init(map, keyType, valueType);} \
    void swl_map ## mapName ## _cleanup(swl_map ## mapName ## _t * map) { \
        swl_map_cleanup(map);} \
    swl_mapEntry_t* swl_map ## mapName ## _alloc(swl_map ## mapName ## _t * map) { \
        return swl_map_alloc(map);} \
    void swl_map ## mapName ## _deleteEntry(swl_map ## mapName ## _t * map, swl_mapEntry_t * entry) { \
        swl_map_deleteEntry(map, entry);}

#define swl_map_implementMapVal(mapName, mapTypeKey, keyType, mapTypeValue, valueType, defaultVal) \
    swl_map_implement_common(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    bool swl_map ## mapName ## _add(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_add(map, &key, &data);} \
    bool swl_map ## mapName ## _addOrSet(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_addOrSet(map, &key, &data);} \
    void swl_map ## mapName ## _delete(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_delete(map, &key);} \
    bool swl_map ## mapName ## _has(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_has(map, &key); } \
    bool swl_map ## mapName ## _set(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue val) { \
        return swl_map_set(map, &key, &val); } \
    mapTypeValue swl_map ## mapName ## _get(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        mapTypeValue* val = (mapTypeValue*) swl_map_get(map, &key); \
        return (val != NULL ? *val : defaultVal);} \
    swl_mapEntry_t* swl_map ## mapName ## _getEntry(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_getEntry(map, &key);} \
    mapTypeKey swl_map ## mapName ## _itKey(swl_mapIt_t * mapIt) { \
        return *((mapTypeKey*) swl_map_itKey(mapIt));} \
    mapTypeValue swl_map ## mapName ## _itValue(swl_mapIt_t * mapIt) { \
        return *((mapTypeValue*) swl_map_itValue(mapIt));}

#define swl_map_implementMapRef(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    swl_map_implement_common(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    bool swl_map ## mapName ## _add(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_add(map, key, data);} \
    bool swl_map ## mapName ## _addOrSet(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_addOrSet(map, key, data);} \
    void swl_map ## mapName ## _delete(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_delete(map, key);} \
    bool swl_map ## mapName ## _has(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_has(map, key); } \
    bool swl_map ## mapName ## _set(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue val) { \
        return swl_map_set(map, key, val); } \
    mapTypeValue swl_map ## mapName ## _get(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return (mapTypeValue) swl_map_get(map, key);} \
    swl_mapEntry_t* swl_map ## mapName ## _getEntry(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_getEntry(map, key);} \
    mapTypeKey swl_map ## mapName ## _itKey(swl_mapIt_t * mapIt) { \
        return (mapTypeKey) swl_map_itKey(mapIt);} \
    mapTypeValue swl_map ## mapName ## _itValue(swl_mapIt_t * mapIt) { \
        return (mapTypeValue) swl_map_itValue(mapIt);}

#define swl_map_implementMapRefVal(mapName, mapTypeKey, keyType, mapTypeValue, valueType, defaultVal) \
    swl_map_implement_common(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    bool swl_map ## mapName ## _add(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_add(map, key, &data);} \
    bool swl_map ## mapName ## _addOrSet(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_addOrSet(map, key, &data);} \
    void swl_map ## mapName ## _delete(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_delete(map, key);} \
    bool swl_map ## mapName ## _has(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_has(map, key); } \
    bool swl_map ## mapName ## _set(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue val) { \
        return swl_map_set(map, key, &val); } \
    mapTypeValue swl_map ## mapName ## _get(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        mapTypeValue* val = (mapTypeValue*) swl_map_get(map, key); \
        return (val != NULL ? *val : defaultVal);} \
    swl_mapEntry_t* swl_map ## mapName ## _getEntry(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_getEntry(map, key);} \
    mapTypeKey swl_map ## mapName ## _itKey(swl_mapIt_t * mapIt) { \
        return (mapTypeKey) swl_map_itKey(mapIt);} \
    mapTypeValue swl_map ## mapName ## _itValue(swl_mapIt_t * mapIt) { \
        return *((mapTypeValue*) swl_map_itValue(mapIt));}


#define swl_map_implementMapValRef(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    swl_map_implement_common(mapName, mapTypeKey, keyType, mapTypeValue, valueType) \
    bool swl_map ## mapName ## _add(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_add(map, &key, data);} \
    bool swl_map ## mapName ## _addOrSet(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue data) { \
        return swl_map_addOrSet(map, &key, data);} \
    void swl_map ## mapName ## _delete(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_delete(map, &key);} \
    bool swl_map ## mapName ## _has(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_has(map, &key); } \
    bool swl_map ## mapName ## _set(swl_map ## mapName ## _t * map, mapTypeKey key, mapTypeValue val) { \
        return swl_map_set(map, &key, val); } \
    mapTypeValue swl_map ## mapName ## _get(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return (mapTypeValue) swl_map_get(map, &key);} \
    swl_mapEntry_t* swl_map ## mapName ## _getEntry(swl_map ## mapName ## _t * map, mapTypeKey key) { \
        return swl_map_getEntry(map, &key);} \
    mapTypeKey swl_map ## mapName ## _itKey(swl_mapIt_t * mapIt) { \
        return *((mapTypeKey*) swl_map_itKey(mapIt));} \
    mapTypeValue swl_map ## mapName ## _itValue(swl_mapIt_t * mapIt) { \
        return (mapTypeValue) swl_map_itValue(mapIt);}

swl_map_createMap(Int32, int32_t, int32_t);
swl_map_createMap(Char, char*, char*);
swl_map_createMap(CharInt32, char*, int32_t);
swl_map_createMap(Int32Char, int32_t, char*);
swl_map_createMap(ChanInt32, int32_t, int32_t);
swl_map_createMap(ChanSpecInt32, swl_chanspec_t, int32_t);

#endif /* SRC_INCLUDE_SWL_MAPS_H_ */
