/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_SWL_COMMON_NAMEDTUPLETYPE_H_
#define SRC_INCLUDE_SWL_SWL_COMMON_NAMEDTUPLETYPE_H_

#include "swl_common_tupleType.h"
#include "subtypes/swl_mapSType.h"

/**
 * Create a named tuple type, using an X-Macro
 * Example:
 *
 * #define MY_TEST_TABLE_VAR(X, Y) \
    X(Y, gtSwl_type_int32, index, "SmallInt") \
    X(Y, gtSwl_type_int64, key, "BigInt") \
    X(Y, gtSwl_type_charPtr, val, "String")
   SWL_NTT(myTestNTTType, swl_myTestNTT_t, MY_TEST_TABLE_VAR, structMod)

 * The X-Macro must have arguments X, Y.
 * Each variable of the named tuple type must be define with:
 * X(Y, typeVariable, variableStructName, variableStringName) \
 * The typeVariable is the type of the variable, i.e. the name of the struct of the type
 * The variableName is the name as the variable should appear in its struct.
 * The variableStringName is the string name, to be used for interacting with the variable in its struct using various functions
 * The structMod is to add potential modifiers to the struct such as SWL_PACKED or force alignment
 */


typedef struct {
    swl_tupleType_t type;
    char** names;
} swl_ntt_t;

extern swl_typeFun_t swl_namedTupleType_fun;
extern swl_mapSTypeFun_t swl_namedTupleType_mapSTypeFun;

/*****************************************
* Internal macro's, DO NOT USE EXTERNALLY
*****************************************/
#define SWL_NTT_EXPAND_TYPE(name, swl_type, varName, stringName) \
    swl_type ## _type varName;

#define SWL_NTT_EXPAND_TUP_TYPE(name, swl_type, varName, stringName) (swl_type_t*) &swl_type,
#define SWL_NTT_EXPAND_NAME(name, swl_type, varName, stringName) stringName,
#define SWL_NTT_EXPAND_VAR_NAME(name, swl_type, varName, stringName) name ## _ ## varName,
#define SWL_NTT_EXPAND_VAR_NAME_MASK(name, swl_type, varName, stringName) m_ ## name ## _ ## varName = (1 << name ## _ ## varName),
#define SWL_NTT_EXPAND_OFFSET(name, swl_type, varName, stringName) offsetof(name, varName),

#define SWL_NTT_H_ENUM(typeName, structName, xMacro) \
    typedef structName typeName ## _type; \
    typedef enum { \
        xMacro(SWL_NTT_EXPAND_VAR_NAME, typeName) \
        typeName ## __max \
    } typeName ## _e; \

#define SWL_NTT_H_COMMON(typeName, structName, xMacro, structMod) \
    typedef struct structMod { \
        xMacro(SWL_NTT_EXPAND_TYPE, typeName) \
    } structName; \
    SWL_NTT_H_ENUM(typeName, structName, xMacro)

#define SWL_NTT_H_EXT(typeName, structName, xMacro) \
    extern swl_type_t* typeName ## Types[]; \
    extern char* typeName ## Names[]; \
    extern size_t typeName ## OffSets[]; \
    extern swl_ntt_t typeName;
/*****************************************
* End of internal macro's, DO NOT USE EXTERNALLY
*****************************************/

// Add named tuple type declaration to header, constains struct and enum definitions, and references to arrays
#define SWL_NTT_H(typeName, structName, xMacro, structMod) \
    SWL_NTT_H_COMMON(typeName, structName, xMacro, structMod) \
    SWL_NTT_H_EXT(typeName, structName, xMacro) \
    SWL_REF_TYPE_H(typeName ## Ptr, typeName)

// Add the tuple type implementation to c file. To be used together with SWL_NTT_H for declaration
#define SWL_NTT_C(typeName, structName, xMacro) \
    swl_type_t* typeName ## Types[] = ARR(xMacro(SWL_NTT_EXPAND_TUP_TYPE, typeName)); \
    char* typeName ## Names[] = ARR(xMacro(SWL_NTT_EXPAND_NAME, typeName)); \
    size_t typeName ## OffSets[] = ARR(xMacro(SWL_NTT_EXPAND_OFFSET, structName) \
                                       sizeof(structName)); \
    swl_ntt_t typeName = { \
        .type = { \
            .type = { \
                .size = sizeof(structName), \
                .typeFun = &swl_namedTupleType_fun, \
            }, \
            .types = typeName ## Types, \
            .nrTypes = SWL_ARRAY_SIZE(typeName ## Types), \
            .offsets = typeName ## OffSets, \
        }, \
        .names = typeName ## Names \
    }; \
    SWL_REF_TYPE_C(typeName ## Ptr, typeName)

// Add both type declaration and implementation to C file. Incompatible with SWL_NTT_H and SWL_NTT_C
#define SWL_NTT(typeName, structName, xMacro, structMod) \
    SWL_NTT_H_COMMON(typeName, structName, xMacro, structMod) \
    SWL_REF_TYPE_COMMON(typeName ## Ptr, typeName) \
    SWL_NTT_C(typeName, structName, xMacro)

// Add type annotation declaration to an existing file in header. For implementation part, SWL_NTT_C can be used
// This is used to "annotate" an existing type. Not all fields need to be annotated.
#define SWL_NTT_H_ANNOTATE(typeName, structName, xMacro) \
    SWL_NTT_H_ENUM(typeName, structName, xMacro) \
    SWL_NTT_H_EXT(typeName, structName, xMacro) \
    SWL_REF_TYPE_H(typeName ## Ptr, typeName)

/**
 * Create a named tuple type annotation for a given struct.
 * This allows export / import / ntt functions to be performed on the
 * tuple, without actually defining the type. allowing some more flexibility.
 * This is incompatible with using SWL_NTT_H_ANNOTATE and SWL_NTT_C
 */
#define SWL_NTT_ANNOTATE(typeName, structName, xMacro) \
    SWL_NTT_H_ENUM(typeName, structName, xMacro) \
    SWL_REF_TYPE_COMMON(typeName ## Ptr, typeName) \
    SWL_NTT_C(typeName, structName, xMacro)

/*
 * Define mask names for all fields in the struct. Note that this will limit possible number of values to
 * 32, as that's the default max size of an enum. As such, this is not standard included.
 */
#define SWL_NTT_MASK(typeName, structName, xMacro) \
    typedef enum { \
        xMacro(SWL_NTT_EXPAND_VAR_NAME_MASK, typeName) \
        m_ ## typeName ## __all = (1 << typeName ## __max) - 1 \
    } typeName ## _m; \


ssize_t swl_ntt_getIndexOfName(swl_ntt_t* type, const char* name);
swl_typeEl_t* swl_ntt_getRefByIndex(swl_ntt_t* type, const swl_tuple_t* tuple, size_t index);
swl_typeData_t* swl_ntt_getValByIndex(swl_ntt_t* type, const swl_tuple_t* tuple, size_t index);
swl_typeEl_t* swl_ntt_getRefByName(swl_ntt_t* type, const swl_tuple_t* tuple, const char* name);
swl_typeData_t* swl_ntt_getValByName(swl_ntt_t* type, const swl_tuple_t* tuple, const char* name);
size_t swl_ntt_getOffset(swl_ntt_t* type, size_t index);


bool swl_ntt_check(swl_ntt_t* type);
size_t swl_ntt_size(swl_ntt_t* type);

bool swl_ntt_equals(swl_ntt_t* tplType, swl_tuple_t* tuple1, swl_tuple_t* tuple2);
bool swl_ntt_equalsByMask(swl_ntt_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2, size_t typeMask);
size_t swl_ntt_getDiffMask(swl_ntt_t* tplType, const swl_tuple_t* tuple1, const swl_tuple_t* tuple2);
void swl_ntt_cleanup(swl_ntt_t* type, swl_tuple_t* tuple);
swl_type_t* swl_ntt_type(swl_ntt_t* type);

#endif /* SRC_INCLUDE_SWL_SWL_COMMON_NAMEDTUPLETYPE_H_ */
