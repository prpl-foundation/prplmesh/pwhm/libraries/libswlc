/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_COMMON_CHANSPEC_H_
#define SRC_INCLUDE_SWL_COMMON_CHANSPEC_H_

#include "swl/swl_common_radioStandards.h"
#include "swl/swl_common_type.h"
#include "swl/swl_typeEnum.h"
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * List of possible countries.
 * Actually , we support only USA , Japan , china and Europe countries
 */
typedef enum {
    SWL_OP_CLASS_COUNTRY_USA,
    SWL_OP_CLASS_COUNTRY_EU,
    SWL_OP_CLASS_COUNTRY_JP,
    SWL_OP_CLASS_COUNTRY_CN,
    SWL_OP_CLASS_COUNTRY_UNKNOWN, //used for unknown or global region
    SWL_OP_CLASS_COUNTRY_GLOBAL = SWL_OP_CLASS_COUNTRY_UNKNOWN,
    SWL_OP_CLASS_COUNTRY_MAX,
} swl_opClassCountry_e;

// List of strings showing operating class country regions
extern const char* swl_opClassRegion_str[];

/**
 * List of possible bandwidth for a communication link.
 *
 * Note that this list MUST first contain all bandwidths that can be used as a radio bandwidth,
 * and then all bandwidths for "sub 20MHz Resource Units". SWL_BW_RAD_MAX shall be the number of values
 * that can be configured as a radio bandwidth, so the "largest radio bandwidth" shall be equal
 * to SWL_BW_RAD_MAX - 1.
 *
 * This does NOT map to TR-181 OperatingChannelBandwidth directly, but rather the swl_radBw_e maps to that.
 * This maps to the MaxBandwidthSupported fields and others where the bandwidth is defined as a pure bandwidth instead
 * of other meta data.
 */
typedef enum {
    SWL_BW_AUTO,
    SWL_BW_20MHZ,
    SWL_BW_40MHZ,
    SWL_BW_80MHZ,
    SWL_BW_160MHZ,
    SWL_BW_320MHZ,
    SWL_BW_10MHZ,
    SWL_BW_5MHZ,
    SWL_BW_2MHZ,
    SWL_BW_MAX,
} swl_bandwidth_e;

/**
 * The last valid bandwidth for a radio. This value needs to be updated if ever a bigger BW gets available.
 */
#define SWL_BW_RAD_LAST SWL_BW_320MHZ


/**
 * Number of bw of swl_bandwidth_e list that are possible for a
 * radio to be set to. This should be all 20MHz bandwidths and above, which MUST come first.
 */
#define SWL_BW_RAD_MAX (SWL_BW_RAD_LAST + 1)


/**
 * Maximum number of channels a radio can cover at once
 */
#define SWL_BW_CHANNELS_MAX 16

/**
 * Default channel increment between 5 and 6GHz channels of 20MHz bw
 */
#define SWL_CHANNEL_INCREMENT 4


typedef uint32_t swl_bandwidth_m;

#define M_SWL_BW_AUTO (1 << SWL_BW_AUTO)
#define M_SWL_BW_20MHZ (1 << SWL_BW_20MHZ)
#define M_SWL_BW_40MHZ (1 << SWL_BW_40MHZ)
#define M_SWL_BW_80MHZ (1 << SWL_BW_80MHZ)
#define M_SWL_BW_160MHZ (1 << SWL_BW_160MHZ)
#define M_SWL_BW_320MHZ (1 << SWL_BW_320MHZ)
#define M_SWL_BW_RAD_LAST (1 << SWL_BW_RAD_LAST)
#define M_SWL_BW_10MHZ (1 << SWL_BW_10MHZ)
#define M_SWL_BW_5MHZ (1 << SWL_BW_5MHZ)
#define M_SWL_BW_2MHZ (1 << SWL_BW_2MHZ)
#define M_SWL_BW_ALL ((1 << SWL_BW_MAX) - 1)

// List of strings showing bandwidth, with SWL_BW_AUTO mapped to "Auto" String
extern const char* swl_bandwidth_str[];
// List of strings showing bandwidth, with SWL_BW_AUTO mapped to "Unknown" String
extern const char* swl_bandwidth_unknown_str[];
// List of integers to encode bandwidth
extern const uint32_t swl_bandwidth_int[];
// List of strings matching the bandwidth integers. So the bandwidth without "MHz" on the end.
extern const char* swl_bandwidth_intStr[];

/**
 * List of possible bandwidth that a radio can be configured, as defined in TR-181 OperatingChannelBandwidth.
 * Note that this is not part of a chanspec, but each chanspec can be mapped on one of these using
 * the swl_chanspec_toRadBw() function.
 */
typedef enum {
    SWL_RAD_BW_AUTO,
    SWL_RAD_BW_20MHZ,
    SWL_RAD_BW_40MHZ,
    SWL_RAD_BW_80MHZ,
    SWL_RAD_BW_160MHZ,
    SWL_RAD_BW_8080MHZ,
    SWL_RAD_BW_320MHZ1,
    SWL_RAD_BW_320MHZ2,
    SWL_RAD_BW_MAX
} swl_radBw_e;

typedef uint32_t swl_radBw_m;

#define M_SWL_RAD_BW_AUTO (1 << SWL_RAD_BW_AUTO)
#define M_SWL_RAD_BW_20MHZ (1 << SWL_BW_20MHZ)
#define M_SWL_RAD_BW_40MHZ (1 << SWL_BW_40MHZ)
#define M_SWL_RAD_BW_80MHZ (1 << SWL_BW_80MHZ)
#define M_SWL_RAD_BW_160MHZ (1 << SWL_RAD_BW_160MHZ)
#define M_SWL_RAD_BW_8080MHZ (1 << SWL_RAD_BW_8080MHZ)
#define M_SWL_RAD_BW_320MHZ1 (1 << SWL_RAD_BW_320MHZ1)
#define M_SWL_RAD_BW_320MHZ2 (1 << SWL_RAD_BW_320MHZ2)
#define M_SWL_RAD_BW_ALL ((1 << SWL_RAD_BW_MAX) - 1)

/**
 * List of frequency bands as possible in TR-181 OperatingFrequencyBand parameter.
 * This does not contain a default or auto value, as to be a 1 to 1 mapping to this parameter.
 */
typedef enum {
    SWL_FREQ_BAND_2_4GHZ,
    SWL_FREQ_BAND_5GHZ,
    SWL_FREQ_BAND_6GHZ,
    SWL_FREQ_BAND_MAX,
} swl_freqBand_e;

typedef uint32_t swl_freqBand_m;

#define M_SWL_FREQ_BAND_NONE_MASK 0x0
#define M_SWL_FREQ_BAND_2_4GHZ (1 << SWL_FREQ_BAND_2_4GHZ)
#define M_SWL_FREQ_BAND_5GHZ (1 << SWL_FREQ_BAND_5GHZ)
#define M_SWL_FREQ_BAND_6GHZ (1 << SWL_FREQ_BAND_6GHZ)
#define M_SWL_FREQ_BAND_MAX (1 << SWL_FREQ_BAND_MAX)
#define M_SWL_BAND_DUAL_MASK (M_SWL_FREQ_BAND_2_4GHZ | M_SWL_FREQ_BAND_5GHZ)

/**
 * Bitmask of radio standards that are defined in band given as key.
 *
 * Keys are from #swl_freqBand_e.
 */
extern const swl_radioStandard_m swl_freqBand_radStd[];

/**
 * Legacy radio standards that are defined in band given as key.
 *
 * Keys are from #swl_freqBand_e.
 */
extern const swl_radStd_e swl_freqBand_legacyRadStd[];

typedef enum {
    SWL_FREQ_BAND_EXT_2_4GHZ,
    SWL_FREQ_BAND_EXT_5GHZ,
    SWL_FREQ_BAND_EXT_6GHZ,
    SWL_FREQ_BAND_EXT_NONE,
    SWL_FREQ_BAND_EXT_AUTO, //AUTO also doubles for unknown band
    SWL_FREQ_BAND_EXT_MAX,
} swl_freqBandExt_e;

typedef uint32_t swl_freqBandExt_m;

#define M_SWL_FREQ_BAND_EXT_2_4GHZ (1 << SWL_FREQ_BAND_EXT_2_4GHZ)
#define M_SWL_FREQ_BAND_EXT_5GHZ (1 << SWL_FREQ_BAND_EXT_5GHZ)
#define M_SWL_FREQ_BAND_EXT_6GHZ (1 << SWL_FREQ_BAND_EXT_6GHZ)
#define M_SWL_FREQ_BAND_EXT_NONE (1 << SWL_FREQ_BAND_EXT_NONE)
#define M_SWL_FREQ_BAND_EXT_AUTO (1 << SWL_FREQ_BAND_EXT_AUTO)
#define M_SWL_FREQ_BAND_EXT_MAX (1 << SWL_FREQ_BAND_EXT_MAX)

// List of strings showing frequency band
extern const char* swl_freqBand_str[];
// List of short strings showing frequency band
extern const char* swl_freqBandShort_str[];
// List of short strings with only the frequency number
extern const char* swl_freqBandNumberOnly_str[];
// List of strings showing frequency band, with SWL_FREQ_BAND_NONE mapped to "None" String
extern const char* swl_freqBandExt_str[];
// List of strings showing frequency band, with SWL_FREQ_BAND_NONE mapped to "Unknown" String
extern const char* swl_freqBandExt_unknown_str[];

#define CHAN_MAX_VALUE  32

typedef enum {
    SWL_BAND_UNII_UNKNOWN,
    SWL_BAND_UNII_1,  //36-48
    SWL_BAND_UNII_2A, //52-68
    SWL_BAND_UNII_2B, //70-96
    SWL_BAND_UNII_2C, //100-144
    SWL_BAND_UNII_3,  //149-165
    SWL_BAND_UNII_4,  //167-181
    SWL_BAND_UNII_5,  //1-93
    SWL_BAND_UNII_6,  //97-113
    SWL_BAND_UNII_7,  //117-185
    SWL_BAND_UNII_8,  //189-233
    SWL_BAND_MAX,
} swl_uniiBand_e;

typedef uint32_t swl_uniiBand_m;
#define M_SWL_BAND_UNII_UNKNOWN (1 << SWL_BAND_UNII_UNKNOWN)
#define M_SWL_BAND_UNII_1       (1 << SWL_BAND_UNII_1)
#define M_SWL_BAND_UNII_2A      (1 << SWL_BAND_UNII_2A)
#define M_SWL_BAND_UNII_2B      (1 << SWL_BAND_UNII_2B)
#define M_SWL_BAND_UNII_2C      (1 << SWL_BAND_UNII_2C)
#define M_SWL_BAND_UNII_3       (1 << SWL_BAND_UNII_3)
/* UNII_2B is not set as part of UNII_2 because it is not officialy supported yet */
#define M_SWL_BAND_UNII_2       (M_SWL_BAND_UNII_2A | M_SWL_BAND_UNII_2C)
#define M_SWL_BAND_UNII_1_2     (M_SWL_BAND_UNII_1 | M_SWL_BAND_UNII_2)
#define M_SWL_BAND_UNII_1_2A    (M_SWL_BAND_UNII_1 | M_SWL_BAND_UNII_2A)
#define M_SWL_BAND_UNII_4       (1 << SWL_BAND_UNII_4)
#define M_SWL_BAND_UNII_5       (1 << SWL_BAND_UNII_5)
#define M_SWL_BAND_UNII_6       (1 << SWL_BAND_UNII_6)
#define M_SWL_BAND_UNII_7       (1 << SWL_BAND_UNII_7)
#define M_SWL_BAND_UNII_8       (1 << SWL_BAND_UNII_8)
#define M_SWL_BAND_UNII_ALL_5G  (M_SWL_BAND_UNII_1 | M_SWL_BAND_UNII_2 | M_SWL_BAND_UNII_3 | M_SWL_BAND_UNII_4)
#define M_SWL_BAND_UNII_ALL_6G  (M_SWL_BAND_UNII_5 | M_SWL_BAND_UNII_6 | M_SWL_BAND_UNII_7 | M_SWL_BAND_UNII_8)
#define M_SWL_BAND_UNII_ALL     (M_SWL_BAND_UNII_ALL_5G | M_SWL_BAND_UNII_ALL_6G)

extern const char* swl_uniiBand_str[];

// Strings for swl_radBw_e directly compatible with tr-181
extern const char* swl_radBw_str[];

// Shorter version of bandwidth strings for debugging purposes.
extern const char* swl_radBw_strShort[];

/**
 * Map a radio bandwidth to the integer bandwidth of the "primary" operating band. So 80+80 gets mapped to 80MHz,
 * 320MHz-1 and 320MHz-2 get mapped to 320.
 */
extern const swl_bandwidth_e swl_radBw_toBw[];


typedef enum {
    SWL_PHYMODE_UNKNOWN = 0,
    SWL_PHYMODE_HT = 7,
    SWL_PHYMODE_VHT = 9,
    SWL_PHYMODE_HE = 14,
    SWL_PHYMODE_EHT = 18,
} swl_phymode_ne;

/**
 * Default invalid channel identifier.
 * 0 is an invalid channel on all bands.
 */
#define SWL_CHANNEL_INVALID 0
#define SWL_CHANNEL_2G_START 1
#define SWL_CHANNEL_2G_END 14

#define SWL_CHANNEL_5G_UNII_1_START 36
#define SWL_CHANNEL_5G_UNII_1_END 48
#define SWL_CHANNEL_5G_UNII_2A_START 52
#define SWL_CHANNEL_5G_UNII_2A_END 64
#define SWL_CHANNEL_5G_UNII_2B_START 68
#define SWL_CHANNEL_5G_UNII_2B_END 96
#define SWL_CHANNEL_5G_UNII_2C_START 100
#define SWL_CHANNEL_5G_UNII_2C_END 144
#define SWL_CHANNEL_5G_UNII_3_START 149
#define SWL_CHANNEL_5G_UNII_3_END 165
#define SWL_CHANNEL_5G_UNII_4_START 167
#define SWL_CHANNEL_5G_UNII_4_END 181
#define SWL_CHANNEL_5G_START SWL_CHANNEL_5G_UNII_1_START
#define SWL_CHANNEL_5G_END SWL_CHANNEL_5G_UNII_4_END
#define SWL_CHANNEL_5G_DFS_START SWL_CHANNEL_5G_UNII_2A_START
#define SWL_CHANNEL_5G_DFS_END SWL_CHANNEL_5G_UNII_2C_END
#define SWL_CHANNEL_5G_WEATHER_START 120
#define SWL_CHANNEL_5G_WEATHER_END 128

#define SWL_CHANNEL_6G_UNII_5_START 1
#define SWL_CHANNEL_6G_UNII_5_END 93
#define SWL_CHANNEL_6G_UNII_6_START 97
#define SWL_CHANNEL_6G_UNII_6_END 113
#define SWL_CHANNEL_6G_UNII_7_START 117
#define SWL_CHANNEL_6G_UNII_7_END 185
#define SWL_CHANNEL_6G_UNII_8_START 189
#define SWL_CHANNEL_6G_UNII_8_END 233
#define SWL_CHANNEL_6G_START SWL_CHANNEL_6G_UNII_5_START
#define SWL_CHANNEL_6G_END SWL_CHANNEL_6G_UNII_8_END
#define SWL_CHANNEL_6G_PSC_FIRST 5
#define SWL_CHANNEL_6G_PSC_INTER 16

#define SWL_CHANNEL_2G_FREQ_START 2407
#define SWL_CHANNEL_2G_FREQ_END 2484
#define SWL_CHANNEL_5G_FREQ_START 5000
#define SWL_CHANNEL_6G_FREQ_START 5950
#define SWL_CHANNEL_6G_FREQ_END 7115
#define SWL_CHANNEL_6G_FREQ_CHAN2 5935
#define SWL_CHANNEL_INTER_FREQ_5MHZ 5

typedef uint8_t swl_channel_t;
typedef uint8_t swl_operatingClass_t;
extern const swl_channel_t swl_channel_defaults[SWL_FREQ_BAND_MAX];
extern const swl_bandwidth_e swl_bandwidth_defaults[SWL_FREQ_BAND_MAX];

typedef enum {
    /**
     * Auto extention handling:
     * For 40MHz @ 2.4: EXT_LOW for > 6, EXT_HIGH for <= 6
     * FOr 320MHz : EXT_LOW always (320MHz-1)
     */
    SWL_CHANSPEC_EXT_AUTO,
    SWL_CHANSPEC_EXT_LOW,
    SWL_CHANSPEC_EXT_HIGH,
    SWL_CHANSPEC_EXT_MAX,
} swl_chanspec_ext_e;

/**
 * A chanspec specifying either a single operating channel, or a radio configuration.
 *
 */
typedef struct {
    swl_channel_t channel;
    swl_bandwidth_e bandwidth;
    swl_freqBandExt_e band;
    /**
     * Second channel. Setting this means this chanspec represents an 80+80 chanspec in case
     * bandwidth == 80.
     * Can also be used for 160+80 mode, in which case this channel is still 80Mhz primary channel
     */
    swl_channel_t secondChannel;
    /**
     * Enum indicating whether the Above or Below control channel is used when bandwidth is 40MHz (only relevant on 2.4GHz)
     * Or if the 320Mhz band is 320MHz-1 (low / auto) or 320MHz-2 (high)
     */
    swl_chanspec_ext_e extensionHigh;
} swl_chanspec_t;

typedef struct {
    swl_uniiBand_m mask;
    const char* name;
} swl_chanspec_uniiBandGroupInfo_t;

swl_phymode_ne swl_chanspec_operClassToPhyMode(swl_operatingClass_t operclass);
swl_freqBandExt_e swl_chanspec_operClassToFreq(swl_operatingClass_t operclass);
swl_uniiBand_m swl_chanspec_operClassToUniiMask(swl_operatingClass_t operclass);
swl_bandwidth_e swl_chanspec_operClassToBandwidth(swl_operatingClass_t operclass);
swl_freqBandExt_e swl_chanspec_uniiToFreq(swl_uniiBand_m uniiBands);
swl_uniiBand_m swl_chanspec_toUniiMask(const swl_chanspec_t* chanspec);
swl_uniiBand_e swl_chanspec_toUnii(const swl_chanspec_t* chanspec);
swl_uniiBand_m swl_chanspec_getUniiBandGroupMaskWithName(const char* groupName);
const char* swl_chanspec_getUniiBandGroupNameWithMask(swl_uniiBand_m groupMask);
bool swl_chanspec_isValidBand(swl_freqBand_e freqBand);

/**
 * Function checks whether freqBandExt has a valid frequency band value (2.4GHz, 5GHz, 6GHz)
 * @param freqBand: frequency Band
 * @return true if frequency Band is in [SWL_FREQ_BAND_EXT_2_4GHZ, SWL_FREQ_BAND_EXT_5GHZ, SWL_FREQ_BAND_EXT_6GHZ] else false
 */
bool swl_chanspec_isValidBandExt(swl_freqBandExt_e freqBandExt);

#define SWL_CHANSPEC_NEW(_channel, _bandwidth, _band) \
    {.channel = _channel, .bandwidth = _bandwidth, .band = _band, .extensionHigh = SWL_CHANSPEC_EXT_AUTO, .secondChannel = 0}

#define SWL_CHANSPEC_NEW_EXT(_channel, _bandwidth, _band, _extHigh, _secondChan) \
    {.channel = _channel, .bandwidth = _bandwidth, .band = _band, .extensionHigh = _extHigh, .secondChannel = _secondChan}

#define SWL_CHANSPEC_EMPTY SWL_CHANSPEC_NEW(0, SWL_BW_AUTO, SWL_FREQ_BAND_EXT_NONE)

/*List of utility functions used for channel spec */
swl_bandwidth_e swl_chanspec_intToBw(uint32_t intBw);
int32_t swl_chanspec_bwToInt(swl_bandwidth_e swlBw);
int32_t swl_chanspec_radBwToInt(swl_radBw_e swlBw);

int32_t swl_chanspec_arrayFromChar(swl_chanspec_t* chanSpec, uint8_t count, const char* srcStrList);
bool swl_chanspec_arrayToChar(char* tgtStr, uint32_t tgtStrSize, swl_chanspec_t* srcData, size_t srcDataSize);

swl_freqBand_e swl_chanspec_getFreq(swl_chanspec_t* chanSpec, swl_freqBand_e defaultFreqBand, bool* hasMatch);
swl_freqBand_e swl_chanspec_freqBandExtToFreqBand(swl_freqBandExt_e freqBandExt, swl_freqBand_e defaultFreqBand, bool* hasMatch);

/**
 * Deduces a valid frequency band when the base channel argument matches only one frequency band channel list.
 *
 * @param baseChannel: base (primary) channel
 * @return: the matching swl_freqBandExt_e,
 *          SWL_FREQ_BAND_EXT_AUTO value when channel has multiple matches,
 *          or SWL_FREQ_BAND_EXT_NONE when channel has no matches
 */
swl_freqBandExt_e swl_chanspec_freqBandExtFromBaseChannel(swl_channel_t baseChannel);

swl_operatingClass_t swl_chanspec_getOperClassDirect(swl_channel_t channel, swl_freqBandExt_e band, swl_bandwidth_e bandwidth);
swl_operatingClass_t swl_chanspec_getOperClass(swl_chanspec_t* chanSpec);

swl_operatingClass_t swl_chanspec_getLocalOperClass(swl_chanspec_t* chanSpec, swl_opClassCountry_e countryIndex);
swl_operatingClass_t swl_chanspec_getGlobalOperClassfromLocal(swl_operatingClass_t localOpertingClass, swl_opClassCountry_e countryIndex);
swl_freqBandExt_e swl_chanspec_localOperClassToFreq(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex);
swl_phymode_ne swl_chanspec_localOperClassToPhyMode(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex);
swl_uniiBand_m swl_chanspec_localOperClassToUniiMask(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex);
swl_bandwidth_e swl_chanspec_localOperClassToBandwidth(swl_operatingClass_t operclass, swl_opClassCountry_e countryIndex);
swl_bandwidth_e swl_chanspec_getBwFromFreqCtrlCentre(swl_freqBandExt_e freqBand, swl_channel_t ctrlChan, swl_channel_t centerChan);
uint8_t swl_chanspec_getNrChannelsPerFreqAndBw(swl_freqBandExt_e band, swl_bandwidth_e bandwidth);
uint8_t swl_chanspec_getNrChannelsInBand(const swl_chanspec_t* chanspec);
swl_channel_t swl_chanspec_getBaseChannel(const swl_chanspec_t* chanspec);
swl_channel_t swl_chanspec_getCentreChannel(const swl_chanspec_t* chanspec);
uint8_t swl_chanspec_getChannelsInChanspec(const swl_chanspec_t* chanspec, swl_channel_t* channelsList, uint8_t listSize);
bool swl_channel_isInChanspec(const swl_chanspec_t* chanspec, const swl_channel_t testChannel);
swl_channel_t swl_channel_getComplementaryBaseChannel(const swl_chanspec_t* chanspec);

swl_rc_ne swl_chanspec_channelFromMHz(swl_chanspec_t* pChanSpec, uint32_t freqMHz);
swl_rc_ne swl_chanspec_channelToMHz(const swl_chanspec_t* pChanSpec, uint32_t* freqMHz);
uint32_t swl_chanspec_channelToMHzDef(const swl_chanspec_t* pChanSpec, uint32_t defFreqMHz);

swl_rc_ne swl_chanspec_fromMHz(swl_chanspec_t* pChanSpec, uint32_t freqMHz);
swl_rc_ne swl_chanspec_fromFreqAndCentreChannel(swl_chanspec_t* pChanSpec, swl_freqBandExt_e freq, swl_channel_t centreChannel);
swl_rc_ne swl_chanspec_fromFreqCtrlCentre(swl_chanspec_t* pChanSpec, swl_freqBandExt_e freq, swl_channel_t ctrlChannel, swl_channel_t centreChannel);
swl_rc_ne swl_chanspec_toMHz(const swl_chanspec_t* pChanSpec, uint32_t* freqMHz);
uint32_t swl_chanspec_toMHzDef(const swl_chanspec_t* pChanSpec, uint32_t defFreqMHz);
swl_radBw_e swl_chanspec_toRadBw(const swl_chanspec_t* pCs);

bool swl_channel_is2g(const swl_channel_t channel);
bool swl_channel_is5g(const swl_channel_t channel);
bool swl_channel_is6g(const swl_channel_t channel);
bool swl_channel_is6gPsc(const swl_channel_t channel);
bool swl_channel_isDfs(const swl_channel_t channel);
bool swl_chanspec_isDfs(const swl_chanspec_t chanspec);
bool swl_channel_isWeather(const swl_channel_t channel);
bool swl_chanspec_isWeather(const swl_chanspec_t chanspec);
bool swl_channel_isHighPower(const swl_channel_t channel);
bool swl_chanspec_isHighPower(const swl_chanspec_t chanspec);
bool swl_channel_isHighPowerWithCountry(const swl_channel_t channel, const swl_opClassCountry_e country);
swl_opClassCountry_e swl_chanspec_regulatoryDomainToCountry(const char* regulatoryDomain);

swl_rc_ne swl_chanspec_fromDmExt(swl_chanspec_t* targetChanspec, swl_channel_t channel, swl_radBw_e bw, swl_freqBandExt_e freq,
                                 swl_channel_t extChannel, swl_chanspec_ext_e extentionHigh);
swl_chanspec_t swl_chanspec_fromDm(swl_channel_t channel, swl_radBw_e bw, swl_freqBandExt_e freq);

/**
 * Type definitions
 */

#define swl_type_chanspec &gtSwl_type_chanspec
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(Chanspec, gtSwl_type_chanspec, swl_chanspec_t)

/*List of utility functions used for channel spec ext */
int32_t swl_chanspec_freqBandExtToInt(swl_freqBandExt_e swlBand);
swl_freqBandExt_e swl_chanspec_intTofreqBandExt(uint32_t intBand);

#define swl_type_chanspecExt &gtSwl_type_chanspecExt
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(ChanspecExt, gtSwl_type_chanspecExt, swl_chanspec_t)

SWL_TYPE_ENUM_H(gtSwl_type_bandwidth, swl_bandwidth_e);
#define swl_type_bandwidth &gtSwl_type_bandwidth
#define swl_bandwidth_etptr &gtSwl_type_bandwidth

SWL_TYPE_ENUM_H(gtSwl_type_bandwidthUnknown, swl_bandwidth_e);
#define swl_type_bandwidthUnknown &gtSwl_type_bandwidthUnknown

SWL_TYPE_ENUM_H(gtSwl_type_freqBand, swl_freqBand_e);
#define swl_type_freqBand &gtSwl_type_freqBand

SWL_TYPE_ENUM_H(gtSwl_type_freqBandExt, swl_freqBandExt_e);
#define swl_type_freqBandExt &gtSwl_type_freqBandExt

SWL_TYPE_ENUM_H(gtSwl_type_freqBandExtUnknown, swl_freqBandExt_e);
#define swl_type_freqBandExtUnknown &gtSwl_type_freqBandExtUnknown

SWL_TYPE_ENUM_H(gtSwl_type_uniiBand, swl_uniiBand_e);
#define swl_type_uniiBand &gtSwl_type_uniiBand



//Obsolete def, please use lower case indent
#define swl_type_ChanSpec swl_type_chanspec
#define swl_type_ChanInt32 swl_type_int32

/*
 * Operating class table for regulatory region to operating class conversion.
 */
typedef struct {
    swl_operatingClass_t global;                              // global operation class
    swl_operatingClass_t local_set[SWL_OP_CLASS_COUNTRY_MAX]; // local operating classes that matches the global operation class value
} swl_operatingClassTranslation_t;

/*
 * Operating class description.
 */
typedef struct {
    swl_operatingClass_t operClass;
    bool isCenterChannelset;         /* if 'Channel set' or 'Channel center set' is used */
    swl_bandwidth_e bandwidth;
    swl_freqBandExt_e band;
    swl_channel_t* channels;
} swl_operatingClassInfo_t;

/*
 * Operating class extended description.
 */
typedef struct {
    swl_opClassCountry_e countryRegion;            // country region Id
    const swl_operatingClassInfo_t* operClassInfo; // pointer to operating class info entry in region's table
} swl_operatingClassCountryInfo_t;

uint32_t swl_chanspec_findAllOperClassCountryInfo(swl_operatingClassCountryInfo_t* pResults, size_t nMaxResults, swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel);
uint32_t swl_chanspec_findAllOperClassMatches(swl_chanspec_t* pChSpecs, size_t nMaxChSpecs, swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel);
swl_chanspec_t swl_chanspec_fromOperClassExt(swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel, bool useFirstMatch);
swl_chanspec_t swl_chanspec_fromOperClass(swl_operatingClass_t opClass, swl_opClassCountry_e opClassRegion, swl_channel_t channel);

#endif /* SRC_INCLUDE_SWL_RADIOCHANNEL_C_ */
