/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_SWL_SWL_COMMON_MEM_H_
#define INCLUDE_SWL_SWL_COMMON_MEM_H_

#include "swl/swl_common.h"
#include "swl/swl_assert.h"
#include "swl/swl_llist.h"
#include "swl/swl_returnCode.h"

/**
 * Defines the memory destructor handler, which is called when the reference
 * of a memory object goes down to zero
 *
 * @param data Pointer to memory object
 */
typedef void (* swl_mem_destroy_h)(void* ptr);

/**
 * Defines the memory threshold handler, which is called when the maximum
 * size set is reached.
 *
 * @param curSize Current size of allocated memory.
 */
typedef void (* swl_mem_threshold_h)(size_t curSize);

/**
 * Defines memory zone, where allocation belong
 */
typedef struct {
    const char* name;
    uint32_t nrAllocs;
    size_t size;
} swl_memZone_t;

/**
 * Initialise internal memory manager
 *
 * @param max Maximum memory allowed for the process
 */
swl_rc_ne swl_mem_setThreshold(size_t max);

/**
 * Initialise internal memory manager
 *
 * @param th Threshold handler, called when threshold reached
 */
swl_rc_ne swl_mem_setThresholdHandler(swl_mem_threshold_h th);

/**
 * Allocate monitored memory using alloc()
 *
 * @param zone Zone where memory object will belong
 * @param size Size of the memory to allocates
 * @param dh Destructor handler
 */
void* swl_mem_alloc(swl_memZone_t* zone, size_t size, swl_mem_destroy_h dh);

/**
 * Same as @swl_mem_alloc with zeroed.
 *
 * @param zone Zone where memory object will belong
 * @param size Size of the memory to allocates
 * @param dh Destructor handler
 */
void* swl_mem_zalloc(swl_memZone_t* zone, size_t size, swl_mem_destroy_h dh);

/**
 * Copy memory into a monitored memory
 *
 * @param ptr Pointer to memory
 */
void* swl_mem_dup(void* ptr);

/**
 * Duplicate string into a monitored memory
 *
 * @param zone Zone where memory object will belong
 * @param str String
 */
char* swl_mem_dupStr(swl_memZone_t* zone, const char* str);

/**
 * Gets a new reference
 *
 * @param ptr Pointer to memory
 */
void* swl_mem_ref(void* ptr);

/**
 * Free one reference, and free all memory if the number
 * of reference reachs zero
 *
 * @param pptr Reference to memory object
 * @note pptr wiil be reset to NULL pointer after call.
 */
void swl_mem_unref(void** pptr);

/**
 * Get number of references to a pointer
 *
 * @param ptr Pointer to memory
 */
uint32_t swl_mem_nref(void* ptr);

/**
 * Get the size of the allocated memory.
 *
 * @param ptr Pointer to memory
 */
size_t swl_mem_size(void* ptr);

/**
 * Get total memory allocated
 */
size_t swl_mem_total();

/**
 * Prints all allocated memory through swl
 */
void swl_mem_log();

#endif /* INCLUDE_SWL_SWL_COMMON_MEM_H_ */
