/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_COMMON_TIME_SPEC_H_
#define SRC_INCLUDE_SWL_COMMON_TIME_SPEC_H_

#include "time.h"
#include "swl/swl_common_type.h"
#include "swl/swl_datetime.h"

#define SWL_TIMESPEC_NANO_PER_SEC 1000000000
#define SWL_TIMESPEC_MICRO_PER_SEC 1000000
#define SWL_TIMESPEC_MILLI_PER_SEC 1000
#define SWL_TIMESPEC_NANO_PER_MILLI 1000000
#define SWL_TIMESPEC_NANO_PER_MICRO 1000
#define SWL_TIMESPEC_MICRO_PER_MILLI 1000

#define SWL_NULLTIME_NANO_STR "0001-01-01T00:00:00.000000Z"
#define SWL_TIMESPEC_STR_SIZE sizeof(SWL_NULLTIME_NANO_STR)

/**
 * Represent an abstract timespec, to encode intervals.
 * It does not represent an actual time.
 */
typedef struct timespec swl_timeSpec_t;

/**
 * Represents a time spec in monotonic format
 */
typedef struct timespec swl_timeSpecMono_t;

/**
 * Represents a time spec from epoch
 */
typedef struct timespec swl_timeSpecReal_t;

void swl_timespec_addTime(struct timespec* ts, int64_t seconds, int64_t nanoseconds);
void swl_timespec_add(struct timespec* result, const struct timespec* spec1, const struct timespec* spec2);

void swl_timespec_getRealOfMonoBaseTime(swl_timeSpecReal_t* base);
swl_timeSpecReal_t swl_timespec_getRealOfMonoBaseTimeVal();
void swl_timespec_getMono(swl_timeSpecMono_t* ts);
swl_timeSpecMono_t swl_timespec_getMonoVal();
void swl_timespec_getReal(swl_timeSpecReal_t* ts);
swl_timeSpecReal_t swl_timespec_getRealVal();
void swl_timespec_monoToReal(swl_timeSpecReal_t* tgtTime, const swl_timeSpecMono_t* srcTime);
void swl_timespec_realToMono(swl_timeSpecMono_t* tgtTime, swl_timeSpecReal_t* srcTime);
ssize_t swl_timespec_monoToDate(char* tgtStr, size_t tgtStrSize, const swl_timeSpecMono_t* srcTime);
ssize_t swl_timespec_realToDate(char* tgtStr, size_t tgtStrSize, const swl_timeSpecReal_t* srcTime);

void swl_timespec_reset(struct timespec* ts);
int64_t swl_timespec_getTimeInNanosec(const struct timespec* ts);
int64_t swl_timespec_getTimeInMicrosec(const struct timespec* ts);
int64_t swl_timespec_getTimeInMillisec(const struct timespec* ts);
#define swl_timespec_getTimeInMicroSec swl_timespec_getTimeInMicrosec
#define swl_timespec_getTimeInNanoSec swl_timespec_getTimeInNanosec

static inline int64_t swl_timespec_toSec(struct timespec* ts) {
    return ts->tv_sec;
}
static inline int64_t swl_timespec_toMs(struct timespec* ts) {
    return swl_timespec_getTimeInMillisec(ts);
}
static inline int64_t swl_timespec_toUs(struct timespec* ts) {
    return swl_timespec_getTimeInMicrosec(ts);
}
static inline int64_t swl_timespec_toNs(struct timespec* ts) {
    return swl_timespec_getTimeInNanosec(ts);
}

int swl_timespec_diff(struct timespec* result, const struct timespec* start, const struct timespec* stop);
int64_t swl_timespec_diffToSec(struct timespec* start, struct timespec* stop);
int64_t swl_timespec_diffToMillisec(struct timespec* start, struct timespec* stop);
int64_t swl_timespec_diffToMicrosec(struct timespec* start, struct timespec* stop);
int64_t swl_timespec_diffToNanosec(struct timespec* start, struct timespec* stop);
bool swl_timespec_equals(struct timespec* firstTs, struct timespec* secondTs);
bool swl_timespec_isZero(const struct timespec* ts);

void swl_timespec_realToDatetime(swl_datetime_t* dt, const swl_timeSpecReal_t* srcTime);
void swl_timespec_monoToDatetime(swl_datetime_t* dt, const swl_timeSpecMono_t* srcTime);
void swl_timespec_datetimeToReal(swl_timeSpecReal_t* mySpec, swl_datetime_t* dateTime);
void swl_timespec_datetimeToMono(swl_timeSpecMono_t* tgtTime, swl_datetime_t* dateTime);

#define swl_type_timeSpecReal &gtSwl_type_timeSpecReal
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(TimeSpecReal, gtSwl_type_timeSpecReal, swl_timeSpecReal_t)
#define swl_type_timeSpecReal_impl gtSwl_type_timeSpecReal
SWL_REF_TYPE_H(gtSwl_type_timeSpecRealPtr, gtSwl_type_timeSpecReal);

#define swl_type_timeSpecMono &gtSwl_type_timeSpecMono
SWL_COMMON_TYPE_DEFINE_FUNCTIONS_VAL_EXT(TimeSpecMono, gtSwl_type_timeSpecMono, swl_timeSpecMono_t)
#define swl_type_timeSpecMono_impl gtSwl_type_timeSpecMono
SWL_REF_TYPE_H(gtSwl_type_timeSpecMonoPtr, gtSwl_type_timeSpecMono);

#endif /* SRC_INCLUDE_SWL_TIME_SPEC_H_ */
