/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_BIT_H_
#define SRC_INCLUDE_SWL_BIT_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * Typedefs to show that these variables contain binary data.
 * We shall not define an swl_byte_t, but to have consistency, swl_bit8_t is equivalent size to a byte.
 */
typedef uint8_t swl_bit8_t;
typedef uint16_t swl_bit16_t;
typedef uint32_t swl_bit32_t;
typedef uint64_t swl_bit64_t;



#define SWL_BYTE_SIZE(X) (sizeof(typeof(X)))
#define SWL_BIT_IN_BYTE 8
#define SWL_BIT_SIZE(X) (SWL_BYTE_SIZE(X) * SWL_BIT_IN_BYTE)

//In order for macro's to work with all value sizes, shift needs to happen with swl_bit64_t
#define SWL_BIT_SHIFT(bit) ((swl_bit64_t) 1 << (bit))

#define SWL_BIT_SET(val, bit) (val | SWL_BIT_SHIFT(bit))
#define SWL_BIT_CLEAR(val, bit) (val & ~SWL_BIT_SHIFT(bit))
#define SWL_BIT_WRITE(val, bit, value) ((value ? val | SWL_BIT_SHIFT(bit) : val & ~SWL_BIT_SHIFT(bit)))
#define SWL_BIT_SET_UNTIL(val, bit) (val | (SWL_BIT_SHIFT(bit) - (swl_bit64_t) 1))
#define SWL_BIT_CLEAR_UNTIL(val, bit) (val & (~(SWL_BIT_SHIFT(bit) - 1)))

#define SWL_BIT_IS_SET(val, bit) ((val & SWL_BIT_SHIFT(bit)) != 0)
#define SWL_BIT_IS_ONLY_SET(val, bit) (val == SWL_BIT_SHIFT(bit))

#define SWL_BIT_IS_SET_UNTIL(val, bit) ((val & (SWL_BIT_SHIFT(bit) - 1)) == (SWL_BIT_SHIFT(bit) - 1))
#define SWL_BIT_IS_CLEAR_UNTIL(val, bit) ((val & (SWL_BIT_SHIFT(bit) - 1)) == 0)

#define W_SWL_BIT_SET(val, bit) (val = SWL_BIT_SET(val, bit))
#define W_SWL_BIT_CLEAR(val, bit) (val = SWL_BIT_CLEAR(val, bit))
#define W_SWL_BIT_WRITE(val, bit, value) (val = SWL_BIT_WRITE(val, bit, value))
#define W_SWL_BIT_SET_UNTIL(val, bit) (val = SWL_BIT_SET_UNTIL(val, bit))
#define W_SWL_BIT_CLEAR_UNTIL(val, bit) (val = SWL_BIT_CLEAR_UNTIL(val, bit))

/* Reverses the bytes in a 16-bit value */
#define SWL_BIT16_SWAP(val) \
    ((swl_bit16_t) ((((swl_bit16_t) (val) & (swl_bit16_t) 0x00ffU) << 8) | \
                    (((swl_bit16_t) (val) & (swl_bit16_t) 0xff00U) >> 8)))

/* Reverses the bytes in a 32-bit value */
#define SWL_BIT32_SWAP(val) \
    ((swl_bit32_t) ((((swl_bit32_t) (val) & (swl_bit32_t) 0x000000ffU) << 24) | \
                    (((swl_bit32_t) (val) & (swl_bit32_t) 0x0000ff00U) << 8) | \
                    (((swl_bit32_t) (val) & (swl_bit32_t) 0x00ff0000U) >> 8) | \
                    (((swl_bit32_t) (val) & (swl_bit32_t) 0xff000000U) >> 24)))

/* Reverses the bytes in a 64-bit value */
#define SWL_BIT64_SWAP(val) \
    ((swl_bit64_t) ((((swl_bit64_t) (val) & 0x00000000000000ffULL) << 56) | \
                    (((swl_bit64_t) (val) & 0x000000000000ff00ULL) << 40) | \
                    (((swl_bit64_t) (val) & 0x0000000000ff0000ULL) << 24) | \
                    (((swl_bit64_t) (val) & 0x00000000ff000000ULL) << 8) | \
                    (((swl_bit64_t) (val) & 0x000000ff00000000ULL) >> 8) | \
                    (((swl_bit64_t) (val) & 0x0000ff0000000000ULL) >> 24) | \
                    (((swl_bit64_t) (val) & 0x00ff000000000000ULL) >> 40) | \
                    (((swl_bit64_t) (val) & 0xff00000000000000ULL) >> 56)))

size_t swl_bit32_getNrSet(swl_bit32_t val);
int32_t swl_bit32_getLowest(swl_bit32_t val);
int32_t swl_bit32_getHighest(swl_bit32_t val);

size_t swl_bit64_getNrSet(swl_bit64_t val);
int32_t swl_bit64_getLowest(swl_bit64_t val);
int32_t swl_bit64_getHighest(swl_bit64_t val);

//setters

void swl_bitArr32_set(swl_bit32_t* array, size_t size, size_t bitNr);
void swl_bitArr32_clear(swl_bit32_t* array, size_t size, size_t bitNr);
void swl_bitArr32_write(swl_bit32_t* array, size_t size, size_t bitNr, bool val);

void swl_bitArr32_setAll(swl_bit32_t* array, size_t size);
void swl_bitArr32_setUntil(swl_bit32_t* array, size_t size, size_t bitNr);
void swl_bitArr32_clearAll(swl_bit32_t* array, size_t size);
void swl_bitArr32_clearUntil(swl_bit32_t* array, size_t size, size_t bitNr);

//getters

bool swl_bitArr32_isSet(swl_bit32_t* array, size_t size, size_t bitNr);
bool swl_bitArr32_isAnySet(swl_bit32_t* array, size_t size);
bool swl_bitArr32_isOnlySet(swl_bit32_t* array, size_t size, size_t bitNr);
bool swl_bitArr32_equals(swl_bit32_t* array1, swl_bit32_t* array2, size_t size);

size_t swl_bitArr32_getNrSet(swl_bit32_t* array, size_t size);
int32_t swl_bitArr32_getLowest(swl_bit32_t* array, size_t size);
int32_t swl_bitArr32_getHighest(swl_bit32_t* array, size_t size);

//List setters

void swl_bitArr32_setList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize);
void swl_bitArr32_clearList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize);
void swl_bitArr32_writeList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize, bool val);

//List getters

bool swl_bitArr32_isSetList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize);
bool swl_bitArr32_isOnlySetList(swl_bit32_t* array, size_t size, size_t* bitNrArray, size_t bitNrArraySize);

// Logic functions

void swl_bitArr32_and(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size);
void swl_bitArr32_or(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size);
void swl_bitArr32_xor(swl_bit32_t* tgtArray, swl_bit32_t* array1, swl_bit32_t* array2, size_t size);
void swl_bitArr32_not(swl_bit32_t* tgtArray, swl_bit32_t* srcArray, size_t size);


#endif /* SRC_INCLUDE_SWL_BIT_H_ */

