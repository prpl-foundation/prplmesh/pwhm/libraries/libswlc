/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_SWL_ASSERT_H_
#define INCLUDE_SWL_ASSERT_H_

#include <stddef.h>
#include "debug/sahtrace.h"

/**
 * Soft at home assertion library.
 *
 * We define two types of asserts:
 * 1. Error asserts (ASSERT_), which are asserts that should NEVER be true.
 * 2. Information asserts (ASSERTI_), which can be true depending on the circumstances,
 *      but the following code requires the assert to be true.
 *
 * So to optimize:
 * Error asserts can in theory be removed, since removing this should not break any functionality, since the case
 * it handles should NEVER happen.
 * Info asserts are checks to proceed. So these CAN NOT be removed, or redefined. The functionality depends
 * on these asserts to do proper precondition checking checking.
 *
 * @param x, y: Must not have side effects since ASSERT_s must be removable.
 * @param ret: if function `f` contains `ASSERT_FALSE(x, ret, ...)` and `x` is false, `f` returns `ret`.
 * @param zone: module where the assert comes from. Do not use hardcoded values,
 *   use a `ME` macro to make code movable between files: e.g. `ASSERT_FALSE(1+1==3, 1234, ME, "Math is hard")`
 *
 */
#define ASSERT_FALSE(x, ret, zone, errMsg, ...) \
    if(x) { \
        SAH_TRACEZ_ERROR(zone, errMsg, ## __VA_ARGS__); \
        return ret; \
    }

#define ASSERTI_FALSE(x, ret, zone, errMsg, ...) \
    if(x) { \
        SAH_TRACEZ_INFO(zone, errMsg, ## __VA_ARGS__); \
        return ret; \
    }

#define ASSERTW_FALSE(x, ret, zone, errMsg, ...) \
    if(x) { \
        SAH_TRACEZ_WARNING(zone, errMsg, ## __VA_ARGS__); \
        return ret; \
    }

#define ASSERTS_FALSE(x, ret, zone, errMsg, ...) \
    if(x) { \
        return ret; \
    }


#define ASSERT_TRUE(x, ret, zone, errMsg, ...) ASSERT_FALSE(!(x), ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_TRUE(x, ret, zone, errMsg, ...) ASSERTW_FALSE(!(x), ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_TRUE(x, ret, zone, errMsg, ...) ASSERTI_FALSE(!(x), ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_TRUE(x, ret, zone, errMsg, ...) ASSERTS_FALSE(!(x), ret, zone, errMsg, ## __VA_ARGS__)

#define ASSERT_NOT_NULL(x, ret, zone, errMsg, ...) ASSERT_FALSE(x == NULL, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_NOT_NULL(x, ret, zone, errMsg, ...) ASSERTW_FALSE(x == NULL, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_NOT_NULL(x, ret, zone, errMsg, ...) ASSERTI_FALSE(x == NULL, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_NOT_NULL(x, ret, zone, errMsg, ...) ASSERTS_FALSE(x == NULL, ret, zone, errMsg, ## __VA_ARGS__)

#define ASSERT_NULL(x, ret, zone, errMsg, ...) ASSERT_TRUE(x == 0, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_NULL(x, ret, zone, errMsg, ...) ASSERTW_TRUE(x == 0, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_NULL(x, ret, zone, errMsg, ...) ASSERTI_TRUE(x == 0, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_NULL(x, ret, zone, errMsg, ...) ASSERTS_TRUE(x == 0, ret, zone, errMsg, ## __VA_ARGS__)

#define ASSERT_EQUALS(x, y, ret, zone, errMsg, ...) ASSERT_TRUE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTW_TRUE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTI_TRUE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTS_TRUE(x == y, ret, zone, errMsg, ## __VA_ARGS__)

#define ASSERT_NOT_EQUALS(x, y, ret, zone, errMsg, ...) ASSERT_FALSE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_NOT_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTW_FALSE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_NOT_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTI_FALSE(x == y, ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_NOT_EQUALS(x, y, ret, zone, errMsg, ...) ASSERTS_FALSE(x == y, ret, zone, errMsg, ## __VA_ARGS__)

//Check if string is non null & not empty
#define ASSERT_STR(x, ret, zone, errMsg, ...) ASSERT_FALSE(x == NULL || x[0] == '\0', ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTW_STR(x, ret, zone, errMsg, ...) ASSERTW_FALSE(x == NULL || x[0] == '\0', ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTI_STR(x, ret, zone, errMsg, ...) ASSERTI_FALSE(x == NULL || x[0] == '\0', ret, zone, errMsg, ## __VA_ARGS__)
#define ASSERTS_STR(x, ret, zone, errMsg, ...) ASSERTS_FALSE(x == NULL || x[0] == '\0', ret, zone, errMsg, ## __VA_ARGS__)

#define ASSERT_CMD_SUCCESS(cmd, ret, zone, errStr, ...){ \
        int _errNo = cmd; \
        if(_errNo < 0) { \
            SAH_TRACEZ_ERROR(zone, errStr, ## __VA_ARGS__); \
            return ret; \
        } \
}

#define ASSERTW_CMD_SUCCESS(cmd, ret, zone, errStr, ...){ \
        int _errNo = cmd; \
        if(_errNo < 0) { \
            SAH_TRACEZ_WARNING(zone, errStr, ## __VA_ARGS__); \
            return ret; \
        } \
}
#define ASSERTI_CMD_SUCCESS(cmd, ret, zone, errStr, ...){ \
        int _errNo = cmd; \
        if(_errNo < 0) { \
            SAH_TRACEZ_INFO(zone, errStr, ## __VA_ARGS__); \
            return ret; \
        } \
}

#define ASSERTS_CMD_SUCCESS(cmd, ret, ...){ \
        int _errNo = cmd; \
        if(_errNo < 0) { \
            return ret; \
        } \
}

#define SWL_TEST(zone) \
    SAH_TRACEZ_ERROR(zone, "tst %u", __COUNTER__); \
    usleep(2000);

#define SWL_TESTP(zone, format, ...) \
    SAH_TRACEZ_ERROR(zone, "tst %u", __COUNTER__); \
    SAH_TRACEZ_ERROR(zone, format, ## __VA_ARGS__); \
    usleep(2000);

/**
 * Check at compile time that `expr` evaluates to true.
 *
 * This is an alternative to C11's `_Static_assert` which is not always available.
 * @param expr: a boolean expression
 * @param diagnostic: a string literal. Message explaining what is wrong when `expr` is false.
 */
#define SWL_ASSERT_STATIC(expr, diagnostic) \
    extern int (*__swl_asssert_staticFun(void)) \
    [!!sizeof(struct { int __swl_assert_val : (expr) ? 1 : -1; })]

#endif /* INCLUDE_SWL_COMMON_ASSERT_H_ */
