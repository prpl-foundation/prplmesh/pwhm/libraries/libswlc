/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_SWL_EVENTQUEUE_H_
#define SRC_INCLUDE_SWL_SWL_EVENTQUEUE_H_

/**
 * @file
 * @brief
 * A simple event queue.
 *
 * This queue allows users to subscribe with a function and user data.
 * The queue shall on publish, call each subscriber function, with the provided user data.
 */

#include "swl/swl_llist.h"
#include "swl/swl_returnCode.h"

/**
 * Structure showing the event queue. Please do not directly reference the fields.
 * However, in order to actually directly declare queue in other files, full definition needs to be in header.
 */
typedef struct {
    bool init;
    bool staticName;
    swl_llist_t subscribers;
    char* name;
} swl_eventQueue_t;

/**
 * This macro shall properly initialize a queue to start subscribing and publishing. No other init necessary.
 */
#define SWL_EVENT_QUEUE_NEW(nameIn) {.init = true, .staticName = true, .name = nameIn, .subscribers = {NULL, NULL}}

typedef void swl_event_t;

//Callback function definition type.
typedef void (* bs_eventQueue_cbf)(const swl_event_t* data, void* userData);

swl_rc_ne swl_eventQueue_init(swl_eventQueue_t* queue, const char* name);
swl_rc_ne swl_eventQueue_destroy(swl_eventQueue_t* queue);
swl_rc_ne swl_eventQueue_subscribe(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data, const char* file, uint32_t line);
swl_rc_ne swl_eventQueue_cancel(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data, const char* file, uint32_t line);
bool swl_eventQueue_hasSubscriber(swl_eventQueue_t* queue, bs_eventQueue_cbf callback, void* data);
swl_rc_ne swl_eventQueue_publish(swl_eventQueue_t* queue, const swl_event_t* event);
uint32_t swl_eventQueue_getNrHandlers(swl_eventQueue_t* queue);
char* swl_eventQueue_getName(swl_eventQueue_t* queue);

/**
 * Subscribe to given even queue with provided user data.
 * User data will be provided on callback.
 */
#define SWL_EVENT_QUEUE_SUBSCRIBE(queue, callback, data) swl_eventQueue_subscribe(queue, callback, data, __FILE__, __LINE__)
#define SWL_EVENT_QUEUE_CANCEL(queue, callback, data) swl_eventQueue_cancel(queue, callback, data, __FILE__, __LINE__)


#endif /* SRC_INCLUDE_SWL_SWL_EVENTQUEUE_H_ */
