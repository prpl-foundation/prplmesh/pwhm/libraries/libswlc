/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _SWL_TLV_
#define _SWL_TLV_

#include <stdbool.h>
#include <stdint.h>
#include "swl_bit.h"
#include "swl_unLiList.h"
#include "swl_tlvItem.h"

typedef struct {
    swl_unLiList_t tlvList;
} swl_tlvList_t;

#define SWL_TLV_HDR_LEN 4

swl_rc_ne swl_tlvList_getData(swl_tlvList_t* tlvList, void* buffer, size_t bufLen, uint16_t id);
swl_rc_ne swl_tlvList_getDataTyped(swl_tlvList_t* tlvList, void* buffer, swl_type_t* dataType, uint16_t id);
void swl_tlvList_prepend(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data);
void swl_tlvList_prependType(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data, swl_type_t* dataType);
void swl_tlvList_append(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data);
void swl_tlvList_appendType(swl_tlvList_t* tlvList, uint16_t id, uint16_t len, void* data, swl_type_t* dataType);
size_t swl_tlvList_getTotalSize(swl_tlvList_t* tlvList);
swl_rc_ne swl_tlvList_parse(swl_tlvList_t* tlvList, char* in, size_t inSize);
swl_rc_ne swl_tlvList_toBuffer(swl_tlvList_t* tlvList, char* out, size_t outLen, size_t* remainingLen);
void swl_tlvList_init(swl_tlvList_t* tlvList);
void swl_tlvList_cleanup(swl_tlvList_t* tlvList);

#endif /* _SWL_TLV_ */
