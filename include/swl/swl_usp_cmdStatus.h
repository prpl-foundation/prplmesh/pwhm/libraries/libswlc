/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_SWL_USP_CMDSTATUS_H_
#define INCLUDE_SWL_USP_CMDSTATUS_H_

/*
 * @brief enum of command status codes as defined in
 * https://usp-data-models.broadband-forum.org/tr-181-2-15-1-usp.html
 * The standard only defines an enumerated string list,
 * so the relative numerical values here below are arbitrary
 * (negative for error cases)
 */
typedef enum {
    SWL_USP_CMD_STATUS_MIN = -9, // to be updated when adding new cmd status error codes
    SWL_USP_CMD_STATUS_ERROR_INVALID_MAC = -8,
    SWL_USP_CMD_STATUS_ERROR_INTERFACE_DOWN = -7,
    SWL_USP_CMD_STATUS_ERROR_TIMEOUT = -6,
    SWL_USP_CMD_STATUS_ERROR_OTHER = -5,
    SWL_USP_CMD_STATUS_ERROR_NOT_IMPLEMENTED = -4,
    SWL_USP_CMD_STATUS_ERROR_NOT_READY = -3,
    SWL_USP_CMD_STATUS_ERROR_INVALID_INPUT = -2,
    SWL_USP_CMD_STATUS_ERROR = -1,
    SWL_USP_CMD_STATUS_SUCCESS = 0,
    SWL_USP_CMD_STATUS_COMPLETE = 1,
    SWL_USP_CMD_STATUS_CANCELED = 2,
    SWL_USP_CMD_STATUS_MAX,
} swl_usp_cmdStatus_ne;

/*
 * @brief macro to count all defined (from usp tr181) command status codes
 */
#define SWL_USP_CMD_STATUS_COUNT  (SWL_USP_CMD_STATUS_MAX - SWL_USP_CMD_STATUS_MIN)

/*
 * @brief macro to define private value for unknown status code
 * (not defined in usp tr181)
 * This is a tweak to have a dynamic (but known) value not conflicting with any
 * of the defined status codes
 * It is used to have position (index) for unknown status string
 * in the status string array (sized with COUNT)
 */
#define SWL_USP_CMD_STATUS_UNDEFINED (SWL_USP_CMD_STATUS_COUNT - 1)

/*
 * @brief private string value for unknown command status
 */
#define SWL_USP_CMD_STATUS_UNDEFINED_STR ""

/**
 * @brief Return the const string for a given command status.
 *
 * @param statusCode: the status code to translate to string
 * @return string associated with given status code.
 *     Shall return SWL_USP_CMD_STATUS_UNDEFINED_STR if returnCode is unknown.
 */
const char* swl_uspCmdStatus_toString(swl_usp_cmdStatus_ne statusCode);

/**
 * @brief Return the command status code for a given status string
 *
 * @param statusStr: the status string to translate to code
 * @return cmdStatus code associated with given status string.
 *     Shall return SWL_USP_CMD_STATUS_UNDEFINED if status string does not matches.
 */
int32_t swl_uspCmdStatus_toCode(const char* statusStr);

#endif /* INCLUDE_SWL_USP_COMMANDSTATUS_H_ */
