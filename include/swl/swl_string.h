/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_STRING_H_
#define SRC_INCLUDE_SWL_STRING_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>
#include <swl/swl_returnCode.h>

void swl_str_toLower(char* str, size_t nrChar);
void swl_str_toUpper(char* str, size_t nrChar);


bool swl_str_ncopy(char* tgtStr, size_t tgtStrSize, const char* srcStr, size_t srcSubStrSize);
bool swl_str_ncat(char* tgtStr, size_t tgtStrSize, const char* srcStr, size_t srcSubStrSize);
bool swl_str_copy(char* tgtStr, size_t tgtStrSize, const char* srcStr);
bool swl_str_copyMalloc(char** tgtStr, const char* srcStr);
bool swl_str_cat(char* tgtStr, size_t tgtStrSize, const char* srcStr);
bool swl_str_catChar(char* tgtStr, size_t tgtStrSize, char srcChar);
bool swl_str_catFormat(char* tgtStr, size_t tgtStrSize, const char* format, ...);
bool swl_strlst_cat(char* tgtStr, size_t tgtStrSize, const char* separator, const char* srcStr);
bool swl_strlst_catFormat(char* tgtStr, size_t tgtStrSize, const char* separator, const char* format, ...);
bool swl_strlst_contains(const char* hayStack, const char* sep, const char* needle);
uint32_t swl_str_countChar(const char* srcStr, char filterChar);
bool swl_str_startsWith(const char* str, const char* prefix);
bool swl_str_matches(const char* str1, const char* str2);
bool swl_str_nmatches(const char* str1, const char* str2, size_t size);
ssize_t swl_str_getMismatchIndex(const char* str1, const char* str2);
ssize_t swl_str_ngetMismatchIndex(const char* str1, const char* str2, ssize_t size);

bool swl_str_addEscapeChar(char* buffer, uint32_t maxBufSize, const char* charsToEscape, char escapeChar);
ssize_t swl_str_addEscapeCharPrint(char* buffer, size_t maxBufSize, size_t curStrSize, const char* charsToEscape, char escapeChar);
bool swl_str_removeEscapeChar(char* buffer, uint32_t maxBufSize, const char* charsToEscape, char escapeChar);
void swl_str_removeWhitespaceEdges(char* buffer);
bool swl_str_stripChar(char* buffer, char stripChar);
size_t swl_str_getNonEscCharLoc(const char* buffer, char target, char escape);
bool swl_str_matchesIgnoreCase(const char* str1, const char* str2);
bool swl_str_nmatchesIgnoreCase(const char* str1, const char* str2, size_t size);
bool swl_str_replace(char* tgtStr, size_t tgtStrSize, const char* baseString, const char* toReplace, const char* replaceTgt);
bool swl_str_isEmpty(const char* str);
uint32_t swl_str_printableByteSequenceLen(const char* buffer, size_t size, bool withSpaces);
uint32_t swl_str_nrCharOccurances(char tgt, const char* buffer, size_t size, bool continuous);
size_t swl_str_removeWhitespace(char* str);
/*
 * Estimated string max size of the dumped data: 3 char per byte (considering the separator) + null ending char
 * STRING_ONE/xx/yy/yy/zz#n//STRING_TWO/jj#m
 * STRING_ONE/xx/yy/yy/zz#nOVERFLOW_STR
 */
#define DUMP_BUF_SIZE(size) (((size) > 0) ? ((size) * 3 + 1) : 1)
bool swl_str_dumpByteBuffer(char* dumpBuf, size_t dumpBufSize, const char* dataBuffer, size_t dataSize, int32_t minPrintable, bool showSpaces, const char* overflowString);
size_t swl_str_nlen(const char* str, size_t maxLen);
size_t swl_str_len(const char* str);
void swl_str_safeAssign(const char** tgtStr, const char* srcStr);
bool swl_str_getParameterValue(char* tgtStr, size_t tgtStrSize, const char* srcStr, const char* paramName, char** paramNamePos);


ssize_t swl_str_find(const char* haystack, const char* needle);
ssize_t swl_str_findNoEscape(const char* haystack, const char* needle, char escape);
bool swl_str_isOnlyWhitespace(const char* str, size_t len);
size_t swl_str_nrStrDiff(const char* test1, const char* test2, size_t len);

char* swl_strlst_copyStringAtIndex(const char* string, const char* sep, ssize_t index, bool ignoreEmptySpace);
swl_rc_ne swl_strlst_copyStringAtIndexToBuffer(char* outBuffer, size_t* outBufferLen, const char* string, const char* sep, ssize_t index, bool ignoreEmptySpace);

#endif /* SRC_INCLUDE_SWL_STRING_H_ */
