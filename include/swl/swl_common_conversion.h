/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _SWL_COMMON_CONVERSION_
#define _SWL_COMMON_CONVERSION_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include "swl_enum.h"

swl_mask_m swl_conv_charToMaskSep(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal, char separator, bool* allUsed);
swl_mask_m swl_conv_charToMask(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal);

swl_enum_e swl_conv_charToEnum(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal, swl_enum_e defaultVal);

//Note: this function must not be used directly, but only through SWL_CONV_CHAR_TO_ENUM_RET and SWL_CONV_CHAR_TO_ENUM macro's
swl_enum_e swl_conv_charToEnum_ext(const char* srcStr, const char* const* srcEnumStrList, swl_enum_e maxVal, swl_enum_e defaultVal, const char* errorFile, uint32_t line, bool* success);

bool swl_conv_uint8ArrayToChar(char* tgtStr, uint32_t tgtStrSize, uint8_t* srcData, size_t srcDataSize);

#define SWL_CONV_CHAR_TO_ENUM_RET(srcStr, srcEnumStrList, maxVal, defaultVal, successPtr) swl_conv_charToEnum_ext(srcStr, srcEnumStrList, maxVal, defaultVal, __FILE__, __LINE__, successPtr)
#define SWL_CONV_CHAR_TO_ENUM(srcStr, srcEnumStrList, maxVal, defaultVal) SWL_CONV_CHAR_TO_ENUM_RET(srcStr, srcEnumStrList, maxVal, defaultVal, NULL)

bool swl_conv_maskToCharSep(char* tgtStr, uint32_t tgtStrSize, swl_mask_m srcMask, const char* const* srcEnumStrList, swl_enum_e maxVal, char separator);
bool swl_conv_maskToChar(char* tgtStr, uint32_t tgtStrSize, swl_mask_m srcMask, const char* const* srcEnumStrList, swl_enum_e maxVal);

int32_t swl_conv_refToDbm(uint8_t ref);
int32_t swl_conv_strToUInt8Arr(uint8_t* tgtList, uint32_t tgtListSize, const char* srcStrList);

char* swl_conv_encodeAsUrl(const char* str);
char* swl_conv_decodeAsUrl(const char* str);

#endif /* _SWL_CONVERSION_ */
