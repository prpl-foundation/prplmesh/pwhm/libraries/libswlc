/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef SRC_INCLUDE_SWL_MAP_H_
#define SRC_INCLUDE_SWL_MAP_H_

#include <stdbool.h>
#include "swl/swl_common_type.h"
#include "swl/swl_unLiList.h"
#include "swl/subtypes/swl_mapSType.h"

#define SWL_MAP_DEFAULT_BLOCK_SIZE 5

typedef struct  {
    swl_type_t* keyType;
    swl_type_t* valueType;
    swl_unLiList_t dataVector;
} swl_map_t;

typedef struct {
    swl_map_t* map;
    swl_unLiListIt_t mapIt;
} swl_mapIt_t;

/**
 * Note, swl_mapEntry_t for swl_map contains the swl_typeData_t for the key and value right after each other
 */

swl_rc_ne swl_map_init(swl_map_t* map, swl_type_t* keyType, swl_type_t* valueType);
swl_rc_ne swl_map_initExt(swl_map_t* map, swl_type_t* keyType, swl_type_t* valueType, uint32_t blockSize);
bool swl_map_isInitialized(const swl_map_t* map);
void swl_map_cleanup(swl_map_t* map);
void swl_map_clear(swl_map_t* map);
uint32_t swl_map_size(const swl_map_t* map);

bool swl_map_add(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* data);
void swl_map_delete(swl_map_t* map, const swl_typeData_t* key);
swl_typeData_t* swl_map_get(const swl_map_t* map, const swl_typeData_t* key);
swl_typeEl_t* swl_map_getRef(const swl_map_t* map, const swl_typeData_t* key);
swl_mapEntry_t* swl_map_find(const swl_map_t* map, const swl_typeData_t* data);
bool swl_map_getCharFromChar(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, const char* key);
bool swl_map_set(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* data);
bool swl_map_addOrSet(swl_map_t* map, swl_typeData_t* key, swl_typeData_t* data);
bool swl_map_has(const swl_map_t* map, swl_typeData_t* key);

bool swl_map_toChar(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map);
bool swl_map_toCharSep(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, char keyValSep, char tupleSep);
bool swl_map_toCharSepEsc(char* tgtStr, uint32_t tgtStrSize, const swl_map_t* map, char keyValSep, char tupleSep,
                          const char* charsToEscape, char escapeChar);
bool swl_map_fromChar(swl_map_t* tgtMap, const char* srcStr, bool overwrite);

bool swl_map_equals(const swl_map_t* map1, const swl_map_t* map2);
bool swl_map_copyTo(swl_map_t* destMap, swl_map_t* srcMap);

swl_mapEntry_t* swl_map_alloc(swl_map_t* map);
swl_mapEntry_t* swl_map_getEntry(const swl_map_t* map, const swl_typeData_t* key);
void swl_map_deleteEntry(swl_map_t* map, swl_mapEntry_t* entry);
swl_typeEl_t* swl_map_getEntryKeyRef(const swl_map_t* map, const swl_mapEntry_t* entry);
swl_typeEl_t* swl_map_getEntryValueRef(const swl_map_t* map, const swl_mapEntry_t* entry);
swl_typeData_t* swl_map_getEntryKeyValue(const swl_map_t* map, const swl_mapEntry_t* entry);
swl_typeData_t* swl_map_getEntryValueValue(const swl_map_t* map, const swl_mapEntry_t* entry);

void swl_map_firstIt(swl_mapIt_t* mapIt, const swl_map_t* map);
void swl_map_nextIt(swl_mapIt_t* mapIt);
swl_typeData_t* swl_map_itKey(swl_mapIt_t* mapIt);
swl_typeData_t* swl_map_itValue(swl_mapIt_t* mapIt);
uint32_t swl_map_itIndex(swl_mapIt_t* mapIt);
ssize_t swl_map_itKeyChar(char* tgtBuffer, uint32_t tgtSize, swl_mapIt_t* mapIt);
ssize_t swl_map_itValueChar(char* tgtBuffer, uint32_t tgtSize, swl_mapIt_t* mapIt);
ssize_t swl_map_keyToFile(FILE* file, swl_mapIt_t* mapIt, const swl_print_args_t* printArg);
ssize_t swl_map_valueToFile(FILE* file, swl_mapIt_t* mapIt, const swl_print_args_t* printArg);

#define swl_map_for_each(item, map) \
    for(swl_map_firstIt(&item, map); item.mapIt.valid; swl_map_nextIt(&item))

#endif /* SRC_INCLUDE_SWL_MAP_H_ */
