/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _SWL_HEX_
#define _SWL_HEX_

#include <stdbool.h>
#include <stdint.h>
#include "swl/swl_bit.h"

#define SWL_HEX_UPPER_LIST "0123456789ABCDEF"
#define SWL_HEX_LOWER_LIST "0123456789abcdef"

bool swl_hex_isSepOneChar(const char c);
bool swl_hex_isHexOneChar(const char c);
bool swl_hex_isHexChar(const char* str, size_t strSize);
bool swl_hex_fromBytes(char* tgtStr, size_t strSize, const swl_bit8_t* bytes, size_t bytesSize, bool upperCase);
bool swl_hex_toBytes(swl_bit8_t* target, size_t targetSize, const char* str, size_t strSize);
bool swl_hex_findValidSepChar(char* pSep, const char* str, size_t strSize);
bool swl_hex_fromBytesSep(char* tgtStr, size_t tgtStrSize, const swl_bit8_t* srcBytes, size_t srcBytesSize, bool upperCase, char separator, size_t* pLenWritten);
bool swl_hex_toBytesSep(swl_bit8_t* target, size_t targetSize, const char* srcStr, size_t srcStrSize, char separator, size_t* pLenWritten);
bool swl_hex_toUint8(uint8_t* target, const char* src, size_t srcSize);
bool swl_hex_toUint16(uint16_t* target, const char* src, size_t srcSize);
bool swl_hex_toUint32(uint32_t* target, const char* src, size_t srcSize);

#endif /* _SWL_HEX_ */
