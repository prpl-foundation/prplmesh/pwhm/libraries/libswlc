/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_SWL_TYPE_TYPES_H_
#define SRC_INCLUDE_SWL_SWL_TYPE_TYPES_H_


#include "swl/fileOps/swl_print.h"
#include "swl/swl_llist.h"

/**
 * TypeData references the actual data of the type.
 * * For value types, typedata is the struct type of type: e.g.  swl_typeData_t of gtSwl_type_uint16 is uint16_t
 * * For reference types, typeData is the struct type of the data referred:  e.g. swl_typeData_t of gtSwl_type_charPtr is char
 */
typedef void swl_typeData_t;

/**
 * TypeEl references the actual type element. So for both value and reference types, the typeEl refers to
 * the actual data containig the type.
 * * Example ref type: swl_typeEl_t for gtSwl_type_uint16 is uint16_t
 * * Example val type: swl_typeEl_t for gtSwl_type_charPtr is char*
 */
typedef void swl_typeEl_t;

typedef struct swl_type_s swl_type_t;

/**
 * Function handler depending on the subType.
 * Subtype is determined by flag. Not all flags may currently support subTypeFun.
 */
typedef void swl_subTypeFun_t;

/**
 * Print the type to a char buffer.
 *
 * Returns the number of bytes that would be written, if the tgtStr were big enough.
 * If there is an issue unrelated to buffer size, a negative number is returned.
 */
typedef ssize_t (* swl_type_toChar_cb) (swl_type_t* type, char* tgtStr, size_t tgtStrSize, const swl_typeData_t* srcData);
typedef bool (* swl_type_fromChar_cb) (swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr);
typedef bool (* swl_type_fromCharExt_cb) (swl_type_t* type, swl_typeEl_t* tgtData, const char* srcStr, const swl_print_args_t* args);
typedef bool (* swl_type_equals_cb) (swl_type_t* type, const swl_typeData_t* key0, const swl_typeData_t* key1);
typedef swl_typeData_t* (* swl_type_copy_cb) (swl_type_t* type, const swl_typeData_t* src);
typedef bool (* swl_type_toFile_cb) (swl_type_t* type, FILE* file, const swl_typeData_t* srcData, swl_print_args_t* args);
typedef bool (* swl_type_toVariant_cb) (swl_type_t* type, void* tgt, const swl_typeData_t* srcData);
typedef bool (* swl_type_fromVariant_cb) (swl_type_t* type, swl_typeEl_t* tgtData, const void* src);
typedef void (* swl_type_cleanup_cb) (swl_type_t* type, swl_typeEl_t* tgtData);

/**
 * Type flags to allow easier encoding in json and other types
 * If no flag is set, then it's assumed to be a string value
 */
typedef enum {
    SWL_TYPE_FLAG_VALUE, //Default value, for types that encode to a simple string
    SWL_TYPE_FLAG_LIST,
    SWL_TYPE_FLAG_MAP,
    SWL_TYPE_FLAG_BASIC, // Basic json value of true, false, null
    SWL_TYPE_FLAG_NUMBER,
    SWL_TYPE_FLAG_REF,   // Special flag for reference type, to get underlying flag, get ref type.
    SWL_TYPE_FLAG_MAX,
} swl_typeFlag_e;

extern const char* swl_typeFlag_str[];

typedef uint16_t swl_typeExtUID_t;
typedef void swl_typeExtFun_t;

typedef struct {
    swl_llist_iterator_t it;
    swl_typeExtUID_t id;
    swl_typeExtFun_t* extFun;
} swl_typeExtIt_t;

typedef struct {
    const char* name;
    bool isValue;
    swl_typeFlag_e flag;
    swl_type_toChar_cb toChar;           // mandatory
    swl_type_fromChar_cb fromChar;       // mandatory this or fromCharExt
    swl_type_fromCharExt_cb fromCharExt; // mandatory this or fromChar
    swl_type_toFile_cb toFile;           // optional, if not set, toChar shall be used
    swl_type_equals_cb equals;           // optional, if not set, memcmp is used with len size
    swl_type_copy_cb copy;               // optional, if not set, memcpy is used with len size
    swl_type_cleanup_cb cleanup;         // optional, if not set, memset 0 is used with len size
    swl_type_toVariant_cb toVariant;     // optional, if not set, toChar is used
    swl_type_fromVariant_cb fromVariant; // optional, if not set, fromChar is used
    swl_llist_t extensions;              // optional, linked list of extensions
    swl_subTypeFun_t* subFun;            // optional, function table for handling of types with specific flag.
} swl_typeFun_t;

struct swl_type_s {
    size_t size;
    swl_typeFun_t* typeFun;
};

#endif /* SRC_INCLUDE_SWL_SWL_TYPE_TYPES_H_ */
