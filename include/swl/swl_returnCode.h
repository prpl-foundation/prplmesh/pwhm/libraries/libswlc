/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef SRC_INCLUDE_SWL_RETURNCODE_H_
#define SRC_INCLUDE_SWL_RETURNCODE_H_

#include <stdbool.h>

/**
 * Enumeration listing return codes
 */
typedef enum {
    SWL_RC_MIN = -7,

    /**
     * Indicates that the operation results in going out of valid bounds
     * of a function. For example the multiplication of 1^30 * 1^30, while both
     * valid parameters, results in going out of bounds of a uint32_t value.
     */
    SWL_RC_RESULT_OUT_OF_BOUNDS = -6,

    /**
     * Indicates that requested resources are not available at this time.
     * Can indicate not enough memory, disk space, or execution units.
     * Trying again later may succeed.
     */
    SWL_RC_NOT_AVAILABLE = -5,

    /**
     * Requested function is not implemented.
     * Should be returned when creating stub functions.
     */
    SWL_RC_NOT_IMPLEMENTED = -4,

    /**
     * System is not in a state to execute requested function at this time.
     */
    SWL_RC_INVALID_STATE = -3,

    /**
     * One or more provided parameters are not as expected.
     *
     * Should be returned when a parameter is NULL while it shouldn't be null,
     * or when integers are proved that are out of range (e.g. invalid enum / mask value)
     */
    SWL_RC_INVALID_PARAM = -2,

    /**
     * Generic error, when the previous error classes don't apply.
     */
    SWL_RC_ERROR = -1,

    /**
     * Generic ok return code.
     */
    SWL_RC_OK = 0,

    /**
     * Return code to signal that the requested action is completed successfully, and doesn't need to continue anymore.
     * Typically used in following cases:
     * * when a function can either return immediately as it's done or continue longer. Indicates done immediately.
     * * when caller can call the same function multiple times to get progressive results,
     *   e.g. read full content of a buffer in chunks. In that case this indicates no further results are available.
     */
    SWL_RC_DONE = 1,

    /**
     * Return code to signal that function executed successfully and a longer lasting process is started,
     * or some more data is available.
     *
     * Generally should be considered the other side of the SWL_RC_DONE return code, where in this case,
     * there is some execution continuing after return, or more data is available when calling again.
     */
    SWL_RC_CONTINUE = 2,

    SWL_RC_MAX = 3
} swl_rc_ne;

const char* swl_rc_toString(swl_rc_ne returnCode);

bool swl_rc_isOk(swl_rc_ne returnCode);

#endif /* SRC_INCLUDE_SWL_RETURNCODE_H_ */
